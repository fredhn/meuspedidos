﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class FormaPagamentoJ
    {
        [MaxLength(100)]
        public string nome { get; set; }

        public bool excluido { get; set; }
    }
}

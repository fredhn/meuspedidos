﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class Usuario
    {
        /*
        **  Entidade Usuário
        */
        public int id { get; set; }

        public string cpf { get; set; }

        public string type { get; set; }

        public string cnpj { get; set; }

        public string email { get; set; }

        public byte sms { get; set; } = 0;

        public string phone { get; set; }

        public string gender { get; set; }

        public DateTime birth { get; set; }

        public string password { get; set; }

        public string comments { get; set; }

        public string last_name { get; set; }

        public string cellphone { get; set; }

        public string permission { get; set; }

        public string first_name { get; set; }

        public byte in_trash { get; set; } = 0;

        public int entity_groups_id { get; set; }

        public byte newsletters { get; set; } = 0;

        public int? shipment_addresses_id { get; set; }

        public float allowed_discount { get; set; } = 0;

        public DateTime created_at { get; set; } = DateTime.Now;
    }
}

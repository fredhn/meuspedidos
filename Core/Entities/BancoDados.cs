﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class BancoDados
    {
        public int id { get; set; }

        [MaxLength(30)]
        public string nome_conexao { get; set; }

        [MaxLength(100)]
        public string nome_servidor { get; set; }

        [MaxLength(100)]
        public string nome_bancodados { get; set; }

        [MaxLength(100)]
        public string usuario_bancodados { get; set; }

        [MaxLength(100)]
        public string senha_bancodados { get; set; }

        public bool autenticacao_segura { get; set; }


    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class Empresa
    {
        /*
        **  Entidade Empresa
        */
        public int id { get; set; }

        public string login { get; set; }

        public string cnpj { get; set; }

        public string ultima_validacao { get; set; }

        public string password { get; set; }

        public string system_version { get; set; }

        public int license_quantity { get; set; }

        public int expire_tolerance { get; set; }

        public string comments { get; set; }

        public string permission { get; set; }

        public string razao_social { get; set; }

        public byte active { get; set; } = 0;

        public byte in_trash { get; set; } = 0;

        public DateTime created_at { get; set; } = DateTime.Now;

        public string app_token { get; set; }
    }
}

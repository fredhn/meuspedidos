﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class ClienteTabelaPreco
    {
        /*
        **  A entidade tabela de preço por cliente permite que as tabelas de preço sejam vinculadas aos clientes, 
        **  permitindo a liberação/bloqueio de tabelas de preço para clientes específicos.
        */
        public int id { get; set; }

        public int cliente_id { get; set; }

        public string tabelas_liberadas { get; set; }

        public string tabelas_liberadas_desc { get; set; }

        [MaxLength(1)]
        public string mptran { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class ClienteCategoria
    {
        /*
        **  A entidade de categorias por cliente permite apenas a inclusão de vínculos entre cliente e categoria
        */
        public int id { get; set; }

        public int cliente_id { get; set; }

        public string categorias_liberadas { get; set; }

        public string categorias_liberadas_desc { get; set; }

        [MaxLength(1)]
        public string mptran { get; set; }
    }
}

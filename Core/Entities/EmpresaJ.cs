﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class EmpresaJ
    {
        /*
        **  Entidade EmpresaJ
        "ZF1_ATIVO":"S",
        "ZF1_CLIENT":"000001",
        "ZF1_CNPJ":"00000000000000",
        "ZF1_LOJA":"01",
        "ZF1_NOME":"PARTNERS RL SOLUCOES EM INFORMATICA",
        "ZF2_DESCRI":"SINCRONIZADOR MEUS PEDIDOS",
        "ZF2_LOGIN":"PARTNERSRL",
        "ZF2_PROD":"PAR_SINCMP",
        "ZF2_SENHA":"123456"
        */

        public string ZF1_ATIVO { get; set; }

        public string ZF1_CLIENT { get; set; }

        public string ZF1_CNPJ { get; set; }

        public string ZF1_LOJA { get; set; }

        public string ZF1_NOME { get; set; }

        public string ZF2_DESCRI { get; set; }

        public string ZF2_LOGIN { get; set; }

        public string ZF2_PROD { get; set; }

        public string ZF2_SENHA { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class Transportadora
    {
        public int id { get; set; }

        [MaxLength(100)]
        public string nome { get; set; }

        [MaxLength(50)]
        public string cidade { get; set; }

        [MaxLength(2)]
        public string estado { get; set; }

        [MaxLength(500)]
        public string informacoes_adicionais { get; set; }

        public List<Telefone> telefones { get; set; }

        public bool excluido { get; set; }

        public DateTime ultima_alteracao { get; set; }

        [MaxLength(1)]
        public string mptran { get; set; }

        public Transportadora()
        {
            telefones = new List<Telefone>(); 
        }

    }
}

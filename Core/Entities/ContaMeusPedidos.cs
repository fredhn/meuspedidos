﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class ContaMeusPedidos
    {
        public int id { get; set; }

        [MaxLength(2)]
        public string filial { get; set; }

        [MaxLength(2)]
        public string codigo { get; set; }

        [MaxLength(60)]
        public string descricao { get; set; }

        [MaxLength(200)]
        public string url { get; set; }

        [MaxLength(150)]
        public string apptoken { get; set; }

        [MaxLength(150)]
        public string cmptoken { get; set; }

        [MaxLength(1)]
        public string status { get; set; }

        public bool enviaPedido {get; set;}

        public bool excluido { get; set; }
    }
}

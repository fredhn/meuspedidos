﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class FormaPagamento
    {
        public int id { get; set; }

        [MaxLength(100)]
        public string nome { get; set; }

        public bool excluido { get; set; }

        public DateTime ultima_alteracao { get; set; }

        [MaxLength(1)]
        public string mptran { get; set; }
    }
}

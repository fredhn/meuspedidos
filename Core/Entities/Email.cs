﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class Email
    {
        public int id { get; set; }

        [MaxLength(1)]
        public string tipo { get; set; }

        [MaxLength(75)]
        public string email { get; set; }
    }
}

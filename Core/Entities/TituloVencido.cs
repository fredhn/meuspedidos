﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class TituloVencido
    {
        public int id { get; set; }

        [MaxLength(18)]
        public string numero_documento { get; set; }

        public double valor { get; set; }

        public DateTime data_vencimento { get; set; }

        [MaxLength(500)]
        public string observacao { get; set; }

        public int cliente_id { get; set; }

        public bool excluido { get; set; }

        public DateTime ultima_alteracao { get; set; }

        [MaxLength(1)]
        public string mptran { get; set; }
    }
}

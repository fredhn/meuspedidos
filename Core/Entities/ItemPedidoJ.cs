﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class ItemPedidoJ
    {
        public int produto_id { get; set; }

        public int tabela_preco_id { get; set; }

        public double preco_bruto { get; set; }

        public double preco_liquido { get; set; }

        public double preco_minimo { get; set; }

        public int moeda { get; set; }

        public double cotacao_moeda { get; set; }

        public List<double> descontos { get; set; }

        [MaxLength(500)]
        public string observacoes { get; set; }

        public double ipi { get; set; }

        [MaxLength(1)]
        public string tipo_ipi { get; set; }

        public double st { get; set; }
    }
}

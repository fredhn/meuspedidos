﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class PedidoJ
    {
        public int cliente_id { get; set; }


        public int transportadora_id { get; set; }

        //public int criador_id { get; set; }

        //public string condicao_pagamento { get; set; }

        public int condicao_pagamento_id { get; set; }

        //public int forma_pagamento_id { get; set; }

        public string data_emissao { get; set; }

        [MaxLength(500)]
        public string observacoes { get; set; }

        public List<ItemPedidoJ> itens { get; set; }

        public List<PedidoCampoExtraJ> extras { get; set; }

        public PedidoJ()
        {
            itens = new List<ItemPedidoJ>();
            extras = new List<PedidoCampoExtraJ>();
        }
    }
}

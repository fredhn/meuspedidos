﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class Entidade_Saida
    {
        public int id { get; set; }

        public int id_contamp { get; set; }

        [MaxLength(2)]
        public string filial { get; set; }
        
        [MaxLength(20)]
        public string tpreg { get; set; }

        [MaxLength(40)]
        public string id_ent { get; set; }

        [MaxLength(40)]
        public string id_mp { get; set; }

        [MaxLength(1)]
        public string status { get; set; }

        [MaxLength(3)]
        public string codret { get; set; }

        [MaxLength(25)]
        public string dtinte { get; set; }

        [MaxLength(1)]
        public string mptran { get; set; }
        
        public string json { get; set; }

        public string jsonret { get; set; }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class Vendedor
    {
        /*
        **  Esta entidade faz a associação entre os Produtos e as Tabelas de Preço.
        */
        public int id { get; set; }

        [MaxLength(100)]
        public string nome { get; set; }

        [MaxLength(75)]
        public string email { get; set; }

        [MaxLength(75)]
        public string telefone { get; set; }

        public bool administrador { get; set; }

        public bool excluido { get; set; }

        public DateTime ultima_alteracao { get; set; }
    }
}

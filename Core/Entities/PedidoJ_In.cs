﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class PedidoJ_In
    {
        public int id { get; set; }

        public int cliente_id { get; set; }

        [MaxLength(100)]
        public string cliente_razao_social { get; set; }

        [MaxLength(100)]
        public string cliente_nome_fantasia { get; set; }

        [MaxLength(14)]
        public string cliente_cnpj { get; set; }

        [MaxLength(30)]
        public string cliente_inscricao_estadual { get; set; }

        [MaxLength(100)]
        public string cliente_rua { get; set; }

        public string cliente_numero { get; set; }

        [MaxLength(50)]
        public string cliente_complemento { get; set; }

        [MaxLength(8)]
        public string cliente_cep { get; set; }

        [MaxLength(30)]
        public string cliente_bairro { get; set; }

        [MaxLength(50)]
        public string cliente_cidade { get; set; }

        [MaxLength(2)]
        public string cliente_estado { get; set; }

        [MaxLength(20)]
        public string cliente_suframa { get; set; }

        public int representada_id { get; set; }

        [MaxLength(50)]
        public string representada_nome_fantasia { get; set; }

        [MaxLength(50)]
        public string representada_razao_social { get; set; }

        public int transportadora_id { get; set; }

        public int criador_id { get; set; }

        [MaxLength(50)]
        public string nome_contato { get; set; }

        [MaxLength(1)]
        public string status { get; set; }

        public int numero { get; set; }

        public double total { get; set; }

        [MaxLength(100)]
        public string condicao_pagamento { get; set; }

        public int condicao_pagamento_id { get; set; }

        public int forma_pagamento_id { get; set; }

        public DateTime data_emissao { get; set; }

        [MaxLength(500)]
        public string observacoes { get; set; }

        [MaxLength(1)]
        public string status_faturamento { get; set; }

        public List<ItemPedidoJ> items { get; set; }

        public List<PedidoCampoExtra> extras { get; set; }

        public DateTime ultima_alteracao { get; set; }

        public PedidoJ_In()
        {
            items = new List<ItemPedidoJ>();
            extras = new List<PedidoCampoExtra>();
        }

        [MaxLength(1)]
        public string mptran { get; set; }
    }
}

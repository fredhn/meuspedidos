﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class ClienteTabelaPrecoJ
    {
        /*
        **  A entidade de condições de pagamento por cliente permite apenas a inclusão de vínculos entre cliente e condição de pagamento.
        */

        public int cliente_id { get; set; }

        public List<int> tabelas_liberadas { get; set; }

        public ClienteTabelaPrecoJ()
        {
            tabelas_liberadas = new List<int>();
        }
    }
}

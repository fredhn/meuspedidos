﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class ProdutoJ
    {

        [MaxLength(50)]
        public string codigo { get; set; }

        [MaxLength(100)]
        public string nome { get; set; }

        public double comissao { get; set; }

        public double preco_tabela { get; set; }

        public double preco_minimo { get; set; }

        public double ipi { get; set; }

        [MaxLength(1)]
        public string tipo_ipi { get; set; }

        public double st { get; set; }

        //public virtual PersistableStringCollection grade_cores { get; set; }
        public List<string> grade_cores { get; set; }

        //public virtual PersistableStringCollection grade_tamanhos { get; set; }
        public List<string> grade_tamanhos { get; set; }

        [MaxLength(1)]
        public string moeda { get; set; }

        [MaxLength(10)]
        public string unidade { get; set; }

        //[MaxLength(7)]
        public double saldo_estoque { get; set; }

        [MaxLength(500)]
        public string observacoes { get; set; }

        public bool excluido { get; set; }

        public bool ativo { get; set; }

        public int categoria_id { get; set; }

        public string codigo_ncm { get; set; }

        public double multiplo { get; set; }

        public double peso_bruto { get; set; }

        public ProdutoJ()
        {
            grade_cores = new List<string>();
            grade_tamanhos = new List<string>();
        }
    }
}

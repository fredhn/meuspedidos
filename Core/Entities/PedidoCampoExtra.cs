﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class PedidoCampoExtra
    {
        /*
        **  A entidade de categorias por cliente permite apenas a inclusão de vínculos entre cliente e categoria
        */
        public int id { get; set; }

        [MaxLength(50)]
        public string nome { get; set; }

        [MaxLength(1)]
        public string tipo { get; set; }

        [MaxLength(500)]
        public string valor { get; set; }

        public bool excluido { get; set; }

        public DateTime ultima_alteracao { get; set; }

        [MaxLength(1)]
        public string mptran { get; set; }
    }
}

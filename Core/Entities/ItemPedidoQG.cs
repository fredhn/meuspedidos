﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class ItemPedidoQG : ItemPedidoJ
    {
        public List<QuantidadeGrades> quantidade_grades { get; set; }

        public ItemPedidoQG()
        {
            this.quantidade_grades = new List<QuantidadeGrades>();
        }
    }
}

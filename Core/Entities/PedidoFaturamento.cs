﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class PedidoFaturamento
    {
        public int id { get; set; }

        public int pedido_id { get; set; }

        public decimal valor_faturado { get; set; }

        public DateTime data_faturamento { get; set; }

        [MaxLength(500)]
        public string numero_nf { get; set; }

        [MaxLength(500)]
        public string informacoes_adicionais { get; set; }

        public bool excluido { get; set; }

        [MaxLength(1)]
        public string mptran { get; set; }
    }
}

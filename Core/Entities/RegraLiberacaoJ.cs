﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class RegraLiberacaoJ
    {
        /*
        **  A entidade de categorias por cliente permite apenas a inclusão de vínculos entre cliente e categoria
        */
        public int cliente_id { get; set; }

        public int usuario_id { get; set; }

        public bool liberado { get; set; }
    }
}

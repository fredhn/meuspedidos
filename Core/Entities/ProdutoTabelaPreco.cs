﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class ProdutoTabelaPreco
    {
        /*
        **  Esta entidade faz a associação entre os Produtos e as Tabelas de Preço.
        */
        public int id { get; set; }

        public float preco { get; set; }

        public int tabela_id { get; set; }

        public int produto_id { get; set; }

        public bool excluido { get; set; }

        public DateTime ultima_alteracao { get; set; }

        [MaxLength(1)]
        public string mptran { get; set; }
    }
}

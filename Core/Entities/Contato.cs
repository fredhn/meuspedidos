﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class Contato
    {
        public int id { get; set; }

        [MaxLength(50)]
        public string nome { get; set; }

        [MaxLength(30)]
        public string cargo { get; set; }

        public bool excluido { get; set; }

        public List<Email> email { get; set; }

        public List<Telefone> telefones { get; set; }

        public Contato()
        {
            email = new List<Email>();
            telefones = new List<Telefone>();
        }
    }
}

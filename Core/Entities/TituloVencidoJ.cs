﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class TituloVencidoJ
    {
        [MaxLength(18)]
        public string numero_documento { get; set; }

        public double valor { get; set; }

        public string data_vencimento { get; set; }

        [MaxLength(500)]
        public string observacao { get; set; }

        public int cliente_id { get; set; }
    }
}

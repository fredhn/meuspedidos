﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class GradeTamanho
    {
        public int id { get; set; }

        [MaxLength(50)]
        public string tamanho { get; set; }
    }
}

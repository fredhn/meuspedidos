﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class Cliente
    {
        public int id { get; set; }

        [MaxLength(100)]
        public string razao_social { get; set; }

        [MaxLength(100)]
        public string nome_fantasia { get; set; }

        [MaxLength(1)]
        public string tipo { get; set; }

        [MaxLength(18)]
        public string cnpj { get; set; }

        [MaxLength(30)]
        public string inscricao_estadual { get; set; }

        [MaxLength(20)]
        public string suframa { get; set; }

        [MaxLength(100)]
        public string rua { get; set; }

        [MaxLength(50)]
        public string complemento { get; set; }

        [MaxLength(9)]
        public string cep { get; set; }

        [MaxLength(30)]
        public string bairro { get; set; }

        [MaxLength(50)]
        public string cidade { get; set; }

        [MaxLength(2)]
        public string estado { get; set; }

        [MaxLength(500)]
        public string observacao { get; set; }

        public List<Email> emails { get; set; }

        public List<Telefone> telefones { get; set; }

        public List<Contato> contatos { get; set; }

        [MaxLength(20)]
        public string nome_excecao_fiscal { get; set; }

        public bool excluido { get; set; }

        public DateTime ultima_alteracao { get; set; }

        [MaxLength(1)]
        public string mptran { get; set; }

        public Cliente()
        {
            emails = new List<Email>();
            telefones = new List<Telefone>();
            contatos = new List<Contato>();
        }
    }
}

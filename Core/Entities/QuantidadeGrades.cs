﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class QuantidadeGrades
    {
        /*
        **  A entidade de categorias por cliente permite apenas a inclusão de vínculos entre cliente e categoria
        */
        public int id { get; set; }

        [MaxLength(15)]
        public string cor { get; set; }

        [MaxLength(15)]
        public string tamanho { get; set; }

        public double quantidade { get; set; }

    }
}

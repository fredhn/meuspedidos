﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class TabelaPrecoJ
    {
        [MaxLength(100)]
        public string nome { get; set; }

        [MaxLength(2)]
        public string tipo { get; set; }

        public float acrescimo { get; set; }

        public float desconto { get; set; }

        public bool excluido { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class ClienteCondicaoPagamento
    {
        /*
        **  A entidade de condições de pagamento por cliente permite apenas a inclusão de vínculos entre cliente e condição de pagamento.
        */
        public int id { get; set; }

        public int cliente_id { get; set; }

        public string condicoes_pagamento_liberadas { get; set; }

        public string condicoes_pagamento_liberadas_desc { get; set; }

        [MaxLength(1)]
        public string mptran { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class PedidoCampoExtraJ
    {
        /*
        **  A entidade de categorias por cliente permite apenas a inclusão de vínculos entre cliente e categoria
        */
        public int id { get; set; }

        [MaxLength(500)]
        public string valor { get; set; }
    }
}

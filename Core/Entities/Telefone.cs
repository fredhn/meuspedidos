﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class Telefone
    {
        public int id { get; set; }

        [MaxLength(1)]
        public string tipo { get; set; }

        [MaxLength(30)]
        public string numero { get; set; }

    }
}

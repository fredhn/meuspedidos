﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class Log_Licenciamento
    {
        public int id { get; set; }

        public string json { get; set; }

        public string status_code { get; set; }

        public string status_description { get; set; }

        public DateTime created_at { get; set; } = DateTime.Now;
    }
}

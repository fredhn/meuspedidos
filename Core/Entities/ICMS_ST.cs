﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class ICMS_ST
    {
        public int id { get; set; }

        [MaxLength(20)]
        public string codigo_ncm { get; set; }

        [MaxLength(20)]
        public string nome_excecao_fiscal { get; set; }

        [MaxLength(2)]
        public string estado_destino { get; set; }

        public string tipo_st { get; set; }

        public double valor_mva { get; set; }

        public double valor_pmc { get; set; }

        public double icms_credito { get; set; }

        public double icms_destino { get; set; }

        public bool excluido { get; set; }

        public DateTime ultima_alteracao { get; set; }

        [MaxLength(1)]
        public string mptran { get; set; }
    }
}

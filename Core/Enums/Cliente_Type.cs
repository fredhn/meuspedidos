﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public enum Cliente_Type
    {
        [Description("Pessoa Física")]
        F,
        [Description("Pessoa Jurídica")]
        J
    }
}

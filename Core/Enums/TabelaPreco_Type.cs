﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public enum TabelaPreco_Type
    {
        [Description("Preço Livre")]
        P,
        [Description("Acréscimo")]
        A,
        [Description("Desconto")]
        D
    }
}

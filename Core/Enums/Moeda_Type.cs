﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public enum Moeda_Type
    {
        [Description("Real")]
        Real = 0,
        [Description("Dólar")]
        Dolar = 1,
        [Description("Euro")]
        Euro = 2,
        [Description("Outras")]
        Outras = 3
    }
}

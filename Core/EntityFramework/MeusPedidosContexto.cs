﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Database.DataConnection;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Infrastructure;
using Core.Migrations;
using System.Data.Entity.Migrations;

namespace Core
{
    public class MeusPedidosContexto : DbContext
    {
        public MeusPedidosContexto() : base (SQL_DbConnection.Conection().ConnectionString)
        {
            System.Data.Entity.Database.SetInitializer(new MigrateDatabaseToLatestVersion<MeusPedidosContexto, Core.Migrations.Configuration>());
            //this.Database.CreateIfNotExists();
            //this.Configuration.LazyLoadingEnabled = false;
            //System.Data.Entity.Database.SetInitializer(new MigrateDatabaseToLatestVersion<MeusPedidosContexto, Configuration>());
        }

        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<ClienteCategoria> ClientesCategorias { get; set; }
        public DbSet<ClienteCondicaoPagamento> ClientesCondicoesPagamento { get; set; }
        public DbSet<ClienteTabelaPreco> ClientesTabelaPreco { get; set; }
        public DbSet<CondicaoPagamento> CondicoesPagamento { get; set; }
        public DbSet<Contato> Contatos { get; set; }
        public DbSet<Email> Emails { get; set; }
        public DbSet<FormaPagamento> FormasPagamento { get; set; }
        public DbSet<ICMS_ST> ICMS_ST { get; set; }
        public DbSet<ItemPedido> ItemsPedido { get; set; }
        public DbSet<Pedido> Pedidos { get; set; }
        public DbSet<PedidoCampoExtra> PedidosCampoExtra { get; set; }
        public DbSet<PedidoFaturamento> PedidosFaturamento { get; set; }
        public DbSet<Produto> Produtos { get; set; }
        public DbSet<GradeCor> GradesCor { get; set; }
        public DbSet<GradeTamanho> GradesTamanho { get; set; }
        public DbSet<ProdutoCategoria> ProdutoCategorias { get; set; }
        public DbSet<ProdutoTabelaPreco> ProdutosTabelaPreco { get; set; }
        public DbSet<QuantidadeGrades> QuantidadeGrades { get; set; }
        public DbSet<RegraLiberacao> RegrasLiberacao { get; set; }
        public DbSet<TabelaPreco> TabelasPreco { get; set; }
        public DbSet<Telefone> Telefones { get; set; }
        public DbSet<TituloVencido> TitulosVencidos { get; set; }
        public DbSet<Transportadora> Transportadoras { get; set; }
        public DbSet<BancoDados> BancosDados { get; set; }
        public DbSet<ContaMeusPedidos> ContasMeusPedidos { get; set; }
        public DbSet<Entidade_Entrada> Entidades_Entrada { get; set; }
        public DbSet<Entidade_Saida> Entidades_Saida { get; set; }
        public DbSet<Vendedor> Vendedores { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Empresa> Empresas { get; set; }
        public DbSet<Log_Licenciamento> Logs_Licenciamento { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<PedidoFaturamento>()
                        .Property(p => p.valor_faturado)
                        .HasPrecision(9, 2);

            base.OnModelCreating(modelBuilder); 
        }
    }
}

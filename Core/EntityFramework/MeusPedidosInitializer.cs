﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Database.DataConnection;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Infrastructure;
using Core.Migrations;
using System.Security.Cryptography;

namespace Core
{
    public class MeusPedidosInitializer : CreateDatabaseIfNotExists<MeusPedidosContexto>
    {
        public MeusPedidosInitializer()
        {
            
        }

        protected override void Seed(MeusPedidosContexto context)
        {
            MD5 md5Hash = MD5.Create();
            // Converter a String para array de bytes, que é como a biblioteca trabalha.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes("Par505560"));
            // Cria-se um StringBuilder para recompôr a string.
            StringBuilder sBuilder = new StringBuilder();
            // Loop para formatar cada byte como uma String em hexadecimal
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("X2"));
            }
            string password = sBuilder.ToString();
            context.Usuarios.Add(
                new Usuario()
                {
                    email = "admin",
                    password = password,
                    birth = Convert.ToDateTime("01/01/2000")

                });

            base.Seed(context);
        }
    }
}

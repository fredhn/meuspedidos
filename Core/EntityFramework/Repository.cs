﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class Repository
    {
        private MeusPedidosContexto session = new MeusPedidosContexto();

        private object locker = new object();

        public virtual DbContext context
        {
            get { return session; }
        }

        public IList<T> GetAll<T>() where T : class
        {
            return session.Set<T>().ToList();
        }

        public IList<T> GetAll<T>(Expression<Func<T, bool>> predicate) where T : class
        {
            return session.Set<T>().Where(predicate).ToList();
        }

        public IEnumerable<T> GetBySpcification<T>(Func<T, bool> predicate) where T : class, new()
        {
            lock (locker)
            {
                return session.Set<T>().Where(predicate);
            }
        }

        public T GetFirstBySpcification<T>(Func<T, bool> predicate) where T : class, new()
        {
            lock (locker)
            {
                //return session.Set<T>().FirstOrDefault(predicate);
                return session.Set<T>().SingleOrDefault(predicate);
            }
        }

        public DataSet GetDataSet(string query)
        {
            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter(query, session.Database.Connection as SqlConnection);

                var ds = new DataSet();

                adapter.Fill(ds);

                adapter.Dispose();

                return ds;
            }
            catch
            {
                return null;
            }
        }

        public void ExecuteCommand(string query, params object[] parameters)
        {
            session.Database.ExecuteSqlCommand(query, parameters);
        }

        public T GetFirstBySQLCommand<T>(string query, params object[] parameters) where T : class, new()
        {
            return session.Database.SqlQuery<T>(query, parameters).FirstOrDefault();
        }

        public bool Add<T>(T entity) where T : class
        {
            if (!IsValid(entity))
                return false;
            try
            {
                session.Set(typeof(T)).Add(entity);

                session.SaveChanges();

                return session.Entry(entity).GetValidationResult().IsValid;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var even in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation erros:", even.Entry.Entity.GetType().Name, even.Entry.State);
                    foreach (var ve in even.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\" ", ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            //catch (Exception ex)
            //{
            //    if (ex.InnerException != null)
            //        throw new Exception(ex.InnerException.Message, ex);
            //    throw new Exception(ex.Message, ex);
            //}
        }

        public bool Update<T>(T entity) where T : class
        {
            //session = new MeusPedidosContexto();

            if (!IsValid(entity))
                return false;
            try
            {
                //session = new MeusPedidosContexto();

                session.Set(typeof(T)).Attach(entity);

                session.Entry(entity).State = EntityState.Modified;

                session.SaveChanges();

                return session.Entry(entity).GetValidationResult().IsValid;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entidade do tipo \"{0}\" no estado \"{1}\" tem os seguintes erros de validação:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Erro: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }

        public bool UpdatePedidos(Pedido entity)
        {
            if (!IsValid(entity))
                return false;
            try
            {
                session.Entry(entity).State = entity.id == 0 ? EntityState.Added : EntityState.Modified;

                foreach (var extra in entity.extras)
                    context.Entry(extra).State = extra.id == 0 ? EntityState.Added : EntityState.Modified;

                foreach (var item in entity.items)
                    context.Entry(item).State = item.id == 0 ? EntityState.Added : EntityState.Modified;

                session.SaveChanges();

                return session.Entry(entity).GetValidationResult().IsValid;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public bool Delete<T>(T entity) where T : class
        {
            if (!IsValid(entity))
                return false;
            try
            {
                //session.Set(typeof(T)).Remove(entity);

                session.Entry(entity).State = EntityState.Deleted;

                session.SaveChanges();

                return session.Entry(entity).GetValidationResult().IsValid;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    throw new Exception(ex.InnerException.Message, ex);

                throw new Exception(ex.Message, ex);
            }
        }

        public virtual bool IsValid<T>(T value) where T : class
        {
            if (value == null)
                throw new ArgumentNullException("A Entidade não Pode ser Nula.");
            return true;
        }

        //CLIENTE RELATED DATA
        public IList<T> GetAllFullCliente<T>() where T : class
        {
            session.Clientes.Include(s => s.contatos).ToList();
            session.Clientes.Include(s => s.emails).ToList();
            session.Clientes.Include(s => s.telefones).ToList();
            return session.Set<T>().ToList();
        }

        public IList<T> GetAllFullClientes_BySpecification<T>(Func<T, bool> predicate) where T : class
        {
            session.Clientes.Include(s => s.contatos).ToList();
            session.Clientes.Include(s => s.emails).ToList();
            session.Clientes.Include(s => s.telefones).ToList();
            return session.Set<T>().Where(predicate).ToList();
        }

        public IEnumerable<T> GetFullClienteBySpecification<T>(Func<T, bool> predicate) where T : class, new()
        {
            lock (locker)
            {
                session.Clientes.Include(s => s.contatos).ToList();
                session.Clientes.Include(s => s.emails).ToList();
                session.Clientes.Include(s => s.telefones).ToList();
                return session.Set<T>().Where(predicate);
            }
        }

        //PRODUTO RELATED DATA
        public IList<T> GetAllFullProduto<T>() where T : class
        {
            session.Produtos.Include(s => s.grade_cores).ToList();
            session.Produtos.Include(s => s.grade_tamanhos).ToList();
            return session.Set<T>().ToList();
        }

        //TRANSPORTADORA RELATED DATA
        public IEnumerable<T> GetFullTransportadoraBySpecification<T>(Func<T, bool> predicate) where T : class, new()
        {
            lock (locker)
            {
                session.Transportadoras.Include(s => s.telefones).ToList();
                return session.Set<T>().Where(predicate);
            }
        }

        public IList<T> GetAllFullTransportadora<T>() where T : class
        {
            session.Transportadoras.Include(s => s.telefones).ToList();
            return session.Set<T>().ToList();
        }

        public IList<T> GetAllFullTransportadora_BySpecification<T>(Func<T, bool> predicate) where T : class
        {
            session.Transportadoras.Include(s => s.telefones).ToList();
            return session.Set<T>().Where(predicate).ToList();
        }

        //PEDIDO RELATED DATA

        public IEnumerable<T> GetFullPedidoBySpecification<T>(Func<T, bool> predicate) where T : class, new()
        {
            lock (locker)
            {
                session.Pedidos.Include(s => s.items).ToList();
                session.ItemsPedido.Include(session => session.quantidade_grades).ToList();
                session.Pedidos.Include(s => s.extras).ToList();
                return session.Set<T>().Where(predicate);
            }
        }

        public Pedido GetFullPedidoByMaxId<T>() where T : class, new()
        {
            lock (locker)
            {
                session.Pedidos.Include(s => s.items).ToList();
                session.ItemsPedido.Include(session => session.quantidade_grades).ToList();
                session.Pedidos.Include(s => s.extras).ToList();
                Pedido p = session.Pedidos.OrderByDescending(s => s.id == session.Pedidos.Where(q => q.id != 0).Max(r => r.id)).FirstOrDefault();
                return p;
            }
        }

        public IList<T> GetAllFullPedidos<T>() where T : class
        {
            session.Pedidos.Include(s => s.items).ToList();
            session.ItemsPedido.Include(s => s.quantidade_grades).ToList();
            session.Pedidos.Include(s => s.extras).ToList();
            return session.Set<T>().ToList();
        }

        public IList<T> GetAllFullPedidos_BySpecification<T>(Func<T, bool> predicate) where T : class
        {
            session.Pedidos.Include(s => s.items).ToList();
            session.ItemsPedido.Include(s => s.quantidade_grades).ToList();
            session.Pedidos.Include(s => s.extras).ToList();
            return session.Set<T>().Where(predicate).ToList();
        }

        //CAMPO EXTRA RELATED DATA
        public PedidoCampoExtra GetCampoExtraByMaxId<T>() where T : class, new()
        {
            lock (locker)
            {
                return session.PedidosCampoExtra.OrderByDescending(s => s.id == session.PedidosCampoExtra.Where(q => q.id != 0).Max(r => r.id)).FirstOrDefault();
            }
        }

        //LOG LICENCIAMENTO RELATED DATA
        public Log_Licenciamento GetLogLicenciamentoByMaxDate<T>() where T : class, new()
        {
            lock (locker)
            {
                Log_Licenciamento p = session.Logs_Licenciamento.OrderByDescending(s => s.created_at == session.Logs_Licenciamento.Where(q => q.id != 0).Max(r => r.created_at)).FirstOrDefault();
                return p;
            }
        }
    }
}

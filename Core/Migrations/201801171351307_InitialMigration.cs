namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BancoDados",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome_conexao = c.String(maxLength: 30),
                        nome_servidor = c.String(maxLength: 100),
                        nome_bancodados = c.String(maxLength: 100),
                        usuario_bancodados = c.String(maxLength: 100),
                        senha_bancodados = c.String(maxLength: 100),
                        autenticacao_segura = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Cliente",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        razao_social = c.String(maxLength: 100),
                        nome_fantasia = c.String(maxLength: 100),
                        tipo = c.String(maxLength: 1),
                        cnpj = c.String(maxLength: 18),
                        inscricao_estadual = c.String(maxLength: 30),
                        suframa = c.String(maxLength: 20),
                        rua = c.String(maxLength: 100),
                        complemento = c.String(maxLength: 50),
                        cep = c.String(maxLength: 9),
                        bairro = c.String(maxLength: 30),
                        cidade = c.String(maxLength: 50),
                        estado = c.String(maxLength: 2),
                        observacao = c.String(maxLength: 500),
                        nome_excecao_fiscal = c.String(maxLength: 20),
                        excluido = c.Boolean(nullable: false),
                        ultima_alteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Contato",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(maxLength: 50),
                        cargo = c.String(maxLength: 30),
                        excluido = c.Boolean(nullable: false),
                        Cliente_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Cliente", t => t.Cliente_id)
                .Index(t => t.Cliente_id);
            
            CreateTable(
                "dbo.Email",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        tipo = c.String(maxLength: 1),
                        email = c.String(maxLength: 75),
                        Contato_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Contato", t => t.Contato_id)
                .Index(t => t.Contato_id);
            
            CreateTable(
                "dbo.Telefone",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        tipo = c.String(maxLength: 1),
                        numero = c.String(maxLength: 30),
                        Contato_id = c.Int(),
                        Transportadora_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Contato", t => t.Contato_id)
                .ForeignKey("dbo.Transportadora", t => t.Transportadora_id)
                .Index(t => t.Contato_id)
                .Index(t => t.Transportadora_id);
            
            CreateTable(
                "dbo.ClienteCategoria",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        cliente_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.ClienteCondicaoPagamento",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        cliente_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.ClienteTabelaPreco",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        cliente_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.CondicaoPagamento",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(maxLength: 100),
                        valor_minimo = c.Double(nullable: false),
                        excluido = c.Boolean(nullable: false),
                        ultima_alteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.ContaMeusPedidos",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        filial = c.String(maxLength: 2),
                        codigo = c.String(maxLength: 2),
                        descricao = c.String(maxLength: 60),
                        url = c.String(maxLength: 200),
                        apptoken = c.String(maxLength: 150),
                        cmptoken = c.String(maxLength: 150),
                        status = c.String(maxLength: 1),
                        excluido = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.FormaPagamento",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(maxLength: 100),
                        excluido = c.Boolean(nullable: false),
                        ultima_alteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.ICMS_ST",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        codigo_ncm = c.String(maxLength: 20),
                        nome_excecao_fiscal = c.String(maxLength: 20),
                        estado_destino = c.String(maxLength: 2),
                        tipo_st = c.String(),
                        valor_mva = c.Double(nullable: false),
                        valor_pmc = c.Double(nullable: false),
                        icms_credito = c.Double(nullable: false),
                        icms_destino = c.Double(nullable: false),
                        excluido = c.Boolean(nullable: false),
                        ultima_alteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.ItemPedido",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        produto_id = c.Int(nullable: false),
                        produto_codigo = c.String(maxLength: 50),
                        produto_nome = c.String(maxLength: 100),
                        tabela_preco_id = c.Int(nullable: false),
                        quantidade = c.Double(nullable: false),
                        preco_bruto = c.Double(nullable: false),
                        preco_liquido = c.Double(nullable: false),
                        cotacao_moeda = c.Double(nullable: false),
                        observacoes = c.String(maxLength: 500),
                        excluido = c.Boolean(nullable: false),
                        ipi = c.Double(nullable: false),
                        tipo_ipi = c.String(maxLength: 1),
                        st = c.Double(nullable: false),
                        subtotal = c.Double(nullable: false),
                        Pedido_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Pedido", t => t.Pedido_id)
                .Index(t => t.Pedido_id);
            
            CreateTable(
                "dbo.QuantidadeGrades",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        cor = c.String(maxLength: 15),
                        tamanho = c.String(maxLength: 15),
                        quantidade = c.Double(nullable: false),
                        ItemPedido_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.ItemPedido", t => t.ItemPedido_id)
                .Index(t => t.ItemPedido_id);
            
            CreateTable(
                "dbo.Pedido",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        cliente_id = c.Int(nullable: false),
                        cliente_razao_social = c.String(maxLength: 100),
                        cliente_nome_fantasia = c.String(maxLength: 100),
                        cliente_cnpj = c.String(maxLength: 14),
                        cliente_inscricao_estadual = c.String(maxLength: 30),
                        cliente_rua = c.String(maxLength: 100),
                        cliente_numero = c.Int(nullable: false),
                        cliente_complemento = c.String(maxLength: 50),
                        cliente_cep = c.String(maxLength: 8),
                        cliente_bairro = c.String(maxLength: 30),
                        cliente_cidade = c.String(maxLength: 50),
                        cliente_estado = c.String(maxLength: 2),
                        cliente_suframa = c.String(maxLength: 20),
                        representada_id = c.Int(nullable: false),
                        representada_nome_fantasia = c.String(maxLength: 50),
                        representada_razao_social = c.String(maxLength: 50),
                        transportadora_id = c.Int(nullable: false),
                        criador_id = c.Int(nullable: false),
                        nome_contato = c.String(maxLength: 50),
                        status = c.String(maxLength: 1),
                        numero = c.Int(nullable: false),
                        total = c.Double(nullable: false),
                        condicao_pagamento = c.String(maxLength: 50),
                        condicao_pagamento_id = c.Int(nullable: false),
                        forma_pagamento_id = c.Int(nullable: false),
                        data_emissao = c.DateTime(nullable: false),
                        observacoes = c.String(maxLength: 500),
                        status_faturamento = c.String(maxLength: 1),
                        ultima_alteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.PedidoCampoExtra",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(maxLength: 50),
                        valor = c.String(maxLength: 500),
                        Pedido_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Pedido", t => t.Pedido_id)
                .Index(t => t.Pedido_id);
            
            CreateTable(
                "dbo.PedidoFaturamento",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        pedido_id = c.Int(nullable: false),
                        valor_faturado = c.Decimal(nullable: false, precision: 9, scale: 2),
                        data_faturamento = c.DateTime(nullable: false),
                        numero_nf = c.String(maxLength: 500),
                        informacoes_adicionais = c.String(maxLength: 500),
                        excluido = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.ProdutoCategoria",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(maxLength: 100),
                        categoria_pai_id = c.Int(nullable: false),
                        ultima_alteracao = c.DateTime(nullable: false),
                        excluido = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Produto",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        codigo = c.String(maxLength: 50),
                        nome = c.String(maxLength: 100),
                        comissao = c.Double(nullable: false),
                        preco_tabela = c.Double(nullable: false),
                        preco_minimo = c.Double(nullable: false),
                        ipi = c.Double(nullable: false),
                        tipo_ipi = c.String(maxLength: 1),
                        st = c.Double(nullable: false),
                        grade_cores_SerializedValue = c.String(),
                        grade_tamanhos_SerializedValue = c.String(),
                        moeda = c.String(maxLength: 1),
                        unidade = c.String(maxLength: 10),
                        saldo_estoque = c.Double(nullable: false),
                        observacoes = c.String(maxLength: 500),
                        ultima_alteracao = c.DateTime(nullable: false),
                        excluido = c.Boolean(nullable: false),
                        ativo = c.Boolean(nullable: false),
                        categoria_id = c.Int(nullable: false),
                        codigo_ncm = c.String(),
                        multiplo = c.Double(nullable: false),
                        peso_bruto = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.ProdutoTabelaPreco",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        preco = c.Single(nullable: false),
                        tabela_id = c.Int(nullable: false),
                        produto_id = c.Int(nullable: false),
                        excluido = c.Boolean(nullable: false),
                        ultima_alteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.RegraLiberacao",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        cliente_id = c.Int(nullable: false),
                        usuario_id = c.Int(nullable: false),
                        liberado = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.TabelaPreco",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(maxLength: 100),
                        tipo = c.String(maxLength: 2),
                        acrescimo = c.Single(nullable: false),
                        desconto = c.Single(nullable: false),
                        excluido = c.Boolean(nullable: false),
                        ultima_alteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.TituloVencido",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        numero_documento = c.String(maxLength: 18),
                        valor = c.Double(nullable: false),
                        data_vencimento = c.DateTime(nullable: false),
                        observacao = c.String(maxLength: 500),
                        cliente_id = c.Int(nullable: false),
                        excluido = c.Boolean(nullable: false),
                        ultima_alteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Transportadora",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(maxLength: 100),
                        cidade = c.String(maxLength: 50),
                        estado = c.String(maxLength: 2),
                        informacoes_adicionais = c.String(maxLength: 500),
                        excluido = c.Boolean(nullable: false),
                        ultima_alteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Telefone", "Transportadora_id", "dbo.Transportadora");
            DropForeignKey("dbo.ItemPedido", "Pedido_id", "dbo.Pedido");
            DropForeignKey("dbo.PedidoCampoExtra", "Pedido_id", "dbo.Pedido");
            DropForeignKey("dbo.QuantidadeGrades", "ItemPedido_id", "dbo.ItemPedido");
            DropForeignKey("dbo.Contato", "Cliente_id", "dbo.Cliente");
            DropForeignKey("dbo.Telefone", "Contato_id", "dbo.Contato");
            DropForeignKey("dbo.Email", "Contato_id", "dbo.Contato");
            DropIndex("dbo.PedidoCampoExtra", new[] { "Pedido_id" });
            DropIndex("dbo.QuantidadeGrades", new[] { "ItemPedido_id" });
            DropIndex("dbo.ItemPedido", new[] { "Pedido_id" });
            DropIndex("dbo.Telefone", new[] { "Transportadora_id" });
            DropIndex("dbo.Telefone", new[] { "Contato_id" });
            DropIndex("dbo.Email", new[] { "Contato_id" });
            DropIndex("dbo.Contato", new[] { "Cliente_id" });
            DropTable("dbo.Transportadora");
            DropTable("dbo.TituloVencido");
            DropTable("dbo.TabelaPreco");
            DropTable("dbo.RegraLiberacao");
            DropTable("dbo.ProdutoTabelaPreco");
            DropTable("dbo.Produto");
            DropTable("dbo.ProdutoCategoria");
            DropTable("dbo.PedidoFaturamento");
            DropTable("dbo.PedidoCampoExtra");
            DropTable("dbo.Pedido");
            DropTable("dbo.QuantidadeGrades");
            DropTable("dbo.ItemPedido");
            DropTable("dbo.ICMS_ST");
            DropTable("dbo.FormaPagamento");
            DropTable("dbo.ContaMeusPedidos");
            DropTable("dbo.CondicaoPagamento");
            DropTable("dbo.ClienteTabelaPreco");
            DropTable("dbo.ClienteCondicaoPagamento");
            DropTable("dbo.ClienteCategoria");
            DropTable("dbo.Telefone");
            DropTable("dbo.Email");
            DropTable("dbo.Contato");
            DropTable("dbo.Cliente");
            DropTable("dbo.BancoDados");
        }
    }
}

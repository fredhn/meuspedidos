namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ESaidaEntradaAlterDtIntMaxLenghFrom8To25 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.PedidoCampoExtra", new[] { "Pedido_id" });
            AlterColumn("dbo.Entidade_Entrada", "dtinte", c => c.String(maxLength: 25));
            AlterColumn("dbo.Entidade_Saida", "dtinte", c => c.String(maxLength: 25));
            AlterColumn("dbo.PedidoCampoExtra", "Pedido_id", c => c.Int());
            AlterColumn("dbo.PedidoCampoExtra", "pedido_id", c => c.Int(nullable: false));
            CreateIndex("dbo.PedidoCampoExtra", "Pedido_id");
        }
        
        public override void Down()
        {
            DropIndex("dbo.PedidoCampoExtra", new[] { "Pedido_id" });
            AlterColumn("dbo.PedidoCampoExtra", "pedido_id", c => c.Int());
            AlterColumn("dbo.PedidoCampoExtra", "Pedido_id", c => c.Int(nullable: false));
            AlterColumn("dbo.Entidade_Saida", "dtinte", c => c.String(maxLength: 8));
            AlterColumn("dbo.Entidade_Entrada", "dtinte", c => c.String(maxLength: 8));
            CreateIndex("dbo.PedidoCampoExtra", "Pedido_id");
        }
    }
}

namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddColumnsTelefonesAndEmailsToCliente : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Email", "Cliente_id", c => c.Int());
            AddColumn("dbo.Telefone", "Cliente_id", c => c.Int());
            CreateIndex("dbo.Email", "Cliente_id");
            CreateIndex("dbo.Telefone", "Cliente_id");
            AddForeignKey("dbo.Email", "Cliente_id", "dbo.Cliente", "id");
            AddForeignKey("dbo.Telefone", "Cliente_id", "dbo.Cliente", "id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Telefone", "Cliente_id", "dbo.Cliente");
            DropForeignKey("dbo.Email", "Cliente_id", "dbo.Cliente");
            DropIndex("dbo.Telefone", new[] { "Cliente_id" });
            DropIndex("dbo.Email", new[] { "Cliente_id" });
            DropColumn("dbo.Telefone", "Cliente_id");
            DropColumn("dbo.Email", "Cliente_id");
        }
    }
}

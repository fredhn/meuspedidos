namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TablePedidoCampoExtraAlterProps : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PedidoCampoExtra", "valor", c => c.String(maxLength: 500));
            AlterColumn("dbo.PedidoCampoExtra", "tipo", c => c.String(maxLength: 1));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PedidoCampoExtra", "tipo", c => c.String(maxLength: 500));
            DropColumn("dbo.PedidoCampoExtra", "valor");
        }
    }
}

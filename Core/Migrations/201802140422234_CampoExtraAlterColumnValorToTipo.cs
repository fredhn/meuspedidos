namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CampoExtraAlterColumnValorToTipo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PedidoCampoExtra", "tipo", c => c.String(maxLength: 500));
            DropColumn("dbo.PedidoCampoExtra", "valor");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PedidoCampoExtra", "valor", c => c.String(maxLength: 500));
            DropColumn("dbo.PedidoCampoExtra", "tipo");
        }
    }
}

namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEntityUsuario : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Usuario",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        cpf = c.String(),
                        type = c.String(),
                        cnpj = c.String(),
                        email = c.String(),
                        sms = c.Byte(nullable: false),
                        phone = c.String(),
                        gender = c.String(),
                        birth = c.DateTime(nullable: false),
                        password = c.String(),
                        comments = c.String(),
                        last_name = c.String(),
                        cellphone = c.String(),
                        permission = c.String(),
                        first_name = c.String(),
                        in_trash = c.Byte(nullable: false),
                        entity_groups_id = c.Int(nullable: false),
                        newsletters = c.Byte(nullable: false),
                        shipment_addresses_id = c.Int(),
                        allowed_discount = c.Single(nullable: false),
                        created_at = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Usuario");
        }
    }
}

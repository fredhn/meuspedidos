namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterClienteTabPreco_TabsLiberadasAndMPTran : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.PedidoCampoExtra", new[] { "Pedido_id" });
            AddColumn("dbo.ClienteTabelaPreco", "tabelas_liberadas", c => c.String());
            AddColumn("dbo.ClienteTabelaPreco", "tabelas_liberadas_desc", c => c.String());
            AddColumn("dbo.ClienteTabelaPreco", "mptran", c => c.String(maxLength: 1));
            AlterColumn("dbo.PedidoCampoExtra", "Pedido_id", c => c.Int());
            AlterColumn("dbo.PedidoCampoExtra", "pedido_id", c => c.Int(nullable: false));
            CreateIndex("dbo.PedidoCampoExtra", "Pedido_id");
        }
        
        public override void Down()
        {
            DropIndex("dbo.PedidoCampoExtra", new[] { "Pedido_id" });
            AlterColumn("dbo.PedidoCampoExtra", "pedido_id", c => c.Int());
            AlterColumn("dbo.PedidoCampoExtra", "Pedido_id", c => c.Int(nullable: false));
            DropColumn("dbo.ClienteTabelaPreco", "mptran");
            DropColumn("dbo.ClienteTabelaPreco", "tabelas_liberadas_desc");
            DropColumn("dbo.ClienteTabelaPreco", "tabelas_liberadas");
            CreateIndex("dbo.PedidoCampoExtra", "Pedido_id");
        }
    }
}

namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Adjusts01 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.PedidoCampoExtra", new[] { "Pedido_id" });
            AlterColumn("dbo.Pedido", "condicao_pagamento", c => c.String(maxLength: 100));
            AlterColumn("dbo.PedidoCampoExtra", "Pedido_id", c => c.Int());
            AlterColumn("dbo.PedidoCampoExtra", "pedido_id", c => c.Int(nullable: false));
            CreateIndex("dbo.PedidoCampoExtra", "Pedido_id");
        }
        
        public override void Down()
        {
            DropIndex("dbo.PedidoCampoExtra", new[] { "Pedido_id" });
            AlterColumn("dbo.PedidoCampoExtra", "pedido_id", c => c.Int());
            AlterColumn("dbo.PedidoCampoExtra", "Pedido_id", c => c.Int(nullable: false));
            AlterColumn("dbo.Pedido", "condicao_pagamento", c => c.String(maxLength: 50));
            CreateIndex("dbo.PedidoCampoExtra", "Pedido_id");
        }
    }
}

namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterClienteCondPagamentoCondicoesLiberadasAndMPTran : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.PedidoCampoExtra", new[] { "Pedido_id" });
            AddColumn("dbo.ClienteCondicaoPagamento", "condicoes_pagamento_liberadas", c => c.String());
            AddColumn("dbo.ClienteCondicaoPagamento", "mptran", c => c.String(maxLength: 1));
            AlterColumn("dbo.PedidoCampoExtra", "Pedido_id", c => c.Int());
            AlterColumn("dbo.PedidoCampoExtra", "pedido_id", c => c.Int(nullable: false));
            CreateIndex("dbo.PedidoCampoExtra", "Pedido_id");
        }
        
        public override void Down()
        {
            DropIndex("dbo.PedidoCampoExtra", new[] { "Pedido_id" });
            AlterColumn("dbo.PedidoCampoExtra", "pedido_id", c => c.Int());
            AlterColumn("dbo.PedidoCampoExtra", "Pedido_id", c => c.Int(nullable: false));
            DropColumn("dbo.ClienteCondicaoPagamento", "mptran");
            DropColumn("dbo.ClienteCondicaoPagamento", "condicoes_pagamento_liberadas");
            CreateIndex("dbo.PedidoCampoExtra", "Pedido_id");
        }
    }
}

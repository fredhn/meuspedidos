namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate2 : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.Cliente_Map");
            DropTable("dbo.CondicaoPagamento_Map");
            DropTable("dbo.FormaPagamento_Map");
            DropTable("dbo.ICMSST_Map");
            DropTable("dbo.Pedido_Map");
            DropTable("dbo.PedidoCampoExtra_Map");
            DropTable("dbo.Produto_Map");
            DropTable("dbo.ProdutoCategoria_Map");
            DropTable("dbo.ProdutoTabelaPreco_Map");
            DropTable("dbo.TabelaPreco_Map");
            DropTable("dbo.TituloVencido_Map");
            DropTable("dbo.Transportadora_Map");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Transportadora_Map",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_erp = c.Int(nullable: false),
                        id_ent = c.Int(nullable: false),
                        id_meuspedidos = c.Int(nullable: false),
                        ultima_alteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.TituloVencido_Map",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_erp = c.Int(nullable: false),
                        id_ent = c.Int(nullable: false),
                        id_meuspedidos = c.Int(nullable: false),
                        ultima_alteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.TabelaPreco_Map",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_erp = c.Int(nullable: false),
                        id_ent = c.Int(nullable: false),
                        id_meuspedidos = c.Int(nullable: false),
                        ultima_alteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.ProdutoTabelaPreco_Map",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_erp = c.Int(nullable: false),
                        id_ent = c.Int(nullable: false),
                        id_meuspedidos = c.Int(nullable: false),
                        ultima_alteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.ProdutoCategoria_Map",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_erp = c.Int(nullable: false),
                        id_ent = c.Int(nullable: false),
                        id_meuspedidos = c.Int(nullable: false),
                        ultima_alteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Produto_Map",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_erp = c.Int(nullable: false),
                        id_ent = c.Int(nullable: false),
                        id_meuspedidos = c.Int(nullable: false),
                        ultima_alteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.PedidoCampoExtra_Map",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_erp = c.Int(nullable: false),
                        id_ent = c.Int(nullable: false),
                        id_meuspedidos = c.Int(nullable: false),
                        ultima_alteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Pedido_Map",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_erp = c.Int(nullable: false),
                        id_ent = c.Int(nullable: false),
                        id_meuspedidos = c.Int(nullable: false),
                        ultima_alteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.ICMSST_Map",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_erp = c.Int(nullable: false),
                        id_ent = c.Int(nullable: false),
                        id_meuspedidos = c.Int(nullable: false),
                        ultima_alteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.FormaPagamento_Map",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_erp = c.Int(nullable: false),
                        id_ent = c.Int(nullable: false),
                        id_meuspedidos = c.Int(nullable: false),
                        ultima_alteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.CondicaoPagamento_Map",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_erp = c.Int(nullable: false),
                        id_ent = c.Int(nullable: false),
                        id_meuspedidos = c.Int(nullable: false),
                        ultima_alteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Cliente_Map",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_erp = c.Int(nullable: false),
                        id_ent = c.Int(nullable: false),
                        id_meuspedidos = c.Int(nullable: false),
                        ultima_alteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
        }
    }
}

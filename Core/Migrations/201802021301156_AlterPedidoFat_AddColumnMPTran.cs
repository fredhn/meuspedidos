namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterPedidoFat_AddColumnMPTran : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.PedidoCampoExtra", new[] { "Pedido_id" });
            AddColumn("dbo.PedidoFaturamento", "mptran", c => c.String(maxLength: 1));
            AlterColumn("dbo.PedidoCampoExtra", "Pedido_id", c => c.Int());
            AlterColumn("dbo.PedidoCampoExtra", "pedido_id", c => c.Int(nullable: false));
            CreateIndex("dbo.PedidoCampoExtra", "Pedido_id");
        }
        
        public override void Down()
        {
            DropIndex("dbo.PedidoCampoExtra", new[] { "Pedido_id" });
            AlterColumn("dbo.PedidoCampoExtra", "pedido_id", c => c.Int());
            AlterColumn("dbo.PedidoCampoExtra", "Pedido_id", c => c.Int(nullable: false));
            DropColumn("dbo.PedidoFaturamento", "mptran");
            CreateIndex("dbo.PedidoCampoExtra", "Pedido_id");
        }
    }
}

// <auto-generated />
namespace Core.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class AlterProdutoGradesTamCorToStringsList : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AlterProdutoGradesTamCorToStringsList));
        
        string IMigrationMetadata.Id
        {
            get { return "201801302233111_AlterProdutoGradesTamCorToStringsList"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterProdutoGradesTamCorToStringsList : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.PedidoCampoExtra", new[] { "Pedido_id" });
            AlterColumn("dbo.PedidoCampoExtra", "Pedido_id", c => c.Int());
            AlterColumn("dbo.PedidoCampoExtra", "pedido_id", c => c.Int(nullable: false));
            CreateIndex("dbo.PedidoCampoExtra", "Pedido_id");
            DropColumn("dbo.Produto", "grade_cores_SerializedValue");
            DropColumn("dbo.Produto", "grade_tamanhos_SerializedValue");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Produto", "grade_tamanhos_SerializedValue", c => c.String());
            AddColumn("dbo.Produto", "grade_cores_SerializedValue", c => c.String());
            DropIndex("dbo.PedidoCampoExtra", new[] { "Pedido_id" });
            AlterColumn("dbo.PedidoCampoExtra", "pedido_id", c => c.Int());
            AlterColumn("dbo.PedidoCampoExtra", "Pedido_id", c => c.Int(nullable: false));
            CreateIndex("dbo.PedidoCampoExtra", "Pedido_id");
        }
    }
}

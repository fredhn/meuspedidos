namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CampoExtraDelColumnPedidoId : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.PedidoCampoExtra", new[] { "Pedido_id" });
            AlterColumn("dbo.PedidoCampoExtra", "Pedido_id", c => c.Int());
            CreateIndex("dbo.PedidoCampoExtra", "Pedido_id");
        }
        
        public override void Down()
        {
            DropIndex("dbo.PedidoCampoExtra", new[] { "Pedido_id" });
            AlterColumn("dbo.PedidoCampoExtra", "Pedido_id", c => c.Int(nullable: false));
            CreateIndex("dbo.PedidoCampoExtra", "Pedido_id");
        }
    }
}

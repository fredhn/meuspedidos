namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTablesEnt_EntradaSaida : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Entidade_Entrada",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_contamp = c.Int(nullable: false),
                        filial = c.String(maxLength: 2),
                        tpreg = c.String(maxLength: 20),
                        id_ent = c.String(maxLength: 40),
                        id_mp = c.String(maxLength: 40),
                        status = c.String(maxLength: 1),
                        codret = c.String(maxLength: 3),
                        dtinte = c.String(maxLength: 8),
                        mptran = c.String(maxLength: 1),
                        json = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Entidade_Saida",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_contamp = c.Int(nullable: false),
                        filial = c.String(maxLength: 2),
                        tpreg = c.String(maxLength: 20),
                        id_ent = c.String(maxLength: 40),
                        id_mp = c.String(maxLength: 40),
                        status = c.String(maxLength: 1),
                        codret = c.String(maxLength: 3),
                        dtinte = c.String(maxLength: 8),
                        mptran = c.String(maxLength: 1),
                        json = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Entidade_Saida");
            DropTable("dbo.Entidade_Entrada");
        }
    }
}

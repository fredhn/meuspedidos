namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TableItemPedidoAddColumnsValorMinTipoMoeda : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ItemPedido", "preco_minimo", c => c.Double(nullable: false));
            AddColumn("dbo.ItemPedido", "moeda", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ItemPedido", "moeda");
            DropColumn("dbo.ItemPedido", "preco_minimo");
        }
    }
}

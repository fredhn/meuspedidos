namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CampoExtraAddColumnsExcluidoAndUltAlteracao : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.PedidoCampoExtra", new[] { "Pedido_id" });
            AddColumn("dbo.PedidoCampoExtra", "excluido", c => c.Boolean(nullable: false));
            AddColumn("dbo.PedidoCampoExtra", "ultima_alteracao", c => c.DateTime(nullable: false));
            AlterColumn("dbo.PedidoCampoExtra", "Pedido_id", c => c.Int());
            AlterColumn("dbo.PedidoCampoExtra", "pedido_id", c => c.Int(nullable: false));
            CreateIndex("dbo.PedidoCampoExtra", "Pedido_id");
        }
        
        public override void Down()
        {
            DropIndex("dbo.PedidoCampoExtra", new[] { "Pedido_id" });
            AlterColumn("dbo.PedidoCampoExtra", "pedido_id", c => c.Int());
            AlterColumn("dbo.PedidoCampoExtra", "Pedido_id", c => c.Int(nullable: false));
            DropColumn("dbo.PedidoCampoExtra", "ultima_alteracao");
            DropColumn("dbo.PedidoCampoExtra", "excluido");
            CreateIndex("dbo.PedidoCampoExtra", "Pedido_id");
        }
    }
}

namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEntitiesMappingTablesAndOthers : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.PedidoCampoExtra", new[] { "Pedido_id" });
            CreateTable(
                "dbo.Cliente_Map",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_erp = c.Int(nullable: false),
                        id_ent = c.Int(nullable: false),
                        id_meuspedidos = c.Int(nullable: false),
                        ultima_alteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.CondicaoPagamento_Map",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_erp = c.Int(nullable: false),
                        id_ent = c.Int(nullable: false),
                        id_meuspedidos = c.Int(nullable: false),
                        ultima_alteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.FormaPagamento_Map",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_erp = c.Int(nullable: false),
                        id_ent = c.Int(nullable: false),
                        id_meuspedidos = c.Int(nullable: false),
                        ultima_alteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.ICMSST_Map",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_erp = c.Int(nullable: false),
                        id_ent = c.Int(nullable: false),
                        id_meuspedidos = c.Int(nullable: false),
                        ultima_alteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Pedido_Map",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_erp = c.Int(nullable: false),
                        id_ent = c.Int(nullable: false),
                        id_meuspedidos = c.Int(nullable: false),
                        ultima_alteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.PedidoCampoExtra_Map",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_erp = c.Int(nullable: false),
                        id_ent = c.Int(nullable: false),
                        id_meuspedidos = c.Int(nullable: false),
                        ultima_alteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Produto_Map",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_erp = c.Int(nullable: false),
                        id_ent = c.Int(nullable: false),
                        id_meuspedidos = c.Int(nullable: false),
                        ultima_alteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.ProdutoCategoria_Map",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_erp = c.Int(nullable: false),
                        id_ent = c.Int(nullable: false),
                        id_meuspedidos = c.Int(nullable: false),
                        ultima_alteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.ProdutoTabelaPreco_Map",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_erp = c.Int(nullable: false),
                        id_ent = c.Int(nullable: false),
                        id_meuspedidos = c.Int(nullable: false),
                        ultima_alteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.TabelaPreco_Map",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_erp = c.Int(nullable: false),
                        id_ent = c.Int(nullable: false),
                        id_meuspedidos = c.Int(nullable: false),
                        ultima_alteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.TituloVencido_Map",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_erp = c.Int(nullable: false),
                        id_ent = c.Int(nullable: false),
                        id_meuspedidos = c.Int(nullable: false),
                        ultima_alteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Transportadora_Map",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_erp = c.Int(nullable: false),
                        id_ent = c.Int(nullable: false),
                        id_meuspedidos = c.Int(nullable: false),
                        ultima_alteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            AddColumn("dbo.Cliente", "mptran", c => c.String(maxLength: 1));
            AddColumn("dbo.CondicaoPagamento", "mptran", c => c.String(maxLength: 1));
            AddColumn("dbo.FormaPagamento", "mptran", c => c.String(maxLength: 1));
            AddColumn("dbo.ICMS_ST", "mptran", c => c.String(maxLength: 1));
            AddColumn("dbo.Pedido", "mptran", c => c.String(maxLength: 1));
            AddColumn("dbo.ProdutoCategoria", "mptran", c => c.String(maxLength: 1));
            AddColumn("dbo.Produto", "mptran", c => c.String(maxLength: 1));
            AddColumn("dbo.ProdutoTabelaPreco", "mptran", c => c.String(maxLength: 1));
            AddColumn("dbo.TabelaPreco", "mptran", c => c.String(maxLength: 1));
            AddColumn("dbo.TituloVencido", "mptran", c => c.String(maxLength: 1));
            AddColumn("dbo.Transportadora", "mptran", c => c.String(maxLength: 1));
            AlterColumn("dbo.PedidoCampoExtra", "pedido_id", c => c.Int(nullable: false));
            CreateIndex("dbo.PedidoCampoExtra", "Pedido_id");
        }
        
        public override void Down()
        {
            DropIndex("dbo.PedidoCampoExtra", new[] { "Pedido_id" });
            AlterColumn("dbo.PedidoCampoExtra", "pedido_id", c => c.Int());
            DropColumn("dbo.Transportadora", "mptran");
            DropColumn("dbo.TituloVencido", "mptran");
            DropColumn("dbo.TabelaPreco", "mptran");
            DropColumn("dbo.ProdutoTabelaPreco", "mptran");
            DropColumn("dbo.Produto", "mptran");
            DropColumn("dbo.ProdutoCategoria", "mptran");
            DropColumn("dbo.Pedido", "mptran");
            DropColumn("dbo.ICMS_ST", "mptran");
            DropColumn("dbo.FormaPagamento", "mptran");
            DropColumn("dbo.CondicaoPagamento", "mptran");
            DropColumn("dbo.Cliente", "mptran");
            DropTable("dbo.Transportadora_Map");
            DropTable("dbo.TituloVencido_Map");
            DropTable("dbo.TabelaPreco_Map");
            DropTable("dbo.ProdutoTabelaPreco_Map");
            DropTable("dbo.ProdutoCategoria_Map");
            DropTable("dbo.Produto_Map");
            DropTable("dbo.PedidoCampoExtra_Map");
            DropTable("dbo.Pedido_Map");
            DropTable("dbo.ICMSST_Map");
            DropTable("dbo.FormaPagamento_Map");
            DropTable("dbo.CondicaoPagamento_Map");
            DropTable("dbo.Cliente_Map");
            CreateIndex("dbo.PedidoCampoExtra", "Pedido_id");
        }
    }
}

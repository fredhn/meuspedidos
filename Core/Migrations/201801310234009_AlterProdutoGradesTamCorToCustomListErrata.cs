namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterProdutoGradesTamCorToCustomListErrata : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.PedidoCampoExtra", new[] { "Pedido_id" });
            AddColumn("dbo.GradeTamanho", "tamanho", c => c.String(maxLength: 50));
            AlterColumn("dbo.PedidoCampoExtra", "Pedido_id", c => c.Int());
            AlterColumn("dbo.PedidoCampoExtra", "pedido_id", c => c.Int(nullable: false));
            CreateIndex("dbo.PedidoCampoExtra", "Pedido_id");
            DropColumn("dbo.GradeTamanho", "cor");
        }
        
        public override void Down()
        {
            AddColumn("dbo.GradeTamanho", "cor", c => c.String(maxLength: 50));
            DropIndex("dbo.PedidoCampoExtra", new[] { "Pedido_id" });
            AlterColumn("dbo.PedidoCampoExtra", "pedido_id", c => c.Int());
            AlterColumn("dbo.PedidoCampoExtra", "Pedido_id", c => c.Int(nullable: false));
            DropColumn("dbo.GradeTamanho", "tamanho");
            CreateIndex("dbo.PedidoCampoExtra", "Pedido_id");
        }
    }
}

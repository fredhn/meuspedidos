namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterProdutoGradesTamCorToList : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.PedidoCampoExtra", new[] { "Pedido_id" });
            CreateTable(
                "dbo.GradeCor",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        cor = c.String(maxLength: 50),
                        Produto_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Produto", t => t.Produto_id)
                .Index(t => t.Produto_id);
            
            CreateTable(
                "dbo.GradeTamanho",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        cor = c.String(maxLength: 50),
                        Produto_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Produto", t => t.Produto_id)
                .Index(t => t.Produto_id);
            
            AlterColumn("dbo.PedidoCampoExtra", "Pedido_id", c => c.Int());
            AlterColumn("dbo.PedidoCampoExtra", "pedido_id", c => c.Int(nullable: false));
            CreateIndex("dbo.PedidoCampoExtra", "Pedido_id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.GradeTamanho", "Produto_id", "dbo.Produto");
            DropForeignKey("dbo.GradeCor", "Produto_id", "dbo.Produto");
            DropIndex("dbo.PedidoCampoExtra", new[] { "Pedido_id" });
            DropIndex("dbo.GradeTamanho", new[] { "Produto_id" });
            DropIndex("dbo.GradeCor", new[] { "Produto_id" });
            AlterColumn("dbo.PedidoCampoExtra", "pedido_id", c => c.Int());
            AlterColumn("dbo.PedidoCampoExtra", "Pedido_id", c => c.Int(nullable: false));
            DropTable("dbo.GradeTamanho");
            DropTable("dbo.GradeCor");
            CreateIndex("dbo.PedidoCampoExtra", "Pedido_id");
        }
    }
}

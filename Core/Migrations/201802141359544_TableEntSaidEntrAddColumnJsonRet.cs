namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TableEntSaidEntrAddColumnJsonRet : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Entidade_Entrada", "jsonret", c => c.String());
            AddColumn("dbo.Entidade_Saida", "jsonret", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Entidade_Saida", "jsonret");
            DropColumn("dbo.Entidade_Entrada", "jsonret");
        }
    }
}

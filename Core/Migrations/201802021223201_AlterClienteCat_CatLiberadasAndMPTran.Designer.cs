// <auto-generated />
namespace Core.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class AlterClienteCat_CatLiberadasAndMPTran : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AlterClienteCat_CatLiberadasAndMPTran));
        
        string IMigrationMetadata.Id
        {
            get { return "201802021223201_AlterClienteCat_CatLiberadasAndMPTran"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

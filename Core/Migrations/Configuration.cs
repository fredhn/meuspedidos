namespace Core.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    internal sealed class Configuration : DbMigrationsConfiguration<Core.MeusPedidosContexto>
    {

        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Core.MeusPedidosContexto context)
        {
            MD5 md5Hash = MD5.Create();
            // Converter a String para array de bytes, que � como a biblioteca trabalha.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes("Par505560"));
            // Cria-se um StringBuilder para recomp�r a string.
            StringBuilder sBuilder = new StringBuilder();
            // Loop para formatar cada byte como uma String em hexadecimal
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("X2"));
            }
            string password = sBuilder.ToString();
            context.Usuarios.AddOrUpdate(u => u.email,
                new Usuario ()
                {
                    email = "admin",
                    password = password,
                    birth = Convert.ToDateTime("01/01/2000")

                });
            context.SaveChanges();
        }
    }
}

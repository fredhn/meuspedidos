namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TableItemPedidoAlterColumnDescontos : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ItemPedido", "descontos", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ItemPedido", "descontos");
        }
    }
}

namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTableVendedores : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.PedidoCampoExtra", new[] { "Pedido_id" });
            CreateTable(
                "dbo.Vendedor",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nome = c.String(maxLength: 100),
                        email = c.String(maxLength: 75),
                        telefone = c.String(maxLength: 75),
                        administrador = c.Boolean(nullable: false),
                        excluido = c.Boolean(nullable: false),
                        ultima_alteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            AlterColumn("dbo.PedidoCampoExtra", "Pedido_id", c => c.Int());
            AlterColumn("dbo.PedidoCampoExtra", "pedido_id", c => c.Int(nullable: false));
            CreateIndex("dbo.PedidoCampoExtra", "Pedido_id");
        }
        
        public override void Down()
        {
            DropIndex("dbo.PedidoCampoExtra", new[] { "Pedido_id" });
            AlterColumn("dbo.PedidoCampoExtra", "pedido_id", c => c.Int());
            AlterColumn("dbo.PedidoCampoExtra", "Pedido_id", c => c.Int(nullable: false));
            DropTable("dbo.Vendedor");
            CreateIndex("dbo.PedidoCampoExtra", "Pedido_id");
        }
    }
}

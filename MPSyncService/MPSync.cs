﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Control;
using System.Threading;
using System.IO;

namespace MPSyncService
{
    public partial class MPSync : ServiceBase
    {
        private Timer syncTimer;
        static object timerLock = new object();
        public MPSync()
        {
            InitializeComponent();
        }
        
        protected override void OnStart(string[] args)
        {
            syncTimer = new Timer(new TimerCallback(ExecuteService), null, 0, 240000);
            Log_Service.WriteErrorLog("Serviço de Integração Meus Pedidos: Iniciado");
        }

        protected override void OnStop()
        {
            Log_Service.WriteErrorLog("Serviço de Integração Meus Pedidos: Parado");
        }

        #region Methods

        public void debugService ()
        {
            SyncController syncControl = new SyncController();
            syncControl.Execute();
            Log_Service.WriteErrorLog("Serviço de Integração Meus Pedidos: Executado");
        }

        public void ExecuteService(object stateInfo)
        {
            int tid = Thread.CurrentThread.ManagedThreadId;

            if (Monitor.TryEnter(timerLock))
            {
                Log_Service.WriteErrorLog("(Sys) Tarefa iniciada: " + tid.ToString());
                try
                {
                    try
                    {
                        FileInfo fi = new FileInfo(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "conn.xml"));
                        Log_Service.WriteErrorLog("\n\nCaminho do conn.xml: " + fi.ToString());
                        string abc = AppDomain.CurrentDomain.BaseDirectory;
                        Log_Service.WriteErrorLog("\nDiretorio do executável do servico: " + abc);

                        SyncController_.Execute();
                    }
                    catch (Exception ex)
                    {
                        Log_Service.WriteErrorLog("Error on: {0} " + ex.Message + ex.StackTrace);
                    }
                }
                finally
                {
                    Monitor.Exit(timerLock);
                }
                Log_Service.WriteErrorLog("(Sys) Tarefa finalizada: " + tid.ToString());
            }
            /*
            try
            {
                FileInfo fi = new FileInfo(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "conn.xml"));
                string conexistis = fi.Exists.ToString();
                Log_Service.WriteErrorLog("\n\nCaminho: " + fi.ToString());
                Log_Service.WriteErrorLog("\n\nArquivo conn.xml existe = " + conexistis);
                string abc = AppDomain.CurrentDomain.BaseDirectory;
                Log_Service.WriteErrorLog("\nDiretorio do executável do servico: " + abc);

                SyncController_.Execute();
            }
            catch (Exception ex)
            {
                Log_Service.WriteErrorLog("Error on: {0} " + ex.Message + ex.StackTrace);
            }
            */
        }
        #endregion
    }
}

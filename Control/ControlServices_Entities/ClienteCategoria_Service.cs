﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control
{
    public class ClienteCategoria_Service
    {
        Repository myRep = new Repository();

        public ClienteCategoria GetBySpecification_ClienteCategoria(int codigo)
        {
            try
            {
                return myRep.GetBySpcification<ClienteCategoria>(c => c.id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public ClienteCategoria GetByClienteID_ClienteCategoria(int codigo)
        {
            try
            {
                return myRep.GetBySpcification<ClienteCategoria>(c => c.cliente_id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public List<ClienteCategoria> GetAll_ClientesCategorias()
        {
            try
            {
                return myRep.GetAll<ClienteCategoria>().ToList();
            }
            catch { throw; }
        }

        public List<ClienteCategoria> GetAllByMPTran_ClienteCategoria(string mptran)
        {
            try
            {
                return myRep.GetAll<ClienteCategoria>(c => c.mptran == mptran).ToList();
            }
            catch { throw; }
        }

        public bool AddNew_ClienteCategoria(ClienteCategoria cliCat)
        {
            try
            {
                return myRep.Add<ClienteCategoria>(cliCat);
            }
            catch { throw; }
        }

        public bool Update_ClienteCategoria(ClienteCategoria cliCat)
        {
            try
            {
                return myRep.Update<ClienteCategoria>(cliCat);
            }
            catch { throw; }
        }

        public bool Delete_ClienteCategoria(ClienteCategoria cliCat)
        {
            try
            {
                return myRep.Delete<ClienteCategoria>(cliCat);
            }
            catch { throw; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control
{
    public class PedidoCampoExtra_Service
    {
        Repository myRep = new Repository();

        public PedidoCampoExtra GetBySpecification_PedidoCampoExtra(int codigo)
        {
            try
            {
                return myRep.GetBySpcification<PedidoCampoExtra>(c => c.id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public PedidoCampoExtra GetByNome_PedidoCampoExtra(string nome)
        {
            try
            {
                return myRep.GetBySpcification<PedidoCampoExtra>(c => c.nome == nome).First();
            }
            catch { throw; }
        }

        public PedidoCampoExtra GetByMaxId_PedidoCampoExtra()
        {
            try
            {
                return myRep.GetCampoExtraByMaxId<PedidoCampoExtra>();
            }
            catch { throw; }
        }

        public List<PedidoCampoExtra> GetAll_PedidoCampoExtra()
        {
            try
            {
                return myRep.GetAll<PedidoCampoExtra>().ToList();
            }
            catch { throw; }
        }

        public List<PedidoCampoExtra> GetAllByMPTran_PedidoCampoExtra(string mptran)
        {
            try
            {
                return myRep.GetAll<PedidoCampoExtra>(c => c.mptran == mptran).ToList();
            }
            catch { throw; }
        }

        public bool AddNew_PedidoCampoExtra(PedidoCampoExtra prod)
        {
            try
            {
                return myRep.Add<PedidoCampoExtra>(prod);
            }
            catch { throw; }
        }

        public bool Update_PedidoCampoExtra(PedidoCampoExtra prod)
        {
            try
            {
                return myRep.Update<PedidoCampoExtra>(prod);
            }
            catch { throw; }
        }

        public bool Delete_PedidoCampoExtra(PedidoCampoExtra prod)
        {
            try
            {
                return myRep.Delete<PedidoCampoExtra>(prod);
            }
            catch { throw; }
        }
    }
}

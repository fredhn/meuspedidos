﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control
{
    public class ItemPedido_Service
    {
        Repository myRep = new Repository();

        public ItemPedido GetBySpecification_ItemPedido(int codigo)
        {
            try
            {
                return myRep.GetBySpcification<ItemPedido>(c => c.id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public List<ItemPedido> GetAll_ItemPedido()
        {
            try
            {
                return myRep.GetAll<ItemPedido>().ToList();
            }
            catch { throw; }
        }

        public bool AddNew_ItemPedido(ItemPedido prod)
        {
            try
            {
                return myRep.Add<ItemPedido>(prod);
            }
            catch { throw; }
        }

        public bool Update_ItemPedido(ItemPedido prod)
        {
            try
            {
                return myRep.Update<ItemPedido>(prod);
            }
            catch { throw; }
        }

        public bool Delete_ItemPedido(ItemPedido prod)
        {
            try
            {
                return myRep.Delete<ItemPedido>(prod);
            }
            catch { throw; }
        }
    }
}

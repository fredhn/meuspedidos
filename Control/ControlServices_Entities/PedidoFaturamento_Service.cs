﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control
{
    public class PedidoFaturamento_Service
    {
        Repository myRep = new Repository();

        public PedidoFaturamento GetBySpecification_PedidoFaturamento(int codigo)
        {
            try
            {
                return myRep.GetBySpcification<PedidoFaturamento>(c => c.id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public PedidoFaturamento GetByPedidoId_PedidoFaturamento(int codigo)
        {
            try
            {
                return myRep.GetBySpcification<PedidoFaturamento>(c => c.pedido_id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public List<PedidoFaturamento> GetAll_PedidoFaturamento()
        {
            try
            {
                return myRep.GetAll<PedidoFaturamento>().ToList();
            }
            catch { throw; }
        }

        public List<PedidoFaturamento> GetAllByMPTran_PedidoFaturamento(string mptran)
        {
            try
            {
                return myRep.GetAll<PedidoFaturamento>(c => c.mptran == mptran).ToList();
            }
            catch { throw; }
        }

        public bool AddNew_PedidoFaturamento(PedidoFaturamento prod)
        {
            try
            {
                return myRep.Add<PedidoFaturamento>(prod);
            }
            catch { throw; }
        }

        public bool Update_PedidoFaturamento(PedidoFaturamento prod)
        {
            try
            {
                return myRep.Update<PedidoFaturamento>(prod);
            }
            catch { throw; }
        }

        public bool Delete_PedidoFaturamento(PedidoFaturamento prod)
        {
            try
            {
                return myRep.Delete<PedidoFaturamento>(prod);
            }
            catch { throw; }
        }
    }
}

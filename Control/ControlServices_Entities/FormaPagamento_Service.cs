﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control
{
    public class FormaPagamento_Service
    {
        Repository myRep = new Repository();

        public FormaPagamento GetBySpecification_FormaPagamento(int codigo)
        {
            try
            {
                return myRep.GetBySpcification<FormaPagamento>(c => c.id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public FormaPagamento GetByNome_FormaPagamento(string codigo)
        {
            try
            {
                return myRep.GetBySpcification<FormaPagamento>(c => c.nome == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public List<FormaPagamento> GetAll_FormaPagamento()
        {
            try
            {
                return myRep.GetAll<FormaPagamento>().ToList();
            }
            catch { throw; }
        }

        public List<FormaPagamento> GetAllByMPTran_FormaPagamento(string mptran)
        {
            try
            {
                return myRep.GetAll<FormaPagamento>(c => c.mptran == mptran).ToList();
            }
            catch { throw; }
        }

        public bool AddNew_FormaPagamento(FormaPagamento prod)
        {
            try
            {
                return myRep.Add<FormaPagamento>(prod);
            }
            catch { throw; }
        }

        public bool Update_FormaPagamento(FormaPagamento prod)
        {
            try
            {
                return myRep.Update<FormaPagamento>(prod);
            }
            catch { throw; }
        }

        public bool Delete_FormaPagamento(FormaPagamento prod)
        {
            try
            {
                return myRep.Delete<FormaPagamento>(prod);
            }
            catch { throw; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control
{
    public class RegraLiberacao_Service
    {
        Repository myRep = new Repository();

        public RegraLiberacao GetBySpecification_RegraLiberacao(int codigo)
        {
            try
            {
                return myRep.GetBySpcification<RegraLiberacao>(c => c.id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public RegraLiberacao GetByUserCli_RegraLiberacao(int user_id, int cli_id)
        {
            try
            {
                return myRep.GetBySpcification<RegraLiberacao>(c => c.usuario_id == user_id && c.cliente_id == cli_id).SingleOrDefault();
            }
            catch { throw; }
        }

        public List<RegraLiberacao> GetAll_RegraLiberacao()
        {
            try
            {
                return myRep.GetAll<RegraLiberacao>().ToList();
            }
            catch { throw; }
        }

        public List<RegraLiberacao> GetAllByMPTran_RegraLiberacao(string mptran)
        {
            try
            {
                return myRep.GetAll<RegraLiberacao>(c => c.mptran == mptran).ToList();
            }
            catch { throw; }
        }

        public bool AddNew_RegraLiberacao(RegraLiberacao prod)
        {
            try
            {
                return myRep.Add<RegraLiberacao>(prod);
            }
            catch { throw; }
        }

        public bool Update_RegraLiberacao(RegraLiberacao prod)
        {
            try
            {
                return myRep.Update<RegraLiberacao>(prod);
            }
            catch { throw; }
        }

        public bool Delete_RegraLiberacao(RegraLiberacao prod)
        {
            try
            {
                return myRep.Delete<RegraLiberacao>(prod);
            }
            catch { throw; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control
{
    public class ProdutoCategoria_Service
    {
        Repository myRep = new Repository();

        public ProdutoCategoria GetBySpecification_ProdutoCategoria(int codigo)
        {
            try
            {
                return myRep.GetBySpcification<ProdutoCategoria>(c => c.id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public ProdutoCategoria GetByNome_ProdutoCategoria(string codigo)
        {
            try
            {
                return myRep.GetBySpcification<ProdutoCategoria>(c => c.nome == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public List<ProdutoCategoria> GetAll_ProdutoCategorias()
        {
            try
            {
                return myRep.GetAll<ProdutoCategoria>().ToList();
            }
            catch { throw; }
        }

        public List<ProdutoCategoria> GetAllByMPTran_ProdutoCategorias(string mptran)
        {
            try
            {
                return myRep.GetAll<ProdutoCategoria>(c => c.mptran == mptran).ToList();
            }
            catch { throw; }
        }

        public bool AddNew_ProdutoCategoria(ProdutoCategoria prod)
        {
            try
            {
                return myRep.Add<ProdutoCategoria>(prod);
            }
            catch { throw; }
        }

        public bool Update_ProdutoCategoria(ProdutoCategoria prod)
        {
            try
            {
                return myRep.Update<ProdutoCategoria>(prod);
            }
            catch { throw; }
        }

        public bool Delete_ProdutoCategoria(ProdutoCategoria prod)
        {
            try
            {
                return myRep.Delete<ProdutoCategoria>(prod);
            }
            catch { throw; }
        }
    }
}

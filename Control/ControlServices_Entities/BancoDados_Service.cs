﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control
{
    public class BancoDados_Service
    {
        Repository myRep = new Repository();

        public BancoDados GetByID_BancoDados(int codigo)
        {
            try
            {
                return myRep.GetBySpcification<BancoDados>(c => c.id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public BancoDados GetByConnectionName_BancoDados(string codigo)
        {
            try
            {
                return myRep.GetBySpcification<BancoDados>(c => c.nome_conexao == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public List<BancoDados> GetAll_BancoDados()
        {
            try
            {
                return myRep.GetAll<BancoDados>().ToList();
            }
            catch { throw; }
        }

        public bool AddNew_BancoDados(BancoDados prod)
        {
            try
            {
                return myRep.Add<BancoDados>(prod);
            }
            catch { throw; }
        }

        public bool Update_BancoDados(BancoDados prod)
        {
            try
            {
                return myRep.Update<BancoDados>(prod);
            }
            catch { throw; }
        }

        public bool Delete_BancoDados(BancoDados prod)
        {
            try
            {
                return myRep.Delete<BancoDados>(prod);
            }
            catch { throw; }
        }
    }
}

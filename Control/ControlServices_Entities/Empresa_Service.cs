﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control
{
    public class Empresa_Service
    {
        Repository myRep = new Repository();

        public Empresa GetBySpecification_Empresa(int codigo)
        {
            try
            {
                return myRep.GetBySpcification<Empresa>(c => c.id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }
        

        public Empresa GetByCnpj_Empresa(string cnpj)
        {
            try
            {
                return myRep.GetFullTransportadoraBySpecification<Empresa>(c => c.cnpj == cnpj).SingleOrDefault();
            }
            catch { throw; }
        }

        public Empresa GetByLogin_Empresa(string login)
        {
            try
            {
                return myRep.GetFullTransportadoraBySpecification<Empresa>(c => c.login == login).SingleOrDefault();
            }
            catch { throw; }
        }

        public List<Empresa> GetAll_Empresas()
        {
            try
            {
                return myRep.GetAll<Empresa>().ToList();
            }
            catch { throw; }
        }

        public bool AddNew_Empresa(Empresa user)
        {
            try
            {
                return myRep.Add<Empresa>(user);
            }
            catch { throw; }
        }

        public bool Update_Empresa(Empresa user)
        {
            try
            {
                return myRep.Update<Empresa>(user);
            }
            catch { throw; }
        }

        public bool Delete_Usuario(Empresa user)
        {
            try
            {
                return myRep.Delete<Empresa>(user);
            }
            catch { throw; }
        }
    }
}

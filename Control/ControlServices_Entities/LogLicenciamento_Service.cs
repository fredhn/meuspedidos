﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control
{
    public class LogLicenciamento_Service
    {
        Repository myRep = new Repository();

        public Log_Licenciamento GetBySpecification_LogLicenciamento(int codigo)
        {
            try
            {
                return myRep.GetBySpcification<Log_Licenciamento>(c => c.id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public Log_Licenciamento GetByMaxDate_LogLicenciamento(DateTime date)
        {
            try
            {
                return myRep.GetLogLicenciamentoByMaxDate<Log_Licenciamento>();
            }
            catch { throw; }
        }

        public List<Log_Licenciamento> GetAll_LogLicenciamento()
        {
            try
            {
                return myRep.GetAll<Log_Licenciamento>().ToList();
            }
            catch { throw; }
        }

        public bool AddNew_LogLicenciamento(Log_Licenciamento prod)
        {
            try
            {
                return myRep.Add<Log_Licenciamento>(prod);
            }
            catch { throw; }
        }

        public bool Update_LogLicenciamento(Log_Licenciamento prod)
        {
            try
            {
                return myRep.Update<Log_Licenciamento>(prod);
            }
            catch { throw; }
        }

        public bool Delete_LogLicenciamento(Log_Licenciamento prod)
        {
            try
            {
                return myRep.Delete<Log_Licenciamento>(prod);
            }
            catch { throw; }
        }
    }
}

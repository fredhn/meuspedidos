﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control
{
    public class ClienteTabelaPreco_Service
    {
        Repository myRep = new Repository();

        public ClienteTabelaPreco GetBySpecification_ClienteTabelaPreco(int codigo)
        {
            try
            {
                return myRep.GetBySpcification<ClienteTabelaPreco>(c => c.id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public ClienteTabelaPreco GetByClienteID_ClienteTabelaPreco(int codigo)
        {
            try
            {
                return myRep.GetBySpcification<ClienteTabelaPreco>(c => c.cliente_id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public List<ClienteTabelaPreco> GetAll_ClienteTabelaPreco()
        {
            try
            {
                return myRep.GetAll<ClienteTabelaPreco>().ToList();
            }
            catch { throw; }
        }

        public List<ClienteTabelaPreco> GetAllByMPTran_ClienteTabelaPreco(string mptran)
        {
            try
            {
                return myRep.GetAll<ClienteTabelaPreco>(c => c.mptran == mptran).ToList();
            }
            catch { throw; }
        }

        public bool AddNew_ClienteTabelaPreco(ClienteTabelaPreco ctp)
        {
            try
            {
                return myRep.Add<ClienteTabelaPreco>(ctp);
            }
            catch { throw; }
        }

        public bool Update_ClienteTabelaPreco(ClienteTabelaPreco ctp)
        {
            try
            {
                return myRep.Update<ClienteTabelaPreco>(ctp);
            }
            catch { throw; }
        }

        public bool Delete_ClienteTabelaPreco(ClienteTabelaPreco ctp)
        {
            try
            {
                return myRep.Delete<ClienteTabelaPreco>(ctp);
            }
            catch { throw; }
        }
    }
}

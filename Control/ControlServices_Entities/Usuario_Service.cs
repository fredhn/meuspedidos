﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control
{
    public class Usuario_Service
    {
        Repository myRep = new Repository();

        public Usuario GetBySpecification_Usuario(int codigo)
        {
            try
            {
                return myRep.GetBySpcification<Usuario>(c => c.id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }
        

        public Usuario GetByEmail_Usuario(string email)
        {
            try
            {
                return myRep.GetFullTransportadoraBySpecification<Usuario>(c => c.email == email).SingleOrDefault();
            }
            catch { throw; }
        }

        public List<Usuario> GetAll_Usuarios()
        {
            try
            {
                return myRep.GetAll<Usuario>().ToList();
            }
            catch { throw; }
        }

        public bool AddNew_Usuario(Usuario user)
        {
            try
            {
                return myRep.Add<Usuario>(user);
            }
            catch { throw; }
        }

        public bool Update_Usuario(Usuario user)
        {
            try
            {
                return myRep.Update<Usuario>(user);
            }
            catch { throw; }
        }

        public bool Delete_Usuario(Usuario user)
        {
            try
            {
                return myRep.Delete<Usuario>(user);
            }
            catch { throw; }
        }
    }
}

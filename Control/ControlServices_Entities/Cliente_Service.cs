﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using System.Data.Entity.Validation;
using System.Data.Entity.Core;

namespace Control
{
    public class Cliente_Service
    {
        Repository myRep = new Repository();

        public Cliente GetBySpecification_Cliente(int codigo)
        {
            try
            {
                //
                return myRep.GetBySpcification<Cliente>(c => c.id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public Cliente GetBySpecification2_Cliente(int codigo)
        {
            try
            {
                //
                return myRep.GetFullClienteBySpecification<Cliente>(c => c.id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public Cliente GetByCNPJ_Cliente(string cnpj)
        {
            try
            {
                return myRep.GetBySpcification<Cliente>(c => c.cnpj == cnpj).SingleOrDefault();
            }
            catch { throw; }
        }

        public Cliente GetByMPTran_FullCliente(string mptran)
        {
            try
            {
                return myRep.GetAllFullClientes_BySpecification<Cliente>(c => c.mptran == mptran).SingleOrDefault();
            }
            catch { throw; }
        }

        public List<Cliente> GetAll_Clientes()
        {
            try
            {
                return myRep.GetAll<Cliente>().ToList();
            }
            catch { throw; }
        }
        public List<Cliente> GetAll_FullClientes()
        {
            try
            {
                return myRep.GetAllFullCliente<Cliente>().ToList();
            }
            catch { throw; }
        }

        public List<Cliente> GetAllByEditionDateAfter_FullClientes(DateTime dt)
        {
            try
            {
                return myRep.GetAllFullClientes_BySpecification<Cliente>(c => c.ultima_alteracao > dt).ToList();
            }
            catch { throw; }
        }

        public List<Cliente> GetAllByMPTran_FullClientes(string mptran)
        {
            try
            {
                return myRep.GetAllFullClientes_BySpecification<Cliente>(c => c.mptran == mptran).ToList();
            }
            catch { throw; }
        }

        public bool AddNew_Cliente(Cliente cli)
        {
            try
            {
                return myRep.Add<Cliente>(cli);
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            //catch { throw; }
        }

        public bool Update_Cliente(Cliente cli)
        {
            try
            {
                return myRep.Update<Cliente>(cli);
            }
            catch (Exception e)
            {
                Console.WriteLine("{0} Second exception caught.", e);
                throw;
            }
        }

        public bool Delete_Cliente(Cliente cli)
        {
            try
            {
                return myRep.Delete<Cliente>(cli);
            }
            catch { throw; }
        }
    }
}

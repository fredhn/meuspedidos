﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control
{
    public class TabelaPreco_Service
    {
        Repository myRep = new Repository();

        public TabelaPreco GetBySpecification_TabelaPreco(int codigo)
        {
            try
            {
                return myRep.GetBySpcification<TabelaPreco>(c => c.id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public TabelaPreco GetByNome_TabelaPreco(string nome)
        {
            try
            {
                return myRep.GetBySpcification<TabelaPreco>(c => c.nome == nome).SingleOrDefault();
            }
            catch { throw; }
        }

        public List<TabelaPreco> GetAll_TabelaPreco()
        {
            try
            {
                return myRep.GetAll<TabelaPreco>().ToList();
            }
            catch { throw; }
        }
        public List<TabelaPreco> GetAllByMPTran_TabelaPreco(string mptran)
        {
            try
            {
                return myRep.GetAll<TabelaPreco>(c => c.mptran == mptran).ToList();
            }
            catch { throw; }
        }

        public bool AddNew_TabelaPreco(TabelaPreco prod)
        {
            try
            {
                return myRep.Add<TabelaPreco>(prod);
            }
            catch { throw; }
        }

        public bool Update_TabelaPreco(TabelaPreco prod)
        {
            try
            {
                return myRep.Update<TabelaPreco>(prod);
            }
            catch { throw; }
        }

        public bool Delete_TabelaPreco(TabelaPreco prod)
        {
            try
            {
                return myRep.Delete<TabelaPreco>(prod);
            }
            catch { throw; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control
{
    public class ContaMeusPedidos_Service
    {
        Repository myRep = new Repository();

        public ContaMeusPedidos GetByID_ContaMeusPedidos(int codigo)
        {
            try
            {
                return myRep.GetBySpcification<ContaMeusPedidos>(c => c.id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public ContaMeusPedidos GetByCodigo_ContaMeusPedidos(string codigo)
        {
            try
            {
                return myRep.GetBySpcification<ContaMeusPedidos>(c => c.codigo == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public ContaMeusPedidos GetByAppToken_ContaMeusPedidos(string appToken)
        {
            try
            {
                return myRep.GetBySpcification<ContaMeusPedidos>(c => c.apptoken == appToken).SingleOrDefault();
            }
            catch { throw; }
        }

        public List<ContaMeusPedidos> GetAll_ContasMeusPedidos()
        {
            try
            {
                return myRep.GetAll<ContaMeusPedidos>().ToList();
            }
            catch { throw; }
        }

        public List<ContaMeusPedidos> GetAll_ContasMeusPedidosAtivas()
        {
            try
            {
                return myRep.GetAll<ContaMeusPedidos>(c => c.excluido == false).ToList();
            }
            catch { throw; }
        }

        public bool AddNew_ContaMeusPedidos(ContaMeusPedidos prod)
        {
            try
            {
                return myRep.Add<ContaMeusPedidos>(prod);
            }
            catch { throw; }
        }

        public bool Update_ContaMeusPedidos(ContaMeusPedidos prod)
        {
            try
            {
                return myRep.Update<ContaMeusPedidos>(prod);
            }
            catch { throw; }
        }

        public bool Delete_ContaMeusPedidos(ContaMeusPedidos prod)
        {
            try
            {
                return myRep.Delete<ContaMeusPedidos>(prod);
            }
            catch { throw; }
        }
    }
}

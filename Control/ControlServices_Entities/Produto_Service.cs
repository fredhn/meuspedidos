﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control
{
    public class Produto_Service
    {
        Repository myRep = new Repository();

        public Produto GetBySpecification_Produto(int codigo)
        {
            try
            {
                return myRep.GetBySpcification<Produto>(c => c.id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public Produto GetByNome_Produto(string nome)
        {
            try
            {
                return myRep.GetBySpcification<Produto>(c => c.nome == nome).SingleOrDefault();
            }
            catch { throw; }
        }

        public List<Produto> GetAll_Produtos()
        {
            try
            {
                return myRep.GetAll<Produto>().ToList();
            }
            catch { throw; }
        }

        public List<Produto> GetAll_FullProdutos()
        {
            try
            {
                return myRep.GetAllFullProduto<Produto>().ToList();
            }    
            catch { throw; }
        }

        public List<Produto> GetAllByMPTran_Produto(string mptran)
        {
            try
            {
                return myRep.GetAll<Produto>(c => c.mptran == mptran).ToList();
            }
            catch { throw; }
        }

        public bool AddNew_Produto(Produto prod)
        {
            try
            {
                return myRep.Add<Produto>(prod);
            }
            catch { throw; }
        }

        public bool Update_Produto(Produto prod)
        {
            try
            {
                return myRep.Update<Produto>(prod);
            }
            catch { throw; }
        }

        public bool Delete_Produto(Produto prod)
        {
            try
            {
                return myRep.Delete<Produto>(prod);
            }
            catch { throw; }
        }
    }
}

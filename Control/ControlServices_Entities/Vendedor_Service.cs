﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control
{
    public class Vendedor_Service
    {
        Repository myRep = new Repository();

        public Vendedor GetBySpecification_Vendedor(int codigo)
        {
            try
            {
                return myRep.GetBySpcification<Vendedor>(c => c.id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }
        

        public Vendedor GetByNome_Vendedor(string nome)
        {
            try
            {
                return myRep.GetFullTransportadoraBySpecification<Vendedor>(c => c.nome == nome).SingleOrDefault();
            }
            catch { throw; }
        }

        public List<Vendedor> GetAll_Vendedores()
        {
            try
            {
                return myRep.GetAll<Vendedor>().ToList();
            }
            catch { throw; }
        }

        public bool AddNew_Vendedor(Vendedor prod)
        {
            try
            {
                return myRep.Add<Vendedor>(prod);
            }
            catch { throw; }
        }

        public bool Update_Vendedor(Vendedor prod)
        {
            try
            {
                return myRep.Update<Vendedor>(prod);
            }
            catch { throw; }
        }

        public bool Delete_Vendedor(Vendedor prod)
        {
            try
            {
                return myRep.Delete<Vendedor>(prod);
            }
            catch { throw; }
        }
    }
}

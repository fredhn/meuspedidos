﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control
{
    public class Transportadora_Service
    {
        Repository myRep = new Repository();

        public Transportadora GetBySpecification_Transportadora(int codigo)
        {
            try
            {
                return myRep.GetBySpcification<Transportadora>(c => c.id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public Transportadora GetBySpecification2_Transportadora(int codigo)
        {
            try
            {
                return myRep.GetFullTransportadoraBySpecification<Transportadora>(c => c.id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public Transportadora GetByNome_Transportadora(string nome)
        {
            try
            {
                return myRep.GetFullTransportadoraBySpecification<Transportadora>(c => c.nome == nome).SingleOrDefault();
            }
            catch { throw; }
        }

        public List<Transportadora> GetAll_Transportadoras()
        {
            try
            {
                return myRep.GetAll<Transportadora>().ToList();
            }
            catch { throw; }
        }

        public List<Transportadora> GetAll_FullTransportadoras()
        {
            try
            {
                return myRep.GetAllFullTransportadora<Transportadora>().ToList();
            }
            catch { throw; }
        }

        public List<Transportadora> GetAllByMPTran_Transportadora(string mptran)
        {
            try
            {
                return myRep.GetAll<Transportadora>(c => c.mptran == mptran).ToList();
            }
            catch { throw; }
        }

        public List<Transportadora> GetAllByMPTran_FullTransportadora(string mptran)
        {
            try
            {
                return myRep.GetAllFullTransportadora_BySpecification<Transportadora>(c => c.mptran == mptran).ToList();
            }
            catch { throw; }
        }

        public bool AddNew_Transportadora(Transportadora prod)
        {
            try
            {
                return myRep.Add<Transportadora>(prod);
            }
            catch { throw; }
        }

        public bool Update_Transportadora(Transportadora prod)
        {
            try
            {
                return myRep.Update<Transportadora>(prod);
            }
            catch { throw; }
        }

        public bool Delete_Transportadora(Transportadora prod)
        {
            try
            {
                return myRep.Delete<Transportadora>(prod);
            }
            catch { throw; }
        }
    }
}

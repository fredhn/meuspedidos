﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control
{
    public class ProdutoTabelaPreco_Service
    {
        Repository myRep = new Repository();

        public ProdutoTabelaPreco GetBySpecification_ProdutoTabelaPreco(int codigo)
        {
            try
            {
                return myRep.GetBySpcification<ProdutoTabelaPreco>(c => c.id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public ProdutoTabelaPreco GetByIdProdIdTabP_ProdutoTabelaPreco(int id_prod, int id_tabp)
        {
            try
            {
                return myRep.GetBySpcification<ProdutoTabelaPreco>(c => c.tabela_id == id_tabp & c.produto_id == id_prod).SingleOrDefault();
            }
            catch { throw; }
        }

        public List<ProdutoTabelaPreco> GetAll_ProdutoTabelaPreco()
        {
            try
            {
                return myRep.GetAll<ProdutoTabelaPreco>().ToList();
            }
            catch { throw; }
        }

        public List<ProdutoTabelaPreco> GetAllByMPTran_ProdutoTabelaPreco(string mptran)
        {
            try
            {
                return myRep.GetAll<ProdutoTabelaPreco>(c => c.mptran == mptran).ToList();
            }
            catch { throw; }
        }

        public bool AddNew_ProdutoTabelaPreco(ProdutoTabelaPreco prod)
        {
            try
            {
                return myRep.Add<ProdutoTabelaPreco>(prod);
            }
            catch { throw; }
        }

        public bool Update_ProdutoTabelaPreco(ProdutoTabelaPreco prod)
        {
            try
            {
                return myRep.Update<ProdutoTabelaPreco>(prod);
            }
            catch { throw; }
        }

        public bool Delete_ProdutoTabelaPreco(ProdutoTabelaPreco prod)
        {
            try
            {
                return myRep.Delete<ProdutoTabelaPreco>(prod);
            }
            catch { throw; }
        }
    }
}

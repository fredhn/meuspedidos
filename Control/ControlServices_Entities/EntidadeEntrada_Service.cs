﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control
{
    public class EntidadeEntrada_Service
    {
        Repository myRep = new Repository();

        public Entidade_Entrada GetByID_EntidadeEntrada(int codigo)
        {
            try
            {
                return myRep.GetBySpcification<Entidade_Entrada>(c => c.id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public Entidade_Entrada GetVendedorByIDMP_EntidadeEntrada(string id_mp)
        {
            try
            {
                return myRep.GetBySpcification<Entidade_Entrada>(c => c.id_mp == id_mp && c.tpreg == "Vendedor").SingleOrDefault();
            }
            catch { throw; }
        }

        public Entidade_Entrada GetPEdidoByIDMP_EntidadeEntrada(string id_mp)
        {
            try
            {
                return myRep.GetBySpcification<Entidade_Entrada>(c => c.id_mp == id_mp && c.tpreg == "Pedido").SingleOrDefault();
            }
            catch { throw; }
        }

        public Entidade_Entrada GetClienteByIDMP_EntidadeEntrada(string id_mp)
        {
            try
            {
                return myRep.GetBySpcification<Entidade_Entrada>(c => c.id_mp == id_mp && c.tpreg == "Cliente").SingleOrDefault();
            }
            catch { throw; }
        }

        public Entidade_Entrada GetTransportadoraByIDMP_EntidadeEntrada(string id_mp)
        {
            try
            {
                return myRep.GetBySpcification<Entidade_Entrada>(c => c.id_mp == id_mp && c.tpreg == "Transportadora").SingleOrDefault();
            }
            catch { throw; }
        }

        public Entidade_Entrada GetPedidoCampoExtraByIDMP_EntidadeEntrada(string id_mp)
        {
            try
            {
                return myRep.GetBySpcification<Entidade_Entrada>(c => c.id_mp == id_mp && c.tpreg == "PedidoCExtra").SingleOrDefault();
            }
            catch { throw; }
        }

        public Entidade_Entrada GetByDtInt_EntidadeEntrada(string codigo)
        {
            try
            {
                return myRep.GetBySpcification<Entidade_Entrada>(c => c.dtinte == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public Entidade_Entrada GetVendedorByIDENT_EntidadeEntrada(string codigo)
        {
            try
            {
                return myRep.GetBySpcification<Entidade_Entrada>(c => c.id_ent == codigo).Where(p => p.tpreg == "Vendedor").SingleOrDefault();
            }
            catch { throw; }
        }

        public List<Entidade_Entrada> GetAll_EntidadeEntrada()
        {
            try
            {
                return myRep.GetAll<Entidade_Entrada>().ToList();
            }
            catch { throw; }
        }

        public bool AddNew_EntidadeEntrada(Entidade_Entrada prod)
        {
            try
            {
                return myRep.Add<Entidade_Entrada>(prod);
            }
            catch { throw; }
        }

        public bool Update_EntidadeEntrada(Entidade_Entrada prod)
        {
            try
            {
                return myRep.Update<Entidade_Entrada>(prod);
            }
            catch { throw; }
        }

        public bool Delete_EntidadeEntrada(Entidade_Entrada prod)
        {
            try
            {
                return myRep.Delete<Entidade_Entrada>(prod);
            }
            catch { throw; }
        }
    }
}

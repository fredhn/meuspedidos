﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using System.Data.Entity.Validation;

namespace Control
{
    public class EntidadeSaida_Service
    {
        Repository myRep = new Repository();

        public Entidade_Saida GetByID_EntidadeSaida(int codigo)
        {
            try
            {
                return myRep.GetBySpcification<Entidade_Saida>(c => c.id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public Entidade_Saida GetByIDENT_EntidadeSaida(string codigo)
        {
            try
            {
                return myRep.GetBySpcification<Entidade_Saida>(c => c.id_ent == codigo).Where(p => p.mptran == "I").SingleOrDefault();
            }
            catch { throw; }
        }

        public Entidade_Saida GetClienteByIDENT_EntidadeSaida(string codigo)
        {
            try
            {
                return myRep.GetBySpcification<Entidade_Saida>(c => c.id_ent == codigo).Where(p => p.mptran == "I" && p.tpreg == "Cliente" && p.codret == "201").SingleOrDefault();
            }
            catch { throw; }
        }

        public Entidade_Saida GetClienteByIDMP_EntidadeSaida(string codigo)
        {
            try
            {
                return myRep.GetBySpcification<Entidade_Saida>(c => c.id_mp == codigo).Where(p => p.mptran == "I" && p.tpreg == "Cliente" && p.codret == "201").SingleOrDefault();
            }
            catch { throw; }
        }

        public Entidade_Saida GetICMSByIDENT_EntidadeSaida(string codigo)
        {
            try
            {
                return myRep.GetBySpcification<Entidade_Saida>(c => c.id_ent == codigo).Where(p => p.mptran == "I" && p.tpreg == "ICMSST" && p.codret == "201").SingleOrDefault();
            }
            catch { throw; }
        }

        public Entidade_Saida GetClienteCatByIDENT_EntidadeSaida(string codigo)
        {
            try
            {
                return myRep.GetBySpcification<Entidade_Saida>(c => c.id_ent == codigo).Where(p => p.mptran == "I" && p.tpreg == "ClienteCat" && p.codret == "200").SingleOrDefault();
            }
            catch { throw; }
        }

        public Entidade_Saida GetProdutoByIDENT_EntidadeSaida(string codigo)
        {
            try
            {
                return myRep.GetBySpcification<Entidade_Saida>(c => c.id_ent == codigo).Where(p => p.mptran == "I" && p.tpreg == "Produto").SingleOrDefault();
            }
            catch { throw; }
        }

        public Entidade_Saida GetProdutoByIDENT2_EntidadeSaida(string codigo)
        {
            try
            {
                return myRep.GetBySpcification<Entidade_Saida>(c => c.id_ent == codigo).Where(p => p.mptran == "I" && p.tpreg == "Produto" && p.id_mp != null).SingleOrDefault();
            }
            catch { throw; }
        }

        public Entidade_Saida GetProdutoTabPrecoByIDENT_EntidadeSaida(string codigo)
        {
            try
            {
                return myRep.GetBySpcification<Entidade_Saida>(c => c.id_ent == codigo).Where(p => p.mptran == "I" && p.tpreg == "ProdTabelaPreco" && p.codret == "200").SingleOrDefault();
            }
            catch { throw; }
        }

        public Entidade_Saida GetProdutoCatByIDENT_EntidadeSaida(string codigo)
        {
            try
            {
                return myRep.GetBySpcification<Entidade_Saida>(c => c.id_ent == codigo).Where(p => p.mptran == "I" && p.tpreg == "CategoriaProduto" && p.codret == "201").SingleOrDefault();
            }
            catch { throw; }
        }

        public Entidade_Saida GetCondicaoPagByIDENT_EntidadeSaida(string codigo)
        {
            try
            {
                return myRep.GetBySpcification<Entidade_Saida>(c => c.id_ent == codigo).Where(p => p.mptran == "I" && p.tpreg == "CondicaoPagamento").SingleOrDefault();
            }
            catch { throw; }
        }

        public Entidade_Saida GetFormaPagByIDENT_EntidadeSaida(string codigo)
        {
            try
            {
                return myRep.GetBySpcification<Entidade_Saida>(c => c.id_ent == codigo).Where(p => p.mptran == "I" && p.tpreg == "FormaPagamento").SingleOrDefault();
            }
            catch { throw; }
        }

        public Entidade_Saida GetPedidoByIDENT_EntidadeSaida(string codigo)
        {
            try
            {
                return myRep.GetBySpcification<Entidade_Saida>(c => c.id_ent == codigo).Where(p => p.mptran == "I" && p.tpreg == "Pedido" && p.codret == "201").SingleOrDefault();
            }
            catch { throw; }
        }

        public Entidade_Saida GetPedidoCampoExtraByIDENT_EntidadeSaida(string codigo) //Erro aqui
        {
            try
            {
                return myRep.GetBySpcification<Entidade_Saida>(c => c.id_ent == codigo).Where(p => p.mptran == "I" && p.tpreg == "PedidoCExtra" && p.status == "I").SingleOrDefault();
                //return myRep.GetFirstBySpcification<Entidade_Saida>(c => c.id_ent == codigo);
            }
            catch { throw; }
        }

        public Entidade_Saida GetPedidoFatByIDENT_EntidadeSaida(string codigo) //Erro aqui
        {
            try
            {
                return myRep.GetBySpcification<Entidade_Saida>(c => c.id_ent == codigo).Where(p => p.mptran == "I" && p.tpreg == "PedidoFat" && p.codret == "201").SingleOrDefault();
                //return myRep.GetFirstBySpcification<Entidade_Saida>(c => c.id_ent == codigo);
            }
            catch { throw; }
        }

        public Entidade_Saida GetTabelaPrecoByIDENT_EntidadeSaida(string codigo)
        {
            try
            {
                return myRep.GetBySpcification<Entidade_Saida>(c => c.id_ent == codigo).Where(p => p.mptran == "I" && p.tpreg == "TabelaPreco").SingleOrDefault();
            }
            catch { throw; }
        }

        public Entidade_Saida GetTituloVencidoByIDENT_EntidadeSaida(string codigo) //Erro aqui
        {
            try
            {
                return myRep.GetBySpcification<Entidade_Saida>(c => c.id_ent == codigo).Where(p => p.mptran == "I" && p.tpreg == "TituloVencido" && p.codret == "201").SingleOrDefault();
                //return myRep.GetFirstBySpcification<Entidade_Saida>(c => c.id_ent == codigo);
            }
            catch { throw; }
        }

        public Entidade_Saida GetTransportadoraByIDENT_EntidadeSaida(string codigo)
        {
            try
            {
                return myRep.GetBySpcification<Entidade_Saida>(c => c.id_ent == codigo).Where(p => p.mptran == "I" && p.tpreg == "Transportadora").SingleOrDefault();
            }
            catch { throw; }
        }

        public Entidade_Saida GetTransportadoraByIDMP_EntidadeSaida(string codigo)
        {
            try
            {
                return myRep.GetBySpcification<Entidade_Saida>(c => c.id_mp == codigo).Where(p => p.mptran == "I" && p.tpreg == "Transportadora").SingleOrDefault();
            }
            catch { throw; }
        }


        public Entidade_Saida GetByDtInt_EntidadeSaida(string codigo)
        {
            try
            {
                return myRep.GetBySpcification<Entidade_Saida>(c => c.dtinte == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public List<Entidade_Saida> GetAll_EntidadeSaida()
        {
            try
            {
                return myRep.GetAll<Entidade_Saida>().ToList();
            }
            catch { throw; }
        }

        public List<Entidade_Saida> GetAllByStatus_EntidadeSaida(string status)
        {
            try
            {
                return myRep.GetAll<Entidade_Saida>(c => c.status == status).ToList();
            }
            catch { throw; }
        }

        public List<Entidade_Saida> GetAllByStatusRessync_EntidadeSaida(string status)
        {
            try
            {
                return myRep.GetAll<Entidade_Saida>(c => c.status == status).Where(c => c.mptran == "I" && c.codret == "201" && c.mptran != null).ToList();
            }
            catch { throw; }
        }

        public bool AddNew_EntidadeSaida(Entidade_Saida prod)
        {
            try
            {
                return myRep.Add<Entidade_Saida>(prod);
            }
            catch { throw; }
        }

        public bool Update_EntidadeSaida(Entidade_Saida prod)
        {
            try
            {
                return myRep.Update<Entidade_Saida>(prod);
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }

        public bool Delete_EntidadeSaida(Entidade_Saida prod)
        {
            try
            {
                return myRep.Delete<Entidade_Saida>(prod);
            }
            catch { throw; }
        }
    }
}

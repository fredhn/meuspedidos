﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using System.Data.Entity.Validation;

namespace Control
{
    public class Pedido_Service
    {
        Repository myRep = new Repository();

        public Pedido GetBySpecification_Pedido(int codigo)
        {
            try
            {
                return myRep.GetFullPedidoBySpecification<Pedido>(c => c.id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public Pedido GetByMaxId_Pedido()
        {
            try
            {
                return myRep.GetFullPedidoByMaxId<Pedido>();
            }
            catch { throw; }
        }

        public List<Pedido> GetAll_Pedidos()
        {
            try
            {
                return myRep.GetAll<Pedido>().ToList();
            }
            catch { throw; }
        }

        public List<Pedido> GetAll_FullPedidos()
        {
            try
            {
                return myRep.GetAllFullPedidos<Pedido>().ToList();
            }
            catch { throw; }
        }

        public List<Pedido> GetAllByMPTran_Pedido(string mptran)
        {
            try
            {
                return myRep.GetAllFullPedidos_BySpecification<Pedido>(c => c.mptran == mptran).ToList();
            }
            catch { throw; }
        }

        public bool AddNew_Pedido(Pedido prod)
        {
            try
            {
                return myRep.Add<Pedido>(prod);
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entidade do tipo \"{0}\" no estado \"{1}\" tem os seguintes erros de validação:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Erro: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            //catch { throw; }
        }

        public bool Update_Pedido(Pedido prod)
        {
            try
            {
                //return myRep.Update<Pedido>(prod);
                return myRep.UpdatePedidos(prod);
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public bool Delete_Pedido(Pedido prod)
        {
            try
            {
                return myRep.Delete<Pedido>(prod);
            }
            catch { throw; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control
{
    public class Email_Service
    {
        Repository myRep = new Repository();

        public Email GetBySpecification_Email(int codigo)
        {
            try
            {
                return myRep.GetBySpcification<Email>(c => c.id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public List<Email> GetAll_Email()
        {
            try
            {
                return myRep.GetAll<Email>().ToList();
            }
            catch { throw; }
        }

        public bool AddNew_Email(Email prod)
        {
            try
            {
                return myRep.Add<Email>(prod);
            }
            catch { throw; }
        }

        public bool Update_Email(Email prod)
        {
            try
            {
                return myRep.Update<Email>(prod);
            }
            catch { throw; }
        }

        public bool Delete_Email(Email prod)
        {
            try
            {
                return myRep.Delete<Email>(prod);
            }
            catch { throw; }
        }
    }
}

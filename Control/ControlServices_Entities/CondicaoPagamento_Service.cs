﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control
{
    public class CondicaoPagamento_Service
    {
        Repository myRep = new Repository();

        public CondicaoPagamento GetBySpecification_CondicaoPagamento(int codigo)
        {
            try
            {
                return myRep.GetBySpcification<CondicaoPagamento>(c => c.id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public CondicaoPagamento GetByNome_CondicaoPagamento(string codigo)
        {
            try
            {
                return myRep.GetBySpcification<CondicaoPagamento>(c => c.nome == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public List<CondicaoPagamento> GetAll_CondicaoPagamento()
        {
            try
            {
                return myRep.GetAll<CondicaoPagamento>().ToList();
            }
            catch { throw; }
        }

        public List<CondicaoPagamento> GetAllByMPTran_CondicaoPagamento(string mptran)
        {
            try
            {
                return myRep.GetAll<CondicaoPagamento>(c => c.mptran == mptran).ToList();
            }
            catch { throw; }
        }

        public bool AddNew_CondicaoPagamento(CondicaoPagamento prod)
        {
            try
            {
                return myRep.Add<CondicaoPagamento>(prod);
            }
            catch { throw; }
        }

        public bool Update_CondicaoPagamento(CondicaoPagamento prod)
        {
            try
            {
                return myRep.Update<CondicaoPagamento>(prod);
            }
            catch { throw; }
        }

        public bool Delete_CondicaoPagamento(CondicaoPagamento prod)
        {
            try
            {
                return myRep.Delete<CondicaoPagamento>(prod);
            }
            catch { throw; }
        }
    }
}

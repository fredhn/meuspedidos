﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control
{
    public class Contato_Service
    {
        Repository myRep = new Repository();

        public Contato GetBySpecification_Contato(int codigo)
        {
            try
            {
                return myRep.GetBySpcification<Contato>(c => c.id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public List<Contato> GetAll_Contato()
        {
            try
            {
                return myRep.GetAll<Contato>().ToList();
            }
            catch { throw; }
        }

        public bool AddNew_Contato(Contato prod)
        {
            try
            {
                return myRep.Add<Contato>(prod);
            }
            catch { throw; }
        }

        public bool Update_Contato(Contato prod)
        {
            try
            {
                return myRep.Update<Contato>(prod);
            }
            catch { throw; }
        }

        public bool Delete_Contato(Contato prod)
        {
            try
            {
                return myRep.Delete<Contato>(prod);
            }
            catch { throw; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control
{
    public class QuantidadeGrades_Service
    {
        Repository myRep = new Repository();

        public QuantidadeGrades GetBySpecification_QuantidadeGrades(int codigo)
        {
            try
            {
                return myRep.GetBySpcification<QuantidadeGrades>(c => c.id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public List<QuantidadeGrades> GetAll_QuantidadeGrades()
        {
            try
            {
                return myRep.GetAll<QuantidadeGrades>().ToList();
            }
            catch { throw; }
        }

        public bool AddNew_QuantidadeGrades(QuantidadeGrades prod)
        {
            try
            {
                return myRep.Add<QuantidadeGrades>(prod);
            }
            catch { throw; }
        }

        public bool Update_QuantidadeGrades(QuantidadeGrades prod)
        {
            try
            {
                return myRep.Update<QuantidadeGrades>(prod);
            }
            catch { throw; }
        }

        public bool Delete_QuantidadeGrades(QuantidadeGrades prod)
        {
            try
            {
                return myRep.Delete<QuantidadeGrades>(prod);
            }
            catch { throw; }
        }
    }
}

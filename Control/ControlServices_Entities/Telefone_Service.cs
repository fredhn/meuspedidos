﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control
{
    public class Telefone_Service
    {
        Repository myRep = new Repository();

        public Telefone GetBySpecification_Telefone(int codigo)
        {
            try
            {
                return myRep.GetBySpcification<Telefone>(c => c.id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public List<Telefone> GetAll_Telefones()
        {
            try
            {
                return myRep.GetAll<Telefone>().ToList();
            }
            catch { throw; }
        }

        public bool AddNew_Telefone(Telefone prod)
        {
            try
            {
                return myRep.Add<Telefone>(prod);
            }
            catch { throw; }
        }

        public bool Update_Telefone(Telefone prod)
        {
            try
            {
                return myRep.Update<Telefone>(prod);
            }
            catch { throw; }
        }

        public bool Delete_Telefone(Telefone prod)
        {
            try
            {
                return myRep.Delete<Telefone>(prod);
            }
            catch { throw; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control
{
    public class ICMS_ST_Service
    {
        Repository myRep = new Repository();

        public ICMS_ST GetBySpecification_ICMS_ST(int codigo)
        {
            try
            {
                return myRep.GetBySpcification<ICMS_ST>(c => c.id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public List<ICMS_ST> GetAll_ICMS_ST()
        {
            try
            {
                return myRep.GetAll<ICMS_ST>().ToList();
            }
            catch { throw; }
        }

        public List<ICMS_ST> GetAllByMPTran_ICMS_ST(string mptran)
        {
            try
            {
                return myRep.GetAll<ICMS_ST>(c => c.mptran == mptran).ToList();
            }
            catch { throw; }
        }

        public bool AddNew_ICMS_ST(ICMS_ST prod)
        {
            try
            {
                return myRep.Add<ICMS_ST>(prod);
            }
            catch { throw; }
        }

        public bool Update_ICMS_ST(ICMS_ST prod)
        {
            try
            {
                return myRep.Update<ICMS_ST>(prod);
            }
            catch { throw; }
        }

        public bool Delete_ICMS_ST(ICMS_ST prod)
        {
            try
            {
                return myRep.Delete<ICMS_ST>(prod);
            }
            catch { throw; }
        }
    }
}

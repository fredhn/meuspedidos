﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control
{
    public class ClienteCondicaoPagamento_Service
    {
        Repository myRep = new Repository();

        public ClienteCondicaoPagamento GetBySpecification_ClienteCondicaoPagamento(int codigo)
        {
            try
            {
                return myRep.GetBySpcification<ClienteCondicaoPagamento>(c => c.id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public ClienteCondicaoPagamento GetByClienteId_ClienteCondicaoPagamento(int codigo)
        {
            try
            {
                return myRep.GetBySpcification<ClienteCondicaoPagamento>(c => c.cliente_id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public ClienteCondicaoPagamento GetByCondPagamento_ClienteCondicaoPagamento(string codigo)
        {
            try
            {
                return myRep.GetBySpcification<ClienteCondicaoPagamento>(c => c.condicoes_pagamento_liberadas == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public List<ClienteCondicaoPagamento> GetAll_ClienteCondicaoPagamento()
        {
            try
            {
                return myRep.GetAll<ClienteCondicaoPagamento>().ToList();
            }
            catch { throw; }
        }

        public List<ClienteCondicaoPagamento> GetAllByMPTran_ClienteCondicaoPagamento(string mptran)
        {
            try
            {
                return myRep.GetAll<ClienteCondicaoPagamento>(c => c.mptran == mptran).ToList();
            }
            catch { throw; }
        }

        public bool AddNew_ClienteCondicaoPagamento(ClienteCondicaoPagamento ccp)
        {
            try
            {
                return myRep.Add<ClienteCondicaoPagamento>(ccp);
            }
            catch { throw; }
        }

        public bool Update_ClienteCondicaoPagamento(ClienteCondicaoPagamento ccp)
        {
            try
            {
                return myRep.Update<ClienteCondicaoPagamento>(ccp);
            }
            catch { throw; }
        }

        public bool Delete_ClienteCondicaoPagamento(ClienteCondicaoPagamento ccp)
        {
            try
            {
                return myRep.Delete<ClienteCondicaoPagamento>(ccp);
            }
            catch { throw; }
        }
    }
}

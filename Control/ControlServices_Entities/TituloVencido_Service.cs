﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control
{
    public class TituloVencido_Service
    {
        Repository myRep = new Repository();

        public TituloVencido GetBySpecification_TituloVencido(int codigo)
        {
            try
            {
                return myRep.GetBySpcification<TituloVencido>(c => c.id == codigo).SingleOrDefault();
            }
            catch { throw; }
        }

        public List<TituloVencido> GetAll_TituloVencido()
        {
            try
            {
                return myRep.GetAll<TituloVencido>().ToList();
            }
            catch { throw; }
        }

        public List<TituloVencido> GetAllByMPTran_TituloVencido(string mptran)
        {
            try
            {
                return myRep.GetAll<TituloVencido>(c => c.mptran == mptran).ToList();
            }
            catch { throw; }
        }

        public bool AddNew_TituloVencido(TituloVencido prod)
        {
            try
            {
                return myRep.Add<TituloVencido>(prod);
            }
            catch { throw; }
        }

        public bool Update_TituloVencido(TituloVencido prod)
        {
            try
            {
                return myRep.Update<TituloVencido>(prod);
            }
            catch { throw; }
        }

        public bool Delete_TituloVencido(TituloVencido prod)
        {
            try
            {
                return myRep.Delete<TituloVencido>(prod);
            }
            catch { throw; }
        }
    }
}

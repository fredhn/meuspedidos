﻿using System;
using Core;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Control.Validations;

namespace Control
{
    public class SyncClientesCategoria
    {
        //ENTIDADES:
        Entidade_Saida es;

        //SERVIÇOS:
        ContaMeusPedidos_Service s_contamp = new ContaMeusPedidos_Service();
        ClienteCategoria_Service s_cli = new ClienteCategoria_Service();
        EntidadeSaida_Service s_esaida = new EntidadeSaida_Service();

        //LISTAS:
        List<ContaMeusPedidos> listContaMP = new List<ContaMeusPedidos>();
        List<ClienteCategoria> listI = new List<ClienteCategoria>();
        List<ClienteCategoria> listA = new List<ClienteCategoria>();

        public void Execute()
        {
            listContaMP = s_contamp.GetAll_ContasMeusPedidosAtivas();
            listI = s_cli.GetAllByMPTran_ClienteCategoria("I");
            listA = s_cli.GetAllByMPTran_ClienteCategoria("A");

            foreach(var cmp in listContaMP)
            {
                if (cmp.status == "1")
                {
                    foreach (var cl in listI)
                    {
                        var a = s_esaida.GetClienteByIDENT_EntidadeSaida(cl.cliente_id.ToString());
                        ClienteCategoriaJ ccpj = ConvertClienteCategoria_V.Do(cl,a.id_mp);

                        string json = JsonConvert.SerializeObject(ccpj);

                        es = new Entidade_Saida();
                        es.filial = cmp.filial;
                        es.id_contamp = cmp.id;
                        es.id_ent = cl.id.ToString();
                        es.mptran = "I";
                        es.tpreg = "ClienteCat";
                        es.json = json;
                        es.status = "A";

                        s_esaida.AddNew_EntidadeSaida(es);

                        cl.mptran = null;
                        s_cli.Update_ClienteCategoria(cl);
                    }

                    foreach (var cl in listA)
                    {
                        var a = s_esaida.GetClienteByIDENT_EntidadeSaida(cl.cliente_id.ToString());
                        ClienteCategoriaJ ccpj = ConvertClienteCategoria_V.Do(cl,a.id_mp);

                        string json = JsonConvert.SerializeObject(ccpj);

                        es = new Entidade_Saida();
                        es.id_contamp = cmp.id;
                        es.filial = cmp.filial;
                        es.id_ent = cl.id.ToString();
                        es.mptran = "A";
                        es.tpreg = "ClienteCat";
                        es.json = json;
                        es.status = "A";

                        s_esaida.AddNew_EntidadeSaida(es);

                        cl.mptran = null;
                        s_cli.Update_ClienteCategoria(cl);
                    }
                }
            }
        }//FIM .Execute();
    }
}

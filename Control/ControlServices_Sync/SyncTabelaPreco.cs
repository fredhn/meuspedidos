﻿using System;
using Core;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Control.Validations;

namespace Control
{
    public class SyncTabelaPreco
    {
        //ENTIDADES:
        Entidade_Saida es;

        //SERVIÇOS:
        ContaMeusPedidos_Service s_contamp = new ContaMeusPedidos_Service();
        TabelaPreco_Service s_ = new TabelaPreco_Service();
        EntidadeSaida_Service s_esaida = new EntidadeSaida_Service();

        //LISTAS:
        List<ContaMeusPedidos> listContaMP = new List<ContaMeusPedidos>();
        List<TabelaPreco> listI = new List<TabelaPreco>();
        List<TabelaPreco> listA = new List<TabelaPreco>();

        public void Execute()
        {
            listContaMP = s_contamp.GetAll_ContasMeusPedidosAtivas();
            listI = s_.GetAllByMPTran_TabelaPreco("I");
            listA = s_.GetAllByMPTran_TabelaPreco("A");

            foreach(var cmp in listContaMP)
            {
                if (cmp.status == "1") //Se a conta estiver ativa
                {
                    foreach (var cl in listI)
                    {
                        TabelaPrecoJ tpj = ConvertTabelaPreco_V.Do(cl);
                        string json = JsonConvert.SerializeObject(tpj);

                        es = new Entidade_Saida();
                        es.filial = cmp.filial;
                        es.id_contamp = cmp.id;
                        es.id_ent = cl.id.ToString();
                        es.mptran = "I";
                        es.tpreg = "TabelaPreco";
                        es.json = json;
                        es.status = "A";

                        s_esaida.AddNew_EntidadeSaida(es);

                        cl.mptran = null;
                        s_.Update_TabelaPreco(cl);
                    }

                    foreach (var cl in listA)
                    {
                        string json = JsonConvert.SerializeObject(cl);

                        es = new Entidade_Saida();
                        es.id_contamp = cmp.id;
                        es.filial = cmp.filial;
                        es.id_ent = cl.id.ToString();
                        es.mptran = "A";
                        es.tpreg = "TabelaPreco";
                        es.json = json;
                        es.status = "A";

                        s_esaida.AddNew_EntidadeSaida(es);

                        cl.mptran = null;
                        s_.Update_TabelaPreco(cl);
                    }
                }
            }
        }//FIM .Execute();
    }
}

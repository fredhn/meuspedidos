﻿using System;
using Core;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Control.Validations;

namespace Control
{
    public class SyncProduto
    {
        //ENTIDADES:
        Entidade_Saida es;

        //SERVIÇOS:
        ContaMeusPedidos_Service s_contamp = new ContaMeusPedidos_Service();
        Produto_Service s_ = new Produto_Service();
        EntidadeSaida_Service s_esaida = new EntidadeSaida_Service();

        //LISTAS:
        List<ContaMeusPedidos> listContaMP = new List<ContaMeusPedidos>();
        List<Produto> listI = new List<Produto>();
        List<Produto> listA = new List<Produto>();

        public void Execute()
        {
            listContaMP = s_contamp.GetAll_ContasMeusPedidosAtivas();
            listI = s_.GetAllByMPTran_Produto("I");
            listA = s_.GetAllByMPTran_Produto("A");

            foreach(var cmp in listContaMP)
            {
                if (cmp.status == "1") //Se a conta estiver ativa
                {
                    foreach (var cl in listI)
                    {
                        //Conversao objeto Produto para formato aceito pela MP
                        ProdutoJ pj = ConvertProduto_V.Do(cl);

                        string json = JsonConvert.SerializeObject(pj);

                        if (pj.categoria_id == 0)
                        {
                            var o = (Newtonsoft.Json.Linq.JObject)JsonConvert.DeserializeObject(json);
                            o.Property("categoria_id").Remove();
                            json = o.ToString();
                        }
                        else
                        {
                            var prodcat = s_esaida.GetProdutoCatByIDENT_EntidadeSaida(pj.categoria_id.ToString());
                            var o = (Newtonsoft.Json.Linq.JObject)JsonConvert.DeserializeObject(json);
                            if(prodcat != null)
                            {
                                o.Property("categoria_id").Value = prodcat.id_mp;
                            }
                            json = o.ToString();
                        }

                        es = new Entidade_Saida();
                        es.filial = cmp.filial;
                        es.id_contamp = cmp.id;
                        es.id_ent = cl.id.ToString();
                        es.mptran = "I";
                        es.tpreg = "Produto";
                        es.json = json;
                        es.status = "A";

                        s_esaida.AddNew_EntidadeSaida(es);

                        cl.mptran = null;
                        s_.Update_Produto(cl);
                    }

                    foreach (var cl in listA)
                    {
                        //Conversao objeto Produto para formato aceito pela MP
                        ProdutoJ pj = ConvertProduto_V.Do(cl);

                        string json = JsonConvert.SerializeObject(pj);

                        if (pj.categoria_id == 0)
                        {
                            var o = (Newtonsoft.Json.Linq.JObject)JsonConvert.DeserializeObject(json);
                            o.Property("categoria_id").Remove();
                            json = o.ToString();
                        }
                        else
                        {
                            var prodcat = s_esaida.GetProdutoCatByIDENT_EntidadeSaida(pj.categoria_id.ToString());
                            var o = (Newtonsoft.Json.Linq.JObject)JsonConvert.DeserializeObject(json);
                            o.Property("categoria_id").Value = prodcat.id_mp;
                            json = o.ToString();
                        }

                        es = new Entidade_Saida();
                        es.id_contamp = cmp.id;
                        es.filial = cmp.filial;
                        es.id_ent = cl.id.ToString();
                        es.mptran = "A";
                        es.tpreg = "Produto";
                        es.json = json;
                        es.status = "A";

                        s_esaida.AddNew_EntidadeSaida(es);

                        cl.mptran = null;
                        s_.Update_Produto(cl);
                    }
                }
            }
        }//FIM .Execute();
    }
}

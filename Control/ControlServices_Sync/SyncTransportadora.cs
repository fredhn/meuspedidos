﻿using System;
using Core;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Control
{
    public class SyncTransportadora
    {
        //ENTIDADES:
        Entidade_Saida es;

        //SERVIÇOS:
        ContaMeusPedidos_Service s_contamp = new ContaMeusPedidos_Service();
        Transportadora_Service s_ = new Transportadora_Service();
        EntidadeSaida_Service s_esaida = new EntidadeSaida_Service();

        //LISTAS:
        List<ContaMeusPedidos> listContaMP = new List<ContaMeusPedidos>();
        List<Transportadora> listI = new List<Transportadora>();
        List<Transportadora> listA = new List<Transportadora>();

        public void Execute()
        {
            listContaMP = s_contamp.GetAll_ContasMeusPedidosAtivas();
            listI = s_.GetAllByMPTran_FullTransportadora("I");
            listA = s_.GetAllByMPTran_FullTransportadora("A");

            foreach(var cmp in listContaMP)
            {
                if (cmp.status == "1") //Se a conta estiver ativa
                {
                    foreach (var cl in listI)
                    {

                        string json = JsonConvert.SerializeObject(cl);

                        es = new Entidade_Saida();
                        es.filial = cmp.filial;
                        es.id_contamp = cmp.id;
                        es.id_ent = cl.id.ToString();
                        es.mptran = "I";
                        es.tpreg = "Transportadora";
                        es.json = json;
                        es.status = "A";

                        s_esaida.AddNew_EntidadeSaida(es);

                        cl.mptran = null;
                        s_.Update_Transportadora(cl);
                    }

                    foreach (var cl in listA)
                    {
                        string json = JsonConvert.SerializeObject(cl);

                        es = new Entidade_Saida();
                        es.id_contamp = cmp.id;
                        es.filial = cmp.filial;
                        es.id_ent = cl.id.ToString();
                        es.mptran = "A";
                        es.tpreg = "Transportadora";
                        es.json = json;
                        es.status = "A";

                        s_esaida.AddNew_EntidadeSaida(es);

                        cl.mptran = null;
                        s_.Update_Transportadora(cl);
                    }
                }
            }
        }//FIM .Execute();
    }
}

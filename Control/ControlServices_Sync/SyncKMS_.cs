﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Service_API;

namespace Control
{
    public static class SyncKMS_
    {
        public static bool Execute()
        {
            SyncEmpresa empSync = new SyncEmpresa();
            var ret = empSync.Execute();

            Log_Service.WriteErrorLog("Autorização KMS: " + ret.ToString());

            return ret;
        }

        public static object ExecuteByUser(string cnpj)
        {
            SyncEmpresaByUser empSyncUser = new SyncEmpresaByUser();
            var ret = empSyncUser.Execute(cnpj);

            return ret;
        }
    }
}

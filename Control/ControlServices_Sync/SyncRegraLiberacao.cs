﻿using System;
using Core;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Service_API;
using Control.Validations;

namespace Control
{
    public class SyncRegraLiberacao
    {
        //ENTIDADES:
        Entidade_Saida es;

        //SERVIÇOS:
        ContaMeusPedidos_Service s_contamp = new ContaMeusPedidos_Service();
        RegraLiberacao_Service s_ = new RegraLiberacao_Service();
        Vendedor_Service s_v = new Vendedor_Service();
        EntidadeSaida_Service s_esaida = new EntidadeSaida_Service();
        EntidadeEntrada_Service s_eentrada = new EntidadeEntrada_Service();

        //LISTAS:
        List<ContaMeusPedidos> listContaMP = new List<ContaMeusPedidos>();
        List<RegraLiberacao> listI = new List<RegraLiberacao>();
        List<RegraLiberacao> listA = new List<RegraLiberacao>();

        public void Execute()
        {
            listContaMP = s_contamp.GetAll_ContasMeusPedidosAtivas();
            listI = s_.GetAllByMPTran_RegraLiberacao("I");
            listA = s_.GetAllByMPTran_RegraLiberacao("A");

            foreach(var cmp in listContaMP)
            {
                if (cmp.status == "1") //Se a conta estiver ativa
                {
                    foreach (var cl in listI)
                    {
                        var ee = s_eentrada.GetVendedorByIDENT_EntidadeEntrada(cl.usuario_id.ToString());
                        //var v = s_v.GetBySpecification_Vendedor(cl.usuario_id);
                        var c = s_esaida.GetClienteByIDENT_EntidadeSaida(cl.cliente_id.ToString());

                        RegraLiberacaoJ rlj = ConvertRegraLiberacao_V.Do(cl, ee.id_mp, c.id_mp);

                        string json = JsonConvert.SerializeObject(rlj);

                        es = new Entidade_Saida();
                        es.filial = cmp.filial;
                        es.id_contamp = cmp.id;
                        es.id_ent = cl.id.ToString();
                        es.mptran = "I";
                        es.tpreg = "RegraLiberacao";
                        es.json = json;
                        es.status = "A";

                        s_esaida.AddNew_EntidadeSaida(es);

                        cl.mptran = null;
                        s_.Update_RegraLiberacao(cl);
                    }

                    foreach (var cl in listA)
                    {
                        string json = JsonConvert.SerializeObject(cl);

                        es = new Entidade_Saida();
                        es.id_contamp = cmp.id;
                        es.filial = cmp.filial;
                        es.id_ent = cl.id.ToString();
                        es.mptran = "A";
                        es.tpreg = "ProdTabelaPreco";
                        es.json = json;
                        es.status = "A";

                        s_esaida.AddNew_EntidadeSaida(es);

                        cl.mptran = null;
                        s_.Update_RegraLiberacao(cl);
                    }
                }
            }
        }//FIM .Execute();
    }
}

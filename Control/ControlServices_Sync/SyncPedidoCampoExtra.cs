﻿using System;
using Core;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Control
{
    public class SyncPedidoCampoExtra
    {
        //ENTIDADES:
        Entidade_Saida es;

        //SERVIÇOS:
        ContaMeusPedidos_Service s_contamp = new ContaMeusPedidos_Service();
        PedidoCampoExtra_Service s_ = new PedidoCampoExtra_Service();
        EntidadeSaida_Service s_esaida = new EntidadeSaida_Service();

        //LISTAS:
        List<ContaMeusPedidos> listContaMP = new List<ContaMeusPedidos>();
        List<PedidoCampoExtra> listI = new List<PedidoCampoExtra>();
        List<PedidoCampoExtra> listA = new List<PedidoCampoExtra>();

        public void Execute()
        {
            listContaMP = s_contamp.GetAll_ContasMeusPedidosAtivas();
            listI = s_.GetAllByMPTran_PedidoCampoExtra("I");
            listA = s_.GetAllByMPTran_PedidoCampoExtra("A");

            foreach(var cmp in listContaMP)
            {
                if (cmp.status == "1") //Se a conta estiver ativa
                {
                    foreach (var cl in listI)
                    {
                        string json = JsonConvert.SerializeObject(cl);

                        es = new Entidade_Saida();
                        es.filial = cmp.filial;
                        es.id_contamp = cmp.id;
                        es.id_ent = cl.id.ToString();
                        es.mptran = "I";
                        es.tpreg = "PedidoCExtra";
                        es.json = json;
                        es.status = "A";

                        s_esaida.AddNew_EntidadeSaida(es);

                        cl.mptran = null;
                        s_.Update_PedidoCampoExtra(cl);
                    }

                    foreach (var cl in listA)
                    {
                        string json = JsonConvert.SerializeObject(cl);

                        es = new Entidade_Saida();
                        es.id_contamp = cmp.id;
                        es.filial = cmp.filial;
                        es.id_ent = cl.id.ToString();
                        es.mptran = "A";
                        es.tpreg = "PedidoCExtra";
                        es.json = json;
                        es.status = "A";

                        s_esaida.AddNew_EntidadeSaida(es);

                        cl.mptran = null;
                        s_.Update_PedidoCampoExtra(cl);
                    }
                }
            }
        }//FIM .Execute();
    }
}

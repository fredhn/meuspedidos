﻿using System;
using Core;
using Control.Validations;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Service_API;
using System.IO;
using System.Xml;
using System.Configuration;

namespace Control
{
    public class SyncEmpresa
    {
        //ENTIDADES:        
        Empresa empresa;
        EmpresaJ empJ;
        Empresa_Service s_empresa = new Empresa_Service();

        Log_Licenciamento log_licence;
        LogLicenciamento_Service s_loglicence = new LogLicenciamento_Service();

        bool ret = false;

        public bool Execute()
        {
            try
            {
                var config2 = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                string codpro = config2.AppSettings.Settings["COD_PRO"].Value.ToString();
                string url = config2.AppSettings.Settings["URL"].Value.ToString();
                string cnpj = config2.AppSettings.Settings["CNPJ"].Value.ToString();

                if (codpro != String.Empty &&
                    url != String.Empty &&
                    cnpj != String.Empty)
                {

                    string url_ = Encryption_V.Decrypt(url.Trim());
                    string cnpj_ = Encryption_V.Decrypt(cnpj.Trim());
                    string codpro_ = Encryption_V.Decrypt(codpro.Trim());
                    string apptoken_ = AppToken_V.Generate();

                    Empresa_ServiceGET empresa_get = new Empresa_ServiceGET();
                    var resp = empresa_get.Empresa(url_, cnpj_, codpro_, apptoken_);

                    log_licence = new Log_Licenciamento();
                    log_licence.json = resp.Content.ToString();
                    log_licence.status_code = resp.StatusCode.ToString();
                    log_licence.status_description = resp.StatusDescription.ToString();
                    s_loglicence.AddNew_LogLicenciamento(log_licence);

                    if (resp != null && resp.Content != "" && resp.StatusDescription != "Not found")
                    {
                        empJ = JsonConvert.DeserializeObject<EmpresaJ>(resp.Content);
                        empresa = new Empresa();
                        empresa = ConvertEmpresa_V.Do(empJ);

                        var emp_ret = s_empresa.GetByCnpj_Empresa(empresa.cnpj);
                        //validar existencia da empresa no banco, caso exista fazer update, caso nao existe fazer insert
                        if (emp_ret == null)
                        {
                            empresa.ultima_validacao = Encryption_V.Encrypt(DateTime.Now.ToString());
                            s_empresa.AddNew_Empresa(empresa);
                        }
                        else
                        {
                            emp_ret.login = empresa.login;
                            emp_ret.password = empresa.password;
                            emp_ret.ultima_validacao = Encryption_V.Encrypt(DateTime.Now.ToString());
                            s_empresa.Update_Empresa(emp_ret);
                        }
                        ret = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Log_Service.WriteErrorLog(ex.Message);
                Log_Service.WriteErrorLog(ex.StackTrace);
                Log_Service.WriteErrorLog(ex);
                throw;
            }

            return ret;
        }//FIM .Execute();
    }
}

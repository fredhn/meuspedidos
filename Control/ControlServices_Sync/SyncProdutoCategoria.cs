﻿using System;
using Core;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Control.Validations;

namespace Control
{
    public class SyncProdutoCategoria
    {
        //ENTIDADES:
        Entidade_Saida es;

        //SERVIÇOS:
        ContaMeusPedidos_Service s_contamp = new ContaMeusPedidos_Service();
        ProdutoCategoria_Service s_prodcat = new ProdutoCategoria_Service();
        EntidadeSaida_Service s_esaida = new EntidadeSaida_Service();

        //LISTAS:
        List<ContaMeusPedidos> listContaMP = new List<ContaMeusPedidos>();
        List<ProdutoCategoria> listI = new List<ProdutoCategoria>();
        List<ProdutoCategoria> listA = new List<ProdutoCategoria>();

        public void Execute()
        {
            listContaMP = s_contamp.GetAll_ContasMeusPedidosAtivas();
            listI = s_prodcat.GetAllByMPTran_ProdutoCategorias("I");
            listA = s_prodcat.GetAllByMPTran_ProdutoCategorias("A");

            foreach(var cmp in listContaMP)
            {
                if (cmp.status == "1") //Se a conta estiver ativa
                {
                    foreach (var cl in listI)
                    {
                        ProdutoCategoriaJ pcj = ConvertProdutoCategoria_V.Do(cl);
                        
                        string json = JsonConvert.SerializeObject(pcj);

                        if (pcj.categoria_pai_id == 0)
                        {
                            var o = (Newtonsoft.Json.Linq.JObject)JsonConvert.DeserializeObject(json);
                            o.Property("categoria_pai_id").Remove();
                            json = o.ToString();
                        }
                        else
                        {
                            var aux = s_prodcat.GetBySpecification_ProdutoCategoria(pcj.categoria_pai_id);
                            var pai = s_esaida.GetProdutoCatByIDENT_EntidadeSaida(aux.id.ToString());
                            var o = (Newtonsoft.Json.Linq.JObject)JsonConvert.DeserializeObject(json);
                            o.Property("categoria_pai_id").Value = pai.id_mp;
                            json = o.ToString();
                        }

                        es = new Entidade_Saida();
                        es.filial = cmp.filial;
                        es.id_contamp = cmp.id;
                        es.id_ent = cl.id.ToString();
                        es.mptran = "I";
                        es.tpreg = "CategoriaProduto";
                        es.json = json.Replace(" ", String.Empty);
                        es.status = "A";

                        s_esaida.AddNew_EntidadeSaida(es);

                        cl.mptran = null;
                        s_prodcat.Update_ProdutoCategoria(cl);
                    }

                    foreach (var cl in listA)
                    {
                        ProdutoCategoriaJ pcj = ConvertProdutoCategoria_V.Do(cl);

                        string json = JsonConvert.SerializeObject(pcj);

                        if (pcj.categoria_pai_id == 0)
                        {
                            var o = (Newtonsoft.Json.Linq.JObject)JsonConvert.DeserializeObject(json);
                            o.Property("categoria_pai_id").Remove();
                            json = o.ToString();
                        }
                        else
                        {
                            var aux = s_prodcat.GetBySpecification_ProdutoCategoria(pcj.categoria_pai_id);
                            var pai = s_esaida.GetProdutoCatByIDENT_EntidadeSaida(aux.id.ToString());
                            var o = (Newtonsoft.Json.Linq.JObject)JsonConvert.DeserializeObject(json);
                            o.Property("categoria_pai_id").Value = pai.id_mp;
                            json = o.ToString();
                        }

                        es = new Entidade_Saida();
                        es.id_contamp = cmp.id;
                        es.filial = cmp.filial;
                        es.id_ent = cl.id.ToString();
                        es.mptran = "A";
                        es.tpreg = "CategoriaProduto";
                        es.json = json.Replace(" ", String.Empty); ;
                        es.status = "A";

                        s_esaida.AddNew_EntidadeSaida(es);

                        cl.mptran = null;
                        s_prodcat.Update_ProdutoCategoria(cl);
                    }
                }
            }
        }//FIM .Execute();
    }
}

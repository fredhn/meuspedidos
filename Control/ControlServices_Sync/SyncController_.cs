﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Control.Validations;
using Core;
using Service_API;

namespace Control
{
    public static class SyncController_
    {
        public static void Execute()
        {
            if (Empresa_V.ExecuteValidation())
            {
                var validacao_status = SyncKMS_.Execute();
            }

            Log_Service.WriteErrorLog("Entidades:\n\n");

            SyncClientes cliSync = new SyncClientes();
            cliSync.Execute();
            SyncAPI.Execute();
            Log_Service.WriteErrorLog("(Out) Entidade sincronizada: Clientes");

            SyncTransportadora transpSync = new SyncTransportadora();
            transpSync.Execute();
            SyncAPI.Execute();
            Log_Service.WriteErrorLog("(Out) Entidade sincronizada: Transportadoras");

            SyncICMSST icmsstSync = new SyncICMSST();
            icmsstSync.Execute();
            SyncAPI.Execute();
            Log_Service.WriteErrorLog("(Out) Entidade sincronizada: ICMS-ST");

            SyncCondicaoPagamento condpagSync = new SyncCondicaoPagamento();
            condpagSync.Execute();
            SyncAPI.Execute();
            Log_Service.WriteErrorLog("(Out) Entidade sincronizada: Condições de Pagamento");

            SyncProdutoCategoria prodcatSync = new SyncProdutoCategoria();
            prodcatSync.Execute();
            SyncAPI.Execute();
            Log_Service.WriteErrorLog("(Out) Entidade sincronizada: Categorias de Produto");

            SyncPedidoCampoExtra pedidoceSync = new SyncPedidoCampoExtra();
            pedidoceSync.Execute();
            SyncAPI.Execute();
            Log_Service.WriteErrorLog("(Out) Entidade sincronizada: Pedidos - Campos Extras");

            SyncTabelaPreco tabelapSync = new SyncTabelaPreco();
            tabelapSync.Execute();
            SyncAPI.Execute();
            Log_Service.WriteErrorLog("(Out) Entidade sincronizada: Tabelas de Preço");

            SyncFormaPagamento fpagSync = new SyncFormaPagamento();
            fpagSync.Execute();
            SyncAPI.Execute();
            Log_Service.WriteErrorLog("(Out) Entidade sincronizada: Formas de Pagamento");

            SyncProduto prodSync = new SyncProduto();
            prodSync.Execute();
            SyncAPI.Execute();
            Log_Service.WriteErrorLog("(Out) Entidade sincronizada: Produtos");

            SyncPedido pedidoSync = new SyncPedido();
            pedidoSync.Execute();
            SyncAPI.Execute();
            Log_Service.WriteErrorLog("(Out) Entidade sincronizada: Pedidos");

            SyncPedidoFaturamento pedidoFatSync = new SyncPedidoFaturamento();
            pedidoFatSync.Execute();
            SyncAPI.Execute();
            Log_Service.WriteErrorLog("(Out) Entidade sincronizada: Pedidos - Faturamento");

            SyncClientesCondicaoPagamento cliCondPagSync = new SyncClientesCondicaoPagamento();
            cliCondPagSync.Execute();
            SyncAPI.Execute();
            Log_Service.WriteErrorLog("(Out) Entidade sincronizada: Condições de Pagamento por Cliente");

            SyncClientesCategoria cliCatSync = new SyncClientesCategoria();
            cliCatSync.Execute();
            SyncAPI.Execute();
            Log_Service.WriteErrorLog("(Out) Entidade sincronizada: Categorias por Cliente");

            SyncProdutoTabelaPreco prodtpSync = new SyncProdutoTabelaPreco();
            prodtpSync.Execute();
            SyncAPI.Execute();
            Log_Service.WriteErrorLog("(Out) Entidade sincronizada: Tabelas de Preço por Produto");

            SyncClientesTabelaPreco cliTabPrecoSync = new SyncClientesTabelaPreco();
            cliTabPrecoSync.Execute();
            SyncAPI.Execute();
            Log_Service.WriteErrorLog("(Out) Entidade sincronizada: Tabelas de Preço por Cliente");

            SyncInVendedores vendedoresSyncIn = new SyncInVendedores();
            vendedoresSyncIn.Execute();
            SyncAPI.Execute();
            Log_Service.WriteErrorLog("(In) Entidade sincronizada: Vendedores");

            SyncRegraLiberacao reglSync = new SyncRegraLiberacao();
            reglSync.Execute();
            SyncAPI.Execute();
            Log_Service.WriteErrorLog("(Out) Entidade sincronizada: Regras de Liberação");

            SyncTituloVencido tvencidoSync = new SyncTituloVencido();
            tvencidoSync.Execute();
            SyncAPI.Execute();
            Log_Service.WriteErrorLog("(Out) Entidade sincronizada: Titulos Vencidos");

            SyncInPedidos pedidoSyncIn = new SyncInPedidos();
            pedidoSyncIn.Execute();
            Log_Service.WriteErrorLog("(In) Entidade sincronizada: Pedidos");

        }
    }
}

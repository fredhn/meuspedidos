﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Service_API;
using Newtonsoft.Json;
using Control.Validations;
using Newtonsoft.Json.Linq;

namespace Control
{
    public class RessyncController
    {
        public static string Execute(Entidade_Saida es)
        {
            EntidadeSaida_Service s_esaida = new EntidadeSaida_Service();
            string jsonRet = String.Empty;
            Entidade_Saida esCli, esTransp, esCondpag, esFpag, esPed, esProd, esTabPreco;

            switch (es.tpreg)
            {
                case "Cliente":

                    Cliente_Service s_cli = new Cliente_Service();
                    Cliente cli = new Cliente();

                    cli = s_cli.GetBySpecification2_Cliente(int.Parse(es.id_ent));

                    jsonRet = JsonConvert.SerializeObject(cli);

                    break;

                case "ClienteCondPag":

                    ClienteCondicaoPagamento_Service s_clicpag = new ClienteCondicaoPagamento_Service();
                    ClienteCondicaoPagamento clicp = new ClienteCondicaoPagamento();
                    clicp = s_clicpag.GetBySpecification_ClienteCondicaoPagamento(int.Parse(es.id_ent));

                    ClienteCondicaoPagamentoJ ccpj = ConvertClienteCondicaoPagamento_V.Do(clicp, es.id_mp);
                    jsonRet = JsonConvert.SerializeObject(ccpj);

                    break;

                case "ClienteCat":

                    ClienteCategoria_Service s_clicat = new ClienteCategoria_Service();
                    ClienteCategoria clicat = new ClienteCategoria();
                    clicat = s_clicat.GetBySpecification_ClienteCategoria(int.Parse(es.id_ent));
                    esCli = s_esaida.GetClienteByIDENT_EntidadeSaida(clicat.cliente_id.ToString());

                    ClienteCategoriaJ clicatj = ConvertClienteCategoria_V.Do(clicat, esCli.id_mp);
                    jsonRet = JsonConvert.SerializeObject(clicatj);

                    break;

                case "ClienteTabPreco":

                    ClienteTabelaPreco_Service s_clitp = new ClienteTabelaPreco_Service();
                    ClienteTabelaPreco clitp = new ClienteTabelaPreco();
                    clitp = s_clitp.GetBySpecification_ClienteTabelaPreco(int.Parse(es.id_ent));
                    esCli = s_esaida.GetClienteByIDENT_EntidadeSaida(clitp.cliente_id.ToString());

                    ClienteTabelaPrecoJ clitpj = ConvertClienteTabelaPreco_V.Do(clitp, esCli.id_mp);
                    jsonRet = JsonConvert.SerializeObject(clitpj);

                    break;

                case "CondicaoPagamento":

                    CondicaoPagamento_Service s_condpag = new CondicaoPagamento_Service();
                    CondicaoPagamento condpag = new CondicaoPagamento();
                    condpag = s_condpag.GetBySpecification_CondicaoPagamento(int.Parse(es.id_ent));

                    CondicaoPagamentoJ condpagj = ConvertCondicaoPagamento_V.Do(condpag);
                    jsonRet = JsonConvert.SerializeObject(condpagj);

                    break;

                case "FormaPagamento":

                    FormaPagamento_Service s_fpag = new FormaPagamento_Service();
                    FormaPagamento fpag = new FormaPagamento();
                    fpag = s_fpag.GetBySpecification_FormaPagamento(int.Parse(es.id_ent));

                    FormaPagamentoJ fpagj = ConvertFormaPagamento_V.Do(fpag);
                    jsonRet = JsonConvert.SerializeObject(fpagj);

                    break;

                case "ICMSST":

                    ICMS_ST_Service s_icmsst = new ICMS_ST_Service();
                    ICMS_ST icmsst = new ICMS_ST();
                    icmsst = s_icmsst.GetBySpecification_ICMS_ST(int.Parse(es.id_ent));

                    ICMS_STJ icmsstj = ConvertICMS_ST_V.Do(icmsst);
                    jsonRet = JsonConvert.SerializeObject(icmsstj);

                    break;

                case "Pedido":

                    Pedido_Service s_ped = new Pedido_Service();
                    Pedido ped = new Pedido();
                    ped = s_ped.GetBySpecification_Pedido(int.Parse(es.id_ent));
                    esCli = s_esaida.GetClienteByIDENT_EntidadeSaida(ped.cliente_id.ToString());
                    esTransp = s_esaida.GetTransportadoraByIDENT_EntidadeSaida(ped.transportadora_id.ToString());
                    esCondpag = s_esaida.GetCondicaoPagByIDENT_EntidadeSaida(ped.condicao_pagamento_id.ToString());
                    esFpag = s_esaida.GetFormaPagByIDENT_EntidadeSaida(ped.forma_pagamento_id.ToString());

                    PedidoJ pedj = ConvertPedido_V.Do(ped, esCli.id_mp, esTransp.id_mp, esCondpag.id_mp, esFpag.id_mp);
                    jsonRet = JsonConvert.SerializeObject(pedj);

                    JObject jsonO = JObject.Parse(jsonRet);

                    JArray itensp = (JArray)jsonO["itens"];
                    itensp.Descendants()
                        .OfType<JProperty>()
                        .Where(attr => attr.Name.StartsWith("descontos") && !attr.Value.HasValues)
                        .ToList()
                        .ForEach(attr => attr.Remove()); // removing unwanted attributes
                    jsonRet = itensp.ToString(); // backing result to json

                    jsonO.SelectToken("itens").Replace(JArray.Parse(jsonRet));
                    jsonRet = jsonO.ToString();

                    //Correr jarray com mais posições (itens)
                    string moeda = (string)jsonO.SelectToken("itens[0].moeda");
                    if (moeda == "0")
                    {
                        itensp.Descendants()
                        .OfType<JProperty>()
                        .Where(attr => attr.Name.StartsWith("moeda") && !attr.Value.HasValues)
                        .ToList()
                        .ForEach(attr => attr.Remove()); // removing unwanted attributes
                        jsonRet = itensp.ToString(); // backing result to json

                        itensp.Descendants()
                        .OfType<JProperty>()
                        .Where(attr => attr.Name.StartsWith("cotacao_moeda"))
                        .ToList()
                        .ForEach(attr => attr.Remove()); // removing unwanted attributes
                        jsonRet = itensp.ToString(); // backing result to json


                        jsonO.SelectToken("itens").Replace(JArray.Parse(jsonRet));
                        jsonRet = jsonO.ToString();
                    }

                    break;

                case "PedidoCExtra":

                    PedidoCampoExtra_Service s_pedce = new PedidoCampoExtra_Service();
                    PedidoCampoExtra pedce = new PedidoCampoExtra();
                    pedce = s_pedce.GetBySpecification_PedidoCampoExtra(int.Parse(es.id_ent));
                    
                    jsonRet = JsonConvert.SerializeObject(pedce);

                    break;

                case "PedidoFat":

                    PedidoFaturamento_Service s_pedfat = new PedidoFaturamento_Service();
                    PedidoFaturamento pedfat = new PedidoFaturamento();
                    pedfat = s_pedfat.GetBySpecification_PedidoFaturamento(int.Parse(es.id_ent));
                    esPed = s_esaida.GetPedidoByIDENT_EntidadeSaida(pedfat.pedido_id.ToString());

                    PedidoFaturamentoJ pedfatj = ConvertPedidoFaturamento_V.Do(pedfat, esPed.id_mp);
                    jsonRet = JsonConvert.SerializeObject(pedfatj);

                    break;

                case "Produto":

                    Produto_Service s_prod = new Produto_Service();
                    Produto prod = new Produto();
                    prod = s_prod.GetBySpecification_Produto(int.Parse(es.id_ent));

                    ProdutoJ pj = ConvertProduto_V.Do(prod);
                    string json = JsonConvert.SerializeObject(pj);

                    if (pj.categoria_id == 0)
                    {
                        var o = (Newtonsoft.Json.Linq.JObject)JsonConvert.DeserializeObject(json);
                        o.Property("categoria_id").Remove();
                        json = o.ToString();
                    }
                    else
                    {
                        var prodcatax = s_esaida.GetProdutoCatByIDENT_EntidadeSaida(pj.categoria_id.ToString());
                        var o = (Newtonsoft.Json.Linq.JObject)JsonConvert.DeserializeObject(json);
                        o.Property("categoria_id").Value = prodcatax.id_mp;
                        json = o.ToString();
                    }
                    jsonRet = json;

                    break;

                case "CategoriaProduto":

                    ProdutoCategoria_Service s_prodcat = new ProdutoCategoria_Service();
                    ProdutoCategoria prodcat = new ProdutoCategoria();
                    prodcat = s_prodcat.GetBySpecification_ProdutoCategoria(int.Parse(es.id_ent));

                    ProdutoCategoriaJ prodcatj = ConvertProdutoCategoria_V.Do(prodcat);
                    jsonRet = JsonConvert.SerializeObject(prodcatj);

                    break;

                case "ProdTabelaPreco":

                    ProdutoTabelaPreco_Service s_prodtp = new ProdutoTabelaPreco_Service();
                    ProdutoTabelaPreco prodtp = new ProdutoTabelaPreco();
                    prodtp = s_prodtp.GetBySpecification_ProdutoTabelaPreco(int.Parse(es.id_ent));
                    esProd = s_esaida.GetProdutoByIDENT_EntidadeSaida(prodtp.produto_id.ToString());
                    esTabPreco = s_esaida.GetTabelaPrecoByIDENT_EntidadeSaida(prodtp.tabela_id.ToString());

                    ProdutoTabelaPrecoJ prodtpj = ConvertProdutoTabelaPreco_V.Do(prodtp, esProd.id_mp, esTabPreco.id_mp);
                    jsonRet = JsonConvert.SerializeObject(prodtpj);

                    break;

                case "RegraLiberacao":

                    RegraLiberacao_Service s_rl = new RegraLiberacao_Service();
                    RegraLiberacao rl = new RegraLiberacao();
                    rl = s_rl.GetBySpecification_RegraLiberacao(int.Parse(es.id_ent));
                    EntidadeEntrada_Service s_eentrada = new EntidadeEntrada_Service();
                    var ee = s_eentrada.GetVendedorByIDENT_EntidadeEntrada(rl.usuario_id.ToString());
                    esCli = s_esaida.GetClienteByIDENT_EntidadeSaida(rl.cliente_id.ToString());

                    RegraLiberacaoJ rlj = ConvertRegraLiberacao_V.Do(rl, ee.id_mp, esCli.id_mp);
                    jsonRet = JsonConvert.SerializeObject(rlj);

                    break;

                case "TabelaPreco":

                    TabelaPreco_Service s_tabpreco = new TabelaPreco_Service();
                    TabelaPreco tabpreco = new TabelaPreco();
                    tabpreco = s_tabpreco.GetBySpecification_TabelaPreco(int.Parse(es.id_ent));

                    TabelaPrecoJ tabprecoj = ConvertTabelaPreco_V.Do(tabpreco);
                    jsonRet = JsonConvert.SerializeObject(tabprecoj);

                    break;

                case "TituloVencido":

                    TituloVencido_Service s_tv = new TituloVencido_Service();
                    TituloVencido tv = new TituloVencido();
                    tv = s_tv.GetBySpecification_TituloVencido(int.Parse(es.id_ent));
                    esCli = s_esaida.GetClienteByIDENT_EntidadeSaida(tv.cliente_id.ToString());

                    TituloVencidoJ tvj = ConvertTituloVencido_V.Do(tv, esCli.id_mp);
                    jsonRet = JsonConvert.SerializeObject(tvj);

                    break;

                case "Transportadora":

                    Transportadora_Service s_transp = new Transportadora_Service();
                    Transportadora transp = new Transportadora();
                    transp = s_transp.GetBySpecification2_Transportadora(int.Parse(es.id_ent));
                    
                    jsonRet = JsonConvert.SerializeObject(transp);

                    break;

                default:
                    //DEFAULT CASE
                    break;
            }
            return jsonRet;
        }
    }
}

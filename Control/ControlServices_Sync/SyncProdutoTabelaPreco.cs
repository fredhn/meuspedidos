﻿using System;
using Core;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Control.Validations;

namespace Control
{
    public class SyncProdutoTabelaPreco
    {
        //ENTIDADES:
        Entidade_Saida es;

        //SERVIÇOS:
        ContaMeusPedidos_Service s_contamp = new ContaMeusPedidos_Service();
        ProdutoTabelaPreco_Service s_ = new ProdutoTabelaPreco_Service();
        EntidadeSaida_Service s_esaida = new EntidadeSaida_Service();

        //LISTAS:
        List<ContaMeusPedidos> listContaMP = new List<ContaMeusPedidos>();
        List<ProdutoTabelaPreco> listI = new List<ProdutoTabelaPreco>();
        List<ProdutoTabelaPreco> listA = new List<ProdutoTabelaPreco>();

        public void Execute()
        {
            listContaMP = s_contamp.GetAll_ContasMeusPedidosAtivas();
            listI = s_.GetAllByMPTran_ProdutoTabelaPreco("I");
            listA = s_.GetAllByMPTran_ProdutoTabelaPreco("A");

            foreach(var cmp in listContaMP)
            {
                if (cmp.status == "1") //Se a conta estiver ativa
                {
                    foreach (var cl in listI)
                    {
                        var getProd = s_esaida.GetProdutoByIDENT2_EntidadeSaida(cl.produto_id.ToString());
                        var getTabP = s_esaida.GetTabelaPrecoByIDENT_EntidadeSaida(cl.tabela_id.ToString());

                        if (getProd.id_mp != null && getTabP.id_mp != null)
                        {
                            ProdutoTabelaPrecoJ ptpj = ConvertProdutoTabelaPreco_V.Do(cl, getProd.id_mp, getTabP.id_mp);

                            string json = JsonConvert.SerializeObject(ptpj);

                            es = new Entidade_Saida();
                            es.filial = cmp.filial;
                            es.id_contamp = cmp.id;
                            es.id_ent = cl.id.ToString();
                            es.mptran = "I";
                            es.tpreg = "ProdTabelaPreco";
                            es.json = json;
                            es.status = "A";

                            s_esaida.AddNew_EntidadeSaida(es);

                            cl.mptran = null;
                            s_.Update_ProdutoTabelaPreco(cl);
                        }   
                    }

                    foreach (var cl in listA)
                    {
                        string json = JsonConvert.SerializeObject(cl);

                        es = new Entidade_Saida();
                        es.id_contamp = cmp.id;
                        es.filial = cmp.filial;
                        es.id_ent = cl.id.ToString();
                        es.mptran = "A";
                        es.tpreg = "ProdTabelaPreco";
                        es.json = json;
                        es.status = "A";

                        s_esaida.AddNew_EntidadeSaida(es);

                        cl.mptran = null;
                        s_.Update_ProdutoTabelaPreco(cl);
                    }
                }
            }
        }//FIM .Execute();
    }
}

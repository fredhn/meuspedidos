﻿using System;
using Core;
using Control.Validations;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Service_API;
using System.IO;
using System.Xml;
using System.Configuration;

namespace Control
{
    public class SyncEmpresaByUser
    {
        //ENTIDADES:        
        Empresa empresa;
        EmpresaJ empJ;
        Empresa_Service s_empresa = new Empresa_Service();

        Log_Licenciamento log_licence;
        LogLicenciamento_Service s_loglicence = new LogLicenciamento_Service();

        string err_retorno;

        public object Execute(string cnpj)
        {
            try
            {
                var config2 = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                string codpro = config2.AppSettings.Settings["COD_PRO"].Value.ToString();
                string url = config2.AppSettings.Settings["URL"].Value.ToString();

                if (codpro != String.Empty &&
                    url != String.Empty &&
                    cnpj != String.Empty)
                {

                    string url_ = Encryption_V.Decrypt(url.Trim());
                    string cnpj_ = Encryption_V.Decrypt(cnpj.Trim());
                    string codpro_ = Encryption_V.Decrypt(codpro.Trim());
                    string apptoken_ = AppToken_V.Generate();

                    Empresa_ServiceGET empresa_get = new Empresa_ServiceGET();
                    var resp = empresa_get.Empresa(url_, cnpj_, codpro_, apptoken_);

                    if(resp.StatusDescription != null)
                    {
                        log_licence = new Log_Licenciamento();
                        log_licence.json = resp.Content.ToString();
                        log_licence.status_code = resp.StatusCode.ToString();
                        log_licence.status_description = resp.StatusDescription.ToString();
                        s_loglicence.AddNew_LogLicenciamento(log_licence);
                    }
                    
                    if (resp != null && resp.Content != "" && resp.StatusDescription != "Not found" && resp.StatusDescription != "Not authorized")
                    {
                        empJ = JsonConvert.DeserializeObject<EmpresaJ>(resp.Content);
                        empresa = new Empresa();
                        empresa = ConvertEmpresa_V.Do(empJ);
                    }
                    else if (resp.StatusDescription == "Not authorized")
                    {
                        err_retorno = "Erro: " + resp.Content.ToString();
                    }
                    else
                    {
                        err_retorno = "Erro: " + resp.ErrorMessage;
                    }
                }
            }
            catch (Exception ex)
            {
                Log_Service.WriteErrorLog(ex.Message);
                Log_Service.WriteErrorLog(ex.StackTrace);
                Log_Service.WriteErrorLog(ex);
                throw;
            }

            if(empresa != null)
            {
                return empresa;
            }
            else
            {
                return err_retorno;
            }
            
        }//FIM .Execute();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Service_API;
using Newtonsoft.Json.Linq;

namespace Control
{
    public class SyncAPI
    {
        public static void Execute()
        {
            EntidadeSaida_Service s_esaida = new EntidadeSaida_Service();
            ContaMeusPedidos_Service s_cmp = new ContaMeusPedidos_Service();

            List<Entidade_Saida> es_list = new List<Entidade_Saida>();
            ContaMeusPedidos cmp = new ContaMeusPedidos();

            es_list = s_esaida.GetAllByStatus_EntidadeSaida("A");

            foreach(var esaida in es_list)
            {
                //Recebe os dados da Conta Meus Pedidos
                cmp = s_cmp.GetByID_ContaMeusPedidos(esaida.id_contamp);

                switch (esaida.tpreg)
                {
                    case "Cliente":
                        if (esaida.mptran == "I" && esaida.status == "A")
                        {
                            Cliente_ServicePOST cl_post = new Cliente_ServicePOST();
                            var resp = cl_post.IncluirCliente(cmp.url, cmp.apptoken, cmp.cmptoken, esaida.json);

                            esaida.codret = ((int)resp.StatusCode).ToString();
                            if (esaida.codret != "200" && esaida.codret != "201")
                            {
                                Cliente_Service s_ = new Cliente_Service();
                                var entidade = s_.GetBySpecification2_Cliente(int.Parse(esaida.id_ent));
                                entidade.mptran = "I";
                                s_.Update_Cliente(entidade);

                                esaida.status = "E";
                                if (resp.Content.ToString() == "")
                                {
                                    esaida.jsonret = resp.ErrorMessage;
                                }
                                else
                                {
                                    esaida.jsonret = resp.Content.ToString();
                                }
                            }
                            else
                            {
                                esaida.jsonret = resp.StatusDescription;
                                esaida.id_mp = resp.Headers[1].Value.ToString();
                                esaida.status = "I";
                                esaida.dtinte = DateTime.Now.ToString("dd-MM-yy");
                            }

                            s_esaida.Update_EntidadeSaida(esaida);
                        }
                        else if (esaida.mptran == "A" && esaida.status == "A")
                        {
                            var ent = s_esaida.GetClienteByIDENT_EntidadeSaida(esaida.id_ent);
                            Cliente_ServicePUT cl_put = new Cliente_ServicePUT();
                            var resp = cl_put.AlterarCliente(cmp.url, cmp.apptoken, cmp.cmptoken, esaida.json, ent.id_mp);

                            esaida.codret = ((int)resp.StatusCode).ToString();
                            if (esaida.codret != "200" && esaida.codret != "201")
                            {
                                Cliente_Service s_ = new Cliente_Service();
                                var entidade = s_.GetBySpecification2_Cliente(int.Parse(esaida.id_ent));
                                entidade.mptran = "A";
                                s_.Update_Cliente(entidade);

                                esaida.status = "E";
                                if (resp.Content.ToString() == "")
                                {
                                    esaida.jsonret = resp.ErrorMessage;
                                }
                                else
                                {
                                    esaida.jsonret = resp.Content.ToString();
                                }
                            }
                            else
                            {
                                esaida.jsonret = resp.StatusDescription;
                                esaida.id_mp = resp.Headers[1].Value.ToString();
                                esaida.status = "I";
                                esaida.dtinte = DateTime.Now.ToString("dd-MM-yy");
                            }

                            s_esaida.Update_EntidadeSaida(esaida);
                        }
                    break;

                    case "ClienteCondPag":
                    if (esaida.mptran == "I" && esaida.status == "A" && esaida.json.Contains("condicoes_pagamento_liberadas"))
                    {
                        ClienteCondicaoPagamento_ServicePOST cl_post = new ClienteCondicaoPagamento_ServicePOST();
                        var resp = cl_post.IncluirClienteCondicaoPagamento(cmp.url, cmp.apptoken, cmp.cmptoken, esaida.json);

                        esaida.codret = ((int)resp.StatusCode).ToString();
                        if(esaida.codret != "201" && esaida.codret != "200")
                        {
                            ClienteCondicaoPagamento_Service s_ = new ClienteCondicaoPagamento_Service();
                            var entidade = s_.GetBySpecification_ClienteCondicaoPagamento(int.Parse(esaida.id_ent));
                            entidade.mptran = "I";
                            s_.Update_ClienteCondicaoPagamento(entidade);

                            esaida.status = "E";
                            if (resp.Content.ToString() == "")
                            {
                                esaida.jsonret = resp.ErrorMessage;
                            }
                            else
                            {
                                esaida.jsonret = resp.Content.ToString();
                            }
                        }
                        else
                        {
                            esaida.jsonret = resp.StatusDescription;
                            esaida.id_mp = resp.Headers[1].Value.ToString();
                            esaida.status = "I";
                            esaida.dtinte = DateTime.Now.ToString("dd-MM-yy");
                        }
                        

                        s_esaida.Update_EntidadeSaida(esaida);
                    }
                    else if (esaida.mptran == "I" && esaida.status == "A" && !esaida.json.Contains("condicoes_pagamento_liberadas"))
                    {
                        ClienteCondicaoPagamento_ServicePOST cl_put = new ClienteCondicaoPagamento_ServicePOST();
                        var resp = cl_put.IncluirClienteTodasCondicoesPagamento(cmp.url, cmp.apptoken, cmp.cmptoken, esaida.json);

                        esaida.codret = ((int)resp.StatusCode).ToString();
                        if (esaida.codret != "200" && esaida.codret != "201")
                        {
                            ClienteCondicaoPagamento_Service s_ = new ClienteCondicaoPagamento_Service();
                            var entidade = s_.GetBySpecification_ClienteCondicaoPagamento(int.Parse(esaida.id_ent));
                            entidade.mptran = "A";
                            s_.Update_ClienteCondicaoPagamento(entidade);

                            esaida.status = "E";
                            if (resp.Content.ToString() == "")
                            {
                                esaida.jsonret = resp.ErrorMessage;
                            }
                            else
                            {
                                esaida.jsonret = resp.Content.ToString();
                            }
                        }
                        else
                        {
                            esaida.jsonret = resp.StatusDescription;
                            esaida.id_mp = resp.Headers[1].Value.ToString();
                            esaida.status = "I";
                            esaida.dtinte = DateTime.Now.ToString("dd-MM-yy");
                        }

                            s_esaida.Update_EntidadeSaida(esaida);
                    }
                    break;

                    case "ClienteCat":
                        if (esaida.mptran == "I" && esaida.status == "A" && esaida.json.Contains("categorias_liberadas"))
                        {
                            ClienteCategoria_ServicePOST cl_post = new ClienteCategoria_ServicePOST();
                            var resp = cl_post.IncluirClienteCategoria(cmp.url, cmp.apptoken, cmp.cmptoken, esaida.json);

                            esaida.codret = ((int)resp.StatusCode).ToString();
                            if (esaida.codret != "200" && esaida.codret != "201")
                            {
                                ClienteCategoria_Service s_ = new ClienteCategoria_Service();
                                var entidade = s_.GetBySpecification_ClienteCategoria(int.Parse(esaida.id_ent));
                                entidade.mptran = "I";
                                s_.Update_ClienteCategoria(entidade);

                                esaida.status = "E";
                                if (resp.Content.ToString() == "")
                                {
                                    esaida.jsonret = resp.ErrorMessage;
                                }
                                else
                                {
                                    esaida.jsonret = resp.Content.ToString();
                                }
                            }
                            else
                            {
                                esaida.jsonret = resp.StatusDescription;
                                esaida.id_mp = resp.Headers[1].Value.ToString();
                                esaida.status = "I";
                                esaida.dtinte = DateTime.Now.ToString("dd-MM-yy");
                            }

                            s_esaida.Update_EntidadeSaida(esaida);
                        }
                        else if (esaida.mptran == "I" && esaida.status == "A" && !esaida.json.Contains("categorias_liberadas"))
                        {
                            var ent = s_esaida.GetClienteCatByIDENT_EntidadeSaida(esaida.id_ent);
                            ClienteCategoria_ServicePOST cl_put = new ClienteCategoria_ServicePOST();
                            var resp = cl_put.IncluirClienteTodasCategorias(cmp.url, cmp.apptoken, cmp.cmptoken, esaida.json);

                            esaida.codret = ((int)resp.StatusCode).ToString();
                            if (esaida.codret != "200" && esaida.codret != "201")
                            {
                                ClienteCategoria_Service s_ = new ClienteCategoria_Service();
                                var entidade = s_.GetBySpecification_ClienteCategoria(int.Parse(esaida.id_ent));
                                entidade.mptran = "I";
                                s_.Update_ClienteCategoria(entidade);

                                esaida.status = "E";
                                if (resp.Content.ToString() == "")
                                {
                                    esaida.jsonret = resp.ErrorMessage;
                                }
                                else
                                {
                                    esaida.jsonret = resp.Content.ToString();
                                }
                            }
                            else
                            {
                                esaida.jsonret = resp.StatusDescription;
                                esaida.id_mp = resp.Headers[1].Value.ToString();
                                esaida.status = "I";
                                esaida.dtinte = DateTime.Now.ToString("dd-MM-yy");
                            }

                            s_esaida.Update_EntidadeSaida(esaida);
                        }
                    break;

                    case "ClienteTabPreco":
                        if (esaida.mptran == "I" && esaida.status == "A" && esaida.json.Contains("tabelas_liberadas"))
                        {
                            ClienteTabelaPreco_ServicePOST cl_post = new ClienteTabelaPreco_ServicePOST();
                            var resp = cl_post.IncluirClienteTabelaPreco(cmp.url, cmp.apptoken, cmp.cmptoken, esaida.json);

                            esaida.codret = ((int)resp.StatusCode).ToString();
                            if (esaida.codret != "200" && esaida.codret != "201")
                            {
                                ClienteTabelaPreco_Service s_ = new ClienteTabelaPreco_Service();
                                var entidade = s_.GetBySpecification_ClienteTabelaPreco(int.Parse(esaida.id_ent));
                                entidade.mptran = "I";
                                s_.Update_ClienteTabelaPreco(entidade);

                                esaida.status = "E";
                                if (resp.Content.ToString() == "")
                                {
                                    esaida.jsonret = resp.ErrorMessage;
                                }
                                else
                                {
                                    esaida.jsonret = resp.Content.ToString();
                                }
                            }
                            else
                            {
                                esaida.jsonret = resp.StatusDescription;
                                esaida.id_mp = resp.Headers[1].Value.ToString();
                                esaida.status = "I";
                                esaida.dtinte = DateTime.Now.ToString("dd-MM-yy");
                            }

                            s_esaida.Update_EntidadeSaida(esaida);
                        }
                        else if (esaida.mptran == "I" && esaida.status == "A" && !esaida.json.Contains("tabelas_liberadas"))
                        {
                            //var ent = s_esaida.GetByIDENT_EntidadeSaida(esaida.id_ent);
                            ClienteTabelaPreco_ServicePOST cl_put = new ClienteTabelaPreco_ServicePOST();
                            var resp = cl_put.IncluirClienteTodasTabelasPreco(cmp.url, cmp.apptoken, cmp.cmptoken, esaida.json);

                            esaida.codret = ((int)resp.StatusCode).ToString();
                            if (esaida.codret != "200" && esaida.codret != "201")
                            {
                                ClienteTabelaPreco_Service s_ = new ClienteTabelaPreco_Service();
                                var entidade = s_.GetBySpecification_ClienteTabelaPreco(int.Parse(esaida.id_ent));
                                entidade.mptran = "I";
                                s_.Update_ClienteTabelaPreco(entidade);

                                esaida.status = "E";
                                if (resp.Content.ToString() == "")
                                {
                                    esaida.jsonret = resp.ErrorMessage;
                                }
                                else
                                {
                                    esaida.jsonret = resp.Content.ToString();
                                }
                            }
                            else
                            {
                                esaida.jsonret = resp.StatusDescription;
                                esaida.id_mp = resp.Headers[1].Value.ToString();
                                esaida.status = "I";
                                esaida.dtinte = DateTime.Now.ToString("dd-MM-yy");
                            }

                            s_esaida.Update_EntidadeSaida(esaida);
                        }
                    break;

                    case "CondicaoPagamento":
                        if (esaida.mptran == "I" && esaida.status == "A")
                        {
                            CondicaoPagamento_ServicePOST ent_post = new CondicaoPagamento_ServicePOST();
                            var resp = ent_post.IncluirCondicaoPagamento(cmp.url, cmp.apptoken, cmp.cmptoken, esaida.json);

                            esaida.codret = ((int)resp.StatusCode).ToString();
                            if (esaida.codret != "200" && esaida.codret != "201")
                            {
                                CondicaoPagamento_Service s_ = new CondicaoPagamento_Service();
                                var entidade = s_.GetBySpecification_CondicaoPagamento(int.Parse(esaida.id_ent));
                                entidade.mptran = "I";
                                s_.Update_CondicaoPagamento(entidade);

                                esaida.status = "E";
                                if (resp.Content.ToString() == "")
                                {
                                    esaida.jsonret = resp.ErrorMessage;
                                }
                                else
                                {
                                    esaida.jsonret = resp.Content.ToString();
                                }
                            }
                            else
                            {
                                esaida.jsonret = resp.StatusDescription;
                                esaida.id_mp = resp.Headers[1].Value.ToString();
                                esaida.status = "I";
                                esaida.dtinte = DateTime.Now.ToString("dd-MM-yy");
                            }

                            s_esaida.Update_EntidadeSaida(esaida);
                        }
                        else if (esaida.mptran == "A" && esaida.status == "A")
                        {
                            var ent = s_esaida.GetCondicaoPagByIDENT_EntidadeSaida(esaida.id_ent);
                            CondicaoPagamento_ServicePUT ent_put = new CondicaoPagamento_ServicePUT();
                            var resp = ent_put.AlterarCondicaoPagamento(cmp.url, cmp.apptoken, cmp.cmptoken, esaida.json, ent.id_mp);

                            esaida.codret = ((int)resp.StatusCode).ToString();
                            if (esaida.codret != "200" && esaida.codret != "201")
                            {
                                CondicaoPagamento_Service s_ = new CondicaoPagamento_Service();
                                var entidade = s_.GetBySpecification_CondicaoPagamento(int.Parse(esaida.id_ent));
                                entidade.mptran = "A";
                                s_.Update_CondicaoPagamento(entidade);

                                esaida.status = "E";
                                if (resp.Content.ToString() == "")
                                {
                                    esaida.jsonret = resp.ErrorMessage;
                                }
                                else
                                {
                                    esaida.jsonret = resp.Content.ToString();
                                }
                            }
                            else
                            {
                                esaida.jsonret = resp.StatusDescription;
                                esaida.id_mp = resp.Headers[1].Value.ToString();
                                esaida.status = "I";
                                esaida.dtinte = DateTime.Now.ToString("dd-MM-yy");
                            }

                            s_esaida.Update_EntidadeSaida(esaida);
                        }
                    break;

                    case "FormaPagamento":
                        if (esaida.mptran == "I" && esaida.status == "A")
                        {
                            FormaPagamento_ServicePOST ent_post = new FormaPagamento_ServicePOST();
                            var resp = ent_post.IncluirFormaPagamento(cmp.url, cmp.apptoken, cmp.cmptoken, esaida.json);

                            esaida.codret = ((int)resp.StatusCode).ToString();
                            if (esaida.codret != "200" && esaida.codret != "201")
                            {
                                FormaPagamento_Service s_ = new FormaPagamento_Service();
                                var entidade = s_.GetBySpecification_FormaPagamento(int.Parse(esaida.id_ent));
                                entidade.mptran = "I";
                                s_.Update_FormaPagamento(entidade);

                                esaida.status = "E";
                                if (resp.Content.ToString() == "")
                                {
                                    esaida.jsonret = resp.ErrorMessage;
                                }
                                else
                                {
                                    esaida.jsonret = resp.Content.ToString();
                                }
                            }
                            else
                            {
                                esaida.jsonret = resp.StatusDescription;
                                esaida.id_mp = resp.Headers[1].Value.ToString();
                                esaida.status = "I";
                                esaida.dtinte = DateTime.Now.ToString("dd-MM-yy");
                            }

                            s_esaida.Update_EntidadeSaida(esaida);
                        }
                        else if (esaida.mptran == "A" && esaida.status == "A")
                        {
                            var ent = s_esaida.GetFormaPagByIDENT_EntidadeSaida(esaida.id_ent);
                            FormaPagamento_ServicePUT ent_put = new FormaPagamento_ServicePUT();
                            var resp = ent_put.AlterarFormaPagamento(cmp.url, cmp.apptoken, cmp.cmptoken, esaida.json, ent.id_mp);

                            esaida.codret = ((int)resp.StatusCode).ToString();
                            if (esaida.codret != "200" && esaida.codret != "201")
                            {
                                FormaPagamento_Service s_ = new FormaPagamento_Service();
                                var entidade = s_.GetBySpecification_FormaPagamento(int.Parse(esaida.id_ent));
                                entidade.mptran = "A";
                                s_.Update_FormaPagamento(entidade);

                                esaida.status = "E";
                                if (resp.Content.ToString() == "")
                                {
                                    esaida.jsonret = resp.ErrorMessage;
                                }
                                else
                                {
                                    esaida.jsonret = resp.Content.ToString();
                                }
                            }
                            else
                            {
                                esaida.jsonret = resp.StatusDescription;
                                esaida.id_mp = resp.Headers[1].Value.ToString();
                                esaida.status = "I";
                                esaida.dtinte = DateTime.Now.ToString("dd-MM-yy");
                            }

                            s_esaida.Update_EntidadeSaida(esaida);
                        }
                    break;

                    case "ICMSST":
                        if (esaida.mptran == "I" && esaida.status == "A")
                        {
                            ICMSST_ServicePOST ent_post = new ICMSST_ServicePOST();
                            var resp = ent_post.IncluirConfigICMSST(cmp.url, cmp.apptoken, cmp.cmptoken, esaida.json);

                            esaida.codret = ((int)resp.StatusCode).ToString();
                            if (esaida.codret != "200" && esaida.codret != "201")
                            {
                                ICMS_ST_Service s_ = new ICMS_ST_Service();
                                var entidade = s_.GetBySpecification_ICMS_ST(int.Parse(esaida.id_ent));
                                entidade.mptran = "I";
                                s_.Update_ICMS_ST(entidade);

                                esaida.status = "E";
                                if (resp.Content.ToString() == "")
                                {
                                    esaida.jsonret = resp.ErrorMessage;
                                }
                                else
                                {
                                    esaida.jsonret = resp.Content.ToString();
                                }
                            }
                            else
                            {
                                esaida.jsonret = resp.StatusDescription;
                                esaida.id_mp = resp.Headers[1].Value.ToString();
                                esaida.status = "I";
                                esaida.dtinte = DateTime.Now.ToString("dd-MM-yy");
                            }

                            s_esaida.Update_EntidadeSaida(esaida);
                        }
                        else if (esaida.mptran == "A" && esaida.status == "A")
                        {
                            var ent = s_esaida.GetICMSByIDENT_EntidadeSaida(esaida.id_ent);
                            ICMSST_ServicePUT ent_put = new ICMSST_ServicePUT();
                            var resp = ent_put.AlterarConfigICMSST(cmp.url, cmp.apptoken, cmp.cmptoken, esaida.json, ent.id_mp);

                            esaida.codret = ((int)resp.StatusCode).ToString();
                            if (esaida.codret != "200" && esaida.codret != "201")
                            {
                                ICMS_ST_Service s_ = new ICMS_ST_Service();
                                var entidade = s_.GetBySpecification_ICMS_ST(int.Parse(esaida.id_ent));
                                entidade.mptran = "A";
                                s_.Update_ICMS_ST(entidade);

                                esaida.status = "E";
                                if (resp.Content.ToString() == "")
                                {
                                    esaida.jsonret = resp.ErrorMessage;
                                }
                                else
                                {
                                    esaida.jsonret = resp.Content.ToString();
                                }
                            }
                            else
                            {
                                esaida.jsonret = resp.StatusDescription;
                                esaida.id_mp = resp.Headers[1].Value.ToString();
                                esaida.status = "I";
                                esaida.dtinte = DateTime.Now.ToString("dd-MM-yy");
                            }

                            s_esaida.Update_EntidadeSaida(esaida);
                        }
                    break;

                    case "Pedido":
                        if (esaida.mptran == "I" && esaida.status == "A")
                        {
                            Pedido_ServicePOST ent_post = new Pedido_ServicePOST();
                            var resp = ent_post.IncluirPedido(cmp.url, cmp.apptoken, cmp.cmptoken, esaida.json);

                            esaida.codret = ((int)resp.StatusCode).ToString();
                            if (esaida.codret != "200" && esaida.codret != "201")
                            {
                                Pedido_Service s_ = new Pedido_Service();
                                var entidade = s_.GetBySpecification_Pedido(int.Parse(esaida.id_ent));
                                entidade.mptran = "I";
                                s_.Update_Pedido(entidade);

                                esaida.json = esaida.json.Replace("\r\n", "");
                                esaida.json = esaida.json.Replace(" ", "");
                                esaida.status = "E";
                                if (resp.Content.ToString() == "")
                                {
                                    esaida.jsonret = resp.ErrorMessage;
                                }
                                else
                                {
                                    esaida.jsonret = resp.Content.ToString();
                                }
                            }
                            else
                            {
                                esaida.json = esaida.json.Replace("\r\n", "");
                                esaida.json = esaida.json.Replace(" ", "");
                                esaida.jsonret = resp.StatusDescription;
                                esaida.id_mp = resp.Headers[0].Value.ToString();
                                esaida.status = "I";
                                esaida.dtinte = DateTime.Now.ToString("dd-MM-yy");
                            }

                            s_esaida.Update_EntidadeSaida(esaida);
                        }
                        else if (esaida.mptran == "A" && esaida.status == "A")
                        {
                           JObject jsonO = JObject.Parse(esaida.json);

                            JArray itensp = (JArray)jsonO["itens"];
                            itensp.Descendants()
                                .OfType<JProperty>()
                                .Where(attr => attr.Name.StartsWith("descontos") && !attr.Value.HasValues)
                                .ToList()
                                .ForEach(attr => attr.Remove()); // removing unwanted attributes
                            esaida.json = itensp.ToString(); // backing result to json

                            jsonO.SelectToken("itens").Replace(JArray.Parse(esaida.json));
                            esaida.json = jsonO.ToString();

                            //Correr jarray com mais posições (itens)
                            string moeda = (string)jsonO.SelectToken("itens[0].moeda");
                            if (moeda == "0")
                            {
                                itensp.Descendants()
                                .OfType<JProperty>()
                                .Where(attr => attr.Name.StartsWith("moeda") && !attr.Value.HasValues)
                                .ToList()
                                .ForEach(attr => attr.Remove()); // removing unwanted attributes
                                esaida.json = itensp.ToString(); // backing result to json

                                itensp.Descendants()
                                .OfType<JProperty>()
                                .Where(attr => attr.Name.StartsWith("cotacao_moeda"))
                                .ToList()
                                .ForEach(attr => attr.Remove()); // removing unwanted attributes
                                esaida.json = itensp.ToString(); // backing result to json


                                jsonO.SelectToken("itens").Replace(JArray.Parse(esaida.json));
                                esaida.json = jsonO.ToString();
                            }

                            var ent = s_esaida.GetPedidoByIDENT_EntidadeSaida(esaida.id_ent);
                            Pedido_ServicePUT ent_put = new Pedido_ServicePUT();
                            var resp = ent_put.AlterarPedido(cmp.url, cmp.apptoken, cmp.cmptoken, esaida.json, ent.id_mp);

                            esaida.codret = ((int)resp.StatusCode).ToString();
                            if (esaida.codret != "200" && esaida.codret != "201")
                            {
                                Pedido_Service s_ = new Pedido_Service();
                                var entidade = s_.GetBySpecification_Pedido(int.Parse(esaida.id_ent));
                                entidade.mptran = "A";
                                s_.Update_Pedido(entidade);

                                esaida.json = esaida.json.Replace("\r\n", "");
                                esaida.json = esaida.json.Replace(" ", "");
                                esaida.status = "E";
                                if (resp.Content.ToString() == "")
                                {
                                    esaida.jsonret = resp.ErrorMessage;
                                }
                                else
                                {
                                    esaida.jsonret = resp.Content.ToString();
                                }
                            }
                            else
                            {
                                esaida.json = esaida.json.Replace("\r\n", "");
                                esaida.json = esaida.json.Replace(" ", "");
                                esaida.jsonret = resp.Headers[1].Value.ToString() + " [Response] Status Description: " + resp.StatusDescription.ToString();
                                esaida.id_mp = resp.Headers[0].Value.ToString();
                                esaida.status = "I";
                                esaida.dtinte = DateTime.Now.ToString("dd-MM-yy");
                            }

                            s_esaida.Update_EntidadeSaida(esaida);
                        }
                    break;

                    case "PedidoCExtra":
                        if (esaida.mptran == "I" && esaida.status == "A")
                        {
                            PedidoCampoExtra_ServicePOST ent_post = new PedidoCampoExtra_ServicePOST();
                            var resp = ent_post.IncluirPedidoCampoExtra(cmp.url, cmp.apptoken, cmp.cmptoken, esaida.json);

                            esaida.codret = ((int)resp.StatusCode).ToString();
                            if (esaida.codret != "200" && esaida.codret != "201")
                            {
                                PedidoCampoExtra_Service s_ = new PedidoCampoExtra_Service();
                                var entidade = s_.GetBySpecification_PedidoCampoExtra(int.Parse(esaida.id_ent));
                                entidade.mptran = "I";
                                s_.Update_PedidoCampoExtra(entidade);

                                esaida.status = "E";
                                if (resp.Content.ToString() == "")
                                {
                                    esaida.jsonret = resp.ErrorMessage;
                                }
                                else
                                {
                                    esaida.jsonret = resp.Content.ToString();
                                }
                            }
                            else
                            {
                                esaida.jsonret = resp.StatusDescription;
                                esaida.id_mp = resp.Headers[1].Value.ToString();
                                esaida.status = "I";
                                esaida.dtinte = DateTime.Now.ToString("dd-MM-yy");
                            }

                            s_esaida.Update_EntidadeSaida(esaida);
                        }
                        else if (esaida.mptran == "A" && esaida.status == "A")
                        {
                            var ent = s_esaida.GetPedidoCampoExtraByIDENT_EntidadeSaida(esaida.id_ent);
                            PedidoCampoExtra_ServicePUT ent_put = new PedidoCampoExtra_ServicePUT();
                            var resp = ent_put.AlterarPedidoCampoExtra(cmp.url, cmp.apptoken, cmp.cmptoken, esaida.json, ent.id_mp);

                            esaida.codret = ((int)resp.StatusCode).ToString();
                            if (esaida.codret != "200" && esaida.codret != "201")
                            {
                                PedidoCampoExtra_Service s_ = new PedidoCampoExtra_Service();
                                var entidade = s_.GetBySpecification_PedidoCampoExtra(int.Parse(esaida.id_ent));
                                entidade.mptran = "A";
                                s_.Update_PedidoCampoExtra(entidade);

                                esaida.status = "E";
                                if (resp.Content.ToString() == "")
                                {
                                    esaida.jsonret = resp.ErrorMessage;
                                }
                                else
                                {
                                    esaida.jsonret = resp.Content.ToString();
                                }
                            }
                            else
                            {
                                esaida.jsonret = resp.StatusDescription;
                                esaida.id_mp = resp.Headers[1].Value.ToString();
                                esaida.status = "I";
                                esaida.dtinte = DateTime.Now.ToString("dd-MM-yy");
                            }

                            s_esaida.Update_EntidadeSaida(esaida);
                        }
                    break;

                    case "PedidoFat":
                        if (esaida.mptran == "I" && esaida.status == "A")
                        {
                            PedidoFaturamento_ServicePOST ent_post = new PedidoFaturamento_ServicePOST();
                            var resp = ent_post.IncluirPedidoFaturamento(cmp.url, cmp.apptoken, cmp.cmptoken, esaida.json);

                            esaida.codret = ((int)resp.StatusCode).ToString();
                            if (esaida.codret != "200" && esaida.codret != "201")
                            {
                                PedidoFaturamento_Service s_ = new PedidoFaturamento_Service();
                                var entidade = s_.GetBySpecification_PedidoFaturamento(int.Parse(esaida.id_ent));
                                entidade.mptran = "I";
                                s_.Update_PedidoFaturamento(entidade);

                                esaida.json = esaida.json.Replace("\r\n", "");
                                esaida.json = esaida.json.Replace(" ", "");
                                esaida.status = "E";
                                if (resp.Content.ToString() == "")
                                {
                                    esaida.jsonret = resp.ErrorMessage;
                                }
                                else
                                {
                                    esaida.jsonret = resp.Content.ToString();
                                }
                            }
                            else
                            {
                                esaida.json = esaida.json.Replace("\r\n", "");
                                esaida.json = esaida.json.Replace(" ", "");
                                esaida.jsonret = resp.StatusDescription;
                                esaida.id_mp = resp.Headers[1].Value.ToString();
                                esaida.status = "I";
                                esaida.dtinte = DateTime.Now.ToString("dd-MM-yy");
                            }

                            s_esaida.Update_EntidadeSaida(esaida);
                        }
                        else if (esaida.mptran == "A" && esaida.status == "A")
                        {
                            var ent = s_esaida.GetPedidoFatByIDENT_EntidadeSaida(esaida.id_ent);
                            PedidoFaturamento_ServicePUT ent_put = new PedidoFaturamento_ServicePUT();
                            var resp = ent_put.AlterarPedidoFaturamento(cmp.url, cmp.apptoken, cmp.cmptoken, esaida.json, ent.id_mp);

                            esaida.codret = ((int)resp.StatusCode).ToString();
                            if (esaida.codret != "200" && esaida.codret != "201")
                            {
                                PedidoFaturamento_Service s_ = new PedidoFaturamento_Service();
                                var entidade = s_.GetBySpecification_PedidoFaturamento(int.Parse(esaida.id_ent));
                                entidade.mptran = "A";
                                s_.Update_PedidoFaturamento(entidade);

                                esaida.json = esaida.json.Replace("\r\n", "");
                                esaida.json = esaida.json.Replace(" ", "");
                                esaida.status = "E";
                                if (resp.Content.ToString() == "")
                                {
                                    esaida.jsonret = resp.ErrorMessage;
                                }
                                else
                                {
                                    esaida.jsonret = resp.Content.ToString();
                                }
                            }
                            else
                            {
                                esaida.json = esaida.json.Replace("\r\n", "");
                                esaida.json = esaida.json.Replace(" ", "");
                                esaida.jsonret = resp.StatusDescription;
                                esaida.id_mp = resp.Headers[1].Value.ToString();
                                esaida.status = "I";
                                esaida.dtinte = DateTime.Now.ToString("dd-MM-yy");
                            }

                            s_esaida.Update_EntidadeSaida(esaida);
                        }
                    break;

                    case "Produto":
                        if (esaida.mptran == "I" && esaida.status == "A")
                        {
                            Produto_ServicePOST ent_post = new Produto_ServicePOST();
                            var resp = ent_post.IncluirProduto(cmp.url, cmp.apptoken, cmp.cmptoken, esaida.json);

                            esaida.codret = ((int)resp.StatusCode).ToString();
                            if (esaida.codret != "200" && esaida.codret != "201")
                            {
                                Produto_Service s_ = new Produto_Service();
                                var entidade = s_.GetBySpecification_Produto(int.Parse(esaida.id_ent));
                                entidade.mptran = "I";
                                s_.Update_Produto(entidade);

                                esaida.status = "E";
                                if (resp.Content.ToString() == "")
                                {
                                    esaida.jsonret = resp.ErrorMessage;
                                }
                                else
                                {
                                    esaida.jsonret = resp.Content.ToString();
                                }
                            }
                            else
                            {
                                esaida.jsonret = resp.StatusDescription;
                                esaida.id_mp = resp.Headers[1].Value.ToString();
                                esaida.status = "I";
                                esaida.dtinte = DateTime.Now.ToString("dd-MM-yy");
                            }

                            s_esaida.Update_EntidadeSaida(esaida);
                        }
                        else if (esaida.mptran == "A" && esaida.status == "A")
                        {
                            var ent = s_esaida.GetProdutoByIDENT_EntidadeSaida(esaida.id_ent);
                            Produto_ServicePUT ent_put = new Produto_ServicePUT();
                            var resp = ent_put.AlterarProduto(cmp.url, cmp.apptoken, cmp.cmptoken, esaida.json, ent.id_mp);

                            esaida.codret = ((int)resp.StatusCode).ToString();
                            if (esaida.codret != "200" && esaida.codret != "201")
                            {
                                Produto_Service s_ = new Produto_Service();
                                var entidade = s_.GetBySpecification_Produto(int.Parse(esaida.id_ent));
                                entidade.mptran = "A";
                                s_.Update_Produto(entidade);

                                esaida.status = "E";
                                if (resp.Content.ToString() == "")
                                {
                                    esaida.jsonret = resp.ErrorMessage;
                                }
                                else
                                {
                                    esaida.jsonret = resp.Content.ToString();
                                }
                            }
                            else
                            {
                                esaida.jsonret = resp.StatusDescription;
                                esaida.id_mp = resp.Headers[1].Value.ToString();
                                esaida.status = "I";
                                esaida.dtinte = DateTime.Now.ToString("dd-MM-yy");
                            }

                            s_esaida.Update_EntidadeSaida(esaida);
                        }
                    break;

                    case "CategoriaProduto":
                        if (esaida.mptran == "I" && esaida.status == "A")
                        {
                            ProdutoCategoria_ServicePOST ent_post = new ProdutoCategoria_ServicePOST();
                            var resp = ent_post.IncluirProdutoCategoria(cmp.url, cmp.apptoken, cmp.cmptoken, esaida.json);

                            esaida.codret = ((int)resp.StatusCode).ToString();
                            if (esaida.codret != "200" && esaida.codret != "201")
                            {
                                ProdutoCategoria_Service s_ = new ProdutoCategoria_Service();
                                var entidade = s_.GetBySpecification_ProdutoCategoria(int.Parse(esaida.id_ent));
                                entidade.mptran = "I";
                                s_.Update_ProdutoCategoria(entidade);

                                esaida.status = "E";
                                if (resp.Content.ToString() == "")
                                {
                                    esaida.jsonret = resp.ErrorMessage;
                                }
                                else
                                {
                                    esaida.jsonret = resp.Content.ToString();
                                }
                            }
                            else
                            {
                                esaida.jsonret = resp.StatusDescription;
                                esaida.id_mp = resp.Headers[1].Value.ToString();
                                esaida.status = "I";
                                esaida.dtinte = DateTime.Now.ToString("dd-MM-yy");
                            }

                            s_esaida.Update_EntidadeSaida(esaida);
                        }
                        else if (esaida.mptran == "A" && esaida.status == "A")
                        {
                            var ent = s_esaida.GetProdutoCatByIDENT_EntidadeSaida(esaida.id_ent);
                            ProdutoCategoria_ServicePUT ent_put = new ProdutoCategoria_ServicePUT();
                            var resp = ent_put.AlterarProdutoCategoria(cmp.url, cmp.apptoken, cmp.cmptoken, esaida.json, ent.id_mp);

                            esaida.codret = ((int)resp.StatusCode).ToString();
                            if (esaida.codret != "200" && esaida.codret != "201")
                            {
                                ProdutoCategoria_Service s_ = new ProdutoCategoria_Service();
                                var entidade = s_.GetBySpecification_ProdutoCategoria(int.Parse(esaida.id_ent));
                                entidade.mptran = "A";
                                s_.Update_ProdutoCategoria(entidade);

                                esaida.status = "E";
                                if (resp.Content.ToString() == "")
                                {
                                    esaida.jsonret = resp.ErrorMessage;
                                }
                                else
                                {
                                    esaida.jsonret = resp.Content.ToString();
                                }
                            }
                            else
                            {
                                esaida.jsonret = resp.StatusDescription;
                                esaida.id_mp = resp.Headers[1].Value.ToString();
                                esaida.status = "I";
                                esaida.dtinte = DateTime.Now.ToString("dd-MM-yy");
                            }

                            s_esaida.Update_EntidadeSaida(esaida);
                        }
                    break;

                    case "ProdTabelaPreco":
                        if (esaida.mptran == "I" && esaida.status == "A")
                        {
                            ProdutoTabelaPreco_ServicePOST ent_post = new ProdutoTabelaPreco_ServicePOST();
                            var resp = ent_post.IncluirProdutoTabelaPreco(cmp.url, cmp.apptoken, cmp.cmptoken, esaida.json);

                            esaida.codret = ((int)resp.StatusCode).ToString();
                            if(esaida.codret != "200" && esaida.codret != "201")
                            {
                                ProdutoTabelaPreco_Service s_ = new ProdutoTabelaPreco_Service();
                                var entidade = s_.GetBySpecification_ProdutoTabelaPreco(int.Parse(esaida.id_ent));
                                entidade.mptran = "I";
                                s_.Update_ProdutoTabelaPreco(entidade);

                                esaida.status = "E";
                                if (resp.Content.ToString() == "")
                                {
                                    esaida.jsonret = resp.ErrorMessage;
                                }
                                else
                                {
                                    esaida.jsonret = resp.Content.ToString();
                                }
                            }
                            else
                            {
                                esaida.jsonret = resp.StatusDescription;
                                esaida.id_mp = resp.Headers[1].Value.ToString();
                                esaida.status = "I";
                                esaida.dtinte = DateTime.Now.ToString("dd-MM-yy");
                            }
                            
                            s_esaida.Update_EntidadeSaida(esaida);
                        }
                        else if (esaida.mptran == "A" && esaida.status == "A")
                        {
                            var ent = s_esaida.GetProdutoTabPrecoByIDENT_EntidadeSaida(esaida.id_ent);
                            ProdutoTabelaPreco_ServicePUT ent_put = new ProdutoTabelaPreco_ServicePUT();
                            var resp = ent_put.AlterarProdutoTabelaPreco(cmp.url, cmp.apptoken, cmp.cmptoken, esaida.json, ent.id_mp);

                            esaida.codret = ((int)resp.StatusCode).ToString();
                            if (esaida.codret != "200" && esaida.codret != "201")
                            {
                                ProdutoTabelaPreco_Service s_ = new ProdutoTabelaPreco_Service();
                                var entidade = s_.GetBySpecification_ProdutoTabelaPreco(int.Parse(esaida.id_ent));
                                entidade.mptran = "A";
                                s_.Update_ProdutoTabelaPreco(entidade);

                                esaida.status = "E";
                                if (resp.Content.ToString() == "")
                                {
                                    esaida.jsonret = resp.ErrorMessage;
                                }
                                else
                                {
                                    esaida.jsonret = resp.Content.ToString();
                                }
                            }
                            else
                            {
                                esaida.jsonret = resp.StatusDescription;
                                esaida.id_mp = resp.Headers[1].Value.ToString();
                                esaida.status = "I";
                                esaida.dtinte = DateTime.Now.ToString("dd-MM-yy");
                            }

                            s_esaida.Update_EntidadeSaida(esaida);
                        }
                    break;

                    case "RegraLiberacao":
                        if (esaida.mptran == "I" && esaida.status == "A")
                        {
                            RegraLiberacao_ServicePOST ent_post = new RegraLiberacao_ServicePOST();
                            var resp = ent_post.IncluirRegraLiberacao(cmp.url, cmp.apptoken, cmp.cmptoken, esaida.json);

                            esaida.codret = ((int)resp.StatusCode).ToString();
                            if (esaida.codret != "200" && esaida.codret != "201")
                            {
                                RegraLiberacao_Service s_ = new RegraLiberacao_Service();
                                var entidade = s_.GetBySpecification_RegraLiberacao(int.Parse(esaida.id_ent));
                                entidade.mptran = "I";
                                s_.Update_RegraLiberacao(entidade);

                                esaida.status = "E";
                                if (resp.Content.ToString() == "")
                                {
                                    esaida.jsonret = resp.ErrorMessage;
                                }
                                else
                                {
                                    esaida.jsonret = resp.Content.ToString();
                                }
                            }
                            else
                            {
                                esaida.jsonret = resp.StatusDescription;
                                esaida.id_mp = resp.Headers[1].Value.ToString();
                                esaida.status = "I";
                                esaida.dtinte = DateTime.Now.ToString("dd-MM-yy");
                            }

                            s_esaida.Update_EntidadeSaida(esaida);
                        }
                        /*
                        else if (esaida.mptran == "A" && esaida.status == "A")
                        {
                            var ent = s_esaida.GetByIDENT_EntidadeSaida(esaida.id_ent);
                            RegraL_ServicePUT prodcat_put = new RegraL_ServicePUT();
                            var resp = prodcat_put.AlterarProdutoTabelaPreco(cmp.url, cmp.apptoken, cmp.cmptoken, esaida.json, ent.id_mp);

                            esaida.codret = ((int)resp.StatusCode).ToString();
                            esaida.status = "I";
                            esaida.dtinte = DateTime.Now.ToString("dd-MM-yy");

                            s_esaida.Update_EntidadeSaida(esaida);
                        }*/
                    break;

                    case "TabelaPreco":
                        if (esaida.mptran == "I" && esaida.status == "A")
                        {
                            TabelaPreco_ServicePOST prodcat_post = new TabelaPreco_ServicePOST();
                            var resp = prodcat_post.IncluirTabelaPreco(cmp.url, cmp.apptoken, cmp.cmptoken, esaida.json);

                            esaida.codret = ((int)resp.StatusCode).ToString();
                            if (esaida.codret != "200" && esaida.codret != "201")
                            {
                                TabelaPreco_Service s_ = new TabelaPreco_Service();
                                var entidade = s_.GetBySpecification_TabelaPreco(int.Parse(esaida.id_ent));
                                entidade.mptran = "I";
                                s_.Update_TabelaPreco(entidade);

                                esaida.status = "E";
                                if (resp.Content.ToString() == "")
                                {
                                    esaida.jsonret = resp.ErrorMessage;
                                }
                                else
                                {
                                    esaida.jsonret = resp.Content.ToString();
                                }
                            }
                            else
                            {
                                esaida.jsonret = resp.StatusDescription;
                                esaida.id_mp = resp.Headers[1].Value.ToString();
                                esaida.status = "I";
                                esaida.dtinte = DateTime.Now.ToString("dd-MM-yy");
                            }

                            s_esaida.Update_EntidadeSaida(esaida);
                        }
                        else if (esaida.mptran == "A" && esaida.status == "A")
                        {
                            var ent = s_esaida.GetTabelaPrecoByIDENT_EntidadeSaida(esaida.id_ent);
                            TabelaPreco_ServicePUT ent_put = new TabelaPreco_ServicePUT();
                            var resp = ent_put.AlterarTabelaPreco(cmp.url, cmp.apptoken, cmp.cmptoken, esaida.json, ent.id_mp);

                            esaida.codret = ((int)resp.StatusCode).ToString();
                            if (esaida.codret != "200" && esaida.codret != "201")
                            {
                                TabelaPreco_Service s_ = new TabelaPreco_Service();
                                var entidade = s_.GetBySpecification_TabelaPreco(int.Parse(esaida.id_ent));
                                entidade.mptran = "A";
                                s_.Update_TabelaPreco(entidade);

                                esaida.status = "E";
                                if (resp.Content.ToString() == "")
                                {
                                    esaida.jsonret = resp.ErrorMessage;
                                }
                                else
                                {
                                    esaida.jsonret = resp.Content.ToString();
                                }
                            }
                            else
                            {
                                esaida.jsonret = resp.StatusDescription;
                                esaida.id_mp = resp.Headers[1].Value.ToString();
                                esaida.status = "I";
                                esaida.dtinte = DateTime.Now.ToString("dd-MM-yy");
                            }

                            s_esaida.Update_EntidadeSaida(esaida);
                        }
                    break;

                    case "TituloVencido":
                        if (esaida.mptran == "I" && esaida.status == "A")
                        {
                            TituloVencido_ServicePOST ent_post = new TituloVencido_ServicePOST();
                            var resp = ent_post.IncluirTituloVencido(cmp.url, cmp.apptoken, cmp.cmptoken, esaida.json);

                            esaida.codret = ((int)resp.StatusCode).ToString();
                            if (esaida.codret != "200" && esaida.codret != "201")
                            {
                                TituloVencido_Service s_ = new TituloVencido_Service();
                                var entidade = s_.GetBySpecification_TituloVencido(int.Parse(esaida.id_ent));
                                entidade.mptran = "I";
                                s_.Update_TituloVencido(entidade);

                                esaida.status = "E";
                                if (resp.Content.ToString() == "")
                                {
                                    esaida.jsonret = resp.ErrorMessage;
                                }
                                else
                                {
                                    esaida.jsonret = resp.Content.ToString();
                                }
                            }
                            else
                            {
                                esaida.jsonret = resp.StatusDescription;
                                esaida.id_mp = resp.Headers[1].Value.ToString();
                                esaida.status = "I";
                                esaida.dtinte = DateTime.Now.ToString("dd-MM-yy");
                            }

                            s_esaida.Update_EntidadeSaida(esaida);
                        }
                        else if (esaida.mptran == "A" && esaida.status == "A")
                        {
                            var ent = s_esaida.GetTituloVencidoByIDENT_EntidadeSaida(esaida.id_ent);
                            TituloVencido_ServicePUT ent_put = new TituloVencido_ServicePUT();
                            var resp = ent_put.AlterarTituloVencido(cmp.url, cmp.apptoken, cmp.cmptoken, esaida.json, ent.id_mp);

                            esaida.codret = ((int)resp.StatusCode).ToString();
                            if (esaida.codret != "200" && esaida.codret != "201")
                            {
                                TituloVencido_Service s_ = new TituloVencido_Service();
                                var entidade = s_.GetBySpecification_TituloVencido(int.Parse(esaida.id_ent));
                                entidade.mptran = "A";
                                s_.Update_TituloVencido(entidade);

                                esaida.status = "E";
                                if (resp.Content.ToString() == "")
                                {
                                    esaida.jsonret = resp.ErrorMessage;
                                }
                                else
                                {
                                    esaida.jsonret = resp.Content.ToString();
                                }
                            }
                            else
                            {
                                esaida.jsonret = resp.StatusDescription;
                                esaida.id_mp = resp.Headers[1].Value.ToString();
                                esaida.status = "I";
                                esaida.dtinte = DateTime.Now.ToString("dd-MM-yy");
                            }

                            s_esaida.Update_EntidadeSaida(esaida);
                        }
                    break;

                    case "Transportadora":
                        if (esaida.mptran == "I" && esaida.status == "A")
                        {
                            Transportadora_ServicePOST ent_post = new Transportadora_ServicePOST();
                            var resp = ent_post.IncluirTransportadora(cmp.url, cmp.apptoken, cmp.cmptoken, esaida.json);

                            esaida.codret = ((int)resp.StatusCode).ToString();
                            if (esaida.codret != "200" && esaida.codret != "201")
                            {
                                Transportadora_Service s_ = new Transportadora_Service();
                                var entidade = s_.GetBySpecification2_Transportadora(int.Parse(esaida.id_ent));
                                entidade.mptran = "I";
                                s_.Update_Transportadora(entidade);

                                esaida.status = "E";
                                if (resp.Content.ToString() == "")
                                {
                                    esaida.jsonret = resp.ErrorMessage;
                                }
                                else
                                {
                                    esaida.jsonret = resp.Content.ToString();
                                }
                            }
                            else
                            {
                                esaida.jsonret = resp.StatusDescription;
                                esaida.id_mp = resp.Headers[1].Value.ToString();
                                esaida.status = "I";
                                esaida.dtinte = DateTime.Now.ToString("dd-MM-yy");
                            }

                            s_esaida.Update_EntidadeSaida(esaida);
                        }
                        else if (esaida.mptran == "A" && esaida.status == "A")
                        {
                            var ent = s_esaida.GetTransportadoraByIDENT_EntidadeSaida(esaida.id_ent);
                            Transportadora_ServicePUT ent_put = new Transportadora_ServicePUT();
                            var resp = ent_put.AlterarTransportadora(cmp.url, cmp.apptoken, cmp.cmptoken, esaida.json, ent.id_mp);

                            esaida.codret = ((int)resp.StatusCode).ToString();
                            if (esaida.codret != "200" && esaida.codret != "201")
                            {
                                Transportadora_Service s_ = new Transportadora_Service();
                                var entidade = s_.GetBySpecification2_Transportadora(int.Parse(esaida.id_ent));
                                entidade.mptran = "A";
                                s_.Update_Transportadora(entidade);

                                esaida.status = "E";
                                if (resp.Content.ToString() == "")
                                {
                                    esaida.jsonret = resp.ErrorMessage;
                                }
                                else
                                {
                                    esaida.jsonret = resp.Content.ToString();
                                }
                            }
                            else
                            {
                                esaida.jsonret = resp.StatusDescription;
                                esaida.id_mp = resp.Headers[1].Value.ToString();
                                esaida.status = "I";
                                esaida.dtinte = DateTime.Now.ToString("dd-MM-yy");
                            }

                            s_esaida.Update_EntidadeSaida(esaida);
                        }
                    break;

                    default:
                        //DEFAULT CASE
                    break;
                }
            }
        }
    }
}

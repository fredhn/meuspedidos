﻿using System;
using Core;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Control.Validations;
using Newtonsoft.Json.Linq;

namespace Control
{
    public class SyncPedido
    {
        //ENTIDADES:
        Entidade_Saida es;

        //SERVIÇOS:
        ContaMeusPedidos_Service s_contamp = new ContaMeusPedidos_Service();
        Pedido_Service s_;
        CondicaoPagamento_Service s_condpag = new CondicaoPagamento_Service();
        FormaPagamento_Service s_fpag = new FormaPagamento_Service();
        EntidadeSaida_Service s_esaida = new EntidadeSaida_Service();

        //LISTAS:
        List<ContaMeusPedidos> listContaMP = new List<ContaMeusPedidos>();
        List<Pedido> listI;
        List<Pedido> listA;

        public void Execute()
        {
            s_ = new Pedido_Service();
            listContaMP = new List<ContaMeusPedidos>();
            listI = new List<Pedido>();
            listA = new List<Pedido>();

            listContaMP = s_contamp.GetAll_ContasMeusPedidosAtivas();
            listI = s_.GetAllByMPTran_Pedido("I");
            listA = s_.GetAllByMPTran_Pedido("A");

            foreach(var cmp in listContaMP)
            {
                if (cmp.status == "1") //Se a conta estiver ativa
                {
                    foreach (var cl in listI)
                    {
                        var es_cli = s_esaida.GetClienteByIDENT_EntidadeSaida(cl.cliente_id.ToString());
                        var es_transp = s_esaida.GetTransportadoraByIDENT_EntidadeSaida(cl.transportadora_id.ToString());
                        var es_condpag = s_esaida.GetCondicaoPagByIDENT_EntidadeSaida(cl.condicao_pagamento_id.ToString());
                        var es_fpag = s_esaida.GetFormaPagByIDENT_EntidadeSaida(cl.forma_pagamento_id.ToString());
                        
                        var condpag = s_condpag.GetBySpecification_CondicaoPagamento(int.Parse(es_condpag.id_ent));

                        PedidoJ pj = ConvertPedido_V.Do(cl, es_cli.id_mp, es_transp.id_mp, es_condpag.id_mp, es_fpag.id_mp);
                        string json = JsonConvert.SerializeObject(pj);

                        JObject jsonO = JObject.Parse(json);

                        JArray itensp = (JArray)jsonO["itens"];
                        itensp.Descendants()
                            .OfType<JProperty>()
                            .Where(attr => attr.Name.StartsWith("descontos") && !attr.Value.HasValues) 
                            .ToList() 
                            .ForEach(attr => attr.Remove()); // removing unwanted attributes
                            json = itensp.ToString(); // backing result to json

                        jsonO.SelectToken("itens").Replace(JArray.Parse(json));
                        json = jsonO.ToString();

                        //Correr jarray com mais posições (itens)
                        string moeda = (string)jsonO.SelectToken("itens[0].moeda");
                        if(moeda == "0")
                        {
                            itensp.Descendants()
                            .OfType<JProperty>()
                            .Where(attr => attr.Name.StartsWith("moeda") && !attr.Value.HasValues)
                            .ToList() 
                            .ForEach(attr => attr.Remove()); // removing unwanted attributes
                            json = itensp.ToString(); // backing result to json

                            itensp.Descendants()
                            .OfType<JProperty>()
                            .Where(attr => attr.Name.StartsWith("cotacao_moeda"))
                            .ToList() 
                            .ForEach(attr => attr.Remove()); // removing unwanted attributes
                            json = itensp.ToString(); // backing result to json


                            jsonO.SelectToken("itens").Replace(JArray.Parse(json));
                            json = jsonO.ToString();
                        }

                        es = new Entidade_Saida();
                        es.filial = cmp.filial;
                        es.id_contamp = cmp.id;
                        es.id_ent = cl.id.ToString();
                        es.mptran = "I";
                        es.tpreg = "Pedido";
                        es.json = json;
                        es.status = "A";

                        s_esaida.AddNew_EntidadeSaida(es);

                        s_ = new Pedido_Service();
                        var ped = s_.GetBySpecification_Pedido(cl.id);
                        ped.mptran = null;
                        s_.Update_Pedido(ped);
                    }

                    foreach (var cl in listA)
                    {
                        var es_cli = s_esaida.GetClienteByIDENT_EntidadeSaida(cl.cliente_id.ToString());
                        var es_transp = s_esaida.GetTransportadoraByIDENT_EntidadeSaida(cl.transportadora_id.ToString());
                        var es_condpag = s_esaida.GetCondicaoPagByIDENT_EntidadeSaida(cl.condicao_pagamento_id.ToString());
                        var es_fpag = s_esaida.GetFormaPagByIDENT_EntidadeSaida(cl.forma_pagamento_id.ToString());

                        var condpag = s_condpag.GetBySpecification_CondicaoPagamento(int.Parse(es_condpag.id_ent));

                        PedidoJ pj = ConvertPedido_V.Do(cl, es_cli.id_mp, es_transp.id_mp, es_condpag.id_mp, es_fpag.id_mp);
                        string json = JsonConvert.SerializeObject(pj);

                        JObject jsonO = JObject.Parse(json);

                        JArray itensp = (JArray)jsonO["itens"];
                        itensp.Descendants()
                            .OfType<JProperty>()
                            .Where(attr => attr.Name.StartsWith("descontos") && !attr.Value.HasValues)
                            .ToList()
                            .ForEach(attr => attr.Remove()); // removing unwanted attributes
                        json = itensp.ToString(); // backing result to json

                        jsonO.SelectToken("itens").Replace(JArray.Parse(json));
                        json = jsonO.ToString();

                        //Correr jarray com mais posições (itens)
                        string moeda = (string)jsonO.SelectToken("itens[0].moeda");
                        if (moeda == "0")
                        {
                            itensp.Descendants()
                            .OfType<JProperty>()
                            .Where(attr => attr.Name.StartsWith("moeda") && !attr.Value.HasValues)
                            .ToList()
                            .ForEach(attr => attr.Remove()); // removing unwanted attributes
                            json = itensp.ToString(); // backing result to json

                            itensp.Descendants()
                            .OfType<JProperty>()
                            .Where(attr => attr.Name.StartsWith("cotacao_moeda"))
                            .ToList()
                            .ForEach(attr => attr.Remove()); // removing unwanted attributes
                            json = itensp.ToString(); // backing result to json


                            jsonO.SelectToken("itens").Replace(JArray.Parse(json));
                            json = jsonO.ToString();
                        }

                        es = new Entidade_Saida();
                        es.id_contamp = cmp.id;
                        es.filial = cmp.filial;
                        es.id_ent = cl.id.ToString();
                        es.mptran = "A";
                        es.tpreg = "Pedido";
                        es.json = json;
                        es.status = "A";

                        s_esaida.AddNew_EntidadeSaida(es);

                        cl.mptran = null;
                        s_.Update_Pedido(cl);
                    }
                }
            }
        }//FIM .Execute();
    }
}

﻿using System;
using Core;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Control.Validations;

namespace Control
{
    public class SyncICMSST
    {
        //ENTIDADES:
        Entidade_Saida es;

        //SERVIÇOS:
        ContaMeusPedidos_Service s_contamp = new ContaMeusPedidos_Service();
        ICMS_ST_Service s_ = new ICMS_ST_Service();
        EntidadeSaida_Service s_esaida = new EntidadeSaida_Service();

        //LISTAS:
        List<ContaMeusPedidos> listContaMP = new List<ContaMeusPedidos>();
        List<ICMS_ST> listI = new List<ICMS_ST>();
        List<ICMS_ST> listA = new List<ICMS_ST>();

        public void Execute()
        {
            listContaMP = s_contamp.GetAll_ContasMeusPedidosAtivas();
            listI = s_.GetAllByMPTran_ICMS_ST("I");
            listA = s_.GetAllByMPTran_ICMS_ST("A");

            foreach(var cmp in listContaMP)
            {
                if (cmp.status == "1") //Se a conta estiver ativa
                {
                    foreach (var cl in listI)
                    {
                        ICMS_STJ icmsj = ConvertICMS_ST_V.Do(cl);
                        string json = JsonConvert.SerializeObject(icmsj);

                        es = new Entidade_Saida();
                        es.filial = cmp.filial;
                        es.id_contamp = cmp.id;
                        es.id_ent = cl.id.ToString();
                        es.mptran = "I";
                        es.tpreg = "ICMSST";
                        es.json = json;
                        es.status = "A";

                        s_esaida.AddNew_EntidadeSaida(es);

                        cl.mptran = null;
                        s_.Update_ICMS_ST(cl);
                    }

                    foreach (var cl in listA)
                    {
                        ICMS_STJ icmsj = ConvertICMS_ST_V.Do(cl);
                        string json = JsonConvert.SerializeObject(icmsj);

                        es = new Entidade_Saida();
                        es.id_contamp = cmp.id;
                        es.filial = cmp.filial;
                        es.id_ent = cl.id.ToString();
                        es.mptran = "A";
                        es.tpreg = "ICMSST";
                        es.json = json;
                        es.status = "A";

                        s_esaida.AddNew_EntidadeSaida(es);

                        cl.mptran = null;
                        s_.Update_ICMS_ST(cl);
                    }
                }
            }
        }//FIM .Execute();
    }
}

﻿using System;
using Core;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Control
{
    public class SyncClientes
    {
        //ENTIDADES:
        Entidade_Saida es;

        //SERVIÇOS:
        ContaMeusPedidos_Service s_contamp = new ContaMeusPedidos_Service();
        Cliente_Service s_cli = new Cliente_Service();
        EntidadeSaida_Service s_esaida = new EntidadeSaida_Service();

        //LISTAS:
        List<ContaMeusPedidos> listContaMP = new List<ContaMeusPedidos>();
        List<Cliente> listI = new List<Cliente>();
        List<Cliente> listA = new List<Cliente>();

        public void Execute()
        {
            try
            {
                listContaMP = s_contamp.GetAll_ContasMeusPedidosAtivas();
                listI = s_cli.GetAllByMPTran_FullClientes("I");
                listA = s_cli.GetAllByMPTran_FullClientes("A");

                foreach (var cmp in listContaMP)
                {
                    if (cmp.status == "1")
                    {
                        foreach (var cl in listI)
                        {
                            string json = JsonConvert.SerializeObject(cl);

                            es = new Entidade_Saida();
                            es.filial = cmp.filial;
                            es.id_contamp = cmp.id;
                            es.id_ent = cl.id.ToString();
                            es.mptran = "I";
                            es.tpreg = "Cliente";
                            es.json = json;
                            es.status = "A";

                            s_esaida.AddNew_EntidadeSaida(es);

                            cl.mptran = null;
                            s_cli.Update_Cliente(cl);
                        }

                        foreach (var cl in listA)
                        {
                            string json = JsonConvert.SerializeObject(cl);

                            es = new Entidade_Saida();
                            es.id_contamp = cmp.id;
                            es.filial = cmp.filial;
                            es.id_ent = cl.id.ToString();
                            es.mptran = "A";
                            es.tpreg = "Cliente";
                            es.json = json;
                            es.status = "A";

                            s_esaida.AddNew_EntidadeSaida(es);

                            cl.mptran = null;
                            s_cli.Update_Cliente(cl);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log_Service.WriteErrorLog(ex.Message);
                Log_Service.WriteErrorLog(ex.StackTrace);
                Log_Service.WriteErrorLog(ex);
                throw;
            }
        }//FIM .Execute();
    }
}

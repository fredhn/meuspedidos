﻿using System;
using Core;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Control.Validations;

namespace Control
{
    public class SyncCondicaoPagamento
    {
        //ENTIDADES:
        Entidade_Saida es;

        //SERVIÇOS:
        ContaMeusPedidos_Service s_contamp = new ContaMeusPedidos_Service();
        CondicaoPagamento_Service s_condpag = new CondicaoPagamento_Service();
        EntidadeSaida_Service s_esaida = new EntidadeSaida_Service();

        //LISTAS:
        List<ContaMeusPedidos> listContaMP = new List<ContaMeusPedidos>();
        List<CondicaoPagamento> listI = new List<CondicaoPagamento>();
        List<CondicaoPagamento> listA = new List<CondicaoPagamento>();

        public void Execute()
        {
            listContaMP = s_contamp.GetAll_ContasMeusPedidosAtivas();
            listI = s_condpag.GetAllByMPTran_CondicaoPagamento("I");
            listA = s_condpag.GetAllByMPTran_CondicaoPagamento("A");

            foreach(var cmp in listContaMP)
            {
                if (cmp.status == "1") //Se a conta estiver ativa
                {
                    foreach (var cl in listI)
                    {
                        CondicaoPagamentoJ cpj = ConvertCondicaoPagamento_V.Do(cl);
                        string json = JsonConvert.SerializeObject(cpj);

                        es = new Entidade_Saida();
                        es.filial = cmp.filial;
                        es.id_contamp = cmp.id;
                        es.id_ent = cl.id.ToString();
                        es.mptran = "I";
                        es.tpreg = "CondicaoPagamento";
                        es.json = json;
                        es.status = "A";

                        s_esaida.AddNew_EntidadeSaida(es);

                        cl.mptran = null;
                        s_condpag.Update_CondicaoPagamento(cl);
                    }

                    foreach (var cl in listA)
                    {
                        CondicaoPagamentoJ cpj = ConvertCondicaoPagamento_V.Do(cl);
                        string json = JsonConvert.SerializeObject(cl);

                        es = new Entidade_Saida();
                        es.id_contamp = cmp.id;
                        es.filial = cmp.filial;
                        es.id_ent = cl.id.ToString();
                        es.mptran = "A";
                        es.tpreg = "CondicaoPagamento";
                        es.json = json;
                        es.status = "A";

                        s_esaida.AddNew_EntidadeSaida(es);

                        cl.mptran = null;
                        s_condpag.Update_CondicaoPagamento(cl);
                    }
                }
            }
        }//FIM .Execute();
    }
}

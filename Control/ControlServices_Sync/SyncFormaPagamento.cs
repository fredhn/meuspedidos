﻿using System;
using Core;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Control.Validations;

namespace Control
{
    public class SyncFormaPagamento
    {
        //ENTIDADES:
        Entidade_Saida es;

        //SERVIÇOS:
        ContaMeusPedidos_Service s_contamp = new ContaMeusPedidos_Service();
        FormaPagamento_Service s_ = new FormaPagamento_Service();
        EntidadeSaida_Service s_esaida = new EntidadeSaida_Service();

        //LISTAS:
        List<ContaMeusPedidos> listContaMP = new List<ContaMeusPedidos>();
        List<FormaPagamento> listI = new List<FormaPagamento>();
        List<FormaPagamento> listA = new List<FormaPagamento>();

        public void Execute()
        {
            listContaMP = s_contamp.GetAll_ContasMeusPedidosAtivas();
            listI = s_.GetAllByMPTran_FormaPagamento("I");
            listA = s_.GetAllByMPTran_FormaPagamento("A");

            foreach(var cmp in listContaMP)
            {
                if (cmp.status == "1") //Se a conta estiver ativa
                {
                    foreach (var cl in listI)
                    {
                        FormaPagamentoJ fpj = ConvertFormaPagamento_V.Do(cl);
                        string json = JsonConvert.SerializeObject(fpj);

                        es = new Entidade_Saida();
                        es.filial = cmp.filial;
                        es.id_contamp = cmp.id;
                        es.id_ent = cl.id.ToString();
                        es.mptran = "I";
                        es.tpreg = "FormaPagamento";
                        es.json = json;
                        es.status = "A";

                        s_esaida.AddNew_EntidadeSaida(es);

                        cl.mptran = null;
                        s_.Update_FormaPagamento(cl);
                    }

                    foreach (var cl in listA)
                    {
                        FormaPagamentoJ fpj = ConvertFormaPagamento_V.Do(cl);
                        string json = JsonConvert.SerializeObject(fpj);

                        es = new Entidade_Saida();
                        es.id_contamp = cmp.id;
                        es.filial = cmp.filial;
                        es.id_ent = cl.id.ToString();
                        es.mptran = "A";
                        es.tpreg = "FormaPagamento";
                        es.json = json;
                        es.status = "A";

                        s_esaida.AddNew_EntidadeSaida(es);

                        cl.mptran = null;
                        s_.Update_FormaPagamento(cl);
                    }
                }
            }
        }//FIM .Execute();
    }
}

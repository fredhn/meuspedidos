﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Control.Validations;

namespace Control.Validations
{
    public class ConvertEmpresa_V
    {
        public static Empresa Do (EmpresaJ empJ)
        {
            Empresa emp = new Empresa();

            emp.cnpj = empJ.ZF1_CNPJ;
            emp.razao_social = empJ.ZF1_NOME;
            emp.comments = empJ.ZF2_DESCRI;
            emp.login = ConvertMD5_V.CalculateMD5Hash(empJ.ZF2_LOGIN);
            emp.password = ConvertMD5_V.CalculateMD5Hash(empJ.ZF2_SENHA);
            if (empJ.ZF1_ATIVO == "S")
            {
                emp.active = 1;
            }
            else
            {
                emp.active = 0;
            }

            return emp;
        }
    }
}

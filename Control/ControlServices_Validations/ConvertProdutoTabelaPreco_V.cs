﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control.Validations
{
    public class ConvertProdutoTabelaPreco_V
    {
        public static ProdutoTabelaPrecoJ Do (ProdutoTabelaPreco ptp, string id_prod, string id_tabelapreco)
        {
            ProdutoTabelaPrecoJ ptpj = new ProdutoTabelaPrecoJ();

            ptpj.produto_id = int.Parse(id_prod);
            ptpj.tabela_id = int.Parse(id_tabelapreco);
            ptpj.preco = ptp.preco;

            return ptpj;
        }
    }
}

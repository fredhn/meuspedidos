﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control.Validations
{
    public class ConvertClienteCategoria_V
    {
        public static ClienteCategoriaJ Do (ClienteCategoria ccp, string id_mp)
        {
            EntidadeSaida_Service s_esaida = new EntidadeSaida_Service();
            ClienteCategoriaJ ccpj = new ClienteCategoriaJ();
            List<int> ccplint = new List<int>();

            List<string> ccpl = ccp.categorias_liberadas.Split(new char[] { ',', ';', '/' }).Select(p => p.Trim()).Where(p => !string.IsNullOrWhiteSpace(p)).ToList();
            foreach (var ccplq in ccpl)
            {
                var a = s_esaida.GetProdutoCatByIDENT_EntidadeSaida(ccplq);
                ccplint.Add(int.Parse(a.id_mp));
            }

            ccpj.cliente_id = int.Parse(id_mp);
            ccpj.categorias_liberadas = ccplint;

            return ccpj;
        }
    }
}

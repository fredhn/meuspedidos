﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control.Validations
{
    public class ConvertRegraLiberacao_V
    {
        public static RegraLiberacaoJ Do (RegraLiberacao rl, string user_id, string cli_id)
        {
            RegraLiberacaoJ rlj = new RegraLiberacaoJ();

            rlj.usuario_id = int.Parse(user_id);
            rlj.cliente_id = int.Parse(cli_id);
            rlj.liberado = rl.liberado;

            return rlj;
        }
    }
}

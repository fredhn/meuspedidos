﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control.Validations
{
    public class ConvertProdutoCategoria_V
    {
        public static ProdutoCategoriaJ Do (ProdutoCategoria pc)
        {
            ProdutoCategoriaJ pcj = new ProdutoCategoriaJ();

            pcj.nome = pc.nome;
            pcj.excluido = pc.excluido;
            if(pc.categoria_pai_id != 0)
            {
                pcj.categoria_pai_id = pc.categoria_pai_id;
            }

            return pcj;
        }
    }
}

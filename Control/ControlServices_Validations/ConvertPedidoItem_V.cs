﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control.Validations
{
    public class ConvertPedidoItem_V
    {
        public static List<ItemPedidoJ> Do (List<ItemPedido> p)
        {
            List<ItemPedidoJ> list_itens = new List<ItemPedidoJ>();
            List<double> list_desc;
            ItemPedidoQ piq;
            ItemPedidoQG piqg;

            foreach (var ax in p)
            {
                list_desc = new List<double>();
                if (ax.quantidade == 0)
                {
                    piqg = new ItemPedidoQG();
                    piqg.produto_id = ax.produto_id;
                    piqg.tabela_preco_id = ax.tabela_preco_id;
                    piqg.quantidade_grades = ax.quantidade_grades;
                    piqg.preco_bruto = ax.preco_bruto;
                    piqg.preco_liquido = ax.preco_liquido;
                    piqg.preco_minimo = ax.preco_minimo;
                    piqg.observacoes = ax.observacoes;
                    piqg.st = ax.st;
                    if (ax.descontos != null)
                    {
                        var des_arr = ax.descontos.Split('/');
                        for (int i = 0; i < des_arr.Length; i++)
                        {
                            double d = double.Parse(des_arr[i]);
                            list_desc.Add(d);
                        }
                        piqg.descontos = list_desc;
                    }
                    piqg.ipi = ax.ipi;
                    piqg.tipo_ipi = ax.tipo_ipi;
                    piqg.moeda = ax.moeda;
                    piqg.cotacao_moeda = ax.cotacao_moeda;

                    list_itens.Add(piqg);
                }
                else
                {
                    piq = new ItemPedidoQ();
                    piq.produto_id = ax.produto_id;
                    piq.tabela_preco_id = ax.tabela_preco_id;
                    piq.quantidade = ax.quantidade;
                    piq.preco_bruto = ax.preco_bruto;
                    piq.preco_liquido = ax.preco_liquido;
                    piq.preco_minimo = ax.preco_minimo;
                    piq.observacoes = ax.observacoes;
                    piq.st = ax.st;
                    if (ax.descontos != null && ax.descontos != " / ")
                    {
                        var des_arr = ax.descontos.Split('/');
                        for (int i = 0; i < des_arr.Length; i++)
                        {
                            var x = des_arr[i].Normalize();
                            if (x != " ")
                            {
                                double d = double.Parse(des_arr[i].ToString());
                                if (d != 0)
                                {
                                    list_desc.Add(d);
                                }
                            }
                        }
                        piq.descontos = list_desc;
                    }
                    piq.ipi = ax.ipi;
                    piq.tipo_ipi = ax.tipo_ipi;
                    piq.moeda = ax.moeda;
                    piq.cotacao_moeda = ax.cotacao_moeda;

                    list_itens.Add(piq);
                }
            }
            

            return list_itens;
        }
    }
}

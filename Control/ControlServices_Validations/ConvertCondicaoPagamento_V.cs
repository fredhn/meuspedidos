﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control.Validations
{
    public class ConvertCondicaoPagamento_V
    {
        public static CondicaoPagamentoJ Do (CondicaoPagamento ccp)
        {
            CondicaoPagamentoJ cpj = new CondicaoPagamentoJ();

            cpj.nome = ccp.nome;
            cpj.valor_minimo = ccp.valor_minimo;
            cpj.excluido = ccp.excluido;

            return cpj;
        }
    }
}

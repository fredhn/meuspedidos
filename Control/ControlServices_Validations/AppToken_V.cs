﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management;
using Core;
using System.Configuration;
using System.Security.Cryptography;

namespace Control.Validations
{
    public static class AppToken_V
    {
        private static readonly byte[] Salt = new byte[] { 10, 20, 30, 40, 50, 60, 70, 80 };

        public static string Generate()
        {
            string appToken = "";

            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            string codpro = config.AppSettings.Settings["COD_PRO"].Value.ToString();
            string cnpj = config.AppSettings.Settings["CNPJ"].Value.ToString();

            string hdserial = HDSerial_V.GetHDSerialNumber();

            appToken = codpro + "-" + cnpj + "-" + Encryption_V.Encrypt(hdserial);

            var at = CreateKey(appToken);

            string str = "";

            foreach(var b in at)
            {
                str += b;
            }

            return str;
        }

        private static byte[] CreateKey(string key, int keyBytes = 8)
        {
            const int Iterations = 300;
            var keyGenerator = new Rfc2898DeriveBytes(key, Salt, Iterations);
            return keyGenerator.GetBytes(keyBytes);
        }

    }
}

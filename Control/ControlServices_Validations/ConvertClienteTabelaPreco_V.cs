﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control.Validations
{
    public class ConvertClienteTabelaPreco_V
    {
        public static ClienteTabelaPrecoJ Do (ClienteTabelaPreco ccp, string id_cliente)
        {
            EntidadeSaida_Service s_esaida = new EntidadeSaida_Service();
            ClienteTabelaPrecoJ ccpj = new ClienteTabelaPrecoJ();
            List<int> ccplint = new List<int>();

            List<string> ccpl = ccp.tabelas_liberadas.Split(new char[] { ',', ';', '/' }).Select(p => p.Trim()).Where(p => !string.IsNullOrWhiteSpace(p)).ToList();
            foreach (var ccplq in ccpl)
            {
                var a = s_esaida.GetTabelaPrecoByIDENT_EntidadeSaida(ccplq);
                ccplint.Add(int.Parse(a.id_mp));
            }

            ccpj.cliente_id = int.Parse(id_cliente);
            ccpj.tabelas_liberadas = ccplint;

            return ccpj;
        }
    }
}

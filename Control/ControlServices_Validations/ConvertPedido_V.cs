﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Control;

namespace Control.Validations
{
    public class ConvertPedido_V
    {
        public static PedidoJ Do (Pedido p, string cli_id, string transp_id, string condpag_id, string fpag_id)
        {
            PedidoJ pj = new PedidoJ();
            EntidadeSaida_Service s_es = new EntidadeSaida_Service();

            pj.cliente_id = int.Parse(cli_id);
            pj.data_emissao = p.data_emissao.ToString("yyyy-MM-dd");
            pj.transportadora_id = int.Parse(transp_id);
            pj.condicao_pagamento_id = int.Parse(condpag_id);
            pj.observacoes = p.observacoes;

            foreach (var prod in p.items)
            {
                var es_prod = s_es.GetProdutoByIDENT_EntidadeSaida(prod.produto_id.ToString());
                var es_tpreco = s_es.GetTabelaPrecoByIDENT_EntidadeSaida(prod.tabela_preco_id.ToString());

                if (es_prod != null)
                {
                    prod.produto_id = int.Parse(es_prod.id_mp);
                }

                if (es_tpreco != null)
                {
                    prod.tabela_preco_id = int.Parse(es_tpreco.id_mp);
                }

            }

            foreach (var extra in p.extras)
            {
                var es_extra = s_es.GetPedidoCampoExtraByIDENT_EntidadeSaida(extra.id.ToString());
                if (es_extra != null)
                {
                    extra.id = int.Parse(es_extra.id_mp);
                }
            }

            pj.extras = ConvertPedidoCampoExtra_V.Do(p.extras);
            pj.itens = ConvertPedidoItem_V.Do(p.items);

            return pj;
        }
    }
}

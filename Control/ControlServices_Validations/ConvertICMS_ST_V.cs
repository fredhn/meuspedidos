﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control.Validations
{
    public class ConvertICMS_ST_V
    {
        public static ICMS_STJ Do (ICMS_ST icms)
        {
            ICMS_STJ icmsj = new ICMS_STJ();

            icmsj.codigo_ncm = icms.codigo_ncm;
            icmsj.estado_destino = icms.estado_destino;
            icmsj.icms_credito = icms.icms_credito;
            icmsj.icms_destino = icms.icms_destino;
            icmsj.tipo_st = icms.tipo_st;
            icmsj.valor_mva = icms.valor_mva;
            icmsj.valor_pmc = icms.valor_pmc;
            icmsj.nome_excecao_fiscal = icms.nome_excecao_fiscal;

            return icmsj;
        }
    }
}

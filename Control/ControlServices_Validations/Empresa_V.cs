﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management;
using Core;
using System.Configuration;
using System.Security.Cryptography;

namespace Control.Validations
{
    public static class Empresa_V
    {
        public static bool ExecuteValidation()
        {
            bool ret = false;

            Empresa_Service s_empresa = new Empresa_Service();

            var empresas = s_empresa.GetAll_Empresas();

            foreach (var emp in empresas)
            {
                var dt_ult_validacao = Encryption_V.Decrypt(emp.ultima_validacao);
                DateTime dtuv = Convert.ToDateTime(dt_ult_validacao);
                if (dtuv.Date.AddDays(7) < DateTime.Now.Date)
                {
                    ret = true;
                }
            }

            if (empresas.Count == 0)
            {
                ret = true;
            }

            return ret;
        }
    }
}

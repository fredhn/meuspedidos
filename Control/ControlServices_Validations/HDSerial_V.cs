﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management;

namespace Control.Validations
{
    public static class HDSerial_V
    {
        public static string GetHDSerialNumber()
        {
            string hd_serial = "";

            ManagementClass mangnmt = new ManagementClass("Win32_LogicalDisk");
            ManagementObjectCollection mcol = mangnmt.GetInstances();

            foreach (ManagementObject strt in mcol)
            {
                hd_serial += Convert.ToString(strt["VolumeSerialNumber"]);
            }

            return hd_serial;
        }
    }
}

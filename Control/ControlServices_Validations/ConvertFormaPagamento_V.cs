﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control.Validations
{
    public class ConvertFormaPagamento_V
    {
        public static FormaPagamentoJ Do (FormaPagamento tp)
        {
            FormaPagamentoJ fpj = new FormaPagamentoJ();

            fpj.nome = tp.nome;
            fpj.excluido = tp.excluido;

            return fpj;
        }
    }
}

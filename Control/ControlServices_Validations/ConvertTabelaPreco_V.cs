﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control.Validations
{
    public class ConvertTabelaPreco_V
    {
        public static TabelaPrecoJ Do (TabelaPreco tp)
        {
            TabelaPrecoJ tpj = new TabelaPrecoJ();

            tpj.nome = tp.nome;
            tpj.tipo = tp.tipo;
            tpj.acrescimo = tp.acrescimo;
            tpj.desconto = tp.desconto;
            tpj.excluido = tp.excluido;

            return tpj;
        }
    }
}

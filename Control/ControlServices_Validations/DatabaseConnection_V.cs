﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Control.Validations
{
    public static class DatabaseConnection_V
    {
        public static bool IsServerConnected(string _cs)
        {
            using (var l_oConnection = new SqlConnection(_cs))
            {
                try
                {
                    l_oConnection.Open();
                    return true;
                }
                catch (SqlException)
                {
                    return false;
                }
            }
        }
    }
}

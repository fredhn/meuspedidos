﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control.Validations
{
    public class ConvertProduto_V
    {
        public static ProdutoJ Do (Produto p)
        {
            List<string> stringsC = new List<string>();
            List<string> stringsT = new List<string>();
            ProdutoJ pj = new ProdutoJ();
            foreach (var ax in p.grade_cores)
            {
                GradeCor gc = ax;
                stringsC.Add(gc.cor);
            }
            foreach (var ax2 in p.grade_tamanhos)
            {
                GradeTamanho gt = ax2;
                stringsT.Add(gt.tamanho);
            }
            pj.grade_cores = stringsC;
            pj.grade_tamanhos = stringsT;
            pj.ativo = p.ativo;
            pj.categoria_id = p.categoria_id;
            pj.codigo = p.codigo;
            pj.codigo_ncm = p.codigo_ncm;
            pj.comissao = p.comissao;
            pj.excluido = p.excluido;
            pj.ipi = p.ipi;
            pj.moeda = p.moeda;
            pj.multiplo = p.multiplo;
            pj.nome = p.nome;
            pj.observacoes = p.observacoes;
            pj.peso_bruto = p.peso_bruto;
            pj.preco_minimo = p.preco_minimo;
            pj.preco_tabela = p.preco_tabela;
            pj.saldo_estoque = p.saldo_estoque;
            pj.st = p.st;
            pj.tipo_ipi = p.tipo_ipi;
            pj.unidade = p.unidade;

            return pj;
        }
    }
}

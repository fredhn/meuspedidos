﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control.Validations
{
    public class ConvertTituloVencido_V
    {
        public static TituloVencidoJ Do (TituloVencido tv, string id_cliente)
        {
            TituloVencidoJ tvj = new TituloVencidoJ();

            tvj.cliente_id = int.Parse(id_cliente);
            tvj.data_vencimento = tv.data_vencimento.ToString("yyyy-MM-dd");
            tvj.numero_documento = tv.numero_documento;
            tvj.valor = tv.valor;
            tvj.observacao = tv.observacao;

            return tvj;
        }
    }
}

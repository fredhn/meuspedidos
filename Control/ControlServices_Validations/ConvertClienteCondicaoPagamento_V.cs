﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control.Validations
{
    public class ConvertClienteCondicaoPagamento_V
    {
        public static ClienteCondicaoPagamentoJ Do (ClienteCondicaoPagamento ccp, string id_mp)
        {
            EntidadeSaida_Service s_esaida = new EntidadeSaida_Service();
            ClienteCondicaoPagamentoJ ccpj = new ClienteCondicaoPagamentoJ();
            List<int> ccplint = new List<int>();

            List<string> ccpl = ccp.condicoes_pagamento_liberadas.Split(new char[] { ',', ';', '/' }).Select(p => p.Trim()).Where(p => !string.IsNullOrWhiteSpace(p)).ToList();
            foreach (var ccplq in ccpl)
            {
                var a = s_esaida.GetCondicaoPagByIDENT_EntidadeSaida(ccplq);
                ccplint.Add(int.Parse(a.id_mp));
            }

            ccpj.cliente_id = id_mp;
            ccpj.condicoes_pagamento_liberadas = ccplint;

            return ccpj;
        }
    }
}

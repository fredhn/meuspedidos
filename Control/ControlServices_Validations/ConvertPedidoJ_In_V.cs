﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Control;

namespace Control.Validations
{
    public class ConvertPedidoJ_In_V
    {
        public static Pedido Do (PedidoJ_In pin)
        {
            Pedido p = new Pedido();
            EntidadeSaida_Service s_es = new EntidadeSaida_Service();

            p.cliente_id = pin.cliente_id;
            p.cliente_bairro = pin.cliente_bairro;
            p.cliente_cep = pin.cliente_cep;
            p.cliente_cidade = pin.cliente_cidade;
            p.cliente_cnpj = pin.cliente_cnpj;
            p.cliente_complemento = pin.cliente_complemento;
            p.cliente_estado = pin.cliente_estado;
            p.cliente_inscricao_estadual = pin.cliente_inscricao_estadual;
            p.cliente_nome_fantasia = pin.cliente_nome_fantasia;
            if (pin.cliente_numero != null)
            {
                p.cliente_numero = int.Parse(pin.cliente_numero);
            }
            p.cliente_razao_social = pin.cliente_razao_social;
            p.cliente_rua = pin.cliente_rua;
            p.cliente_suframa = pin.cliente_suframa;
            p.data_emissao = pin.data_emissao;
            p.transportadora_id = pin.transportadora_id;
            p.condicao_pagamento_id = pin.condicao_pagamento_id;
            p.condicao_pagamento = pin.condicao_pagamento;
            p.observacoes = pin.observacoes;
            p.criador_id = pin.criador_id;
            p.forma_pagamento_id = pin.forma_pagamento_id;
            p.id = 0;
            p.nome_contato = pin.nome_contato;
            p.numero = pin.numero;
            p.representada_id = pin.representada_id;
            p.representada_nome_fantasia = pin.representada_nome_fantasia;
            p.representada_razao_social = pin.representada_razao_social;
            p.status = pin.status;
            p.status_faturamento = pin.status_faturamento;
            p.total = pin.total;
            p.ultima_alteracao = pin.ultima_alteracao;

            foreach (var prod in p.items)
            {
                var es_prod = s_es.GetProdutoByIDENT_EntidadeSaida(prod.produto_id.ToString());
                var es_tpreco = s_es.GetTabelaPrecoByIDENT_EntidadeSaida(prod.tabela_preco_id.ToString());

                if (es_prod != null)
                {
                    prod.produto_id = int.Parse(es_prod.id_mp);
                }

                if (es_tpreco != null)
                {
                    prod.tabela_preco_id = int.Parse(es_tpreco.id_mp);
                }

            }

            foreach (var extra in p.extras)
            {
                var es_extra = s_es.GetPedidoCampoExtraByIDENT_EntidadeSaida(extra.id.ToString());
                if (es_extra != null)
                {
                    extra.id = int.Parse(es_extra.id_mp);
                }
            }

            p.extras = pin.extras;

            List<ItemPedidoJ> lpin_items = pin.items;
            List<ItemPedido> lp_items = new List<ItemPedido>();
            ItemPedido itemp;
            string desc_string = String.Empty;

            foreach (var pinitem in lpin_items)
            {
                desc_string = String.Empty;
                itemp = new ItemPedido();
                foreach (var desc in pinitem.descontos)
                {
                    desc_string = desc_string + desc.ToString() + " / ";
                }
                itemp.descontos = desc_string;
                itemp.moeda = pinitem.moeda;
                itemp.cotacao_moeda = pinitem.cotacao_moeda;
                itemp.ipi = pinitem.ipi;
                itemp.observacoes = pinitem.observacoes;
                itemp.preco_bruto = pinitem.preco_bruto;
                itemp.preco_liquido = pinitem.preco_liquido;
                itemp.preco_minimo = pinitem.preco_minimo;
                itemp.produto_id = pinitem.produto_id;
                itemp.st = pinitem.st;
                itemp.tabela_preco_id = pinitem.tabela_preco_id;
                itemp.tipo_ipi = pinitem.tipo_ipi;

                lp_items.Add(itemp);
            }

            p.items = lp_items;

            return p;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control.Validations
{
    public class ConvertPedidoCampoExtra_V
    {
        public static List<PedidoCampoExtraJ> Do (List<PedidoCampoExtra> p)
        {
            List<PedidoCampoExtraJ> list_pcej = new List<PedidoCampoExtraJ>();
            PedidoCampoExtraJ pcej;

            foreach (var ax in p)
            {
                pcej = new PedidoCampoExtraJ();
                pcej.id = ax.id;
                pcej.valor = ax.valor;

                list_pcej.Add(pcej);
            }
            

            return list_pcej;
        }
    }
}

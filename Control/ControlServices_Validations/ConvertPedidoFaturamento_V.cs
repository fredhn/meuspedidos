﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Control.Validations
{
    public class ConvertPedidoFaturamento_V
    {
        public static PedidoFaturamentoJ Do (PedidoFaturamento p, string pedido_id)
        {
            PedidoFaturamentoJ pfatj = new PedidoFaturamentoJ();

            pfatj.pedido_id = int.Parse(pedido_id);
            pfatj.numero_nf = p.numero_nf;
            pfatj.valor_faturado = p.valor_faturado;
            pfatj.data_faturamento = p.data_faturamento.ToString("yyyy-MM-dd"); ;
            pfatj.informacoes_adicionais = p.informacoes_adicionais;
            pfatj.excluido = p.excluido;

            return pfatj;
        }
    }
}

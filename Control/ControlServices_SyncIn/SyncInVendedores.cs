﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Service_API;
using Newtonsoft.Json;

namespace Control
{
    public class SyncInVendedores
    {
        //ENTIDADES:
        Entidade_Entrada ee;

        //SERVIÇOS:
        ContaMeusPedidos_Service s_contamp = new ContaMeusPedidos_Service();
        RegraLiberacao_Service s_ = new RegraLiberacao_Service();
        EntidadeEntrada_Service s_eentrada = new EntidadeEntrada_Service();
        Vendedores_ServiceGET sget_vendedor = new Vendedores_ServiceGET();
        Vendedor_Service s_vendedor = new Vendedor_Service();

        //LISTAS:
        List<ContaMeusPedidos> listContaMP = new List<ContaMeusPedidos>();

        public void Execute()
        {
            listContaMP = s_contamp.GetAll_ContasMeusPedidosAtivas();
            DateTime dataEntS =  new DateTime();

            foreach (var cmp in listContaMP)
            {
                if (cmp.status == "1") //Se a conta estiver ativa
                {
                    var vendedores = sget_vendedor.TodosVendedores(cmp.url, cmp.apptoken, cmp.cmptoken);
                    if (vendedores.StatusCode.ToString() != "Unauthorized" && vendedores.Content != "")
                    {
                        Vendedor[] v = JsonConvert.DeserializeObject<Vendedor[]>(vendedores.Content);

                        foreach (Vendedor vq in v)
                        {
                            var ax = s_eentrada.GetVendedorByIDMP_EntidadeEntrada(vq.id.ToString());
                            if (ax != null)
                            {
                                dataEntS = Convert.ToDateTime(ax.dtinte);
                                //var dataEntS = DateTime.ParseExact(ax.dtinte, "dd / MM / yyyy HH: mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                            }

                            if (ax == null)
                            {
                                ee = new Entidade_Entrada();
                                ee.tpreg = "Vendedor";
                                ee.id_mp = vq.id.ToString();
                                ee.dtinte = vq.ultima_alteracao.ToString();
                                ee.json = JsonConvert.SerializeObject(vq);
                                ee.codret = ((int)vendedores.StatusCode).ToString();
                                ee.id_contamp = cmp.id;
                                ee.filial = cmp.filial;
                                ee.status = "I";

                                s_vendedor.AddNew_Vendedor(vq);
                                var q = s_vendedor.GetByNome_Vendedor(vq.nome);
                                ee.id_ent = q.id.ToString();
                                s_eentrada.AddNew_EntidadeEntrada(ee);
                            }
                            else if (vq.ultima_alteracao > dataEntS)
                            {
                                ee = new Entidade_Entrada();
                                ee.tpreg = "Vendedor";
                                ee.id_mp = vq.id.ToString();
                                ee.dtinte = vq.ultima_alteracao.ToString();
                                ee.json = JsonConvert.SerializeObject(vq);
                                ee.codret = ((int)vendedores.StatusCode).ToString();
                                ee.id_contamp = cmp.id;
                                ee.filial = cmp.filial;
                                ee.status = "A";

                                s_vendedor.Update_Vendedor(vq);
                                s_eentrada.Update_EntidadeEntrada(ee);
                            }
                        }
                    }
                }
            }
        }
    }
}

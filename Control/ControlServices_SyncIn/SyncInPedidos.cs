﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Service_API;
using Newtonsoft.Json;
using Control.Validations;

namespace Control
{
    public class SyncInPedidos
    {
        //ENTIDADES:
        Entidade_Entrada ee;

        //SERVIÇOS:
        ContaMeusPedidos_Service s_contamp = new ContaMeusPedidos_Service();
        EntidadeEntrada_Service s_eentrada = new EntidadeEntrada_Service();
        Pedido_ServiceGET sget_pedido = new Pedido_ServiceGET();
        PedidoCampoExtra_ServiceGET sget_pedidoce = new PedidoCampoExtra_ServiceGET();
        Pedido_Service s_ = new Pedido_Service();
        PedidoCampoExtra_Service s_ce = new PedidoCampoExtra_Service();

        //LISTAS:
        List<ContaMeusPedidos> listContaMP = new List<ContaMeusPedidos>();

        public void Execute()
        {
            listContaMP = s_contamp.GetAll_ContasMeusPedidosAtivas();
            DateTime dataEntS =  new DateTime();

            foreach (var cmp in listContaMP)
            {
                if (cmp.status == "1") //Se a conta estiver ativa
                {

                    var camposextras = sget_pedidoce.TodosPedidosCamposExtras(cmp.apptoken, cmp.cmptoken);
                    var pedidos = sget_pedido.TodosPedidos(cmp.apptoken, cmp.cmptoken);


                    if (camposextras.StatusCode.ToString() != "Unauthorized" && camposextras.Content != "")
                    {
                        PedidoCampoExtra[] pcejin_list = JsonConvert.DeserializeObject<PedidoCampoExtra[]>(camposextras.Content);
                        foreach (PedidoCampoExtra cein in pcejin_list)
                        {
                            var ax = s_eentrada.GetPedidoCampoExtraByIDMP_EntidadeEntrada(cein.id.ToString());
                            if (ax != null)
                            {
                                dataEntS = Convert.ToDateTime(ax.dtinte);
                                //var dataEntS = DateTime.ParseExact(ax.dtinte, "dd / MM / yyyy HH: mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                            }

                            if (ax == null)
                            {
                                ee = new Entidade_Entrada();
                                ee.tpreg = "PedidoCExtra";
                                ee.id_mp = cein.id.ToString();
                                ee.dtinte = DateTime.Now.ToString(); // DateTime.Now.Date;
                                ee.json = JsonConvert.SerializeObject(cein);
                                ee.codret = ((int)pedidos.StatusCode).ToString();
                                ee.id_contamp = cmp.id;
                                ee.filial = cmp.filial;
                                ee.status = "I";

                                s_ce.AddNew_PedidoCampoExtra(cein);
                                var q = s_ce.GetByMaxId_PedidoCampoExtra();
                                ee.id_ent = q.id.ToString();
                                s_eentrada.AddNew_EntidadeEntrada(ee);
                            }
                        }
                    }

                    if (pedidos.StatusCode.ToString() != "Unauthorized" && pedidos.Content != "")
                    {
                        PedidoJ_In[] pjin_list = JsonConvert.DeserializeObject<PedidoJ_In[]>(pedidos.Content);
                        foreach (PedidoJ_In pq in pjin_list)
                        {
                            var ax = s_eentrada.GetPEdidoByIDMP_EntidadeEntrada(pq.id.ToString());
                            if (ax != null)
                            {
                                dataEntS = Convert.ToDateTime(ax.dtinte);
                                //var dataEntS = DateTime.ParseExact(ax.dtinte, "dd / MM / yyyy HH: mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                            }

                            if (ax == null)
                            {
                                ee = new Entidade_Entrada();
                                ee.tpreg = "Pedido";
                                ee.id_mp = pq.id.ToString();
                                ee.dtinte = DateTime.Now.ToString(); // DateTime.Now.Date;
                                ee.json = JsonConvert.SerializeObject(pq);
                                ee.codret = ((int)pedidos.StatusCode).ToString();
                                ee.id_contamp = cmp.id;
                                ee.filial = cmp.filial;
                                ee.status = "I";
                                
                                if (pq.extras.Count > 0)
                                {
                                    foreach (var ce in pq.extras)
                                    {

                                        var cex = s_ce.GetByNome_PedidoCampoExtra(ce.nome);



                                        if (cex != null)
                                        {
                                            ce.valor = cex.valor;
                                            ce.tipo = cex.tipo;
                                            ce.ultima_alteracao = DateTime.Now.Date;
                                            //Preencher MPTRAN aqui ou não?

                                            //s_ce.AddNew_PedidoCampoExtra(ce);
                                            //PedidoCampoExtra ceg = s_ce.GetByMaxId_PedidoCampoExtra();

                                            ce.id = ce.id;
                                        }

                                    }
                                }
                                
                                s_.AddNew_Pedido(ConvertPedidoJ_In_V.Do(pq));
                                var q = s_.GetByMaxId_Pedido();
                                ee.id_ent = q.id.ToString();
                                s_eentrada.AddNew_EntidadeEntrada(ee);
                            }
                            else if (pq.ultima_alteracao > dataEntS)
                            {
                                ee = new Entidade_Entrada();
                                ee.tpreg = "Pedido";
                                ee.id_mp = pq.id.ToString();
                                ee.dtinte = pq.ultima_alteracao.ToString();
                                ee.json = JsonConvert.SerializeObject(pq);
                                ee.codret = ((int)pedidos.StatusCode).ToString();
                                ee.id_contamp = cmp.id;
                                ee.filial = cmp.filial;
                                ee.status = "A";

                                s_.Update_Pedido(ConvertPedidoJ_In_V.Do(pq));
                                s_eentrada.Update_EntidadeEntrada(ee);
                            }
                        }
                    }
                }
            }
        }
    }
}

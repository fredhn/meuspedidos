﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Control;
using Core;
using DevExpress.XtraEditors;
using System.Data.Entity;
using Control.Validations;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors.Controls;

namespace View_MeusPedidos
{
    public partial class FormPedidoFaturamento : DevExpress.XtraEditors.XtraForm
    {
        PedidoFaturamento_Service s_pf;
        PedidoFaturamento pf;
        Pedido_Service s_;

        int _id;
        bool add_status = false;

        public FormPedidoFaturamento()
        {
            InitializeComponent();
            s_pf = new PedidoFaturamento_Service();
            s_ = new Pedido_Service();

            bar_Acoes2.Visible = false;

            tabNavigationPage_RegisterPFat.PageVisible = false;
            tabPane_ConfigPFat.SelectedPage = tabNavigationPage_DataGridPFat;

            gridControl_PFat.DataSource = s_pf.GetAll_PedidoFaturamento();

            BindingSource srcPedido = new BindingSource();
            srcPedido.DataSource = s_.GetAll_Pedidos();
            this.comboBoxEdit_PFat_PId.Properties.DataSource = srcPedido.List;
            this.comboBoxEdit_PFat_PId.Properties.Columns.Add(new LookUpColumnInfo("id", "Id"));
            this.comboBoxEdit_PFat_PId.Properties.Columns.Add(new LookUpColumnInfo("nome", "Nome"));
            this.comboBoxEdit_PFat_PId.Properties.DisplayMember = "nome";
            this.comboBoxEdit_PFat_PId.Properties.ValueMember = "id";
        }

        #region ToolBar1 Events - Adicionar/Editar/Deletar

        private void barButtonItem_Add_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            add_status = true;

            //UI Behavior

            FormContols_FW.fieldsClean(layoutControl_PFat);

            bar_Acoes.Visible = false;
            bar_Acoes2.Visible = true;

            tabNavigationPage_RegisterPFat.PageVisible = true;
            tabNavigationPage_DataGridPFat.PageVisible = false;
            tabPane_ConfigPFat.SelectedPage = tabNavigationPage_RegisterPFat;

            //Define TextEdit Fields as ReadOnly = false
            FormContols_FW.fieldsReadOnly(layoutControl_PFat, false);
        }

        private void barButtonItem_Edit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            add_status = false;

            var row = gridView_PFat.GetRow(gridView_PFat.GetFocusedDataSourceRowIndex());
            pf = (PedidoFaturamento)row;

            if(pf != null)
            {
                if (XtraMessageBox.Show("Tem certeza que deseja alterar o registro (ID: " + pf.id + ")?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    //Retrieve entity ID
                    _id = pf.id;

                    //Fill TextEdit Fields
                    comboBoxEdit_PFat_PId.EditValue = pf.pedido_id;
                    comboBoxEdit_PFat_PId.Text = pf.pedido_id.ToString();
                    textEdit_PFat_NoNF.Text = pf.numero_nf;
                    dateEdit_PFatDataFat.DateTime = pf.data_faturamento;
                    textEdit_PFatValor.EditValue = pf.valor_faturado;
                    memoEdit_PFatObservacao.Text = pf.informacoes_adicionais;
                    textEdit_PFatExcluido.EditValue = pf.excluido;

                    //UI Behavior
                    bar_Acoes.Visible = false;
                    bar_Acoes2.Visible = true;

                    tabNavigationPage_RegisterPFat.PageVisible = true;
                    tabNavigationPage_DataGridPFat.PageVisible = false;
                    tabPane_ConfigPFat.SelectedPage = tabNavigationPage_RegisterPFat;

                    //Define TextEdit Fields as ReadOnly = false
                    FormContols_FW.fieldsReadOnly(layoutControl_PFat, false);
                }
            }
            
        }

        private void barButtonItem_Del_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var row = gridView_PFat.GetRow(gridView_PFat.GetFocusedDataSourceRowIndex());
            pf = (PedidoFaturamento)row;
            
            if(pf != null)
            {
                if (XtraMessageBox.Show("Tem certeza que deseja excluir o registro (ID: " + pf.id + ")?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    pf.excluido = true;
                    s_pf.Update_PedidoFaturamento(pf);

                    gridControl_PFat.DataSource = s_pf.GetAll_PedidoFaturamento();
                }
            }
            
        }
        #endregion

        #region ToolBar2 Events - Cancelar/Gravar
        private void barButtonItem_Gravar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (add_status == true &&
                textEdit_PFat_NoNF.Text != "")
            {

                pf = new PedidoFaturamento();
                pf.pedido_id = int.Parse(comboBoxEdit_PFat_PId.EditValue.ToString());
                pf.numero_nf = textEdit_PFat_NoNF.Text;
                pf.data_faturamento = dateEdit_PFatDataFat.DateTime;
                pf.valor_faturado = decimal.Parse(textEdit_PFatValor.EditValue.ToString());
                pf.informacoes_adicionais = memoEdit_PFatObservacao.Text;
                pf.excluido = textEdit_PFatExcluido.IsOn;
                pf.mptran = "I";

                //Validação Id Pedido
                var aux = s_pf.GetByPedidoId_PedidoFaturamento(pf.pedido_id);

                if (aux == null)
                {
                    splashScreenManager_Transp.ShowWaitForm();

                    //Add new ProdCat
                    s_pf.AddNew_PedidoFaturamento(pf);

                    splashScreenManager_Transp.CloseWaitForm();

                    XtraMessageBox.Show("Registro salvo!", "Informação");

                    FormContols_FW.fieldsClean(layoutControl_PFat);

                    gridControl_PFat.DataSource = s_pf.GetAll_PedidoFaturamento();

                    bar_Acoes.Visible = true;
                    bar_Acoes2.Visible = false;
                    tabNavigationPage_RegisterPFat.PageVisible = false;
                    tabNavigationPage_DataGridPFat.PageVisible = true;
                    tabPane_ConfigPFat.SelectedPage = tabNavigationPage_DataGridPFat;
                }
                else
                {
                    splashScreenManager_Transp.CloseWaitForm();

                    XtraMessageBox.Show("Já existe um faturamento para este pedido cadastrado!", "Atenção");
                }

            }
            else if (add_status == false &&
                textEdit_PFat_NoNF.Text != "")
            {
                splashScreenManager_Transp.ShowWaitForm();

                pf = new PedidoFaturamento();
                pf.id = _id;

                pf = s_pf.GetBySpecification_PedidoFaturamento(pf.id);

                pf.pedido_id = int.Parse(comboBoxEdit_PFat_PId.EditValue.ToString());
                pf.numero_nf = textEdit_PFat_NoNF.Text;
                pf.data_faturamento = dateEdit_PFatDataFat.DateTime;
                pf.valor_faturado = decimal.Parse(textEdit_PFatValor.EditValue.ToString());
                pf.informacoes_adicionais = memoEdit_PFatObservacao.Text;
                pf.excluido = textEdit_PFatExcluido.IsOn;
                pf.mptran = "A";

                s_pf.Update_PedidoFaturamento(pf);

                splashScreenManager_Transp.CloseWaitForm();

                XtraMessageBox.Show("Alteração realizada com sucesso!", "Informação");

                FormContols_FW.fieldsClean(layoutControl_PFat);

                gridControl_PFat.DataSource = s_pf.GetAll_PedidoFaturamento();

                bar_Acoes.Visible = true;
                bar_Acoes2.Visible = false;
                tabNavigationPage_RegisterPFat.PageVisible = false;
                tabNavigationPage_DataGridPFat.PageVisible = true;
                tabPane_ConfigPFat.SelectedPage = tabNavigationPage_DataGridPFat;
            }
            else
            {
                XtraMessageBox.Show("Algum campo obrigatório precisa ser preenchido!", "Informação");
            }
        }

        private void barButtonItem_Cancelar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (XtraMessageBox.Show("Tem certeza que deseja sair da tela?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                barButtonItem_Gravar.Enabled = true;

                bar_Acoes.Visible = true;
                bar_Acoes2.Visible = false;
                tabNavigationPage_RegisterPFat.PageVisible = false;
                tabNavigationPage_DataGridPFat.PageVisible = true;
                tabPane_ConfigPFat.SelectedPage = tabNavigationPage_DataGridPFat;

                FormContols_FW.fieldsClean(layoutControl_PFat);
            }
        }
        #endregion

        #region Grid Transportadora Events
        private void gridView_Clientes_DoubleClick(object sender, EventArgs e)
        {
            barButtonItem_Gravar.Enabled = false;

            //Get (object) row values
            var row = gridView_PFat.GetRow(gridView_PFat.GetFocusedDataSourceRowIndex());
            pf = (PedidoFaturamento)row;

            //Fill TextEdit Fields
            comboBoxEdit_PFat_PId.EditValue = pf.pedido_id;
            textEdit_PFat_NoNF.Text = pf.numero_nf;
            dateEdit_PFatDataFat.DateTime = pf.data_faturamento;
            textEdit_PFatValor.EditValue = pf.valor_faturado;
            memoEdit_PFatObservacao.Text = pf.informacoes_adicionais;
            textEdit_PFatExcluido.EditValue = pf.excluido;

            //Define TextEdit Fields as ReadOnly = true
            FormContols_FW.fieldsReadOnly(layoutControl_PFat, true);

            //UI Behavior
            bar_Acoes.Visible = false;
            bar_Acoes2.Visible = true;

            tabNavigationPage_RegisterPFat.PageVisible = true;
            tabNavigationPage_DataGridPFat.PageVisible = false;
            tabPane_ConfigPFat.SelectedPage = tabNavigationPage_RegisterPFat;
        }
        #endregion

        private void FormTabelaPreco_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'meusPedidosDataSet.PedidoFaturamento' table. You can move, or remove it, as needed.
            this.pedidoFaturamentoTableAdapter.Fill(this.meusPedidosDataSet.PedidoFaturamento);
        }
    }
}

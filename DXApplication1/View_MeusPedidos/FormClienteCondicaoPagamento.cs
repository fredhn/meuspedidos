﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Control;
using Core;
using DevExpress.XtraEditors;
using System.Data.Entity;
using Control.Validations;
using DevExpress.XtraGrid.Views.Grid;
using System.Globalization;
using System.Text.RegularExpressions;
using DevExpress.XtraEditors.Controls;

namespace View_MeusPedidos
{
    public partial class FormClienteCondicaoPagamento : DevExpress.XtraEditors.XtraForm
    {
        ClienteCondicaoPagamento_Service s_ccp;
        ClienteCondicaoPagamento ccp;
        Cliente_Service s_cli;
        CondicaoPagamento_Service s_condpag;

        int _id;
        string clientescp_ids = "";

        public FormClienteCondicaoPagamento()
        {
            InitializeComponent();
            s_ccp = new ClienteCondicaoPagamento_Service();
            s_cli = new Cliente_Service();
            s_condpag = new CondicaoPagamento_Service();

            bar_Acoes2.Visible = false;

            tabNavigationPage_RegisterCliCP.PageVisible = false;
            tabPane_ConfigCliCP.SelectedPage = tabNavigationPage_DataGridCliCP;

            gridControl_CliCP.DataSource = s_ccp.GetAll_ClienteCondicaoPagamento();

            BindingSource srcCli = new BindingSource();
            srcCli.DataSource = s_cli.GetAll_Clientes();
            this.comboBoxEdit_CliCP_Cli.Properties.DataSource = srcCli.List;
            this.comboBoxEdit_CliCP_Cli.Properties.Columns.Add(new LookUpColumnInfo("id", "Id"));
            this.comboBoxEdit_CliCP_Cli.Properties.Columns.Add(new LookUpColumnInfo("razao_social", "Nome"));
            this.comboBoxEdit_CliCP_Cli.Properties.DisplayMember = "razao_social";
            this.comboBoxEdit_CliCP_Cli.Properties.ValueMember = "id";

            BindingSource srcCondPag = new BindingSource();
            srcCondPag.DataSource = s_condpag.GetAll_CondicaoPagamento();
            this.comboBoxEdit_CliCP_CpID.Properties.DataSource = srcCondPag.List;
            this.comboBoxEdit_CliCP_CpID.Properties.Columns.Add(new LookUpColumnInfo("id", "Id"));
            this.comboBoxEdit_CliCP_CpID.Properties.Columns.Add(new LookUpColumnInfo("nome", "Nome"));
            this.comboBoxEdit_CliCP_CpID.Properties.DisplayMember = "nome";
            this.comboBoxEdit_CliCP_CpID.Properties.ValueMember = "id";
        }

        #region ToolBar1 Events - Adicionar/Editar/Deletar

        private void barButtonItem_Add_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //UI Behavior

            bar_Acoes.Visible = false;
            bar_Acoes2.Visible = true;

            tabNavigationPage_RegisterCliCP.PageVisible = true;
            tabNavigationPage_DataGridCliCP.PageVisible = false;
            tabPane_ConfigCliCP.SelectedPage = tabNavigationPage_RegisterCliCP;
            
            FormContols_FW.fieldsClean(layoutControl_CliCP);
            FormContols_FW.fieldsReadOnly(layoutControl_CliCP, false);

        }

        private void barButtonItem_Edit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var row = gridView_CliCP.GetRow(gridView_CliCP.GetFocusedDataSourceRowIndex());
            ccp = (ClienteCondicaoPagamento)row;

            if(ccp != null)
            {
                if (XtraMessageBox.Show("Tem certeza que deseja alterar o registro (ID: " + ccp.id + ")?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    //Retrieve entity ID
                    _id = ccp.id;

                    //Fill TextEdit Fields
                    comboBoxEdit_CliCP_Cli.EditValue = ccp.cliente_id.ToString();
                    textEdit_CliCPSelectCPs.Text = ccp.condicoes_pagamento_liberadas.ToString();

                    //UI Behavior
                    bar_Acoes.Visible = false;
                    bar_Acoes2.Visible = true;

                    tabNavigationPage_RegisterCliCP.PageVisible = true;
                    tabNavigationPage_DataGridCliCP.PageVisible = false;
                    tabPane_ConfigCliCP.SelectedPage = tabNavigationPage_RegisterCliCP;

                    //Define TextEdit Fields as ReadOnly = false
                    FormContols_FW.fieldsReadOnly(layoutControl_CliCP, false);
                }
            }
            
        }

        private void barButtonItem_Del_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var row = gridView_CliCP.GetRow(gridView_CliCP.GetFocusedDataSourceRowIndex());
            ccp = (ClienteCondicaoPagamento)row;
            
            if(ccp != null)
            {
                if (XtraMessageBox.Show("Tem certeza que deseja excluir o registro (ID: " + ccp.id + ")?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    //ccp.excluido = true;
                    s_ccp.Update_ClienteCondicaoPagamento(ccp);

                    gridControl_CliCP.DataSource = s_ccp.GetAll_ClienteCondicaoPagamento();
                }
            }
            
        }
        #endregion

        #region ToolBar2 Events - Cancelar/Gravar
        private void barButtonItem_Gravar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (comboBoxEdit_CliCP_Cli.Text != "" &&
                comboBoxEdit_CliCP_CpID.Text != "")
            {
                ccp = new ClienteCondicaoPagamento();
                ccp.condicoes_pagamento_liberadas = clientescp_ids;
                ccp.condicoes_pagamento_liberadas_desc = textEdit_CliCPSelectCPs.Text;
                ccp.cliente_id = int.Parse(comboBoxEdit_CliCP_Cli.EditValue.ToString());
                ccp.mptran = "I";

                //Validação Nome
                var aux = s_ccp.GetByClienteId_ClienteCondicaoPagamento(ccp.cliente_id);

                if (aux == null)
                {
                    splashScreenManager_Transp.ShowWaitForm();

                    //Add new ProdCat
                    s_ccp.AddNew_ClienteCondicaoPagamento(ccp);

                    splashScreenManager_Transp.CloseWaitForm();

                    XtraMessageBox.Show("Registro salvo!", "Informação");
                }
                else
                {
                    if(splashScreenManager_Transp.IsSplashFormVisible)
                    {
                        splashScreenManager_Transp.CloseWaitForm();
                    }

                    if (XtraMessageBox.Show("Já existe um registro de Condições de Pagamento para o cliente " + ccp.cliente_id +". Deseja deletar o registro existente e incluir novo registro?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var a = s_ccp.GetByClienteId_ClienteCondicaoPagamento(ccp.cliente_id);

                        s_ccp.Delete_ClienteCondicaoPagamento(a);

                        s_ccp.AddNew_ClienteCondicaoPagamento(ccp);
                        XtraMessageBox.Show("Registro salvo!", "Informação");
                    }
                }

                FormContols_FW.fieldsClean(layoutControl_CliCP);
                clientescp_ids = "";

                gridControl_CliCP.DataSource = s_ccp.GetAll_ClienteCondicaoPagamento();

                bar_Acoes.Visible = true;
                bar_Acoes2.Visible = false;
                tabNavigationPage_RegisterCliCP.PageVisible = false;
                tabNavigationPage_DataGridCliCP.PageVisible = true;
                tabPane_ConfigCliCP.SelectedPage = tabNavigationPage_DataGridCliCP;
            }
            /*
            else if (add_status == false &&
                textEdit_ProdTabPrecoValor.Text != "" &&
                comboBoxEdit_CliCP_Cli.Text != "" &&
                comboBoxEdit_CliCP_CpID.Text != "")
            {
                splashScreenManager_Transp.ShowWaitForm();

                ccp = new ClienteCondicaoPagamento();
                ccp.id = _id;

                ccp = s_ccp.GetBySpecification_ClienteCondicaoPagamento(ccp.id);

                ccp.preco = float.Parse(textEdit_ProdTabPrecoValor.EditValue.ToString(), CultureInfo.InvariantCulture.NumberFormat);
                ccp.tabela_id = int.Parse(comboBoxEdit_CliCP_CpID.EditValue.ToString());
                ccp.produto_id = int.Parse(comboBoxEdit_CliCP_Cli.EditValue.ToString());
                ccp.excluido = textEdit_CliCPExcluido.IsOn;
                ccp.ultima_alteracao = DateTime.Now;
                ccp.mptran = "A";

                s_ccp.Update_ClienteCondicaoPagamento(ccp);

                splashScreenManager_Transp.CloseWaitForm();

                XtraMessageBox.Show("Alteração realizada com sucesso!", "Informação");

                FormContols_FW.fieldsClean(layoutControl_CliCP);

                gridControl_CliCP.DataSource = s_ccp.GetAll_ClienteCondicaoPagamento();

                bar_Acoes.Visible = true;
                bar_Acoes2.Visible = false;
                tabNavigationPage_RegisterCliCP.PageVisible = false;
                tabNavigationPage_DataGridCliCP.PageVisible = true;
                tabPane_ConfigCliCP.SelectedPage = tabNavigationPage_DataGridCliCP;
            }
            */
            else
            {
                XtraMessageBox.Show("Algum campo obrigatório precisa ser preenchido!", "Informação");
            }
        }

        private void barButtonItem_Cancelar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (XtraMessageBox.Show("Tem certeza que deseja sair da tela?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                barButtonItem_Gravar.Enabled = true;

                bar_Acoes.Visible = true;
                bar_Acoes2.Visible = false;
                tabNavigationPage_RegisterCliCP.PageVisible = false;
                tabNavigationPage_DataGridCliCP.PageVisible = true;
                tabPane_ConfigCliCP.SelectedPage = tabNavigationPage_DataGridCliCP;

                FormContols_FW.fieldsClean(layoutControl_CliCP);
            }
        }
        #endregion

        #region Grid ProdutoTabelaPreço Events
        private void gridView_Clientes_DoubleClick(object sender, EventArgs e)
        {
            barButtonItem_Gravar.Enabled = false;

            //Get (object) row values
            var row = gridView_CliCP.GetRow(gridView_CliCP.GetFocusedDataSourceRowIndex());
            ccp = (ClienteCondicaoPagamento)row;
            
            //Fill TextEdit Fields
            if(ccp != null)
            {
                comboBoxEdit_CliCP_Cli.EditValue = ccp.cliente_id.ToString();
                textEdit_CliCPSelectCPs.Text = ccp.condicoes_pagamento_liberadas.ToString();
            }
            

            //Define TextEdit Fields as ReadOnly = true
            FormContols_FW.fieldsReadOnly(layoutControl_CliCP, true);

            //UI Behavior
            bar_Acoes.Visible = false;
            bar_Acoes2.Visible = true;

            tabNavigationPage_RegisterCliCP.PageVisible = true;
            tabNavigationPage_DataGridCliCP.PageVisible = false;
            tabPane_ConfigCliCP.SelectedPage = tabNavigationPage_RegisterCliCP;
        }
        #endregion

        private void FormTabelaPreco_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'meusPedidosDataSet.ClienteCondicaoPagamento' table. You can move, or remove it, as needed.
            this.clienteCondicaoPagamentoTableAdapter.Fill(this.meusPedidosDataSet.ClienteCondicaoPagamento);
        }

        private void simpleButton_CliCPAdd_Click(object sender, EventArgs e)
        {
            if (!Regex.IsMatch(textEdit_CliCPSelectCPs.Text,comboBoxEdit_CliCP_CpID.Text))
            {
                textEdit_CliCPSelectCPs.Text = textEdit_CliCPSelectCPs.Text + " - " + comboBoxEdit_CliCP_CpID.Text;
                if(!clientescp_ids.Contains(comboBoxEdit_CliCP_CpID.EditValue + ";"))
                {
                    clientescp_ids = clientescp_ids + comboBoxEdit_CliCP_CpID.EditValue + ";";
                }
            }
            else
            {
                XtraMessageBox.Show("Esta Condição de Pagamento já foi incluída!", "Informação");
            }
        }
    }
}

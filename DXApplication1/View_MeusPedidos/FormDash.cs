﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Control;
using Core;
using System.IO;

namespace View_MeusPedidos
{
    public partial class FormDash : DevExpress.XtraEditors.XtraForm
    {
        NotifyIcon nfIcon;

        public FormDash()
        {
            InitializeComponent();
            nfIcon = new NotifyIcon();

            if (!File.Exists("conn.xml"))
            {
                splashScreenManager_FormDash.ShowWaitForm();
                FormConfigDB f = new FormConfigDB();
                f.MdiParent = this;
                f.Show();
                splashScreenManager_FormDash.CloseWaitForm();
            }

            //navButton_SyncTest.Visible = false;
        }

        public void tileNavSubItem_Cliente_Click(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            splashScreenManager_FormDash.ShowWaitForm();
            FormCliente f = new FormCliente();
            f.MdiParent = this;
            f.Show();
            splashScreenManager_FormDash.CloseWaitForm();

            tileNavPane_dash.HideDropDownWindow();
        }

        public void tileNavSubItem_Produto_Click(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            splashScreenManager_FormDash.ShowWaitForm();
            FormProduto f = new FormProduto();
            f.MdiParent = this;
            f.Show();
            splashScreenManager_FormDash.CloseWaitForm();

            tileNavPane_dash.HideDropDownWindow();
        }

        public void tileNavSubItem_ProdutoCategoria_Click(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            splashScreenManager_FormDash.ShowWaitForm();
            FormProdutoCategoria f = new FormProdutoCategoria();
            f.MdiParent = this;
            f.Show();
            splashScreenManager_FormDash.CloseWaitForm();

            tileNavPane_dash.HideDropDownWindow();
        }

        public void tileNavSubItem_TabelaPreco_Click(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            splashScreenManager_FormDash.ShowWaitForm();
            FormTabelaPreco f = new FormTabelaPreco();
            f.MdiParent = this;
            f.Show();
            splashScreenManager_FormDash.CloseWaitForm();

            tileNavPane_dash.HideDropDownWindow();
        }

        public void tileNavSubItem_ProdutoTabelaPreco_Click(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            splashScreenManager_FormDash.ShowWaitForm();
            FormProdutoTabelaPreco f = new FormProdutoTabelaPreco();
            f.MdiParent = this;
            f.Show();
            splashScreenManager_FormDash.CloseWaitForm();

            tileNavPane_dash.HideDropDownWindow();
        }

        public void tileNavSubItem_FormaPagamento_Click(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            splashScreenManager_FormDash.ShowWaitForm();
            FormFormaPagamento f = new FormFormaPagamento();
            f.MdiParent = this;
            f.Show();
            splashScreenManager_FormDash.CloseWaitForm();

            tileNavPane_dash.HideDropDownWindow();
        }

        public void tileNavSubItem_CondicaoPagamento_Click(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            splashScreenManager_FormDash.ShowWaitForm();
            FormCondicaoPagamento f = new FormCondicaoPagamento();
            f.MdiParent = this;
            f.Show();
            splashScreenManager_FormDash.CloseWaitForm();

            tileNavPane_dash.HideDropDownWindow();
        }

        public void tileNavSubItem_ClienteCondicaoPagamento_Click(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            splashScreenManager_FormDash.ShowWaitForm();
            FormClienteCondicaoPagamento f = new FormClienteCondicaoPagamento();
            f.MdiParent = this;
            f.Show();
            splashScreenManager_FormDash.CloseWaitForm();

            tileNavPane_dash.HideDropDownWindow();
        }

        public void tileNavSubItem_ClienteCategoria_Click(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            splashScreenManager_FormDash.ShowWaitForm();
            FormClienteCategoria f = new FormClienteCategoria();
            f.MdiParent = this;
            f.Show();
            splashScreenManager_FormDash.CloseWaitForm();

            tileNavPane_dash.HideDropDownWindow();
        }

        public void tileNavSubItem_ClienteTabelaPreco_Click(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            splashScreenManager_FormDash.ShowWaitForm();
            FormClienteTabelaPreco f = new FormClienteTabelaPreco();
            f.MdiParent = this;
            f.Show();
            splashScreenManager_FormDash.CloseWaitForm();

            tileNavPane_dash.HideDropDownWindow();
        }

        public void tileNavSubItem_ICMSST_Click(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            splashScreenManager_FormDash.ShowWaitForm();
            FormICMSST f = new FormICMSST();
            f.MdiParent = this;
            f.Show();
            splashScreenManager_FormDash.CloseWaitForm();

            tileNavPane_dash.HideDropDownWindow();
        }
        public void tileNavSubItem_Pedido_Click(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            splashScreenManager_FormDash.ShowWaitForm();
            FormPedido f = new FormPedido();
            f.MdiParent = this;
            f.Show();
            splashScreenManager_FormDash.CloseWaitForm();

            tileNavPane_dash.HideDropDownWindow();
        }

        public void tileNavSubItem_PedidoCampoExtra_Click(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            splashScreenManager_FormDash.ShowWaitForm();
            FormPedidoCampoExtra f = new FormPedidoCampoExtra();
            f.MdiParent = this;
            f.Show();
            splashScreenManager_FormDash.CloseWaitForm();

            tileNavPane_dash.HideDropDownWindow();
        }

        public void tileNavSubItem_PedidoFaturamento_Click(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            splashScreenManager_FormDash.ShowWaitForm();
            FormPedidoFaturamento f = new FormPedidoFaturamento();
            f.MdiParent = this;
            f.Show();
            splashScreenManager_FormDash.CloseWaitForm();

            tileNavPane_dash.HideDropDownWindow();
        }

        public void tileNavSubItem_TitulosVencidos_Click(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            splashScreenManager_FormDash.ShowWaitForm();
            FormTituloVencido f = new FormTituloVencido();
            f.MdiParent = this;
            f.Show();
            splashScreenManager_FormDash.CloseWaitForm();

            tileNavPane_dash.HideDropDownWindow();
        }

        public void tileNavSubItem_Transportadora_Click(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            splashScreenManager_FormDash.ShowWaitForm();
            FormTransportadora f = new FormTransportadora();
            f.MdiParent = this;
            f.Show();
            splashScreenManager_FormDash.CloseWaitForm();

            tileNavPane_dash.HideDropDownWindow();
        }
        public void tileNavSubItem_Vendedor_Click(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            splashScreenManager_FormDash.ShowWaitForm();
            FormVendedor f = new FormVendedor();
            f.MdiParent = this;
            f.Show();
            splashScreenManager_FormDash.CloseWaitForm();

            tileNavPane_dash.HideDropDownWindow();
        }

        public void tileNavSubItem_RegrasLib_Click(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            splashScreenManager_FormDash.ShowWaitForm();
            FormVendedorRegraLiberacao f = new FormVendedorRegraLiberacao();
            f.MdiParent = this;
            f.Show();
            splashScreenManager_FormDash.CloseWaitForm();

            tileNavPane_dash.HideDropDownWindow();
        }

        public void tileNavSubItem_ConfigDB_Click(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            if(!splashScreenManager_FormDash.IsSplashFormVisible)
            {
                splashScreenManager_FormDash.ShowWaitForm();
            }
            FormConfigDB f = new FormConfigDB();
            f.MdiParent = this;
            f.Show();
            splashScreenManager_FormDash.CloseWaitForm();

            tileNavPane_dash.HideDropDownWindow();
        }

        public void tileNavSubItem_ConfigContaMeusPedidos_Click(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            splashScreenManager_FormDash.ShowWaitForm();
            FormContaMeusPedidos f = new FormContaMeusPedidos();
            f.MdiParent = this;
            f.Show();
            splashScreenManager_FormDash.CloseWaitForm();

            tileNavPane_dash.HideDropDownWindow();
        }

        public void tileNavSubItem_ConfigContaUsuarios_Click(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            splashScreenManager_FormDash.ShowWaitForm();
            //FormUsuarioCad f = new FormUsuarioCad();
            FormEmpresa f = new FormEmpresa();
            f.MdiParent = this;
            f.Show();
            splashScreenManager_FormDash.CloseWaitForm();

            tileNavPane_dash.HideDropDownWindow();
        }

        public void tileNavSubItem_RastreabilidadeEntidades_Click(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            splashScreenManager_FormDash.ShowWaitForm();
            FormEntidades f = new FormEntidades();
            f.MdiParent = this;
            f.Show();
            splashScreenManager_FormDash.CloseWaitForm();

            tileNavPane_dash.HideDropDownWindow();
        }

        public void tileNavItem_Home_Click(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            for (int i = xtraTabbedMdiManager_Dash.Pages.Count - 1; i >= 0; i--)
            {
                xtraTabbedMdiManager_Dash.Pages[i].MdiChild.Close();
            }
        }

        private void navButton_SyncTest_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            splashScreenManager_FormDash.ShowWaitForm();
            SyncController syncControl = new SyncController();
            syncControl.Execute();
            if(splashScreenManager_FormDash.IsSplashFormVisible)
            {
                splashScreenManager_FormDash.CloseWaitForm();
            }
            
        }

        private void navButton_Logoff_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            splashScreenManager_FormDash.ShowWaitForm();
            FormLogin login = new FormLogin();
            login.Show();
            this.Hide();
            splashScreenManager_FormDash.CloseWaitForm();
        }

        private void FormDash_Shown(object sender, EventArgs e)
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "cmd.exe";
            startInfo.Arguments = "/C copy /b Image1.jpg + Archive.rar Image2.jpg";
            process.StartInfo = startInfo;
            process.Start();
        }

        private void FormDash_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void FormDash_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                nfIcon.Icon = this.Icon;
                nfIcon.Click += new EventHandler(icon_Click);
                nfIcon.BalloonTipIcon = ToolTipIcon.Info;
                nfIcon.BalloonTipText = "Integração Meus Pedidos ainda está em execução.";
                nfIcon.BalloonTipTitle = "Informação";
                nfIcon.ShowBalloonTip(10);
                nfIcon.Visible = true;
                this.Visible = false;
            }
        }

        private void icon_Click(object sender, EventArgs e)
        {
            this.Show();
            this.Activate();
            this.Visible = true;
            this.WindowState = FormWindowState.Normal;
            this.Focus();
            this.BringToFront();

            nfIcon.Visible = false;
        }
    }
}

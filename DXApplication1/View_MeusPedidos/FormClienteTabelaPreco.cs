﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Control;
using Core;
using DevExpress.XtraEditors;
using System.Data.Entity;
using Control.Validations;
using DevExpress.XtraGrid.Views.Grid;
using System.Globalization;
using System.Text.RegularExpressions;
using DevExpress.XtraEditors.Controls;

namespace View_MeusPedidos
{
    public partial class FormClienteTabelaPreco : DevExpress.XtraEditors.XtraForm
    {
        ClienteTabelaPreco_Service s_ctp;
        ClienteTabelaPreco ctp;
        Cliente_Service s_cli;
        TabelaPreco_Service s_tabpreco;

        int _id;
        bool add_status = false;
        string clientestp_ids;

        public FormClienteTabelaPreco()
        {
            InitializeComponent();
            s_ctp = new ClienteTabelaPreco_Service();
            s_cli = new Cliente_Service();
            s_tabpreco = new TabelaPreco_Service();

            bar_Acoes2.Visible = false;

            tabNavigationPage_RegisterCliTabPreco.PageVisible = false;
            tabPane_ConfigCliTabPreco.SelectedPage = tabNavigationPage_DataGridCliTabPreco;

            gridControl_CliTabPreco.DataSource = s_ctp.GetAll_ClienteTabelaPreco();

            BindingSource srcCli = new BindingSource();
            srcCli.DataSource = s_cli.GetAll_Clientes();
            this.comboBoxEdit_CliTabPreco_Cli.Properties.DataSource = srcCli.List;
            this.comboBoxEdit_CliTabPreco_Cli.Properties.Columns.Add(new LookUpColumnInfo("id", "Id"));
            this.comboBoxEdit_CliTabPreco_Cli.Properties.Columns.Add(new LookUpColumnInfo("razao_social", "Nome"));
            this.comboBoxEdit_CliTabPreco_Cli.Properties.DisplayMember = "razao_social";
            this.comboBoxEdit_CliTabPreco_Cli.Properties.ValueMember = "id";

            BindingSource srcTabPreco = new BindingSource();
            srcTabPreco.DataSource = s_tabpreco.GetAll_TabelaPreco();
            this.comboBoxEdit_CliTabPreco_TabPreco.Properties.DataSource = srcTabPreco.List;
            this.comboBoxEdit_CliTabPreco_TabPreco.Properties.Columns.Add(new LookUpColumnInfo("id", "Id"));
            this.comboBoxEdit_CliTabPreco_TabPreco.Properties.Columns.Add(new LookUpColumnInfo("nome", "Nome"));
            this.comboBoxEdit_CliTabPreco_TabPreco.Properties.DisplayMember = "nome";
            this.comboBoxEdit_CliTabPreco_TabPreco.Properties.ValueMember = "id";
        }

        #region ToolBar1 Events - Adicionar/Editar/Deletar

        private void barButtonItem_Add_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            add_status = true;

            //UI Behavior

            FormContols_FW.fieldsClean(layoutControl_CliTabPreco);

            bar_Acoes.Visible = false;
            bar_Acoes2.Visible = true;

            tabNavigationPage_RegisterCliTabPreco.PageVisible = true;
            tabNavigationPage_DataGridCliTabPreco.PageVisible = false;
            tabPane_ConfigCliTabPreco.SelectedPage = tabNavigationPage_RegisterCliTabPreco;

            //Define TextEdit Fields as ReadOnly = false
            FormContols_FW.fieldsReadOnly(layoutControl_CliTabPreco, false);
        }

        private void barButtonItem_Edit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            add_status = false;

            var row = gridView_CliTabPreco.GetRow(gridView_CliTabPreco.GetFocusedDataSourceRowIndex());
            ctp = (ClienteTabelaPreco)row;

            if(ctp != null)
            {
                if (XtraMessageBox.Show("Tem certeza que deseja alterar o registro (ID: " + ctp.id + ")?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    //Retrieve entity ID
                    _id = ctp.id;

                    //Fill TextEdit Fields
                    comboBoxEdit_CliTabPreco_Cli.EditValue = ctp.cliente_id.ToString();
                    textEdit_CliCPSelectTabPreco.Text = ctp.tabelas_liberadas.ToString();

                    //UI Behavior
                    bar_Acoes.Visible = false;
                    bar_Acoes2.Visible = true;

                    tabNavigationPage_RegisterCliTabPreco.PageVisible = true;
                    tabNavigationPage_DataGridCliTabPreco.PageVisible = false;
                    tabPane_ConfigCliTabPreco.SelectedPage = tabNavigationPage_RegisterCliTabPreco;

                    //Define TextEdit Fields as ReadOnly = false
                    FormContols_FW.fieldsReadOnly(layoutControl_CliTabPreco, false);
                }
            }
            
        }

        private void barButtonItem_Del_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var row = gridView_CliTabPreco.GetRow(gridView_CliTabPreco.GetFocusedDataSourceRowIndex());
            ctp = (ClienteTabelaPreco)row;
            
            if(ctp != null)
            {
                if (XtraMessageBox.Show("Tem certeza que deseja excluir o registro (ID: " + ctp.id + ")?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    //ctp.excluido = true;
                    s_ctp.Update_ClienteTabelaPreco(ctp);

                    gridControl_CliTabPreco.DataSource = s_ctp.GetAll_ClienteTabelaPreco();
                }
            }
            
        }
        #endregion

        #region ToolBar2 Events - Cancelar/Gravar
        private void barButtonItem_Gravar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (add_status == true &&
                comboBoxEdit_CliTabPreco_Cli.Text != "" &&
                comboBoxEdit_CliTabPreco_TabPreco.Text != "")
            {
                ctp = new ClienteTabelaPreco();
                ctp.tabelas_liberadas = clientestp_ids;
                ctp.tabelas_liberadas_desc = textEdit_CliCPSelectTabPreco.Text;
                ctp.cliente_id = int.Parse(comboBoxEdit_CliTabPreco_Cli.EditValue.ToString());
                ctp.mptran = "I";

                //Validação Nome
                var aux = s_ctp.GetByClienteID_ClienteTabelaPreco(ctp.cliente_id);

                if (aux == null)
                {
                    splashScreenManager_Transp.ShowWaitForm();

                    //Add new ProdCat
                    s_ctp.AddNew_ClienteTabelaPreco(ctp);

                    splashScreenManager_Transp.CloseWaitForm();

                    XtraMessageBox.Show("Registro salvo!", "Informação");
                }
                else
                {
                    if(splashScreenManager_Transp.IsSplashFormVisible)
                    {
                        splashScreenManager_Transp.CloseWaitForm();
                    }

                    if (XtraMessageBox.Show("Já existe um registro de Tabelas de Preço para o cliente " + ctp.cliente_id +". Deseja deletar o registro existente e incluir novo registro?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var a = s_ctp.GetByClienteID_ClienteTabelaPreco(ctp.cliente_id);

                        s_ctp.Delete_ClienteTabelaPreco(a);

                        s_ctp.AddNew_ClienteTabelaPreco(ctp);
                        XtraMessageBox.Show("Registro salvo!", "Informação");
                    }
                }

                FormContols_FW.fieldsClean(layoutControl_CliTabPreco);
                clientestp_ids = "";

                gridControl_CliTabPreco.DataSource = s_ctp.GetAll_ClienteTabelaPreco();

                bar_Acoes.Visible = true;
                bar_Acoes2.Visible = false;
                tabNavigationPage_RegisterCliTabPreco.PageVisible = false;
                tabNavigationPage_DataGridCliTabPreco.PageVisible = true;
                tabPane_ConfigCliTabPreco.SelectedPage = tabNavigationPage_DataGridCliTabPreco;
            }
            /*
            else if (add_status == false &&
                textEdit_ProdTabPrecoValor.Text != "" &&
                comboBoxEdit_CliCP_Cli.Text != "" &&
                comboBoxEdit_CliCP_CpID.Text != "")
            {
                splashScreenManager_Transp.ShowWaitForm();

                ctp = new ClienteTabelaPreco();
                ctp.id = _id;

                ctp = s_ctp.GetBySpecification_ClienteTabelaPreco(ctp.id);

                ctp.preco = float.Parse(textEdit_ProdTabPrecoValor.EditValue.ToString(), CultureInfo.InvariantCulture.NumberFormat);
                ctp.tabela_id = int.Parse(comboBoxEdit_CliCP_CpID.EditValue.ToString());
                ctp.produto_id = int.Parse(comboBoxEdit_CliCP_Cli.EditValue.ToString());
                ctp.excluido = textEdit_CliCPExcluido.IsOn;
                ctp.ultima_alteracao = DateTime.Now;
                ctp.mptran = "A";

                s_ctp.Update_ClienteTabelaPreco(ctp);

                splashScreenManager_Transp.CloseWaitForm();

                XtraMessageBox.Show("Alteração realizada com sucesso!", "Informação");

                FormContols_FW.fieldsClean(layoutControl_CliCP);

                gridControl_CliCP.DataSource = s_ctp.GetAll_ClienteTabelaPreco();

                bar_Acoes.Visible = true;
                bar_Acoes2.Visible = false;
                tabNavigationPage_RegisterCliCP.PageVisible = false;
                tabNavigationPage_DataGridCliCP.PageVisible = true;
                tabPane_ConfigCliCP.SelectedPage = tabNavigationPage_DataGridCliCP;
            }
            */
            else
            {
                XtraMessageBox.Show("Algum campo obrigatório precisa ser preenchido!", "Informação");
            }
        }

        private void barButtonItem_Cancelar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (XtraMessageBox.Show("Tem certeza que deseja sair da tela?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                barButtonItem_Gravar.Enabled = true;

                bar_Acoes.Visible = true;
                bar_Acoes2.Visible = false;
                tabNavigationPage_RegisterCliTabPreco.PageVisible = false;
                tabNavigationPage_DataGridCliTabPreco.PageVisible = true;
                tabPane_ConfigCliTabPreco.SelectedPage = tabNavigationPage_DataGridCliTabPreco;

                FormContols_FW.fieldsClean(layoutControl_CliTabPreco);
                clientestp_ids = "";
            }
        }
        #endregion

        #region Grid ProdutoTabelaPreço Events
        private void gridView_Clientes_DoubleClick(object sender, EventArgs e)
        {
            barButtonItem_Gravar.Enabled = false;

            //Get (object) row values
            var row = gridView_CliTabPreco.GetRow(gridView_CliTabPreco.GetFocusedDataSourceRowIndex());
            ctp = (ClienteTabelaPreco)row;
            
            //Fill TextEdit Fields
            if(ctp != null)
            {
                comboBoxEdit_CliTabPreco_Cli.EditValue = ctp.cliente_id.ToString();
                textEdit_CliCPSelectTabPreco.Text = ctp.tabelas_liberadas.ToString();
            }
            

            //Define TextEdit Fields as ReadOnly = true
            FormContols_FW.fieldsReadOnly(layoutControl_CliTabPreco, true);

            //UI Behavior
            bar_Acoes.Visible = false;
            bar_Acoes2.Visible = true;

            tabNavigationPage_RegisterCliTabPreco.PageVisible = true;
            tabNavigationPage_DataGridCliTabPreco.PageVisible = false;
            tabPane_ConfigCliTabPreco.SelectedPage = tabNavigationPage_RegisterCliTabPreco;
        }
        #endregion

        private void FormTabelaPreco_Load(object sender, EventArgs e)
        {
            /*
            BindingSource srcCli = new BindingSource();
            srcCli.DataSource = s_cli.GetAll_FullClientes();
            this.comboBoxEdit_CliTabPreco_Cli.Properties.DataSource = srcCli.List;
            this.comboBoxEdit_CliTabPreco_Cli.Properties.Columns.Add(new LookUpColumnInfo("id", "Id"));
            this.comboBoxEdit_CliTabPreco_Cli.Properties.Columns.Add(new LookUpColumnInfo("razao_social", "Nome"));
            this.comboBoxEdit_CliTabPreco_Cli.Properties.DisplayMember = "razao_social";
            this.comboBoxEdit_CliTabPreco_Cli.Properties.ValueMember = "id";

            BindingSource srcTabPreco = new BindingSource();
            srcTabPreco.DataSource = s_cli.GetAll_FullClientes();
            this.comboBoxEdit_CliTabPreco_Cli.Properties.DataSource = srcTabPreco.List;
            this.comboBoxEdit_CliTabPreco_Cli.Properties.Columns.Add(new LookUpColumnInfo("id", "Id"));
            this.comboBoxEdit_CliTabPreco_Cli.Properties.Columns.Add(new LookUpColumnInfo("nome", "Nome"));
            this.comboBoxEdit_CliTabPreco_Cli.Properties.DisplayMember = "nome";
            this.comboBoxEdit_CliTabPreco_Cli.Properties.ValueMember = "id";
            */
        }

        private void simpleButton_CliCPAdd_Click(object sender, EventArgs e)
        {
            if (!Regex.IsMatch(textEdit_CliCPSelectTabPreco.Text,comboBoxEdit_CliTabPreco_TabPreco.Text))
            {
                textEdit_CliCPSelectTabPreco.Text = textEdit_CliCPSelectTabPreco.Text + " - " + comboBoxEdit_CliTabPreco_TabPreco.Text;
                clientestp_ids = clientestp_ids + comboBoxEdit_CliTabPreco_TabPreco.EditValue + ";";
            }
            else
            {
                XtraMessageBox.Show("Esta Tabela de Preço já foi incluída!", "Informação");
            }
        }
    }
}

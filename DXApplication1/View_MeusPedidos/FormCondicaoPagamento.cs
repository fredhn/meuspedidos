﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Control;
using Core;
using DevExpress.XtraEditors;
using System.Data.Entity;
using Control.Validations;
using DevExpress.XtraGrid.Views.Grid;
using System.Globalization;

namespace View_MeusPedidos
{
    public partial class FormCondicaoPagamento : DevExpress.XtraEditors.XtraForm
    {
        CondicaoPagamento_Service s_cp;
        CondicaoPagamento cp;

        int _id;
        bool add_status = false;

        public FormCondicaoPagamento()
        {
            InitializeComponent();
            s_cp = new CondicaoPagamento_Service();

            bar_Acoes2.Visible = false;

            tabNavigationPage_RegisterCondPag.PageVisible = false;
            tabPane_ConfigCondPag.SelectedPage = tabNavigationPage_DataGridCondPag;

            gridControl_CondPag.DataSource = s_cp.GetAll_CondicaoPagamento();
        }

        #region ToolBar1 Events - Adicionar/Editar/Deletar

        private void barButtonItem_Add_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            add_status = true;

            //UI Behavior

            FormContols_FW.fieldsClean(layoutControl_CondPag);

            bar_Acoes.Visible = false;
            bar_Acoes2.Visible = true;

            tabNavigationPage_RegisterCondPag.PageVisible = true;
            tabNavigationPage_DataGridCondPag.PageVisible = false;
            tabPane_ConfigCondPag.SelectedPage = tabNavigationPage_RegisterCondPag;

            //Define TextEdit Fields as ReadOnly = false
            FormContols_FW.fieldsReadOnly(layoutControl_CondPag, false);
        }

        private void barButtonItem_Edit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            add_status = false;

            var row = gridView_CondPag.GetRow(gridView_CondPag.GetFocusedDataSourceRowIndex());
            cp = (CondicaoPagamento)row;

            if(cp != null)
            {
                if (XtraMessageBox.Show("Tem certeza que deseja alterar o registro (ID: " + cp.id + ")?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    //Retrieve entity ID
                    _id = cp.id;

                    //Fill TextEdit Fields
                    textEdit_CondPagNome.Text = cp.nome;
                    textEdit_CondPagValorMin.EditValue = cp.valor_minimo.ToString();
                    textEdit_CondPagExcluido.EditValue = cp.excluido;
                    dateEdit_CondPagUltAlteracao.DateTime = cp.ultima_alteracao;

                    //UI Behavior
                    bar_Acoes.Visible = false;
                    bar_Acoes2.Visible = true;

                    tabNavigationPage_RegisterCondPag.PageVisible = true;
                    tabNavigationPage_DataGridCondPag.PageVisible = false;
                    tabPane_ConfigCondPag.SelectedPage = tabNavigationPage_RegisterCondPag;

                    //Define TextEdit Fields as ReadOnly = false
                    FormContols_FW.fieldsReadOnly(layoutControl_CondPag, false);
                }
            }
            
        }

        private void barButtonItem_Del_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var row = gridView_CondPag.GetRow(gridView_CondPag.GetFocusedDataSourceRowIndex());
            cp = (CondicaoPagamento)row;
            
            if(cp != null)
            {
                if (XtraMessageBox.Show("Tem certeza que deseja excluir o registro (ID: " + cp.id + ")?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    cp.excluido = true;
                    s_cp.Update_CondicaoPagamento(cp);

                    gridControl_CondPag.DataSource = s_cp.GetAll_CondicaoPagamento();
                }
            }
            
        }
        #endregion

        #region ToolBar2 Events - Cancelar/Gravar
        private void barButtonItem_Gravar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (add_status == true &&
                textEdit_CondPagNome.Text != "")
            {
                cp = new CondicaoPagamento();
                cp.nome = textEdit_CondPagNome.Text;
                if(textEdit_CondPagValorMin.EditValue.ToString() != "")
                {
                    cp.valor_minimo = double.Parse(textEdit_CondPagValorMin.EditValue.ToString(), CultureInfo.InvariantCulture.NumberFormat);
                }
                cp.excluido = textEdit_CondPagExcluido.IsOn;
                cp.ultima_alteracao = DateTime.Now;
                cp.mptran = "I";

                //Validação Nome
                var aux = s_cp.GetByNome_CondicaoPagamento(cp.nome);

                if (aux == null)
                {
                    splashScreenManager_Transp.ShowWaitForm();

                    //Add new ProdCat
                    s_cp.AddNew_CondicaoPagamento(cp);

                    splashScreenManager_Transp.CloseWaitForm();

                    XtraMessageBox.Show("Registro salvo!", "Informação");

                    FormContols_FW.fieldsClean(layoutControl_CondPag);

                    gridControl_CondPag.DataSource = s_cp.GetAll_CondicaoPagamento();

                    bar_Acoes.Visible = true;
                    bar_Acoes2.Visible = false;
                    tabNavigationPage_RegisterCondPag.PageVisible = false;
                    tabNavigationPage_DataGridCondPag.PageVisible = true;
                    tabPane_ConfigCondPag.SelectedPage = tabNavigationPage_DataGridCondPag;
                }
                else
                {
                    splashScreenManager_Transp.CloseWaitForm();

                    XtraMessageBox.Show("Já existe uma transportadora com este nome cadastrado!", "Atenção");
                }

            }
            else if (add_status == false &&
                textEdit_CondPagNome.Text != "")
            {
                splashScreenManager_Transp.ShowWaitForm();

                cp = new CondicaoPagamento();
                cp.id = _id;

                cp = s_cp.GetBySpecification_CondicaoPagamento(cp.id);

                cp.nome = textEdit_CondPagNome.Text;
                cp.valor_minimo = double.Parse(textEdit_CondPagValorMin.EditValue.ToString(), CultureInfo.InvariantCulture.NumberFormat);
                cp.excluido = textEdit_CondPagExcluido.IsOn;
                cp.ultima_alteracao = DateTime.Now;
                cp.mptran = "A";

                s_cp.Update_CondicaoPagamento(cp);

                splashScreenManager_Transp.CloseWaitForm();

                XtraMessageBox.Show("Alteração realizada com sucesso!", "Informação");

                FormContols_FW.fieldsClean(layoutControl_CondPag);

                gridControl_CondPag.DataSource = s_cp.GetAll_CondicaoPagamento();

                bar_Acoes.Visible = true;
                bar_Acoes2.Visible = false;
                tabNavigationPage_RegisterCondPag.PageVisible = false;
                tabNavigationPage_DataGridCondPag.PageVisible = true;
                tabPane_ConfigCondPag.SelectedPage = tabNavigationPage_DataGridCondPag;
            }
            else
            {
                XtraMessageBox.Show("Algum campo obrigatório precisa ser preenchido!", "Informação");
            }
        }

        private void barButtonItem_Cancelar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (XtraMessageBox.Show("Tem certeza que deseja sair da tela?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                barButtonItem_Gravar.Enabled = true;

                bar_Acoes.Visible = true;
                bar_Acoes2.Visible = false;
                tabNavigationPage_RegisterCondPag.PageVisible = false;
                tabNavigationPage_DataGridCondPag.PageVisible = true;
                tabPane_ConfigCondPag.SelectedPage = tabNavigationPage_DataGridCondPag;

                FormContols_FW.fieldsClean(layoutControl_CondPag);
            }
        }
        #endregion

        #region Grid Transportadora Events
        private void gridView_Clientes_DoubleClick(object sender, EventArgs e)
        {
            barButtonItem_Gravar.Enabled = false;

            //Get (object) row values
            var row = gridView_CondPag.GetRow(gridView_CondPag.GetFocusedDataSourceRowIndex());
            cp = (CondicaoPagamento)row;

            //Fill TextEdit Fields
            textEdit_CondPagNome.Text = cp.nome;
            textEdit_CondPagValorMin.EditValue = cp.valor_minimo.ToString();
            textEdit_CondPagExcluido.EditValue = cp.excluido;
            dateEdit_CondPagUltAlteracao.DateTime = cp.ultima_alteracao;

            //Define TextEdit Fields as ReadOnly = true
            FormContols_FW.fieldsReadOnly(layoutControl_CondPag, true);

            //UI Behavior
            bar_Acoes.Visible = false;
            bar_Acoes2.Visible = true;

            tabNavigationPage_RegisterCondPag.PageVisible = true;
            tabNavigationPage_DataGridCondPag.PageVisible = false;
            tabPane_ConfigCondPag.SelectedPage = tabNavigationPage_RegisterCondPag;
        }
        #endregion

        private void FormTabelaPreco_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'meusPedidosDataSet.CondicaoPagamento' table. You can move, or remove it, as needed.
            this.condicaoPagamentoTableAdapter.Fill(this.meusPedidosDataSet.CondicaoPagamento);
        }
    }
}

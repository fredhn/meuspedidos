﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout;
using DevExpress.XtraLayout.Helpers;
using Core;
using Control;
using DevExpress.XtraEditors.Controls;

namespace View_MeusPedidos
{
    public partial class FormPedidoItem : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        Produto_Service s_prod;
        TabelaPreco_Service s_tabpreco;

        public QuantidadeGrades qtdGradeItem;
        public ItemPedido item_p;
        List<QuantidadeGrades> list_grades;
        string ax_descontos;

        public List<QuantidadeGrades> GetListGrades
        {
            get { return list_grades; }
        }

        public FormPedidoItem()
        {
            InitializeComponent();

            s_prod = new Produto_Service();
            s_tabpreco = new TabelaPreco_Service();

            list_grades = new List<QuantidadeGrades>();
            item_p = new ItemPedido();

            textEdit_Moeda.Properties.Items.AddRange(typeof(Core.Moeda_Type).GetEnumValues());
            textEdit_TipoIPI.Properties.Items.AddRange(typeof(Core.IPI_Type).GetEnumValues());

            //Populate Comboboxes:

            //Produto
            BindingSource srcProd = new BindingSource();
            srcProd.DataSource = s_prod.GetAll_FullProdutos();
            this.textEdit_Prod.Properties.DataSource = srcProd.List;
            this.textEdit_Prod.Properties.Columns.Add(new LookUpColumnInfo("id", "Id"));
            this.textEdit_Prod.Properties.Columns.Add(new LookUpColumnInfo("nome", "Nome"));
            this.textEdit_Prod.Properties.DisplayMember = "nome";
            this.textEdit_Prod.Properties.ValueMember = "id";

            //Tabela Preço
            BindingSource srcTabPreco = new BindingSource();
            srcTabPreco.DataSource = s_tabpreco.GetAll_TabelaPreco();
            this.textEdit_TabelaPreco.Properties.DataSource = srcTabPreco.List;
            this.textEdit_TabelaPreco.Properties.Columns.Add(new LookUpColumnInfo("id", "Id"));
            this.textEdit_TabelaPreco.Properties.Columns.Add(new LookUpColumnInfo("nome", "Nome"));
            this.textEdit_TabelaPreco.Properties.DisplayMember = "nome";
            this.textEdit_TabelaPreco.Properties.ValueMember = "id";
        }
        
        private void bbiSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (textEdit_Prod.Text.ToString() != "" &&
               textEdit_Quantidade.Text.ToString() != "" &&
               textEdit_PrecoBruto.EditValue.ToString() != "" &&
               textEdit_CotacaoMoeda.Text != "" &&
               textEdit_TipoIPI.Text != "")
            {
                List<QuantidadeGrades> qtd_grade = new List<QuantidadeGrades>();
                item_p = new ItemPedido();

                item_p.produto_id = int.Parse(textEdit_Prod.EditValue.ToString());
                item_p.produto_nome = textEdit_Prod.Text;
                item_p.tabela_preco_id = int.Parse(textEdit_TabelaPreco.EditValue.ToString());
                item_p.quantidade = double.Parse(textEdit_Quantidade.Text.ToString());
                item_p.produto_nome = textEdit_Prod.Text;
                if (textEdit_TipoIPI.Text.ToString() == "Percentual")
                {
                    item_p.tipo_ipi = "P";
                }
                else if (textEdit_TipoIPI.Text.ToString() == "Valor")
                {
                    item_p.tipo_ipi = "V";
                } 
                if(textEdit_IPI.Text.ToString() != "")
                {
                    item_p.ipi = double.Parse(textEdit_IPI.Text.ToString());
                }
                item_p.st = double.Parse(textEdit_ST.Text);
                item_p.preco_bruto = double.Parse(textEdit_PrecoBruto.Text);
                item_p.preco_minimo = double.Parse(textEdit_PrecoMin.EditValue.ToString());
                item_p.preco_liquido = double.Parse(textEdit_PrecoLiquido.EditValue.ToString());
                item_p.moeda = int.Parse(textEdit_Moeda.SelectedIndex.ToString());
                item_p.cotacao_moeda = double.Parse(textEdit_CotacaoMoeda.EditValue.ToString());
                foreach(var cbi in comboBoxEdit_DescontosList.Properties.Items)
                {
                    ax_descontos = ax_descontos + cbi.ToString() + " / ";
                } //Atribuir valores corretos aos descontos
                item_p.descontos = ax_descontos;
                item_p.observacoes = memoEdit_Obs.Text;

                FormContols_FW.fieldsClean(dataLayoutControl1);
                this.Hide();
            }    //item_p.descontos = double.Parse(textEdit_Descontos.Text);
            else
            {
                XtraMessageBox.Show("Algum campo obrigatório precisa ser preenchido!", "Informação");
            }
        }

        private void FormPedidoItem_Load(object sender, EventArgs e)
        {

        }

        private void simpleButton_GradeQtd_Click(object sender, EventArgs e)
        {
            qtdGradeItem = new QuantidadeGrades();

            if (textEdit_TipoTamCor.Text == "Cor")
            {
                qtdGradeItem.cor = textEdit_GradeCorTamDesc.Text;
                qtdGradeItem.quantidade = double.Parse(textEdit_GradeCorTamQuantidade.Text);
                qtdGradeItem.tamanho = null;

                listBoxControl_GradeCorTam.Items.Add("Tipo: " + textEdit_TipoTamCor.Text + " / Cor: " + qtdGradeItem.cor + " / Quantidade: " + qtdGradeItem.quantidade.ToString());
            }
            else if (textEdit_TipoTamCor.Text == "Tamanho")
            {
                qtdGradeItem.tamanho = textEdit_GradeCorTamDesc.Text;
                qtdGradeItem.quantidade = double.Parse(textEdit_GradeCorTamQuantidade.Text);
                qtdGradeItem.cor = null;

                listBoxControl_GradeCorTam.Items.Add("Tipo: " + textEdit_TipoTamCor.Text + " / Tamanho: " + qtdGradeItem.tamanho + " / Quantidade: " + qtdGradeItem.quantidade.ToString());
            }

            list_grades.Add(qtdGradeItem);
            item_p.quantidade_grades = list_grades;

            //listBoxControl_GradeCorTam.Items.Clear();
            //listBoxControl_GradeCorTam.Refresh();
            
        }

        private void comboBoxEdit_DescontosList_Properties_ButtonPressed(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Index == 2)
            {
                //User clicked the "minus" button
                comboBoxEdit_DescontosList.Properties.Items.Remove(comboBoxEdit_DescontosList.SelectedItem);
                //Clear out the control
                comboBoxEdit_DescontosList.EditValue = null;
            }
        }

        private void textEdit_Descontos_Properties_EditValueChanged(object sender, EventArgs e)
        {
            string desconto = textEdit_Descontos.Text;
            comboBoxEdit_DescontosList.Properties.Items.Add(desconto);
        }

        private void simpleButton_DescontoAdd_Click(object sender, EventArgs e)
        {
            string desconto = textEdit_Descontos.Text;
            comboBoxEdit_DescontosList.Properties.Items.Add(desconto);
        }
    }
}
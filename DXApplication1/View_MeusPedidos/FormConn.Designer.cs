﻿namespace View_MeusPedidos
{
    partial class FormConn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormConn));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit_NomeDB = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_NomeServer = new DevExpress.XtraEditors.TextEdit();
            this.checkEdit_AutenticacaoSegura = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit_SenhaDB = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_UsuarioDB = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup_Conn = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup_ConnData = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem_NomeDB = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_NomeServer = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem_AutenticacaoSegura = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_UsuarioDB = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_SenhaDB = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleButton_Conectar = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_NomeDB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_NomeServer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_AutenticacaoSegura.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_SenhaDB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_UsuarioDB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_Conn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_ConnData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_NomeDB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_NomeServer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_AutenticacaoSegura)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_UsuarioDB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_SenhaDB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.textEdit_NomeDB);
            this.layoutControl1.Controls.Add(this.textEdit_NomeServer);
            this.layoutControl1.Controls.Add(this.checkEdit_AutenticacaoSegura);
            this.layoutControl1.Controls.Add(this.textEdit_SenhaDB);
            this.layoutControl1.Controls.Add(this.textEdit_UsuarioDB);
            this.layoutControl1.Controls.Add(this.simpleButton_Conectar);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup_Conn;
            this.layoutControl1.Size = new System.Drawing.Size(383, 320);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textEdit_NomeDB
            // 
            this.textEdit_NomeDB.Location = new System.Drawing.Point(159, 129);
            this.textEdit_NomeDB.Name = "textEdit_NomeDB";
            this.textEdit_NomeDB.Size = new System.Drawing.Size(190, 20);
            this.textEdit_NomeDB.StyleController = this.layoutControl1;
            this.textEdit_NomeDB.TabIndex = 4;
            // 
            // textEdit_NomeServer
            // 
            this.textEdit_NomeServer.Location = new System.Drawing.Point(159, 105);
            this.textEdit_NomeServer.Name = "textEdit_NomeServer";
            this.textEdit_NomeServer.Size = new System.Drawing.Size(190, 20);
            this.textEdit_NomeServer.StyleController = this.layoutControl1;
            this.textEdit_NomeServer.TabIndex = 5;
            // 
            // checkEdit_AutenticacaoSegura
            // 
            this.checkEdit_AutenticacaoSegura.Location = new System.Drawing.Point(159, 153);
            this.checkEdit_AutenticacaoSegura.Name = "checkEdit_AutenticacaoSegura";
            this.checkEdit_AutenticacaoSegura.Properties.Caption = "";
            this.checkEdit_AutenticacaoSegura.Size = new System.Drawing.Size(190, 19);
            this.checkEdit_AutenticacaoSegura.StyleController = this.layoutControl1;
            this.checkEdit_AutenticacaoSegura.TabIndex = 6;
            // 
            // textEdit_SenhaDB
            // 
            this.textEdit_SenhaDB.Location = new System.Drawing.Point(159, 200);
            this.textEdit_SenhaDB.Name = "textEdit_SenhaDB";
            this.textEdit_SenhaDB.Properties.UseSystemPasswordChar = true;
            this.textEdit_SenhaDB.Size = new System.Drawing.Size(190, 20);
            this.textEdit_SenhaDB.StyleController = this.layoutControl1;
            this.textEdit_SenhaDB.TabIndex = 4;
            // 
            // textEdit_UsuarioDB
            // 
            this.textEdit_UsuarioDB.Location = new System.Drawing.Point(159, 176);
            this.textEdit_UsuarioDB.Name = "textEdit_UsuarioDB";
            this.textEdit_UsuarioDB.Size = new System.Drawing.Size(190, 20);
            this.textEdit_UsuarioDB.StyleController = this.layoutControl1;
            this.textEdit_UsuarioDB.TabIndex = 5;
            // 
            // layoutControlGroup_Conn
            // 
            this.layoutControlGroup_Conn.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup_Conn.GroupBordersVisible = false;
            this.layoutControlGroup_Conn.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.layoutControlGroup_ConnData,
            this.layoutControlItem1,
            this.emptySpaceItem5,
            this.emptySpaceItem8,
            this.emptySpaceItem7});
            this.layoutControlGroup_Conn.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup_Conn.Name = "Root";
            this.layoutControlGroup_Conn.Size = new System.Drawing.Size(383, 320);
            this.layoutControlGroup_Conn.Text = "Conexão ao Banco de Dados";
            this.layoutControlGroup_Conn.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(10, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(343, 47);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(10, 300);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(353, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(10, 300);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(10, 234);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(105, 26);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup_ConnData
            // 
            this.layoutControlGroup_ConnData.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem_NomeDB,
            this.layoutControlItem_NomeServer,
            this.emptySpaceItem2,
            this.layoutControlItem_AutenticacaoSegura,
            this.layoutControlItem_UsuarioDB,
            this.layoutControlItem_SenhaDB,
            this.emptySpaceItem6});
            this.layoutControlGroup_ConnData.Location = new System.Drawing.Point(10, 47);
            this.layoutControlGroup_ConnData.Name = "layoutControlGroup_ConnData";
            this.layoutControlGroup_ConnData.Size = new System.Drawing.Size(343, 187);
            this.layoutControlGroup_ConnData.Text = "Dados de Conexão";
            // 
            // layoutControlItem_NomeDB
            // 
            this.layoutControlItem_NomeDB.Control = this.textEdit_NomeDB;
            this.layoutControlItem_NomeDB.Location = new System.Drawing.Point(0, 40);
            this.layoutControlItem_NomeDB.Name = "layoutControlItem_NomeDB";
            this.layoutControlItem_NomeDB.Size = new System.Drawing.Size(319, 24);
            this.layoutControlItem_NomeDB.Text = "Nome do Banco de Dados";
            this.layoutControlItem_NomeDB.TextSize = new System.Drawing.Size(122, 13);
            // 
            // layoutControlItem_NomeServer
            // 
            this.layoutControlItem_NomeServer.Control = this.textEdit_NomeServer;
            this.layoutControlItem_NomeServer.Location = new System.Drawing.Point(0, 16);
            this.layoutControlItem_NomeServer.Name = "layoutControlItem_NomeServer";
            this.layoutControlItem_NomeServer.Size = new System.Drawing.Size(319, 24);
            this.layoutControlItem_NomeServer.Text = "Nome do Servidor";
            this.layoutControlItem_NomeServer.TextSize = new System.Drawing.Size(122, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(319, 16);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem_AutenticacaoSegura
            // 
            this.layoutControlItem_AutenticacaoSegura.Control = this.checkEdit_AutenticacaoSegura;
            this.layoutControlItem_AutenticacaoSegura.Location = new System.Drawing.Point(0, 64);
            this.layoutControlItem_AutenticacaoSegura.Name = "layoutControlItem_AutenticacaoSegura";
            this.layoutControlItem_AutenticacaoSegura.Size = new System.Drawing.Size(319, 23);
            this.layoutControlItem_AutenticacaoSegura.Text = "Autenticação Segura";
            this.layoutControlItem_AutenticacaoSegura.TextSize = new System.Drawing.Size(122, 13);
            // 
            // layoutControlItem_UsuarioDB
            // 
            this.layoutControlItem_UsuarioDB.Control = this.textEdit_UsuarioDB;
            this.layoutControlItem_UsuarioDB.CustomizationFormText = "Nome do Servidor";
            this.layoutControlItem_UsuarioDB.Location = new System.Drawing.Point(0, 87);
            this.layoutControlItem_UsuarioDB.Name = "layoutControlItem_UsuarioDB";
            this.layoutControlItem_UsuarioDB.Size = new System.Drawing.Size(319, 24);
            this.layoutControlItem_UsuarioDB.Text = "Usuário";
            this.layoutControlItem_UsuarioDB.TextSize = new System.Drawing.Size(122, 13);
            // 
            // layoutControlItem_SenhaDB
            // 
            this.layoutControlItem_SenhaDB.Control = this.textEdit_SenhaDB;
            this.layoutControlItem_SenhaDB.CustomizationFormText = "Nome do Banco de Dados";
            this.layoutControlItem_SenhaDB.Location = new System.Drawing.Point(0, 111);
            this.layoutControlItem_SenhaDB.Name = "layoutControlItem_SenhaDB";
            this.layoutControlItem_SenhaDB.Size = new System.Drawing.Size(319, 24);
            this.layoutControlItem_SenhaDB.Text = "Senha";
            this.layoutControlItem_SenhaDB.TextSize = new System.Drawing.Size(122, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 135);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(319, 10);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleButton_Conectar
            // 
            this.simpleButton_Conectar.Location = new System.Drawing.Point(127, 246);
            this.simpleButton_Conectar.Name = "simpleButton_Conectar";
            this.simpleButton_Conectar.Size = new System.Drawing.Size(129, 22);
            this.simpleButton_Conectar.StyleController = this.layoutControl1;
            this.simpleButton_Conectar.TabIndex = 7;
            this.simpleButton_Conectar.Text = "Conectar";
            this.simpleButton_Conectar.Click += new System.EventHandler(this.simpleButton_Conectar_Click);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.simpleButton_Conectar;
            this.layoutControlItem1.Location = new System.Drawing.Point(115, 234);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(133, 26);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(248, 234);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(105, 26);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.Location = new System.Drawing.Point(10, 260);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(343, 40);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // FormConn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(383, 320);
            this.Controls.Add(this.layoutControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormConn";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Conexão ao Banco de Dados";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_NomeDB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_NomeServer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_AutenticacaoSegura.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_SenhaDB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_UsuarioDB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_Conn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_ConnData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_NomeDB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_NomeServer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_AutenticacaoSegura)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_UsuarioDB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_SenhaDB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_Conn;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_ConnData;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.TextEdit textEdit_NomeDB;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_NomeDB;
        private DevExpress.XtraEditors.TextEdit textEdit_NomeServer;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_NomeServer;
        private DevExpress.XtraEditors.CheckEdit checkEdit_AutenticacaoSegura;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_AutenticacaoSegura;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraEditors.TextEdit textEdit_SenhaDB;
        private DevExpress.XtraEditors.TextEdit textEdit_UsuarioDB;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_UsuarioDB;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_SenhaDB;
        private DevExpress.XtraEditors.SimpleButton simpleButton_Conectar;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
    }
}
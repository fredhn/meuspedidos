﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Control;
using Core;
using DevExpress.XtraEditors;
using System.Data.Entity;
using Control.Validations;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid;

namespace View_MeusPedidos
{
    public partial class FormTransportadora : DevExpress.XtraEditors.XtraForm
    {
        Transportadora_Service s_transp;
        Transportadora transp;
        Telefone tel;

        List<Telefone> ltel;
        BindingList<Telefone> telBindingList = new BindingList<Telefone>();

        int _id;
        bool add_status = false;

        public FormTransportadora()
        {
            InitializeComponent();
            s_transp = new Transportadora_Service();

            bar_Acoes2.Visible = false;

            tabNavigationPage_RegisterTransp.PageVisible = false;
            tabPane_ConfigTransp.SelectedPage = tabNavigationPage_DataGridTransp;

            gridControl_Transp.DataSource = s_transp.GetAll_FullTransportadoras();

            //Parametros Aba Contatos (Carregando ComboBox)
            comboBoxEdit_TranspEstado.Properties.DataSource = Enum.GetValues(typeof(Core.Estados_BR));
            comboBoxEdit_TranspTelefoneTipo.Properties.DataSource = Enum.GetValues(typeof(Core.Telefone_Type));
        }

        #region ToolBar1 Events - Adicionar/Editar/Deletar

        private void barButtonItem_Add_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            add_status = true;

            //UI Behavior

            FormContols_FW.fieldsClean(layoutControl_Transp);
            gridControl_TranspTelefones.DataSource = null;
            gridView_TranspTelefones.Columns.Clear();
            gridControl_TranspTelefones.DataSource = telBindingList;

            bar_Acoes.Visible = false;
            bar_Acoes2.Visible = true;

            tabNavigationPage_RegisterTransp.PageVisible = true;
            tabNavigationPage_DataGridTransp.PageVisible = false;
            tabPane_ConfigTransp.SelectedPage = tabNavigationPage_RegisterTransp;

            //Define TextEdit Fields as ReadOnly = false
            FormContols_FW.fieldsReadOnly(layoutControl_Transp, false);
        }

        private void barButtonItem_Edit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            add_status = false;

            var row = gridView_Transp.GetRow(gridView_Transp.GetFocusedDataSourceRowIndex());
            transp = (Transportadora)row;

            if(transp != null)
            {
                if (XtraMessageBox.Show("Tem certeza que deseja alterar o registro (ID: " + transp.id + ")?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    //Retrieve entity ID
                    _id = transp.id;

                    //Fill TextEdit Fields
                    textEdit_TranspNome.Text = transp.nome;
                    textEdit_TranspCidade.Text = transp.cidade;
                    comboBoxEdit_TranspEstado.Text = transp.estado;
                    memoEdit_TranspInfoAd.Text = transp.informacoes_adicionais;
                    textEdit_TranspExcluido.EditValue = transp.excluido;
                    dateEdit_TranspUltAlteracao.DateTime = transp.ultima_alteracao;

                    //Load DataGrid with data
                    var transp_ = s_transp.GetBySpecification2_Transportadora(transp.id);
                    telBindingList = new BindingList<Telefone>(transp_.telefones);
                    gridControl_TranspTelefones.DataSource = telBindingList;
                    gridControl_TranspTelefones.RefreshDataSource();

                    //UI Behavior
                    bar_Acoes.Visible = false;
                    bar_Acoes2.Visible = true;

                    tabNavigationPage_RegisterTransp.PageVisible = true;
                    tabNavigationPage_DataGridTransp.PageVisible = false;
                    tabPane_ConfigTransp.SelectedPage = tabNavigationPage_RegisterTransp;

                    //Define TextEdit Fields as ReadOnly = false
                    FormContols_FW.fieldsReadOnly(layoutControl_Transp, false);
                }
            }
            
        }

        private void barButtonItem_Del_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var row = gridView_Transp.GetRow(gridView_Transp.GetFocusedDataSourceRowIndex());
            transp = (Transportadora)row;
            
            if(transp != null)
            {
                if (XtraMessageBox.Show("Tem certeza que deseja excluir o registro (ID: " + transp.id + ")?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    transp.excluido = true;
                    s_transp.Update_Transportadora(transp);

                    gridControl_Transp.DataSource = s_transp.GetAll_FullTransportadoras();
                }
            }
            
        }
        #endregion

        #region ToolBar2 Events - Cancelar/Gravar
        private void barButtonItem_Gravar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (add_status == true &&
                textEdit_TranspNome.Text != "")
            {
                transp = new Transportadora();
                transp.nome = textEdit_TranspNome.Text;
                transp.cidade = textEdit_TranspCidade.Text;
                transp.estado = comboBoxEdit_TranspEstado.Text;
                transp.informacoes_adicionais = memoEdit_TranspInfoAd.Text;
                transp.excluido = textEdit_TranspExcluido.IsOn;
                transp.ultima_alteracao = DateTime.Now;
                transp.mptran = "I";

                //Validação Nome
                var aux = s_transp.GetByNome_Transportadora(transp.nome);

                if (aux == null)
                {
                    splashScreenManager_Transp.ShowWaitForm();

                    if (gridView_TranspTelefones.RowCount > 0)
                    {
                        for (int i = 0; i < gridView_TranspTelefones.DataRowCount; i++)
                        {
                            var row = gridView_TranspTelefones.GetRow(gridView_TranspTelefones.GetVisibleRowHandle(i));
                            tel = (Telefone)row;

                            Telefone tel_ = new Telefone();
                            tel_.numero = tel.numero;
                            tel_.tipo = tel.tipo;

                            transp.telefones.Add(new Telefone() { tipo = tel_.tipo, numero = tel_.numero });
                            //..
                        }
                    }

                    //Add new ProdCat
                    s_transp.AddNew_Transportadora(transp);

                    splashScreenManager_Transp.CloseWaitForm();

                    XtraMessageBox.Show("Registro salvo!", "Informação");

                    FormContols_FW.fieldsClean(layoutControl_Transp);
                    gridControl_TranspTelefones.DataSource = null;
                    gridView_TranspTelefones.Columns.Clear();

                    gridControl_Transp.DataSource = s_transp.GetAll_FullTransportadoras();

                    bar_Acoes.Visible = true;
                    bar_Acoes2.Visible = false;
                    tabNavigationPage_RegisterTransp.PageVisible = false;
                    tabNavigationPage_DataGridTransp.PageVisible = true;
                    tabPane_ConfigTransp.SelectedPage = tabNavigationPage_DataGridTransp;
                }
                else
                {
                    splashScreenManager_Transp.CloseWaitForm();

                    XtraMessageBox.Show("Já existe uma transportadora com este nome cadastrado!", "Atenção");
                }

            }
            else if (add_status == false &&
                textEdit_TranspNome.Text != "")
            {
                splashScreenManager_Transp.ShowWaitForm();

                transp = new Transportadora();
                transp.id = _id;
                transp = s_transp.GetBySpecification2_Transportadora(transp.id);

                transp.nome = textEdit_TranspNome.Text;
                transp.cidade = textEdit_TranspCidade.Text;
                transp.estado = comboBoxEdit_TranspEstado.Text;
                transp.informacoes_adicionais = memoEdit_TranspInfoAd.Text;
                transp.excluido = textEdit_TranspExcluido.IsOn;
                transp.ultima_alteracao = DateTime.Now;
                transp.mptran = "A";

                ltel = new List<Telefone>();

                var rowCountTel = gridView_TranspTelefones.RowCount;
                for (int i = 0; i < rowCountTel; i++)
                {
                    var row = gridView_TranspTelefones.GetRow(gridView_TranspTelefones.GetVisibleRowHandle(i));
                    tel = (Telefone)row;

                    ltel.Add(tel);
                }

                s_transp.Update_Transportadora(transp);

                splashScreenManager_Transp.CloseWaitForm();

                XtraMessageBox.Show("Alteração realizada com sucesso!", "Informação");

                FormContols_FW.fieldsClean(layoutControl_Transp);
                gridControl_TranspTelefones.DataSource = null;
                gridView_TranspTelefones.Columns.Clear();

                gridControl_Transp.DataSource = s_transp.GetAll_FullTransportadoras();

                bar_Acoes.Visible = true;
                bar_Acoes2.Visible = false;
                tabNavigationPage_RegisterTransp.PageVisible = false;
                tabNavigationPage_DataGridTransp.PageVisible = true;
                tabPane_ConfigTransp.SelectedPage = tabNavigationPage_DataGridTransp;
            }
            else
            {
                XtraMessageBox.Show("Algum campo obrigatório precisa ser preenchido!", "Informação");
            }
        }

        private void barButtonItem_Cancelar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (XtraMessageBox.Show("Tem certeza que deseja sair da tela?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                barButtonItem_Gravar.Enabled = true;

                bar_Acoes.Visible = true;
                bar_Acoes2.Visible = false;
                tabNavigationPage_RegisterTransp.PageVisible = false;
                tabNavigationPage_DataGridTransp.PageVisible = true;
                tabPane_ConfigTransp.SelectedPage = tabNavigationPage_DataGridTransp;

                FormContols_FW.fieldsClean(layoutControl_Transp);
                telBindingList = new BindingList<Telefone>();
                gridControl_TranspTelefones.DataSource = null;
                gridView_TranspTelefones.Columns.Clear();
            }
        }
        #endregion

        #region Grid Transportadora Events
        private void gridView_Clientes_DoubleClick(object sender, EventArgs e)
        {
            barButtonItem_Gravar.Enabled = false;

            //Get (object) row values
            var row = gridView_Transp.GetRow(gridView_Transp.GetFocusedDataSourceRowIndex());
            transp = (Transportadora)row;

            //Fill TextEdit Fields
            textEdit_TranspNome.Text = transp.nome;
            textEdit_TranspCidade.Text = transp.cidade;
            comboBoxEdit_TranspEstado.Text = transp.estado;
            memoEdit_TranspInfoAd.Text = transp.informacoes_adicionais;
            textEdit_TranspExcluido.EditValue = transp.excluido;
            dateEdit_TranspUltAlteracao.DateTime = transp.ultima_alteracao;

            //Load DataGrid with data
            var transp_ = s_transp.GetBySpecification2_Transportadora(transp.id);
            telBindingList = new BindingList<Telefone>(transp_.telefones);
            gridControl_TranspTelefones.DataSource = telBindingList;
            gridControl_TranspTelefones.RefreshDataSource();

            //Define TextEdit Fields as ReadOnly = true
            FormContols_FW.fieldsReadOnly(layoutControl_Transp, true);

            //UI Behavior
            bar_Acoes.Visible = false;
            bar_Acoes2.Visible = true;

            tabNavigationPage_RegisterTransp.PageVisible = true;
            tabNavigationPage_DataGridTransp.PageVisible = false;
            tabPane_ConfigTransp.SelectedPage = tabNavigationPage_RegisterTransp;
        }
        #endregion

        #region Grid Transportadora - Telefone Events
        private void simpleButton_TranspTelefoneAdd_Click(object sender, EventArgs e)
        {
            tel = new Telefone();
            tel.numero = textEdit_TranspTelefoneNumero.Text;
            tel.tipo = comboBoxEdit_TranspTelefoneTipo.Text;

            telBindingList.Add(tel);
            gridControl_TranspTelefones.DataSource = telBindingList;
            gridView_TranspTelefones.RefreshData();
        }

        private void simpleButton_TranspTelefoneDel_Click(object sender, EventArgs e)
        {

        }

        private void gridControl_TranspTelefones_ProcessGridKey(object sender, KeyEventArgs e)
        {
            GridControl grid = sender as GridControl;
            GridView view = grid.FocusedView as GridView;
            if (e.KeyData == Keys.Delete)
            {
                view.DeleteSelectedRows();
                e.Handled = true;
            }
        }
        #endregion

        private void FormTransportadora_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'meusPedidosDataSet.Transportadora' table. You can move, or remove it, as needed.
            this.transportadoraTableAdapter.Fill(this.meusPedidosDataSet.Transportadora);

        }
    }
}

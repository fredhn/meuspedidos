﻿namespace View_MeusPedidos
{
    partial class FormEmpresa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.TileItemElement tileItemElement18 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement19 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement20 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement21 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement22 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement23 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement24 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement25 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement26 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement27 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement28 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement29 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement30 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement31 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement32 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement33 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement34 = new DevExpress.XtraEditors.TileItemElement();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormEmpresa));
            this.tileNavSubItem_Cliente = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CondicaoPagamentoCliente = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CategoriasCliente = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TabelaPreco = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Usuarios = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Produto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_ImagensProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TabelaProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TabelaPrecoProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CategoriaProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Pedido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CamposExtraPedido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Transportadora = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CondicaoPagamento = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_FormaPagamento = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_FaturamentoPedido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TituloVencido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.splashScreenManager_User = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::View_MeusPedidos.WaitForm1), true, true);
            this.simpleLabelItem_RegisterClientes = new DevExpress.XtraLayout.SimpleLabelItem();
            this.meusPedidosDataSet1 = new View_MeusPedidos.MeusPedidosDataSet();
            this.tabPane1 = new DevExpress.XtraBars.Navigation.TabPane();
            this.tabNavigationPage1 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.tabNavigationPage2 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit8 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit9 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit10 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit11 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit12 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit13 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit14 = new DevExpress.XtraEditors.TextEdit();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.toggleSwitch1 = new DevExpress.XtraEditors.ToggleSwitch();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleLabelItem1 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem16 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup3 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup4 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.simpleSeparator2 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator3 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlGroup_CLITelefone1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.colid1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colrazao_social = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colnome_fantasia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup_CatProd = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabNavigationPage_RegisterEmpresa = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.panelControl_RegisterEmpresa = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl_Empresa = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit_EmpresaCNPJ = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_EmpresaQtdLicencas = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_EmpresaSenha = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_EmpresaTolerancia = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_EmpresaRazaoSocial = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_EmpresaSysVersion = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_EmpresaComentarios = new DevExpress.XtraEditors.MemoEdit();
            this.dateEdit_UsuarioDtCriacao = new DevExpress.XtraEditors.DateEdit();
            this.textEdit_EmpresaExcluido = new DevExpress.XtraEditors.ToggleSwitch();
            this.dateEdit_EmpresaUltimaValidacao = new DevExpress.XtraEditors.DateEdit();
            this.toggleSwitch_EmpresaAtivo = new DevExpress.XtraEditors.ToggleSwitch();
            this.simpleButton_validarCNPJ = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton_gravarEmpresa = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup_RegisterEmpresa = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleLabelItem_RegisterUsuario = new DevExpress.XtraLayout.SimpleLabelItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup_Empresa = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem_EmpresaRazaoSocial = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_EmpresaComentarios1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_EmpresaToleranciaAposExpirar = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem_TabPrecoExcluido = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_EmpresaDataCriacao = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_EmpresaSenha = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_EmpresaUltimaValidacao = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_EmpresaAtivo = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_EmpresaQtdLicencas = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_EmpresaSysVersion = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem17 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem18 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem_EmpresaCNPJ = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem_btnGravar = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem19 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.barManager_Empresa = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barButtonItem_Add = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem_Edit = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem_Del = new DevExpress.XtraBars.BarButtonItem();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barButtonItem_Gravar = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem_Cancelar = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.layoutControlItem_EmpresaComentarios = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabNavigationPage_DataGridEmpresa = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.gridControl_Empresa = new DevExpress.XtraGrid.GridControl();
            this.gridView_Empresa = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcnpj = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colultima_validacao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colsystem_version = new DevExpress.XtraGrid.Columns.GridColumn();
            this.collicense_quantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colexpire_tolerance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colrazao_social1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colactive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colin_trash = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcreated_at = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.tabPane_ConfigEmpresa = new DevExpress.XtraBars.Navigation.TabPane();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem_RegisterClientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meusPedidosDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).BeginInit();
            this.tabPane1.SuspendLayout();
            this.tabNavigationPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toggleSwitch1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CLITelefone1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CatProd)).BeginInit();
            this.tabNavigationPage_RegisterEmpresa.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl_RegisterEmpresa)).BeginInit();
            this.panelControl_RegisterEmpresa.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_Empresa)).BeginInit();
            this.layoutControl_Empresa.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_EmpresaCNPJ.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_EmpresaQtdLicencas.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_EmpresaSenha.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_EmpresaTolerancia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_EmpresaRazaoSocial.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_EmpresaSysVersion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_EmpresaComentarios.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_UsuarioDtCriacao.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_UsuarioDtCriacao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_EmpresaExcluido.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_EmpresaUltimaValidacao.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_EmpresaUltimaValidacao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toggleSwitch_EmpresaAtivo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_RegisterEmpresa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem_RegisterUsuario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_Empresa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_EmpresaRazaoSocial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_EmpresaComentarios1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_EmpresaToleranciaAposExpirar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_TabPrecoExcluido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_EmpresaDataCriacao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_EmpresaSenha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_EmpresaUltimaValidacao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_EmpresaAtivo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_EmpresaQtdLicencas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_EmpresaSysVersion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_EmpresaCNPJ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_btnGravar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager_Empresa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_EmpresaComentarios)).BeginInit();
            this.tabNavigationPage_DataGridEmpresa.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_Empresa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_Empresa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane_ConfigEmpresa)).BeginInit();
            this.tabPane_ConfigEmpresa.SuspendLayout();
            this.SuspendLayout();
            // 
            // tileNavSubItem_Cliente
            // 
            this.tileNavSubItem_Cliente.Caption = "Cliente";
            this.tileNavSubItem_Cliente.Name = "tileNavSubItem_Cliente";
            // 
            // 
            // 
            this.tileNavSubItem_Cliente.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement18.Text = "Cliente";
            this.tileNavSubItem_Cliente.Tile.Elements.Add(tileItemElement18);
            this.tileNavSubItem_Cliente.Tile.Name = "tileBarItem3";
            // 
            // tileNavSubItem_CondicaoPagamentoCliente
            // 
            this.tileNavSubItem_CondicaoPagamentoCliente.Caption = "Condições de Pagamento por Cliente";
            this.tileNavSubItem_CondicaoPagamentoCliente.Name = "tileNavSubItem_CondicaoPagamentoCliente";
            // 
            // 
            // 
            this.tileNavSubItem_CondicaoPagamentoCliente.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement19.Text = "Condições de Pagamento por Cliente";
            this.tileNavSubItem_CondicaoPagamentoCliente.Tile.Elements.Add(tileItemElement19);
            this.tileNavSubItem_CondicaoPagamentoCliente.Tile.Name = "tileBarItem4";
            // 
            // tileNavSubItem_CategoriasCliente
            // 
            this.tileNavSubItem_CategoriasCliente.Caption = "Categorias por Cliente";
            this.tileNavSubItem_CategoriasCliente.Name = "tileNavSubItem_CategoriasCliente";
            // 
            // 
            // 
            this.tileNavSubItem_CategoriasCliente.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement20.Text = "Categorias por Cliente";
            this.tileNavSubItem_CategoriasCliente.Tile.Elements.Add(tileItemElement20);
            this.tileNavSubItem_CategoriasCliente.Tile.Name = "tileBarItem6";
            // 
            // tileNavSubItem_TabelaPreco
            // 
            this.tileNavSubItem_TabelaPreco.Caption = "Tabelas de Preço por Cliente";
            this.tileNavSubItem_TabelaPreco.Name = "tileNavSubItem_TabelaPreco";
            // 
            // 
            // 
            this.tileNavSubItem_TabelaPreco.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement21.Text = "Tabelas de Preço por Cliente";
            this.tileNavSubItem_TabelaPreco.Tile.Elements.Add(tileItemElement21);
            this.tileNavSubItem_TabelaPreco.Tile.Name = "tileBarItem7";
            // 
            // tileNavSubItem_Usuarios
            // 
            this.tileNavSubItem_Usuarios.Caption = "Usuários (Vendedores)";
            this.tileNavSubItem_Usuarios.Name = "tileNavSubItem_Usuarios";
            // 
            // 
            // 
            this.tileNavSubItem_Usuarios.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement22.Text = "Usuários (Vendedores)";
            this.tileNavSubItem_Usuarios.Tile.Elements.Add(tileItemElement22);
            this.tileNavSubItem_Usuarios.Tile.Name = "tileBarItem8";
            // 
            // tileNavSubItem_Produto
            // 
            this.tileNavSubItem_Produto.Caption = "Produto";
            this.tileNavSubItem_Produto.Name = "tileNavSubItem_Produto";
            // 
            // 
            // 
            this.tileNavSubItem_Produto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement23.Text = "Produto";
            this.tileNavSubItem_Produto.Tile.Elements.Add(tileItemElement23);
            this.tileNavSubItem_Produto.Tile.Name = "tileBarItem9";
            // 
            // tileNavSubItem_ImagensProduto
            // 
            this.tileNavSubItem_ImagensProduto.Caption = "Imagens do Produto";
            this.tileNavSubItem_ImagensProduto.Name = "tileNavSubItem_ImagensProduto";
            // 
            // 
            // 
            this.tileNavSubItem_ImagensProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement24.Text = "Imagens do Produto";
            this.tileNavSubItem_ImagensProduto.Tile.Elements.Add(tileItemElement24);
            this.tileNavSubItem_ImagensProduto.Tile.Name = "tileBarItem10";
            // 
            // tileNavSubItem_TabelaProduto
            // 
            this.tileNavSubItem_TabelaProduto.Caption = "Tabela de Preço";
            this.tileNavSubItem_TabelaProduto.Name = "tileNavSubItem_TabelaProduto";
            // 
            // 
            // 
            this.tileNavSubItem_TabelaProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement25.Text = "Tabelas de Preço";
            this.tileNavSubItem_TabelaProduto.Tile.Elements.Add(tileItemElement25);
            this.tileNavSubItem_TabelaProduto.Tile.Name = "tileBarItem11";
            // 
            // tileNavSubItem_TabelaPrecoProduto
            // 
            this.tileNavSubItem_TabelaPrecoProduto.Caption = "Tabelas de Preço por Produto";
            this.tileNavSubItem_TabelaPrecoProduto.Name = "tileNavSubItem_TabelaPrecoProduto";
            // 
            // 
            // 
            this.tileNavSubItem_TabelaPrecoProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement26.Text = "Tabelas de Preço por Produto";
            this.tileNavSubItem_TabelaPrecoProduto.Tile.Elements.Add(tileItemElement26);
            this.tileNavSubItem_TabelaPrecoProduto.Tile.Name = "tileBarItem12";
            // 
            // tileNavSubItem_CategoriaProduto
            // 
            this.tileNavSubItem_CategoriaProduto.Caption = "Categorias de Produtos";
            this.tileNavSubItem_CategoriaProduto.Name = "tileNavSubItem_CategoriaProduto";
            // 
            // 
            // 
            this.tileNavSubItem_CategoriaProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement27.Text = "Categorias de Produtos";
            this.tileNavSubItem_CategoriaProduto.Tile.Elements.Add(tileItemElement27);
            this.tileNavSubItem_CategoriaProduto.Tile.Name = "tileBarItem13";
            // 
            // tileNavSubItem_Pedido
            // 
            this.tileNavSubItem_Pedido.Caption = "Pedido";
            this.tileNavSubItem_Pedido.Name = "tileNavSubItem_Pedido";
            // 
            // 
            // 
            this.tileNavSubItem_Pedido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement28.Text = "Pedido";
            this.tileNavSubItem_Pedido.Tile.Elements.Add(tileItemElement28);
            this.tileNavSubItem_Pedido.Tile.Name = "tileBarItem17";
            // 
            // tileNavSubItem_CamposExtraPedido
            // 
            this.tileNavSubItem_CamposExtraPedido.Caption = "Campos Extras do Pedido";
            this.tileNavSubItem_CamposExtraPedido.Name = "tileNavSubItem_CamposExtraPedido";
            // 
            // 
            // 
            this.tileNavSubItem_CamposExtraPedido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement29.Text = "Campos Extras do Pedido";
            this.tileNavSubItem_CamposExtraPedido.Tile.Elements.Add(tileItemElement29);
            this.tileNavSubItem_CamposExtraPedido.Tile.Name = "tileBarItem18";
            // 
            // tileNavSubItem_Transportadora
            // 
            this.tileNavSubItem_Transportadora.Caption = "Transportadoras";
            this.tileNavSubItem_Transportadora.Name = "tileNavSubItem_Transportadora";
            // 
            // 
            // 
            this.tileNavSubItem_Transportadora.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement30.Text = "Transportadoras";
            this.tileNavSubItem_Transportadora.Tile.Elements.Add(tileItemElement30);
            this.tileNavSubItem_Transportadora.Tile.Name = "tileBarItem14";
            // 
            // tileNavSubItem_CondicaoPagamento
            // 
            this.tileNavSubItem_CondicaoPagamento.Caption = "Condições de Pagamento";
            this.tileNavSubItem_CondicaoPagamento.Name = "tileNavSubItem_CondicaoPagamento";
            // 
            // 
            // 
            this.tileNavSubItem_CondicaoPagamento.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement31.Text = "Condições de Pagamento";
            this.tileNavSubItem_CondicaoPagamento.Tile.Elements.Add(tileItemElement31);
            this.tileNavSubItem_CondicaoPagamento.Tile.Name = "tileBarItem15";
            // 
            // tileNavSubItem_FormaPagamento
            // 
            this.tileNavSubItem_FormaPagamento.Caption = "Formas de Pagamento";
            this.tileNavSubItem_FormaPagamento.Name = "tileNavSubItem_FormaPagamento";
            // 
            // 
            // 
            this.tileNavSubItem_FormaPagamento.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement32.Text = "Formas de Pagamento";
            this.tileNavSubItem_FormaPagamento.Tile.Elements.Add(tileItemElement32);
            this.tileNavSubItem_FormaPagamento.Tile.Name = "tileBarItem16";
            // 
            // tileNavSubItem_FaturamentoPedido
            // 
            this.tileNavSubItem_FaturamentoPedido.Caption = "Faturamento de Pedido";
            this.tileNavSubItem_FaturamentoPedido.Name = "tileNavSubItem_FaturamentoPedido";
            // 
            // 
            // 
            this.tileNavSubItem_FaturamentoPedido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement33.Text = "Faturamento de Pedido";
            this.tileNavSubItem_FaturamentoPedido.Tile.Elements.Add(tileItemElement33);
            this.tileNavSubItem_FaturamentoPedido.Tile.Name = "tileBarItem19";
            // 
            // tileNavSubItem_TituloVencido
            // 
            this.tileNavSubItem_TituloVencido.Caption = "Títulos Vencidos";
            this.tileNavSubItem_TituloVencido.Name = "tileNavSubItem_TituloVencido";
            // 
            // 
            // 
            this.tileNavSubItem_TituloVencido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement34.Text = "Títulos Vencidos";
            this.tileNavSubItem_TituloVencido.Tile.Elements.Add(tileItemElement34);
            this.tileNavSubItem_TituloVencido.Tile.Name = "tileBarItem20";
            // 
            // splashScreenManager_User
            // 
            this.splashScreenManager_User.ClosingDelay = 500;
            // 
            // simpleLabelItem_RegisterClientes
            // 
            this.simpleLabelItem_RegisterClientes.AllowHotTrack = false;
            this.simpleLabelItem_RegisterClientes.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem_RegisterClientes.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem_RegisterClientes.Location = new System.Drawing.Point(10, 10);
            this.simpleLabelItem_RegisterClientes.Name = "simpleLabelItem_RegisterClientes";
            this.simpleLabelItem_RegisterClientes.OptionsPrint.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem_RegisterClientes.OptionsPrint.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem_RegisterClientes.Size = new System.Drawing.Size(1185, 17);
            this.simpleLabelItem_RegisterClientes.Text = "Cadastro Clientes";
            this.simpleLabelItem_RegisterClientes.TextSize = new System.Drawing.Size(155, 13);
            // 
            // meusPedidosDataSet1
            // 
            this.meusPedidosDataSet1.DataSetName = "MeusPedidosDataSet";
            this.meusPedidosDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tabPane1
            // 
            this.tabPane1.Controls.Add(this.tabNavigationPage1);
            this.tabPane1.Controls.Add(this.tabNavigationPage2);
            this.tabPane1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPane1.Location = new System.Drawing.Point(0, 94);
            this.tabPane1.Name = "tabPane1";
            this.tabPane1.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.tabNavigationPage1,
            this.tabNavigationPage2});
            this.tabPane1.RegularSize = new System.Drawing.Size(1264, 445);
            this.tabPane1.SelectedPage = this.tabNavigationPage2;
            this.tabPane1.Size = new System.Drawing.Size(1264, 445);
            this.tabPane1.TabIndex = 8;
            this.tabPane1.Text = "Clientes";
            // 
            // tabNavigationPage1
            // 
            this.tabNavigationPage1.Caption = "Clientes";
            this.tabNavigationPage1.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage1.Name = "tabNavigationPage1";
            this.tabNavigationPage1.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage1.Size = new System.Drawing.Size(1246, 381);
            // 
            // tabNavigationPage2
            // 
            this.tabNavigationPage2.Caption = "Cadastro";
            this.tabNavigationPage2.Controls.Add(this.panelControl1);
            this.tabNavigationPage2.Image = ((System.Drawing.Image)(resources.GetObject("tabNavigationPage2.Image")));
            this.tabNavigationPage2.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage2.Name = "tabNavigationPage2";
            this.tabNavigationPage2.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage2.Size = new System.Drawing.Size(1246, 381);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.layoutControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1246, 381);
            this.panelControl1.TabIndex = 0;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.textEdit2);
            this.layoutControl1.Controls.Add(this.textEdit3);
            this.layoutControl1.Controls.Add(this.textEdit4);
            this.layoutControl1.Controls.Add(this.textEdit5);
            this.layoutControl1.Controls.Add(this.textEdit6);
            this.layoutControl1.Controls.Add(this.textEdit7);
            this.layoutControl1.Controls.Add(this.textEdit8);
            this.layoutControl1.Controls.Add(this.textEdit9);
            this.layoutControl1.Controls.Add(this.textEdit10);
            this.layoutControl1.Controls.Add(this.textEdit11);
            this.layoutControl1.Controls.Add(this.textEdit12);
            this.layoutControl1.Controls.Add(this.textEdit13);
            this.layoutControl1.Controls.Add(this.textEdit14);
            this.layoutControl1.Controls.Add(this.dateEdit1);
            this.layoutControl1.Controls.Add(this.toggleSwitch1);
            this.layoutControl1.Controls.Add(this.memoEdit1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup3;
            this.layoutControl1.Size = new System.Drawing.Size(1242, 377);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textEdit2
            // 
            this.textEdit2.Location = new System.Drawing.Point(136, 83);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Size = new System.Drawing.Size(1072, 20);
            this.textEdit2.StyleController = this.layoutControl1;
            this.textEdit2.TabIndex = 4;
            this.textEdit2.Visible = false;
            // 
            // textEdit3
            // 
            this.textEdit3.Location = new System.Drawing.Point(136, 107);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Size = new System.Drawing.Size(1072, 20);
            this.textEdit3.StyleController = this.layoutControl1;
            this.textEdit3.TabIndex = 5;
            this.textEdit3.Visible = false;
            // 
            // textEdit4
            // 
            this.textEdit4.Location = new System.Drawing.Point(136, 131);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Properties.MaxLength = 1;
            this.textEdit4.Size = new System.Drawing.Size(1072, 20);
            this.textEdit4.StyleController = this.layoutControl1;
            this.textEdit4.TabIndex = 6;
            this.textEdit4.Visible = false;
            // 
            // textEdit5
            // 
            this.textEdit5.Location = new System.Drawing.Point(136, 155);
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Size = new System.Drawing.Size(1072, 20);
            this.textEdit5.StyleController = this.layoutControl1;
            this.textEdit5.TabIndex = 7;
            this.textEdit5.Visible = false;
            // 
            // textEdit6
            // 
            this.textEdit6.Location = new System.Drawing.Point(136, 179);
            this.textEdit6.Name = "textEdit6";
            this.textEdit6.Size = new System.Drawing.Size(1072, 20);
            this.textEdit6.StyleController = this.layoutControl1;
            this.textEdit6.TabIndex = 8;
            this.textEdit6.Visible = false;
            // 
            // textEdit7
            // 
            this.textEdit7.Location = new System.Drawing.Point(136, 203);
            this.textEdit7.Name = "textEdit7";
            this.textEdit7.Size = new System.Drawing.Size(1072, 20);
            this.textEdit7.StyleController = this.layoutControl1;
            this.textEdit7.TabIndex = 9;
            this.textEdit7.Visible = false;
            // 
            // textEdit8
            // 
            this.textEdit8.Location = new System.Drawing.Point(136, 303);
            this.textEdit8.Name = "textEdit8";
            this.textEdit8.Size = new System.Drawing.Size(1072, 20);
            this.textEdit8.StyleController = this.layoutControl1;
            this.textEdit8.TabIndex = 10;
            // 
            // textEdit9
            // 
            this.textEdit9.Location = new System.Drawing.Point(136, 327);
            this.textEdit9.Name = "textEdit9";
            this.textEdit9.Size = new System.Drawing.Size(1072, 20);
            this.textEdit9.StyleController = this.layoutControl1;
            this.textEdit9.TabIndex = 11;
            // 
            // textEdit10
            // 
            this.textEdit10.Location = new System.Drawing.Point(136, 351);
            this.textEdit10.Name = "textEdit10";
            this.textEdit10.Size = new System.Drawing.Size(1072, 20);
            this.textEdit10.StyleController = this.layoutControl1;
            this.textEdit10.TabIndex = 12;
            // 
            // textEdit11
            // 
            this.textEdit11.Location = new System.Drawing.Point(136, 375);
            this.textEdit11.Name = "textEdit11";
            this.textEdit11.Size = new System.Drawing.Size(1072, 20);
            this.textEdit11.StyleController = this.layoutControl1;
            this.textEdit11.TabIndex = 13;
            // 
            // textEdit12
            // 
            this.textEdit12.Location = new System.Drawing.Point(136, 399);
            this.textEdit12.Name = "textEdit12";
            this.textEdit12.Size = new System.Drawing.Size(1072, 20);
            this.textEdit12.StyleController = this.layoutControl1;
            this.textEdit12.TabIndex = 14;
            // 
            // textEdit13
            // 
            this.textEdit13.Location = new System.Drawing.Point(136, 423);
            this.textEdit13.Name = "textEdit13";
            this.textEdit13.Size = new System.Drawing.Size(1072, 20);
            this.textEdit13.StyleController = this.layoutControl1;
            this.textEdit13.TabIndex = 15;
            // 
            // textEdit14
            // 
            this.textEdit14.Location = new System.Drawing.Point(136, 227);
            this.textEdit14.Name = "textEdit14";
            this.textEdit14.Size = new System.Drawing.Size(1072, 20);
            this.textEdit14.StyleController = this.layoutControl1;
            this.textEdit14.TabIndex = 16;
            this.textEdit14.Visible = false;
            // 
            // dateEdit1
            // 
            this.dateEdit1.EditValue = null;
            this.dateEdit1.Location = new System.Drawing.Point(980, 499);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.ReadOnly = true;
            this.dateEdit1.Size = new System.Drawing.Size(228, 20);
            this.dateEdit1.StyleController = this.layoutControl1;
            this.dateEdit1.TabIndex = 17;
            // 
            // toggleSwitch1
            // 
            this.toggleSwitch1.Location = new System.Drawing.Point(980, 523);
            this.toggleSwitch1.Name = "toggleSwitch1";
            this.toggleSwitch1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.toggleSwitch1.Properties.OffText = "Não";
            this.toggleSwitch1.Properties.OnText = "Sim";
            this.toggleSwitch1.Size = new System.Drawing.Size(228, 24);
            this.toggleSwitch1.StyleController = this.layoutControl1;
            this.toggleSwitch1.TabIndex = 10;
            // 
            // memoEdit1
            // 
            this.memoEdit1.Location = new System.Drawing.Point(136, 499);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(728, 48);
            this.memoEdit1.StyleController = this.layoutControl1;
            this.memoEdit1.TabIndex = 18;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem6,
            this.emptySpaceItem8,
            this.emptySpaceItem11,
            this.emptySpaceItem12,
            this.simpleLabelItem1,
            this.emptySpaceItem13,
            this.emptySpaceItem14,
            this.layoutControlGroup4,
            this.emptySpaceItem15,
            this.layoutControlGroup5,
            this.tabbedControlGroup3});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "Root";
            this.layoutControlGroup3.Size = new System.Drawing.Size(1242, 581);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(10, 0);
            this.emptySpaceItem6.Name = "emptySpaceItem1";
            this.emptySpaceItem6.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.Location = new System.Drawing.Point(10, 251);
            this.emptySpaceItem8.Name = "emptySpaceItem2";
            this.emptySpaceItem8.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem11.Name = "emptySpaceItem3";
            this.emptySpaceItem11.Size = new System.Drawing.Size(10, 561);
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.Location = new System.Drawing.Point(1212, 0);
            this.emptySpaceItem12.Name = "emptySpaceItem4";
            this.emptySpaceItem12.Size = new System.Drawing.Size(10, 561);
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleLabelItem1
            // 
            this.simpleLabelItem1.AllowHotTrack = false;
            this.simpleLabelItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem1.Location = new System.Drawing.Point(10, 10);
            this.simpleLabelItem1.Name = "simpleLabelItem_RegisterClientes";
            this.simpleLabelItem1.OptionsPrint.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem1.OptionsPrint.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem1.Size = new System.Drawing.Size(1202, 17);
            this.simpleLabelItem1.Text = "Cadastro Clientes";
            this.simpleLabelItem1.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.Location = new System.Drawing.Point(10, 27);
            this.emptySpaceItem13.Name = "emptySpaceItem5";
            this.emptySpaceItem13.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.Location = new System.Drawing.Point(10, 447);
            this.emptySpaceItem14.Name = "emptySpaceItem7";
            this.emptySpaceItem14.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9});
            this.layoutControlGroup4.Location = new System.Drawing.Point(10, 261);
            this.layoutControlGroup4.Name = "layoutControlGroup_Endereço";
            this.layoutControlGroup4.Size = new System.Drawing.Size(1202, 186);
            this.layoutControlGroup4.Text = "Endereço";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.textEdit8;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem_CLIRua";
            this.layoutControlItem4.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem4.Text = "Rua";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.textEdit9;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem5.Name = "layoutControlItem_CLIComplemento";
            this.layoutControlItem5.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem5.Text = "Complemento";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.textEdit10;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem6.Name = "layoutControlItem_CLICep";
            this.layoutControlItem6.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem6.Text = "CEP";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.textEdit11;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem7.Name = "layoutControlItem_Bairro";
            this.layoutControlItem7.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem7.Text = "Bairro";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.textEdit12;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem8.Name = "layoutControlItem_Cidade";
            this.layoutControlItem8.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem8.Text = "Cidade";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.textEdit13;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem9.Name = "layoutControlItem_CLIEstado";
            this.layoutControlItem9.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem9.Text = "Estado";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.Location = new System.Drawing.Point(10, 551);
            this.emptySpaceItem15.Name = "emptySpaceItem9";
            this.emptySpaceItem15.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.emptySpaceItem16,
            this.layoutControlItem12});
            this.layoutControlGroup5.Location = new System.Drawing.Point(10, 457);
            this.layoutControlGroup5.Name = "layoutControlGroup1";
            this.layoutControlGroup5.Size = new System.Drawing.Size(1202, 94);
            this.layoutControlGroup5.Text = "Status";
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.dateEdit1;
            this.layoutControlItem10.Location = new System.Drawing.Point(844, 0);
            this.layoutControlItem10.Name = "layoutControlItem_CLIUltimaAlteracao";
            this.layoutControlItem10.Size = new System.Drawing.Size(334, 24);
            this.layoutControlItem10.Text = "Última Alteração";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.toggleSwitch1;
            this.layoutControlItem11.CustomizationFormText = "Status";
            this.layoutControlItem11.Location = new System.Drawing.Point(844, 24);
            this.layoutControlItem11.Name = "layoutControlItem_CLIExcluido";
            this.layoutControlItem11.Size = new System.Drawing.Size(334, 28);
            this.layoutControlItem11.Text = "Excluído";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem16
            // 
            this.emptySpaceItem16.AllowHotTrack = false;
            this.emptySpaceItem16.Location = new System.Drawing.Point(834, 0);
            this.emptySpaceItem16.Name = "emptySpaceItem10";
            this.emptySpaceItem16.Size = new System.Drawing.Size(10, 52);
            this.emptySpaceItem16.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.memoEdit1;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem12.Name = "layoutControlItem_CLIObservacao";
            this.layoutControlItem12.Size = new System.Drawing.Size(834, 52);
            this.layoutControlItem12.Text = "Observação";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(99, 13);
            // 
            // tabbedControlGroup3
            // 
            this.tabbedControlGroup3.Location = new System.Drawing.Point(10, 37);
            this.tabbedControlGroup3.Name = "tabbedControlGroup1";
            this.tabbedControlGroup3.SelectedTabPage = this.layoutControlGroup6;
            this.tabbedControlGroup3.SelectedTabPageIndex = 1;
            this.tabbedControlGroup3.Size = new System.Drawing.Size(1202, 214);
            this.tabbedControlGroup3.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup9,
            this.layoutControlGroup6});
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup4});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup_CLIContato";
            this.layoutControlGroup6.Size = new System.Drawing.Size(1178, 168);
            this.layoutControlGroup6.Text = "Contatos";
            // 
            // tabbedControlGroup4
            // 
            this.tabbedControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup4.Name = "tabbedControlGroup2";
            this.tabbedControlGroup4.SelectedTabPage = this.layoutControlGroup7;
            this.tabbedControlGroup4.SelectedTabPageIndex = 0;
            this.tabbedControlGroup4.Size = new System.Drawing.Size(1178, 168);
            this.tabbedControlGroup4.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup7,
            this.layoutControlGroup8});
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleSeparator2});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup_CLITelefone";
            this.layoutControlGroup7.Size = new System.Drawing.Size(1154, 122);
            this.layoutControlGroup7.Text = "Telefone";
            // 
            // simpleSeparator2
            // 
            this.simpleSeparator2.AllowHotTrack = false;
            this.simpleSeparator2.Location = new System.Drawing.Point(0, 0);
            this.simpleSeparator2.Name = "simpleSeparator1";
            this.simpleSeparator2.Size = new System.Drawing.Size(1154, 122);
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup8.Name = "layoutControlGroup_CLIEmail";
            this.layoutControlGroup8.Size = new System.Drawing.Size(1154, 122);
            this.layoutControlGroup8.Text = "E-mail";
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.layoutControlItem23});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup9.Name = "layoutControlGroup_CLIDados";
            this.layoutControlGroup9.Size = new System.Drawing.Size(1178, 168);
            this.layoutControlGroup9.Text = "Dados";
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.textEdit2;
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem17.Name = "layoutControlItem_CLIRazaoSocial";
            this.layoutControlItem17.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem17.Text = "Razão Social";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.textEdit3;
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem18.Name = "layoutControlItem_CLINomeFantasia";
            this.layoutControlItem18.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem18.Text = "Nome Fantasia";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.textEdit4;
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem19.Name = "layoutControlItem_CLITipo";
            this.layoutControlItem19.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem19.Text = "Tipo";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.textEdit5;
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem20.Name = "layoutControlItem_CLICnpj";
            this.layoutControlItem20.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem20.Text = "CNPJ";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.textEdit6;
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem21.Name = "layoutControlItem_CLIInscricaoEstadual";
            this.layoutControlItem21.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem21.Text = "Inscrição Estadual";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.textEdit7;
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem22.Name = "layoutControlItem_CLISuframa";
            this.layoutControlItem22.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem22.Text = "Suframa";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.textEdit14;
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItem23.Name = "layoutControlItem_CLINomeExcFiscal";
            this.layoutControlItem23.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem23.Text = "Nome Exceção Fiscal";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(99, 13);
            // 
            // simpleSeparator3
            // 
            this.simpleSeparator3.AllowHotTrack = false;
            this.simpleSeparator3.CustomizationFormText = "simpleSeparator1";
            this.simpleSeparator3.Location = new System.Drawing.Point(1005, 0);
            this.simpleSeparator3.Name = "simpleSeparator3";
            this.simpleSeparator3.Size = new System.Drawing.Size(2, 26);
            this.simpleSeparator3.Text = "simpleSeparator1";
            // 
            // layoutControlGroup_CLITelefone1
            // 
            this.layoutControlGroup_CLITelefone1.CustomizationFormText = "Telefone";
            this.layoutControlGroup_CLITelefone1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleSeparator3});
            this.layoutControlGroup_CLITelefone1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup_CLITelefone1.Name = "layoutControlGroup_CLITelefone1";
            this.layoutControlGroup_CLITelefone1.Size = new System.Drawing.Size(1205, 31);
            this.layoutControlGroup_CLITelefone1.Text = "Telefone";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup_CLITelefone1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 561);
            this.layoutControlGroup2.Name = "LayoutRootGroupForRestore";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1205, 31);
            this.layoutControlGroup2.Tag = "LayoutRootGroupForRestore";
            // 
            // colid1
            // 
            this.colid1.FieldName = "id";
            this.colid1.Name = "colid1";
            // 
            // colrazao_social
            // 
            this.colrazao_social.FieldName = "razao_social";
            this.colrazao_social.Name = "colrazao_social";
            // 
            // colnome_fantasia
            // 
            this.colnome_fantasia.FieldName = "nome_fantasia";
            this.colnome_fantasia.Name = "colnome_fantasia";
            // 
            // layoutControlGroup_CatProd
            // 
            this.layoutControlGroup_CatProd.Location = new System.Drawing.Point(10, 37);
            this.layoutControlGroup_CatProd.Name = "layoutControlGroup_CatProd";
            this.layoutControlGroup_CatProd.Size = new System.Drawing.Size(1202, 218);
            // 
            // tabNavigationPage_RegisterEmpresa
            // 
            this.tabNavigationPage_RegisterEmpresa.Caption = "Cadastro";
            this.tabNavigationPage_RegisterEmpresa.Controls.Add(this.panelControl_RegisterEmpresa);
            this.tabNavigationPage_RegisterEmpresa.Image = ((System.Drawing.Image)(resources.GetObject("tabNavigationPage_RegisterEmpresa.Image")));
            this.tabNavigationPage_RegisterEmpresa.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage_RegisterEmpresa.Name = "tabNavigationPage_RegisterEmpresa";
            this.tabNavigationPage_RegisterEmpresa.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage_RegisterEmpresa.Size = new System.Drawing.Size(1246, 404);
            // 
            // panelControl_RegisterEmpresa
            // 
            this.panelControl_RegisterEmpresa.Controls.Add(this.layoutControl_Empresa);
            this.panelControl_RegisterEmpresa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl_RegisterEmpresa.Location = new System.Drawing.Point(0, 0);
            this.panelControl_RegisterEmpresa.Name = "panelControl_RegisterEmpresa";
            this.panelControl_RegisterEmpresa.Size = new System.Drawing.Size(1246, 404);
            this.panelControl_RegisterEmpresa.TabIndex = 0;
            // 
            // layoutControl_Empresa
            // 
            this.layoutControl_Empresa.Controls.Add(this.textEdit_EmpresaCNPJ);
            this.layoutControl_Empresa.Controls.Add(this.textEdit_EmpresaQtdLicencas);
            this.layoutControl_Empresa.Controls.Add(this.textEdit_EmpresaSenha);
            this.layoutControl_Empresa.Controls.Add(this.textEdit_EmpresaTolerancia);
            this.layoutControl_Empresa.Controls.Add(this.textEdit_EmpresaRazaoSocial);
            this.layoutControl_Empresa.Controls.Add(this.textEdit_EmpresaSysVersion);
            this.layoutControl_Empresa.Controls.Add(this.textEdit_EmpresaComentarios);
            this.layoutControl_Empresa.Controls.Add(this.dateEdit_UsuarioDtCriacao);
            this.layoutControl_Empresa.Controls.Add(this.textEdit_EmpresaExcluido);
            this.layoutControl_Empresa.Controls.Add(this.dateEdit_EmpresaUltimaValidacao);
            this.layoutControl_Empresa.Controls.Add(this.toggleSwitch_EmpresaAtivo);
            this.layoutControl_Empresa.Controls.Add(this.simpleButton_validarCNPJ);
            this.layoutControl_Empresa.Controls.Add(this.simpleButton_gravarEmpresa);
            this.layoutControl_Empresa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl_Empresa.Location = new System.Drawing.Point(2, 2);
            this.layoutControl_Empresa.Name = "layoutControl_Empresa";
            this.layoutControl_Empresa.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(124, 125, 450, 400);
            this.layoutControl_Empresa.Root = this.layoutControlGroup_RegisterEmpresa;
            this.layoutControl_Empresa.Size = new System.Drawing.Size(1242, 400);
            this.layoutControl_Empresa.TabIndex = 0;
            this.layoutControl_Empresa.Text = "layoutControl_ProdCat";
            // 
            // textEdit_EmpresaCNPJ
            // 
            this.textEdit_EmpresaCNPJ.Location = new System.Drawing.Point(384, 113);
            this.textEdit_EmpresaCNPJ.Name = "textEdit_EmpresaCNPJ";
            this.textEdit_EmpresaCNPJ.Size = new System.Drawing.Size(510, 20);
            this.textEdit_EmpresaCNPJ.StyleController = this.layoutControl_Empresa;
            this.textEdit_EmpresaCNPJ.TabIndex = 7;
            // 
            // textEdit_EmpresaQtdLicencas
            // 
            this.textEdit_EmpresaQtdLicencas.Enabled = false;
            this.textEdit_EmpresaQtdLicencas.Location = new System.Drawing.Point(734, 174);
            this.textEdit_EmpresaQtdLicencas.Name = "textEdit_EmpresaQtdLicencas";
            this.textEdit_EmpresaQtdLicencas.Size = new System.Drawing.Size(60, 20);
            this.textEdit_EmpresaQtdLicencas.StyleController = this.layoutControl_Empresa;
            this.textEdit_EmpresaQtdLicencas.TabIndex = 7;
            // 
            // textEdit_EmpresaSenha
            // 
            this.textEdit_EmpresaSenha.Enabled = false;
            this.textEdit_EmpresaSenha.Location = new System.Drawing.Point(384, 79);
            this.textEdit_EmpresaSenha.Name = "textEdit_EmpresaSenha";
            this.textEdit_EmpresaSenha.Properties.UseSystemPasswordChar = true;
            this.textEdit_EmpresaSenha.Size = new System.Drawing.Size(594, 20);
            this.textEdit_EmpresaSenha.StyleController = this.layoutControl_Empresa;
            this.textEdit_EmpresaSenha.TabIndex = 7;
            // 
            // textEdit_EmpresaTolerancia
            // 
            this.textEdit_EmpresaTolerancia.Enabled = false;
            this.textEdit_EmpresaTolerancia.Location = new System.Drawing.Point(914, 174);
            this.textEdit_EmpresaTolerancia.Name = "textEdit_EmpresaTolerancia";
            this.textEdit_EmpresaTolerancia.Size = new System.Drawing.Size(64, 20);
            this.textEdit_EmpresaTolerancia.StyleController = this.layoutControl_Empresa;
            this.textEdit_EmpresaTolerancia.TabIndex = 7;
            // 
            // textEdit_EmpresaRazaoSocial
            // 
            this.textEdit_EmpresaRazaoSocial.Enabled = false;
            this.textEdit_EmpresaRazaoSocial.Location = new System.Drawing.Point(384, 139);
            this.textEdit_EmpresaRazaoSocial.Name = "textEdit_EmpresaRazaoSocial";
            this.textEdit_EmpresaRazaoSocial.Size = new System.Drawing.Size(594, 20);
            this.textEdit_EmpresaRazaoSocial.StyleController = this.layoutControl_Empresa;
            this.textEdit_EmpresaRazaoSocial.TabIndex = 7;
            // 
            // textEdit_EmpresaSysVersion
            // 
            this.textEdit_EmpresaSysVersion.Enabled = false;
            this.textEdit_EmpresaSysVersion.Location = new System.Drawing.Point(384, 174);
            this.textEdit_EmpresaSysVersion.Name = "textEdit_EmpresaSysVersion";
            this.textEdit_EmpresaSysVersion.Size = new System.Drawing.Size(230, 20);
            this.textEdit_EmpresaSysVersion.StyleController = this.layoutControl_Empresa;
            this.textEdit_EmpresaSysVersion.TabIndex = 7;
            // 
            // textEdit_EmpresaComentarios
            // 
            this.textEdit_EmpresaComentarios.Enabled = false;
            this.textEdit_EmpresaComentarios.Location = new System.Drawing.Point(384, 280);
            this.textEdit_EmpresaComentarios.MinimumSize = new System.Drawing.Size(0, 40);
            this.textEdit_EmpresaComentarios.Name = "textEdit_EmpresaComentarios";
            this.textEdit_EmpresaComentarios.Size = new System.Drawing.Size(594, 40);
            this.textEdit_EmpresaComentarios.StyleController = this.layoutControl_Empresa;
            this.textEdit_EmpresaComentarios.TabIndex = 7;
            // 
            // dateEdit_UsuarioDtCriacao
            // 
            this.dateEdit_UsuarioDtCriacao.EditValue = null;
            this.dateEdit_UsuarioDtCriacao.Enabled = false;
            this.dateEdit_UsuarioDtCriacao.Location = new System.Drawing.Point(384, 238);
            this.dateEdit_UsuarioDtCriacao.Name = "dateEdit_UsuarioDtCriacao";
            this.dateEdit_UsuarioDtCriacao.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit_UsuarioDtCriacao.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit_UsuarioDtCriacao.Size = new System.Drawing.Size(320, 20);
            this.dateEdit_UsuarioDtCriacao.StyleController = this.layoutControl_Empresa;
            this.dateEdit_UsuarioDtCriacao.TabIndex = 7;
            // 
            // textEdit_EmpresaExcluido
            // 
            this.textEdit_EmpresaExcluido.Enabled = false;
            this.textEdit_EmpresaExcluido.Location = new System.Drawing.Point(824, 242);
            this.textEdit_EmpresaExcluido.Name = "textEdit_EmpresaExcluido";
            this.textEdit_EmpresaExcluido.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.textEdit_EmpresaExcluido.Properties.OffText = "Não";
            this.textEdit_EmpresaExcluido.Properties.OnText = "Sim";
            this.textEdit_EmpresaExcluido.Size = new System.Drawing.Size(154, 24);
            this.textEdit_EmpresaExcluido.StyleController = this.layoutControl_Empresa;
            this.textEdit_EmpresaExcluido.TabIndex = 10;
            // 
            // dateEdit_EmpresaUltimaValidacao
            // 
            this.dateEdit_EmpresaUltimaValidacao.EditValue = null;
            this.dateEdit_EmpresaUltimaValidacao.Enabled = false;
            this.dateEdit_EmpresaUltimaValidacao.Location = new System.Drawing.Point(384, 214);
            this.dateEdit_EmpresaUltimaValidacao.Name = "dateEdit_EmpresaUltimaValidacao";
            this.dateEdit_EmpresaUltimaValidacao.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit_EmpresaUltimaValidacao.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit_EmpresaUltimaValidacao.Size = new System.Drawing.Size(320, 20);
            this.dateEdit_EmpresaUltimaValidacao.StyleController = this.layoutControl_Empresa;
            this.dateEdit_EmpresaUltimaValidacao.TabIndex = 7;
            // 
            // toggleSwitch_EmpresaAtivo
            // 
            this.toggleSwitch_EmpresaAtivo.Enabled = false;
            this.toggleSwitch_EmpresaAtivo.Location = new System.Drawing.Point(824, 214);
            this.toggleSwitch_EmpresaAtivo.Name = "toggleSwitch_EmpresaAtivo";
            this.toggleSwitch_EmpresaAtivo.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.toggleSwitch_EmpresaAtivo.Properties.OffText = "Não";
            this.toggleSwitch_EmpresaAtivo.Properties.OnText = "Sim";
            this.toggleSwitch_EmpresaAtivo.Size = new System.Drawing.Size(154, 24);
            this.toggleSwitch_EmpresaAtivo.StyleController = this.layoutControl_Empresa;
            this.toggleSwitch_EmpresaAtivo.TabIndex = 10;
            // 
            // simpleButton_validarCNPJ
            // 
            this.simpleButton_validarCNPJ.Location = new System.Drawing.Point(898, 113);
            this.simpleButton_validarCNPJ.Name = "simpleButton_validarCNPJ";
            this.simpleButton_validarCNPJ.Size = new System.Drawing.Size(80, 22);
            this.simpleButton_validarCNPJ.StyleController = this.layoutControl_Empresa;
            this.simpleButton_validarCNPJ.TabIndex = 11;
            this.simpleButton_validarCNPJ.Text = "Validar CNPJ";
            this.simpleButton_validarCNPJ.Click += new System.EventHandler(this.simpleButton_validarCNPJ_Click);
            // 
            // simpleButton_gravarEmpresa
            // 
            this.simpleButton_gravarEmpresa.Location = new System.Drawing.Point(841, 366);
            this.simpleButton_gravarEmpresa.Name = "simpleButton_gravarEmpresa";
            this.simpleButton_gravarEmpresa.Size = new System.Drawing.Size(149, 22);
            this.simpleButton_gravarEmpresa.StyleController = this.layoutControl_Empresa;
            this.simpleButton_gravarEmpresa.TabIndex = 12;
            this.simpleButton_gravarEmpresa.Text = "Salvar alterações";
            this.simpleButton_gravarEmpresa.Visible = false;
            this.simpleButton_gravarEmpresa.Click += new System.EventHandler(this.simpleButton_gravarEmpresa_Click);
            // 
            // layoutControlGroup_RegisterEmpresa
            // 
            this.layoutControlGroup_RegisterEmpresa.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup_RegisterEmpresa.GroupBordersVisible = false;
            this.layoutControlGroup_RegisterEmpresa.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.emptySpaceItem4,
            this.simpleLabelItem_RegisterUsuario,
            this.emptySpaceItem5,
            this.emptySpaceItem9,
            this.layoutControlGroup_Empresa,
            this.emptySpaceItem3,
            this.layoutControlItem_btnGravar,
            this.emptySpaceItem19});
            this.layoutControlGroup_RegisterEmpresa.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup_RegisterEmpresa.Name = "layoutControlGroup_RegisterEmpresa";
            this.layoutControlGroup_RegisterEmpresa.Size = new System.Drawing.Size(1242, 400);
            this.layoutControlGroup_RegisterEmpresa.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(244, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(738, 10);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(982, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(240, 380);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleLabelItem_RegisterUsuario
            // 
            this.simpleLabelItem_RegisterUsuario.AllowHotTrack = false;
            this.simpleLabelItem_RegisterUsuario.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem_RegisterUsuario.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem_RegisterUsuario.Location = new System.Drawing.Point(244, 10);
            this.simpleLabelItem_RegisterUsuario.Name = "simpleLabelItem_RegisterUsuario";
            this.simpleLabelItem_RegisterUsuario.OptionsPrint.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem_RegisterUsuario.OptionsPrint.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem_RegisterUsuario.Size = new System.Drawing.Size(738, 17);
            this.simpleLabelItem_RegisterUsuario.Text = "Cadastro de Empresa";
            this.simpleLabelItem_RegisterUsuario.TextSize = new System.Drawing.Size(113, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(244, 27);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(738, 10);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.Location = new System.Drawing.Point(244, 334);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(738, 20);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup_Empresa
            // 
            this.layoutControlGroup_Empresa.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem_EmpresaRazaoSocial,
            this.layoutControlItem_EmpresaComentarios1,
            this.layoutControlItem_EmpresaToleranciaAposExpirar,
            this.emptySpaceItem2,
            this.emptySpaceItem10,
            this.layoutControlItem_TabPrecoExcluido,
            this.layoutControlItem_EmpresaDataCriacao,
            this.layoutControlItem_EmpresaSenha,
            this.layoutControlItem_EmpresaUltimaValidacao,
            this.layoutControlItem_EmpresaAtivo,
            this.layoutControlItem_EmpresaQtdLicencas,
            this.layoutControlItem_EmpresaSysVersion,
            this.emptySpaceItem17,
            this.emptySpaceItem7,
            this.emptySpaceItem18,
            this.layoutControlItem_EmpresaCNPJ,
            this.layoutControlItem1});
            this.layoutControlGroup_Empresa.Location = new System.Drawing.Point(244, 37);
            this.layoutControlGroup_Empresa.Name = "layoutControlGroup_Empresa";
            this.layoutControlGroup_Empresa.Size = new System.Drawing.Size(738, 297);
            this.layoutControlGroup_Empresa.Text = "Empresa";
            // 
            // layoutControlItem_EmpresaRazaoSocial
            // 
            this.layoutControlItem_EmpresaRazaoSocial.Control = this.textEdit_EmpresaRazaoSocial;
            this.layoutControlItem_EmpresaRazaoSocial.CustomizationFormText = "CPF";
            this.layoutControlItem_EmpresaRazaoSocial.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItem_EmpresaRazaoSocial.Name = "layoutControlItem_EmpresaRazaoSocial";
            this.layoutControlItem_EmpresaRazaoSocial.Size = new System.Drawing.Size(714, 24);
            this.layoutControlItem_EmpresaRazaoSocial.Text = "Razão Social";
            this.layoutControlItem_EmpresaRazaoSocial.TextSize = new System.Drawing.Size(113, 13);
            // 
            // layoutControlItem_EmpresaComentarios1
            // 
            this.layoutControlItem_EmpresaComentarios1.Control = this.textEdit_EmpresaComentarios;
            this.layoutControlItem_EmpresaComentarios1.CustomizationFormText = "CPF";
            this.layoutControlItem_EmpresaComentarios1.Location = new System.Drawing.Point(0, 201);
            this.layoutControlItem_EmpresaComentarios1.Name = "layoutControlItem_EmpresaComentarios1";
            this.layoutControlItem_EmpresaComentarios1.Size = new System.Drawing.Size(714, 44);
            this.layoutControlItem_EmpresaComentarios1.Text = "Comentários";
            this.layoutControlItem_EmpresaComentarios1.TextSize = new System.Drawing.Size(113, 13);
            // 
            // layoutControlItem_EmpresaToleranciaAposExpirar
            // 
            this.layoutControlItem_EmpresaToleranciaAposExpirar.Control = this.textEdit_EmpresaTolerancia;
            this.layoutControlItem_EmpresaToleranciaAposExpirar.CustomizationFormText = "CPF";
            this.layoutControlItem_EmpresaToleranciaAposExpirar.Location = new System.Drawing.Point(530, 95);
            this.layoutControlItem_EmpresaToleranciaAposExpirar.Name = "layoutControlItem_EmpresaToleranciaAposExpirar";
            this.layoutControlItem_EmpresaToleranciaAposExpirar.Size = new System.Drawing.Size(184, 24);
            this.layoutControlItem_EmpresaToleranciaAposExpirar.Text = "Tolerância Após Expirar";
            this.layoutControlItem_EmpresaToleranciaAposExpirar.TextSize = new System.Drawing.Size(113, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 245);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(714, 10);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.Location = new System.Drawing.Point(0, 24);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(714, 10);
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem_TabPrecoExcluido
            // 
            this.layoutControlItem_TabPrecoExcluido.Control = this.textEdit_EmpresaExcluido;
            this.layoutControlItem_TabPrecoExcluido.CustomizationFormText = "Status";
            this.layoutControlItem_TabPrecoExcluido.Location = new System.Drawing.Point(440, 163);
            this.layoutControlItem_TabPrecoExcluido.Name = "layoutControlItem_TabPrecoExcluido";
            this.layoutControlItem_TabPrecoExcluido.Size = new System.Drawing.Size(274, 28);
            this.layoutControlItem_TabPrecoExcluido.Text = "Excluído";
            this.layoutControlItem_TabPrecoExcluido.TextSize = new System.Drawing.Size(113, 13);
            // 
            // layoutControlItem_EmpresaDataCriacao
            // 
            this.layoutControlItem_EmpresaDataCriacao.Control = this.dateEdit_UsuarioDtCriacao;
            this.layoutControlItem_EmpresaDataCriacao.CustomizationFormText = "Última Alteração";
            this.layoutControlItem_EmpresaDataCriacao.Location = new System.Drawing.Point(0, 159);
            this.layoutControlItem_EmpresaDataCriacao.Name = "layoutControlItem_EmpresaDataCriacao";
            this.layoutControlItem_EmpresaDataCriacao.Size = new System.Drawing.Size(440, 32);
            this.layoutControlItem_EmpresaDataCriacao.Text = "Data de Criação";
            this.layoutControlItem_EmpresaDataCriacao.TextSize = new System.Drawing.Size(113, 13);
            // 
            // layoutControlItem_EmpresaSenha
            // 
            this.layoutControlItem_EmpresaSenha.Control = this.textEdit_EmpresaSenha;
            this.layoutControlItem_EmpresaSenha.CustomizationFormText = "CPF";
            this.layoutControlItem_EmpresaSenha.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem_EmpresaSenha.Name = "layoutControlItem_EmpresaSenha";
            this.layoutControlItem_EmpresaSenha.Size = new System.Drawing.Size(714, 24);
            this.layoutControlItem_EmpresaSenha.Text = "Senha";
            this.layoutControlItem_EmpresaSenha.TextSize = new System.Drawing.Size(113, 13);
            // 
            // layoutControlItem_EmpresaUltimaValidacao
            // 
            this.layoutControlItem_EmpresaUltimaValidacao.Control = this.dateEdit_EmpresaUltimaValidacao;
            this.layoutControlItem_EmpresaUltimaValidacao.CustomizationFormText = "Última Alteração";
            this.layoutControlItem_EmpresaUltimaValidacao.Location = new System.Drawing.Point(0, 135);
            this.layoutControlItem_EmpresaUltimaValidacao.Name = "layoutControlItem_EmpresaUltimaValidacao";
            this.layoutControlItem_EmpresaUltimaValidacao.Size = new System.Drawing.Size(440, 24);
            this.layoutControlItem_EmpresaUltimaValidacao.Text = "Última Validação";
            this.layoutControlItem_EmpresaUltimaValidacao.TextSize = new System.Drawing.Size(113, 13);
            // 
            // layoutControlItem_EmpresaAtivo
            // 
            this.layoutControlItem_EmpresaAtivo.Control = this.toggleSwitch_EmpresaAtivo;
            this.layoutControlItem_EmpresaAtivo.CustomizationFormText = "Status";
            this.layoutControlItem_EmpresaAtivo.Location = new System.Drawing.Point(440, 135);
            this.layoutControlItem_EmpresaAtivo.Name = "layoutControlItem_EmpresaAtivo";
            this.layoutControlItem_EmpresaAtivo.Size = new System.Drawing.Size(274, 28);
            this.layoutControlItem_EmpresaAtivo.Text = "Ativo";
            this.layoutControlItem_EmpresaAtivo.TextSize = new System.Drawing.Size(113, 13);
            // 
            // layoutControlItem_EmpresaQtdLicencas
            // 
            this.layoutControlItem_EmpresaQtdLicencas.Control = this.textEdit_EmpresaQtdLicencas;
            this.layoutControlItem_EmpresaQtdLicencas.CustomizationFormText = "CPF";
            this.layoutControlItem_EmpresaQtdLicencas.Location = new System.Drawing.Point(350, 95);
            this.layoutControlItem_EmpresaQtdLicencas.Name = "layoutControlItem_EmpresaQtdLicencas";
            this.layoutControlItem_EmpresaQtdLicencas.Size = new System.Drawing.Size(180, 24);
            this.layoutControlItem_EmpresaQtdLicencas.Text = "Licenças";
            this.layoutControlItem_EmpresaQtdLicencas.TextSize = new System.Drawing.Size(113, 13);
            // 
            // layoutControlItem_EmpresaSysVersion
            // 
            this.layoutControlItem_EmpresaSysVersion.Control = this.textEdit_EmpresaSysVersion;
            this.layoutControlItem_EmpresaSysVersion.CustomizationFormText = "CPF";
            this.layoutControlItem_EmpresaSysVersion.Location = new System.Drawing.Point(0, 95);
            this.layoutControlItem_EmpresaSysVersion.Name = "layoutControlItem_EmpresaSysVersion";
            this.layoutControlItem_EmpresaSysVersion.Size = new System.Drawing.Size(350, 24);
            this.layoutControlItem_EmpresaSysVersion.Text = "Versão do Sistema";
            this.layoutControlItem_EmpresaSysVersion.TextSize = new System.Drawing.Size(113, 13);
            // 
            // emptySpaceItem17
            // 
            this.emptySpaceItem17.AllowHotTrack = false;
            this.emptySpaceItem17.Location = new System.Drawing.Point(0, 119);
            this.emptySpaceItem17.Name = "emptySpaceItem17";
            this.emptySpaceItem17.Size = new System.Drawing.Size(714, 16);
            this.emptySpaceItem17.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 191);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(714, 10);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem18
            // 
            this.emptySpaceItem18.AllowHotTrack = false;
            this.emptySpaceItem18.Location = new System.Drawing.Point(0, 84);
            this.emptySpaceItem18.Name = "emptySpaceItem18";
            this.emptySpaceItem18.Size = new System.Drawing.Size(714, 11);
            this.emptySpaceItem18.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem_EmpresaCNPJ
            // 
            this.layoutControlItem_EmpresaCNPJ.Control = this.textEdit_EmpresaCNPJ;
            this.layoutControlItem_EmpresaCNPJ.Location = new System.Drawing.Point(0, 34);
            this.layoutControlItem_EmpresaCNPJ.Name = "layoutControlItem_EmpresaCNPJ";
            this.layoutControlItem_EmpresaCNPJ.Size = new System.Drawing.Size(630, 26);
            this.layoutControlItem_EmpresaCNPJ.Text = "CNPJ";
            this.layoutControlItem_EmpresaCNPJ.TextSize = new System.Drawing.Size(113, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.simpleButton_validarCNPJ;
            this.layoutControlItem1.Location = new System.Drawing.Point(630, 34);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(84, 26);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(244, 380);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem_btnGravar
            // 
            this.layoutControlItem_btnGravar.ContentVisible = false;
            this.layoutControlItem_btnGravar.Control = this.simpleButton_gravarEmpresa;
            this.layoutControlItem_btnGravar.Location = new System.Drawing.Point(829, 354);
            this.layoutControlItem_btnGravar.Name = "layoutControlItem_btnGravar";
            this.layoutControlItem_btnGravar.Size = new System.Drawing.Size(153, 26);
            this.layoutControlItem_btnGravar.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem_btnGravar.TextVisible = false;
            // 
            // emptySpaceItem19
            // 
            this.emptySpaceItem19.AllowHotTrack = false;
            this.emptySpaceItem19.Location = new System.Drawing.Point(244, 354);
            this.emptySpaceItem19.Name = "emptySpaceItem19";
            this.emptySpaceItem19.Size = new System.Drawing.Size(585, 26);
            this.emptySpaceItem19.TextSize = new System.Drawing.Size(0, 0);
            // 
            // barManager_Empresa
            // 
            this.barManager_Empresa.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar2});
            this.barManager_Empresa.DockControls.Add(this.barDockControlTop);
            this.barManager_Empresa.DockControls.Add(this.barDockControlBottom);
            this.barManager_Empresa.DockControls.Add(this.barDockControlLeft);
            this.barManager_Empresa.DockControls.Add(this.barDockControlRight);
            this.barManager_Empresa.Form = this;
            this.barManager_Empresa.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem_Add,
            this.barButtonItem_Edit,
            this.barButtonItem_Del,
            this.barButtonItem_Gravar,
            this.barButtonItem_Cancelar});
            this.barManager_Empresa.MaxItemId = 5;
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Add),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Edit),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Del)});
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Tools";
            // 
            // barButtonItem_Add
            // 
            this.barButtonItem_Add.Caption = "Adicionar";
            this.barButtonItem_Add.Id = 0;
            this.barButtonItem_Add.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Add.ImageOptions.Image")));
            this.barButtonItem_Add.Name = "barButtonItem_Add";
            this.barButtonItem_Add.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barButtonItem_Edit
            // 
            this.barButtonItem_Edit.Caption = "Editar";
            this.barButtonItem_Edit.Id = 1;
            this.barButtonItem_Edit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Edit.ImageOptions.Image")));
            this.barButtonItem_Edit.Name = "barButtonItem_Edit";
            this.barButtonItem_Edit.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barButtonItem_Del
            // 
            this.barButtonItem_Del.Caption = "Excluir";
            this.barButtonItem_Del.Id = 2;
            this.barButtonItem_Del.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Del.ImageOptions.Image")));
            this.barButtonItem_Del.Name = "barButtonItem_Del";
            this.barButtonItem_Del.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bar2
            // 
            this.bar2.BarName = "Tools_GravarCancelar";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 1;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Gravar),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Cancelar)});
            this.bar2.OptionsBar.AllowRename = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Tools_GravarCancelar";
            this.bar2.Visible = false;
            // 
            // barButtonItem_Gravar
            // 
            this.barButtonItem_Gravar.Caption = "Gravar";
            this.barButtonItem_Gravar.Enabled = false;
            this.barButtonItem_Gravar.Id = 3;
            this.barButtonItem_Gravar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Gravar.ImageOptions.Image")));
            this.barButtonItem_Gravar.Name = "barButtonItem_Gravar";
            this.barButtonItem_Gravar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barButtonItem_Cancelar
            // 
            this.barButtonItem_Cancelar.Caption = "Cancelar";
            this.barButtonItem_Cancelar.Id = 4;
            this.barButtonItem_Cancelar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Cancelar.ImageOptions.Image")));
            this.barButtonItem_Cancelar.Name = "barButtonItem_Cancelar";
            this.barButtonItem_Cancelar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Cancelar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Cancelar_ItemClick_1);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager_Empresa;
            this.barDockControlTop.Size = new System.Drawing.Size(1264, 94);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 562);
            this.barDockControlBottom.Manager = this.barManager_Empresa;
            this.barDockControlBottom.Size = new System.Drawing.Size(1264, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 94);
            this.barDockControlLeft.Manager = this.barManager_Empresa;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 468);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1264, 94);
            this.barDockControlRight.Manager = this.barManager_Empresa;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 468);
            // 
            // layoutControlItem_EmpresaComentarios
            // 
            this.layoutControlItem_EmpresaComentarios.Control = this.textEdit_EmpresaComentarios;
            this.layoutControlItem_EmpresaComentarios.CustomizationFormText = "CPF";
            this.layoutControlItem_EmpresaComentarios.Location = new System.Drawing.Point(0, 212);
            this.layoutControlItem_EmpresaComentarios.Name = "layoutControlItem_EmpresaComentarios";
            this.layoutControlItem_EmpresaComentarios.Size = new System.Drawing.Size(699, 44);
            this.layoutControlItem_EmpresaComentarios.Text = "Comentários";
            this.layoutControlItem_EmpresaComentarios.TextSize = new System.Drawing.Size(113, 13);
            // 
            // tabNavigationPage_DataGridEmpresa
            // 
            this.tabNavigationPage_DataGridEmpresa.Caption = "Empresa";
            this.tabNavigationPage_DataGridEmpresa.Controls.Add(this.gridControl_Empresa);
            this.tabNavigationPage_DataGridEmpresa.Image = ((System.Drawing.Image)(resources.GetObject("tabNavigationPage_DataGridEmpresa.Image")));
            this.tabNavigationPage_DataGridEmpresa.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage_DataGridEmpresa.Name = "tabNavigationPage_DataGridEmpresa";
            this.tabNavigationPage_DataGridEmpresa.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage_DataGridEmpresa.Size = new System.Drawing.Size(1246, 404);
            // 
            // gridControl_Empresa
            // 
            this.gridControl_Empresa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl_Empresa.Location = new System.Drawing.Point(0, 0);
            this.gridControl_Empresa.MainView = this.gridView_Empresa;
            this.gridControl_Empresa.Name = "gridControl_Empresa";
            this.gridControl_Empresa.Size = new System.Drawing.Size(1246, 404);
            this.gridControl_Empresa.TabIndex = 0;
            this.gridControl_Empresa.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView_Empresa,
            this.gridView1});
            // 
            // gridView_Empresa
            // 
            this.gridView_Empresa.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colid,
            this.colcnpj,
            this.colultima_validacao,
            this.colsystem_version,
            this.collicense_quantity,
            this.colexpire_tolerance,
            this.colrazao_social1,
            this.colactive,
            this.colin_trash,
            this.colcreated_at});
            this.gridView_Empresa.GridControl = this.gridControl_Empresa;
            this.gridView_Empresa.Name = "gridView_Empresa";
            this.gridView_Empresa.OptionsBehavior.Editable = false;
            this.gridView_Empresa.OptionsBehavior.ReadOnly = true;
            this.gridView_Empresa.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView_Empresa.OptionsSelection.MultiSelect = true;
            this.gridView_Empresa.OptionsSelection.UseIndicatorForSelection = false;
            this.gridView_Empresa.OptionsView.ShowGroupPanel = false;
            this.gridView_Empresa.OptionsView.ShowIndicator = false;
            this.gridView_Empresa.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView_Empresa_MouseDown);
            this.gridView_Empresa.DoubleClick += new System.EventHandler(this.gridView_Clientes_DoubleClick);
            // 
            // colid
            // 
            this.colid.Caption = "ID";
            this.colid.FieldName = "id";
            this.colid.Name = "colid";
            this.colid.Visible = true;
            this.colid.VisibleIndex = 0;
            // 
            // colcnpj
            // 
            this.colcnpj.Caption = "CNPJ";
            this.colcnpj.FieldName = "cnpj";
            this.colcnpj.Name = "colcnpj";
            this.colcnpj.Visible = true;
            this.colcnpj.VisibleIndex = 1;
            // 
            // colultima_validacao
            // 
            this.colultima_validacao.Caption = "Última Validação";
            this.colultima_validacao.FieldName = "ultima_validacao";
            this.colultima_validacao.Name = "colultima_validacao";
            this.colultima_validacao.Visible = true;
            this.colultima_validacao.VisibleIndex = 2;
            // 
            // colsystem_version
            // 
            this.colsystem_version.Caption = "Versão";
            this.colsystem_version.FieldName = "system_version";
            this.colsystem_version.Name = "colsystem_version";
            this.colsystem_version.Visible = true;
            this.colsystem_version.VisibleIndex = 3;
            // 
            // collicense_quantity
            // 
            this.collicense_quantity.Caption = "Licenças";
            this.collicense_quantity.FieldName = "license_quantity";
            this.collicense_quantity.Name = "collicense_quantity";
            this.collicense_quantity.Visible = true;
            this.collicense_quantity.VisibleIndex = 4;
            // 
            // colexpire_tolerance
            // 
            this.colexpire_tolerance.Caption = "Prazo de Tolerância";
            this.colexpire_tolerance.FieldName = "expire_tolerance";
            this.colexpire_tolerance.Name = "colexpire_tolerance";
            this.colexpire_tolerance.Visible = true;
            this.colexpire_tolerance.VisibleIndex = 5;
            // 
            // colrazao_social1
            // 
            this.colrazao_social1.Caption = "Razão Social";
            this.colrazao_social1.FieldName = "razao_social";
            this.colrazao_social1.Name = "colrazao_social1";
            this.colrazao_social1.Visible = true;
            this.colrazao_social1.VisibleIndex = 6;
            // 
            // colactive
            // 
            this.colactive.Caption = "Ativo";
            this.colactive.FieldName = "active";
            this.colactive.Name = "colactive";
            this.colactive.Visible = true;
            this.colactive.VisibleIndex = 7;
            // 
            // colin_trash
            // 
            this.colin_trash.Caption = "Excluído";
            this.colin_trash.FieldName = "in_trash";
            this.colin_trash.Name = "colin_trash";
            this.colin_trash.Visible = true;
            this.colin_trash.VisibleIndex = 8;
            // 
            // colcreated_at
            // 
            this.colcreated_at.Caption = "Data da Criação";
            this.colcreated_at.FieldName = "created_at";
            this.colcreated_at.Name = "colcreated_at";
            this.colcreated_at.Visible = true;
            this.colcreated_at.VisibleIndex = 9;
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl_Empresa;
            this.gridView1.Name = "gridView1";
            // 
            // tabPane_ConfigEmpresa
            // 
            this.tabPane_ConfigEmpresa.Controls.Add(this.tabNavigationPage_DataGridEmpresa);
            this.tabPane_ConfigEmpresa.Controls.Add(this.tabNavigationPage_RegisterEmpresa);
            this.tabPane_ConfigEmpresa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPane_ConfigEmpresa.Location = new System.Drawing.Point(0, 94);
            this.tabPane_ConfigEmpresa.Name = "tabPane_ConfigEmpresa";
            this.tabPane_ConfigEmpresa.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.tabNavigationPage_DataGridEmpresa,
            this.tabNavigationPage_RegisterEmpresa});
            this.tabPane_ConfigEmpresa.RegularSize = new System.Drawing.Size(1264, 468);
            this.tabPane_ConfigEmpresa.SelectedPage = this.tabNavigationPage_RegisterEmpresa;
            this.tabPane_ConfigEmpresa.Size = new System.Drawing.Size(1264, 468);
            this.tabPane_ConfigEmpresa.TabIndex = 8;
            this.tabPane_ConfigEmpresa.Text = "Vendedores";
            // 
            // FormEmpresa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1264, 562);
            this.Controls.Add(this.tabPane_ConfigEmpresa);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormEmpresa";
            this.ShowMdiChildCaptionInParentTitle = true;
            this.Text = "Empresa";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormEmpresa_Load);
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem_RegisterClientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meusPedidosDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).EndInit();
            this.tabPane1.ResumeLayout(false);
            this.tabNavigationPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toggleSwitch1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CLITelefone1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CatProd)).EndInit();
            this.tabNavigationPage_RegisterEmpresa.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl_RegisterEmpresa)).EndInit();
            this.panelControl_RegisterEmpresa.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_Empresa)).EndInit();
            this.layoutControl_Empresa.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_EmpresaCNPJ.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_EmpresaQtdLicencas.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_EmpresaSenha.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_EmpresaTolerancia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_EmpresaRazaoSocial.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_EmpresaSysVersion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_EmpresaComentarios.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_UsuarioDtCriacao.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_UsuarioDtCriacao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_EmpresaExcluido.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_EmpresaUltimaValidacao.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_EmpresaUltimaValidacao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toggleSwitch_EmpresaAtivo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_RegisterEmpresa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem_RegisterUsuario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_Empresa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_EmpresaRazaoSocial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_EmpresaComentarios1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_EmpresaToleranciaAposExpirar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_TabPrecoExcluido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_EmpresaDataCriacao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_EmpresaSenha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_EmpresaUltimaValidacao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_EmpresaAtivo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_EmpresaQtdLicencas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_EmpresaSysVersion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_EmpresaCNPJ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_btnGravar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager_Empresa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_EmpresaComentarios)).EndInit();
            this.tabNavigationPage_DataGridEmpresa.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_Empresa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_Empresa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane_ConfigEmpresa)).EndInit();
            this.tabPane_ConfigEmpresa.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Cliente;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CondicaoPagamentoCliente;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CategoriasCliente;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TabelaPreco;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Usuarios;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Produto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_ImagensProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TabelaProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TabelaPrecoProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CategoriaProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Transportadora;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CondicaoPagamento;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_FormaPagamento;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Pedido;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CamposExtraPedido;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_FaturamentoPedido;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TituloVencido;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager_User;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem_RegisterClientes;
        private DevExpress.XtraBars.Navigation.TabPane tabPane1;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage1;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage2;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraEditors.TextEdit textEdit3;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        private DevExpress.XtraEditors.TextEdit textEdit6;
        private DevExpress.XtraEditors.TextEdit textEdit7;
        private DevExpress.XtraEditors.TextEdit textEdit8;
        private DevExpress.XtraEditors.TextEdit textEdit9;
        private DevExpress.XtraEditors.TextEdit textEdit10;
        private DevExpress.XtraEditors.TextEdit textEdit11;
        private DevExpress.XtraEditors.TextEdit textEdit12;
        private DevExpress.XtraEditors.TextEdit textEdit13;
        private DevExpress.XtraEditors.TextEdit textEdit14;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
        private DevExpress.XtraEditors.ToggleSwitch toggleSwitch1;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_CLITelefone1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraGrid.Columns.GridColumn colid1;
        private DevExpress.XtraGrid.Columns.GridColumn colrazao_social;
        private DevExpress.XtraGrid.Columns.GridColumn colnome_fantasia;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_CatProd;
        private MeusPedidosDataSet meusPedidosDataSet1;
        private DevExpress.XtraBars.Navigation.TabPane tabPane_ConfigEmpresa;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage_DataGridEmpresa;
        private DevExpress.XtraGrid.GridControl gridControl_Empresa;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView_Empresa;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage_RegisterEmpresa;
        private DevExpress.XtraEditors.PanelControl panelControl_RegisterEmpresa;
        private DevExpress.XtraLayout.LayoutControl layoutControl_Empresa;
        private DevExpress.XtraEditors.TextEdit textEdit_EmpresaCNPJ;
        private DevExpress.XtraEditors.TextEdit textEdit_EmpresaQtdLicencas;
        private DevExpress.XtraEditors.TextEdit textEdit_EmpresaSenha;
        private DevExpress.XtraEditors.TextEdit textEdit_EmpresaTolerancia;
        private DevExpress.XtraEditors.TextEdit textEdit_EmpresaRazaoSocial;
        private DevExpress.XtraEditors.TextEdit textEdit_EmpresaSysVersion;
        private DevExpress.XtraEditors.MemoEdit textEdit_EmpresaComentarios;
        private DevExpress.XtraEditors.DateEdit dateEdit_UsuarioDtCriacao;
        private DevExpress.XtraEditors.ToggleSwitch textEdit_EmpresaExcluido;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_RegisterEmpresa;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem_RegisterUsuario;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_Empresa;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_EmpresaCNPJ;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_EmpresaQtdLicencas;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_EmpresaToleranciaAposExpirar;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_EmpresaRazaoSocial;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_EmpresaSysVersion;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_EmpresaSenha;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_EmpresaComentarios;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_EmpresaDataCriacao;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_TabPrecoExcluido;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem17;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarManager barManager_Empresa;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Add;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Edit;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Del;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Gravar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Cancelar;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.DateEdit dateEdit_EmpresaUltimaValidacao;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_EmpresaUltimaValidacao;
        private DevExpress.XtraEditors.ToggleSwitch toggleSwitch_EmpresaAtivo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_EmpresaAtivo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_EmpresaComentarios1;
        private DevExpress.XtraGrid.Columns.GridColumn colid;
        private DevExpress.XtraGrid.Columns.GridColumn colcnpj;
        private DevExpress.XtraGrid.Columns.GridColumn colultima_validacao;
        private DevExpress.XtraGrid.Columns.GridColumn colsystem_version;
        private DevExpress.XtraGrid.Columns.GridColumn collicense_quantity;
        private DevExpress.XtraGrid.Columns.GridColumn colexpire_tolerance;
        private DevExpress.XtraGrid.Columns.GridColumn colrazao_social1;
        private DevExpress.XtraGrid.Columns.GridColumn colactive;
        private DevExpress.XtraGrid.Columns.GridColumn colin_trash;
        private DevExpress.XtraGrid.Columns.GridColumn colcreated_at;
        private DevExpress.XtraEditors.SimpleButton simpleButton_validarCNPJ;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.SimpleButton simpleButton_gravarEmpresa;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_btnGravar;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem19;
    }
}


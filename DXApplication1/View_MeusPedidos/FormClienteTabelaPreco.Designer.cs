﻿namespace View_MeusPedidos
{
    partial class FormClienteTabelaPreco
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement10 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement11 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement12 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement13 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement14 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement15 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement16 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement17 = new DevExpress.XtraEditors.TileItemElement();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormClienteTabelaPreco));
            this.tileNavSubItem_Cliente = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CondicaoPagamentoCliente = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CategoriasCliente = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TabelaPreco = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Usuarios = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Produto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_ImagensProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TabelaProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TabelaPrecoProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CategoriaProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Pedido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CamposExtraPedido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Transportadora = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CondicaoPagamento = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_FormaPagamento = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_FaturamentoPedido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TituloVencido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.splashScreenManager_Transp = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::View_MeusPedidos.WaitForm1), true, true);
            this.tabNavigationPage_RegisterCliTabPreco = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.panelControl_RegisterCliTabPreco = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl_CliTabPreco = new DevExpress.XtraLayout.LayoutControl();
            this.comboBoxEdit_CliTabPreco_Cli = new DevExpress.XtraEditors.LookUpEdit();
            this.barManager_ContasMP = new DevExpress.XtraBars.BarManager(this.components);
            this.bar_Acoes = new DevExpress.XtraBars.Bar();
            this.barButtonItem_Add = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem_Edit = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem_Del = new DevExpress.XtraBars.BarButtonItem();
            this.bar_Acoes2 = new DevExpress.XtraBars.Bar();
            this.barButtonItem_Gravar = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem_Cancelar = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.comboBoxEdit_CliTabPreco_TabPreco = new DevExpress.XtraEditors.LookUpEdit();
            this.simpleButton_CliTabPrecoAdd = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit_CliCPSelectTabPreco = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup_RegisterCliTabPreco = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleLabelItem_RegisterCliTabPreco = new DevExpress.XtraLayout.SimpleLabelItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup_CliTabPreco = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem_CliTabPreco_CliID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem17 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem_ProdTabPreco_CliTabPreco_TabPreco = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_CliCPSelectCPs = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.meusPedidosDataSet = new View_MeusPedidos.MeusPedidosDataSet();
            this.simpleLabelItem_RegisterClientes = new DevExpress.XtraLayout.SimpleLabelItem();
            this.tabNavigationPage_DataGridCliTabPreco = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.gridControl_CliTabPreco = new DevExpress.XtraGrid.GridControl();
            this.gridView_CliTabPreco = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcliente_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coltabelas_liberadas = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coltabelas_liberadas_desc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colmptran = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tabPane_ConfigCliTabPreco = new DevExpress.XtraBars.Navigation.TabPane();
            this.tabPane1 = new DevExpress.XtraBars.Navigation.TabPane();
            this.tabNavigationPage1 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.tabNavigationPage2 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit8 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit9 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit10 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit11 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit12 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit13 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit14 = new DevExpress.XtraEditors.TextEdit();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.toggleSwitch1 = new DevExpress.XtraEditors.ToggleSwitch();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleLabelItem1 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem16 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup3 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup4 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.simpleSeparator2 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator3 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlGroup_CLITelefone1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.colid1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colrazao_social = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colnome_fantasia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup_CatProd = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabNavigationPage_RegisterCliTabPreco.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl_RegisterCliTabPreco)).BeginInit();
            this.panelControl_RegisterCliTabPreco.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_CliTabPreco)).BeginInit();
            this.layoutControl_CliTabPreco.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit_CliTabPreco_Cli.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager_ContasMP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit_CliTabPreco_TabPreco.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CliCPSelectTabPreco.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_RegisterCliTabPreco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem_RegisterCliTabPreco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CliTabPreco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CliTabPreco_CliID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdTabPreco_CliTabPreco_TabPreco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CliCPSelectCPs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meusPedidosDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem_RegisterClientes)).BeginInit();
            this.tabNavigationPage_DataGridCliTabPreco.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_CliTabPreco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_CliTabPreco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane_ConfigCliTabPreco)).BeginInit();
            this.tabPane_ConfigCliTabPreco.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).BeginInit();
            this.tabPane1.SuspendLayout();
            this.tabNavigationPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toggleSwitch1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CLITelefone1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CatProd)).BeginInit();
            this.SuspendLayout();
            // 
            // tileNavSubItem_Cliente
            // 
            this.tileNavSubItem_Cliente.Caption = "Cliente";
            this.tileNavSubItem_Cliente.Name = "tileNavSubItem_Cliente";
            // 
            // 
            // 
            this.tileNavSubItem_Cliente.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement1.Text = "Cliente";
            this.tileNavSubItem_Cliente.Tile.Elements.Add(tileItemElement1);
            this.tileNavSubItem_Cliente.Tile.Name = "tileBarItem3";
            // 
            // tileNavSubItem_CondicaoPagamentoCliente
            // 
            this.tileNavSubItem_CondicaoPagamentoCliente.Caption = "Condições de Pagamento por Cliente";
            this.tileNavSubItem_CondicaoPagamentoCliente.Name = "tileNavSubItem_CondicaoPagamentoCliente";
            // 
            // 
            // 
            this.tileNavSubItem_CondicaoPagamentoCliente.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement2.Text = "Condições de Pagamento por Cliente";
            this.tileNavSubItem_CondicaoPagamentoCliente.Tile.Elements.Add(tileItemElement2);
            this.tileNavSubItem_CondicaoPagamentoCliente.Tile.Name = "tileBarItem4";
            // 
            // tileNavSubItem_CategoriasCliente
            // 
            this.tileNavSubItem_CategoriasCliente.Caption = "Categorias por Cliente";
            this.tileNavSubItem_CategoriasCliente.Name = "tileNavSubItem_CategoriasCliente";
            // 
            // 
            // 
            this.tileNavSubItem_CategoriasCliente.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement3.Text = "Categorias por Cliente";
            this.tileNavSubItem_CategoriasCliente.Tile.Elements.Add(tileItemElement3);
            this.tileNavSubItem_CategoriasCliente.Tile.Name = "tileBarItem6";
            // 
            // tileNavSubItem_TabelaPreco
            // 
            this.tileNavSubItem_TabelaPreco.Caption = "Tabelas de Preço por Cliente";
            this.tileNavSubItem_TabelaPreco.Name = "tileNavSubItem_TabelaPreco";
            // 
            // 
            // 
            this.tileNavSubItem_TabelaPreco.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement4.Text = "Tabelas de Preço por Cliente";
            this.tileNavSubItem_TabelaPreco.Tile.Elements.Add(tileItemElement4);
            this.tileNavSubItem_TabelaPreco.Tile.Name = "tileBarItem7";
            // 
            // tileNavSubItem_Usuarios
            // 
            this.tileNavSubItem_Usuarios.Caption = "Usuários (Vendedores)";
            this.tileNavSubItem_Usuarios.Name = "tileNavSubItem_Usuarios";
            // 
            // 
            // 
            this.tileNavSubItem_Usuarios.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement5.Text = "Usuários (Vendedores)";
            this.tileNavSubItem_Usuarios.Tile.Elements.Add(tileItemElement5);
            this.tileNavSubItem_Usuarios.Tile.Name = "tileBarItem8";
            // 
            // tileNavSubItem_Produto
            // 
            this.tileNavSubItem_Produto.Caption = "Produto";
            this.tileNavSubItem_Produto.Name = "tileNavSubItem_Produto";
            // 
            // 
            // 
            this.tileNavSubItem_Produto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement6.Text = "Produto";
            this.tileNavSubItem_Produto.Tile.Elements.Add(tileItemElement6);
            this.tileNavSubItem_Produto.Tile.Name = "tileBarItem9";
            // 
            // tileNavSubItem_ImagensProduto
            // 
            this.tileNavSubItem_ImagensProduto.Caption = "Imagens do Produto";
            this.tileNavSubItem_ImagensProduto.Name = "tileNavSubItem_ImagensProduto";
            // 
            // 
            // 
            this.tileNavSubItem_ImagensProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement7.Text = "Imagens do Produto";
            this.tileNavSubItem_ImagensProduto.Tile.Elements.Add(tileItemElement7);
            this.tileNavSubItem_ImagensProduto.Tile.Name = "tileBarItem10";
            // 
            // tileNavSubItem_TabelaProduto
            // 
            this.tileNavSubItem_TabelaProduto.Caption = "Tabela de Preço";
            this.tileNavSubItem_TabelaProduto.Name = "tileNavSubItem_TabelaProduto";
            // 
            // 
            // 
            this.tileNavSubItem_TabelaProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement8.Text = "Tabelas de Preço";
            this.tileNavSubItem_TabelaProduto.Tile.Elements.Add(tileItemElement8);
            this.tileNavSubItem_TabelaProduto.Tile.Name = "tileBarItem11";
            // 
            // tileNavSubItem_TabelaPrecoProduto
            // 
            this.tileNavSubItem_TabelaPrecoProduto.Caption = "Tabelas de Preço por Produto";
            this.tileNavSubItem_TabelaPrecoProduto.Name = "tileNavSubItem_TabelaPrecoProduto";
            // 
            // 
            // 
            this.tileNavSubItem_TabelaPrecoProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement9.Text = "Tabelas de Preço por Produto";
            this.tileNavSubItem_TabelaPrecoProduto.Tile.Elements.Add(tileItemElement9);
            this.tileNavSubItem_TabelaPrecoProduto.Tile.Name = "tileBarItem12";
            // 
            // tileNavSubItem_CategoriaProduto
            // 
            this.tileNavSubItem_CategoriaProduto.Caption = "Categorias de Produtos";
            this.tileNavSubItem_CategoriaProduto.Name = "tileNavSubItem_CategoriaProduto";
            // 
            // 
            // 
            this.tileNavSubItem_CategoriaProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement10.Text = "Categorias de Produtos";
            this.tileNavSubItem_CategoriaProduto.Tile.Elements.Add(tileItemElement10);
            this.tileNavSubItem_CategoriaProduto.Tile.Name = "tileBarItem13";
            // 
            // tileNavSubItem_Pedido
            // 
            this.tileNavSubItem_Pedido.Caption = "Pedido";
            this.tileNavSubItem_Pedido.Name = "tileNavSubItem_Pedido";
            // 
            // 
            // 
            this.tileNavSubItem_Pedido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement11.Text = "Pedido";
            this.tileNavSubItem_Pedido.Tile.Elements.Add(tileItemElement11);
            this.tileNavSubItem_Pedido.Tile.Name = "tileBarItem17";
            // 
            // tileNavSubItem_CamposExtraPedido
            // 
            this.tileNavSubItem_CamposExtraPedido.Caption = "Campos Extras do Pedido";
            this.tileNavSubItem_CamposExtraPedido.Name = "tileNavSubItem_CamposExtraPedido";
            // 
            // 
            // 
            this.tileNavSubItem_CamposExtraPedido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement12.Text = "Campos Extras do Pedido";
            this.tileNavSubItem_CamposExtraPedido.Tile.Elements.Add(tileItemElement12);
            this.tileNavSubItem_CamposExtraPedido.Tile.Name = "tileBarItem18";
            // 
            // tileNavSubItem_Transportadora
            // 
            this.tileNavSubItem_Transportadora.Caption = "Transportadoras";
            this.tileNavSubItem_Transportadora.Name = "tileNavSubItem_Transportadora";
            // 
            // 
            // 
            this.tileNavSubItem_Transportadora.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement13.Text = "Transportadoras";
            this.tileNavSubItem_Transportadora.Tile.Elements.Add(tileItemElement13);
            this.tileNavSubItem_Transportadora.Tile.Name = "tileBarItem14";
            // 
            // tileNavSubItem_CondicaoPagamento
            // 
            this.tileNavSubItem_CondicaoPagamento.Caption = "Condições de Pagamento";
            this.tileNavSubItem_CondicaoPagamento.Name = "tileNavSubItem_CondicaoPagamento";
            // 
            // 
            // 
            this.tileNavSubItem_CondicaoPagamento.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement14.Text = "Condições de Pagamento";
            this.tileNavSubItem_CondicaoPagamento.Tile.Elements.Add(tileItemElement14);
            this.tileNavSubItem_CondicaoPagamento.Tile.Name = "tileBarItem15";
            // 
            // tileNavSubItem_FormaPagamento
            // 
            this.tileNavSubItem_FormaPagamento.Caption = "Formas de Pagamento";
            this.tileNavSubItem_FormaPagamento.Name = "tileNavSubItem_FormaPagamento";
            // 
            // 
            // 
            this.tileNavSubItem_FormaPagamento.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement15.Text = "Formas de Pagamento";
            this.tileNavSubItem_FormaPagamento.Tile.Elements.Add(tileItemElement15);
            this.tileNavSubItem_FormaPagamento.Tile.Name = "tileBarItem16";
            // 
            // tileNavSubItem_FaturamentoPedido
            // 
            this.tileNavSubItem_FaturamentoPedido.Caption = "Faturamento de Pedido";
            this.tileNavSubItem_FaturamentoPedido.Name = "tileNavSubItem_FaturamentoPedido";
            // 
            // 
            // 
            this.tileNavSubItem_FaturamentoPedido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement16.Text = "Faturamento de Pedido";
            this.tileNavSubItem_FaturamentoPedido.Tile.Elements.Add(tileItemElement16);
            this.tileNavSubItem_FaturamentoPedido.Tile.Name = "tileBarItem19";
            // 
            // tileNavSubItem_TituloVencido
            // 
            this.tileNavSubItem_TituloVencido.Caption = "Títulos Vencidos";
            this.tileNavSubItem_TituloVencido.Name = "tileNavSubItem_TituloVencido";
            // 
            // 
            // 
            this.tileNavSubItem_TituloVencido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement17.Text = "Títulos Vencidos";
            this.tileNavSubItem_TituloVencido.Tile.Elements.Add(tileItemElement17);
            this.tileNavSubItem_TituloVencido.Tile.Name = "tileBarItem20";
            // 
            // splashScreenManager_Transp
            // 
            this.splashScreenManager_Transp.ClosingDelay = 500;
            // 
            // tabNavigationPage_RegisterCliTabPreco
            // 
            this.tabNavigationPage_RegisterCliTabPreco.Caption = "Cadastro";
            this.tabNavigationPage_RegisterCliTabPreco.Controls.Add(this.panelControl_RegisterCliTabPreco);
            this.tabNavigationPage_RegisterCliTabPreco.Image = ((System.Drawing.Image)(resources.GetObject("tabNavigationPage_RegisterCliTabPreco.Image")));
            this.tabNavigationPage_RegisterCliTabPreco.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage_RegisterCliTabPreco.Name = "tabNavigationPage_RegisterCliTabPreco";
            this.tabNavigationPage_RegisterCliTabPreco.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage_RegisterCliTabPreco.Size = new System.Drawing.Size(1246, 404);
            // 
            // panelControl_RegisterCliTabPreco
            // 
            this.panelControl_RegisterCliTabPreco.Controls.Add(this.layoutControl_CliTabPreco);
            this.panelControl_RegisterCliTabPreco.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl_RegisterCliTabPreco.Location = new System.Drawing.Point(0, 0);
            this.panelControl_RegisterCliTabPreco.Name = "panelControl_RegisterCliTabPreco";
            this.panelControl_RegisterCliTabPreco.Size = new System.Drawing.Size(1246, 404);
            this.panelControl_RegisterCliTabPreco.TabIndex = 0;
            // 
            // layoutControl_CliTabPreco
            // 
            this.layoutControl_CliTabPreco.Controls.Add(this.comboBoxEdit_CliTabPreco_Cli);
            this.layoutControl_CliTabPreco.Controls.Add(this.comboBoxEdit_CliTabPreco_TabPreco);
            this.layoutControl_CliTabPreco.Controls.Add(this.simpleButton_CliTabPrecoAdd);
            this.layoutControl_CliTabPreco.Controls.Add(this.textEdit_CliCPSelectTabPreco);
            this.layoutControl_CliTabPreco.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl_CliTabPreco.Location = new System.Drawing.Point(2, 2);
            this.layoutControl_CliTabPreco.Name = "layoutControl_CliTabPreco";
            this.layoutControl_CliTabPreco.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(124, 125, 450, 400);
            this.layoutControl_CliTabPreco.Root = this.layoutControlGroup_RegisterCliTabPreco;
            this.layoutControl_CliTabPreco.Size = new System.Drawing.Size(1242, 400);
            this.layoutControl_CliTabPreco.TabIndex = 0;
            this.layoutControl_CliTabPreco.Text = "layoutControl_CliTabPreco";
            // 
            // comboBoxEdit_CliTabPreco_Cli
            // 
            this.comboBoxEdit_CliTabPreco_Cli.Location = new System.Drawing.Point(216, 181);
            this.comboBoxEdit_CliTabPreco_Cli.MenuManager = this.barManager_ContasMP;
            this.comboBoxEdit_CliTabPreco_Cli.Name = "comboBoxEdit_CliTabPreco_Cli";
            this.comboBoxEdit_CliTabPreco_Cli.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit_CliTabPreco_Cli.Properties.NullText = "";
            this.comboBoxEdit_CliTabPreco_Cli.Properties.PopupSizeable = false;
            this.comboBoxEdit_CliTabPreco_Cli.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.comboBoxEdit_CliTabPreco_Cli.Size = new System.Drawing.Size(992, 20);
            this.comboBoxEdit_CliTabPreco_Cli.StyleController = this.layoutControl_CliTabPreco;
            this.comboBoxEdit_CliTabPreco_Cli.TabIndex = 12;
            // 
            // barManager_ContasMP
            // 
            this.barManager_ContasMP.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar_Acoes,
            this.bar_Acoes2});
            this.barManager_ContasMP.DockControls.Add(this.barDockControlTop);
            this.barManager_ContasMP.DockControls.Add(this.barDockControlBottom);
            this.barManager_ContasMP.DockControls.Add(this.barDockControlLeft);
            this.barManager_ContasMP.DockControls.Add(this.barDockControlRight);
            this.barManager_ContasMP.Form = this;
            this.barManager_ContasMP.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem_Add,
            this.barButtonItem_Edit,
            this.barButtonItem_Del,
            this.barButtonItem_Gravar,
            this.barButtonItem_Cancelar});
            this.barManager_ContasMP.MaxItemId = 5;
            // 
            // bar_Acoes
            // 
            this.bar_Acoes.BarName = "Tools";
            this.bar_Acoes.DockCol = 0;
            this.bar_Acoes.DockRow = 0;
            this.bar_Acoes.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar_Acoes.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Add),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Edit),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Del)});
            this.bar_Acoes.OptionsBar.UseWholeRow = true;
            this.bar_Acoes.Text = "Tools";
            // 
            // barButtonItem_Add
            // 
            this.barButtonItem_Add.Caption = "Adicionar";
            this.barButtonItem_Add.Id = 0;
            this.barButtonItem_Add.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Add.ImageOptions.Image")));
            this.barButtonItem_Add.Name = "barButtonItem_Add";
            this.barButtonItem_Add.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Add.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Add_ItemClick);
            // 
            // barButtonItem_Edit
            // 
            this.barButtonItem_Edit.Caption = "Editar";
            this.barButtonItem_Edit.Enabled = false;
            this.barButtonItem_Edit.Id = 1;
            this.barButtonItem_Edit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Edit.ImageOptions.Image")));
            this.barButtonItem_Edit.Name = "barButtonItem_Edit";
            this.barButtonItem_Edit.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Edit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Edit_ItemClick);
            // 
            // barButtonItem_Del
            // 
            this.barButtonItem_Del.Caption = "Excluir";
            this.barButtonItem_Del.Enabled = false;
            this.barButtonItem_Del.Id = 2;
            this.barButtonItem_Del.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Del.ImageOptions.Image")));
            this.barButtonItem_Del.Name = "barButtonItem_Del";
            this.barButtonItem_Del.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Del.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Del_ItemClick);
            // 
            // bar_Acoes2
            // 
            this.bar_Acoes2.BarName = "Tools_GravarCancelar";
            this.bar_Acoes2.DockCol = 0;
            this.bar_Acoes2.DockRow = 1;
            this.bar_Acoes2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar_Acoes2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Gravar),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Cancelar)});
            this.bar_Acoes2.OptionsBar.AllowRename = true;
            this.bar_Acoes2.OptionsBar.UseWholeRow = true;
            this.bar_Acoes2.Text = "Tools_GravarCancelar";
            // 
            // barButtonItem_Gravar
            // 
            this.barButtonItem_Gravar.Caption = "Gravar";
            this.barButtonItem_Gravar.Id = 3;
            this.barButtonItem_Gravar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Gravar.ImageOptions.Image")));
            this.barButtonItem_Gravar.Name = "barButtonItem_Gravar";
            this.barButtonItem_Gravar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Gravar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Gravar_ItemClick);
            // 
            // barButtonItem_Cancelar
            // 
            this.barButtonItem_Cancelar.Caption = "Cancelar";
            this.barButtonItem_Cancelar.Id = 4;
            this.barButtonItem_Cancelar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Cancelar.ImageOptions.Image")));
            this.barButtonItem_Cancelar.Name = "barButtonItem_Cancelar";
            this.barButtonItem_Cancelar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Cancelar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Cancelar_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager_ContasMP;
            this.barDockControlTop.Size = new System.Drawing.Size(1264, 94);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 562);
            this.barDockControlBottom.Manager = this.barManager_ContasMP;
            this.barDockControlBottom.Size = new System.Drawing.Size(1264, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 94);
            this.barDockControlLeft.Manager = this.barManager_ContasMP;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 468);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1264, 94);
            this.barDockControlRight.Manager = this.barManager_ContasMP;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 468);
            // 
            // comboBoxEdit_CliTabPreco_TabPreco
            // 
            this.comboBoxEdit_CliTabPreco_TabPreco.Location = new System.Drawing.Point(216, 215);
            this.comboBoxEdit_CliTabPreco_TabPreco.MenuManager = this.barManager_ContasMP;
            this.comboBoxEdit_CliTabPreco_TabPreco.Name = "comboBoxEdit_CliTabPreco_TabPreco";
            this.comboBoxEdit_CliTabPreco_TabPreco.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit_CliTabPreco_TabPreco.Properties.NullText = "";
            this.comboBoxEdit_CliTabPreco_TabPreco.Properties.PopupSizeable = false;
            this.comboBoxEdit_CliTabPreco_TabPreco.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.comboBoxEdit_CliTabPreco_TabPreco.Size = new System.Drawing.Size(721, 20);
            this.comboBoxEdit_CliTabPreco_TabPreco.StyleController = this.layoutControl_CliTabPreco;
            this.comboBoxEdit_CliTabPreco_TabPreco.TabIndex = 14;
            // 
            // simpleButton_CliTabPrecoAdd
            // 
            this.simpleButton_CliTabPrecoAdd.Location = new System.Drawing.Point(941, 215);
            this.simpleButton_CliTabPrecoAdd.Name = "simpleButton_CliTabPrecoAdd";
            this.simpleButton_CliTabPrecoAdd.Size = new System.Drawing.Size(267, 22);
            this.simpleButton_CliTabPrecoAdd.StyleController = this.layoutControl_CliTabPreco;
            this.simpleButton_CliTabPrecoAdd.TabIndex = 15;
            this.simpleButton_CliTabPrecoAdd.Text = "Add";
            this.simpleButton_CliTabPrecoAdd.Click += new System.EventHandler(this.simpleButton_CliCPAdd_Click);
            // 
            // textEdit_CliCPSelectTabPreco
            // 
            this.textEdit_CliCPSelectTabPreco.Enabled = false;
            this.textEdit_CliCPSelectTabPreco.Location = new System.Drawing.Point(34, 241);
            this.textEdit_CliCPSelectTabPreco.MenuManager = this.barManager_ContasMP;
            this.textEdit_CliCPSelectTabPreco.Name = "textEdit_CliCPSelectTabPreco";
            this.textEdit_CliCPSelectTabPreco.Size = new System.Drawing.Size(1174, 20);
            this.textEdit_CliCPSelectTabPreco.StyleController = this.layoutControl_CliTabPreco;
            this.textEdit_CliCPSelectTabPreco.TabIndex = 16;
            // 
            // layoutControlGroup_RegisterCliTabPreco
            // 
            this.layoutControlGroup_RegisterCliTabPreco.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup_RegisterCliTabPreco.GroupBordersVisible = false;
            this.layoutControlGroup_RegisterCliTabPreco.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.emptySpaceItem4,
            this.simpleLabelItem_RegisterCliTabPreco,
            this.emptySpaceItem5,
            this.emptySpaceItem9,
            this.layoutControlGroup_CliTabPreco,
            this.emptySpaceItem3});
            this.layoutControlGroup_RegisterCliTabPreco.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup_RegisterCliTabPreco.Name = "layoutControlGroup_RegisterCliTabPreco";
            this.layoutControlGroup_RegisterCliTabPreco.Size = new System.Drawing.Size(1242, 400);
            this.layoutControlGroup_RegisterCliTabPreco.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(10, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(1212, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(10, 380);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleLabelItem_RegisterCliTabPreco
            // 
            this.simpleLabelItem_RegisterCliTabPreco.AllowHotTrack = false;
            this.simpleLabelItem_RegisterCliTabPreco.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem_RegisterCliTabPreco.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem_RegisterCliTabPreco.Location = new System.Drawing.Point(10, 10);
            this.simpleLabelItem_RegisterCliTabPreco.Name = "simpleLabelItem_RegisterCliTabPreco";
            this.simpleLabelItem_RegisterCliTabPreco.OptionsPrint.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem_RegisterCliTabPreco.OptionsPrint.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem_RegisterCliTabPreco.Size = new System.Drawing.Size(1202, 17);
            this.simpleLabelItem_RegisterCliTabPreco.Text = "Cadastro Tabela de Preço por Cliente";
            this.simpleLabelItem_RegisterCliTabPreco.TextSize = new System.Drawing.Size(179, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(10, 27);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.Location = new System.Drawing.Point(10, 370);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup_CliTabPreco
            // 
            this.layoutControlGroup_CliTabPreco.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem10,
            this.emptySpaceItem2,
            this.layoutControlItem_CliTabPreco_CliID,
            this.emptySpaceItem17,
            this.layoutControlItem_ProdTabPreco_CliTabPreco_TabPreco,
            this.layoutControlItem1,
            this.layoutControlItem_CliCPSelectCPs});
            this.layoutControlGroup_CliTabPreco.Location = new System.Drawing.Point(10, 37);
            this.layoutControlGroup_CliTabPreco.Name = "layoutControlGroup_CliTabPreco";
            this.layoutControlGroup_CliTabPreco.Size = new System.Drawing.Size(1202, 333);
            this.layoutControlGroup_CliTabPreco.Text = "Tabela de Preço por Cliente";
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(1178, 102);
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 186);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(1178, 105);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem_CliTabPreco_CliID
            // 
            this.layoutControlItem_CliTabPreco_CliID.Control = this.comboBoxEdit_CliTabPreco_Cli;
            this.layoutControlItem_CliTabPreco_CliID.Location = new System.Drawing.Point(0, 102);
            this.layoutControlItem_CliTabPreco_CliID.Name = "layoutControlItem_CliTabPreco_CliID";
            this.layoutControlItem_CliTabPreco_CliID.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem_CliTabPreco_CliID.Text = "Cliente";
            this.layoutControlItem_CliTabPreco_CliID.TextSize = new System.Drawing.Size(179, 13);
            // 
            // emptySpaceItem17
            // 
            this.emptySpaceItem17.AllowHotTrack = false;
            this.emptySpaceItem17.Location = new System.Drawing.Point(0, 126);
            this.emptySpaceItem17.Name = "emptySpaceItem17";
            this.emptySpaceItem17.Size = new System.Drawing.Size(1178, 10);
            this.emptySpaceItem17.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem_ProdTabPreco_CliTabPreco_TabPreco
            // 
            this.layoutControlItem_ProdTabPreco_CliTabPreco_TabPreco.Control = this.comboBoxEdit_CliTabPreco_TabPreco;
            this.layoutControlItem_ProdTabPreco_CliTabPreco_TabPreco.Location = new System.Drawing.Point(0, 136);
            this.layoutControlItem_ProdTabPreco_CliTabPreco_TabPreco.Name = "layoutControlItem_ProdTabPreco_CliTabPreco_TabPreco";
            this.layoutControlItem_ProdTabPreco_CliTabPreco_TabPreco.Size = new System.Drawing.Size(907, 26);
            this.layoutControlItem_ProdTabPreco_CliTabPreco_TabPreco.Text = "Tabela de Preço";
            this.layoutControlItem_ProdTabPreco_CliTabPreco_TabPreco.TextSize = new System.Drawing.Size(179, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.simpleButton_CliTabPrecoAdd;
            this.layoutControlItem1.Location = new System.Drawing.Point(907, 136);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(271, 26);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem_CliCPSelectCPs
            // 
            this.layoutControlItem_CliCPSelectCPs.Control = this.textEdit_CliCPSelectTabPreco;
            this.layoutControlItem_CliCPSelectCPs.Location = new System.Drawing.Point(0, 162);
            this.layoutControlItem_CliCPSelectCPs.Name = "layoutControlItem_CliCPSelectCPs";
            this.layoutControlItem_CliCPSelectCPs.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem_CliCPSelectCPs.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem_CliCPSelectCPs.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(10, 380);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // meusPedidosDataSet
            // 
            this.meusPedidosDataSet.DataSetName = "MeusPedidosDataSet";
            this.meusPedidosDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // simpleLabelItem_RegisterClientes
            // 
            this.simpleLabelItem_RegisterClientes.AllowHotTrack = false;
            this.simpleLabelItem_RegisterClientes.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem_RegisterClientes.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem_RegisterClientes.Location = new System.Drawing.Point(10, 10);
            this.simpleLabelItem_RegisterClientes.Name = "simpleLabelItem_RegisterClientes";
            this.simpleLabelItem_RegisterClientes.OptionsPrint.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem_RegisterClientes.OptionsPrint.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem_RegisterClientes.Size = new System.Drawing.Size(1185, 17);
            this.simpleLabelItem_RegisterClientes.Text = "Cadastro Clientes";
            this.simpleLabelItem_RegisterClientes.TextSize = new System.Drawing.Size(155, 13);
            // 
            // tabNavigationPage_DataGridCliTabPreco
            // 
            this.tabNavigationPage_DataGridCliTabPreco.Caption = "Tabelas de Preço por Cliente";
            this.tabNavigationPage_DataGridCliTabPreco.Controls.Add(this.gridControl_CliTabPreco);
            this.tabNavigationPage_DataGridCliTabPreco.Image = ((System.Drawing.Image)(resources.GetObject("tabNavigationPage_DataGridCliTabPreco.Image")));
            this.tabNavigationPage_DataGridCliTabPreco.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage_DataGridCliTabPreco.Name = "tabNavigationPage_DataGridCliTabPreco";
            this.tabNavigationPage_DataGridCliTabPreco.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage_DataGridCliTabPreco.Size = new System.Drawing.Size(1246, 404);
            // 
            // gridControl_CliTabPreco
            // 
            this.gridControl_CliTabPreco.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl_CliTabPreco.Location = new System.Drawing.Point(0, 0);
            this.gridControl_CliTabPreco.MainView = this.gridView_CliTabPreco;
            this.gridControl_CliTabPreco.MenuManager = this.barManager_ContasMP;
            this.gridControl_CliTabPreco.Name = "gridControl_CliTabPreco";
            this.gridControl_CliTabPreco.Size = new System.Drawing.Size(1246, 404);
            this.gridControl_CliTabPreco.TabIndex = 0;
            this.gridControl_CliTabPreco.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView_CliTabPreco});
            // 
            // gridView_CliTabPreco
            // 
            this.gridView_CliTabPreco.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colid,
            this.colcliente_id,
            this.coltabelas_liberadas,
            this.coltabelas_liberadas_desc,
            this.colmptran});
            this.gridView_CliTabPreco.GridControl = this.gridControl_CliTabPreco;
            this.gridView_CliTabPreco.Name = "gridView_CliTabPreco";
            this.gridView_CliTabPreco.OptionsBehavior.Editable = false;
            this.gridView_CliTabPreco.OptionsBehavior.ReadOnly = true;
            this.gridView_CliTabPreco.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView_CliTabPreco.OptionsSelection.MultiSelect = true;
            this.gridView_CliTabPreco.OptionsSelection.UseIndicatorForSelection = false;
            this.gridView_CliTabPreco.OptionsView.ShowGroupPanel = false;
            this.gridView_CliTabPreco.OptionsView.ShowIndicator = false;
            this.gridView_CliTabPreco.DoubleClick += new System.EventHandler(this.gridView_Clientes_DoubleClick);
            // 
            // colid
            // 
            this.colid.Caption = "ID";
            this.colid.FieldName = "id";
            this.colid.Name = "colid";
            this.colid.Visible = true;
            this.colid.VisibleIndex = 0;
            // 
            // colcliente_id
            // 
            this.colcliente_id.Caption = "ID Cliente";
            this.colcliente_id.FieldName = "cliente_id";
            this.colcliente_id.Name = "colcliente_id";
            this.colcliente_id.Visible = true;
            this.colcliente_id.VisibleIndex = 1;
            // 
            // coltabelas_liberadas
            // 
            this.coltabelas_liberadas.Caption = "ID Tabelas Liberadas";
            this.coltabelas_liberadas.FieldName = "tabelas_liberadas";
            this.coltabelas_liberadas.Name = "coltabelas_liberadas";
            this.coltabelas_liberadas.Visible = true;
            this.coltabelas_liberadas.VisibleIndex = 2;
            // 
            // coltabelas_liberadas_desc
            // 
            this.coltabelas_liberadas_desc.Caption = "Tabelas Liberadas";
            this.coltabelas_liberadas_desc.FieldName = "tabelas_liberadas_desc";
            this.coltabelas_liberadas_desc.Name = "coltabelas_liberadas_desc";
            this.coltabelas_liberadas_desc.Visible = true;
            this.coltabelas_liberadas_desc.VisibleIndex = 3;
            // 
            // colmptran
            // 
            this.colmptran.Caption = "MP Tran";
            this.colmptran.FieldName = "mptran";
            this.colmptran.Name = "colmptran";
            this.colmptran.Visible = true;
            this.colmptran.VisibleIndex = 4;
            // 
            // tabPane_ConfigCliTabPreco
            // 
            this.tabPane_ConfigCliTabPreco.Controls.Add(this.tabNavigationPage_DataGridCliTabPreco);
            this.tabPane_ConfigCliTabPreco.Controls.Add(this.tabNavigationPage_RegisterCliTabPreco);
            this.tabPane_ConfigCliTabPreco.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPane_ConfigCliTabPreco.Location = new System.Drawing.Point(0, 94);
            this.tabPane_ConfigCliTabPreco.Name = "tabPane_ConfigCliTabPreco";
            this.tabPane_ConfigCliTabPreco.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.tabNavigationPage_DataGridCliTabPreco,
            this.tabNavigationPage_RegisterCliTabPreco});
            this.tabPane_ConfigCliTabPreco.RegularSize = new System.Drawing.Size(1264, 468);
            this.tabPane_ConfigCliTabPreco.SelectedPage = this.tabNavigationPage_RegisterCliTabPreco;
            this.tabPane_ConfigCliTabPreco.Size = new System.Drawing.Size(1264, 468);
            this.tabPane_ConfigCliTabPreco.TabIndex = 8;
            this.tabPane_ConfigCliTabPreco.Text = "Tabela de Preço por Cliente";
            // 
            // tabPane1
            // 
            this.tabPane1.Controls.Add(this.tabNavigationPage1);
            this.tabPane1.Controls.Add(this.tabNavigationPage2);
            this.tabPane1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPane1.Location = new System.Drawing.Point(0, 94);
            this.tabPane1.Name = "tabPane1";
            this.tabPane1.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.tabNavigationPage1,
            this.tabNavigationPage2});
            this.tabPane1.RegularSize = new System.Drawing.Size(1264, 445);
            this.tabPane1.SelectedPage = this.tabNavigationPage2;
            this.tabPane1.Size = new System.Drawing.Size(1264, 445);
            this.tabPane1.TabIndex = 8;
            this.tabPane1.Text = "Clientes";
            // 
            // tabNavigationPage1
            // 
            this.tabNavigationPage1.Caption = "Clientes";
            this.tabNavigationPage1.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage1.Name = "tabNavigationPage1";
            this.tabNavigationPage1.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage1.Size = new System.Drawing.Size(1246, 381);
            // 
            // tabNavigationPage2
            // 
            this.tabNavigationPage2.Caption = "Cadastro";
            this.tabNavigationPage2.Controls.Add(this.panelControl1);
            this.tabNavigationPage2.Image = ((System.Drawing.Image)(resources.GetObject("tabNavigationPage2.Image")));
            this.tabNavigationPage2.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage2.Name = "tabNavigationPage2";
            this.tabNavigationPage2.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage2.Size = new System.Drawing.Size(1246, 381);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.layoutControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1246, 381);
            this.panelControl1.TabIndex = 0;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.textEdit2);
            this.layoutControl1.Controls.Add(this.textEdit3);
            this.layoutControl1.Controls.Add(this.textEdit4);
            this.layoutControl1.Controls.Add(this.textEdit5);
            this.layoutControl1.Controls.Add(this.textEdit6);
            this.layoutControl1.Controls.Add(this.textEdit7);
            this.layoutControl1.Controls.Add(this.textEdit8);
            this.layoutControl1.Controls.Add(this.textEdit9);
            this.layoutControl1.Controls.Add(this.textEdit10);
            this.layoutControl1.Controls.Add(this.textEdit11);
            this.layoutControl1.Controls.Add(this.textEdit12);
            this.layoutControl1.Controls.Add(this.textEdit13);
            this.layoutControl1.Controls.Add(this.textEdit14);
            this.layoutControl1.Controls.Add(this.dateEdit1);
            this.layoutControl1.Controls.Add(this.toggleSwitch1);
            this.layoutControl1.Controls.Add(this.memoEdit1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup3;
            this.layoutControl1.Size = new System.Drawing.Size(1242, 377);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textEdit2
            // 
            this.textEdit2.Location = new System.Drawing.Point(136, 83);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Size = new System.Drawing.Size(1072, 20);
            this.textEdit2.StyleController = this.layoutControl1;
            this.textEdit2.TabIndex = 4;
            this.textEdit2.Visible = false;
            // 
            // textEdit3
            // 
            this.textEdit3.Location = new System.Drawing.Point(136, 107);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Size = new System.Drawing.Size(1072, 20);
            this.textEdit3.StyleController = this.layoutControl1;
            this.textEdit3.TabIndex = 5;
            this.textEdit3.Visible = false;
            // 
            // textEdit4
            // 
            this.textEdit4.Location = new System.Drawing.Point(136, 131);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Properties.MaxLength = 1;
            this.textEdit4.Size = new System.Drawing.Size(1072, 20);
            this.textEdit4.StyleController = this.layoutControl1;
            this.textEdit4.TabIndex = 6;
            this.textEdit4.Visible = false;
            // 
            // textEdit5
            // 
            this.textEdit5.Location = new System.Drawing.Point(136, 155);
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Size = new System.Drawing.Size(1072, 20);
            this.textEdit5.StyleController = this.layoutControl1;
            this.textEdit5.TabIndex = 7;
            this.textEdit5.Visible = false;
            // 
            // textEdit6
            // 
            this.textEdit6.Location = new System.Drawing.Point(136, 179);
            this.textEdit6.Name = "textEdit6";
            this.textEdit6.Size = new System.Drawing.Size(1072, 20);
            this.textEdit6.StyleController = this.layoutControl1;
            this.textEdit6.TabIndex = 8;
            this.textEdit6.Visible = false;
            // 
            // textEdit7
            // 
            this.textEdit7.Location = new System.Drawing.Point(136, 203);
            this.textEdit7.Name = "textEdit7";
            this.textEdit7.Size = new System.Drawing.Size(1072, 20);
            this.textEdit7.StyleController = this.layoutControl1;
            this.textEdit7.TabIndex = 9;
            this.textEdit7.Visible = false;
            // 
            // textEdit8
            // 
            this.textEdit8.Location = new System.Drawing.Point(136, 303);
            this.textEdit8.Name = "textEdit8";
            this.textEdit8.Size = new System.Drawing.Size(1072, 20);
            this.textEdit8.StyleController = this.layoutControl1;
            this.textEdit8.TabIndex = 10;
            // 
            // textEdit9
            // 
            this.textEdit9.Location = new System.Drawing.Point(136, 327);
            this.textEdit9.Name = "textEdit9";
            this.textEdit9.Size = new System.Drawing.Size(1072, 20);
            this.textEdit9.StyleController = this.layoutControl1;
            this.textEdit9.TabIndex = 11;
            // 
            // textEdit10
            // 
            this.textEdit10.Location = new System.Drawing.Point(136, 351);
            this.textEdit10.Name = "textEdit10";
            this.textEdit10.Size = new System.Drawing.Size(1072, 20);
            this.textEdit10.StyleController = this.layoutControl1;
            this.textEdit10.TabIndex = 12;
            // 
            // textEdit11
            // 
            this.textEdit11.Location = new System.Drawing.Point(136, 375);
            this.textEdit11.Name = "textEdit11";
            this.textEdit11.Size = new System.Drawing.Size(1072, 20);
            this.textEdit11.StyleController = this.layoutControl1;
            this.textEdit11.TabIndex = 13;
            // 
            // textEdit12
            // 
            this.textEdit12.Location = new System.Drawing.Point(136, 399);
            this.textEdit12.Name = "textEdit12";
            this.textEdit12.Size = new System.Drawing.Size(1072, 20);
            this.textEdit12.StyleController = this.layoutControl1;
            this.textEdit12.TabIndex = 14;
            // 
            // textEdit13
            // 
            this.textEdit13.Location = new System.Drawing.Point(136, 423);
            this.textEdit13.Name = "textEdit13";
            this.textEdit13.Size = new System.Drawing.Size(1072, 20);
            this.textEdit13.StyleController = this.layoutControl1;
            this.textEdit13.TabIndex = 15;
            // 
            // textEdit14
            // 
            this.textEdit14.Location = new System.Drawing.Point(136, 227);
            this.textEdit14.Name = "textEdit14";
            this.textEdit14.Size = new System.Drawing.Size(1072, 20);
            this.textEdit14.StyleController = this.layoutControl1;
            this.textEdit14.TabIndex = 16;
            this.textEdit14.Visible = false;
            // 
            // dateEdit1
            // 
            this.dateEdit1.EditValue = null;
            this.dateEdit1.Location = new System.Drawing.Point(980, 499);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.ReadOnly = true;
            this.dateEdit1.Size = new System.Drawing.Size(228, 20);
            this.dateEdit1.StyleController = this.layoutControl1;
            this.dateEdit1.TabIndex = 17;
            // 
            // toggleSwitch1
            // 
            this.toggleSwitch1.Location = new System.Drawing.Point(980, 523);
            this.toggleSwitch1.Name = "toggleSwitch1";
            this.toggleSwitch1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.toggleSwitch1.Properties.OffText = "Não";
            this.toggleSwitch1.Properties.OnText = "Sim";
            this.toggleSwitch1.Size = new System.Drawing.Size(228, 24);
            this.toggleSwitch1.StyleController = this.layoutControl1;
            this.toggleSwitch1.TabIndex = 10;
            // 
            // memoEdit1
            // 
            this.memoEdit1.Location = new System.Drawing.Point(136, 499);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(728, 48);
            this.memoEdit1.StyleController = this.layoutControl1;
            this.memoEdit1.TabIndex = 18;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem6,
            this.emptySpaceItem8,
            this.emptySpaceItem11,
            this.emptySpaceItem12,
            this.simpleLabelItem1,
            this.emptySpaceItem13,
            this.emptySpaceItem14,
            this.layoutControlGroup4,
            this.emptySpaceItem15,
            this.layoutControlGroup5,
            this.tabbedControlGroup3});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "Root";
            this.layoutControlGroup3.Size = new System.Drawing.Size(1242, 581);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(10, 0);
            this.emptySpaceItem6.Name = "emptySpaceItem1";
            this.emptySpaceItem6.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.Location = new System.Drawing.Point(10, 251);
            this.emptySpaceItem8.Name = "emptySpaceItem2";
            this.emptySpaceItem8.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem11.Name = "emptySpaceItem3";
            this.emptySpaceItem11.Size = new System.Drawing.Size(10, 561);
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.Location = new System.Drawing.Point(1212, 0);
            this.emptySpaceItem12.Name = "emptySpaceItem4";
            this.emptySpaceItem12.Size = new System.Drawing.Size(10, 561);
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleLabelItem1
            // 
            this.simpleLabelItem1.AllowHotTrack = false;
            this.simpleLabelItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem1.Location = new System.Drawing.Point(10, 10);
            this.simpleLabelItem1.Name = "simpleLabelItem_RegisterClientes";
            this.simpleLabelItem1.OptionsPrint.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem1.OptionsPrint.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem1.Size = new System.Drawing.Size(1202, 17);
            this.simpleLabelItem1.Text = "Cadastro Clientes";
            this.simpleLabelItem1.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.Location = new System.Drawing.Point(10, 27);
            this.emptySpaceItem13.Name = "emptySpaceItem5";
            this.emptySpaceItem13.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.Location = new System.Drawing.Point(10, 447);
            this.emptySpaceItem14.Name = "emptySpaceItem7";
            this.emptySpaceItem14.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9});
            this.layoutControlGroup4.Location = new System.Drawing.Point(10, 261);
            this.layoutControlGroup4.Name = "layoutControlGroup_Endereço";
            this.layoutControlGroup4.Size = new System.Drawing.Size(1202, 186);
            this.layoutControlGroup4.Text = "Endereço";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.textEdit8;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem_CLIRua";
            this.layoutControlItem4.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem4.Text = "Rua";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.textEdit9;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem5.Name = "layoutControlItem_CLIComplemento";
            this.layoutControlItem5.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem5.Text = "Complemento";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.textEdit10;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem6.Name = "layoutControlItem_CLICep";
            this.layoutControlItem6.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem6.Text = "CEP";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.textEdit11;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem7.Name = "layoutControlItem_Bairro";
            this.layoutControlItem7.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem7.Text = "Bairro";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.textEdit12;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem8.Name = "layoutControlItem_Cidade";
            this.layoutControlItem8.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem8.Text = "Cidade";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.textEdit13;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem9.Name = "layoutControlItem_CLIEstado";
            this.layoutControlItem9.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem9.Text = "Estado";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.Location = new System.Drawing.Point(10, 551);
            this.emptySpaceItem15.Name = "emptySpaceItem9";
            this.emptySpaceItem15.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.emptySpaceItem16,
            this.layoutControlItem12});
            this.layoutControlGroup5.Location = new System.Drawing.Point(10, 457);
            this.layoutControlGroup5.Name = "layoutControlGroup1";
            this.layoutControlGroup5.Size = new System.Drawing.Size(1202, 94);
            this.layoutControlGroup5.Text = "Status";
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.dateEdit1;
            this.layoutControlItem10.Location = new System.Drawing.Point(844, 0);
            this.layoutControlItem10.Name = "layoutControlItem_CLIUltimaAlteracao";
            this.layoutControlItem10.Size = new System.Drawing.Size(334, 24);
            this.layoutControlItem10.Text = "Última Alteração";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.toggleSwitch1;
            this.layoutControlItem11.CustomizationFormText = "Status";
            this.layoutControlItem11.Location = new System.Drawing.Point(844, 24);
            this.layoutControlItem11.Name = "layoutControlItem_CLIExcluido";
            this.layoutControlItem11.Size = new System.Drawing.Size(334, 28);
            this.layoutControlItem11.Text = "Excluído";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem16
            // 
            this.emptySpaceItem16.AllowHotTrack = false;
            this.emptySpaceItem16.Location = new System.Drawing.Point(834, 0);
            this.emptySpaceItem16.Name = "emptySpaceItem10";
            this.emptySpaceItem16.Size = new System.Drawing.Size(10, 52);
            this.emptySpaceItem16.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.memoEdit1;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem12.Name = "layoutControlItem_CLIObservacao";
            this.layoutControlItem12.Size = new System.Drawing.Size(834, 52);
            this.layoutControlItem12.Text = "Observação";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(99, 13);
            // 
            // tabbedControlGroup3
            // 
            this.tabbedControlGroup3.Location = new System.Drawing.Point(10, 37);
            this.tabbedControlGroup3.Name = "tabbedControlGroup1";
            this.tabbedControlGroup3.SelectedTabPage = this.layoutControlGroup6;
            this.tabbedControlGroup3.SelectedTabPageIndex = 1;
            this.tabbedControlGroup3.Size = new System.Drawing.Size(1202, 214);
            this.tabbedControlGroup3.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup9,
            this.layoutControlGroup6});
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup4});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup_CLIContato";
            this.layoutControlGroup6.Size = new System.Drawing.Size(1178, 168);
            this.layoutControlGroup6.Text = "Contatos";
            // 
            // tabbedControlGroup4
            // 
            this.tabbedControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup4.Name = "tabbedControlGroup2";
            this.tabbedControlGroup4.SelectedTabPage = this.layoutControlGroup7;
            this.tabbedControlGroup4.SelectedTabPageIndex = 0;
            this.tabbedControlGroup4.Size = new System.Drawing.Size(1178, 168);
            this.tabbedControlGroup4.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup7,
            this.layoutControlGroup8});
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleSeparator2});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup_CLITelefone";
            this.layoutControlGroup7.Size = new System.Drawing.Size(1154, 122);
            this.layoutControlGroup7.Text = "Telefone";
            // 
            // simpleSeparator2
            // 
            this.simpleSeparator2.AllowHotTrack = false;
            this.simpleSeparator2.Location = new System.Drawing.Point(0, 0);
            this.simpleSeparator2.Name = "simpleSeparator1";
            this.simpleSeparator2.Size = new System.Drawing.Size(1154, 122);
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup8.Name = "layoutControlGroup_CLIEmail";
            this.layoutControlGroup8.Size = new System.Drawing.Size(1154, 122);
            this.layoutControlGroup8.Text = "E-mail";
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.layoutControlItem23});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup9.Name = "layoutControlGroup_CLIDados";
            this.layoutControlGroup9.Size = new System.Drawing.Size(1178, 168);
            this.layoutControlGroup9.Text = "Dados";
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.textEdit2;
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem17.Name = "layoutControlItem_CLIRazaoSocial";
            this.layoutControlItem17.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem17.Text = "Razão Social";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.textEdit3;
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem18.Name = "layoutControlItem_CLINomeFantasia";
            this.layoutControlItem18.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem18.Text = "Nome Fantasia";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.textEdit4;
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem19.Name = "layoutControlItem_CLITipo";
            this.layoutControlItem19.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem19.Text = "Tipo";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.textEdit5;
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem20.Name = "layoutControlItem_CLICnpj";
            this.layoutControlItem20.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem20.Text = "CNPJ";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.textEdit6;
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem21.Name = "layoutControlItem_CLIInscricaoEstadual";
            this.layoutControlItem21.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem21.Text = "Inscrição Estadual";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.textEdit7;
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem22.Name = "layoutControlItem_CLISuframa";
            this.layoutControlItem22.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem22.Text = "Suframa";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.textEdit14;
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItem23.Name = "layoutControlItem_CLINomeExcFiscal";
            this.layoutControlItem23.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem23.Text = "Nome Exceção Fiscal";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(99, 13);
            // 
            // simpleSeparator3
            // 
            this.simpleSeparator3.AllowHotTrack = false;
            this.simpleSeparator3.CustomizationFormText = "simpleSeparator1";
            this.simpleSeparator3.Location = new System.Drawing.Point(1005, 0);
            this.simpleSeparator3.Name = "simpleSeparator3";
            this.simpleSeparator3.Size = new System.Drawing.Size(2, 26);
            this.simpleSeparator3.Text = "simpleSeparator1";
            // 
            // layoutControlGroup_CLITelefone1
            // 
            this.layoutControlGroup_CLITelefone1.CustomizationFormText = "Telefone";
            this.layoutControlGroup_CLITelefone1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleSeparator3});
            this.layoutControlGroup_CLITelefone1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup_CLITelefone1.Name = "layoutControlGroup_CLITelefone1";
            this.layoutControlGroup_CLITelefone1.Size = new System.Drawing.Size(1205, 31);
            this.layoutControlGroup_CLITelefone1.Text = "Telefone";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup_CLITelefone1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 561);
            this.layoutControlGroup2.Name = "LayoutRootGroupForRestore";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1205, 31);
            this.layoutControlGroup2.Tag = "LayoutRootGroupForRestore";
            // 
            // colid1
            // 
            this.colid1.FieldName = "id";
            this.colid1.Name = "colid1";
            // 
            // colrazao_social
            // 
            this.colrazao_social.FieldName = "razao_social";
            this.colrazao_social.Name = "colrazao_social";
            // 
            // colnome_fantasia
            // 
            this.colnome_fantasia.FieldName = "nome_fantasia";
            this.colnome_fantasia.Name = "colnome_fantasia";
            // 
            // layoutControlGroup_CatProd
            // 
            this.layoutControlGroup_CatProd.Location = new System.Drawing.Point(10, 37);
            this.layoutControlGroup_CatProd.Name = "layoutControlGroup_CatProd";
            this.layoutControlGroup_CatProd.Size = new System.Drawing.Size(1202, 218);
            // 
            // FormClienteTabelaPreco
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1264, 562);
            this.Controls.Add(this.tabPane_ConfigCliTabPreco);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormClienteTabelaPreco";
            this.ShowMdiChildCaptionInParentTitle = true;
            this.Text = "Tabelas de Preço por Cliente";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormTabelaPreco_Load);
            this.tabNavigationPage_RegisterCliTabPreco.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl_RegisterCliTabPreco)).EndInit();
            this.panelControl_RegisterCliTabPreco.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_CliTabPreco)).EndInit();
            this.layoutControl_CliTabPreco.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit_CliTabPreco_Cli.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager_ContasMP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit_CliTabPreco_TabPreco.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CliCPSelectTabPreco.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_RegisterCliTabPreco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem_RegisterCliTabPreco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CliTabPreco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CliTabPreco_CliID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdTabPreco_CliTabPreco_TabPreco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CliCPSelectCPs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meusPedidosDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem_RegisterClientes)).EndInit();
            this.tabNavigationPage_DataGridCliTabPreco.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_CliTabPreco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_CliTabPreco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane_ConfigCliTabPreco)).EndInit();
            this.tabPane_ConfigCliTabPreco.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).EndInit();
            this.tabPane1.ResumeLayout(false);
            this.tabNavigationPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toggleSwitch1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CLITelefone1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CatProd)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Cliente;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CondicaoPagamentoCliente;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CategoriasCliente;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TabelaPreco;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Usuarios;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Produto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_ImagensProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TabelaProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TabelaPrecoProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CategoriaProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Transportadora;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CondicaoPagamento;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_FormaPagamento;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Pedido;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CamposExtraPedido;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_FaturamentoPedido;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TituloVencido;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager_Transp;
        private DevExpress.XtraBars.Navigation.TabPane tabPane_ConfigCliTabPreco;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage_DataGridCliTabPreco;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage_RegisterCliTabPreco;
        private DevExpress.XtraBars.BarManager barManager_ContasMP;
        private DevExpress.XtraBars.Bar bar_Acoes;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Add;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Edit;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Del;
        private DevExpress.XtraBars.Bar bar_Acoes2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Gravar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Cancelar;
        private DevExpress.XtraEditors.PanelControl panelControl_RegisterCliTabPreco;
        private DevExpress.XtraLayout.LayoutControl layoutControl_CliTabPreco;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_RegisterCliTabPreco;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem_RegisterClientes;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraBars.Navigation.TabPane tabPane1;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage1;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage2;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraEditors.TextEdit textEdit3;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        private DevExpress.XtraEditors.TextEdit textEdit6;
        private DevExpress.XtraEditors.TextEdit textEdit7;
        private DevExpress.XtraEditors.TextEdit textEdit8;
        private DevExpress.XtraEditors.TextEdit textEdit9;
        private DevExpress.XtraEditors.TextEdit textEdit10;
        private DevExpress.XtraEditors.TextEdit textEdit11;
        private DevExpress.XtraEditors.TextEdit textEdit12;
        private DevExpress.XtraEditors.TextEdit textEdit13;
        private DevExpress.XtraEditors.TextEdit textEdit14;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
        private DevExpress.XtraEditors.ToggleSwitch toggleSwitch1;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_CLITelefone1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraGrid.Columns.GridColumn colid1;
        private DevExpress.XtraGrid.Columns.GridColumn colrazao_social;
        private DevExpress.XtraGrid.Columns.GridColumn colnome_fantasia;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraGrid.GridControl gridControl_CliTabPreco;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView_CliTabPreco;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_CliTabPreco;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_CatProd;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem_RegisterCliTabPreco;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_CliTabPreco_CliID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem17;
        private MeusPedidosDataSet meusPedidosDataSet;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_ProdTabPreco_CliTabPreco_TabPreco;
        private DevExpress.XtraEditors.LookUpEdit comboBoxEdit_CliTabPreco_Cli;
        private DevExpress.XtraEditors.LookUpEdit comboBoxEdit_CliTabPreco_TabPreco;
        private DevExpress.XtraEditors.SimpleButton simpleButton_CliTabPrecoAdd;
        private DevExpress.XtraEditors.TextEdit textEdit_CliCPSelectTabPreco;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_CliCPSelectCPs;
        private DevExpress.XtraGrid.Columns.GridColumn colid;
        private DevExpress.XtraGrid.Columns.GridColumn colcliente_id;
        private DevExpress.XtraGrid.Columns.GridColumn coltabelas_liberadas;
        private DevExpress.XtraGrid.Columns.GridColumn coltabelas_liberadas_desc;
        private DevExpress.XtraGrid.Columns.GridColumn colmptran;
    }
}


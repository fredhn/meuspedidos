﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.XtraLayout;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;

namespace View_MeusPedidos
{
    public class FormContols_FW
    {
        public static void fieldsReadOnly(LayoutControl lc, bool value)
        {
            foreach(var control in lc.Controls)
            {
                Type obj = control.GetType();
                //var obj2;
                
                switch(obj.Name.ToUpper())
                {
                    case "TEXTEDIT":
                        TextEdit te = (TextEdit) control;
                        te.ReadOnly = value;
                    break;

                    case "TOGGLESWITCH":
                        ToggleSwitch ts = (ToggleSwitch) control;
                        ts.ReadOnly = value;
                    break;
                    case "DATEEDIT":
                        DateEdit de = (DateEdit)control;
                        de.ReadOnly = value;
                    break;
                    case "COMBOBOXEDIT":
                        ComboBoxEdit cbe = (ComboBoxEdit)control;
                        cbe.ReadOnly = value;
                    break;
                    case "MEMOEDIT":
                        MemoEdit me = (MemoEdit)control;
                        me.ReadOnly = value;
                    break;
                    case "GRIDCONTROL":
                        GridControl gc = (GridControl)control;
                        gc.Enabled = !value;
                    break;
                    case "SIMPLEBUTTON":
                        SimpleButton sb = (SimpleButton)control;
                        sb.Enabled = !value;
                    break;
                    case "LOOKUPEDIT":
                        LookUpEdit lue = (LookUpEdit)control;
                        lue.Enabled = !value;
                    break;

                    default:
                    break;
                }
            }
        }

        public static void fieldsClean(LayoutControl lc)
        {
            foreach (var control in lc.Controls)
            {
                Type obj = control.GetType();
                //var obj2;

                switch (obj.Name.ToUpper())
                {
                    case "TEXTEDIT":
                        TextEdit te = (TextEdit)control;
                        te.Text = "";
                    break;
                    case "TOGGLESWITCH":
                        ToggleSwitch ts = (ToggleSwitch)control;
                        ts.EditValue = 0;
                    break;
                    case "DATEEDIT":
                        DateEdit de = (DateEdit)control;
                        de.Text = "";
                    break;
                    case "COMBOBOXEDIT":
                        ComboBoxEdit cbe = (ComboBoxEdit)control;
                        cbe.Text = "";
                        cbe.EditValue = "";
                        cbe.Properties.Items.Clear();
                        break;
                    case "MEMOEDIT":
                        MemoEdit me = (MemoEdit)control;
                        me.Text = "";
                    break;
                    case "LOOKUPEDIT":
                        LookUpEdit lue = (LookUpEdit)control;
                        lue.Text = "";
                        lue.EditValue = "";
                    break;

                    default:
                        break;
                }
            }
        }
    }
}

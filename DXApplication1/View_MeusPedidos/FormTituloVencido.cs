﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Control;
using Core;
using DevExpress.XtraEditors;
using System.Data.Entity;
using Control.Validations;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors.Controls;

namespace View_MeusPedidos
{
    public partial class FormTituloVencido : DevExpress.XtraEditors.XtraForm
    {
        TituloVencido_Service s_tv;
        TituloVencido tv;
        Cliente_Service s_cli;

        int _id;
        bool add_status = false;

        public FormTituloVencido()
        {
            InitializeComponent();
            s_tv = new TituloVencido_Service();
            s_cli = new Cliente_Service();

            bar_Acoes2.Visible = false;

            tabNavigationPage_RegisterTituloVenc.PageVisible = false;
            tabPane_ConfigTituloVenc.SelectedPage = tabNavigationPage_DataGridFormaPag;

            gridControl_TituloVenc.DataSource = s_tv.GetAll_TituloVencido();

            BindingSource srcCli = new BindingSource();
            srcCli.DataSource = s_cli.GetAll_Clientes();
            this.comboBoxEdit_TituloVencCliente.Properties.DataSource = srcCli.List;
            this.comboBoxEdit_TituloVencCliente.Properties.Columns.Add(new LookUpColumnInfo("id", "Id"));
            this.comboBoxEdit_TituloVencCliente.Properties.Columns.Add(new LookUpColumnInfo("razao_social", "Nome"));
            this.comboBoxEdit_TituloVencCliente.Properties.DisplayMember = "razao_social";
            this.comboBoxEdit_TituloVencCliente.Properties.ValueMember = "id";
        }

        #region ToolBar1 Events - Adicionar/Editar/Deletar

        private void barButtonItem_Add_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            add_status = true;

            //UI Behavior

            FormContols_FW.fieldsClean(layoutControl_TituloVenc);

            bar_Acoes.Visible = false;
            bar_Acoes2.Visible = true;

            tabNavigationPage_RegisterTituloVenc.PageVisible = true;
            tabNavigationPage_DataGridFormaPag.PageVisible = false;
            tabPane_ConfigTituloVenc.SelectedPage = tabNavigationPage_RegisterTituloVenc;

            //Define TextEdit Fields as ReadOnly = false
            FormContols_FW.fieldsReadOnly(layoutControl_TituloVenc, false);
        }

        private void barButtonItem_Edit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            add_status = false;

            var row = gridView_TituloVenc.GetRow(gridView_TituloVenc.GetFocusedDataSourceRowIndex());
            tv = (TituloVencido)row;

            if(tv != null)
            {
                if (XtraMessageBox.Show("Tem certeza que deseja alterar o registro (ID: " + tv.id + ")?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    //Retrieve entity ID
                    _id = tv.id;

                    //Fill TextEdit Fields
                    comboBoxEdit_TituloVencCliente.EditValue = tv.cliente_id;
                    textEdit_TituloVencNoDoc.Text = tv.numero_documento;
                    dateEdit_TituloVencDataVencimento.DateTime = tv.data_vencimento;
                    textEdit_TituloVencValor.EditValue = tv.valor;
                    memoEdit_TituloVencObservacao.Text = tv.observacao;
                    textEdit_TituloVencExcluido.EditValue = tv.excluido;
                    dateEdit_TituloVencUltAlteracao.DateTime = tv.ultima_alteracao;

                    //UI Behavior
                    bar_Acoes.Visible = false;
                    bar_Acoes2.Visible = true;

                    tabNavigationPage_RegisterTituloVenc.PageVisible = true;
                    tabNavigationPage_DataGridFormaPag.PageVisible = false;
                    tabPane_ConfigTituloVenc.SelectedPage = tabNavigationPage_RegisterTituloVenc;

                    //Define TextEdit Fields as ReadOnly = false
                    FormContols_FW.fieldsReadOnly(layoutControl_TituloVenc, false);
                }
            }
            
        }

        private void barButtonItem_Del_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var row = gridView_TituloVenc.GetRow(gridView_TituloVenc.GetFocusedDataSourceRowIndex());
            tv = (TituloVencido)row;
            
            if(tv != null)
            {
                if (XtraMessageBox.Show("Tem certeza que deseja excluir o registro (ID: " + tv.id + ")?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    tv.excluido = true;
                    s_tv.Update_TituloVencido(tv);

                    gridControl_TituloVenc.DataSource = s_tv.GetAll_TituloVencido();
                }
            }
            
        }
        #endregion

        #region ToolBar2 Events - Cancelar/Gravar
        private void barButtonItem_Gravar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (add_status == true &&
                textEdit_TituloVencNoDoc.Text != "" &&
                textEdit_TituloVencValor.Text != "" &&
                comboBoxEdit_TituloVencCliente.Text != "" &&
                dateEdit_TituloVencDataVencimento.Text != "")
            {

                tv = new TituloVencido();
                tv.numero_documento = textEdit_TituloVencNoDoc.Text;
                tv.cliente_id = int.Parse(comboBoxEdit_TituloVencCliente.EditValue.ToString());
                tv.data_vencimento = dateEdit_TituloVencDataVencimento.DateTime;
                tv.valor = double.Parse(textEdit_TituloVencValor.EditValue.ToString());
                tv.observacao = memoEdit_TituloVencObservacao.Text;
                tv.excluido = textEdit_TituloVencExcluido.IsOn;
                tv.ultima_alteracao = DateTime.Now;
                tv.mptran = "I";

                //Validação Nome
                var aux = s_tv.GetBySpecification_TituloVencido(tv.id);

                if (aux == null)
                {
                    splashScreenManager_Transp.ShowWaitForm();

                    //Add new ProdCat
                    s_tv.AddNew_TituloVencido(tv);

                    splashScreenManager_Transp.CloseWaitForm();

                    XtraMessageBox.Show("Registro salvo!", "Informação");

                    FormContols_FW.fieldsClean(layoutControl_TituloVenc);

                    gridControl_TituloVenc.DataSource = s_tv.GetAll_TituloVencido();

                    bar_Acoes.Visible = true;
                    bar_Acoes2.Visible = false;
                    tabNavigationPage_RegisterTituloVenc.PageVisible = false;
                    tabNavigationPage_DataGridFormaPag.PageVisible = true;
                    tabPane_ConfigTituloVenc.SelectedPage = tabNavigationPage_DataGridFormaPag;
                }
                else
                {
                    splashScreenManager_Transp.CloseWaitForm();

                    XtraMessageBox.Show("Já existe uma transportadora com este nome cadastrado!", "Atenção");
                }

            }
            else if (add_status == false &&
                textEdit_TituloVencNoDoc.Text != "" &&
                textEdit_TituloVencValor.Text != "" &&
                comboBoxEdit_TituloVencCliente.Text != "" &&
                dateEdit_TituloVencDataVencimento.Text != "")
            {
                splashScreenManager_Transp.ShowWaitForm();

                tv = new TituloVencido();
                tv.id = _id;

                tv = s_tv.GetBySpecification_TituloVencido(tv.id);

                tv.cliente_id = int.Parse(comboBoxEdit_TituloVencCliente.EditValue.ToString());
                tv.numero_documento = textEdit_TituloVencNoDoc.Text;
                tv.data_vencimento = dateEdit_TituloVencDataVencimento.DateTime;
                tv.valor = double.Parse(textEdit_TituloVencValor.EditValue.ToString());
                tv.observacao = memoEdit_TituloVencObservacao.Text;
                tv.excluido = textEdit_TituloVencExcluido.IsOn;
                tv.ultima_alteracao = DateTime.Now;
                tv.mptran = "A";

                s_tv.Update_TituloVencido(tv);

                splashScreenManager_Transp.CloseWaitForm();

                XtraMessageBox.Show("Alteração realizada com sucesso!", "Informação");

                FormContols_FW.fieldsClean(layoutControl_TituloVenc);

                gridControl_TituloVenc.DataSource = s_tv.GetAll_TituloVencido();

                bar_Acoes.Visible = true;
                bar_Acoes2.Visible = false;
                tabNavigationPage_RegisterTituloVenc.PageVisible = false;
                tabNavigationPage_DataGridFormaPag.PageVisible = true;
                tabPane_ConfigTituloVenc.SelectedPage = tabNavigationPage_DataGridFormaPag;
            }
            else
            {
                XtraMessageBox.Show("Algum campo obrigatório precisa ser preenchido!", "Informação");
            }
        }

        private void barButtonItem_Cancelar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (XtraMessageBox.Show("Tem certeza que deseja sair da tela?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                barButtonItem_Gravar.Enabled = true;

                bar_Acoes.Visible = true;
                bar_Acoes2.Visible = false;
                tabNavigationPage_RegisterTituloVenc.PageVisible = false;
                tabNavigationPage_DataGridFormaPag.PageVisible = true;
                tabPane_ConfigTituloVenc.SelectedPage = tabNavigationPage_DataGridFormaPag;

                FormContols_FW.fieldsClean(layoutControl_TituloVenc);
            }
        }
        #endregion

        #region Grid Transportadora Events
        private void gridView_Clientes_DoubleClick(object sender, EventArgs e)
        {
            barButtonItem_Gravar.Enabled = false;

            //Get (object) row values
            var row = gridView_TituloVenc.GetRow(gridView_TituloVenc.GetFocusedDataSourceRowIndex());
            tv = (TituloVencido)row;

            //Fill TextEdit Fields
            textEdit_TituloVencNoDoc.Text = tv.numero_documento;
            dateEdit_TituloVencDataVencimento.DateTime = tv.data_vencimento;
            textEdit_TituloVencValor.EditValue = tv.valor;
            memoEdit_TituloVencObservacao.Text = tv.observacao;
            textEdit_TituloVencExcluido.EditValue = tv.excluido;
            dateEdit_TituloVencUltAlteracao.DateTime = tv.ultima_alteracao;

            //Define TextEdit Fields as ReadOnly = true
            FormContols_FW.fieldsReadOnly(layoutControl_TituloVenc, true);

            //UI Behavior
            bar_Acoes.Visible = false;
            bar_Acoes2.Visible = true;

            tabNavigationPage_RegisterTituloVenc.PageVisible = true;
            tabNavigationPage_DataGridFormaPag.PageVisible = false;
            tabPane_ConfigTituloVenc.SelectedPage = tabNavigationPage_RegisterTituloVenc;
        }
        #endregion

        private void FormTabelaPreco_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'meusPedidosDataSet.TituloVencido' table. You can move, or remove it, as needed.
            this.tituloVencidoTableAdapter.Fill(this.meusPedidosDataSet.TituloVencido);
        }
    }
}

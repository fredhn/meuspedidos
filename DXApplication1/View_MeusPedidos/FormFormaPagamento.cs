﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Control;
using Core;
using DevExpress.XtraEditors;
using System.Data.Entity;
using Control.Validations;
using DevExpress.XtraGrid.Views.Grid;

namespace View_MeusPedidos
{
    public partial class FormFormaPagamento : DevExpress.XtraEditors.XtraForm
    {
        FormaPagamento_Service s_fp;
        FormaPagamento fp;

        int _id;
        bool add_status = false;

        public FormFormaPagamento()
        {
            InitializeComponent();
            s_fp = new FormaPagamento_Service();

            bar_Acoes2.Visible = false;

            tabNavigationPage_RegisterFormaPag.PageVisible = false;
            tabPane_ConfigFormaPag.SelectedPage = tabNavigationPage_DataGridFormaPag;

            gridControl_FormaPag.DataSource = s_fp.GetAll_FormaPagamento();
        }

        #region ToolBar1 Events - Adicionar/Editar/Deletar

        private void barButtonItem_Add_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            add_status = true;

            //UI Behavior

            FormContols_FW.fieldsClean(layoutControl_FormaPag);

            bar_Acoes.Visible = false;
            bar_Acoes2.Visible = true;

            tabNavigationPage_RegisterFormaPag.PageVisible = true;
            tabNavigationPage_DataGridFormaPag.PageVisible = false;
            tabPane_ConfigFormaPag.SelectedPage = tabNavigationPage_RegisterFormaPag;

            //Define TextEdit Fields as ReadOnly = false
            FormContols_FW.fieldsReadOnly(layoutControl_FormaPag, false);
        }

        private void barButtonItem_Edit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            add_status = false;

            var row = gridView_FormaPag.GetRow(gridView_FormaPag.GetFocusedDataSourceRowIndex());
            fp = (FormaPagamento)row;

            if(fp != null)
            {
                if (XtraMessageBox.Show("Tem certeza que deseja alterar o registro (ID: " + fp.id + ")?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    //Retrieve entity ID
                    _id = fp.id;

                    //Fill TextEdit Fields
                    textEdit_FormaPagNome.Text = fp.nome;
                    textEdit_FormaPagExcluido.EditValue = fp.excluido;
                    dateEdit_FormaPagUltAlteracao.DateTime = fp.ultima_alteracao;

                    //UI Behavior
                    bar_Acoes.Visible = false;
                    bar_Acoes2.Visible = true;

                    tabNavigationPage_RegisterFormaPag.PageVisible = true;
                    tabNavigationPage_DataGridFormaPag.PageVisible = false;
                    tabPane_ConfigFormaPag.SelectedPage = tabNavigationPage_RegisterFormaPag;

                    //Define TextEdit Fields as ReadOnly = false
                    FormContols_FW.fieldsReadOnly(layoutControl_FormaPag, false);
                }
            }
            
        }

        private void barButtonItem_Del_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var row = gridView_FormaPag.GetRow(gridView_FormaPag.GetFocusedDataSourceRowIndex());
            fp = (FormaPagamento)row;
            
            if(fp != null)
            {
                if (XtraMessageBox.Show("Tem certeza que deseja excluir o registro (ID: " + fp.id + ")?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    fp.excluido = true;
                    s_fp.Update_FormaPagamento(fp);

                    gridControl_FormaPag.DataSource = s_fp.GetAll_FormaPagamento();
                }
            }
            
        }
        #endregion

        #region ToolBar2 Events - Cancelar/Gravar
        private void barButtonItem_Gravar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (add_status == true &&
                textEdit_FormaPagNome.Text != "")
            {
                fp = new FormaPagamento();
                fp.nome = textEdit_FormaPagNome.Text;
                fp.excluido = textEdit_FormaPagExcluido.IsOn;
                fp.ultima_alteracao = DateTime.Now;
                fp.mptran = "I";

                //Validação Nome
                var aux = s_fp.GetByNome_FormaPagamento(fp.nome);

                if (aux == null)
                {
                    splashScreenManager_Transp.ShowWaitForm();

                    //Add new ProdCat
                    s_fp.AddNew_FormaPagamento(fp);

                    splashScreenManager_Transp.CloseWaitForm();

                    XtraMessageBox.Show("Registro salvo!", "Informação");

                    FormContols_FW.fieldsClean(layoutControl_FormaPag);

                    gridControl_FormaPag.DataSource = s_fp.GetAll_FormaPagamento();

                    bar_Acoes.Visible = true;
                    bar_Acoes2.Visible = false;
                    tabNavigationPage_RegisterFormaPag.PageVisible = false;
                    tabNavigationPage_DataGridFormaPag.PageVisible = true;
                    tabPane_ConfigFormaPag.SelectedPage = tabNavigationPage_DataGridFormaPag;
                }
                else
                {
                    splashScreenManager_Transp.CloseWaitForm();

                    XtraMessageBox.Show("Já existe uma transportadora com este nome cadastrado!", "Atenção");
                }

            }
            else if (add_status == false &&
                textEdit_FormaPagNome.Text != "")
            {
                splashScreenManager_Transp.ShowWaitForm();

                fp = new FormaPagamento();
                fp.id = _id;

                fp = s_fp.GetBySpecification_FormaPagamento(fp.id);

                fp.nome = textEdit_FormaPagNome.Text;
                fp.excluido = textEdit_FormaPagExcluido.IsOn;
                fp.ultima_alteracao = DateTime.Now;
                fp.mptran = "A";

                s_fp.Update_FormaPagamento(fp);

                splashScreenManager_Transp.CloseWaitForm();

                XtraMessageBox.Show("Alteração realizada com sucesso!", "Informação");

                FormContols_FW.fieldsClean(layoutControl_FormaPag);

                gridControl_FormaPag.DataSource = s_fp.GetAll_FormaPagamento();

                bar_Acoes.Visible = true;
                bar_Acoes2.Visible = false;
                tabNavigationPage_RegisterFormaPag.PageVisible = false;
                tabNavigationPage_DataGridFormaPag.PageVisible = true;
                tabPane_ConfigFormaPag.SelectedPage = tabNavigationPage_DataGridFormaPag;
            }
            else
            {
                XtraMessageBox.Show("Algum campo obrigatório precisa ser preenchido!", "Informação");
            }
        }

        private void barButtonItem_Cancelar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (XtraMessageBox.Show("Tem certeza que deseja sair da tela?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                barButtonItem_Gravar.Enabled = true;

                bar_Acoes.Visible = true;
                bar_Acoes2.Visible = false;
                tabNavigationPage_RegisterFormaPag.PageVisible = false;
                tabNavigationPage_DataGridFormaPag.PageVisible = true;
                tabPane_ConfigFormaPag.SelectedPage = tabNavigationPage_DataGridFormaPag;

                FormContols_FW.fieldsClean(layoutControl_FormaPag);
            }
        }
        #endregion

        #region Grid Transportadora Events
        private void gridView_Clientes_DoubleClick(object sender, EventArgs e)
        {
            barButtonItem_Gravar.Enabled = false;

            //Get (object) row values
            var row = gridView_FormaPag.GetRow(gridView_FormaPag.GetFocusedDataSourceRowIndex());
            fp = (FormaPagamento)row;

            //Fill TextEdit Fields
            textEdit_FormaPagNome.Text = fp.nome;
            textEdit_FormaPagExcluido.EditValue = fp.excluido;
            dateEdit_FormaPagUltAlteracao.DateTime = fp.ultima_alteracao;

            //Define TextEdit Fields as ReadOnly = true
            FormContols_FW.fieldsReadOnly(layoutControl_FormaPag, true);

            //UI Behavior
            bar_Acoes.Visible = false;
            bar_Acoes2.Visible = true;

            tabNavigationPage_RegisterFormaPag.PageVisible = true;
            tabNavigationPage_DataGridFormaPag.PageVisible = false;
            tabPane_ConfigFormaPag.SelectedPage = tabNavigationPage_RegisterFormaPag;
        }
        #endregion

        private void FormTabelaPreco_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'meusPedidosDataSet.FormaPagamento' table. You can move, or remove it, as needed.
            this.formaPagamentoTableAdapter.Fill(this.meusPedidosDataSet.FormaPagamento);
        }
    }
}

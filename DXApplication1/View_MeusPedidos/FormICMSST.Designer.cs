﻿namespace View_MeusPedidos
{
    partial class FormICMSST
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement10 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement11 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement12 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement13 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement14 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement15 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement16 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement17 = new DevExpress.XtraEditors.TileItemElement();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormICMSST));
            this.tileNavSubItem_Cliente = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CondicaoPagamentoCliente = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CategoriasCliente = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TabelaPreco = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Usuarios = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Produto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_ImagensProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TabelaProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TabelaPrecoProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CategoriaProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Pedido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CamposExtraPedido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Transportadora = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CondicaoPagamento = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_FormaPagamento = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_FaturamentoPedido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TituloVencido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.splashScreenManager_ICMSST = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::View_MeusPedidos.WaitForm1), true, true);
            this.tabNavigationPage_RegisterICMSST = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.panelControl_RegisterICMSST = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl_ICMSST = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit_ICMSSTCod = new DevExpress.XtraEditors.TextEdit();
            this.barManager_ContasMP = new DevExpress.XtraBars.BarManager(this.components);
            this.bar_Acoes = new DevExpress.XtraBars.Bar();
            this.barButtonItem_Add = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem_Edit = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem_Del = new DevExpress.XtraBars.BarButtonItem();
            this.bar_Acoes2 = new DevExpress.XtraBars.Bar();
            this.barButtonItem_Gravar = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem_Cancelar = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.dateEdit_ICMSSTUltAlteracao = new DevExpress.XtraEditors.DateEdit();
            this.textEdit_ICMSSTExcluido = new DevExpress.XtraEditors.ToggleSwitch();
            this.textEdit_ICMSSTValMVA = new DevExpress.XtraEditors.TextEdit();
            this.textEdit__ICMSSTNomeExcFiscal = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_ICMSSTValPMC = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_ICMSSTCredito = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_ICMSSTDestino = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEdit_ICMSSTEstDestino = new DevExpress.XtraEditors.LookUpEdit();
            this.comboBoxEdit_ICMSSTTipoST = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup_RegisterICMSST = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleLabelItem_RegisterICMSST = new DevExpress.XtraLayout.SimpleLabelItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup_ICMSST = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem_ICMSSTCod = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_ICMSSTDtUltAlteracao = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_ICMSSTExcluido = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem18 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem_ICMSSTNomeExcFiscal = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_ICMSSTEstDestino = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_ICMSSTValMVA = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_ICMSSTCredito = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_ICMSSTTipoSt = new DevExpress.XtraLayout.LayoutControlItem();
            this._ICMSSTValPMC = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem__ICMSSTDestino = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem20 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.meusPedidosDataSet = new View_MeusPedidos.MeusPedidosDataSet();
            this.simpleLabelItem_RegisterClientes = new DevExpress.XtraLayout.SimpleLabelItem();
            this.tabNavigationPage_DataGridICMSST = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.gridControl_ICMSST = new DevExpress.XtraGrid.GridControl();
            this.iCMSSTBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView_ICMSST = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcodigo_ncm = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colnome_excecao_fiscal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colestado_destino = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coltipo_st = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colvalor_mva = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colvalor_pmc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colicms_credito = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colicms_destino = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colexcluido = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colultima_alteracao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colmptran = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tabPane_ConfigICMSST = new DevExpress.XtraBars.Navigation.TabPane();
            this.tabPane1 = new DevExpress.XtraBars.Navigation.TabPane();
            this.tabNavigationPage1 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.tabNavigationPage2 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit8 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit9 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit10 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit11 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit12 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit13 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit14 = new DevExpress.XtraEditors.TextEdit();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.toggleSwitch1 = new DevExpress.XtraEditors.ToggleSwitch();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleLabelItem1 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem16 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup3 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup4 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.simpleSeparator2 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator3 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlGroup_CLITelefone1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.colid1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colrazao_social = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colnome_fantasia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup_CatProd = new DevExpress.XtraLayout.LayoutControlGroup();
            this.iCMS_STTableAdapter = new View_MeusPedidos.MeusPedidosDataSetTableAdapters.ICMS_STTableAdapter();
            this.tabNavigationPage_RegisterICMSST.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl_RegisterICMSST)).BeginInit();
            this.panelControl_RegisterICMSST.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_ICMSST)).BeginInit();
            this.layoutControl_ICMSST.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ICMSSTCod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager_ContasMP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_ICMSSTUltAlteracao.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_ICMSSTUltAlteracao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ICMSSTExcluido.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ICMSSTValMVA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit__ICMSSTNomeExcFiscal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ICMSSTValPMC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ICMSSTCredito.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ICMSSTDestino.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit_ICMSSTEstDestino.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit_ICMSSTTipoST.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_RegisterICMSST)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem_RegisterICMSST)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_ICMSST)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ICMSSTCod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ICMSSTDtUltAlteracao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ICMSSTExcluido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ICMSSTNomeExcFiscal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ICMSSTEstDestino)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ICMSSTValMVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ICMSSTCredito)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ICMSSTTipoSt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._ICMSSTValPMC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem__ICMSSTDestino)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meusPedidosDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem_RegisterClientes)).BeginInit();
            this.tabNavigationPage_DataGridICMSST.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_ICMSST)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iCMSSTBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_ICMSST)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane_ConfigICMSST)).BeginInit();
            this.tabPane_ConfigICMSST.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).BeginInit();
            this.tabPane1.SuspendLayout();
            this.tabNavigationPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toggleSwitch1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CLITelefone1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CatProd)).BeginInit();
            this.SuspendLayout();
            // 
            // tileNavSubItem_Cliente
            // 
            this.tileNavSubItem_Cliente.Caption = "Cliente";
            this.tileNavSubItem_Cliente.Name = "tileNavSubItem_Cliente";
            // 
            // 
            // 
            this.tileNavSubItem_Cliente.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement1.Text = "Cliente";
            this.tileNavSubItem_Cliente.Tile.Elements.Add(tileItemElement1);
            this.tileNavSubItem_Cliente.Tile.Name = "tileBarItem3";
            // 
            // tileNavSubItem_CondicaoPagamentoCliente
            // 
            this.tileNavSubItem_CondicaoPagamentoCliente.Caption = "Condições de Pagamento por Cliente";
            this.tileNavSubItem_CondicaoPagamentoCliente.Name = "tileNavSubItem_CondicaoPagamentoCliente";
            // 
            // 
            // 
            this.tileNavSubItem_CondicaoPagamentoCliente.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement2.Text = "Condições de Pagamento por Cliente";
            this.tileNavSubItem_CondicaoPagamentoCliente.Tile.Elements.Add(tileItemElement2);
            this.tileNavSubItem_CondicaoPagamentoCliente.Tile.Name = "tileBarItem4";
            // 
            // tileNavSubItem_CategoriasCliente
            // 
            this.tileNavSubItem_CategoriasCliente.Caption = "Categorias por Cliente";
            this.tileNavSubItem_CategoriasCliente.Name = "tileNavSubItem_CategoriasCliente";
            // 
            // 
            // 
            this.tileNavSubItem_CategoriasCliente.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement3.Text = "Categorias por Cliente";
            this.tileNavSubItem_CategoriasCliente.Tile.Elements.Add(tileItemElement3);
            this.tileNavSubItem_CategoriasCliente.Tile.Name = "tileBarItem6";
            // 
            // tileNavSubItem_TabelaPreco
            // 
            this.tileNavSubItem_TabelaPreco.Caption = "Tabelas de Preço por Cliente";
            this.tileNavSubItem_TabelaPreco.Name = "tileNavSubItem_TabelaPreco";
            // 
            // 
            // 
            this.tileNavSubItem_TabelaPreco.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement4.Text = "Tabelas de Preço por Cliente";
            this.tileNavSubItem_TabelaPreco.Tile.Elements.Add(tileItemElement4);
            this.tileNavSubItem_TabelaPreco.Tile.Name = "tileBarItem7";
            // 
            // tileNavSubItem_Usuarios
            // 
            this.tileNavSubItem_Usuarios.Caption = "Usuários (Vendedores)";
            this.tileNavSubItem_Usuarios.Name = "tileNavSubItem_Usuarios";
            // 
            // 
            // 
            this.tileNavSubItem_Usuarios.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement5.Text = "Usuários (Vendedores)";
            this.tileNavSubItem_Usuarios.Tile.Elements.Add(tileItemElement5);
            this.tileNavSubItem_Usuarios.Tile.Name = "tileBarItem8";
            // 
            // tileNavSubItem_Produto
            // 
            this.tileNavSubItem_Produto.Caption = "Produto";
            this.tileNavSubItem_Produto.Name = "tileNavSubItem_Produto";
            // 
            // 
            // 
            this.tileNavSubItem_Produto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement6.Text = "Produto";
            this.tileNavSubItem_Produto.Tile.Elements.Add(tileItemElement6);
            this.tileNavSubItem_Produto.Tile.Name = "tileBarItem9";
            // 
            // tileNavSubItem_ImagensProduto
            // 
            this.tileNavSubItem_ImagensProduto.Caption = "Imagens do Produto";
            this.tileNavSubItem_ImagensProduto.Name = "tileNavSubItem_ImagensProduto";
            // 
            // 
            // 
            this.tileNavSubItem_ImagensProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement7.Text = "Imagens do Produto";
            this.tileNavSubItem_ImagensProduto.Tile.Elements.Add(tileItemElement7);
            this.tileNavSubItem_ImagensProduto.Tile.Name = "tileBarItem10";
            // 
            // tileNavSubItem_TabelaProduto
            // 
            this.tileNavSubItem_TabelaProduto.Caption = "Tabela de Preço";
            this.tileNavSubItem_TabelaProduto.Name = "tileNavSubItem_TabelaProduto";
            // 
            // 
            // 
            this.tileNavSubItem_TabelaProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement8.Text = "Tabelas de Preço";
            this.tileNavSubItem_TabelaProduto.Tile.Elements.Add(tileItemElement8);
            this.tileNavSubItem_TabelaProduto.Tile.Name = "tileBarItem11";
            // 
            // tileNavSubItem_TabelaPrecoProduto
            // 
            this.tileNavSubItem_TabelaPrecoProduto.Caption = "Tabelas de Preço por Produto";
            this.tileNavSubItem_TabelaPrecoProduto.Name = "tileNavSubItem_TabelaPrecoProduto";
            // 
            // 
            // 
            this.tileNavSubItem_TabelaPrecoProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement9.Text = "Tabelas de Preço por Produto";
            this.tileNavSubItem_TabelaPrecoProduto.Tile.Elements.Add(tileItemElement9);
            this.tileNavSubItem_TabelaPrecoProduto.Tile.Name = "tileBarItem12";
            // 
            // tileNavSubItem_CategoriaProduto
            // 
            this.tileNavSubItem_CategoriaProduto.Caption = "Categorias de Produtos";
            this.tileNavSubItem_CategoriaProduto.Name = "tileNavSubItem_CategoriaProduto";
            // 
            // 
            // 
            this.tileNavSubItem_CategoriaProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement10.Text = "Categorias de Produtos";
            this.tileNavSubItem_CategoriaProduto.Tile.Elements.Add(tileItemElement10);
            this.tileNavSubItem_CategoriaProduto.Tile.Name = "tileBarItem13";
            // 
            // tileNavSubItem_Pedido
            // 
            this.tileNavSubItem_Pedido.Caption = "Pedido";
            this.tileNavSubItem_Pedido.Name = "tileNavSubItem_Pedido";
            // 
            // 
            // 
            this.tileNavSubItem_Pedido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement11.Text = "Pedido";
            this.tileNavSubItem_Pedido.Tile.Elements.Add(tileItemElement11);
            this.tileNavSubItem_Pedido.Tile.Name = "tileBarItem17";
            // 
            // tileNavSubItem_CamposExtraPedido
            // 
            this.tileNavSubItem_CamposExtraPedido.Caption = "Campos Extras do Pedido";
            this.tileNavSubItem_CamposExtraPedido.Name = "tileNavSubItem_CamposExtraPedido";
            // 
            // 
            // 
            this.tileNavSubItem_CamposExtraPedido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement12.Text = "Campos Extras do Pedido";
            this.tileNavSubItem_CamposExtraPedido.Tile.Elements.Add(tileItemElement12);
            this.tileNavSubItem_CamposExtraPedido.Tile.Name = "tileBarItem18";
            // 
            // tileNavSubItem_Transportadora
            // 
            this.tileNavSubItem_Transportadora.Caption = "Transportadoras";
            this.tileNavSubItem_Transportadora.Name = "tileNavSubItem_Transportadora";
            // 
            // 
            // 
            this.tileNavSubItem_Transportadora.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement13.Text = "Transportadoras";
            this.tileNavSubItem_Transportadora.Tile.Elements.Add(tileItemElement13);
            this.tileNavSubItem_Transportadora.Tile.Name = "tileBarItem14";
            // 
            // tileNavSubItem_CondicaoPagamento
            // 
            this.tileNavSubItem_CondicaoPagamento.Caption = "Condições de Pagamento";
            this.tileNavSubItem_CondicaoPagamento.Name = "tileNavSubItem_CondicaoPagamento";
            // 
            // 
            // 
            this.tileNavSubItem_CondicaoPagamento.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement14.Text = "Condições de Pagamento";
            this.tileNavSubItem_CondicaoPagamento.Tile.Elements.Add(tileItemElement14);
            this.tileNavSubItem_CondicaoPagamento.Tile.Name = "tileBarItem15";
            // 
            // tileNavSubItem_FormaPagamento
            // 
            this.tileNavSubItem_FormaPagamento.Caption = "Formas de Pagamento";
            this.tileNavSubItem_FormaPagamento.Name = "tileNavSubItem_FormaPagamento";
            // 
            // 
            // 
            this.tileNavSubItem_FormaPagamento.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement15.Text = "Formas de Pagamento";
            this.tileNavSubItem_FormaPagamento.Tile.Elements.Add(tileItemElement15);
            this.tileNavSubItem_FormaPagamento.Tile.Name = "tileBarItem16";
            // 
            // tileNavSubItem_FaturamentoPedido
            // 
            this.tileNavSubItem_FaturamentoPedido.Caption = "Faturamento de Pedido";
            this.tileNavSubItem_FaturamentoPedido.Name = "tileNavSubItem_FaturamentoPedido";
            // 
            // 
            // 
            this.tileNavSubItem_FaturamentoPedido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement16.Text = "Faturamento de Pedido";
            this.tileNavSubItem_FaturamentoPedido.Tile.Elements.Add(tileItemElement16);
            this.tileNavSubItem_FaturamentoPedido.Tile.Name = "tileBarItem19";
            // 
            // tileNavSubItem_TituloVencido
            // 
            this.tileNavSubItem_TituloVencido.Caption = "Títulos Vencidos";
            this.tileNavSubItem_TituloVencido.Name = "tileNavSubItem_TituloVencido";
            // 
            // 
            // 
            this.tileNavSubItem_TituloVencido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement17.Text = "Títulos Vencidos";
            this.tileNavSubItem_TituloVencido.Tile.Elements.Add(tileItemElement17);
            this.tileNavSubItem_TituloVencido.Tile.Name = "tileBarItem20";
            // 
            // splashScreenManager_ICMSST
            // 
            this.splashScreenManager_ICMSST.ClosingDelay = 500;
            // 
            // tabNavigationPage_RegisterICMSST
            // 
            this.tabNavigationPage_RegisterICMSST.Caption = "Cadastro";
            this.tabNavigationPage_RegisterICMSST.Controls.Add(this.panelControl_RegisterICMSST);
            this.tabNavigationPage_RegisterICMSST.Image = ((System.Drawing.Image)(resources.GetObject("tabNavigationPage_RegisterICMSST.Image")));
            this.tabNavigationPage_RegisterICMSST.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage_RegisterICMSST.Name = "tabNavigationPage_RegisterICMSST";
            this.tabNavigationPage_RegisterICMSST.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage_RegisterICMSST.Size = new System.Drawing.Size(1246, 404);
            // 
            // panelControl_RegisterICMSST
            // 
            this.panelControl_RegisterICMSST.Controls.Add(this.layoutControl_ICMSST);
            this.panelControl_RegisterICMSST.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl_RegisterICMSST.Location = new System.Drawing.Point(0, 0);
            this.panelControl_RegisterICMSST.Name = "panelControl_RegisterICMSST";
            this.panelControl_RegisterICMSST.Size = new System.Drawing.Size(1246, 404);
            this.panelControl_RegisterICMSST.TabIndex = 0;
            // 
            // layoutControl_ICMSST
            // 
            this.layoutControl_ICMSST.Controls.Add(this.textEdit_ICMSSTCod);
            this.layoutControl_ICMSST.Controls.Add(this.dateEdit_ICMSSTUltAlteracao);
            this.layoutControl_ICMSST.Controls.Add(this.textEdit_ICMSSTExcluido);
            this.layoutControl_ICMSST.Controls.Add(this.textEdit_ICMSSTValMVA);
            this.layoutControl_ICMSST.Controls.Add(this.textEdit__ICMSSTNomeExcFiscal);
            this.layoutControl_ICMSST.Controls.Add(this.textEdit_ICMSSTValPMC);
            this.layoutControl_ICMSST.Controls.Add(this.textEdit_ICMSSTCredito);
            this.layoutControl_ICMSST.Controls.Add(this.textEdit_ICMSSTDestino);
            this.layoutControl_ICMSST.Controls.Add(this.comboBoxEdit_ICMSSTEstDestino);
            this.layoutControl_ICMSST.Controls.Add(this.comboBoxEdit_ICMSSTTipoST);
            this.layoutControl_ICMSST.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl_ICMSST.Location = new System.Drawing.Point(2, 2);
            this.layoutControl_ICMSST.Name = "layoutControl_ICMSST";
            this.layoutControl_ICMSST.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(124, 125, 450, 400);
            this.layoutControl_ICMSST.Root = this.layoutControlGroup_RegisterICMSST;
            this.layoutControl_ICMSST.Size = new System.Drawing.Size(1242, 400);
            this.layoutControl_ICMSST.TabIndex = 0;
            this.layoutControl_ICMSST.Text = "layoutControl_TituloVenc";
            // 
            // textEdit_ICMSSTCod
            // 
            this.textEdit_ICMSSTCod.Location = new System.Drawing.Point(136, 117);
            this.textEdit_ICMSSTCod.MenuManager = this.barManager_ContasMP;
            this.textEdit_ICMSSTCod.Name = "textEdit_ICMSSTCod";
            this.textEdit_ICMSSTCod.Properties.MaxLength = 20;
            this.textEdit_ICMSSTCod.Size = new System.Drawing.Size(1072, 20);
            this.textEdit_ICMSSTCod.StyleController = this.layoutControl_ICMSST;
            this.textEdit_ICMSSTCod.TabIndex = 6;
            // 
            // barManager_ContasMP
            // 
            this.barManager_ContasMP.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar_Acoes,
            this.bar_Acoes2});
            this.barManager_ContasMP.DockControls.Add(this.barDockControlTop);
            this.barManager_ContasMP.DockControls.Add(this.barDockControlBottom);
            this.barManager_ContasMP.DockControls.Add(this.barDockControlLeft);
            this.barManager_ContasMP.DockControls.Add(this.barDockControlRight);
            this.barManager_ContasMP.Form = this;
            this.barManager_ContasMP.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem_Add,
            this.barButtonItem_Edit,
            this.barButtonItem_Del,
            this.barButtonItem_Gravar,
            this.barButtonItem_Cancelar});
            this.barManager_ContasMP.MaxItemId = 5;
            // 
            // bar_Acoes
            // 
            this.bar_Acoes.BarName = "Tools";
            this.bar_Acoes.DockCol = 0;
            this.bar_Acoes.DockRow = 0;
            this.bar_Acoes.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar_Acoes.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Add),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Edit),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Del)});
            this.bar_Acoes.OptionsBar.UseWholeRow = true;
            this.bar_Acoes.Text = "Tools";
            // 
            // barButtonItem_Add
            // 
            this.barButtonItem_Add.Caption = "Adicionar";
            this.barButtonItem_Add.Id = 0;
            this.barButtonItem_Add.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Add.ImageOptions.Image")));
            this.barButtonItem_Add.Name = "barButtonItem_Add";
            this.barButtonItem_Add.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Add.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Add_ItemClick);
            // 
            // barButtonItem_Edit
            // 
            this.barButtonItem_Edit.Caption = "Editar";
            this.barButtonItem_Edit.Id = 1;
            this.barButtonItem_Edit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Edit.ImageOptions.Image")));
            this.barButtonItem_Edit.Name = "barButtonItem_Edit";
            this.barButtonItem_Edit.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Edit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Edit_ItemClick);
            // 
            // barButtonItem_Del
            // 
            this.barButtonItem_Del.Caption = "Excluir";
            this.barButtonItem_Del.Id = 2;
            this.barButtonItem_Del.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Del.ImageOptions.Image")));
            this.barButtonItem_Del.Name = "barButtonItem_Del";
            this.barButtonItem_Del.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Del.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Del_ItemClick);
            // 
            // bar_Acoes2
            // 
            this.bar_Acoes2.BarName = "Tools_GravarCancelar";
            this.bar_Acoes2.DockCol = 0;
            this.bar_Acoes2.DockRow = 1;
            this.bar_Acoes2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar_Acoes2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Gravar),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Cancelar)});
            this.bar_Acoes2.OptionsBar.AllowRename = true;
            this.bar_Acoes2.OptionsBar.UseWholeRow = true;
            this.bar_Acoes2.Text = "Tools_GravarCancelar";
            // 
            // barButtonItem_Gravar
            // 
            this.barButtonItem_Gravar.Caption = "Gravar";
            this.barButtonItem_Gravar.Id = 3;
            this.barButtonItem_Gravar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Gravar.ImageOptions.Image")));
            this.barButtonItem_Gravar.Name = "barButtonItem_Gravar";
            this.barButtonItem_Gravar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Gravar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Gravar_ItemClick);
            // 
            // barButtonItem_Cancelar
            // 
            this.barButtonItem_Cancelar.Caption = "Cancelar";
            this.barButtonItem_Cancelar.Id = 4;
            this.barButtonItem_Cancelar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Cancelar.ImageOptions.Image")));
            this.barButtonItem_Cancelar.Name = "barButtonItem_Cancelar";
            this.barButtonItem_Cancelar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Cancelar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Cancelar_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager_ContasMP;
            this.barDockControlTop.Size = new System.Drawing.Size(1264, 94);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 562);
            this.barDockControlBottom.Manager = this.barManager_ContasMP;
            this.barDockControlBottom.Size = new System.Drawing.Size(1264, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 94);
            this.barDockControlLeft.Manager = this.barManager_ContasMP;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 468);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1264, 94);
            this.barDockControlRight.Manager = this.barManager_ContasMP;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 468);
            // 
            // dateEdit_ICMSSTUltAlteracao
            // 
            this.dateEdit_ICMSSTUltAlteracao.EditValue = null;
            this.dateEdit_ICMSSTUltAlteracao.Enabled = false;
            this.dateEdit_ICMSSTUltAlteracao.Location = new System.Drawing.Point(136, 267);
            this.dateEdit_ICMSSTUltAlteracao.MenuManager = this.barManager_ContasMP;
            this.dateEdit_ICMSSTUltAlteracao.Name = "dateEdit_ICMSSTUltAlteracao";
            this.dateEdit_ICMSSTUltAlteracao.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit_ICMSSTUltAlteracao.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit_ICMSSTUltAlteracao.Size = new System.Drawing.Size(1072, 20);
            this.dateEdit_ICMSSTUltAlteracao.StyleController = this.layoutControl_ICMSST;
            this.dateEdit_ICMSSTUltAlteracao.TabIndex = 7;
            // 
            // textEdit_ICMSSTExcluido
            // 
            this.textEdit_ICMSSTExcluido.Location = new System.Drawing.Point(136, 291);
            this.textEdit_ICMSSTExcluido.Name = "textEdit_ICMSSTExcluido";
            this.textEdit_ICMSSTExcluido.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.textEdit_ICMSSTExcluido.Properties.OffText = "Não";
            this.textEdit_ICMSSTExcluido.Properties.OnText = "Sim";
            this.textEdit_ICMSSTExcluido.Size = new System.Drawing.Size(1072, 24);
            this.textEdit_ICMSSTExcluido.StyleController = this.layoutControl_ICMSST;
            this.textEdit_ICMSSTExcluido.TabIndex = 10;
            // 
            // textEdit_ICMSSTValMVA
            // 
            this.textEdit_ICMSSTValMVA.Location = new System.Drawing.Point(136, 199);
            this.textEdit_ICMSSTValMVA.MenuManager = this.barManager_ContasMP;
            this.textEdit_ICMSSTValMVA.Name = "textEdit_ICMSSTValMVA";
            this.textEdit_ICMSSTValMVA.Properties.Mask.EditMask = "P";
            this.textEdit_ICMSSTValMVA.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEdit_ICMSSTValMVA.Size = new System.Drawing.Size(482, 20);
            this.textEdit_ICMSSTValMVA.StyleController = this.layoutControl_ICMSST;
            this.textEdit_ICMSSTValMVA.TabIndex = 12;
            // 
            // textEdit__ICMSSTNomeExcFiscal
            // 
            this.textEdit__ICMSSTNomeExcFiscal.Location = new System.Drawing.Point(136, 141);
            this.textEdit__ICMSSTNomeExcFiscal.MenuManager = this.barManager_ContasMP;
            this.textEdit__ICMSSTNomeExcFiscal.Name = "textEdit__ICMSSTNomeExcFiscal";
            this.textEdit__ICMSSTNomeExcFiscal.Properties.MaxLength = 20;
            this.textEdit__ICMSSTNomeExcFiscal.Size = new System.Drawing.Size(1072, 20);
            this.textEdit__ICMSSTNomeExcFiscal.StyleController = this.layoutControl_ICMSST;
            this.textEdit__ICMSSTNomeExcFiscal.TabIndex = 15;
            // 
            // textEdit_ICMSSTValPMC
            // 
            this.textEdit_ICMSSTValPMC.Location = new System.Drawing.Point(724, 199);
            this.textEdit_ICMSSTValPMC.Name = "textEdit_ICMSSTValPMC";
            this.textEdit_ICMSSTValPMC.Properties.Mask.EditMask = "P";
            this.textEdit_ICMSSTValPMC.Size = new System.Drawing.Size(484, 20);
            this.textEdit_ICMSSTValPMC.StyleController = this.layoutControl_ICMSST;
            this.textEdit_ICMSSTValPMC.TabIndex = 12;
            // 
            // textEdit_ICMSSTCredito
            // 
            this.textEdit_ICMSSTCredito.Location = new System.Drawing.Point(136, 223);
            this.textEdit_ICMSSTCredito.Name = "textEdit_ICMSSTCredito";
            this.textEdit_ICMSSTCredito.Properties.Mask.EditMask = "P";
            this.textEdit_ICMSSTCredito.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEdit_ICMSSTCredito.Size = new System.Drawing.Size(482, 20);
            this.textEdit_ICMSSTCredito.StyleController = this.layoutControl_ICMSST;
            this.textEdit_ICMSSTCredito.TabIndex = 12;
            // 
            // textEdit_ICMSSTDestino
            // 
            this.textEdit_ICMSSTDestino.Location = new System.Drawing.Point(724, 223);
            this.textEdit_ICMSSTDestino.Name = "textEdit_ICMSSTDestino";
            this.textEdit_ICMSSTDestino.Properties.Mask.EditMask = "P";
            this.textEdit_ICMSSTDestino.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEdit_ICMSSTDestino.Size = new System.Drawing.Size(484, 20);
            this.textEdit_ICMSSTDestino.StyleController = this.layoutControl_ICMSST;
            this.textEdit_ICMSSTDestino.TabIndex = 12;
            // 
            // comboBoxEdit_ICMSSTEstDestino
            // 
            this.comboBoxEdit_ICMSSTEstDestino.Location = new System.Drawing.Point(136, 175);
            this.comboBoxEdit_ICMSSTEstDestino.MenuManager = this.barManager_ContasMP;
            this.comboBoxEdit_ICMSSTEstDestino.Name = "comboBoxEdit_ICMSSTEstDestino";
            this.comboBoxEdit_ICMSSTEstDestino.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit_ICMSSTEstDestino.Properties.NullText = "";
            this.comboBoxEdit_ICMSSTEstDestino.Properties.PopupSizeable = false;
            this.comboBoxEdit_ICMSSTEstDestino.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.comboBoxEdit_ICMSSTEstDestino.Size = new System.Drawing.Size(482, 20);
            this.comboBoxEdit_ICMSSTEstDestino.StyleController = this.layoutControl_ICMSST;
            this.comboBoxEdit_ICMSSTEstDestino.TabIndex = 16;
            // 
            // comboBoxEdit_ICMSSTTipoST
            // 
            this.comboBoxEdit_ICMSSTTipoST.Location = new System.Drawing.Point(724, 175);
            this.comboBoxEdit_ICMSSTTipoST.MenuManager = this.barManager_ContasMP;
            this.comboBoxEdit_ICMSSTTipoST.Name = "comboBoxEdit_ICMSSTTipoST";
            this.comboBoxEdit_ICMSSTTipoST.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit_ICMSSTTipoST.Properties.NullText = "";
            this.comboBoxEdit_ICMSSTTipoST.Properties.PopupSizeable = false;
            this.comboBoxEdit_ICMSSTTipoST.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.comboBoxEdit_ICMSSTTipoST.Size = new System.Drawing.Size(484, 20);
            this.comboBoxEdit_ICMSSTTipoST.StyleController = this.layoutControl_ICMSST;
            this.comboBoxEdit_ICMSSTTipoST.TabIndex = 17;
            // 
            // layoutControlGroup_RegisterICMSST
            // 
            this.layoutControlGroup_RegisterICMSST.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup_RegisterICMSST.GroupBordersVisible = false;
            this.layoutControlGroup_RegisterICMSST.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.emptySpaceItem4,
            this.simpleLabelItem_RegisterICMSST,
            this.emptySpaceItem5,
            this.emptySpaceItem9,
            this.layoutControlGroup_ICMSST,
            this.emptySpaceItem3});
            this.layoutControlGroup_RegisterICMSST.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup_RegisterICMSST.Name = "layoutControlGroup_RegisterICMSST";
            this.layoutControlGroup_RegisterICMSST.Size = new System.Drawing.Size(1242, 400);
            this.layoutControlGroup_RegisterICMSST.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(10, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(1212, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(10, 380);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleLabelItem_RegisterICMSST
            // 
            this.simpleLabelItem_RegisterICMSST.AllowHotTrack = false;
            this.simpleLabelItem_RegisterICMSST.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem_RegisterICMSST.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem_RegisterICMSST.Location = new System.Drawing.Point(10, 10);
            this.simpleLabelItem_RegisterICMSST.Name = "simpleLabelItem_RegisterICMSST";
            this.simpleLabelItem_RegisterICMSST.OptionsPrint.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem_RegisterICMSST.OptionsPrint.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem_RegisterICMSST.Size = new System.Drawing.Size(1202, 17);
            this.simpleLabelItem_RegisterICMSST.Text = "Cadastro ICMS-ST";
            this.simpleLabelItem_RegisterICMSST.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(10, 27);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.Location = new System.Drawing.Point(10, 353);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(1202, 27);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup_ICMSST
            // 
            this.layoutControlGroup_ICMSST.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem10,
            this.layoutControlItem_ICMSSTCod,
            this.layoutControlItem_ICMSSTDtUltAlteracao,
            this.layoutControlItem_ICMSSTExcluido,
            this.emptySpaceItem18,
            this.layoutControlItem_ICMSSTNomeExcFiscal,
            this.layoutControlItem_ICMSSTEstDestino,
            this.layoutControlItem_ICMSSTValMVA,
            this.layoutControlItem_ICMSSTCredito,
            this.layoutControlItem_ICMSSTTipoSt,
            this._ICMSSTValPMC,
            this.layoutControlItem__ICMSSTDestino,
            this.emptySpaceItem7,
            this.emptySpaceItem20});
            this.layoutControlGroup_ICMSST.Location = new System.Drawing.Point(10, 37);
            this.layoutControlGroup_ICMSST.Name = "layoutControlGroup_ICMSST";
            this.layoutControlGroup_ICMSST.Size = new System.Drawing.Size(1202, 316);
            this.layoutControlGroup_ICMSST.Text = "ICMS-ST";
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(1178, 38);
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem_ICMSSTCod
            // 
            this.layoutControlItem_ICMSSTCod.Control = this.textEdit_ICMSSTCod;
            this.layoutControlItem_ICMSSTCod.Location = new System.Drawing.Point(0, 38);
            this.layoutControlItem_ICMSSTCod.Name = "layoutControlItem_ICMSSTCod";
            this.layoutControlItem_ICMSSTCod.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem_ICMSSTCod.Text = "Código NCM";
            this.layoutControlItem_ICMSSTCod.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem_ICMSSTDtUltAlteracao
            // 
            this.layoutControlItem_ICMSSTDtUltAlteracao.Control = this.dateEdit_ICMSSTUltAlteracao;
            this.layoutControlItem_ICMSSTDtUltAlteracao.Location = new System.Drawing.Point(0, 188);
            this.layoutControlItem_ICMSSTDtUltAlteracao.Name = "layoutControlItem_ICMSSTDtUltAlteracao";
            this.layoutControlItem_ICMSSTDtUltAlteracao.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem_ICMSSTDtUltAlteracao.Text = "Última Alteração";
            this.layoutControlItem_ICMSSTDtUltAlteracao.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem_ICMSSTExcluido
            // 
            this.layoutControlItem_ICMSSTExcluido.Control = this.textEdit_ICMSSTExcluido;
            this.layoutControlItem_ICMSSTExcluido.CustomizationFormText = "Status";
            this.layoutControlItem_ICMSSTExcluido.Location = new System.Drawing.Point(0, 212);
            this.layoutControlItem_ICMSSTExcluido.Name = "layoutControlItem_ICMSSTExcluido";
            this.layoutControlItem_ICMSSTExcluido.Size = new System.Drawing.Size(1178, 28);
            this.layoutControlItem_ICMSSTExcluido.Text = "Excluído";
            this.layoutControlItem_ICMSSTExcluido.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem18
            // 
            this.emptySpaceItem18.AllowHotTrack = false;
            this.emptySpaceItem18.Location = new System.Drawing.Point(0, 168);
            this.emptySpaceItem18.Name = "emptySpaceItem18";
            this.emptySpaceItem18.Size = new System.Drawing.Size(1178, 20);
            this.emptySpaceItem18.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem_ICMSSTNomeExcFiscal
            // 
            this.layoutControlItem_ICMSSTNomeExcFiscal.Control = this.textEdit__ICMSSTNomeExcFiscal;
            this.layoutControlItem_ICMSSTNomeExcFiscal.Location = new System.Drawing.Point(0, 62);
            this.layoutControlItem_ICMSSTNomeExcFiscal.Name = "layoutControlItem_ICMSSTNomeExcFiscal";
            this.layoutControlItem_ICMSSTNomeExcFiscal.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem_ICMSSTNomeExcFiscal.Text = "Nome Exceção Fiscal";
            this.layoutControlItem_ICMSSTNomeExcFiscal.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem_ICMSSTEstDestino
            // 
            this.layoutControlItem_ICMSSTEstDestino.Control = this.comboBoxEdit_ICMSSTEstDestino;
            this.layoutControlItem_ICMSSTEstDestino.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem_ICMSSTEstDestino.Name = "layoutControlItem_ICMSSTEstDestino";
            this.layoutControlItem_ICMSSTEstDestino.Size = new System.Drawing.Size(588, 24);
            this.layoutControlItem_ICMSSTEstDestino.Text = "Estado Destino";
            this.layoutControlItem_ICMSSTEstDestino.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem_ICMSSTValMVA
            // 
            this.layoutControlItem_ICMSSTValMVA.Control = this.textEdit_ICMSSTValMVA;
            this.layoutControlItem_ICMSSTValMVA.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem_ICMSSTValMVA.Name = "layoutControlItem_ICMSSTValMVA";
            this.layoutControlItem_ICMSSTValMVA.Size = new System.Drawing.Size(588, 24);
            this.layoutControlItem_ICMSSTValMVA.Text = "Valor MVA";
            this.layoutControlItem_ICMSSTValMVA.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem_ICMSSTCredito
            // 
            this.layoutControlItem_ICMSSTCredito.Control = this.textEdit_ICMSSTCredito;
            this.layoutControlItem_ICMSSTCredito.CustomizationFormText = "ICMS Crédito";
            this.layoutControlItem_ICMSSTCredito.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItem_ICMSSTCredito.Name = "layoutControlItem_ICMSSTCredito";
            this.layoutControlItem_ICMSSTCredito.Size = new System.Drawing.Size(588, 24);
            this.layoutControlItem_ICMSSTCredito.Text = "ICMS Crédito";
            this.layoutControlItem_ICMSSTCredito.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem_ICMSSTTipoSt
            // 
            this.layoutControlItem_ICMSSTTipoSt.Control = this.comboBoxEdit_ICMSSTTipoST;
            this.layoutControlItem_ICMSSTTipoSt.Location = new System.Drawing.Point(588, 96);
            this.layoutControlItem_ICMSSTTipoSt.Name = "layoutControlItem_ICMSSTTipoSt";
            this.layoutControlItem_ICMSSTTipoSt.Size = new System.Drawing.Size(590, 24);
            this.layoutControlItem_ICMSSTTipoSt.Text = "Tipo ST";
            this.layoutControlItem_ICMSSTTipoSt.TextSize = new System.Drawing.Size(99, 13);
            // 
            // _ICMSSTValPMC
            // 
            this._ICMSSTValPMC.Control = this.textEdit_ICMSSTValPMC;
            this._ICMSSTValPMC.CustomizationFormText = "Valor PMC";
            this._ICMSSTValPMC.Location = new System.Drawing.Point(588, 120);
            this._ICMSSTValPMC.Name = "_ICMSSTValPMC";
            this._ICMSSTValPMC.Size = new System.Drawing.Size(590, 24);
            this._ICMSSTValPMC.Text = "Valor PMC";
            this._ICMSSTValPMC.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem__ICMSSTDestino
            // 
            this.layoutControlItem__ICMSSTDestino.Control = this.textEdit_ICMSSTDestino;
            this.layoutControlItem__ICMSSTDestino.CustomizationFormText = "ICMS Destino";
            this.layoutControlItem__ICMSSTDestino.Location = new System.Drawing.Point(588, 144);
            this.layoutControlItem__ICMSSTDestino.Name = "layoutControlItem__ICMSSTDestino";
            this.layoutControlItem__ICMSSTDestino.Size = new System.Drawing.Size(590, 24);
            this.layoutControlItem__ICMSSTDestino.Text = "ICMS Destino";
            this.layoutControlItem__ICMSSTDestino.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 86);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(1178, 10);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem20
            // 
            this.emptySpaceItem20.AllowHotTrack = false;
            this.emptySpaceItem20.Location = new System.Drawing.Point(0, 240);
            this.emptySpaceItem20.Name = "emptySpaceItem20";
            this.emptySpaceItem20.Size = new System.Drawing.Size(1178, 34);
            this.emptySpaceItem20.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(10, 380);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // meusPedidosDataSet
            // 
            this.meusPedidosDataSet.DataSetName = "MeusPedidosDataSet";
            this.meusPedidosDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // simpleLabelItem_RegisterClientes
            // 
            this.simpleLabelItem_RegisterClientes.AllowHotTrack = false;
            this.simpleLabelItem_RegisterClientes.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem_RegisterClientes.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem_RegisterClientes.Location = new System.Drawing.Point(10, 10);
            this.simpleLabelItem_RegisterClientes.Name = "simpleLabelItem_RegisterClientes";
            this.simpleLabelItem_RegisterClientes.OptionsPrint.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem_RegisterClientes.OptionsPrint.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem_RegisterClientes.Size = new System.Drawing.Size(1185, 17);
            this.simpleLabelItem_RegisterClientes.Text = "Cadastro Clientes";
            this.simpleLabelItem_RegisterClientes.TextSize = new System.Drawing.Size(155, 13);
            // 
            // tabNavigationPage_DataGridICMSST
            // 
            this.tabNavigationPage_DataGridICMSST.Caption = "ICMS-ST";
            this.tabNavigationPage_DataGridICMSST.Controls.Add(this.gridControl_ICMSST);
            this.tabNavigationPage_DataGridICMSST.Image = ((System.Drawing.Image)(resources.GetObject("tabNavigationPage_DataGridICMSST.Image")));
            this.tabNavigationPage_DataGridICMSST.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage_DataGridICMSST.Name = "tabNavigationPage_DataGridICMSST";
            this.tabNavigationPage_DataGridICMSST.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage_DataGridICMSST.Size = new System.Drawing.Size(1246, 404);
            // 
            // gridControl_ICMSST
            // 
            this.gridControl_ICMSST.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl_ICMSST.Location = new System.Drawing.Point(0, 0);
            this.gridControl_ICMSST.MainView = this.gridView_ICMSST;
            this.gridControl_ICMSST.MenuManager = this.barManager_ContasMP;
            this.gridControl_ICMSST.Name = "gridControl_ICMSST";
            this.gridControl_ICMSST.Size = new System.Drawing.Size(1246, 404);
            this.gridControl_ICMSST.TabIndex = 0;
            this.gridControl_ICMSST.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView_ICMSST});
            // 
            // iCMSSTBindingSource
            // 
            this.iCMSSTBindingSource.DataMember = "ICMS_ST";
            this.iCMSSTBindingSource.DataSource = this.meusPedidosDataSet;
            // 
            // gridView_ICMSST
            // 
            this.gridView_ICMSST.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colid,
            this.colcodigo_ncm,
            this.colnome_excecao_fiscal,
            this.colestado_destino,
            this.coltipo_st,
            this.colvalor_mva,
            this.colvalor_pmc,
            this.colicms_credito,
            this.colicms_destino,
            this.colexcluido,
            this.colultima_alteracao,
            this.colmptran});
            this.gridView_ICMSST.GridControl = this.gridControl_ICMSST;
            this.gridView_ICMSST.Name = "gridView_ICMSST";
            this.gridView_ICMSST.OptionsBehavior.Editable = false;
            this.gridView_ICMSST.OptionsBehavior.ReadOnly = true;
            this.gridView_ICMSST.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView_ICMSST.OptionsSelection.MultiSelect = true;
            this.gridView_ICMSST.OptionsSelection.UseIndicatorForSelection = false;
            this.gridView_ICMSST.OptionsView.ShowGroupPanel = false;
            this.gridView_ICMSST.OptionsView.ShowIndicator = false;
            this.gridView_ICMSST.DoubleClick += new System.EventHandler(this.gridView_Clientes_DoubleClick);
            // 
            // colid
            // 
            this.colid.Caption = "ID";
            this.colid.FieldName = "id";
            this.colid.Name = "colid";
            this.colid.Visible = true;
            this.colid.VisibleIndex = 0;
            // 
            // colcodigo_ncm
            // 
            this.colcodigo_ncm.Caption = "Código NCM";
            this.colcodigo_ncm.FieldName = "codigo_ncm";
            this.colcodigo_ncm.Name = "colcodigo_ncm";
            this.colcodigo_ncm.Visible = true;
            this.colcodigo_ncm.VisibleIndex = 1;
            // 
            // colnome_excecao_fiscal
            // 
            this.colnome_excecao_fiscal.Caption = "Exceção Fiscal";
            this.colnome_excecao_fiscal.FieldName = "nome_excecao_fiscal";
            this.colnome_excecao_fiscal.Name = "colnome_excecao_fiscal";
            this.colnome_excecao_fiscal.Visible = true;
            this.colnome_excecao_fiscal.VisibleIndex = 2;
            // 
            // colestado_destino
            // 
            this.colestado_destino.Caption = "UF Destino";
            this.colestado_destino.FieldName = "estado_destino";
            this.colestado_destino.Name = "colestado_destino";
            this.colestado_destino.Visible = true;
            this.colestado_destino.VisibleIndex = 3;
            // 
            // coltipo_st
            // 
            this.coltipo_st.Caption = "Tipo ST";
            this.coltipo_st.FieldName = "tipo_st";
            this.coltipo_st.Name = "coltipo_st";
            this.coltipo_st.Visible = true;
            this.coltipo_st.VisibleIndex = 4;
            // 
            // colvalor_mva
            // 
            this.colvalor_mva.Caption = "Valor MVA";
            this.colvalor_mva.FieldName = "valor_mva";
            this.colvalor_mva.Name = "colvalor_mva";
            this.colvalor_mva.Visible = true;
            this.colvalor_mva.VisibleIndex = 5;
            // 
            // colvalor_pmc
            // 
            this.colvalor_pmc.Caption = "Valor PMC";
            this.colvalor_pmc.FieldName = "valor_pmc";
            this.colvalor_pmc.Name = "colvalor_pmc";
            this.colvalor_pmc.Visible = true;
            this.colvalor_pmc.VisibleIndex = 6;
            // 
            // colicms_credito
            // 
            this.colicms_credito.Caption = "ICMS Crédito";
            this.colicms_credito.FieldName = "icms_credito";
            this.colicms_credito.Name = "colicms_credito";
            this.colicms_credito.Visible = true;
            this.colicms_credito.VisibleIndex = 7;
            // 
            // colicms_destino
            // 
            this.colicms_destino.Caption = "ICMS Destino";
            this.colicms_destino.FieldName = "icms_destino";
            this.colicms_destino.Name = "colicms_destino";
            this.colicms_destino.Visible = true;
            this.colicms_destino.VisibleIndex = 8;
            // 
            // colexcluido
            // 
            this.colexcluido.Caption = "Excluído";
            this.colexcluido.FieldName = "excluido";
            this.colexcluido.Name = "colexcluido";
            this.colexcluido.Visible = true;
            this.colexcluido.VisibleIndex = 9;
            // 
            // colultima_alteracao
            // 
            this.colultima_alteracao.Caption = "Última Alteração";
            this.colultima_alteracao.FieldName = "ultima_alteracao";
            this.colultima_alteracao.Name = "colultima_alteracao";
            this.colultima_alteracao.Visible = true;
            this.colultima_alteracao.VisibleIndex = 10;
            // 
            // colmptran
            // 
            this.colmptran.Caption = "MP Tran";
            this.colmptran.FieldName = "mptran";
            this.colmptran.Name = "colmptran";
            this.colmptran.Visible = true;
            this.colmptran.VisibleIndex = 11;
            // 
            // tabPane_ConfigICMSST
            // 
            this.tabPane_ConfigICMSST.Controls.Add(this.tabNavigationPage_DataGridICMSST);
            this.tabPane_ConfigICMSST.Controls.Add(this.tabNavigationPage_RegisterICMSST);
            this.tabPane_ConfigICMSST.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPane_ConfigICMSST.Location = new System.Drawing.Point(0, 94);
            this.tabPane_ConfigICMSST.Name = "tabPane_ConfigICMSST";
            this.tabPane_ConfigICMSST.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.tabNavigationPage_DataGridICMSST,
            this.tabNavigationPage_RegisterICMSST});
            this.tabPane_ConfigICMSST.RegularSize = new System.Drawing.Size(1264, 468);
            this.tabPane_ConfigICMSST.SelectedPage = this.tabNavigationPage_RegisterICMSST;
            this.tabPane_ConfigICMSST.Size = new System.Drawing.Size(1264, 468);
            this.tabPane_ConfigICMSST.TabIndex = 8;
            this.tabPane_ConfigICMSST.Text = "ICMS-ST";
            // 
            // tabPane1
            // 
            this.tabPane1.Controls.Add(this.tabNavigationPage1);
            this.tabPane1.Controls.Add(this.tabNavigationPage2);
            this.tabPane1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPane1.Location = new System.Drawing.Point(0, 94);
            this.tabPane1.Name = "tabPane1";
            this.tabPane1.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.tabNavigationPage1,
            this.tabNavigationPage2});
            this.tabPane1.RegularSize = new System.Drawing.Size(1264, 445);
            this.tabPane1.SelectedPage = this.tabNavigationPage2;
            this.tabPane1.Size = new System.Drawing.Size(1264, 445);
            this.tabPane1.TabIndex = 8;
            this.tabPane1.Text = "Clientes";
            // 
            // tabNavigationPage1
            // 
            this.tabNavigationPage1.Caption = "Clientes";
            this.tabNavigationPage1.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage1.Name = "tabNavigationPage1";
            this.tabNavigationPage1.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage1.Size = new System.Drawing.Size(1246, 381);
            // 
            // tabNavigationPage2
            // 
            this.tabNavigationPage2.Caption = "Cadastro";
            this.tabNavigationPage2.Controls.Add(this.panelControl1);
            this.tabNavigationPage2.Image = ((System.Drawing.Image)(resources.GetObject("tabNavigationPage2.Image")));
            this.tabNavigationPage2.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage2.Name = "tabNavigationPage2";
            this.tabNavigationPage2.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage2.Size = new System.Drawing.Size(1246, 381);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.layoutControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1246, 381);
            this.panelControl1.TabIndex = 0;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.textEdit2);
            this.layoutControl1.Controls.Add(this.textEdit3);
            this.layoutControl1.Controls.Add(this.textEdit4);
            this.layoutControl1.Controls.Add(this.textEdit5);
            this.layoutControl1.Controls.Add(this.textEdit6);
            this.layoutControl1.Controls.Add(this.textEdit7);
            this.layoutControl1.Controls.Add(this.textEdit8);
            this.layoutControl1.Controls.Add(this.textEdit9);
            this.layoutControl1.Controls.Add(this.textEdit10);
            this.layoutControl1.Controls.Add(this.textEdit11);
            this.layoutControl1.Controls.Add(this.textEdit12);
            this.layoutControl1.Controls.Add(this.textEdit13);
            this.layoutControl1.Controls.Add(this.textEdit14);
            this.layoutControl1.Controls.Add(this.dateEdit1);
            this.layoutControl1.Controls.Add(this.toggleSwitch1);
            this.layoutControl1.Controls.Add(this.memoEdit1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup3;
            this.layoutControl1.Size = new System.Drawing.Size(1242, 377);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textEdit2
            // 
            this.textEdit2.Location = new System.Drawing.Point(136, 83);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Size = new System.Drawing.Size(1072, 20);
            this.textEdit2.StyleController = this.layoutControl1;
            this.textEdit2.TabIndex = 4;
            this.textEdit2.Visible = false;
            // 
            // textEdit3
            // 
            this.textEdit3.Location = new System.Drawing.Point(136, 107);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Size = new System.Drawing.Size(1072, 20);
            this.textEdit3.StyleController = this.layoutControl1;
            this.textEdit3.TabIndex = 5;
            this.textEdit3.Visible = false;
            // 
            // textEdit4
            // 
            this.textEdit4.Location = new System.Drawing.Point(136, 131);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Properties.MaxLength = 1;
            this.textEdit4.Size = new System.Drawing.Size(1072, 20);
            this.textEdit4.StyleController = this.layoutControl1;
            this.textEdit4.TabIndex = 6;
            this.textEdit4.Visible = false;
            // 
            // textEdit5
            // 
            this.textEdit5.Location = new System.Drawing.Point(136, 155);
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Size = new System.Drawing.Size(1072, 20);
            this.textEdit5.StyleController = this.layoutControl1;
            this.textEdit5.TabIndex = 7;
            this.textEdit5.Visible = false;
            // 
            // textEdit6
            // 
            this.textEdit6.Location = new System.Drawing.Point(136, 179);
            this.textEdit6.Name = "textEdit6";
            this.textEdit6.Size = new System.Drawing.Size(1072, 20);
            this.textEdit6.StyleController = this.layoutControl1;
            this.textEdit6.TabIndex = 8;
            this.textEdit6.Visible = false;
            // 
            // textEdit7
            // 
            this.textEdit7.Location = new System.Drawing.Point(136, 203);
            this.textEdit7.Name = "textEdit7";
            this.textEdit7.Size = new System.Drawing.Size(1072, 20);
            this.textEdit7.StyleController = this.layoutControl1;
            this.textEdit7.TabIndex = 9;
            this.textEdit7.Visible = false;
            // 
            // textEdit8
            // 
            this.textEdit8.Location = new System.Drawing.Point(136, 303);
            this.textEdit8.Name = "textEdit8";
            this.textEdit8.Size = new System.Drawing.Size(1072, 20);
            this.textEdit8.StyleController = this.layoutControl1;
            this.textEdit8.TabIndex = 10;
            // 
            // textEdit9
            // 
            this.textEdit9.Location = new System.Drawing.Point(136, 327);
            this.textEdit9.Name = "textEdit9";
            this.textEdit9.Size = new System.Drawing.Size(1072, 20);
            this.textEdit9.StyleController = this.layoutControl1;
            this.textEdit9.TabIndex = 11;
            // 
            // textEdit10
            // 
            this.textEdit10.Location = new System.Drawing.Point(136, 351);
            this.textEdit10.Name = "textEdit10";
            this.textEdit10.Size = new System.Drawing.Size(1072, 20);
            this.textEdit10.StyleController = this.layoutControl1;
            this.textEdit10.TabIndex = 12;
            // 
            // textEdit11
            // 
            this.textEdit11.Location = new System.Drawing.Point(136, 375);
            this.textEdit11.Name = "textEdit11";
            this.textEdit11.Size = new System.Drawing.Size(1072, 20);
            this.textEdit11.StyleController = this.layoutControl1;
            this.textEdit11.TabIndex = 13;
            // 
            // textEdit12
            // 
            this.textEdit12.Location = new System.Drawing.Point(136, 399);
            this.textEdit12.Name = "textEdit12";
            this.textEdit12.Size = new System.Drawing.Size(1072, 20);
            this.textEdit12.StyleController = this.layoutControl1;
            this.textEdit12.TabIndex = 14;
            // 
            // textEdit13
            // 
            this.textEdit13.Location = new System.Drawing.Point(136, 423);
            this.textEdit13.Name = "textEdit13";
            this.textEdit13.Size = new System.Drawing.Size(1072, 20);
            this.textEdit13.StyleController = this.layoutControl1;
            this.textEdit13.TabIndex = 15;
            // 
            // textEdit14
            // 
            this.textEdit14.Location = new System.Drawing.Point(136, 227);
            this.textEdit14.Name = "textEdit14";
            this.textEdit14.Size = new System.Drawing.Size(1072, 20);
            this.textEdit14.StyleController = this.layoutControl1;
            this.textEdit14.TabIndex = 16;
            this.textEdit14.Visible = false;
            // 
            // dateEdit1
            // 
            this.dateEdit1.EditValue = null;
            this.dateEdit1.Location = new System.Drawing.Point(980, 499);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.ReadOnly = true;
            this.dateEdit1.Size = new System.Drawing.Size(228, 20);
            this.dateEdit1.StyleController = this.layoutControl1;
            this.dateEdit1.TabIndex = 17;
            // 
            // toggleSwitch1
            // 
            this.toggleSwitch1.Location = new System.Drawing.Point(980, 523);
            this.toggleSwitch1.Name = "toggleSwitch1";
            this.toggleSwitch1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.toggleSwitch1.Properties.OffText = "Não";
            this.toggleSwitch1.Properties.OnText = "Sim";
            this.toggleSwitch1.Size = new System.Drawing.Size(228, 24);
            this.toggleSwitch1.StyleController = this.layoutControl1;
            this.toggleSwitch1.TabIndex = 10;
            // 
            // memoEdit1
            // 
            this.memoEdit1.Location = new System.Drawing.Point(136, 499);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(728, 48);
            this.memoEdit1.StyleController = this.layoutControl1;
            this.memoEdit1.TabIndex = 18;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem6,
            this.emptySpaceItem8,
            this.emptySpaceItem11,
            this.emptySpaceItem12,
            this.simpleLabelItem1,
            this.emptySpaceItem13,
            this.emptySpaceItem14,
            this.layoutControlGroup4,
            this.emptySpaceItem15,
            this.layoutControlGroup5,
            this.tabbedControlGroup3});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "Root";
            this.layoutControlGroup3.Size = new System.Drawing.Size(1242, 581);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(10, 0);
            this.emptySpaceItem6.Name = "emptySpaceItem1";
            this.emptySpaceItem6.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.Location = new System.Drawing.Point(10, 251);
            this.emptySpaceItem8.Name = "emptySpaceItem2";
            this.emptySpaceItem8.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem11.Name = "emptySpaceItem3";
            this.emptySpaceItem11.Size = new System.Drawing.Size(10, 561);
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.Location = new System.Drawing.Point(1212, 0);
            this.emptySpaceItem12.Name = "emptySpaceItem4";
            this.emptySpaceItem12.Size = new System.Drawing.Size(10, 561);
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleLabelItem1
            // 
            this.simpleLabelItem1.AllowHotTrack = false;
            this.simpleLabelItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem1.Location = new System.Drawing.Point(10, 10);
            this.simpleLabelItem1.Name = "simpleLabelItem_RegisterClientes";
            this.simpleLabelItem1.OptionsPrint.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem1.OptionsPrint.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem1.Size = new System.Drawing.Size(1202, 17);
            this.simpleLabelItem1.Text = "Cadastro Clientes";
            this.simpleLabelItem1.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.Location = new System.Drawing.Point(10, 27);
            this.emptySpaceItem13.Name = "emptySpaceItem5";
            this.emptySpaceItem13.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.Location = new System.Drawing.Point(10, 447);
            this.emptySpaceItem14.Name = "emptySpaceItem7";
            this.emptySpaceItem14.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9});
            this.layoutControlGroup4.Location = new System.Drawing.Point(10, 261);
            this.layoutControlGroup4.Name = "layoutControlGroup_Endereço";
            this.layoutControlGroup4.Size = new System.Drawing.Size(1202, 186);
            this.layoutControlGroup4.Text = "Endereço";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.textEdit8;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem_CLIRua";
            this.layoutControlItem4.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem4.Text = "Rua";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.textEdit9;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem5.Name = "layoutControlItem_CLIComplemento";
            this.layoutControlItem5.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem5.Text = "Complemento";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.textEdit10;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem6.Name = "layoutControlItem_CLICep";
            this.layoutControlItem6.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem6.Text = "CEP";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.textEdit11;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem7.Name = "layoutControlItem_Bairro";
            this.layoutControlItem7.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem7.Text = "Bairro";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.textEdit12;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem8.Name = "layoutControlItem_Cidade";
            this.layoutControlItem8.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem8.Text = "Cidade";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.textEdit13;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem9.Name = "layoutControlItem_CLIEstado";
            this.layoutControlItem9.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem9.Text = "Estado";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.Location = new System.Drawing.Point(10, 551);
            this.emptySpaceItem15.Name = "emptySpaceItem9";
            this.emptySpaceItem15.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.emptySpaceItem16,
            this.layoutControlItem12});
            this.layoutControlGroup5.Location = new System.Drawing.Point(10, 457);
            this.layoutControlGroup5.Name = "layoutControlGroup1";
            this.layoutControlGroup5.Size = new System.Drawing.Size(1202, 94);
            this.layoutControlGroup5.Text = "Status";
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.dateEdit1;
            this.layoutControlItem10.Location = new System.Drawing.Point(844, 0);
            this.layoutControlItem10.Name = "layoutControlItem_CLIUltimaAlteracao";
            this.layoutControlItem10.Size = new System.Drawing.Size(334, 24);
            this.layoutControlItem10.Text = "Última Alteração";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.toggleSwitch1;
            this.layoutControlItem11.CustomizationFormText = "Status";
            this.layoutControlItem11.Location = new System.Drawing.Point(844, 24);
            this.layoutControlItem11.Name = "layoutControlItem_CLIExcluido";
            this.layoutControlItem11.Size = new System.Drawing.Size(334, 28);
            this.layoutControlItem11.Text = "Excluído";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem16
            // 
            this.emptySpaceItem16.AllowHotTrack = false;
            this.emptySpaceItem16.Location = new System.Drawing.Point(834, 0);
            this.emptySpaceItem16.Name = "emptySpaceItem10";
            this.emptySpaceItem16.Size = new System.Drawing.Size(10, 52);
            this.emptySpaceItem16.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.memoEdit1;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem12.Name = "layoutControlItem_CLIObservacao";
            this.layoutControlItem12.Size = new System.Drawing.Size(834, 52);
            this.layoutControlItem12.Text = "Observação";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(99, 13);
            // 
            // tabbedControlGroup3
            // 
            this.tabbedControlGroup3.Location = new System.Drawing.Point(10, 37);
            this.tabbedControlGroup3.Name = "tabbedControlGroup1";
            this.tabbedControlGroup3.SelectedTabPage = this.layoutControlGroup6;
            this.tabbedControlGroup3.SelectedTabPageIndex = 1;
            this.tabbedControlGroup3.Size = new System.Drawing.Size(1202, 214);
            this.tabbedControlGroup3.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup9,
            this.layoutControlGroup6});
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup4});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup_CLIContato";
            this.layoutControlGroup6.Size = new System.Drawing.Size(1178, 168);
            this.layoutControlGroup6.Text = "Contatos";
            // 
            // tabbedControlGroup4
            // 
            this.tabbedControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup4.Name = "tabbedControlGroup2";
            this.tabbedControlGroup4.SelectedTabPage = this.layoutControlGroup7;
            this.tabbedControlGroup4.SelectedTabPageIndex = 0;
            this.tabbedControlGroup4.Size = new System.Drawing.Size(1178, 168);
            this.tabbedControlGroup4.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup7,
            this.layoutControlGroup8});
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleSeparator2});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup_CLITelefone";
            this.layoutControlGroup7.Size = new System.Drawing.Size(1154, 122);
            this.layoutControlGroup7.Text = "Telefone";
            // 
            // simpleSeparator2
            // 
            this.simpleSeparator2.AllowHotTrack = false;
            this.simpleSeparator2.Location = new System.Drawing.Point(0, 0);
            this.simpleSeparator2.Name = "simpleSeparator1";
            this.simpleSeparator2.Size = new System.Drawing.Size(1154, 122);
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup8.Name = "layoutControlGroup_CLIEmail";
            this.layoutControlGroup8.Size = new System.Drawing.Size(1154, 122);
            this.layoutControlGroup8.Text = "E-mail";
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.layoutControlItem23});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup9.Name = "layoutControlGroup_CLIDados";
            this.layoutControlGroup9.Size = new System.Drawing.Size(1178, 168);
            this.layoutControlGroup9.Text = "Dados";
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.textEdit2;
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem17.Name = "layoutControlItem_CLIRazaoSocial";
            this.layoutControlItem17.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem17.Text = "Razão Social";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.textEdit3;
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem18.Name = "layoutControlItem_CLINomeFantasia";
            this.layoutControlItem18.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem18.Text = "Nome Fantasia";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.textEdit4;
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem19.Name = "layoutControlItem_CLITipo";
            this.layoutControlItem19.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem19.Text = "Tipo";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.textEdit5;
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem20.Name = "layoutControlItem_CLICnpj";
            this.layoutControlItem20.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem20.Text = "CNPJ";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.textEdit6;
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem21.Name = "layoutControlItem_CLIInscricaoEstadual";
            this.layoutControlItem21.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem21.Text = "Inscrição Estadual";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.textEdit7;
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem22.Name = "layoutControlItem_CLISuframa";
            this.layoutControlItem22.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem22.Text = "Suframa";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.textEdit14;
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItem23.Name = "layoutControlItem_CLINomeExcFiscal";
            this.layoutControlItem23.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem23.Text = "Nome Exceção Fiscal";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(99, 13);
            // 
            // simpleSeparator3
            // 
            this.simpleSeparator3.AllowHotTrack = false;
            this.simpleSeparator3.CustomizationFormText = "simpleSeparator1";
            this.simpleSeparator3.Location = new System.Drawing.Point(1005, 0);
            this.simpleSeparator3.Name = "simpleSeparator3";
            this.simpleSeparator3.Size = new System.Drawing.Size(2, 26);
            this.simpleSeparator3.Text = "simpleSeparator1";
            // 
            // layoutControlGroup_CLITelefone1
            // 
            this.layoutControlGroup_CLITelefone1.CustomizationFormText = "Telefone";
            this.layoutControlGroup_CLITelefone1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleSeparator3});
            this.layoutControlGroup_CLITelefone1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup_CLITelefone1.Name = "layoutControlGroup_CLITelefone1";
            this.layoutControlGroup_CLITelefone1.Size = new System.Drawing.Size(1205, 31);
            this.layoutControlGroup_CLITelefone1.Text = "Telefone";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup_CLITelefone1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 561);
            this.layoutControlGroup2.Name = "LayoutRootGroupForRestore";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1205, 31);
            this.layoutControlGroup2.Tag = "LayoutRootGroupForRestore";
            // 
            // colid1
            // 
            this.colid1.FieldName = "id";
            this.colid1.Name = "colid1";
            // 
            // colrazao_social
            // 
            this.colrazao_social.FieldName = "razao_social";
            this.colrazao_social.Name = "colrazao_social";
            // 
            // colnome_fantasia
            // 
            this.colnome_fantasia.FieldName = "nome_fantasia";
            this.colnome_fantasia.Name = "colnome_fantasia";
            // 
            // layoutControlGroup_CatProd
            // 
            this.layoutControlGroup_CatProd.Location = new System.Drawing.Point(10, 37);
            this.layoutControlGroup_CatProd.Name = "layoutControlGroup_CatProd";
            this.layoutControlGroup_CatProd.Size = new System.Drawing.Size(1202, 218);
            // 
            // iCMS_STTableAdapter
            // 
            this.iCMS_STTableAdapter.ClearBeforeFill = true;
            // 
            // FormICMSST
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1264, 562);
            this.Controls.Add(this.tabPane_ConfigICMSST);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormICMSST";
            this.ShowMdiChildCaptionInParentTitle = true;
            this.Text = "ICMS-ST";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormTabelaPreco_Load);
            this.tabNavigationPage_RegisterICMSST.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl_RegisterICMSST)).EndInit();
            this.panelControl_RegisterICMSST.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_ICMSST)).EndInit();
            this.layoutControl_ICMSST.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ICMSSTCod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager_ContasMP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_ICMSSTUltAlteracao.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_ICMSSTUltAlteracao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ICMSSTExcluido.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ICMSSTValMVA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit__ICMSSTNomeExcFiscal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ICMSSTValPMC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ICMSSTCredito.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ICMSSTDestino.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit_ICMSSTEstDestino.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit_ICMSSTTipoST.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_RegisterICMSST)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem_RegisterICMSST)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_ICMSST)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ICMSSTCod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ICMSSTDtUltAlteracao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ICMSSTExcluido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ICMSSTNomeExcFiscal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ICMSSTEstDestino)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ICMSSTValMVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ICMSSTCredito)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ICMSSTTipoSt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._ICMSSTValPMC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem__ICMSSTDestino)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meusPedidosDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem_RegisterClientes)).EndInit();
            this.tabNavigationPage_DataGridICMSST.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_ICMSST)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iCMSSTBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_ICMSST)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane_ConfigICMSST)).EndInit();
            this.tabPane_ConfigICMSST.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).EndInit();
            this.tabPane1.ResumeLayout(false);
            this.tabNavigationPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toggleSwitch1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CLITelefone1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CatProd)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Cliente;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CondicaoPagamentoCliente;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CategoriasCliente;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TabelaPreco;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Usuarios;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Produto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_ImagensProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TabelaProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TabelaPrecoProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CategoriaProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Transportadora;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CondicaoPagamento;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_FormaPagamento;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Pedido;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CamposExtraPedido;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_FaturamentoPedido;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TituloVencido;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager_ICMSST;
        private DevExpress.XtraBars.Navigation.TabPane tabPane_ConfigICMSST;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage_DataGridICMSST;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage_RegisterICMSST;
        private DevExpress.XtraBars.BarManager barManager_ContasMP;
        private DevExpress.XtraBars.Bar bar_Acoes;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Add;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Edit;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Del;
        private DevExpress.XtraBars.Bar bar_Acoes2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Gravar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Cancelar;
        private DevExpress.XtraEditors.PanelControl panelControl_RegisterICMSST;
        private DevExpress.XtraLayout.LayoutControl layoutControl_ICMSST;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_RegisterICMSST;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem_RegisterClientes;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraBars.Navigation.TabPane tabPane1;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage1;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage2;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraEditors.TextEdit textEdit3;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        private DevExpress.XtraEditors.TextEdit textEdit6;
        private DevExpress.XtraEditors.TextEdit textEdit7;
        private DevExpress.XtraEditors.TextEdit textEdit8;
        private DevExpress.XtraEditors.TextEdit textEdit9;
        private DevExpress.XtraEditors.TextEdit textEdit10;
        private DevExpress.XtraEditors.TextEdit textEdit11;
        private DevExpress.XtraEditors.TextEdit textEdit12;
        private DevExpress.XtraEditors.TextEdit textEdit13;
        private DevExpress.XtraEditors.TextEdit textEdit14;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
        private DevExpress.XtraEditors.ToggleSwitch toggleSwitch1;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_CLITelefone1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraGrid.Columns.GridColumn colid1;
        private DevExpress.XtraGrid.Columns.GridColumn colrazao_social;
        private DevExpress.XtraGrid.Columns.GridColumn colnome_fantasia;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraGrid.GridControl gridControl_ICMSST;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView_ICMSST;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_ICMSST;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_CatProd;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem_RegisterICMSST;
        private DevExpress.XtraEditors.TextEdit textEdit_ICMSSTCod;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_ICMSSTCod;
        private DevExpress.XtraEditors.DateEdit dateEdit_ICMSSTUltAlteracao;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_ICMSSTDtUltAlteracao;
        private DevExpress.XtraEditors.ToggleSwitch textEdit_ICMSSTExcluido;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_ICMSSTExcluido;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem18;
        private MeusPedidosDataSet meusPedidosDataSet;
        private DevExpress.XtraEditors.TextEdit textEdit_ICMSSTValMVA;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_ICMSSTValMVA;
        private DevExpress.XtraEditors.TextEdit textEdit__ICMSSTNomeExcFiscal;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_ICMSSTNomeExcFiscal;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_ICMSSTEstDestino;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_ICMSSTTipoSt;
        private DevExpress.XtraEditors.TextEdit textEdit_ICMSSTValPMC;
        private DevExpress.XtraLayout.LayoutControlItem _ICMSSTValPMC;
        private DevExpress.XtraEditors.TextEdit textEdit_ICMSSTCredito;
        private DevExpress.XtraEditors.TextEdit textEdit_ICMSSTDestino;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_ICMSSTCredito;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem__ICMSSTDestino;
        private System.Windows.Forms.BindingSource iCMSSTBindingSource;
        private MeusPedidosDataSetTableAdapters.ICMS_STTableAdapter iCMS_STTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colid;
        private DevExpress.XtraGrid.Columns.GridColumn colcodigo_ncm;
        private DevExpress.XtraGrid.Columns.GridColumn colnome_excecao_fiscal;
        private DevExpress.XtraGrid.Columns.GridColumn colestado_destino;
        private DevExpress.XtraGrid.Columns.GridColumn coltipo_st;
        private DevExpress.XtraGrid.Columns.GridColumn colvalor_mva;
        private DevExpress.XtraGrid.Columns.GridColumn colvalor_pmc;
        private DevExpress.XtraGrid.Columns.GridColumn colicms_credito;
        private DevExpress.XtraGrid.Columns.GridColumn colicms_destino;
        private DevExpress.XtraGrid.Columns.GridColumn colexcluido;
        private DevExpress.XtraGrid.Columns.GridColumn colultima_alteracao;
        private DevExpress.XtraGrid.Columns.GridColumn colmptran;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem20;
        private DevExpress.XtraEditors.LookUpEdit comboBoxEdit_ICMSSTEstDestino;
        private DevExpress.XtraEditors.LookUpEdit comboBoxEdit_ICMSSTTipoST;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Control;
using Core;
using DevExpress.XtraEditors;
using System.Data.Entity;
using Control.Validations;
using DevExpress.XtraGrid.Views.Grid;
using System.Globalization;

namespace View_MeusPedidos
{
    public partial class FormVendedor : DevExpress.XtraEditors.XtraForm
    {
        Vendedor_Service s_v;
        Vendedor vend;

        int _id;
        bool add_status = false;

        public FormVendedor()
        {
            InitializeComponent();
            s_v = new Vendedor_Service();

            bar_Acoes2.Visible = false;

            tabNavigationPage_RegisterVendedor.PageVisible = false;
            tabPane_ConfigVendedor.SelectedPage = tabNavigationPage_DataGridVendedor;

            gridControl_Vendedor.DataSource = s_v.GetAll_Vendedores();
        }

        #region ToolBar1 Events - Adicionar/Editar/Deletar

        private void barButtonItem_Add_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            add_status = true;

            //UI Behavior

            FormContols_FW.fieldsClean(layoutControl_TabPreco);

            bar_Acoes.Visible = false;
            bar_Acoes2.Visible = true;

            tabNavigationPage_RegisterVendedor.PageVisible = true;
            tabNavigationPage_DataGridVendedor.PageVisible = false;
            tabPane_ConfigVendedor.SelectedPage = tabNavigationPage_RegisterVendedor;

            //Define TextEdit Fields as ReadOnly = false
            FormContols_FW.fieldsReadOnly(layoutControl_TabPreco, false);
        }

        private void barButtonItem_Edit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            add_status = false;

            var row = gridView_TabPreco.GetRow(gridView_TabPreco.GetFocusedDataSourceRowIndex());
            vend = (Vendedor)row;

            if(vend != null)
            {
                if (XtraMessageBox.Show("Tem certeza que deseja alterar o registro (ID: " + vend.id + ")?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    //Retrieve entity ID
                    _id = vend.id;

                    //Fill TextEdit Fields
                    textEdit_TabPrecoNome.Text = vend.nome;
                    textEdit_TabPrecoExcluido.EditValue = vend.excluido;
                    dateEdit_TabPrecoUltAlteracao.DateTime = vend.ultima_alteracao;

                    //UI Behavior
                    bar_Acoes.Visible = false;
                    bar_Acoes2.Visible = true;

                    tabNavigationPage_RegisterVendedor.PageVisible = true;
                    tabNavigationPage_DataGridVendedor.PageVisible = false;
                    tabPane_ConfigVendedor.SelectedPage = tabNavigationPage_RegisterVendedor;

                    //Define TextEdit Fields as ReadOnly = false
                    FormContols_FW.fieldsReadOnly(layoutControl_TabPreco, false);
                }
            }
            
        }

        private void barButtonItem_Del_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var row = gridView_TabPreco.GetRow(gridView_TabPreco.GetFocusedDataSourceRowIndex());
            vend = (Vendedor)row;
            
            if(vend != null)
            {
                if (XtraMessageBox.Show("Tem certeza que deseja excluir o registro (ID: " + vend.id + ")?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    vend.excluido = true;
                    s_v.Update_Vendedor(vend);

                    gridControl_Vendedor.DataSource = s_v.GetAll_Vendedores();
                }
            }
            
        }
        #endregion

        #region ToolBar2 Events - Cancelar/Gravar
        private void barButtonItem_Gravar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (add_status == true &&
                textEdit_TabPrecoNome.Text != "")
            {
                /*
                vend = new Vendedor();
                vend.nome = textEdit_TabPrecoNome.Text;
                vend.acrescimo = float.Parse(textEdit_TabPrecoAcrescimo.EditValue.ToString(), CultureInfo.InvariantCulture.NumberFormat);
                vend.desconto = float.Parse(textEdit_TabPrecoDesconto.EditValue.ToString(), CultureInfo.InvariantCulture.NumberFormat);
                vend.tipo = comboBoxEdit_TabPrecoTipo.Text;
                vend.excluido = textEdit_TabPrecoExcluido.IsOn;
                vend.ultima_alteracao = DateTime.Now;
                vend.mptran = "I";
                */
                //Validação Nome
                var aux = s_v.GetByNome_Vendedor(vend.nome);

                if (aux == null)
                {
                    splashScreenManager_Transp.ShowWaitForm();

                    //Add new ProdCat
                    s_v.AddNew_Vendedor(vend);

                    splashScreenManager_Transp.CloseWaitForm();

                    XtraMessageBox.Show("Registro salvo!", "Informação");

                    FormContols_FW.fieldsClean(layoutControl_TabPreco);

                    gridControl_Vendedor.DataSource = s_v.GetAll_Vendedores();

                    bar_Acoes.Visible = true;
                    bar_Acoes2.Visible = false;
                    tabNavigationPage_RegisterVendedor.PageVisible = false;
                    tabNavigationPage_DataGridVendedor.PageVisible = true;
                    tabPane_ConfigVendedor.SelectedPage = tabNavigationPage_DataGridVendedor;
                }
                else
                {
                    splashScreenManager_Transp.CloseWaitForm();

                    XtraMessageBox.Show("Já existe uma transportadora com este nome cadastrado!", "Atenção");
                }

            }
            else if (add_status == false &&
                textEdit_TabPrecoNome.Text != "")
            {
                splashScreenManager_Transp.ShowWaitForm();

                vend = new Vendedor();
                vend.id = _id;

                vend = s_v.GetBySpecification_Vendedor(vend.id);
                /*
                vend.nome = textEdit_TabPrecoNome.Text;
                vend.acrescimo = float.Parse(textEdit_TabPrecoAcrescimo.EditValue.ToString(), CultureInfo.InvariantCulture.NumberFormat);
                vend.desconto = float.Parse(textEdit_TabPrecoDesconto.EditValue.ToString(), CultureInfo.InvariantCulture.NumberFormat);
                vend.tipo = comboBoxEdit_TabPrecoTipo.Text;
                vend.excluido = textEdit_TabPrecoExcluido.IsOn;
                vend.ultima_alteracao = DateTime.Now;
                vend.mptran = "A";
                */
                s_v.Update_Vendedor(vend);

                splashScreenManager_Transp.CloseWaitForm();

                XtraMessageBox.Show("Alteração realizada com sucesso!", "Informação");

                FormContols_FW.fieldsClean(layoutControl_TabPreco);

                gridControl_Vendedor.DataSource = s_v.GetAll_Vendedores();

                bar_Acoes.Visible = true;
                bar_Acoes2.Visible = false;
                tabNavigationPage_RegisterVendedor.PageVisible = false;
                tabNavigationPage_DataGridVendedor.PageVisible = true;
                tabPane_ConfigVendedor.SelectedPage = tabNavigationPage_DataGridVendedor;
            }
            else
            {
                XtraMessageBox.Show("Algum campo obrigatório precisa ser preenchido!", "Informação");
            }
        }

        private void barButtonItem_Cancelar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (XtraMessageBox.Show("Tem certeza que deseja sair da tela?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                barButtonItem_Gravar.Enabled = true;

                bar_Acoes.Visible = true;
                bar_Acoes2.Visible = false;
                tabNavigationPage_RegisterVendedor.PageVisible = false;
                tabNavigationPage_DataGridVendedor.PageVisible = true;
                tabPane_ConfigVendedor.SelectedPage = tabNavigationPage_DataGridVendedor;

                FormContols_FW.fieldsClean(layoutControl_TabPreco);
            }
        }
        #endregion

        #region Grid Transportadora Events
        private void gridView_Clientes_DoubleClick(object sender, EventArgs e)
        {
            barButtonItem_Gravar.Enabled = false;

            //Get (object) row values
            var row = gridView_TabPreco.GetRow(gridView_TabPreco.GetFocusedDataSourceRowIndex());
            vend = (Vendedor)row;
            /*
            //Fill TextEdit Fields
            textEdit_TabPrecoNome.Text = vend.nome;
            textEdit_TabPrecoAcrescimo.Text = vend.acrescimo.ToString();
            textEdit_TabPrecoDesconto.Text = vend.desconto.ToString();
            comboBoxEdit_TabPrecoTipo.Text = vend.tipo;
            textEdit_TabPrecoExcluido.EditValue = vend.excluido;
            dateEdit_TabPrecoUltAlteracao.DateTime = vend.ultima_alteracao;
            */
            //Define TextEdit Fields as ReadOnly = true
            FormContols_FW.fieldsReadOnly(layoutControl_TabPreco, true);

            //UI Behavior
            bar_Acoes.Visible = false;
            bar_Acoes2.Visible = true;

            tabNavigationPage_RegisterVendedor.PageVisible = true;
            tabNavigationPage_DataGridVendedor.PageVisible = false;
            tabPane_ConfigVendedor.SelectedPage = tabNavigationPage_RegisterVendedor;
        }
        #endregion

        private void FormVendedor_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'meusPedidosDataSet1.Vendedor' table. You can move, or remove it, as needed.
            this.vendedorTableAdapter1.Fill(this.meusPedidosDataSet1.Vendedor);

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Control;
using Core;
using DevExpress.XtraEditors;
using System.Data.Entity;
using Control.Validations;
using DevExpress.XtraGrid.Views.Grid;

namespace View_MeusPedidos
{
    public partial class FormICMSST : DevExpress.XtraEditors.XtraForm
    {
        ICMS_ST_Service s_icmsst;
        ICMS_ST icmsst;

        int _id;
        bool add_status = false;

        public FormICMSST()
        {
            InitializeComponent();
            s_icmsst = new ICMS_ST_Service();

            bar_Acoes2.Visible = false;

            tabNavigationPage_RegisterICMSST.PageVisible = false;
            tabPane_ConfigICMSST.SelectedPage = tabNavigationPage_DataGridICMSST;

            gridControl_ICMSST.DataSource = s_icmsst.GetAll_ICMS_ST();

            //Load ComboBOx;
            comboBoxEdit_ICMSSTTipoST.Properties.DataSource = Enum.GetValues(typeof(Core.IPI_Type));
            comboBoxEdit_ICMSSTEstDestino.Properties.DataSource = Enum.GetValues(typeof(Core.Estados_BR));
        }

        #region ToolBar1 Events - Adicionar/Editar/Deletar

        private void barButtonItem_Add_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            add_status = true;

            //UI Behavior

            FormContols_FW.fieldsClean(layoutControl_ICMSST);

            bar_Acoes.Visible = false;
            bar_Acoes2.Visible = true;

            tabNavigationPage_RegisterICMSST.PageVisible = true;
            tabNavigationPage_DataGridICMSST.PageVisible = false;
            tabPane_ConfigICMSST.SelectedPage = tabNavigationPage_RegisterICMSST;

            //Define TextEdit Fields as ReadOnly = false
            FormContols_FW.fieldsReadOnly(layoutControl_ICMSST, false);
        }

        private void barButtonItem_Edit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            add_status = false;

            var row = gridView_ICMSST.GetRow(gridView_ICMSST.GetFocusedDataSourceRowIndex());
            icmsst = (ICMS_ST)row;

            if(icmsst != null)
            {
                if (XtraMessageBox.Show("Tem certeza que deseja alterar o registro (ID: " + icmsst.id + ")?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    //Retrieve entity ID
                    _id = icmsst.id;

                    //Fill TextEdit Fields
                    textEdit_ICMSSTCod.Text = icmsst.codigo_ncm;
                    textEdit__ICMSSTNomeExcFiscal.Text = icmsst.nome_excecao_fiscal;
                    comboBoxEdit_ICMSSTTipoST.Text = icmsst.tipo_st;
                    comboBoxEdit_ICMSSTEstDestino.EditValue = icmsst.estado_destino;
                    textEdit_ICMSSTValMVA.EditValue = icmsst.valor_mva;
                    textEdit_ICMSSTValPMC.EditValue = icmsst.valor_pmc;
                    textEdit_ICMSSTCredito.EditValue = icmsst.icms_credito;
                    textEdit_ICMSSTDestino.EditValue = icmsst.icms_destino;
                    textEdit_ICMSSTExcluido.EditValue = icmsst.excluido;
                    dateEdit_ICMSSTUltAlteracao.DateTime = icmsst.ultima_alteracao;

                    //UI Behavior
                    bar_Acoes.Visible = false;
                    bar_Acoes2.Visible = true;

                    tabNavigationPage_RegisterICMSST.PageVisible = true;
                    tabNavigationPage_DataGridICMSST.PageVisible = false;
                    tabPane_ConfigICMSST.SelectedPage = tabNavigationPage_RegisterICMSST;

                    //Define TextEdit Fields as ReadOnly = false
                    FormContols_FW.fieldsReadOnly(layoutControl_ICMSST, false);
                }
            }
            
        }

        private void barButtonItem_Del_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var row = gridView_ICMSST.GetRow(gridView_ICMSST.GetFocusedDataSourceRowIndex());
            icmsst = (ICMS_ST)row;
            
            if(icmsst != null)
            {
                if (XtraMessageBox.Show("Tem certeza que deseja excluir o registro (ID: " + icmsst.id + ")?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    icmsst.excluido = true;
                    s_icmsst.Update_ICMS_ST(icmsst);

                    gridControl_ICMSST.DataSource = s_icmsst.GetAll_ICMS_ST();
                }
            }
            
        }
        #endregion

        #region ToolBar2 Events - Cancelar/Gravar
        private void barButtonItem_Gravar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (add_status == true &&
                comboBoxEdit_ICMSSTEstDestino.EditValue != null &&
                comboBoxEdit_ICMSSTTipoST.EditValue != null &&
                textEdit_ICMSSTCredito.EditValue != null &&
                textEdit_ICMSSTDestino.EditValue != null &&
                textEdit_ICMSSTCod.Text != "")
            {

                icmsst = new ICMS_ST();
                icmsst.codigo_ncm = textEdit_ICMSSTCod.Text;
                icmsst.nome_excecao_fiscal = textEdit__ICMSSTNomeExcFiscal.Text;
                icmsst.estado_destino = comboBoxEdit_ICMSSTEstDestino.Text.ToString();
                icmsst.valor_mva = double.Parse(textEdit_ICMSSTValMVA.EditValue.ToString());
                icmsst.valor_pmc = double.Parse(textEdit_ICMSSTValPMC.EditValue.ToString());
                icmsst.tipo_st = comboBoxEdit_ICMSSTTipoST.EditValue.ToString();
                icmsst.icms_credito = double.Parse(textEdit_ICMSSTCredito.EditValue.ToString());
                icmsst.icms_destino = double.Parse(textEdit_ICMSSTDestino.EditValue.ToString());
                icmsst.excluido = textEdit_ICMSSTExcluido.IsOn;
                icmsst.ultima_alteracao = DateTime.Now;
                icmsst.mptran = "I";

                //Validação Nome
                var aux = s_icmsst.GetBySpecification_ICMS_ST(icmsst.id);

                if (aux == null)
                {
                    if(!splashScreenManager_ICMSST.IsSplashFormVisible)
                    {
                        splashScreenManager_ICMSST.ShowWaitForm();
                    }

                    //Add new ProdCat
                    s_icmsst.AddNew_ICMS_ST(icmsst);

                    splashScreenManager_ICMSST.CloseWaitForm();

                    XtraMessageBox.Show("Registro salvo!", "Informação");

                    FormContols_FW.fieldsClean(layoutControl_ICMSST);

                    gridControl_ICMSST.DataSource = s_icmsst.GetAll_ICMS_ST();

                    bar_Acoes.Visible = true;
                    bar_Acoes2.Visible = false;
                    tabNavigationPage_RegisterICMSST.PageVisible = false;
                    tabNavigationPage_DataGridICMSST.PageVisible = true;
                    tabPane_ConfigICMSST.SelectedPage = tabNavigationPage_DataGridICMSST;
                }
                else
                {
                    splashScreenManager_ICMSST.CloseWaitForm();

                    XtraMessageBox.Show("Já existe uma transportadora com este nome cadastrado!", "Atenção");
                }

            }
            else if (add_status == false &&
                    comboBoxEdit_ICMSSTEstDestino.EditValue != null &&
                    comboBoxEdit_ICMSSTTipoST.EditValue != null &&
                    textEdit_ICMSSTCredito.EditValue != null &&
                    textEdit_ICMSSTDestino.EditValue != null &&
                    textEdit_ICMSSTCod.Text != "")
            {
                splashScreenManager_ICMSST.ShowWaitForm();

                icmsst = new ICMS_ST();
                icmsst.id = _id;

                icmsst = s_icmsst.GetBySpecification_ICMS_ST(icmsst.id);

                icmsst.codigo_ncm = textEdit_ICMSSTCod.Text;
                icmsst.nome_excecao_fiscal = textEdit__ICMSSTNomeExcFiscal.Text;
                icmsst.estado_destino = comboBoxEdit_ICMSSTEstDestino.Text;
                icmsst.tipo_st = comboBoxEdit_ICMSSTTipoST.EditValue.ToString();
                icmsst.valor_mva = double.Parse(textEdit_ICMSSTValMVA.EditValue.ToString());
                icmsst.valor_pmc = double.Parse(textEdit_ICMSSTValPMC.EditValue.ToString());
                icmsst.icms_credito = double.Parse(textEdit_ICMSSTCredito.EditValue.ToString());
                icmsst.icms_destino = double.Parse(textEdit_ICMSSTDestino.EditValue.ToString());
                icmsst.excluido = textEdit_ICMSSTExcluido.IsOn;
                icmsst.ultima_alteracao = DateTime.Now;
                icmsst.mptran = "A";

                s_icmsst.Update_ICMS_ST(icmsst);

                splashScreenManager_ICMSST.CloseWaitForm();

                XtraMessageBox.Show("Alteração realizada com sucesso!", "Informação");

                FormContols_FW.fieldsClean(layoutControl_ICMSST);

                gridControl_ICMSST.DataSource = s_icmsst.GetAll_ICMS_ST();

                bar_Acoes.Visible = true;
                bar_Acoes2.Visible = false;
                tabNavigationPage_RegisterICMSST.PageVisible = false;
                tabNavigationPage_DataGridICMSST.PageVisible = true;
                tabPane_ConfigICMSST.SelectedPage = tabNavigationPage_DataGridICMSST;
            }
            else
            {
                XtraMessageBox.Show("Algum campo obrigatório precisa ser preenchido!", "Informação");
            }
        }

        private void barButtonItem_Cancelar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (XtraMessageBox.Show("Tem certeza que deseja sair da tela?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                barButtonItem_Gravar.Enabled = true;

                bar_Acoes.Visible = true;
                bar_Acoes2.Visible = false;
                tabNavigationPage_RegisterICMSST.PageVisible = false;
                tabNavigationPage_DataGridICMSST.PageVisible = true;
                tabPane_ConfigICMSST.SelectedPage = tabNavigationPage_DataGridICMSST;

                FormContols_FW.fieldsClean(layoutControl_ICMSST);
            }
        }
        #endregion

        #region Grid Transportadora Events
        private void gridView_Clientes_DoubleClick(object sender, EventArgs e)
        {
            barButtonItem_Gravar.Enabled = false;

            //Get (object) row values
            var row = gridView_ICMSST.GetRow(gridView_ICMSST.GetFocusedDataSourceRowIndex());
            icmsst = (ICMS_ST)row;

            //Fill TextEdit Fields
            //Fill TextEdit Fields
            textEdit_ICMSSTCod.Text = icmsst.codigo_ncm;
            textEdit__ICMSSTNomeExcFiscal.Text = icmsst.nome_excecao_fiscal;
            textEdit_ICMSSTDestino.EditValue = icmsst.estado_destino;
            textEdit_ICMSSTValMVA.EditValue = icmsst.valor_mva;
            textEdit_ICMSSTValPMC.EditValue = icmsst.valor_pmc;
            textEdit_ICMSSTCredito.EditValue = icmsst.icms_credito;
            textEdit_ICMSSTDestino.EditValue = icmsst.icms_destino;
            textEdit_ICMSSTExcluido.EditValue = icmsst.excluido;
            dateEdit_ICMSSTUltAlteracao.DateTime = icmsst.ultima_alteracao;

            //Define TextEdit Fields as ReadOnly = true
            FormContols_FW.fieldsReadOnly(layoutControl_ICMSST, true);

            //UI Behavior
            bar_Acoes.Visible = false;
            bar_Acoes2.Visible = true;

            tabNavigationPage_RegisterICMSST.PageVisible = true;
            tabNavigationPage_DataGridICMSST.PageVisible = false;
            tabPane_ConfigICMSST.SelectedPage = tabNavigationPage_RegisterICMSST;
        }
        #endregion

        private void FormTabelaPreco_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'meusPedidosDataSet.ICMS_ST' table. You can move, or remove it, as needed.
            this.iCMS_STTableAdapter.Fill(this.meusPedidosDataSet.ICMS_ST);
        }
    }
}

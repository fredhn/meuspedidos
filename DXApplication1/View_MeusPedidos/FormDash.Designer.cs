﻿namespace View_MeusPedidos
{
    partial class FormDash
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDash));
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement13 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement10 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement11 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement12 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement22 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement14 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement15 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement16 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement17 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement18 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement19 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement20 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement21 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement28 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement23 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement24 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement25 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement26 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement27 = new DevExpress.XtraEditors.TileItemElement();
            this.splashScreenManager_FormDash = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::View_MeusPedidos.WaitForm1), true, true);
            this.tileNavPane_dash = new DevExpress.XtraBars.Navigation.TileNavPane();
            this.navButton_Home = new DevExpress.XtraBars.Navigation.NavButton();
            this.tileNavCategoryBtn_Cadastro = new DevExpress.XtraBars.Navigation.TileNavCategory();
            this.tileNavItem_Cliente = new DevExpress.XtraBars.Navigation.TileNavItem();
            this.tileNavSubItem_Cliente = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CondicaoPagamentoCliente = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CategoriasCliente = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_ClienteTabelaPreco = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Usuarios = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_rl = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavItem_Produto = new DevExpress.XtraBars.Navigation.TileNavItem();
            this.tileNavSubItem_Produto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_ImagensProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TabelaProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TabelaPrecoProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CategoriaProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavItem_Pedido = new DevExpress.XtraBars.Navigation.TileNavItem();
            this.tileNavSubItem_Pedido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CamposExtraPedido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Transportadora = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CondicaoPagamento = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_FormaPagamento = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_FaturamentoPedido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TituloVencido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_ICMSST = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavCategoryBtn_Configuracao = new DevExpress.XtraBars.Navigation.TileNavCategory();
            this.tileNavItem_ConfigDB = new DevExpress.XtraBars.Navigation.TileNavItem();
            this.tileNavItem_CadastroContas = new DevExpress.XtraBars.Navigation.TileNavItem();
            this.tileNavItem_Rastreabilidade = new DevExpress.XtraBars.Navigation.TileNavItem();
            this.tileNavItem_ConfigUsuarios = new DevExpress.XtraBars.Navigation.TileNavItem();
            this.tileNavItem_Logoff = new DevExpress.XtraBars.Navigation.TileNavItem();
            this.navButton_SyncTest = new DevExpress.XtraBars.Navigation.NavButton();
            this.xtraTabbedMdiManager_Dash = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.tileNavPane_dash)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager_Dash)).BeginInit();
            this.SuspendLayout();
            // 
            // splashScreenManager_FormDash
            // 
            this.splashScreenManager_FormDash.ClosingDelay = 500;
            // 
            // tileNavPane_dash
            // 
            this.tileNavPane_dash.Buttons.Add(this.navButton_Home);
            this.tileNavPane_dash.Buttons.Add(this.tileNavCategoryBtn_Cadastro);
            this.tileNavPane_dash.Buttons.Add(this.tileNavCategoryBtn_Configuracao);
            this.tileNavPane_dash.Buttons.Add(this.navButton_SyncTest);
            // 
            // tileNavCategory1
            // 
            this.tileNavPane_dash.DefaultCategory.Name = "tileNavCategory1";
            this.tileNavPane_dash.DefaultCategory.OwnerCollection = null;
            // 
            // 
            // 
            this.tileNavPane_dash.DefaultCategory.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            this.tileNavPane_dash.Dock = System.Windows.Forms.DockStyle.Top;
            this.tileNavPane_dash.Location = new System.Drawing.Point(0, 0);
            this.tileNavPane_dash.Name = "tileNavPane_dash";
            this.tileNavPane_dash.Size = new System.Drawing.Size(1264, 40);
            this.tileNavPane_dash.TabIndex = 0;
            this.tileNavPane_dash.Text = "tileNavPane1";
            // 
            // navButton_Home
            // 
            this.navButton_Home.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Left;
            this.navButton_Home.Caption = "Home";
            this.navButton_Home.Glyph = ((System.Drawing.Image)(resources.GetObject("navButton_Home.Glyph")));
            this.navButton_Home.Name = "navButton_Home";
            this.navButton_Home.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.tileNavItem_Home_Click);
            // 
            // tileNavCategoryBtn_Cadastro
            // 
            this.tileNavCategoryBtn_Cadastro.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Left;
            this.tileNavCategoryBtn_Cadastro.Caption = "Cadastro";
            this.tileNavCategoryBtn_Cadastro.Glyph = ((System.Drawing.Image)(resources.GetObject("tileNavCategoryBtn_Cadastro.Glyph")));
            this.tileNavCategoryBtn_Cadastro.Items.AddRange(new DevExpress.XtraBars.Navigation.TileNavItem[] {
            this.tileNavItem_Cliente,
            this.tileNavItem_Produto,
            this.tileNavItem_Pedido});
            this.tileNavCategoryBtn_Cadastro.Name = "tileNavCategoryBtn_Cadastro";
            this.tileNavCategoryBtn_Cadastro.OwnerCollection = null;
            // 
            // 
            // 
            this.tileNavCategoryBtn_Cadastro.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            // 
            // tileNavItem_Cliente
            // 
            this.tileNavItem_Cliente.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Left;
            this.tileNavItem_Cliente.Caption = "Clientes";
            this.tileNavItem_Cliente.Name = "tileNavItem_Cliente";
            this.tileNavItem_Cliente.OwnerCollection = this.tileNavCategoryBtn_Cadastro.Items;
            this.tileNavItem_Cliente.SubItems.AddRange(new DevExpress.XtraBars.Navigation.TileNavSubItem[] {
            this.tileNavSubItem_Cliente,
            this.tileNavSubItem_CondicaoPagamentoCliente,
            this.tileNavSubItem_CategoriasCliente,
            this.tileNavSubItem_ClienteTabelaPreco,
            this.tileNavSubItem_Usuarios,
            this.tileNavSubItem_rl});
            // 
            // 
            // 
            this.tileNavItem_Cliente.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement7.Text = "Clientes";
            this.tileNavItem_Cliente.Tile.Elements.Add(tileItemElement7);
            this.tileNavItem_Cliente.Tile.Name = "tileBarItem1";
            // 
            // tileNavSubItem_Cliente
            // 
            this.tileNavSubItem_Cliente.Caption = "Cliente";
            this.tileNavSubItem_Cliente.Name = "tileNavSubItem_Cliente";
            // 
            // 
            // 
            this.tileNavSubItem_Cliente.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement1.Text = "Cliente";
            this.tileNavSubItem_Cliente.Tile.Elements.Add(tileItemElement1);
            this.tileNavSubItem_Cliente.Tile.Name = "tileBarItem3";
            this.tileNavSubItem_Cliente.TileClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.tileNavSubItem_Cliente_Click);
            // 
            // tileNavSubItem_CondicaoPagamentoCliente
            // 
            this.tileNavSubItem_CondicaoPagamentoCliente.Caption = "Condições de Pagamento por Cliente";
            this.tileNavSubItem_CondicaoPagamentoCliente.Name = "tileNavSubItem_CondicaoPagamentoCliente";
            // 
            // 
            // 
            this.tileNavSubItem_CondicaoPagamentoCliente.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement2.Text = "Condições de Pagamento por Cliente";
            this.tileNavSubItem_CondicaoPagamentoCliente.Tile.Elements.Add(tileItemElement2);
            this.tileNavSubItem_CondicaoPagamentoCliente.Tile.Name = "tileBarItem4";
            this.tileNavSubItem_CondicaoPagamentoCliente.TileClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.tileNavSubItem_ClienteCondicaoPagamento_Click);
            // 
            // tileNavSubItem_CategoriasCliente
            // 
            this.tileNavSubItem_CategoriasCliente.Caption = "Categorias por Cliente";
            this.tileNavSubItem_CategoriasCliente.Name = "tileNavSubItem_CategoriasCliente";
            // 
            // 
            // 
            this.tileNavSubItem_CategoriasCliente.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement3.Text = "Categorias por Cliente";
            this.tileNavSubItem_CategoriasCliente.Tile.Elements.Add(tileItemElement3);
            this.tileNavSubItem_CategoriasCliente.Tile.Name = "tileBarItem6";
            this.tileNavSubItem_CategoriasCliente.TileClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.tileNavSubItem_ClienteCategoria_Click);
            // 
            // tileNavSubItem_ClienteTabelaPreco
            // 
            this.tileNavSubItem_ClienteTabelaPreco.Caption = "Tabelas de Preço por Cliente";
            this.tileNavSubItem_ClienteTabelaPreco.Name = "tileNavSubItem_ClienteTabelaPreco";
            // 
            // 
            // 
            this.tileNavSubItem_ClienteTabelaPreco.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement4.Text = "Tabelas de Preço por Cliente";
            this.tileNavSubItem_ClienteTabelaPreco.Tile.Elements.Add(tileItemElement4);
            this.tileNavSubItem_ClienteTabelaPreco.Tile.Name = "tileBarItem7";
            this.tileNavSubItem_ClienteTabelaPreco.TileClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.tileNavSubItem_ClienteTabelaPreco_Click);
            // 
            // tileNavSubItem_Usuarios
            // 
            this.tileNavSubItem_Usuarios.Caption = "Usuários (Vendedores)";
            this.tileNavSubItem_Usuarios.Name = "tileNavSubItem_Usuarios";
            // 
            // 
            // 
            this.tileNavSubItem_Usuarios.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement5.Text = "Usuários (Vendedores)";
            this.tileNavSubItem_Usuarios.Tile.Elements.Add(tileItemElement5);
            this.tileNavSubItem_Usuarios.Tile.Name = "tileBarItem8";
            this.tileNavSubItem_Usuarios.TileClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.tileNavSubItem_Vendedor_Click);
            // 
            // tileNavSubItem_rl
            // 
            this.tileNavSubItem_rl.Caption = "Regras de Liberação";
            this.tileNavSubItem_rl.Name = "tileNavSubItem_rl";
            // 
            // 
            // 
            this.tileNavSubItem_rl.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement6.Text = "Regras de Liberação";
            this.tileNavSubItem_rl.Tile.Elements.Add(tileItemElement6);
            this.tileNavSubItem_rl.Tile.Name = "tileBarItem1";
            this.tileNavSubItem_rl.TileClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.tileNavSubItem_RegrasLib_Click);
            // 
            // tileNavItem_Produto
            // 
            this.tileNavItem_Produto.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Left;
            this.tileNavItem_Produto.Caption = "Produtos";
            this.tileNavItem_Produto.Name = "tileNavItem_Produto";
            this.tileNavItem_Produto.OwnerCollection = this.tileNavCategoryBtn_Cadastro.Items;
            this.tileNavItem_Produto.SubItems.AddRange(new DevExpress.XtraBars.Navigation.TileNavSubItem[] {
            this.tileNavSubItem_Produto,
            this.tileNavSubItem_ImagensProduto,
            this.tileNavSubItem_TabelaProduto,
            this.tileNavSubItem_TabelaPrecoProduto,
            this.tileNavSubItem_CategoriaProduto});
            // 
            // 
            // 
            this.tileNavItem_Produto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement13.Text = "Produtos";
            this.tileNavItem_Produto.Tile.Elements.Add(tileItemElement13);
            this.tileNavItem_Produto.Tile.Name = "tileBarItem2";
            // 
            // tileNavSubItem_Produto
            // 
            this.tileNavSubItem_Produto.Caption = "Produto";
            this.tileNavSubItem_Produto.Name = "tileNavSubItem_Produto";
            // 
            // 
            // 
            this.tileNavSubItem_Produto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement8.Text = "Produto";
            this.tileNavSubItem_Produto.Tile.Elements.Add(tileItemElement8);
            this.tileNavSubItem_Produto.Tile.Name = "tileBarItem9";
            this.tileNavSubItem_Produto.TileClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.tileNavSubItem_Produto_Click);
            // 
            // tileNavSubItem_ImagensProduto
            // 
            this.tileNavSubItem_ImagensProduto.Caption = "Imagens do Produto";
            this.tileNavSubItem_ImagensProduto.Name = "tileNavSubItem_ImagensProduto";
            // 
            // 
            // 
            this.tileNavSubItem_ImagensProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement9.Text = "Imagens do Produto";
            this.tileNavSubItem_ImagensProduto.Tile.Elements.Add(tileItemElement9);
            this.tileNavSubItem_ImagensProduto.Tile.Name = "tileBarItem10";
            // 
            // tileNavSubItem_TabelaProduto
            // 
            this.tileNavSubItem_TabelaProduto.Caption = "Tabela de Preço";
            this.tileNavSubItem_TabelaProduto.Name = "tileNavSubItem_TabelaProduto";
            // 
            // 
            // 
            this.tileNavSubItem_TabelaProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement10.Text = "Tabelas de Preço";
            this.tileNavSubItem_TabelaProduto.Tile.Elements.Add(tileItemElement10);
            this.tileNavSubItem_TabelaProduto.Tile.Name = "tileBarItem11";
            this.tileNavSubItem_TabelaProduto.TileClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.tileNavSubItem_TabelaPreco_Click);
            // 
            // tileNavSubItem_TabelaPrecoProduto
            // 
            this.tileNavSubItem_TabelaPrecoProduto.Caption = "Tabelas de Preço por Produto";
            this.tileNavSubItem_TabelaPrecoProduto.Name = "tileNavSubItem_TabelaPrecoProduto";
            // 
            // 
            // 
            this.tileNavSubItem_TabelaPrecoProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement11.Text = "Tabelas de Preço por Produto";
            this.tileNavSubItem_TabelaPrecoProduto.Tile.Elements.Add(tileItemElement11);
            this.tileNavSubItem_TabelaPrecoProduto.Tile.Name = "tileBarItem12";
            this.tileNavSubItem_TabelaPrecoProduto.TileClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.tileNavSubItem_ProdutoTabelaPreco_Click);
            // 
            // tileNavSubItem_CategoriaProduto
            // 
            this.tileNavSubItem_CategoriaProduto.Caption = "Categorias de Produtos";
            this.tileNavSubItem_CategoriaProduto.Name = "tileNavSubItem_CategoriaProduto";
            // 
            // 
            // 
            this.tileNavSubItem_CategoriaProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement12.Text = "Categorias de Produtos";
            this.tileNavSubItem_CategoriaProduto.Tile.Elements.Add(tileItemElement12);
            this.tileNavSubItem_CategoriaProduto.Tile.Name = "tileBarItem13";
            this.tileNavSubItem_CategoriaProduto.TileClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.tileNavSubItem_ProdutoCategoria_Click);
            // 
            // tileNavItem_Pedido
            // 
            this.tileNavItem_Pedido.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Left;
            this.tileNavItem_Pedido.Caption = "Pedidos";
            this.tileNavItem_Pedido.Name = "tileNavItem_Pedido";
            this.tileNavItem_Pedido.OwnerCollection = this.tileNavCategoryBtn_Cadastro.Items;
            this.tileNavItem_Pedido.SubItems.AddRange(new DevExpress.XtraBars.Navigation.TileNavSubItem[] {
            this.tileNavSubItem_Pedido,
            this.tileNavSubItem_CamposExtraPedido,
            this.tileNavSubItem_Transportadora,
            this.tileNavSubItem_CondicaoPagamento,
            this.tileNavSubItem_FormaPagamento,
            this.tileNavSubItem_FaturamentoPedido,
            this.tileNavSubItem_TituloVencido,
            this.tileNavSubItem_ICMSST});
            // 
            // 
            // 
            this.tileNavItem_Pedido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement22.Text = "Pedidos";
            this.tileNavItem_Pedido.Tile.Elements.Add(tileItemElement22);
            this.tileNavItem_Pedido.Tile.Name = "tileBarItem5";
            // 
            // tileNavSubItem_Pedido
            // 
            this.tileNavSubItem_Pedido.Caption = "Pedido";
            this.tileNavSubItem_Pedido.Name = "tileNavSubItem_Pedido";
            // 
            // 
            // 
            this.tileNavSubItem_Pedido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement14.Text = "Pedido";
            this.tileNavSubItem_Pedido.Tile.Elements.Add(tileItemElement14);
            this.tileNavSubItem_Pedido.Tile.Name = "tileBarItem17";
            this.tileNavSubItem_Pedido.TileClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.tileNavSubItem_Pedido_Click);
            // 
            // tileNavSubItem_CamposExtraPedido
            // 
            this.tileNavSubItem_CamposExtraPedido.Caption = "Campos Extras do Pedido";
            this.tileNavSubItem_CamposExtraPedido.Name = "tileNavSubItem_CamposExtraPedido";
            // 
            // 
            // 
            this.tileNavSubItem_CamposExtraPedido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement15.Text = "Campos Extras do Pedido";
            this.tileNavSubItem_CamposExtraPedido.Tile.Elements.Add(tileItemElement15);
            this.tileNavSubItem_CamposExtraPedido.Tile.Name = "tileBarItem18";
            this.tileNavSubItem_CamposExtraPedido.TileClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.tileNavSubItem_PedidoCampoExtra_Click);
            // 
            // tileNavSubItem_Transportadora
            // 
            this.tileNavSubItem_Transportadora.Caption = "Transportadoras";
            this.tileNavSubItem_Transportadora.Name = "tileNavSubItem_Transportadora";
            // 
            // 
            // 
            this.tileNavSubItem_Transportadora.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement16.Text = "Transportadoras";
            this.tileNavSubItem_Transportadora.Tile.Elements.Add(tileItemElement16);
            this.tileNavSubItem_Transportadora.Tile.Name = "tileBarItem14";
            this.tileNavSubItem_Transportadora.TileClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.tileNavSubItem_Transportadora_Click);
            // 
            // tileNavSubItem_CondicaoPagamento
            // 
            this.tileNavSubItem_CondicaoPagamento.Caption = "Condições de Pagamento";
            this.tileNavSubItem_CondicaoPagamento.Name = "tileNavSubItem_CondicaoPagamento";
            // 
            // 
            // 
            this.tileNavSubItem_CondicaoPagamento.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement17.Text = "Condições de Pagamento";
            this.tileNavSubItem_CondicaoPagamento.Tile.Elements.Add(tileItemElement17);
            this.tileNavSubItem_CondicaoPagamento.Tile.Name = "tileBarItem15";
            this.tileNavSubItem_CondicaoPagamento.TileClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.tileNavSubItem_CondicaoPagamento_Click);
            // 
            // tileNavSubItem_FormaPagamento
            // 
            this.tileNavSubItem_FormaPagamento.Caption = "Formas de Pagamento";
            this.tileNavSubItem_FormaPagamento.Name = "tileNavSubItem_FormaPagamento";
            // 
            // 
            // 
            this.tileNavSubItem_FormaPagamento.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement18.Text = "Formas de Pagamento";
            this.tileNavSubItem_FormaPagamento.Tile.Elements.Add(tileItemElement18);
            this.tileNavSubItem_FormaPagamento.Tile.Name = "tileBarItem16";
            this.tileNavSubItem_FormaPagamento.TileClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.tileNavSubItem_FormaPagamento_Click);
            // 
            // tileNavSubItem_FaturamentoPedido
            // 
            this.tileNavSubItem_FaturamentoPedido.Caption = "Faturamento de Pedido";
            this.tileNavSubItem_FaturamentoPedido.Name = "tileNavSubItem_FaturamentoPedido";
            // 
            // 
            // 
            this.tileNavSubItem_FaturamentoPedido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement19.Text = "Faturamento de Pedido";
            this.tileNavSubItem_FaturamentoPedido.Tile.Elements.Add(tileItemElement19);
            this.tileNavSubItem_FaturamentoPedido.Tile.Name = "tileBarItem19";
            this.tileNavSubItem_FaturamentoPedido.TileClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.tileNavSubItem_PedidoFaturamento_Click);
            // 
            // tileNavSubItem_TituloVencido
            // 
            this.tileNavSubItem_TituloVencido.Caption = "Títulos Vencidos";
            this.tileNavSubItem_TituloVencido.Name = "tileNavSubItem_TituloVencido";
            // 
            // 
            // 
            this.tileNavSubItem_TituloVencido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement20.Text = "Títulos Vencidos";
            this.tileNavSubItem_TituloVencido.Tile.Elements.Add(tileItemElement20);
            this.tileNavSubItem_TituloVencido.Tile.Name = "tileBarItem20";
            this.tileNavSubItem_TituloVencido.TileClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.tileNavSubItem_TitulosVencidos_Click);
            // 
            // tileNavSubItem_ICMSST
            // 
            this.tileNavSubItem_ICMSST.Caption = "ICMS_ST";
            this.tileNavSubItem_ICMSST.Name = "tileNavSubItem_ICMSST";
            // 
            // 
            // 
            this.tileNavSubItem_ICMSST.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement21.Text = "ICMS-ST";
            this.tileNavSubItem_ICMSST.Tile.Elements.Add(tileItemElement21);
            this.tileNavSubItem_ICMSST.Tile.Name = "tileBarItem1";
            this.tileNavSubItem_ICMSST.TileClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.tileNavSubItem_ICMSST_Click);
            // 
            // tileNavCategoryBtn_Configuracao
            // 
            this.tileNavCategoryBtn_Configuracao.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.tileNavCategoryBtn_Configuracao.Caption = "Configurações";
            this.tileNavCategoryBtn_Configuracao.Glyph = ((System.Drawing.Image)(resources.GetObject("tileNavCategoryBtn_Configuracao.Glyph")));
            this.tileNavCategoryBtn_Configuracao.Items.AddRange(new DevExpress.XtraBars.Navigation.TileNavItem[] {
            this.tileNavItem_ConfigDB,
            this.tileNavItem_CadastroContas,
            this.tileNavItem_Rastreabilidade,
            this.tileNavItem_ConfigUsuarios,
            this.tileNavItem_Logoff});
            this.tileNavCategoryBtn_Configuracao.Name = "tileNavCategoryBtn_Configuracao";
            this.tileNavCategoryBtn_Configuracao.OwnerCollection = null;
            // 
            // 
            // 
            this.tileNavCategoryBtn_Configuracao.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement28.Image = ((System.Drawing.Image)(resources.GetObject("tileItemElement28.Image")));
            tileItemElement28.Text = "Configurações";
            this.tileNavCategoryBtn_Configuracao.Tile.Elements.Add(tileItemElement28);
            // 
            // tileNavItem_ConfigDB
            // 
            this.tileNavItem_ConfigDB.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.tileNavItem_ConfigDB.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("tileNavItem_ConfigDB.Appearance.Image")));
            this.tileNavItem_ConfigDB.Appearance.Options.UseImage = true;
            this.tileNavItem_ConfigDB.Caption = "Banco de Dados";
            this.tileNavItem_ConfigDB.Glyph = ((System.Drawing.Image)(resources.GetObject("tileNavItem_ConfigDB.Glyph")));
            this.tileNavItem_ConfigDB.Name = "tileNavItem_ConfigDB";
            this.tileNavItem_ConfigDB.OwnerCollection = this.tileNavCategoryBtn_Configuracao.Items;
            // 
            // 
            // 
            this.tileNavItem_ConfigDB.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement23.Image = ((System.Drawing.Image)(resources.GetObject("tileItemElement23.Image")));
            tileItemElement23.Text = "Banco de Dados";
            this.tileNavItem_ConfigDB.Tile.Elements.Add(tileItemElement23);
            this.tileNavItem_ConfigDB.Tile.Name = "tileBarItem1";
            this.tileNavItem_ConfigDB.TileClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.tileNavSubItem_ConfigDB_Click);
            // 
            // tileNavItem_CadastroContas
            // 
            this.tileNavItem_CadastroContas.Caption = "Cadastro de Contas";
            this.tileNavItem_CadastroContas.Name = "tileNavItem_CadastroContas";
            this.tileNavItem_CadastroContas.OwnerCollection = this.tileNavCategoryBtn_Configuracao.Items;
            // 
            // 
            // 
            this.tileNavItem_CadastroContas.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement24.Image = ((System.Drawing.Image)(resources.GetObject("tileItemElement24.Image")));
            tileItemElement24.Text = "Cadastro de Contas";
            this.tileNavItem_CadastroContas.Tile.Elements.Add(tileItemElement24);
            this.tileNavItem_CadastroContas.Tile.Name = "tileBarItem2";
            this.tileNavItem_CadastroContas.TileClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.tileNavSubItem_ConfigContaMeusPedidos_Click);
            // 
            // tileNavItem_Rastreabilidade
            // 
            this.tileNavItem_Rastreabilidade.Caption = "Rastreabilidade Entidades";
            this.tileNavItem_Rastreabilidade.Name = "tileNavItem_Rastreabilidade";
            this.tileNavItem_Rastreabilidade.OwnerCollection = this.tileNavCategoryBtn_Configuracao.Items;
            // 
            // 
            // 
            this.tileNavItem_Rastreabilidade.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement25.Image = ((System.Drawing.Image)(resources.GetObject("tileItemElement25.Image")));
            tileItemElement25.Text = "Rastreabilidade Entidades";
            this.tileNavItem_Rastreabilidade.Tile.Elements.Add(tileItemElement25);
            this.tileNavItem_Rastreabilidade.Tile.Name = "tileBarItem1";
            this.tileNavItem_Rastreabilidade.TileClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.tileNavSubItem_RastreabilidadeEntidades_Click);
            // 
            // tileNavItem_ConfigUsuarios
            // 
            this.tileNavItem_ConfigUsuarios.Caption = "Config. Empresas";
            this.tileNavItem_ConfigUsuarios.Name = "tileNavItem_ConfigUsuarios";
            this.tileNavItem_ConfigUsuarios.OwnerCollection = this.tileNavCategoryBtn_Configuracao.Items;
            // 
            // 
            // 
            this.tileNavItem_ConfigUsuarios.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement26.Image = ((System.Drawing.Image)(resources.GetObject("tileItemElement26.Image")));
            tileItemElement26.Text = "Empresas";
            this.tileNavItem_ConfigUsuarios.Tile.Elements.Add(tileItemElement26);
            this.tileNavItem_ConfigUsuarios.Tile.Name = "tileBarItem_ConfigUsuarios";
            this.tileNavItem_ConfigUsuarios.TileClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.tileNavSubItem_ConfigContaUsuarios_Click);
            // 
            // tileNavItem_Logoff
            // 
            this.tileNavItem_Logoff.Caption = "Logoff";
            this.tileNavItem_Logoff.Name = "tileNavItem_Logoff";
            this.tileNavItem_Logoff.OwnerCollection = this.tileNavCategoryBtn_Configuracao.Items;
            // 
            // 
            // 
            this.tileNavItem_Logoff.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement27.Image = ((System.Drawing.Image)(resources.GetObject("tileItemElement27.Image")));
            tileItemElement27.Text = "Sair";
            this.tileNavItem_Logoff.Tile.Elements.Add(tileItemElement27);
            this.tileNavItem_Logoff.Tile.Name = "tileBarItem1";
            this.tileNavItem_Logoff.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navButton_Logoff_ElementClick);
            // 
            // navButton_SyncTest
            // 
            this.navButton_SyncTest.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navButton_SyncTest.Appearance.Image = global::View_MeusPedidos.Properties.Resources.icon_ball_meus_pedidos_second;
            this.navButton_SyncTest.Appearance.Options.UseImage = true;
            this.navButton_SyncTest.Caption = null;
            this.navButton_SyncTest.Glyph = global::View_MeusPedidos.Properties.Resources.meus_pedidos_32;
            this.navButton_SyncTest.Name = "navButton_SyncTest";
            this.navButton_SyncTest.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navButton_SyncTest_ElementClick);
            // 
            // xtraTabbedMdiManager_Dash
            // 
            this.xtraTabbedMdiManager_Dash.MdiParent = this;
            // 
            // FormDash
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 562);
            this.Controls.Add(this.tileNavPane_dash);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "FormDash";
            this.Text = "Partners - Meus Pedidos";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormDash_FormClosed);
            this.Shown += new System.EventHandler(this.FormDash_Shown);
            this.Resize += new System.EventHandler(this.FormDash_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.tileNavPane_dash)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager_Dash)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Navigation.TileNavPane tileNavPane_dash;
        private DevExpress.XtraBars.Navigation.NavButton navButton_Home;
        private DevExpress.XtraBars.Navigation.TileNavCategory tileNavCategoryBtn_Cadastro;
        private DevExpress.XtraBars.Navigation.TileNavItem tileNavItem_Cliente;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Cliente;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CondicaoPagamentoCliente;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CategoriasCliente;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_ClienteTabelaPreco;
        private DevExpress.XtraBars.Navigation.TileNavItem tileNavItem_Produto;
        private DevExpress.XtraBars.Navigation.TileNavItem tileNavItem_Pedido;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Usuarios;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Produto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_ImagensProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TabelaProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TabelaPrecoProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CategoriaProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Transportadora;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CondicaoPagamento;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_FormaPagamento;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Pedido;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CamposExtraPedido;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_FaturamentoPedido;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TituloVencido;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager_Dash;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager_FormDash;
        private DevExpress.XtraBars.Navigation.TileNavCategory tileNavCategoryBtn_Configuracao;
        private DevExpress.XtraBars.Navigation.TileNavItem tileNavItem_ConfigDB;
        private DevExpress.XtraBars.Navigation.TileNavItem tileNavItem_CadastroContas;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_ICMSST;
        private DevExpress.XtraBars.Navigation.NavButton navButton_SyncTest;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_rl;
        private DevExpress.XtraBars.Navigation.TileNavItem tileNavItem_Rastreabilidade;
        private DevExpress.XtraBars.Navigation.TileNavItem tileNavItem_ConfigUsuarios;
        private DevExpress.XtraBars.Navigation.TileNavItem tileNavItem_Logoff;
    }
}


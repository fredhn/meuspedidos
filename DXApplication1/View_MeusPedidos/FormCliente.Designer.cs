﻿namespace View_MeusPedidos
{
    partial class FormCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement10 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement11 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement12 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement13 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement14 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement15 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement16 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement17 = new DevExpress.XtraEditors.TileItemElement();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCliente));
            this.tileNavSubItem_Cliente = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CondicaoPagamentoCliente = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CategoriasCliente = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TabelaPreco = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Usuarios = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Produto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_ImagensProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TabelaProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TabelaPrecoProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CategoriaProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Pedido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CamposExtraPedido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Transportadora = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CondicaoPagamento = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_FormaPagamento = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_FaturamentoPedido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TituloVencido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.splashScreenManager_Clientes = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::View_MeusPedidos.WaitForm1), true, true);
            this.tabNavigationPage_RegisterClientes = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.panelControl_RegisterClientes = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl_Clientes = new DevExpress.XtraLayout.LayoutControl();
            this.gridControl_CLIEmail = new DevExpress.XtraGrid.GridControl();
            this.gridView_CLIEmails = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.barManager_ContasMP = new DevExpress.XtraBars.BarManager(this.components);
            this.bar_Acoes = new DevExpress.XtraBars.Bar();
            this.barButtonItem_Add = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem_Edit = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem_Del = new DevExpress.XtraBars.BarButtonItem();
            this.bar_Acoes2 = new DevExpress.XtraBars.Bar();
            this.barButtonItem_Gravar = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem_Cancelar = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.gridControl_CLITelefones = new DevExpress.XtraGrid.GridControl();
            this.gridView_CLITelefones = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.textEdit_CLIRazaoSocial = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_CLINomeFantasia = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_CLICNPJ = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_CLIInscricaoEstadual = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_CLISuframa = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_CLIRua = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_CLIComplemento = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_CLICEP = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_CLIBairro = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_CLICidade = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_CLINomeExcFiscal = new DevExpress.XtraEditors.TextEdit();
            this.dateEdit_CLIUltimaAlteracao = new DevExpress.XtraEditors.DateEdit();
            this.textEdit_CLIExcluido = new DevExpress.XtraEditors.ToggleSwitch();
            this.memoEdit_CLIObservacao = new DevExpress.XtraEditors.MemoEdit();
            this.textEdit_CLITelefoneNumero = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton_CLITelefoneAdd = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit_CLIEmail = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton_CLIEmailAdd = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit_CLITipo = new DevExpress.XtraEditors.LookUpEdit();
            this.comboBoxEdit_CLIEstado = new DevExpress.XtraEditors.LookUpEdit();
            this.comboBoxEdit_CLIEmailTipo = new DevExpress.XtraEditors.LookUpEdit();
            this.comboBoxEdit_CLITelefoneTipo = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup_RegisterCliente = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleLabelItem_RegisterClientes = new DevExpress.XtraLayout.SimpleLabelItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup_Endereco = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem_CLIRua = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_CLIComplemento = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_CLICep = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_Bairro = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_Cidade = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_CLIEstado = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem_CLIUltimaAlteracao = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_CLIExcluido = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem_CLIObservacao = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup_CLIDados = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup_CLIContato = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup_CLIContatos = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup_CLITelefone = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem_CLITelefoneNumero = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator1 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_CLITelefoneTipo = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup_CLIEmail = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem_CLIEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator4 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlItem_CLIEmailTipo = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup_CLIDados = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem_CLIRazaoSocial = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_CLINomeFantasia = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_CLITipo = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_CLICnpj = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_CLIInscricaoEstadual = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_CLISuframa = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_CLINomeExcFiscal = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.tabNavigationPage_DataGridClientes = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.gridControl_Clientes = new DevExpress.XtraGrid.GridControl();
            this.clienteBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.meusPedidosDataSet = new View_MeusPedidos.MeusPedidosDataSet();
            this.gridView_Clientes = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colrazao_social1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colnome_fantasia1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coltipo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcnpj = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colinscricao_estadual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colsuframa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colrua = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcomplemento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcep = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colbairro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcidade = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colestado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colobservacao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colnome_excecao_fiscal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colultima_alteracao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colexcluido = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tabPane_ConfigClientes = new DevExpress.XtraBars.Navigation.TabPane();
            this.tabPane1 = new DevExpress.XtraBars.Navigation.TabPane();
            this.tabNavigationPage1 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.tabNavigationPage2 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit8 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit9 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit10 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit11 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit12 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit13 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit14 = new DevExpress.XtraEditors.TextEdit();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.toggleSwitch1 = new DevExpress.XtraEditors.ToggleSwitch();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleLabelItem1 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem16 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup3 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup4 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.simpleSeparator2 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator3 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlGroup_CLITelefone1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.colid1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colrazao_social = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colnome_fantasia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.clienteTableAdapter = new View_MeusPedidos.MeusPedidosDataSetTableAdapters.ClienteTableAdapter();
            this.tabNavigationPage_RegisterClientes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl_RegisterClientes)).BeginInit();
            this.panelControl_RegisterClientes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_Clientes)).BeginInit();
            this.layoutControl_Clientes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_CLIEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_CLIEmails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager_ContasMP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_CLITelefones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_CLITelefones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CLIRazaoSocial.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CLINomeFantasia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CLICNPJ.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CLIInscricaoEstadual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CLISuframa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CLIRua.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CLIComplemento.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CLICEP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CLIBairro.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CLICidade.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CLINomeExcFiscal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_CLIUltimaAlteracao.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_CLIUltimaAlteracao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CLIExcluido.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_CLIObservacao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CLITelefoneNumero.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CLIEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CLITipo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit_CLIEstado.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit_CLIEmailTipo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit_CLITelefoneTipo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_RegisterCliente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem_RegisterClientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_Endereco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLIRua)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLIComplemento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLICep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_Bairro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_Cidade)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLIEstado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLIUltimaAlteracao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLIExcluido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLIObservacao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup_CLIDados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CLIContato)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup_CLIContatos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CLITelefone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLITelefoneNumero)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLITelefoneTipo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CLIEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLIEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLIEmailTipo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CLIDados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLIRazaoSocial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLINomeFantasia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLITipo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLICnpj)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLIInscricaoEstadual)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLISuframa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLINomeExcFiscal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            this.tabNavigationPage_DataGridClientes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_Clientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clienteBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meusPedidosDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_Clientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane_ConfigClientes)).BeginInit();
            this.tabPane_ConfigClientes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).BeginInit();
            this.tabPane1.SuspendLayout();
            this.tabNavigationPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toggleSwitch1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CLITelefone1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            this.SuspendLayout();
            // 
            // tileNavSubItem_Cliente
            // 
            this.tileNavSubItem_Cliente.Caption = "Cliente";
            this.tileNavSubItem_Cliente.Name = "tileNavSubItem_Cliente";
            // 
            // 
            // 
            this.tileNavSubItem_Cliente.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement1.Text = "Cliente";
            this.tileNavSubItem_Cliente.Tile.Elements.Add(tileItemElement1);
            this.tileNavSubItem_Cliente.Tile.Name = "tileBarItem3";
            // 
            // tileNavSubItem_CondicaoPagamentoCliente
            // 
            this.tileNavSubItem_CondicaoPagamentoCliente.Caption = "Condições de Pagamento por Cliente";
            this.tileNavSubItem_CondicaoPagamentoCliente.Name = "tileNavSubItem_CondicaoPagamentoCliente";
            // 
            // 
            // 
            this.tileNavSubItem_CondicaoPagamentoCliente.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement2.Text = "Condições de Pagamento por Cliente";
            this.tileNavSubItem_CondicaoPagamentoCliente.Tile.Elements.Add(tileItemElement2);
            this.tileNavSubItem_CondicaoPagamentoCliente.Tile.Name = "tileBarItem4";
            // 
            // tileNavSubItem_CategoriasCliente
            // 
            this.tileNavSubItem_CategoriasCliente.Caption = "Categorias por Cliente";
            this.tileNavSubItem_CategoriasCliente.Name = "tileNavSubItem_CategoriasCliente";
            // 
            // 
            // 
            this.tileNavSubItem_CategoriasCliente.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement3.Text = "Categorias por Cliente";
            this.tileNavSubItem_CategoriasCliente.Tile.Elements.Add(tileItemElement3);
            this.tileNavSubItem_CategoriasCliente.Tile.Name = "tileBarItem6";
            // 
            // tileNavSubItem_TabelaPreco
            // 
            this.tileNavSubItem_TabelaPreco.Caption = "Tabelas de Preço por Cliente";
            this.tileNavSubItem_TabelaPreco.Name = "tileNavSubItem_TabelaPreco";
            // 
            // 
            // 
            this.tileNavSubItem_TabelaPreco.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement4.Text = "Tabelas de Preço por Cliente";
            this.tileNavSubItem_TabelaPreco.Tile.Elements.Add(tileItemElement4);
            this.tileNavSubItem_TabelaPreco.Tile.Name = "tileBarItem7";
            // 
            // tileNavSubItem_Usuarios
            // 
            this.tileNavSubItem_Usuarios.Caption = "Usuários (Vendedores)";
            this.tileNavSubItem_Usuarios.Name = "tileNavSubItem_Usuarios";
            // 
            // 
            // 
            this.tileNavSubItem_Usuarios.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement5.Text = "Usuários (Vendedores)";
            this.tileNavSubItem_Usuarios.Tile.Elements.Add(tileItemElement5);
            this.tileNavSubItem_Usuarios.Tile.Name = "tileBarItem8";
            // 
            // tileNavSubItem_Produto
            // 
            this.tileNavSubItem_Produto.Caption = "Produto";
            this.tileNavSubItem_Produto.Name = "tileNavSubItem_Produto";
            // 
            // 
            // 
            this.tileNavSubItem_Produto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement6.Text = "Produto";
            this.tileNavSubItem_Produto.Tile.Elements.Add(tileItemElement6);
            this.tileNavSubItem_Produto.Tile.Name = "tileBarItem9";
            // 
            // tileNavSubItem_ImagensProduto
            // 
            this.tileNavSubItem_ImagensProduto.Caption = "Imagens do Produto";
            this.tileNavSubItem_ImagensProduto.Name = "tileNavSubItem_ImagensProduto";
            // 
            // 
            // 
            this.tileNavSubItem_ImagensProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement7.Text = "Imagens do Produto";
            this.tileNavSubItem_ImagensProduto.Tile.Elements.Add(tileItemElement7);
            this.tileNavSubItem_ImagensProduto.Tile.Name = "tileBarItem10";
            // 
            // tileNavSubItem_TabelaProduto
            // 
            this.tileNavSubItem_TabelaProduto.Caption = "Tabela de Preço";
            this.tileNavSubItem_TabelaProduto.Name = "tileNavSubItem_TabelaProduto";
            // 
            // 
            // 
            this.tileNavSubItem_TabelaProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement8.Text = "Tabelas de Preço";
            this.tileNavSubItem_TabelaProduto.Tile.Elements.Add(tileItemElement8);
            this.tileNavSubItem_TabelaProduto.Tile.Name = "tileBarItem11";
            // 
            // tileNavSubItem_TabelaPrecoProduto
            // 
            this.tileNavSubItem_TabelaPrecoProduto.Caption = "Tabelas de Preço por Produto";
            this.tileNavSubItem_TabelaPrecoProduto.Name = "tileNavSubItem_TabelaPrecoProduto";
            // 
            // 
            // 
            this.tileNavSubItem_TabelaPrecoProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement9.Text = "Tabelas de Preço por Produto";
            this.tileNavSubItem_TabelaPrecoProduto.Tile.Elements.Add(tileItemElement9);
            this.tileNavSubItem_TabelaPrecoProduto.Tile.Name = "tileBarItem12";
            // 
            // tileNavSubItem_CategoriaProduto
            // 
            this.tileNavSubItem_CategoriaProduto.Caption = "Categorias de Produtos";
            this.tileNavSubItem_CategoriaProduto.Name = "tileNavSubItem_CategoriaProduto";
            // 
            // 
            // 
            this.tileNavSubItem_CategoriaProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement10.Text = "Categorias de Produtos";
            this.tileNavSubItem_CategoriaProduto.Tile.Elements.Add(tileItemElement10);
            this.tileNavSubItem_CategoriaProduto.Tile.Name = "tileBarItem13";
            // 
            // tileNavSubItem_Pedido
            // 
            this.tileNavSubItem_Pedido.Caption = "Pedido";
            this.tileNavSubItem_Pedido.Name = "tileNavSubItem_Pedido";
            // 
            // 
            // 
            this.tileNavSubItem_Pedido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement11.Text = "Pedido";
            this.tileNavSubItem_Pedido.Tile.Elements.Add(tileItemElement11);
            this.tileNavSubItem_Pedido.Tile.Name = "tileBarItem17";
            // 
            // tileNavSubItem_CamposExtraPedido
            // 
            this.tileNavSubItem_CamposExtraPedido.Caption = "Campos Extras do Pedido";
            this.tileNavSubItem_CamposExtraPedido.Name = "tileNavSubItem_CamposExtraPedido";
            // 
            // 
            // 
            this.tileNavSubItem_CamposExtraPedido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement12.Text = "Campos Extras do Pedido";
            this.tileNavSubItem_CamposExtraPedido.Tile.Elements.Add(tileItemElement12);
            this.tileNavSubItem_CamposExtraPedido.Tile.Name = "tileBarItem18";
            // 
            // tileNavSubItem_Transportadora
            // 
            this.tileNavSubItem_Transportadora.Caption = "Transportadoras";
            this.tileNavSubItem_Transportadora.Name = "tileNavSubItem_Transportadora";
            // 
            // 
            // 
            this.tileNavSubItem_Transportadora.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement13.Text = "Transportadoras";
            this.tileNavSubItem_Transportadora.Tile.Elements.Add(tileItemElement13);
            this.tileNavSubItem_Transportadora.Tile.Name = "tileBarItem14";
            // 
            // tileNavSubItem_CondicaoPagamento
            // 
            this.tileNavSubItem_CondicaoPagamento.Caption = "Condições de Pagamento";
            this.tileNavSubItem_CondicaoPagamento.Name = "tileNavSubItem_CondicaoPagamento";
            // 
            // 
            // 
            this.tileNavSubItem_CondicaoPagamento.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement14.Text = "Condições de Pagamento";
            this.tileNavSubItem_CondicaoPagamento.Tile.Elements.Add(tileItemElement14);
            this.tileNavSubItem_CondicaoPagamento.Tile.Name = "tileBarItem15";
            // 
            // tileNavSubItem_FormaPagamento
            // 
            this.tileNavSubItem_FormaPagamento.Caption = "Formas de Pagamento";
            this.tileNavSubItem_FormaPagamento.Name = "tileNavSubItem_FormaPagamento";
            // 
            // 
            // 
            this.tileNavSubItem_FormaPagamento.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement15.Text = "Formas de Pagamento";
            this.tileNavSubItem_FormaPagamento.Tile.Elements.Add(tileItemElement15);
            this.tileNavSubItem_FormaPagamento.Tile.Name = "tileBarItem16";
            // 
            // tileNavSubItem_FaturamentoPedido
            // 
            this.tileNavSubItem_FaturamentoPedido.Caption = "Faturamento de Pedido";
            this.tileNavSubItem_FaturamentoPedido.Name = "tileNavSubItem_FaturamentoPedido";
            // 
            // 
            // 
            this.tileNavSubItem_FaturamentoPedido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement16.Text = "Faturamento de Pedido";
            this.tileNavSubItem_FaturamentoPedido.Tile.Elements.Add(tileItemElement16);
            this.tileNavSubItem_FaturamentoPedido.Tile.Name = "tileBarItem19";
            // 
            // tileNavSubItem_TituloVencido
            // 
            this.tileNavSubItem_TituloVencido.Caption = "Títulos Vencidos";
            this.tileNavSubItem_TituloVencido.Name = "tileNavSubItem_TituloVencido";
            // 
            // 
            // 
            this.tileNavSubItem_TituloVencido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement17.Text = "Títulos Vencidos";
            this.tileNavSubItem_TituloVencido.Tile.Elements.Add(tileItemElement17);
            this.tileNavSubItem_TituloVencido.Tile.Name = "tileBarItem20";
            // 
            // splashScreenManager_Clientes
            // 
            this.splashScreenManager_Clientes.ClosingDelay = 500;
            // 
            // tabNavigationPage_RegisterClientes
            // 
            this.tabNavigationPage_RegisterClientes.Caption = "Cadastro";
            this.tabNavigationPage_RegisterClientes.Controls.Add(this.panelControl_RegisterClientes);
            this.tabNavigationPage_RegisterClientes.Image = ((System.Drawing.Image)(resources.GetObject("tabNavigationPage_RegisterClientes.Image")));
            this.tabNavigationPage_RegisterClientes.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage_RegisterClientes.Name = "tabNavigationPage_RegisterClientes";
            this.tabNavigationPage_RegisterClientes.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage_RegisterClientes.Size = new System.Drawing.Size(1246, 404);
            // 
            // panelControl_RegisterClientes
            // 
            this.panelControl_RegisterClientes.Controls.Add(this.layoutControl_Clientes);
            this.panelControl_RegisterClientes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl_RegisterClientes.Location = new System.Drawing.Point(0, 0);
            this.panelControl_RegisterClientes.Name = "panelControl_RegisterClientes";
            this.panelControl_RegisterClientes.Size = new System.Drawing.Size(1246, 404);
            this.panelControl_RegisterClientes.TabIndex = 0;
            // 
            // layoutControl_Clientes
            // 
            this.layoutControl_Clientes.Controls.Add(this.gridControl_CLIEmail);
            this.layoutControl_Clientes.Controls.Add(this.gridControl_CLITelefones);
            this.layoutControl_Clientes.Controls.Add(this.textEdit_CLIRazaoSocial);
            this.layoutControl_Clientes.Controls.Add(this.textEdit_CLINomeFantasia);
            this.layoutControl_Clientes.Controls.Add(this.textEdit_CLICNPJ);
            this.layoutControl_Clientes.Controls.Add(this.textEdit_CLIInscricaoEstadual);
            this.layoutControl_Clientes.Controls.Add(this.textEdit_CLISuframa);
            this.layoutControl_Clientes.Controls.Add(this.textEdit_CLIRua);
            this.layoutControl_Clientes.Controls.Add(this.textEdit_CLIComplemento);
            this.layoutControl_Clientes.Controls.Add(this.textEdit_CLICEP);
            this.layoutControl_Clientes.Controls.Add(this.textEdit_CLIBairro);
            this.layoutControl_Clientes.Controls.Add(this.textEdit_CLICidade);
            this.layoutControl_Clientes.Controls.Add(this.textEdit_CLINomeExcFiscal);
            this.layoutControl_Clientes.Controls.Add(this.dateEdit_CLIUltimaAlteracao);
            this.layoutControl_Clientes.Controls.Add(this.textEdit_CLIExcluido);
            this.layoutControl_Clientes.Controls.Add(this.memoEdit_CLIObservacao);
            this.layoutControl_Clientes.Controls.Add(this.textEdit_CLITelefoneNumero);
            this.layoutControl_Clientes.Controls.Add(this.simpleButton_CLITelefoneAdd);
            this.layoutControl_Clientes.Controls.Add(this.textEdit_CLIEmail);
            this.layoutControl_Clientes.Controls.Add(this.simpleButton_CLIEmailAdd);
            this.layoutControl_Clientes.Controls.Add(this.textEdit_CLITipo);
            this.layoutControl_Clientes.Controls.Add(this.comboBoxEdit_CLIEstado);
            this.layoutControl_Clientes.Controls.Add(this.comboBoxEdit_CLIEmailTipo);
            this.layoutControl_Clientes.Controls.Add(this.comboBoxEdit_CLITelefoneTipo);
            this.layoutControl_Clientes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl_Clientes.Location = new System.Drawing.Point(2, 2);
            this.layoutControl_Clientes.Name = "layoutControl_Clientes";
            this.layoutControl_Clientes.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(124, 125, 450, 400);
            this.layoutControl_Clientes.Root = this.layoutControlGroup_RegisterCliente;
            this.layoutControl_Clientes.Size = new System.Drawing.Size(1242, 400);
            this.layoutControl_Clientes.TabIndex = 0;
            this.layoutControl_Clientes.Text = "layoutControl1";
            // 
            // gridControl_CLIEmail
            // 
            this.gridControl_CLIEmail.Location = new System.Drawing.Point(46, 143);
            this.gridControl_CLIEmail.MainView = this.gridView_CLIEmails;
            this.gridControl_CLIEmail.MenuManager = this.barManager_ContasMP;
            this.gridControl_CLIEmail.Name = "gridControl_CLIEmail";
            this.gridControl_CLIEmail.Size = new System.Drawing.Size(1133, 92);
            this.gridControl_CLIEmail.TabIndex = 23;
            this.gridControl_CLIEmail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView_CLIEmails});
            this.gridControl_CLIEmail.ProcessGridKey += new System.Windows.Forms.KeyEventHandler(this.gridControl_CLIEmail_ProcessGridKey);
            // 
            // gridView_CLIEmails
            // 
            this.gridView_CLIEmails.GridControl = this.gridControl_CLIEmail;
            this.gridView_CLIEmails.Name = "gridView_CLIEmails";
            this.gridView_CLIEmails.OptionsView.ShowGroupPanel = false;
            // 
            // barManager_ContasMP
            // 
            this.barManager_ContasMP.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar_Acoes,
            this.bar_Acoes2});
            this.barManager_ContasMP.DockControls.Add(this.barDockControlTop);
            this.barManager_ContasMP.DockControls.Add(this.barDockControlBottom);
            this.barManager_ContasMP.DockControls.Add(this.barDockControlLeft);
            this.barManager_ContasMP.DockControls.Add(this.barDockControlRight);
            this.barManager_ContasMP.Form = this;
            this.barManager_ContasMP.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem_Add,
            this.barButtonItem_Edit,
            this.barButtonItem_Del,
            this.barButtonItem_Gravar,
            this.barButtonItem_Cancelar});
            this.barManager_ContasMP.MaxItemId = 5;
            // 
            // bar_Acoes
            // 
            this.bar_Acoes.BarName = "Tools";
            this.bar_Acoes.DockCol = 0;
            this.bar_Acoes.DockRow = 0;
            this.bar_Acoes.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar_Acoes.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Add),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Edit),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Del)});
            this.bar_Acoes.OptionsBar.UseWholeRow = true;
            this.bar_Acoes.Text = "Tools";
            // 
            // barButtonItem_Add
            // 
            this.barButtonItem_Add.Caption = "Adicionar";
            this.barButtonItem_Add.Id = 0;
            this.barButtonItem_Add.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Add.ImageOptions.Image")));
            this.barButtonItem_Add.Name = "barButtonItem_Add";
            this.barButtonItem_Add.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Add.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Add_ItemClick);
            // 
            // barButtonItem_Edit
            // 
            this.barButtonItem_Edit.Caption = "Editar";
            this.barButtonItem_Edit.Id = 1;
            this.barButtonItem_Edit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Edit.ImageOptions.Image")));
            this.barButtonItem_Edit.Name = "barButtonItem_Edit";
            this.barButtonItem_Edit.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Edit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Edit_ItemClick);
            // 
            // barButtonItem_Del
            // 
            this.barButtonItem_Del.Caption = "Excluir";
            this.barButtonItem_Del.Id = 2;
            this.barButtonItem_Del.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Del.ImageOptions.Image")));
            this.barButtonItem_Del.Name = "barButtonItem_Del";
            this.barButtonItem_Del.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Del.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Del_ItemClick);
            // 
            // bar_Acoes2
            // 
            this.bar_Acoes2.BarName = "Tools_GravarCancelar";
            this.bar_Acoes2.DockCol = 0;
            this.bar_Acoes2.DockRow = 1;
            this.bar_Acoes2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar_Acoes2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Gravar),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Cancelar)});
            this.bar_Acoes2.OptionsBar.AllowRename = true;
            this.bar_Acoes2.OptionsBar.UseWholeRow = true;
            this.bar_Acoes2.Text = "Tools_GravarCancelar";
            // 
            // barButtonItem_Gravar
            // 
            this.barButtonItem_Gravar.Caption = "Gravar";
            this.barButtonItem_Gravar.Id = 3;
            this.barButtonItem_Gravar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Gravar.ImageOptions.Image")));
            this.barButtonItem_Gravar.Name = "barButtonItem_Gravar";
            this.barButtonItem_Gravar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Gravar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Gravar_ItemClick);
            // 
            // barButtonItem_Cancelar
            // 
            this.barButtonItem_Cancelar.Caption = "Cancelar";
            this.barButtonItem_Cancelar.Id = 4;
            this.barButtonItem_Cancelar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Cancelar.ImageOptions.Image")));
            this.barButtonItem_Cancelar.Name = "barButtonItem_Cancelar";
            this.barButtonItem_Cancelar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Cancelar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Cancelar_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager_ContasMP;
            this.barDockControlTop.Size = new System.Drawing.Size(1264, 94);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 562);
            this.barDockControlBottom.Manager = this.barManager_ContasMP;
            this.barDockControlBottom.Size = new System.Drawing.Size(1264, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 94);
            this.barDockControlLeft.Manager = this.barManager_ContasMP;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 468);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1264, 94);
            this.barDockControlRight.Manager = this.barManager_ContasMP;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 468);
            // 
            // gridControl_CLITelefones
            // 
            this.gridControl_CLITelefones.Location = new System.Drawing.Point(46, 143);
            this.gridControl_CLITelefones.MainView = this.gridView_CLITelefones;
            this.gridControl_CLITelefones.MenuManager = this.barManager_ContasMP;
            this.gridControl_CLITelefones.Name = "gridControl_CLITelefones";
            this.gridControl_CLITelefones.Size = new System.Drawing.Size(1133, 92);
            this.gridControl_CLITelefones.TabIndex = 22;
            this.gridControl_CLITelefones.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView_CLITelefones});
            this.gridControl_CLITelefones.ProcessGridKey += new System.Windows.Forms.KeyEventHandler(this.gridControl_CLITelefones_ProcessGridKey);
            // 
            // gridView_CLITelefones
            // 
            this.gridView_CLITelefones.GridControl = this.gridControl_CLITelefones;
            this.gridView_CLITelefones.Name = "gridView_CLITelefones";
            this.gridView_CLITelefones.OptionsView.ShowGroupPanel = false;
            // 
            // textEdit_CLIRazaoSocial
            // 
            this.textEdit_CLIRazaoSocial.Location = new System.Drawing.Point(136, 83);
            this.textEdit_CLIRazaoSocial.MenuManager = this.barManager_ContasMP;
            this.textEdit_CLIRazaoSocial.Name = "textEdit_CLIRazaoSocial";
            this.textEdit_CLIRazaoSocial.Size = new System.Drawing.Size(1055, 20);
            this.textEdit_CLIRazaoSocial.StyleController = this.layoutControl_Clientes;
            this.textEdit_CLIRazaoSocial.TabIndex = 4;
            // 
            // textEdit_CLINomeFantasia
            // 
            this.textEdit_CLINomeFantasia.Location = new System.Drawing.Point(136, 107);
            this.textEdit_CLINomeFantasia.MenuManager = this.barManager_ContasMP;
            this.textEdit_CLINomeFantasia.Name = "textEdit_CLINomeFantasia";
            this.textEdit_CLINomeFantasia.Size = new System.Drawing.Size(1055, 20);
            this.textEdit_CLINomeFantasia.StyleController = this.layoutControl_Clientes;
            this.textEdit_CLINomeFantasia.TabIndex = 5;
            // 
            // textEdit_CLICNPJ
            // 
            this.textEdit_CLICNPJ.Location = new System.Drawing.Point(136, 155);
            this.textEdit_CLICNPJ.MenuManager = this.barManager_ContasMP;
            this.textEdit_CLICNPJ.Name = "textEdit_CLICNPJ";
            this.textEdit_CLICNPJ.Properties.MaxLength = 14;
            this.textEdit_CLICNPJ.Size = new System.Drawing.Size(1055, 20);
            this.textEdit_CLICNPJ.StyleController = this.layoutControl_Clientes;
            this.textEdit_CLICNPJ.TabIndex = 7;
            this.textEdit_CLICNPJ.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.textEdit_CLICNPJ_EditValueChanging);
            // 
            // textEdit_CLIInscricaoEstadual
            // 
            this.textEdit_CLIInscricaoEstadual.Location = new System.Drawing.Point(136, 179);
            this.textEdit_CLIInscricaoEstadual.MenuManager = this.barManager_ContasMP;
            this.textEdit_CLIInscricaoEstadual.Name = "textEdit_CLIInscricaoEstadual";
            this.textEdit_CLIInscricaoEstadual.Size = new System.Drawing.Size(1055, 20);
            this.textEdit_CLIInscricaoEstadual.StyleController = this.layoutControl_Clientes;
            this.textEdit_CLIInscricaoEstadual.TabIndex = 8;
            // 
            // textEdit_CLISuframa
            // 
            this.textEdit_CLISuframa.Location = new System.Drawing.Point(136, 203);
            this.textEdit_CLISuframa.MenuManager = this.barManager_ContasMP;
            this.textEdit_CLISuframa.Name = "textEdit_CLISuframa";
            this.textEdit_CLISuframa.Size = new System.Drawing.Size(1055, 20);
            this.textEdit_CLISuframa.StyleController = this.layoutControl_Clientes;
            this.textEdit_CLISuframa.TabIndex = 9;
            // 
            // textEdit_CLIRua
            // 
            this.textEdit_CLIRua.Location = new System.Drawing.Point(136, 303);
            this.textEdit_CLIRua.MenuManager = this.barManager_ContasMP;
            this.textEdit_CLIRua.Name = "textEdit_CLIRua";
            this.textEdit_CLIRua.Size = new System.Drawing.Size(1055, 20);
            this.textEdit_CLIRua.StyleController = this.layoutControl_Clientes;
            this.textEdit_CLIRua.TabIndex = 10;
            // 
            // textEdit_CLIComplemento
            // 
            this.textEdit_CLIComplemento.Location = new System.Drawing.Point(136, 327);
            this.textEdit_CLIComplemento.MenuManager = this.barManager_ContasMP;
            this.textEdit_CLIComplemento.Name = "textEdit_CLIComplemento";
            this.textEdit_CLIComplemento.Size = new System.Drawing.Size(1055, 20);
            this.textEdit_CLIComplemento.StyleController = this.layoutControl_Clientes;
            this.textEdit_CLIComplemento.TabIndex = 11;
            // 
            // textEdit_CLICEP
            // 
            this.textEdit_CLICEP.Location = new System.Drawing.Point(136, 351);
            this.textEdit_CLICEP.MenuManager = this.barManager_ContasMP;
            this.textEdit_CLICEP.Name = "textEdit_CLICEP";
            this.textEdit_CLICEP.Properties.Mask.EditMask = ".{8,}";
            this.textEdit_CLICEP.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.textEdit_CLICEP.Properties.MaxLength = 8;
            this.textEdit_CLICEP.Size = new System.Drawing.Size(1055, 20);
            this.textEdit_CLICEP.StyleController = this.layoutControl_Clientes;
            this.textEdit_CLICEP.TabIndex = 12;
            this.textEdit_CLICEP.Validating += new System.ComponentModel.CancelEventHandler(this.textEdit_CLICEP_Validating);
            // 
            // textEdit_CLIBairro
            // 
            this.textEdit_CLIBairro.Location = new System.Drawing.Point(136, 375);
            this.textEdit_CLIBairro.MenuManager = this.barManager_ContasMP;
            this.textEdit_CLIBairro.Name = "textEdit_CLIBairro";
            this.textEdit_CLIBairro.Size = new System.Drawing.Size(1055, 20);
            this.textEdit_CLIBairro.StyleController = this.layoutControl_Clientes;
            this.textEdit_CLIBairro.TabIndex = 13;
            // 
            // textEdit_CLICidade
            // 
            this.textEdit_CLICidade.Location = new System.Drawing.Point(136, 399);
            this.textEdit_CLICidade.MenuManager = this.barManager_ContasMP;
            this.textEdit_CLICidade.Name = "textEdit_CLICidade";
            this.textEdit_CLICidade.Size = new System.Drawing.Size(1055, 20);
            this.textEdit_CLICidade.StyleController = this.layoutControl_Clientes;
            this.textEdit_CLICidade.TabIndex = 14;
            // 
            // textEdit_CLINomeExcFiscal
            // 
            this.textEdit_CLINomeExcFiscal.Location = new System.Drawing.Point(136, 227);
            this.textEdit_CLINomeExcFiscal.MenuManager = this.barManager_ContasMP;
            this.textEdit_CLINomeExcFiscal.Name = "textEdit_CLINomeExcFiscal";
            this.textEdit_CLINomeExcFiscal.Size = new System.Drawing.Size(1055, 20);
            this.textEdit_CLINomeExcFiscal.StyleController = this.layoutControl_Clientes;
            this.textEdit_CLINomeExcFiscal.TabIndex = 16;
            // 
            // dateEdit_CLIUltimaAlteracao
            // 
            this.dateEdit_CLIUltimaAlteracao.EditValue = null;
            this.dateEdit_CLIUltimaAlteracao.Location = new System.Drawing.Point(784, 499);
            this.dateEdit_CLIUltimaAlteracao.MenuManager = this.barManager_ContasMP;
            this.dateEdit_CLIUltimaAlteracao.Name = "dateEdit_CLIUltimaAlteracao";
            this.dateEdit_CLIUltimaAlteracao.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit_CLIUltimaAlteracao.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit_CLIUltimaAlteracao.Properties.ReadOnly = true;
            this.dateEdit_CLIUltimaAlteracao.Size = new System.Drawing.Size(407, 20);
            this.dateEdit_CLIUltimaAlteracao.StyleController = this.layoutControl_Clientes;
            this.dateEdit_CLIUltimaAlteracao.TabIndex = 17;
            // 
            // textEdit_CLIExcluido
            // 
            this.textEdit_CLIExcluido.Location = new System.Drawing.Point(784, 523);
            this.textEdit_CLIExcluido.Name = "textEdit_CLIExcluido";
            this.textEdit_CLIExcluido.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.textEdit_CLIExcluido.Properties.OffText = "Não";
            this.textEdit_CLIExcluido.Properties.OnText = "Sim";
            this.textEdit_CLIExcluido.Size = new System.Drawing.Size(407, 24);
            this.textEdit_CLIExcluido.StyleController = this.layoutControl_Clientes;
            this.textEdit_CLIExcluido.TabIndex = 10;
            // 
            // memoEdit_CLIObservacao
            // 
            this.memoEdit_CLIObservacao.Location = new System.Drawing.Point(136, 499);
            this.memoEdit_CLIObservacao.MenuManager = this.barManager_ContasMP;
            this.memoEdit_CLIObservacao.Name = "memoEdit_CLIObservacao";
            this.memoEdit_CLIObservacao.Size = new System.Drawing.Size(442, 48);
            this.memoEdit_CLIObservacao.StyleController = this.layoutControl_Clientes;
            this.memoEdit_CLIObservacao.TabIndex = 18;
            // 
            // textEdit_CLITelefoneNumero
            // 
            this.textEdit_CLITelefoneNumero.Location = new System.Drawing.Point(414, 117);
            this.textEdit_CLITelefoneNumero.MenuManager = this.barManager_ContasMP;
            this.textEdit_CLITelefoneNumero.Name = "textEdit_CLITelefoneNumero";
            this.textEdit_CLITelefoneNumero.Size = new System.Drawing.Size(555, 20);
            this.textEdit_CLITelefoneNumero.StyleController = this.layoutControl_Clientes;
            this.textEdit_CLITelefoneNumero.TabIndex = 19;
            // 
            // simpleButton_CLITelefoneAdd
            // 
            this.simpleButton_CLITelefoneAdd.Location = new System.Drawing.Point(975, 117);
            this.simpleButton_CLITelefoneAdd.Name = "simpleButton_CLITelefoneAdd";
            this.simpleButton_CLITelefoneAdd.Size = new System.Drawing.Size(204, 22);
            this.simpleButton_CLITelefoneAdd.StyleController = this.layoutControl_Clientes;
            this.simpleButton_CLITelefoneAdd.TabIndex = 20;
            this.simpleButton_CLITelefoneAdd.Text = "Add";
            this.simpleButton_CLITelefoneAdd.Click += new System.EventHandler(this.simpleButton_CLITelefoneAdd_Click);
            // 
            // textEdit_CLIEmail
            // 
            this.textEdit_CLIEmail.Location = new System.Drawing.Point(414, 117);
            this.textEdit_CLIEmail.MenuManager = this.barManager_ContasMP;
            this.textEdit_CLIEmail.Name = "textEdit_CLIEmail";
            this.textEdit_CLIEmail.Size = new System.Drawing.Size(555, 20);
            this.textEdit_CLIEmail.StyleController = this.layoutControl_Clientes;
            this.textEdit_CLIEmail.TabIndex = 24;
            this.textEdit_CLIEmail.Validating += new System.ComponentModel.CancelEventHandler(this.textEdit_CLIEmail_Validating);
            // 
            // simpleButton_CLIEmailAdd
            // 
            this.simpleButton_CLIEmailAdd.Location = new System.Drawing.Point(975, 117);
            this.simpleButton_CLIEmailAdd.Name = "simpleButton_CLIEmailAdd";
            this.simpleButton_CLIEmailAdd.Size = new System.Drawing.Size(204, 22);
            this.simpleButton_CLIEmailAdd.StyleController = this.layoutControl_Clientes;
            this.simpleButton_CLIEmailAdd.TabIndex = 25;
            this.simpleButton_CLIEmailAdd.Text = "Add";
            this.simpleButton_CLIEmailAdd.Click += new System.EventHandler(this.simpleButton_CLIEmailAdd_Click);
            // 
            // textEdit_CLITipo
            // 
            this.textEdit_CLITipo.Location = new System.Drawing.Point(136, 131);
            this.textEdit_CLITipo.MenuManager = this.barManager_ContasMP;
            this.textEdit_CLITipo.Name = "textEdit_CLITipo";
            this.textEdit_CLITipo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.textEdit_CLITipo.Properties.MaxLength = 1;
            this.textEdit_CLITipo.Properties.NullText = "";
            this.textEdit_CLITipo.Properties.PopupSizeable = false;
            this.textEdit_CLITipo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.textEdit_CLITipo.Size = new System.Drawing.Size(1055, 20);
            this.textEdit_CLITipo.StyleController = this.layoutControl_Clientes;
            this.textEdit_CLITipo.TabIndex = 6;
            this.textEdit_CLITipo.EditValueChanged += new System.EventHandler(this.textEdit_CLITipo_EditValueChanged);
            this.textEdit_CLITipo.Leave += new System.EventHandler(this.textEdit_CLITipo_EditValueChanged);
            // 
            // comboBoxEdit_CLIEstado
            // 
            this.comboBoxEdit_CLIEstado.Location = new System.Drawing.Point(136, 423);
            this.comboBoxEdit_CLIEstado.MenuManager = this.barManager_ContasMP;
            this.comboBoxEdit_CLIEstado.Name = "comboBoxEdit_CLIEstado";
            this.comboBoxEdit_CLIEstado.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit_CLIEstado.Properties.MaxLength = 2;
            this.comboBoxEdit_CLIEstado.Properties.NullText = "";
            this.comboBoxEdit_CLIEstado.Properties.PopupSizeable = false;
            this.comboBoxEdit_CLIEstado.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.comboBoxEdit_CLIEstado.Size = new System.Drawing.Size(1055, 20);
            this.comboBoxEdit_CLIEstado.StyleController = this.layoutControl_Clientes;
            this.comboBoxEdit_CLIEstado.TabIndex = 15;
            // 
            // comboBoxEdit_CLIEmailTipo
            // 
            this.comboBoxEdit_CLIEmailTipo.Location = new System.Drawing.Point(148, 117);
            this.comboBoxEdit_CLIEmailTipo.MenuManager = this.barManager_ContasMP;
            this.comboBoxEdit_CLIEmailTipo.Name = "comboBoxEdit_CLIEmailTipo";
            this.comboBoxEdit_CLIEmailTipo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit_CLIEmailTipo.Properties.NullText = "";
            this.comboBoxEdit_CLIEmailTipo.Properties.PopupSizeable = false;
            this.comboBoxEdit_CLIEmailTipo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.comboBoxEdit_CLIEmailTipo.Size = new System.Drawing.Size(160, 20);
            this.comboBoxEdit_CLIEmailTipo.StyleController = this.layoutControl_Clientes;
            this.comboBoxEdit_CLIEmailTipo.TabIndex = 28;
            // 
            // comboBoxEdit_CLITelefoneTipo
            // 
            this.comboBoxEdit_CLITelefoneTipo.Location = new System.Drawing.Point(148, 117);
            this.comboBoxEdit_CLITelefoneTipo.MenuManager = this.barManager_ContasMP;
            this.comboBoxEdit_CLITelefoneTipo.Name = "comboBoxEdit_CLITelefoneTipo";
            this.comboBoxEdit_CLITelefoneTipo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit_CLITelefoneTipo.Properties.NullText = "";
            this.comboBoxEdit_CLITelefoneTipo.Properties.PopupSizeable = false;
            this.comboBoxEdit_CLITelefoneTipo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.comboBoxEdit_CLITelefoneTipo.Size = new System.Drawing.Size(160, 20);
            this.comboBoxEdit_CLITelefoneTipo.StyleController = this.layoutControl_Clientes;
            this.comboBoxEdit_CLITelefoneTipo.TabIndex = 27;
            // 
            // layoutControlGroup_RegisterCliente
            // 
            this.layoutControlGroup_RegisterCliente.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup_RegisterCliente.GroupBordersVisible = false;
            this.layoutControlGroup_RegisterCliente.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.emptySpaceItem4,
            this.simpleLabelItem_RegisterClientes,
            this.emptySpaceItem5,
            this.emptySpaceItem7,
            this.layoutControlGroup_Endereco,
            this.emptySpaceItem9,
            this.layoutControlGroup1,
            this.tabbedControlGroup_CLIDados,
            this.emptySpaceItem3});
            this.layoutControlGroup_RegisterCliente.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup_RegisterCliente.Name = "Root";
            this.layoutControlGroup_RegisterCliente.Size = new System.Drawing.Size(1225, 581);
            this.layoutControlGroup_RegisterCliente.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(10, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(1185, 10);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(10, 251);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(1185, 10);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(1195, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(10, 561);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleLabelItem_RegisterClientes
            // 
            this.simpleLabelItem_RegisterClientes.AllowHotTrack = false;
            this.simpleLabelItem_RegisterClientes.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem_RegisterClientes.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem_RegisterClientes.Location = new System.Drawing.Point(10, 10);
            this.simpleLabelItem_RegisterClientes.Name = "simpleLabelItem_RegisterClientes";
            this.simpleLabelItem_RegisterClientes.OptionsPrint.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem_RegisterClientes.OptionsPrint.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem_RegisterClientes.Size = new System.Drawing.Size(1185, 17);
            this.simpleLabelItem_RegisterClientes.Text = "Cadastro Clientes";
            this.simpleLabelItem_RegisterClientes.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(10, 27);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(1185, 10);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(10, 447);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(1185, 10);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup_Endereco
            // 
            this.layoutControlGroup_Endereco.AppearanceGroup.BackColor = System.Drawing.Color.White;
            this.layoutControlGroup_Endereco.AppearanceGroup.Options.UseBackColor = true;
            this.layoutControlGroup_Endereco.AppearanceTabPage.PageClient.BackColor = System.Drawing.Color.White;
            this.layoutControlGroup_Endereco.AppearanceTabPage.PageClient.Options.UseBackColor = true;
            this.layoutControlGroup_Endereco.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem_CLIRua,
            this.layoutControlItem_CLIComplemento,
            this.layoutControlItem_CLICep,
            this.layoutControlItem_Bairro,
            this.layoutControlItem_Cidade,
            this.layoutControlItem_CLIEstado});
            this.layoutControlGroup_Endereco.Location = new System.Drawing.Point(10, 261);
            this.layoutControlGroup_Endereco.Name = "layoutControlGroup_Endereco";
            this.layoutControlGroup_Endereco.Size = new System.Drawing.Size(1185, 186);
            this.layoutControlGroup_Endereco.Text = "Endereço";
            // 
            // layoutControlItem_CLIRua
            // 
            this.layoutControlItem_CLIRua.Control = this.textEdit_CLIRua;
            this.layoutControlItem_CLIRua.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem_CLIRua.Name = "layoutControlItem_CLIRua";
            this.layoutControlItem_CLIRua.Size = new System.Drawing.Size(1161, 24);
            this.layoutControlItem_CLIRua.Text = "Rua";
            this.layoutControlItem_CLIRua.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem_CLIComplemento
            // 
            this.layoutControlItem_CLIComplemento.Control = this.textEdit_CLIComplemento;
            this.layoutControlItem_CLIComplemento.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem_CLIComplemento.Name = "layoutControlItem_CLIComplemento";
            this.layoutControlItem_CLIComplemento.Size = new System.Drawing.Size(1161, 24);
            this.layoutControlItem_CLIComplemento.Text = "Complemento";
            this.layoutControlItem_CLIComplemento.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem_CLICep
            // 
            this.layoutControlItem_CLICep.Control = this.textEdit_CLICEP;
            this.layoutControlItem_CLICep.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem_CLICep.Name = "layoutControlItem_CLICep";
            this.layoutControlItem_CLICep.Size = new System.Drawing.Size(1161, 24);
            this.layoutControlItem_CLICep.Text = "CEP";
            this.layoutControlItem_CLICep.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem_Bairro
            // 
            this.layoutControlItem_Bairro.Control = this.textEdit_CLIBairro;
            this.layoutControlItem_Bairro.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem_Bairro.Name = "layoutControlItem_Bairro";
            this.layoutControlItem_Bairro.Size = new System.Drawing.Size(1161, 24);
            this.layoutControlItem_Bairro.Text = "Bairro";
            this.layoutControlItem_Bairro.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem_Cidade
            // 
            this.layoutControlItem_Cidade.Control = this.textEdit_CLICidade;
            this.layoutControlItem_Cidade.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem_Cidade.Name = "layoutControlItem_Cidade";
            this.layoutControlItem_Cidade.Size = new System.Drawing.Size(1161, 24);
            this.layoutControlItem_Cidade.Text = "Cidade";
            this.layoutControlItem_Cidade.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem_CLIEstado
            // 
            this.layoutControlItem_CLIEstado.Control = this.comboBoxEdit_CLIEstado;
            this.layoutControlItem_CLIEstado.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem_CLIEstado.Name = "layoutControlItem_CLIEstado";
            this.layoutControlItem_CLIEstado.Size = new System.Drawing.Size(1161, 24);
            this.layoutControlItem_CLIEstado.Text = "Estado";
            this.layoutControlItem_CLIEstado.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.Location = new System.Drawing.Point(10, 551);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(1185, 10);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem_CLIUltimaAlteracao,
            this.layoutControlItem_CLIExcluido,
            this.emptySpaceItem10,
            this.layoutControlItem_CLIObservacao});
            this.layoutControlGroup1.Location = new System.Drawing.Point(10, 457);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1185, 94);
            this.layoutControlGroup1.Text = "Status";
            // 
            // layoutControlItem_CLIUltimaAlteracao
            // 
            this.layoutControlItem_CLIUltimaAlteracao.Control = this.dateEdit_CLIUltimaAlteracao;
            this.layoutControlItem_CLIUltimaAlteracao.Location = new System.Drawing.Point(648, 0);
            this.layoutControlItem_CLIUltimaAlteracao.Name = "layoutControlItem_CLIUltimaAlteracao";
            this.layoutControlItem_CLIUltimaAlteracao.Size = new System.Drawing.Size(513, 24);
            this.layoutControlItem_CLIUltimaAlteracao.Text = "Última Alteração";
            this.layoutControlItem_CLIUltimaAlteracao.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem_CLIExcluido
            // 
            this.layoutControlItem_CLIExcluido.Control = this.textEdit_CLIExcluido;
            this.layoutControlItem_CLIExcluido.CustomizationFormText = "Status";
            this.layoutControlItem_CLIExcluido.Location = new System.Drawing.Point(648, 24);
            this.layoutControlItem_CLIExcluido.Name = "layoutControlItem_CLIExcluido";
            this.layoutControlItem_CLIExcluido.Size = new System.Drawing.Size(513, 28);
            this.layoutControlItem_CLIExcluido.Text = "Excluído";
            this.layoutControlItem_CLIExcluido.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.Location = new System.Drawing.Point(548, 0);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(100, 52);
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem_CLIObservacao
            // 
            this.layoutControlItem_CLIObservacao.Control = this.memoEdit_CLIObservacao;
            this.layoutControlItem_CLIObservacao.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem_CLIObservacao.Name = "layoutControlItem_CLIObservacao";
            this.layoutControlItem_CLIObservacao.Size = new System.Drawing.Size(548, 52);
            this.layoutControlItem_CLIObservacao.Text = "Observação";
            this.layoutControlItem_CLIObservacao.TextSize = new System.Drawing.Size(99, 13);
            // 
            // tabbedControlGroup_CLIDados
            // 
            this.tabbedControlGroup_CLIDados.Location = new System.Drawing.Point(10, 37);
            this.tabbedControlGroup_CLIDados.Name = "tabbedControlGroup_CLIDados";
            this.tabbedControlGroup_CLIDados.SelectedTabPage = this.layoutControlGroup_CLIContato;
            this.tabbedControlGroup_CLIDados.SelectedTabPageIndex = 1;
            this.tabbedControlGroup_CLIDados.Size = new System.Drawing.Size(1185, 214);
            this.tabbedControlGroup_CLIDados.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup_CLIDados,
            this.layoutControlGroup_CLIContato});
            // 
            // layoutControlGroup_CLIContato
            // 
            this.layoutControlGroup_CLIContato.AppearanceGroup.BackColor = System.Drawing.SystemColors.Control;
            this.layoutControlGroup_CLIContato.AppearanceGroup.Options.UseBackColor = true;
            this.layoutControlGroup_CLIContato.AppearanceTabPage.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.layoutControlGroup_CLIContato.AppearanceTabPage.PageClient.Options.UseBackColor = true;
            this.layoutControlGroup_CLIContato.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup_CLIContatos});
            this.layoutControlGroup_CLIContato.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup_CLIContato.Name = "layoutControlGroup_CLIContato";
            this.layoutControlGroup_CLIContato.Size = new System.Drawing.Size(1161, 168);
            this.layoutControlGroup_CLIContato.Text = "Contatos";
            // 
            // tabbedControlGroup_CLIContatos
            // 
            this.tabbedControlGroup_CLIContatos.AppearanceGroup.BackColor = System.Drawing.SystemColors.Control;
            this.tabbedControlGroup_CLIContatos.AppearanceGroup.Options.UseBackColor = true;
            this.tabbedControlGroup_CLIContatos.AppearanceTabPage.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tabbedControlGroup_CLIContatos.AppearanceTabPage.PageClient.Options.UseBackColor = true;
            this.tabbedControlGroup_CLIContatos.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup_CLIContatos.Name = "tabbedControlGroup_CLIContatos";
            this.tabbedControlGroup_CLIContatos.SelectedTabPage = this.layoutControlGroup_CLITelefone;
            this.tabbedControlGroup_CLIContatos.SelectedTabPageIndex = 0;
            this.tabbedControlGroup_CLIContatos.Size = new System.Drawing.Size(1161, 168);
            this.tabbedControlGroup_CLIContatos.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup_CLITelefone,
            this.layoutControlGroup_CLIEmail});
            // 
            // layoutControlGroup_CLITelefone
            // 
            this.layoutControlGroup_CLITelefone.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem_CLITelefoneNumero,
            this.layoutControlItem1,
            this.simpleSeparator1,
            this.layoutControlItem3,
            this.layoutControlItem_CLITelefoneTipo});
            this.layoutControlGroup_CLITelefone.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup_CLITelefone.Name = "layoutControlGroup_CLITelefone";
            this.layoutControlGroup_CLITelefone.Size = new System.Drawing.Size(1137, 122);
            this.layoutControlGroup_CLITelefone.Text = "Telefone";
            // 
            // layoutControlItem_CLITelefoneNumero
            // 
            this.layoutControlItem_CLITelefoneNumero.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem_CLITelefoneNumero.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem_CLITelefoneNumero.Control = this.textEdit_CLITelefoneNumero;
            this.layoutControlItem_CLITelefoneNumero.Location = new System.Drawing.Point(266, 0);
            this.layoutControlItem_CLITelefoneNumero.Name = "layoutControlItem_CLITelefoneNumero";
            this.layoutControlItem_CLITelefoneNumero.Size = new System.Drawing.Size(661, 26);
            this.layoutControlItem_CLITelefoneNumero.Text = "Telefone";
            this.layoutControlItem_CLITelefoneNumero.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.simpleButton_CLITelefoneAdd;
            this.layoutControlItem1.Location = new System.Drawing.Point(929, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(208, 26);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // simpleSeparator1
            // 
            this.simpleSeparator1.AllowHotTrack = false;
            this.simpleSeparator1.Location = new System.Drawing.Point(927, 0);
            this.simpleSeparator1.Name = "simpleSeparator1";
            this.simpleSeparator1.Size = new System.Drawing.Size(2, 26);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gridControl_CLITelefones;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(1137, 96);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem_CLITelefoneTipo
            // 
            this.layoutControlItem_CLITelefoneTipo.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem_CLITelefoneTipo.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem_CLITelefoneTipo.Control = this.comboBoxEdit_CLITelefoneTipo;
            this.layoutControlItem_CLITelefoneTipo.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem_CLITelefoneTipo.Name = "layoutControlItem_CLITelefoneTipo";
            this.layoutControlItem_CLITelefoneTipo.Size = new System.Drawing.Size(266, 26);
            this.layoutControlItem_CLITelefoneTipo.Text = "Tipo";
            this.layoutControlItem_CLITelefoneTipo.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlGroup_CLIEmail
            // 
            this.layoutControlGroup_CLIEmail.AppearanceGroup.BackColor = System.Drawing.SystemColors.Control;
            this.layoutControlGroup_CLIEmail.AppearanceGroup.Options.UseBackColor = true;
            this.layoutControlGroup_CLIEmail.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem_CLIEmail,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.simpleSeparator4,
            this.layoutControlItem_CLIEmailTipo});
            this.layoutControlGroup_CLIEmail.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup_CLIEmail.Name = "layoutControlGroup_CLIEmail";
            this.layoutControlGroup_CLIEmail.Size = new System.Drawing.Size(1137, 122);
            this.layoutControlGroup_CLIEmail.Text = "E-mail";
            // 
            // layoutControlItem_CLIEmail
            // 
            this.layoutControlItem_CLIEmail.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem_CLIEmail.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem_CLIEmail.Control = this.textEdit_CLIEmail;
            this.layoutControlItem_CLIEmail.Location = new System.Drawing.Point(266, 0);
            this.layoutControlItem_CLIEmail.MinSize = new System.Drawing.Size(156, 24);
            this.layoutControlItem_CLIEmail.Name = "layoutControlItem_CLIEmail";
            this.layoutControlItem_CLIEmail.Size = new System.Drawing.Size(661, 26);
            this.layoutControlItem_CLIEmail.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem_CLIEmail.Text = "E-mail";
            this.layoutControlItem_CLIEmail.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.gridControl_CLIEmail;
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(1137, 96);
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.simpleButton_CLIEmailAdd;
            this.layoutControlItem14.Location = new System.Drawing.Point(929, 0);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(208, 26);
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextVisible = false;
            // 
            // simpleSeparator4
            // 
            this.simpleSeparator4.AllowHotTrack = false;
            this.simpleSeparator4.Location = new System.Drawing.Point(927, 0);
            this.simpleSeparator4.Name = "simpleSeparator4";
            this.simpleSeparator4.Size = new System.Drawing.Size(2, 26);
            // 
            // layoutControlItem_CLIEmailTipo
            // 
            this.layoutControlItem_CLIEmailTipo.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem_CLIEmailTipo.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem_CLIEmailTipo.Control = this.comboBoxEdit_CLIEmailTipo;
            this.layoutControlItem_CLIEmailTipo.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem_CLIEmailTipo.Name = "layoutControlItem_CLIEmailTipo";
            this.layoutControlItem_CLIEmailTipo.Size = new System.Drawing.Size(266, 26);
            this.layoutControlItem_CLIEmailTipo.Text = "Tipo";
            this.layoutControlItem_CLIEmailTipo.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlGroup_CLIDados
            // 
            this.layoutControlGroup_CLIDados.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem_CLIRazaoSocial,
            this.layoutControlItem_CLINomeFantasia,
            this.layoutControlItem_CLITipo,
            this.layoutControlItem_CLICnpj,
            this.layoutControlItem_CLIInscricaoEstadual,
            this.layoutControlItem_CLISuframa,
            this.layoutControlItem_CLINomeExcFiscal});
            this.layoutControlGroup_CLIDados.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup_CLIDados.Name = "layoutControlGroup_CLIDados";
            this.layoutControlGroup_CLIDados.Size = new System.Drawing.Size(1161, 168);
            this.layoutControlGroup_CLIDados.Text = "Dados";
            // 
            // layoutControlItem_CLIRazaoSocial
            // 
            this.layoutControlItem_CLIRazaoSocial.Control = this.textEdit_CLIRazaoSocial;
            this.layoutControlItem_CLIRazaoSocial.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem_CLIRazaoSocial.Name = "layoutControlItem_CLIRazaoSocial";
            this.layoutControlItem_CLIRazaoSocial.Size = new System.Drawing.Size(1161, 24);
            this.layoutControlItem_CLIRazaoSocial.Text = "Razão Social";
            this.layoutControlItem_CLIRazaoSocial.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem_CLINomeFantasia
            // 
            this.layoutControlItem_CLINomeFantasia.Control = this.textEdit_CLINomeFantasia;
            this.layoutControlItem_CLINomeFantasia.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem_CLINomeFantasia.Name = "layoutControlItem_CLINomeFantasia";
            this.layoutControlItem_CLINomeFantasia.Size = new System.Drawing.Size(1161, 24);
            this.layoutControlItem_CLINomeFantasia.Text = "Nome Fantasia";
            this.layoutControlItem_CLINomeFantasia.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem_CLITipo
            // 
            this.layoutControlItem_CLITipo.Control = this.textEdit_CLITipo;
            this.layoutControlItem_CLITipo.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem_CLITipo.Name = "layoutControlItem_CLITipo";
            this.layoutControlItem_CLITipo.Size = new System.Drawing.Size(1161, 24);
            this.layoutControlItem_CLITipo.Text = "Tipo";
            this.layoutControlItem_CLITipo.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem_CLICnpj
            // 
            this.layoutControlItem_CLICnpj.Control = this.textEdit_CLICNPJ;
            this.layoutControlItem_CLICnpj.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem_CLICnpj.Name = "layoutControlItem_CLICnpj";
            this.layoutControlItem_CLICnpj.Size = new System.Drawing.Size(1161, 24);
            this.layoutControlItem_CLICnpj.Text = "CNPJ";
            this.layoutControlItem_CLICnpj.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem_CLIInscricaoEstadual
            // 
            this.layoutControlItem_CLIInscricaoEstadual.Control = this.textEdit_CLIInscricaoEstadual;
            this.layoutControlItem_CLIInscricaoEstadual.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem_CLIInscricaoEstadual.Name = "layoutControlItem_CLIInscricaoEstadual";
            this.layoutControlItem_CLIInscricaoEstadual.Size = new System.Drawing.Size(1161, 24);
            this.layoutControlItem_CLIInscricaoEstadual.Text = "Inscrição Estadual";
            this.layoutControlItem_CLIInscricaoEstadual.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem_CLISuframa
            // 
            this.layoutControlItem_CLISuframa.Control = this.textEdit_CLISuframa;
            this.layoutControlItem_CLISuframa.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem_CLISuframa.Name = "layoutControlItem_CLISuframa";
            this.layoutControlItem_CLISuframa.Size = new System.Drawing.Size(1161, 24);
            this.layoutControlItem_CLISuframa.Text = "Suframa";
            this.layoutControlItem_CLISuframa.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem_CLINomeExcFiscal
            // 
            this.layoutControlItem_CLINomeExcFiscal.Control = this.textEdit_CLINomeExcFiscal;
            this.layoutControlItem_CLINomeExcFiscal.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItem_CLINomeExcFiscal.Name = "layoutControlItem_CLINomeExcFiscal";
            this.layoutControlItem_CLINomeExcFiscal.Size = new System.Drawing.Size(1161, 24);
            this.layoutControlItem_CLINomeExcFiscal.Text = "Nome Exceção Fiscal";
            this.layoutControlItem_CLINomeExcFiscal.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(10, 561);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // tabNavigationPage_DataGridClientes
            // 
            this.tabNavigationPage_DataGridClientes.Caption = "Clientes";
            this.tabNavigationPage_DataGridClientes.Controls.Add(this.gridControl_Clientes);
            this.tabNavigationPage_DataGridClientes.Image = ((System.Drawing.Image)(resources.GetObject("tabNavigationPage_DataGridClientes.Image")));
            this.tabNavigationPage_DataGridClientes.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage_DataGridClientes.Name = "tabNavigationPage_DataGridClientes";
            this.tabNavigationPage_DataGridClientes.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage_DataGridClientes.Size = new System.Drawing.Size(1246, 404);
            // 
            // gridControl_Clientes
            // 
            this.gridControl_Clientes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl_Clientes.Location = new System.Drawing.Point(0, 0);
            this.gridControl_Clientes.MainView = this.gridView_Clientes;
            this.gridControl_Clientes.MenuManager = this.barManager_ContasMP;
            this.gridControl_Clientes.Name = "gridControl_Clientes";
            this.gridControl_Clientes.Size = new System.Drawing.Size(1246, 404);
            this.gridControl_Clientes.TabIndex = 0;
            this.gridControl_Clientes.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView_Clientes});
            // 
            // clienteBindingSource
            // 
            this.clienteBindingSource.DataMember = "Cliente";
            this.clienteBindingSource.DataSource = this.meusPedidosDataSet;
            // 
            // meusPedidosDataSet
            // 
            this.meusPedidosDataSet.DataSetName = "MeusPedidosDataSet";
            this.meusPedidosDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView_Clientes
            // 
            this.gridView_Clientes.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colid,
            this.colrazao_social1,
            this.colnome_fantasia1,
            this.coltipo,
            this.colcnpj,
            this.colinscricao_estadual,
            this.colsuframa,
            this.colrua,
            this.colcomplemento,
            this.colcep,
            this.colbairro,
            this.colcidade,
            this.colestado,
            this.colobservacao,
            this.colnome_excecao_fiscal,
            this.colultima_alteracao,
            this.colexcluido});
            this.gridView_Clientes.GridControl = this.gridControl_Clientes;
            this.gridView_Clientes.Name = "gridView_Clientes";
            this.gridView_Clientes.OptionsBehavior.Editable = false;
            this.gridView_Clientes.OptionsBehavior.ReadOnly = true;
            this.gridView_Clientes.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView_Clientes.OptionsSelection.MultiSelect = true;
            this.gridView_Clientes.OptionsSelection.UseIndicatorForSelection = false;
            this.gridView_Clientes.OptionsView.ShowGroupPanel = false;
            this.gridView_Clientes.OptionsView.ShowIndicator = false;
            this.gridView_Clientes.DoubleClick += new System.EventHandler(this.gridView_Clientes_DoubleClick);
            // 
            // colid
            // 
            this.colid.Caption = "ID";
            this.colid.FieldName = "id";
            this.colid.Name = "colid";
            this.colid.Visible = true;
            this.colid.VisibleIndex = 0;
            // 
            // colrazao_social1
            // 
            this.colrazao_social1.Caption = "Razão Social";
            this.colrazao_social1.FieldName = "razao_social";
            this.colrazao_social1.Name = "colrazao_social1";
            this.colrazao_social1.Visible = true;
            this.colrazao_social1.VisibleIndex = 1;
            // 
            // colnome_fantasia1
            // 
            this.colnome_fantasia1.Caption = "Nome Fantasia";
            this.colnome_fantasia1.FieldName = "nome_fantasia";
            this.colnome_fantasia1.Name = "colnome_fantasia1";
            this.colnome_fantasia1.Visible = true;
            this.colnome_fantasia1.VisibleIndex = 2;
            // 
            // coltipo
            // 
            this.coltipo.Caption = "Tipo";
            this.coltipo.FieldName = "tipo";
            this.coltipo.Name = "coltipo";
            this.coltipo.Visible = true;
            this.coltipo.VisibleIndex = 3;
            // 
            // colcnpj
            // 
            this.colcnpj.Caption = "CNPJ";
            this.colcnpj.FieldName = "cnpj";
            this.colcnpj.Name = "colcnpj";
            this.colcnpj.Visible = true;
            this.colcnpj.VisibleIndex = 4;
            // 
            // colinscricao_estadual
            // 
            this.colinscricao_estadual.Caption = "Inscrição Estadual";
            this.colinscricao_estadual.FieldName = "inscricao_estadual";
            this.colinscricao_estadual.Name = "colinscricao_estadual";
            this.colinscricao_estadual.Visible = true;
            this.colinscricao_estadual.VisibleIndex = 5;
            // 
            // colsuframa
            // 
            this.colsuframa.Caption = "Suframa";
            this.colsuframa.FieldName = "suframa";
            this.colsuframa.Name = "colsuframa";
            this.colsuframa.Visible = true;
            this.colsuframa.VisibleIndex = 6;
            // 
            // colrua
            // 
            this.colrua.Caption = "Rua";
            this.colrua.FieldName = "rua";
            this.colrua.Name = "colrua";
            this.colrua.Visible = true;
            this.colrua.VisibleIndex = 7;
            // 
            // colcomplemento
            // 
            this.colcomplemento.Caption = "Complemento";
            this.colcomplemento.FieldName = "complemento";
            this.colcomplemento.Name = "colcomplemento";
            this.colcomplemento.Visible = true;
            this.colcomplemento.VisibleIndex = 8;
            // 
            // colcep
            // 
            this.colcep.Caption = "CEP";
            this.colcep.FieldName = "cep";
            this.colcep.Name = "colcep";
            this.colcep.Visible = true;
            this.colcep.VisibleIndex = 9;
            // 
            // colbairro
            // 
            this.colbairro.Caption = "Bairro";
            this.colbairro.FieldName = "bairro";
            this.colbairro.Name = "colbairro";
            this.colbairro.Visible = true;
            this.colbairro.VisibleIndex = 10;
            // 
            // colcidade
            // 
            this.colcidade.Caption = "Cidade";
            this.colcidade.FieldName = "cidade";
            this.colcidade.Name = "colcidade";
            this.colcidade.Visible = true;
            this.colcidade.VisibleIndex = 11;
            // 
            // colestado
            // 
            this.colestado.Caption = "Estado";
            this.colestado.FieldName = "estado";
            this.colestado.Name = "colestado";
            this.colestado.Visible = true;
            this.colestado.VisibleIndex = 12;
            // 
            // colobservacao
            // 
            this.colobservacao.Caption = "Observação";
            this.colobservacao.FieldName = "observacao";
            this.colobservacao.Name = "colobservacao";
            this.colobservacao.Visible = true;
            this.colobservacao.VisibleIndex = 13;
            // 
            // colnome_excecao_fiscal
            // 
            this.colnome_excecao_fiscal.Caption = "Nome Exc. Fiscal";
            this.colnome_excecao_fiscal.FieldName = "nome_excecao_fiscal";
            this.colnome_excecao_fiscal.Name = "colnome_excecao_fiscal";
            this.colnome_excecao_fiscal.Visible = true;
            this.colnome_excecao_fiscal.VisibleIndex = 14;
            // 
            // colultima_alteracao
            // 
            this.colultima_alteracao.Caption = "Última Alteração";
            this.colultima_alteracao.FieldName = "ultima_alteracao";
            this.colultima_alteracao.Name = "colultima_alteracao";
            this.colultima_alteracao.Visible = true;
            this.colultima_alteracao.VisibleIndex = 16;
            // 
            // colexcluido
            // 
            this.colexcluido.Caption = "Excluído";
            this.colexcluido.FieldName = "excluido";
            this.colexcluido.Name = "colexcluido";
            this.colexcluido.Visible = true;
            this.colexcluido.VisibleIndex = 15;
            // 
            // tabPane_ConfigClientes
            // 
            this.tabPane_ConfigClientes.Controls.Add(this.tabNavigationPage_DataGridClientes);
            this.tabPane_ConfigClientes.Controls.Add(this.tabNavigationPage_RegisterClientes);
            this.tabPane_ConfigClientes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPane_ConfigClientes.Location = new System.Drawing.Point(0, 94);
            this.tabPane_ConfigClientes.Name = "tabPane_ConfigClientes";
            this.tabPane_ConfigClientes.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.tabNavigationPage_DataGridClientes,
            this.tabNavigationPage_RegisterClientes});
            this.tabPane_ConfigClientes.RegularSize = new System.Drawing.Size(1264, 468);
            this.tabPane_ConfigClientes.SelectedPage = this.tabNavigationPage_RegisterClientes;
            this.tabPane_ConfigClientes.Size = new System.Drawing.Size(1264, 468);
            this.tabPane_ConfigClientes.TabIndex = 8;
            this.tabPane_ConfigClientes.Text = "Clientes";
            // 
            // tabPane1
            // 
            this.tabPane1.Controls.Add(this.tabNavigationPage1);
            this.tabPane1.Controls.Add(this.tabNavigationPage2);
            this.tabPane1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPane1.Location = new System.Drawing.Point(0, 94);
            this.tabPane1.Name = "tabPane1";
            this.tabPane1.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.tabNavigationPage1,
            this.tabNavigationPage2});
            this.tabPane1.RegularSize = new System.Drawing.Size(1264, 445);
            this.tabPane1.SelectedPage = this.tabNavigationPage2;
            this.tabPane1.Size = new System.Drawing.Size(1264, 445);
            this.tabPane1.TabIndex = 8;
            this.tabPane1.Text = "Clientes";
            // 
            // tabNavigationPage1
            // 
            this.tabNavigationPage1.Caption = "Clientes";
            this.tabNavigationPage1.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage1.Name = "tabNavigationPage1";
            this.tabNavigationPage1.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage1.Size = new System.Drawing.Size(1246, 381);
            // 
            // tabNavigationPage2
            // 
            this.tabNavigationPage2.Caption = "Cadastro";
            this.tabNavigationPage2.Controls.Add(this.panelControl1);
            this.tabNavigationPage2.Image = ((System.Drawing.Image)(resources.GetObject("tabNavigationPage2.Image")));
            this.tabNavigationPage2.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage2.Name = "tabNavigationPage2";
            this.tabNavigationPage2.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage2.Size = new System.Drawing.Size(1246, 381);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.layoutControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1246, 381);
            this.panelControl1.TabIndex = 0;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.textEdit2);
            this.layoutControl1.Controls.Add(this.textEdit3);
            this.layoutControl1.Controls.Add(this.textEdit4);
            this.layoutControl1.Controls.Add(this.textEdit5);
            this.layoutControl1.Controls.Add(this.textEdit6);
            this.layoutControl1.Controls.Add(this.textEdit7);
            this.layoutControl1.Controls.Add(this.textEdit8);
            this.layoutControl1.Controls.Add(this.textEdit9);
            this.layoutControl1.Controls.Add(this.textEdit10);
            this.layoutControl1.Controls.Add(this.textEdit11);
            this.layoutControl1.Controls.Add(this.textEdit12);
            this.layoutControl1.Controls.Add(this.textEdit13);
            this.layoutControl1.Controls.Add(this.textEdit14);
            this.layoutControl1.Controls.Add(this.dateEdit1);
            this.layoutControl1.Controls.Add(this.toggleSwitch1);
            this.layoutControl1.Controls.Add(this.memoEdit1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup3;
            this.layoutControl1.Size = new System.Drawing.Size(1242, 377);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textEdit2
            // 
            this.textEdit2.Location = new System.Drawing.Point(136, 83);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Size = new System.Drawing.Size(1072, 20);
            this.textEdit2.StyleController = this.layoutControl1;
            this.textEdit2.TabIndex = 4;
            this.textEdit2.Visible = false;
            // 
            // textEdit3
            // 
            this.textEdit3.Location = new System.Drawing.Point(136, 107);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Size = new System.Drawing.Size(1072, 20);
            this.textEdit3.StyleController = this.layoutControl1;
            this.textEdit3.TabIndex = 5;
            this.textEdit3.Visible = false;
            // 
            // textEdit4
            // 
            this.textEdit4.Location = new System.Drawing.Point(136, 131);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Properties.MaxLength = 1;
            this.textEdit4.Size = new System.Drawing.Size(1072, 20);
            this.textEdit4.StyleController = this.layoutControl1;
            this.textEdit4.TabIndex = 6;
            this.textEdit4.Visible = false;
            // 
            // textEdit5
            // 
            this.textEdit5.Location = new System.Drawing.Point(136, 155);
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Size = new System.Drawing.Size(1072, 20);
            this.textEdit5.StyleController = this.layoutControl1;
            this.textEdit5.TabIndex = 7;
            this.textEdit5.Visible = false;
            // 
            // textEdit6
            // 
            this.textEdit6.Location = new System.Drawing.Point(136, 179);
            this.textEdit6.Name = "textEdit6";
            this.textEdit6.Size = new System.Drawing.Size(1072, 20);
            this.textEdit6.StyleController = this.layoutControl1;
            this.textEdit6.TabIndex = 8;
            this.textEdit6.Visible = false;
            // 
            // textEdit7
            // 
            this.textEdit7.Location = new System.Drawing.Point(136, 203);
            this.textEdit7.Name = "textEdit7";
            this.textEdit7.Size = new System.Drawing.Size(1072, 20);
            this.textEdit7.StyleController = this.layoutControl1;
            this.textEdit7.TabIndex = 9;
            this.textEdit7.Visible = false;
            // 
            // textEdit8
            // 
            this.textEdit8.Location = new System.Drawing.Point(136, 303);
            this.textEdit8.Name = "textEdit8";
            this.textEdit8.Size = new System.Drawing.Size(1072, 20);
            this.textEdit8.StyleController = this.layoutControl1;
            this.textEdit8.TabIndex = 10;
            // 
            // textEdit9
            // 
            this.textEdit9.Location = new System.Drawing.Point(136, 327);
            this.textEdit9.Name = "textEdit9";
            this.textEdit9.Size = new System.Drawing.Size(1072, 20);
            this.textEdit9.StyleController = this.layoutControl1;
            this.textEdit9.TabIndex = 11;
            // 
            // textEdit10
            // 
            this.textEdit10.Location = new System.Drawing.Point(136, 351);
            this.textEdit10.Name = "textEdit10";
            this.textEdit10.Size = new System.Drawing.Size(1072, 20);
            this.textEdit10.StyleController = this.layoutControl1;
            this.textEdit10.TabIndex = 12;
            // 
            // textEdit11
            // 
            this.textEdit11.Location = new System.Drawing.Point(136, 375);
            this.textEdit11.Name = "textEdit11";
            this.textEdit11.Size = new System.Drawing.Size(1072, 20);
            this.textEdit11.StyleController = this.layoutControl1;
            this.textEdit11.TabIndex = 13;
            // 
            // textEdit12
            // 
            this.textEdit12.Location = new System.Drawing.Point(136, 399);
            this.textEdit12.Name = "textEdit12";
            this.textEdit12.Size = new System.Drawing.Size(1072, 20);
            this.textEdit12.StyleController = this.layoutControl1;
            this.textEdit12.TabIndex = 14;
            // 
            // textEdit13
            // 
            this.textEdit13.Location = new System.Drawing.Point(136, 423);
            this.textEdit13.Name = "textEdit13";
            this.textEdit13.Size = new System.Drawing.Size(1072, 20);
            this.textEdit13.StyleController = this.layoutControl1;
            this.textEdit13.TabIndex = 15;
            // 
            // textEdit14
            // 
            this.textEdit14.Location = new System.Drawing.Point(136, 227);
            this.textEdit14.Name = "textEdit14";
            this.textEdit14.Size = new System.Drawing.Size(1072, 20);
            this.textEdit14.StyleController = this.layoutControl1;
            this.textEdit14.TabIndex = 16;
            this.textEdit14.Visible = false;
            // 
            // dateEdit1
            // 
            this.dateEdit1.EditValue = null;
            this.dateEdit1.Location = new System.Drawing.Point(980, 499);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.ReadOnly = true;
            this.dateEdit1.Size = new System.Drawing.Size(228, 20);
            this.dateEdit1.StyleController = this.layoutControl1;
            this.dateEdit1.TabIndex = 17;
            // 
            // toggleSwitch1
            // 
            this.toggleSwitch1.Location = new System.Drawing.Point(980, 523);
            this.toggleSwitch1.Name = "toggleSwitch1";
            this.toggleSwitch1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.toggleSwitch1.Properties.OffText = "Não";
            this.toggleSwitch1.Properties.OnText = "Sim";
            this.toggleSwitch1.Size = new System.Drawing.Size(228, 24);
            this.toggleSwitch1.StyleController = this.layoutControl1;
            this.toggleSwitch1.TabIndex = 10;
            // 
            // memoEdit1
            // 
            this.memoEdit1.Location = new System.Drawing.Point(136, 499);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(728, 48);
            this.memoEdit1.StyleController = this.layoutControl1;
            this.memoEdit1.TabIndex = 18;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem6,
            this.emptySpaceItem8,
            this.emptySpaceItem11,
            this.emptySpaceItem12,
            this.simpleLabelItem1,
            this.emptySpaceItem13,
            this.emptySpaceItem14,
            this.layoutControlGroup4,
            this.emptySpaceItem15,
            this.layoutControlGroup5,
            this.tabbedControlGroup3});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "Root";
            this.layoutControlGroup3.Size = new System.Drawing.Size(1242, 581);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(10, 0);
            this.emptySpaceItem6.Name = "emptySpaceItem1";
            this.emptySpaceItem6.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.Location = new System.Drawing.Point(10, 251);
            this.emptySpaceItem8.Name = "emptySpaceItem2";
            this.emptySpaceItem8.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem11.Name = "emptySpaceItem3";
            this.emptySpaceItem11.Size = new System.Drawing.Size(10, 561);
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.Location = new System.Drawing.Point(1212, 0);
            this.emptySpaceItem12.Name = "emptySpaceItem4";
            this.emptySpaceItem12.Size = new System.Drawing.Size(10, 561);
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleLabelItem1
            // 
            this.simpleLabelItem1.AllowHotTrack = false;
            this.simpleLabelItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem1.Location = new System.Drawing.Point(10, 10);
            this.simpleLabelItem1.Name = "simpleLabelItem_RegisterClientes";
            this.simpleLabelItem1.OptionsPrint.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem1.OptionsPrint.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem1.Size = new System.Drawing.Size(1202, 17);
            this.simpleLabelItem1.Text = "Cadastro Clientes";
            this.simpleLabelItem1.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.Location = new System.Drawing.Point(10, 27);
            this.emptySpaceItem13.Name = "emptySpaceItem5";
            this.emptySpaceItem13.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.Location = new System.Drawing.Point(10, 447);
            this.emptySpaceItem14.Name = "emptySpaceItem7";
            this.emptySpaceItem14.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9});
            this.layoutControlGroup4.Location = new System.Drawing.Point(10, 261);
            this.layoutControlGroup4.Name = "layoutControlGroup_Endereço";
            this.layoutControlGroup4.Size = new System.Drawing.Size(1202, 186);
            this.layoutControlGroup4.Text = "Endereço";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.textEdit8;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem_CLIRua";
            this.layoutControlItem4.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem4.Text = "Rua";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.textEdit9;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem5.Name = "layoutControlItem_CLIComplemento";
            this.layoutControlItem5.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem5.Text = "Complemento";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.textEdit10;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem6.Name = "layoutControlItem_CLICep";
            this.layoutControlItem6.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem6.Text = "CEP";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.textEdit11;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem7.Name = "layoutControlItem_Bairro";
            this.layoutControlItem7.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem7.Text = "Bairro";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.textEdit12;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem8.Name = "layoutControlItem_Cidade";
            this.layoutControlItem8.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem8.Text = "Cidade";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.textEdit13;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem9.Name = "layoutControlItem_CLIEstado";
            this.layoutControlItem9.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem9.Text = "Estado";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.Location = new System.Drawing.Point(10, 551);
            this.emptySpaceItem15.Name = "emptySpaceItem9";
            this.emptySpaceItem15.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.emptySpaceItem16,
            this.layoutControlItem12});
            this.layoutControlGroup5.Location = new System.Drawing.Point(10, 457);
            this.layoutControlGroup5.Name = "layoutControlGroup1";
            this.layoutControlGroup5.Size = new System.Drawing.Size(1202, 94);
            this.layoutControlGroup5.Text = "Status";
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.dateEdit1;
            this.layoutControlItem10.Location = new System.Drawing.Point(844, 0);
            this.layoutControlItem10.Name = "layoutControlItem_CLIUltimaAlteracao";
            this.layoutControlItem10.Size = new System.Drawing.Size(334, 24);
            this.layoutControlItem10.Text = "Última Alteração";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.toggleSwitch1;
            this.layoutControlItem11.CustomizationFormText = "Status";
            this.layoutControlItem11.Location = new System.Drawing.Point(844, 24);
            this.layoutControlItem11.Name = "layoutControlItem_CLIExcluido";
            this.layoutControlItem11.Size = new System.Drawing.Size(334, 28);
            this.layoutControlItem11.Text = "Excluído";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem16
            // 
            this.emptySpaceItem16.AllowHotTrack = false;
            this.emptySpaceItem16.Location = new System.Drawing.Point(834, 0);
            this.emptySpaceItem16.Name = "emptySpaceItem10";
            this.emptySpaceItem16.Size = new System.Drawing.Size(10, 52);
            this.emptySpaceItem16.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.memoEdit1;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem12.Name = "layoutControlItem_CLIObservacao";
            this.layoutControlItem12.Size = new System.Drawing.Size(834, 52);
            this.layoutControlItem12.Text = "Observação";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(99, 13);
            // 
            // tabbedControlGroup3
            // 
            this.tabbedControlGroup3.Location = new System.Drawing.Point(10, 37);
            this.tabbedControlGroup3.Name = "tabbedControlGroup1";
            this.tabbedControlGroup3.SelectedTabPage = this.layoutControlGroup6;
            this.tabbedControlGroup3.SelectedTabPageIndex = 1;
            this.tabbedControlGroup3.Size = new System.Drawing.Size(1202, 214);
            this.tabbedControlGroup3.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup9,
            this.layoutControlGroup6});
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup4});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup_CLIContato";
            this.layoutControlGroup6.Size = new System.Drawing.Size(1178, 168);
            this.layoutControlGroup6.Text = "Contatos";
            // 
            // tabbedControlGroup4
            // 
            this.tabbedControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup4.Name = "tabbedControlGroup2";
            this.tabbedControlGroup4.SelectedTabPage = this.layoutControlGroup7;
            this.tabbedControlGroup4.SelectedTabPageIndex = 0;
            this.tabbedControlGroup4.Size = new System.Drawing.Size(1178, 168);
            this.tabbedControlGroup4.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup7,
            this.layoutControlGroup8});
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleSeparator2});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup_CLITelefone";
            this.layoutControlGroup7.Size = new System.Drawing.Size(1154, 122);
            this.layoutControlGroup7.Text = "Telefone";
            // 
            // simpleSeparator2
            // 
            this.simpleSeparator2.AllowHotTrack = false;
            this.simpleSeparator2.Location = new System.Drawing.Point(0, 0);
            this.simpleSeparator2.Name = "simpleSeparator1";
            this.simpleSeparator2.Size = new System.Drawing.Size(1154, 122);
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup8.Name = "layoutControlGroup_CLIEmail";
            this.layoutControlGroup8.Size = new System.Drawing.Size(1154, 122);
            this.layoutControlGroup8.Text = "E-mail";
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.layoutControlItem23});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup9.Name = "layoutControlGroup_CLIDados";
            this.layoutControlGroup9.Size = new System.Drawing.Size(1178, 168);
            this.layoutControlGroup9.Text = "Dados";
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.textEdit2;
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem17.Name = "layoutControlItem_CLIRazaoSocial";
            this.layoutControlItem17.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem17.Text = "Razão Social";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.textEdit3;
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem18.Name = "layoutControlItem_CLINomeFantasia";
            this.layoutControlItem18.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem18.Text = "Nome Fantasia";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.textEdit4;
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem19.Name = "layoutControlItem_CLITipo";
            this.layoutControlItem19.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem19.Text = "Tipo";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.textEdit5;
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem20.Name = "layoutControlItem_CLICnpj";
            this.layoutControlItem20.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem20.Text = "CNPJ";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.textEdit6;
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem21.Name = "layoutControlItem_CLIInscricaoEstadual";
            this.layoutControlItem21.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem21.Text = "Inscrição Estadual";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.textEdit7;
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem22.Name = "layoutControlItem_CLISuframa";
            this.layoutControlItem22.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem22.Text = "Suframa";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.textEdit14;
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItem23.Name = "layoutControlItem_CLINomeExcFiscal";
            this.layoutControlItem23.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem23.Text = "Nome Exceção Fiscal";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(99, 13);
            // 
            // simpleSeparator3
            // 
            this.simpleSeparator3.AllowHotTrack = false;
            this.simpleSeparator3.CustomizationFormText = "simpleSeparator1";
            this.simpleSeparator3.Location = new System.Drawing.Point(1005, 0);
            this.simpleSeparator3.Name = "simpleSeparator3";
            this.simpleSeparator3.Size = new System.Drawing.Size(2, 26);
            this.simpleSeparator3.Text = "simpleSeparator1";
            // 
            // layoutControlGroup_CLITelefone1
            // 
            this.layoutControlGroup_CLITelefone1.CustomizationFormText = "Telefone";
            this.layoutControlGroup_CLITelefone1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleSeparator3});
            this.layoutControlGroup_CLITelefone1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup_CLITelefone1.Name = "layoutControlGroup_CLITelefone1";
            this.layoutControlGroup_CLITelefone1.Size = new System.Drawing.Size(1205, 31);
            this.layoutControlGroup_CLITelefone1.Text = "Telefone";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup_CLITelefone1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 561);
            this.layoutControlGroup2.Name = "LayoutRootGroupForRestore";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1205, 31);
            this.layoutControlGroup2.Tag = "LayoutRootGroupForRestore";
            // 
            // colid1
            // 
            this.colid1.FieldName = "id";
            this.colid1.Name = "colid1";
            // 
            // colrazao_social
            // 
            this.colrazao_social.FieldName = "razao_social";
            this.colrazao_social.Name = "colrazao_social";
            // 
            // colnome_fantasia
            // 
            this.colnome_fantasia.FieldName = "nome_fantasia";
            this.colnome_fantasia.Name = "colnome_fantasia";
            // 
            // clienteTableAdapter
            // 
            this.clienteTableAdapter.ClearBeforeFill = true;
            // 
            // FormCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1264, 562);
            this.Controls.Add(this.tabPane_ConfigClientes);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormCliente";
            this.ShowMdiChildCaptionInParentTitle = true;
            this.Text = "Clientes";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormCliente_Load);
            this.tabNavigationPage_RegisterClientes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl_RegisterClientes)).EndInit();
            this.panelControl_RegisterClientes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_Clientes)).EndInit();
            this.layoutControl_Clientes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_CLIEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_CLIEmails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager_ContasMP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_CLITelefones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_CLITelefones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CLIRazaoSocial.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CLINomeFantasia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CLICNPJ.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CLIInscricaoEstadual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CLISuframa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CLIRua.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CLIComplemento.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CLICEP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CLIBairro.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CLICidade.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CLINomeExcFiscal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_CLIUltimaAlteracao.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_CLIUltimaAlteracao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CLIExcluido.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_CLIObservacao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CLITelefoneNumero.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CLIEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CLITipo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit_CLIEstado.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit_CLIEmailTipo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit_CLITelefoneTipo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_RegisterCliente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem_RegisterClientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_Endereco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLIRua)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLIComplemento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLICep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_Bairro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_Cidade)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLIEstado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLIUltimaAlteracao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLIExcluido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLIObservacao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup_CLIDados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CLIContato)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup_CLIContatos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CLITelefone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLITelefoneNumero)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLITelefoneTipo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CLIEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLIEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLIEmailTipo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CLIDados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLIRazaoSocial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLINomeFantasia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLITipo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLICnpj)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLIInscricaoEstadual)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLISuframa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLINomeExcFiscal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            this.tabNavigationPage_DataGridClientes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_Clientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clienteBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meusPedidosDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_Clientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane_ConfigClientes)).EndInit();
            this.tabPane_ConfigClientes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).EndInit();
            this.tabPane1.ResumeLayout(false);
            this.tabNavigationPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toggleSwitch1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CLITelefone1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Cliente;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CondicaoPagamentoCliente;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CategoriasCliente;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TabelaPreco;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Usuarios;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Produto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_ImagensProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TabelaProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TabelaPrecoProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CategoriaProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Transportadora;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CondicaoPagamento;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_FormaPagamento;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Pedido;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CamposExtraPedido;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_FaturamentoPedido;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TituloVencido;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager_Clientes;
        private DevExpress.XtraBars.Navigation.TabPane tabPane_ConfigClientes;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage_DataGridClientes;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage_RegisterClientes;
        private DevExpress.XtraBars.BarManager barManager_ContasMP;
        private DevExpress.XtraBars.Bar bar_Acoes;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Add;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Edit;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Del;
        private DevExpress.XtraBars.Bar bar_Acoes2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Gravar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Cancelar;
        private DevExpress.XtraEditors.PanelControl panelControl_RegisterClientes;
        private DevExpress.XtraLayout.LayoutControl layoutControl_Clientes;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_RegisterCliente;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem_RegisterClientes;
        private DevExpress.XtraEditors.TextEdit textEdit_CLIRazaoSocial;
        private DevExpress.XtraEditors.TextEdit textEdit_CLINomeFantasia;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_Endereco;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraEditors.TextEdit textEdit_CLICNPJ;
        private DevExpress.XtraEditors.TextEdit textEdit_CLIInscricaoEstadual;
        private DevExpress.XtraEditors.TextEdit textEdit_CLISuframa;
        private DevExpress.XtraEditors.TextEdit textEdit_CLIRua;
        private DevExpress.XtraEditors.TextEdit textEdit_CLIComplemento;
        private DevExpress.XtraEditors.TextEdit textEdit_CLICEP;
        private DevExpress.XtraEditors.TextEdit textEdit_CLIBairro;
        private DevExpress.XtraEditors.TextEdit textEdit_CLICidade;
        private DevExpress.XtraEditors.TextEdit textEdit_CLINomeExcFiscal;
        private DevExpress.XtraEditors.DateEdit dateEdit_CLIUltimaAlteracao;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_CLIRua;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_CLIComplemento;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_CLICep;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_Bairro;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_Cidade;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_CLIEstado;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_CLIUltimaAlteracao;
        private DevExpress.XtraEditors.ToggleSwitch textEdit_CLIExcluido;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_CLIExcluido;
        private DevExpress.XtraEditors.MemoEdit memoEdit_CLIObservacao;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_CLIObservacao;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup_CLIDados;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_CLIDados;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_CLIRazaoSocial;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_CLINomeFantasia;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_CLITipo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_CLICnpj;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_CLIInscricaoEstadual;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_CLISuframa;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_CLINomeExcFiscal;
        private DevExpress.XtraEditors.TextEdit textEdit_CLITelefoneNumero;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_CLIContato;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup_CLIContatos;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_CLIEmail;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_CLITelefone;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_CLITelefoneNumero;
        private DevExpress.XtraGrid.GridControl gridControl_CLITelefones;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView_CLITelefones;
        private DevExpress.XtraEditors.SimpleButton simpleButton_CLITelefoneAdd;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraBars.Navigation.TabPane tabPane1;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage1;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage2;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraEditors.TextEdit textEdit3;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        private DevExpress.XtraEditors.TextEdit textEdit6;
        private DevExpress.XtraEditors.TextEdit textEdit7;
        private DevExpress.XtraEditors.TextEdit textEdit8;
        private DevExpress.XtraEditors.TextEdit textEdit9;
        private DevExpress.XtraEditors.TextEdit textEdit10;
        private DevExpress.XtraEditors.TextEdit textEdit11;
        private DevExpress.XtraEditors.TextEdit textEdit12;
        private DevExpress.XtraEditors.TextEdit textEdit13;
        private DevExpress.XtraEditors.TextEdit textEdit14;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
        private DevExpress.XtraEditors.ToggleSwitch toggleSwitch1;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_CLITelefone1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraGrid.Columns.GridColumn colid1;
        private DevExpress.XtraGrid.Columns.GridColumn colrazao_social;
        private DevExpress.XtraGrid.Columns.GridColumn colnome_fantasia;
        //private MeusPedidosDataSet_Clientes meusPedidosDataSet_Clientes1;
        private DevExpress.XtraGrid.GridControl gridControl_CLIEmail;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView_CLIEmails;
        private DevExpress.XtraEditors.TextEdit textEdit_CLIEmail;
        private DevExpress.XtraEditors.SimpleButton simpleButton_CLIEmailAdd;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_CLIEmail;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_CLITelefoneTipo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_CLIEmailTipo;
        private DevExpress.XtraGrid.GridControl gridControl_Clientes;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView_Clientes;
        private DevExpress.XtraGrid.Columns.GridColumn colid;
        private DevExpress.XtraGrid.Columns.GridColumn colrazao_social1;
        private DevExpress.XtraGrid.Columns.GridColumn colnome_fantasia1;
        private DevExpress.XtraGrid.Columns.GridColumn coltipo;
        private DevExpress.XtraGrid.Columns.GridColumn colcnpj;
        private DevExpress.XtraGrid.Columns.GridColumn colinscricao_estadual;
        private DevExpress.XtraGrid.Columns.GridColumn colsuframa;
        private DevExpress.XtraGrid.Columns.GridColumn colrua;
        private DevExpress.XtraGrid.Columns.GridColumn colcomplemento;
        private DevExpress.XtraGrid.Columns.GridColumn colcep;
        private DevExpress.XtraGrid.Columns.GridColumn colbairro;
        private DevExpress.XtraGrid.Columns.GridColumn colcidade;
        private DevExpress.XtraGrid.Columns.GridColumn colestado;
        private DevExpress.XtraGrid.Columns.GridColumn colobservacao;
        private DevExpress.XtraGrid.Columns.GridColumn colnome_excecao_fiscal;
        private DevExpress.XtraGrid.Columns.GridColumn colexcluido;
        private DevExpress.XtraGrid.Columns.GridColumn colultima_alteracao;
        private MeusPedidosDataSet meusPedidosDataSet;
        private System.Windows.Forms.BindingSource clienteBindingSource;
        private MeusPedidosDataSetTableAdapters.ClienteTableAdapter clienteTableAdapter;
        private DevExpress.XtraEditors.LookUpEdit textEdit_CLITipo;
        private DevExpress.XtraEditors.LookUpEdit comboBoxEdit_CLIEstado;
        private DevExpress.XtraEditors.LookUpEdit comboBoxEdit_CLIEmailTipo;
        private DevExpress.XtraEditors.LookUpEdit comboBoxEdit_CLITelefoneTipo;
    }
}


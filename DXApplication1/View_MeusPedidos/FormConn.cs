﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Core;

namespace View_MeusPedidos
{
    public partial class FormConn : Form
    {
        public BancoDados bd_conn;

        public FormConn()
        {
            InitializeComponent();
        }

        private void simpleButton_Conectar_Click(object sender, EventArgs e)
        {
            bd_conn = new BancoDados();

            bd_conn.nome_conexao = "Conn";
            bd_conn.nome_servidor = textEdit_NomeServer.Text;
            bd_conn.nome_bancodados = textEdit_NomeDB.Text;
            bd_conn.usuario_bancodados = textEdit_UsuarioDB.Text;
            bd_conn.senha_bancodados = textEdit_SenhaDB.Text;
            bd_conn.autenticacao_segura = checkEdit_AutenticacaoSegura.Checked;

            this.Hide();
        }
    }
}

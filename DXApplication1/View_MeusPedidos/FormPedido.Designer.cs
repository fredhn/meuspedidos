﻿namespace View_MeusPedidos
{
    partial class FormPedido
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement10 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement11 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement12 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement13 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement14 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement15 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement16 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement17 = new DevExpress.XtraEditors.TileItemElement();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPedido));
            this.tileNavSubItem_Cliente = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CondicaoPagamentoCliente = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CategoriasCliente = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TabelaPreco = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Usuarios = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Produto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_ImagensProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TabelaProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TabelaPrecoProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CategoriaProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Pedido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CamposExtraPedido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Transportadora = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CondicaoPagamento = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_FormaPagamento = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_FaturamentoPedido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TituloVencido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.splashScreenManager_Clientes = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::View_MeusPedidos.WaitForm1), true, true);
            this.tabNavigationPage_RegisterPedido = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.panelControl_RegisterPedido = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl_Pedido = new DevExpress.XtraLayout.LayoutControl();
            this.gridControl_CampoExtra = new DevExpress.XtraGrid.GridControl();
            this.gridView_CampoExtra = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.barManager_ContasMP = new DevExpress.XtraBars.BarManager(this.components);
            this.bar_Acoes = new DevExpress.XtraBars.Bar();
            this.barButtonItem_Add = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem_Edit = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem_Del = new DevExpress.XtraBars.BarButtonItem();
            this.bar_Acoes2 = new DevExpress.XtraBars.Bar();
            this.barButtonItem_Gravar = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem_Cancelar = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.gridControl_PedidoItens = new DevExpress.XtraGrid.GridControl();
            this.gridView_PedidoItens = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.dateEdit_CLIUltimaAlteracao = new DevExpress.XtraEditors.DateEdit();
            this.memoEdit_CLIObservacao = new DevExpress.XtraEditors.MemoEdit();
            this.textEdit_PedidoData = new DevExpress.XtraEditors.DateEdit();
            this.textEdit_PedidoTransportadora = new DevExpress.XtraEditors.LookUpEdit();
            this.textEdit_PedidoCondPag = new DevExpress.XtraEditors.LookUpEdit();
            this.simpleButton_GradeCorTamAdd = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit_PedidoClienteId = new DevExpress.XtraEditors.LookUpEdit();
            this.simpleButton_CampoExtraAdd = new DevExpress.XtraEditors.SimpleButton();
            this.comboBoxEdit_CampoExtraTipo = new DevExpress.XtraEditors.LookUpEdit();
            this.pedidoCampoExtraBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.meusPedidosDataSet = new View_MeusPedidos.MeusPedidosDataSet();
            this.textEdit_PedidoStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit_PedidoFormaPag = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup_RegisterPedido = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleLabelItem_RegisterPedido = new DevExpress.XtraLayout.SimpleLabelItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup_Pedido = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem_PedidoTransportadora = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_PedidoData = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem18 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem_PedidoCondPag = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_PedidoClienteId = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem28 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem_PedidoFormaPag = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem_CLIUltimaAlteracao = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_PedidoStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem_CLIObservacao = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem17 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup_PedidoItems = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_GradeCorTamAdd = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem24 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem22 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem20 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem23 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup_CamposExtra = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem25 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem19 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem26 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem27 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem_CampoExtraTipo = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabNavigationPage_DataGridPedido = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.gridControl_Pedido = new DevExpress.XtraGrid.GridControl();
            this.gridView_Pedidos = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcliente_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colrepresentada_nome_fantasia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coltransportadora_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcriador_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colnumero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coltotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcondicao_pagamento_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colforma_pagamento_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldata_emissao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colobservacoes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstatus_faturamento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colultima_alteracao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colmptran = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tabPane_ConfigPedidos = new DevExpress.XtraBars.Navigation.TabPane();
            this.tabPane1 = new DevExpress.XtraBars.Navigation.TabPane();
            this.tabNavigationPage1 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.tabNavigationPage2 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit8 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit9 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit10 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit11 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit12 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit13 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit14 = new DevExpress.XtraEditors.TextEdit();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.toggleSwitch1 = new DevExpress.XtraEditors.ToggleSwitch();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleLabelItem1 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem16 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup3 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup4 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.simpleSeparator2 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator3 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlGroup_CLITelefone1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.colid1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colrazao_social = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colnome_fantasia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.emptySpaceItem21 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.pedidoCampoExtraTableAdapter = new View_MeusPedidos.MeusPedidosDataSetTableAdapters.PedidoCampoExtraTableAdapter();
            this.tabNavigationPage_RegisterPedido.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl_RegisterPedido)).BeginInit();
            this.panelControl_RegisterPedido.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_Pedido)).BeginInit();
            this.layoutControl_Pedido.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_CampoExtra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_CampoExtra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager_ContasMP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_PedidoItens)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_PedidoItens)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_CLIUltimaAlteracao.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_CLIUltimaAlteracao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_CLIObservacao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_PedidoData.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_PedidoData.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_PedidoTransportadora.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_PedidoCondPag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_PedidoClienteId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit_CampoExtraTipo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pedidoCampoExtraBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meusPedidosDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_PedidoStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_PedidoFormaPag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_RegisterPedido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem_RegisterPedido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_Pedido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_PedidoTransportadora)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_PedidoData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_PedidoCondPag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_PedidoClienteId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_PedidoFormaPag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLIUltimaAlteracao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_PedidoStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLIObservacao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_PedidoItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_GradeCorTamAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CamposExtra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CampoExtraTipo)).BeginInit();
            this.tabNavigationPage_DataGridPedido.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_Pedido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_Pedidos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane_ConfigPedidos)).BeginInit();
            this.tabPane_ConfigPedidos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).BeginInit();
            this.tabPane1.SuspendLayout();
            this.tabNavigationPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toggleSwitch1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CLITelefone1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            this.SuspendLayout();
            // 
            // tileNavSubItem_Cliente
            // 
            this.tileNavSubItem_Cliente.Caption = "Cliente";
            this.tileNavSubItem_Cliente.Name = "tileNavSubItem_Cliente";
            // 
            // 
            // 
            this.tileNavSubItem_Cliente.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement1.Text = "Cliente";
            this.tileNavSubItem_Cliente.Tile.Elements.Add(tileItemElement1);
            this.tileNavSubItem_Cliente.Tile.Name = "tileBarItem3";
            // 
            // tileNavSubItem_CondicaoPagamentoCliente
            // 
            this.tileNavSubItem_CondicaoPagamentoCliente.Caption = "Condições de Pagamento por Cliente";
            this.tileNavSubItem_CondicaoPagamentoCliente.Name = "tileNavSubItem_CondicaoPagamentoCliente";
            // 
            // 
            // 
            this.tileNavSubItem_CondicaoPagamentoCliente.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement2.Text = "Condições de Pagamento por Cliente";
            this.tileNavSubItem_CondicaoPagamentoCliente.Tile.Elements.Add(tileItemElement2);
            this.tileNavSubItem_CondicaoPagamentoCliente.Tile.Name = "tileBarItem4";
            // 
            // tileNavSubItem_CategoriasCliente
            // 
            this.tileNavSubItem_CategoriasCliente.Caption = "Categorias por Cliente";
            this.tileNavSubItem_CategoriasCliente.Name = "tileNavSubItem_CategoriasCliente";
            // 
            // 
            // 
            this.tileNavSubItem_CategoriasCliente.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement3.Text = "Categorias por Cliente";
            this.tileNavSubItem_CategoriasCliente.Tile.Elements.Add(tileItemElement3);
            this.tileNavSubItem_CategoriasCliente.Tile.Name = "tileBarItem6";
            // 
            // tileNavSubItem_TabelaPreco
            // 
            this.tileNavSubItem_TabelaPreco.Caption = "Tabelas de Preço por Cliente";
            this.tileNavSubItem_TabelaPreco.Name = "tileNavSubItem_TabelaPreco";
            // 
            // 
            // 
            this.tileNavSubItem_TabelaPreco.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement4.Text = "Tabelas de Preço por Cliente";
            this.tileNavSubItem_TabelaPreco.Tile.Elements.Add(tileItemElement4);
            this.tileNavSubItem_TabelaPreco.Tile.Name = "tileBarItem7";
            // 
            // tileNavSubItem_Usuarios
            // 
            this.tileNavSubItem_Usuarios.Caption = "Usuários (Vendedores)";
            this.tileNavSubItem_Usuarios.Name = "tileNavSubItem_Usuarios";
            // 
            // 
            // 
            this.tileNavSubItem_Usuarios.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement5.Text = "Usuários (Vendedores)";
            this.tileNavSubItem_Usuarios.Tile.Elements.Add(tileItemElement5);
            this.tileNavSubItem_Usuarios.Tile.Name = "tileBarItem8";
            // 
            // tileNavSubItem_Produto
            // 
            this.tileNavSubItem_Produto.Caption = "Produto";
            this.tileNavSubItem_Produto.Name = "tileNavSubItem_Produto";
            // 
            // 
            // 
            this.tileNavSubItem_Produto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement6.Text = "Produto";
            this.tileNavSubItem_Produto.Tile.Elements.Add(tileItemElement6);
            this.tileNavSubItem_Produto.Tile.Name = "tileBarItem9";
            // 
            // tileNavSubItem_ImagensProduto
            // 
            this.tileNavSubItem_ImagensProduto.Caption = "Imagens do Produto";
            this.tileNavSubItem_ImagensProduto.Name = "tileNavSubItem_ImagensProduto";
            // 
            // 
            // 
            this.tileNavSubItem_ImagensProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement7.Text = "Imagens do Produto";
            this.tileNavSubItem_ImagensProduto.Tile.Elements.Add(tileItemElement7);
            this.tileNavSubItem_ImagensProduto.Tile.Name = "tileBarItem10";
            // 
            // tileNavSubItem_TabelaProduto
            // 
            this.tileNavSubItem_TabelaProduto.Caption = "Tabela de Preço";
            this.tileNavSubItem_TabelaProduto.Name = "tileNavSubItem_TabelaProduto";
            // 
            // 
            // 
            this.tileNavSubItem_TabelaProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement8.Text = "Tabelas de Preço";
            this.tileNavSubItem_TabelaProduto.Tile.Elements.Add(tileItemElement8);
            this.tileNavSubItem_TabelaProduto.Tile.Name = "tileBarItem11";
            // 
            // tileNavSubItem_TabelaPrecoProduto
            // 
            this.tileNavSubItem_TabelaPrecoProduto.Caption = "Tabelas de Preço por Produto";
            this.tileNavSubItem_TabelaPrecoProduto.Name = "tileNavSubItem_TabelaPrecoProduto";
            // 
            // 
            // 
            this.tileNavSubItem_TabelaPrecoProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement9.Text = "Tabelas de Preço por Produto";
            this.tileNavSubItem_TabelaPrecoProduto.Tile.Elements.Add(tileItemElement9);
            this.tileNavSubItem_TabelaPrecoProduto.Tile.Name = "tileBarItem12";
            // 
            // tileNavSubItem_CategoriaProduto
            // 
            this.tileNavSubItem_CategoriaProduto.Caption = "Categorias de Produtos";
            this.tileNavSubItem_CategoriaProduto.Name = "tileNavSubItem_CategoriaProduto";
            // 
            // 
            // 
            this.tileNavSubItem_CategoriaProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement10.Text = "Categorias de Produtos";
            this.tileNavSubItem_CategoriaProduto.Tile.Elements.Add(tileItemElement10);
            this.tileNavSubItem_CategoriaProduto.Tile.Name = "tileBarItem13";
            // 
            // tileNavSubItem_Pedido
            // 
            this.tileNavSubItem_Pedido.Caption = "Pedido";
            this.tileNavSubItem_Pedido.Name = "tileNavSubItem_Pedido";
            // 
            // 
            // 
            this.tileNavSubItem_Pedido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement11.Text = "Pedido";
            this.tileNavSubItem_Pedido.Tile.Elements.Add(tileItemElement11);
            this.tileNavSubItem_Pedido.Tile.Name = "tileBarItem17";
            // 
            // tileNavSubItem_CamposExtraPedido
            // 
            this.tileNavSubItem_CamposExtraPedido.Caption = "Campos Extras do Pedido";
            this.tileNavSubItem_CamposExtraPedido.Name = "tileNavSubItem_CamposExtraPedido";
            // 
            // 
            // 
            this.tileNavSubItem_CamposExtraPedido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement12.Text = "Campos Extras do Pedido";
            this.tileNavSubItem_CamposExtraPedido.Tile.Elements.Add(tileItemElement12);
            this.tileNavSubItem_CamposExtraPedido.Tile.Name = "tileBarItem18";
            // 
            // tileNavSubItem_Transportadora
            // 
            this.tileNavSubItem_Transportadora.Caption = "Transportadoras";
            this.tileNavSubItem_Transportadora.Name = "tileNavSubItem_Transportadora";
            // 
            // 
            // 
            this.tileNavSubItem_Transportadora.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement13.Text = "Transportadoras";
            this.tileNavSubItem_Transportadora.Tile.Elements.Add(tileItemElement13);
            this.tileNavSubItem_Transportadora.Tile.Name = "tileBarItem14";
            // 
            // tileNavSubItem_CondicaoPagamento
            // 
            this.tileNavSubItem_CondicaoPagamento.Caption = "Condições de Pagamento";
            this.tileNavSubItem_CondicaoPagamento.Name = "tileNavSubItem_CondicaoPagamento";
            // 
            // 
            // 
            this.tileNavSubItem_CondicaoPagamento.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement14.Text = "Condições de Pagamento";
            this.tileNavSubItem_CondicaoPagamento.Tile.Elements.Add(tileItemElement14);
            this.tileNavSubItem_CondicaoPagamento.Tile.Name = "tileBarItem15";
            // 
            // tileNavSubItem_FormaPagamento
            // 
            this.tileNavSubItem_FormaPagamento.Caption = "Formas de Pagamento";
            this.tileNavSubItem_FormaPagamento.Name = "tileNavSubItem_FormaPagamento";
            // 
            // 
            // 
            this.tileNavSubItem_FormaPagamento.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement15.Text = "Formas de Pagamento";
            this.tileNavSubItem_FormaPagamento.Tile.Elements.Add(tileItemElement15);
            this.tileNavSubItem_FormaPagamento.Tile.Name = "tileBarItem16";
            // 
            // tileNavSubItem_FaturamentoPedido
            // 
            this.tileNavSubItem_FaturamentoPedido.Caption = "Faturamento de Pedido";
            this.tileNavSubItem_FaturamentoPedido.Name = "tileNavSubItem_FaturamentoPedido";
            // 
            // 
            // 
            this.tileNavSubItem_FaturamentoPedido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement16.Text = "Faturamento de Pedido";
            this.tileNavSubItem_FaturamentoPedido.Tile.Elements.Add(tileItemElement16);
            this.tileNavSubItem_FaturamentoPedido.Tile.Name = "tileBarItem19";
            // 
            // tileNavSubItem_TituloVencido
            // 
            this.tileNavSubItem_TituloVencido.Caption = "Títulos Vencidos";
            this.tileNavSubItem_TituloVencido.Name = "tileNavSubItem_TituloVencido";
            // 
            // 
            // 
            this.tileNavSubItem_TituloVencido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement17.Text = "Títulos Vencidos";
            this.tileNavSubItem_TituloVencido.Tile.Elements.Add(tileItemElement17);
            this.tileNavSubItem_TituloVencido.Tile.Name = "tileBarItem20";
            // 
            // splashScreenManager_Clientes
            // 
            this.splashScreenManager_Clientes.ClosingDelay = 500;
            // 
            // tabNavigationPage_RegisterPedido
            // 
            this.tabNavigationPage_RegisterPedido.Caption = "Cadastro";
            this.tabNavigationPage_RegisterPedido.Controls.Add(this.panelControl_RegisterPedido);
            this.tabNavigationPage_RegisterPedido.Image = ((System.Drawing.Image)(resources.GetObject("tabNavigationPage_RegisterPedido.Image")));
            this.tabNavigationPage_RegisterPedido.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage_RegisterPedido.Name = "tabNavigationPage_RegisterPedido";
            this.tabNavigationPage_RegisterPedido.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage_RegisterPedido.Size = new System.Drawing.Size(1246, 404);
            // 
            // panelControl_RegisterPedido
            // 
            this.panelControl_RegisterPedido.Controls.Add(this.layoutControl_Pedido);
            this.panelControl_RegisterPedido.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl_RegisterPedido.Location = new System.Drawing.Point(0, 0);
            this.panelControl_RegisterPedido.Name = "panelControl_RegisterPedido";
            this.panelControl_RegisterPedido.Size = new System.Drawing.Size(1246, 404);
            this.panelControl_RegisterPedido.TabIndex = 0;
            // 
            // layoutControl_Pedido
            // 
            this.layoutControl_Pedido.Controls.Add(this.gridControl_CampoExtra);
            this.layoutControl_Pedido.Controls.Add(this.gridControl_PedidoItens);
            this.layoutControl_Pedido.Controls.Add(this.dateEdit_CLIUltimaAlteracao);
            this.layoutControl_Pedido.Controls.Add(this.memoEdit_CLIObservacao);
            this.layoutControl_Pedido.Controls.Add(this.textEdit_PedidoData);
            this.layoutControl_Pedido.Controls.Add(this.textEdit_PedidoTransportadora);
            this.layoutControl_Pedido.Controls.Add(this.textEdit_PedidoCondPag);
            this.layoutControl_Pedido.Controls.Add(this.simpleButton_GradeCorTamAdd);
            this.layoutControl_Pedido.Controls.Add(this.textEdit_PedidoClienteId);
            this.layoutControl_Pedido.Controls.Add(this.simpleButton_CampoExtraAdd);
            this.layoutControl_Pedido.Controls.Add(this.comboBoxEdit_CampoExtraTipo);
            this.layoutControl_Pedido.Controls.Add(this.textEdit_PedidoStatus);
            this.layoutControl_Pedido.Controls.Add(this.textEdit_PedidoFormaPag);
            this.layoutControl_Pedido.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl_Pedido.Location = new System.Drawing.Point(2, 2);
            this.layoutControl_Pedido.Name = "layoutControl_Pedido";
            this.layoutControl_Pedido.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(502, 155, 450, 400);
            this.layoutControl_Pedido.Root = this.layoutControlGroup_RegisterPedido;
            this.layoutControl_Pedido.Size = new System.Drawing.Size(1242, 400);
            this.layoutControl_Pedido.TabIndex = 0;
            this.layoutControl_Pedido.Text = "layoutControl_Pedido";
            // 
            // gridControl_CampoExtra
            // 
            this.gridControl_CampoExtra.Location = new System.Drawing.Point(44, 431);
            this.gridControl_CampoExtra.MainView = this.gridView_CampoExtra;
            this.gridControl_CampoExtra.MenuManager = this.barManager_ContasMP;
            this.gridControl_CampoExtra.MinimumSize = new System.Drawing.Size(0, 80);
            this.gridControl_CampoExtra.Name = "gridControl_CampoExtra";
            this.gridControl_CampoExtra.Size = new System.Drawing.Size(1137, 80);
            this.gridControl_CampoExtra.TabIndex = 32;
            this.gridControl_CampoExtra.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView_CampoExtra});
            this.gridControl_CampoExtra.ProcessGridKey += new System.Windows.Forms.KeyEventHandler(this.gridControl_CampoExtra_ProcessGridKey);
            // 
            // gridView_CampoExtra
            // 
            this.gridView_CampoExtra.GridControl = this.gridControl_CampoExtra;
            this.gridView_CampoExtra.Name = "gridView_CampoExtra";
            this.gridView_CampoExtra.OptionsView.ShowGroupPanel = false;
            this.gridView_CampoExtra.OptionsView.ShowIndicator = false;
            this.gridView_CampoExtra.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView_CampoExtra_CustomRowCellEdit);
            this.gridView_CampoExtra.LostFocus += new System.EventHandler(this.gridView_CampoExtra_LostFocus);
            // 
            // barManager_ContasMP
            // 
            this.barManager_ContasMP.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar_Acoes,
            this.bar_Acoes2});
            this.barManager_ContasMP.DockControls.Add(this.barDockControlTop);
            this.barManager_ContasMP.DockControls.Add(this.barDockControlBottom);
            this.barManager_ContasMP.DockControls.Add(this.barDockControlLeft);
            this.barManager_ContasMP.DockControls.Add(this.barDockControlRight);
            this.barManager_ContasMP.Form = this;
            this.barManager_ContasMP.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem_Add,
            this.barButtonItem_Edit,
            this.barButtonItem_Del,
            this.barButtonItem_Gravar,
            this.barButtonItem_Cancelar});
            this.barManager_ContasMP.MaxItemId = 5;
            // 
            // bar_Acoes
            // 
            this.bar_Acoes.BarName = "Tools";
            this.bar_Acoes.DockCol = 0;
            this.bar_Acoes.DockRow = 0;
            this.bar_Acoes.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar_Acoes.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Add),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Edit),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Del)});
            this.bar_Acoes.OptionsBar.UseWholeRow = true;
            this.bar_Acoes.Text = "Tools";
            // 
            // barButtonItem_Add
            // 
            this.barButtonItem_Add.Caption = "Adicionar";
            this.barButtonItem_Add.Id = 0;
            this.barButtonItem_Add.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Add.ImageOptions.Image")));
            this.barButtonItem_Add.Name = "barButtonItem_Add";
            this.barButtonItem_Add.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Add.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Add_ItemClick);
            // 
            // barButtonItem_Edit
            // 
            this.barButtonItem_Edit.Caption = "Editar";
            this.barButtonItem_Edit.Id = 1;
            this.barButtonItem_Edit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Edit.ImageOptions.Image")));
            this.barButtonItem_Edit.Name = "barButtonItem_Edit";
            this.barButtonItem_Edit.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Edit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Edit_ItemClick);
            // 
            // barButtonItem_Del
            // 
            this.barButtonItem_Del.Caption = "Excluir";
            this.barButtonItem_Del.Id = 2;
            this.barButtonItem_Del.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Del.ImageOptions.Image")));
            this.barButtonItem_Del.Name = "barButtonItem_Del";
            this.barButtonItem_Del.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Del.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Del_ItemClick);
            // 
            // bar_Acoes2
            // 
            this.bar_Acoes2.BarName = "Tools_GravarCancelar";
            this.bar_Acoes2.DockCol = 0;
            this.bar_Acoes2.DockRow = 1;
            this.bar_Acoes2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar_Acoes2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Gravar),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Cancelar)});
            this.bar_Acoes2.OptionsBar.AllowRename = true;
            this.bar_Acoes2.OptionsBar.UseWholeRow = true;
            this.bar_Acoes2.Text = "Tools_GravarCancelar";
            // 
            // barButtonItem_Gravar
            // 
            this.barButtonItem_Gravar.Caption = "Gravar";
            this.barButtonItem_Gravar.Id = 3;
            this.barButtonItem_Gravar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Gravar.ImageOptions.Image")));
            this.barButtonItem_Gravar.Name = "barButtonItem_Gravar";
            this.barButtonItem_Gravar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Gravar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Gravar_ItemClick);
            // 
            // barButtonItem_Cancelar
            // 
            this.barButtonItem_Cancelar.Caption = "Cancelar";
            this.barButtonItem_Cancelar.Id = 4;
            this.barButtonItem_Cancelar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Cancelar.ImageOptions.Image")));
            this.barButtonItem_Cancelar.Name = "barButtonItem_Cancelar";
            this.barButtonItem_Cancelar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Cancelar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Cancelar_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager_ContasMP;
            this.barDockControlTop.Size = new System.Drawing.Size(1264, 94);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 562);
            this.barDockControlBottom.Manager = this.barManager_ContasMP;
            this.barDockControlBottom.Size = new System.Drawing.Size(1264, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 94);
            this.barDockControlLeft.Manager = this.barManager_ContasMP;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 468);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1264, 94);
            this.barDockControlRight.Manager = this.barManager_ContasMP;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 468);
            // 
            // gridControl_PedidoItens
            // 
            this.gridControl_PedidoItens.Location = new System.Drawing.Point(44, 249);
            this.gridControl_PedidoItens.MainView = this.gridView_PedidoItens;
            this.gridControl_PedidoItens.MenuManager = this.barManager_ContasMP;
            this.gridControl_PedidoItens.MinimumSize = new System.Drawing.Size(0, 80);
            this.gridControl_PedidoItens.Name = "gridControl_PedidoItens";
            this.gridControl_PedidoItens.Size = new System.Drawing.Size(1137, 80);
            this.gridControl_PedidoItens.TabIndex = 30;
            this.gridControl_PedidoItens.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView_PedidoItens});
            this.gridControl_PedidoItens.Visible = false;
            this.gridControl_PedidoItens.ProcessGridKey += new System.Windows.Forms.KeyEventHandler(this.gridControl_PedidoItens_ProcessGridKey);
            // 
            // gridView_PedidoItens
            // 
            this.gridView_PedidoItens.GridControl = this.gridControl_PedidoItens;
            this.gridView_PedidoItens.Name = "gridView_PedidoItens";
            this.gridView_PedidoItens.OptionsBehavior.Editable = false;
            this.gridView_PedidoItens.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView_PedidoItens.OptionsView.ShowColumnHeaders = false;
            this.gridView_PedidoItens.OptionsView.ShowGroupPanel = false;
            this.gridView_PedidoItens.OptionsView.ShowIndicator = false;
            // 
            // dateEdit_CLIUltimaAlteracao
            // 
            this.dateEdit_CLIUltimaAlteracao.EditValue = null;
            this.dateEdit_CLIUltimaAlteracao.Enabled = false;
            this.dateEdit_CLIUltimaAlteracao.Location = new System.Drawing.Point(554, 577);
            this.dateEdit_CLIUltimaAlteracao.MenuManager = this.barManager_ContasMP;
            this.dateEdit_CLIUltimaAlteracao.Name = "dateEdit_CLIUltimaAlteracao";
            this.dateEdit_CLIUltimaAlteracao.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit_CLIUltimaAlteracao.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit_CLIUltimaAlteracao.Properties.ReadOnly = true;
            this.dateEdit_CLIUltimaAlteracao.Size = new System.Drawing.Size(637, 20);
            this.dateEdit_CLIUltimaAlteracao.StyleController = this.layoutControl_Pedido;
            this.dateEdit_CLIUltimaAlteracao.TabIndex = 17;
            // 
            // memoEdit_CLIObservacao
            // 
            this.memoEdit_CLIObservacao.Location = new System.Drawing.Point(153, 577);
            this.memoEdit_CLIObservacao.MenuManager = this.barManager_ContasMP;
            this.memoEdit_CLIObservacao.Name = "memoEdit_CLIObservacao";
            this.memoEdit_CLIObservacao.Size = new System.Drawing.Size(268, 44);
            this.memoEdit_CLIObservacao.StyleController = this.layoutControl_Pedido;
            this.memoEdit_CLIObservacao.TabIndex = 18;
            // 
            // textEdit_PedidoData
            // 
            this.textEdit_PedidoData.EditValue = null;
            this.textEdit_PedidoData.Location = new System.Drawing.Point(745, 79);
            this.textEdit_PedidoData.MenuManager = this.barManager_ContasMP;
            this.textEdit_PedidoData.Name = "textEdit_PedidoData";
            this.textEdit_PedidoData.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.textEdit_PedidoData.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.textEdit_PedidoData.Properties.DisplayFormat.FormatString = "";
            this.textEdit_PedidoData.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.textEdit_PedidoData.Properties.EditFormat.FormatString = "";
            this.textEdit_PedidoData.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.textEdit_PedidoData.Properties.Mask.EditMask = "";
            this.textEdit_PedidoData.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.textEdit_PedidoData.Size = new System.Drawing.Size(446, 20);
            this.textEdit_PedidoData.StyleController = this.layoutControl_Pedido;
            this.textEdit_PedidoData.TabIndex = 11;
            // 
            // textEdit_PedidoTransportadora
            // 
            this.textEdit_PedidoTransportadora.Location = new System.Drawing.Point(153, 103);
            this.textEdit_PedidoTransportadora.MenuManager = this.barManager_ContasMP;
            this.textEdit_PedidoTransportadora.Name = "textEdit_PedidoTransportadora";
            this.textEdit_PedidoTransportadora.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.textEdit_PedidoTransportadora.Properties.DisplayMember = "nome";
            this.textEdit_PedidoTransportadora.Properties.MaxLength = 8;
            this.textEdit_PedidoTransportadora.Properties.NullText = "";
            this.textEdit_PedidoTransportadora.Properties.ValueMember = "id";
            this.textEdit_PedidoTransportadora.Size = new System.Drawing.Size(453, 20);
            this.textEdit_PedidoTransportadora.StyleController = this.layoutControl_Pedido;
            this.textEdit_PedidoTransportadora.TabIndex = 12;
            // 
            // textEdit_PedidoCondPag
            // 
            this.textEdit_PedidoCondPag.Location = new System.Drawing.Point(745, 103);
            this.textEdit_PedidoCondPag.MenuManager = this.barManager_ContasMP;
            this.textEdit_PedidoCondPag.Name = "textEdit_PedidoCondPag";
            this.textEdit_PedidoCondPag.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.textEdit_PedidoCondPag.Properties.DisplayMember = "nome";
            this.textEdit_PedidoCondPag.Properties.NullText = "";
            this.textEdit_PedidoCondPag.Properties.ValueMember = "id";
            this.textEdit_PedidoCondPag.Size = new System.Drawing.Size(446, 20);
            this.textEdit_PedidoCondPag.StyleController = this.layoutControl_Pedido;
            this.textEdit_PedidoCondPag.TabIndex = 13;
            // 
            // simpleButton_GradeCorTamAdd
            // 
            this.simpleButton_GradeCorTamAdd.Appearance.ForeColor = System.Drawing.SystemColors.Highlight;
            this.simpleButton_GradeCorTamAdd.Appearance.Options.UseForeColor = true;
            this.simpleButton_GradeCorTamAdd.Location = new System.Drawing.Point(34, 213);
            this.simpleButton_GradeCorTamAdd.Name = "simpleButton_GradeCorTamAdd";
            this.simpleButton_GradeCorTamAdd.Size = new System.Drawing.Size(1157, 22);
            this.simpleButton_GradeCorTamAdd.StyleController = this.layoutControl_Pedido;
            this.simpleButton_GradeCorTamAdd.TabIndex = 23;
            this.simpleButton_GradeCorTamAdd.Text = "Adicionar itens ao pedido";
            this.simpleButton_GradeCorTamAdd.Click += new System.EventHandler(this.simpleButton_GradeCorTamAdd_Click);
            // 
            // textEdit_PedidoClienteId
            // 
            this.textEdit_PedidoClienteId.Location = new System.Drawing.Point(153, 79);
            this.textEdit_PedidoClienteId.MenuManager = this.barManager_ContasMP;
            this.textEdit_PedidoClienteId.Name = "textEdit_PedidoClienteId";
            this.textEdit_PedidoClienteId.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.textEdit_PedidoClienteId.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("id", "id", 31, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("razao_social", "razao_social", 69, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("nome_fantasia", "nome_fantasia", 81, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("tipo", "tipo", 28, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("cnpj", "cnpj", 30, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("cidade", "cidade", 41, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("estado", "estado", 43, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("observacao", "observacao", 66, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.textEdit_PedidoClienteId.Properties.DisplayMember = "razao_social";
            this.textEdit_PedidoClienteId.Properties.NullText = "";
            this.textEdit_PedidoClienteId.Properties.ValueMember = "id";
            this.textEdit_PedidoClienteId.Size = new System.Drawing.Size(453, 20);
            this.textEdit_PedidoClienteId.StyleController = this.layoutControl_Pedido;
            this.textEdit_PedidoClienteId.TabIndex = 26;
            // 
            // simpleButton_CampoExtraAdd
            // 
            this.simpleButton_CampoExtraAdd.Appearance.ForeColor = System.Drawing.SystemColors.Highlight;
            this.simpleButton_CampoExtraAdd.Appearance.Options.UseForeColor = true;
            this.simpleButton_CampoExtraAdd.Location = new System.Drawing.Point(298, 395);
            this.simpleButton_CampoExtraAdd.Name = "simpleButton_CampoExtraAdd";
            this.simpleButton_CampoExtraAdd.Size = new System.Drawing.Size(893, 22);
            this.simpleButton_CampoExtraAdd.StyleController = this.layoutControl_Pedido;
            this.simpleButton_CampoExtraAdd.TabIndex = 31;
            this.simpleButton_CampoExtraAdd.Text = "Add Campo Extra";
            this.simpleButton_CampoExtraAdd.Click += new System.EventHandler(this.simpleButton_CampoExtraAdd_Click);
            // 
            // comboBoxEdit_CampoExtraTipo
            // 
            this.comboBoxEdit_CampoExtraTipo.Location = new System.Drawing.Point(124, 395);
            this.comboBoxEdit_CampoExtraTipo.MenuManager = this.barManager_ContasMP;
            this.comboBoxEdit_CampoExtraTipo.Name = "comboBoxEdit_CampoExtraTipo";
            this.comboBoxEdit_CampoExtraTipo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit_CampoExtraTipo.Properties.DataSource = this.pedidoCampoExtraBindingSource;
            this.comboBoxEdit_CampoExtraTipo.Properties.DisplayMember = "nome";
            this.comboBoxEdit_CampoExtraTipo.Properties.NullText = "";
            this.comboBoxEdit_CampoExtraTipo.Properties.PopupSizeable = false;
            this.comboBoxEdit_CampoExtraTipo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.comboBoxEdit_CampoExtraTipo.Properties.ValueMember = "id";
            this.comboBoxEdit_CampoExtraTipo.Size = new System.Drawing.Size(170, 20);
            this.comboBoxEdit_CampoExtraTipo.StyleController = this.layoutControl_Pedido;
            this.comboBoxEdit_CampoExtraTipo.TabIndex = 33;
            // 
            // pedidoCampoExtraBindingSource
            // 
            this.pedidoCampoExtraBindingSource.DataMember = "PedidoCampoExtra";
            this.pedidoCampoExtraBindingSource.DataSource = this.meusPedidosDataSet;
            // 
            // meusPedidosDataSet
            // 
            this.meusPedidosDataSet.DataSetName = "MeusPedidosDataSet";
            this.meusPedidosDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // textEdit_PedidoStatus
            // 
            this.textEdit_PedidoStatus.EditValue = "1";
            this.textEdit_PedidoStatus.Location = new System.Drawing.Point(554, 601);
            this.textEdit_PedidoStatus.Name = "textEdit_PedidoStatus";
            this.textEdit_PedidoStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.textEdit_PedidoStatus.Properties.Items.AddRange(new object[] {
            "0 = Cancelado",
            "1 = Orçamento",
            "2 = Pedido"});
            this.textEdit_PedidoStatus.Size = new System.Drawing.Size(637, 20);
            this.textEdit_PedidoStatus.StyleController = this.layoutControl_Pedido;
            this.textEdit_PedidoStatus.TabIndex = 10;
            // 
            // textEdit_PedidoFormaPag
            // 
            this.textEdit_PedidoFormaPag.Location = new System.Drawing.Point(745, 127);
            this.textEdit_PedidoFormaPag.Name = "textEdit_PedidoFormaPag";
            this.textEdit_PedidoFormaPag.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.textEdit_PedidoFormaPag.Properties.DisplayMember = "nome";
            this.textEdit_PedidoFormaPag.Properties.NullText = "";
            this.textEdit_PedidoFormaPag.Properties.ValueMember = "id";
            this.textEdit_PedidoFormaPag.Size = new System.Drawing.Size(446, 20);
            this.textEdit_PedidoFormaPag.StyleController = this.layoutControl_Pedido;
            this.textEdit_PedidoFormaPag.TabIndex = 13;
            // 
            // layoutControlGroup_RegisterPedido
            // 
            this.layoutControlGroup_RegisterPedido.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup_RegisterPedido.GroupBordersVisible = false;
            this.layoutControlGroup_RegisterPedido.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.emptySpaceItem4,
            this.simpleLabelItem_RegisterPedido,
            this.emptySpaceItem5,
            this.layoutControlGroup_Pedido,
            this.emptySpaceItem9,
            this.layoutControlGroup1,
            this.emptySpaceItem3,
            this.emptySpaceItem17,
            this.emptySpaceItem2,
            this.layoutControlGroup_PedidoItems,
            this.emptySpaceItem7,
            this.layoutControlGroup_CamposExtra});
            this.layoutControlGroup_RegisterPedido.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup_RegisterPedido.Name = "Root";
            this.layoutControlGroup_RegisterPedido.Size = new System.Drawing.Size(1225, 655);
            this.layoutControlGroup_RegisterPedido.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(10, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(1185, 10);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(1195, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(10, 625);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleLabelItem_RegisterPedido
            // 
            this.simpleLabelItem_RegisterPedido.AllowHotTrack = false;
            this.simpleLabelItem_RegisterPedido.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem_RegisterPedido.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem_RegisterPedido.Location = new System.Drawing.Point(10, 10);
            this.simpleLabelItem_RegisterPedido.Name = "simpleLabelItem_RegisterPedido";
            this.simpleLabelItem_RegisterPedido.OptionsPrint.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem_RegisterPedido.OptionsPrint.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem_RegisterPedido.Size = new System.Drawing.Size(1185, 17);
            this.simpleLabelItem_RegisterPedido.Text = "Cadastro de Pedidos";
            this.simpleLabelItem_RegisterPedido.TextSize = new System.Drawing.Size(116, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(10, 27);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(1185, 10);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup_Pedido
            // 
            this.layoutControlGroup_Pedido.AppearanceGroup.BackColor = System.Drawing.Color.White;
            this.layoutControlGroup_Pedido.AppearanceGroup.Options.UseBackColor = true;
            this.layoutControlGroup_Pedido.AppearanceTabPage.PageClient.BackColor = System.Drawing.Color.White;
            this.layoutControlGroup_Pedido.AppearanceTabPage.PageClient.Options.UseBackColor = true;
            this.layoutControlGroup_Pedido.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem_PedidoTransportadora,
            this.layoutControlItem_PedidoData,
            this.emptySpaceItem18,
            this.layoutControlItem_PedidoCondPag,
            this.layoutControlItem_PedidoClienteId,
            this.emptySpaceItem28,
            this.layoutControlItem_PedidoFormaPag});
            this.layoutControlGroup_Pedido.Location = new System.Drawing.Point(10, 37);
            this.layoutControlGroup_Pedido.Name = "layoutControlGroup_Pedido";
            this.layoutControlGroup_Pedido.Size = new System.Drawing.Size(1185, 124);
            this.layoutControlGroup_Pedido.Text = "Pedido";
            // 
            // layoutControlItem_PedidoTransportadora
            // 
            this.layoutControlItem_PedidoTransportadora.Control = this.textEdit_PedidoTransportadora;
            this.layoutControlItem_PedidoTransportadora.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem_PedidoTransportadora.Name = "layoutControlItem_PedidoTransportadora";
            this.layoutControlItem_PedidoTransportadora.Size = new System.Drawing.Size(576, 48);
            this.layoutControlItem_PedidoTransportadora.Text = "Transportadora";
            this.layoutControlItem_PedidoTransportadora.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlItem_PedidoData
            // 
            this.layoutControlItem_PedidoData.Control = this.textEdit_PedidoData;
            this.layoutControlItem_PedidoData.Location = new System.Drawing.Point(592, 0);
            this.layoutControlItem_PedidoData.Name = "layoutControlItem_PedidoData";
            this.layoutControlItem_PedidoData.Size = new System.Drawing.Size(569, 24);
            this.layoutControlItem_PedidoData.Text = "Data Emissão";
            this.layoutControlItem_PedidoData.TextSize = new System.Drawing.Size(116, 13);
            // 
            // emptySpaceItem18
            // 
            this.emptySpaceItem18.AllowHotTrack = false;
            this.emptySpaceItem18.Location = new System.Drawing.Point(0, 72);
            this.emptySpaceItem18.Name = "emptySpaceItem18";
            this.emptySpaceItem18.Size = new System.Drawing.Size(1161, 10);
            this.emptySpaceItem18.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem_PedidoCondPag
            // 
            this.layoutControlItem_PedidoCondPag.Control = this.textEdit_PedidoCondPag;
            this.layoutControlItem_PedidoCondPag.Location = new System.Drawing.Point(592, 24);
            this.layoutControlItem_PedidoCondPag.Name = "layoutControlItem_PedidoCondPag";
            this.layoutControlItem_PedidoCondPag.Size = new System.Drawing.Size(569, 24);
            this.layoutControlItem_PedidoCondPag.Text = "Condição de Pagamento";
            this.layoutControlItem_PedidoCondPag.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlItem_PedidoClienteId
            // 
            this.layoutControlItem_PedidoClienteId.Control = this.textEdit_PedidoClienteId;
            this.layoutControlItem_PedidoClienteId.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem_PedidoClienteId.Name = "layoutControlItem_PedidoClienteId";
            this.layoutControlItem_PedidoClienteId.Size = new System.Drawing.Size(576, 24);
            this.layoutControlItem_PedidoClienteId.Text = "Cliente";
            this.layoutControlItem_PedidoClienteId.TextSize = new System.Drawing.Size(116, 13);
            // 
            // emptySpaceItem28
            // 
            this.emptySpaceItem28.AllowHotTrack = false;
            this.emptySpaceItem28.Location = new System.Drawing.Point(576, 0);
            this.emptySpaceItem28.Name = "emptySpaceItem28";
            this.emptySpaceItem28.Size = new System.Drawing.Size(16, 72);
            this.emptySpaceItem28.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem_PedidoFormaPag
            // 
            this.layoutControlItem_PedidoFormaPag.Control = this.textEdit_PedidoFormaPag;
            this.layoutControlItem_PedidoFormaPag.CustomizationFormText = "Condição de Pagamento";
            this.layoutControlItem_PedidoFormaPag.Location = new System.Drawing.Point(592, 48);
            this.layoutControlItem_PedidoFormaPag.Name = "layoutControlItem_PedidoFormaPag";
            this.layoutControlItem_PedidoFormaPag.Size = new System.Drawing.Size(569, 24);
            this.layoutControlItem_PedidoFormaPag.Text = "Forma de Pagamento";
            this.layoutControlItem_PedidoFormaPag.TextSize = new System.Drawing.Size(116, 13);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.Location = new System.Drawing.Point(0, 625);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(1205, 10);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem_CLIUltimaAlteracao,
            this.layoutControlItem_PedidoStatus,
            this.emptySpaceItem10,
            this.layoutControlItem_CLIObservacao});
            this.layoutControlGroup1.Location = new System.Drawing.Point(10, 535);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1185, 90);
            this.layoutControlGroup1.Text = "Status";
            // 
            // layoutControlItem_CLIUltimaAlteracao
            // 
            this.layoutControlItem_CLIUltimaAlteracao.Control = this.dateEdit_CLIUltimaAlteracao;
            this.layoutControlItem_CLIUltimaAlteracao.Location = new System.Drawing.Point(401, 0);
            this.layoutControlItem_CLIUltimaAlteracao.Name = "layoutControlItem_CLIUltimaAlteracao";
            this.layoutControlItem_CLIUltimaAlteracao.Size = new System.Drawing.Size(760, 24);
            this.layoutControlItem_CLIUltimaAlteracao.Text = "Última Alteração";
            this.layoutControlItem_CLIUltimaAlteracao.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlItem_PedidoStatus
            // 
            this.layoutControlItem_PedidoStatus.Control = this.textEdit_PedidoStatus;
            this.layoutControlItem_PedidoStatus.CustomizationFormText = "Status";
            this.layoutControlItem_PedidoStatus.Location = new System.Drawing.Point(401, 24);
            this.layoutControlItem_PedidoStatus.Name = "layoutControlItem_PedidoStatus";
            this.layoutControlItem_PedidoStatus.Size = new System.Drawing.Size(760, 24);
            this.layoutControlItem_PedidoStatus.Text = "Status";
            this.layoutControlItem_PedidoStatus.TextSize = new System.Drawing.Size(116, 13);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.Location = new System.Drawing.Point(391, 0);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(10, 48);
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem_CLIObservacao
            // 
            this.layoutControlItem_CLIObservacao.Control = this.memoEdit_CLIObservacao;
            this.layoutControlItem_CLIObservacao.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem_CLIObservacao.Name = "layoutControlItem_CLIObservacao";
            this.layoutControlItem_CLIObservacao.Size = new System.Drawing.Size(391, 48);
            this.layoutControlItem_CLIObservacao.Text = "Observação";
            this.layoutControlItem_CLIObservacao.TextSize = new System.Drawing.Size(116, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(10, 625);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem17
            // 
            this.emptySpaceItem17.AllowHotTrack = false;
            this.emptySpaceItem17.Location = new System.Drawing.Point(10, 161);
            this.emptySpaceItem17.Name = "emptySpaceItem17";
            this.emptySpaceItem17.Size = new System.Drawing.Size(1185, 10);
            this.emptySpaceItem17.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(10, 343);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(1185, 10);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup_PedidoItems
            // 
            this.layoutControlGroup_PedidoItems.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem14,
            this.layoutControlItem_GradeCorTamAdd,
            this.emptySpaceItem24,
            this.emptySpaceItem22,
            this.emptySpaceItem20,
            this.emptySpaceItem23});
            this.layoutControlGroup_PedidoItems.Location = new System.Drawing.Point(10, 171);
            this.layoutControlGroup_PedidoItems.Name = "layoutControlGroup_PedidoItems";
            this.layoutControlGroup_PedidoItems.Size = new System.Drawing.Size(1185, 172);
            this.layoutControlGroup_PedidoItems.Text = "Itens do Pedido";
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.gridControl_PedidoItens;
            this.layoutControlItem14.Location = new System.Drawing.Point(10, 36);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(1141, 84);
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextVisible = false;
            // 
            // layoutControlItem_GradeCorTamAdd
            // 
            this.layoutControlItem_GradeCorTamAdd.Control = this.simpleButton_GradeCorTamAdd;
            this.layoutControlItem_GradeCorTamAdd.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem_GradeCorTamAdd.Name = "layoutControlItem_GradeCorTamAdd";
            this.layoutControlItem_GradeCorTamAdd.Size = new System.Drawing.Size(1161, 26);
            this.layoutControlItem_GradeCorTamAdd.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem_GradeCorTamAdd.TextVisible = false;
            // 
            // emptySpaceItem24
            // 
            this.emptySpaceItem24.AllowHotTrack = false;
            this.emptySpaceItem24.Location = new System.Drawing.Point(1151, 36);
            this.emptySpaceItem24.Name = "emptySpaceItem24";
            this.emptySpaceItem24.Size = new System.Drawing.Size(10, 84);
            this.emptySpaceItem24.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem22
            // 
            this.emptySpaceItem22.AllowHotTrack = false;
            this.emptySpaceItem22.Location = new System.Drawing.Point(0, 120);
            this.emptySpaceItem22.Name = "emptySpaceItem22";
            this.emptySpaceItem22.Size = new System.Drawing.Size(1161, 10);
            this.emptySpaceItem22.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem20
            // 
            this.emptySpaceItem20.AllowHotTrack = false;
            this.emptySpaceItem20.Location = new System.Drawing.Point(0, 26);
            this.emptySpaceItem20.Name = "emptySpaceItem20";
            this.emptySpaceItem20.Size = new System.Drawing.Size(1161, 10);
            this.emptySpaceItem20.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem23
            // 
            this.emptySpaceItem23.AllowHotTrack = false;
            this.emptySpaceItem23.Location = new System.Drawing.Point(0, 36);
            this.emptySpaceItem23.Name = "emptySpaceItem23";
            this.emptySpaceItem23.Size = new System.Drawing.Size(10, 84);
            this.emptySpaceItem23.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(10, 525);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(1185, 10);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup_CamposExtra
            // 
            this.layoutControlGroup_CamposExtra.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.emptySpaceItem25,
            this.layoutControlItem2,
            this.emptySpaceItem19,
            this.emptySpaceItem26,
            this.emptySpaceItem27,
            this.layoutControlItem_CampoExtraTipo});
            this.layoutControlGroup_CamposExtra.Location = new System.Drawing.Point(10, 353);
            this.layoutControlGroup_CamposExtra.Name = "layoutControlGroup_CamposExtra";
            this.layoutControlGroup_CamposExtra.Size = new System.Drawing.Size(1185, 172);
            this.layoutControlGroup_CamposExtra.Text = "Campos Extra";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.simpleButton_CampoExtraAdd;
            this.layoutControlItem1.Location = new System.Drawing.Point(264, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(897, 26);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem25
            // 
            this.emptySpaceItem25.AllowHotTrack = false;
            this.emptySpaceItem25.Location = new System.Drawing.Point(0, 120);
            this.emptySpaceItem25.Name = "emptySpaceItem25";
            this.emptySpaceItem25.Size = new System.Drawing.Size(1161, 10);
            this.emptySpaceItem25.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControl_CampoExtra;
            this.layoutControlItem2.Location = new System.Drawing.Point(10, 36);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(1141, 84);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem19
            // 
            this.emptySpaceItem19.AllowHotTrack = false;
            this.emptySpaceItem19.Location = new System.Drawing.Point(0, 26);
            this.emptySpaceItem19.Name = "emptySpaceItem19";
            this.emptySpaceItem19.Size = new System.Drawing.Size(1161, 10);
            this.emptySpaceItem19.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem26
            // 
            this.emptySpaceItem26.AllowHotTrack = false;
            this.emptySpaceItem26.Location = new System.Drawing.Point(1151, 36);
            this.emptySpaceItem26.Name = "emptySpaceItem26";
            this.emptySpaceItem26.Size = new System.Drawing.Size(10, 84);
            this.emptySpaceItem26.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem27
            // 
            this.emptySpaceItem27.AllowHotTrack = false;
            this.emptySpaceItem27.Location = new System.Drawing.Point(0, 36);
            this.emptySpaceItem27.Name = "emptySpaceItem27";
            this.emptySpaceItem27.Size = new System.Drawing.Size(10, 84);
            this.emptySpaceItem27.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem_CampoExtraTipo
            // 
            this.layoutControlItem_CampoExtraTipo.Control = this.comboBoxEdit_CampoExtraTipo;
            this.layoutControlItem_CampoExtraTipo.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem_CampoExtraTipo.Name = "layoutControlItem_CampoExtraTipo";
            this.layoutControlItem_CampoExtraTipo.Size = new System.Drawing.Size(264, 26);
            this.layoutControlItem_CampoExtraTipo.Text = "Tipo Campo Extra";
            this.layoutControlItem_CampoExtraTipo.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem_CampoExtraTipo.TextSize = new System.Drawing.Size(85, 13);
            this.layoutControlItem_CampoExtraTipo.TextToControlDistance = 5;
            // 
            // tabNavigationPage_DataGridPedido
            // 
            this.tabNavigationPage_DataGridPedido.Caption = "Pedido";
            this.tabNavigationPage_DataGridPedido.Controls.Add(this.gridControl_Pedido);
            this.tabNavigationPage_DataGridPedido.Image = ((System.Drawing.Image)(resources.GetObject("tabNavigationPage_DataGridPedido.Image")));
            this.tabNavigationPage_DataGridPedido.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage_DataGridPedido.Name = "tabNavigationPage_DataGridPedido";
            this.tabNavigationPage_DataGridPedido.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage_DataGridPedido.Size = new System.Drawing.Size(1246, 404);
            // 
            // gridControl_Pedido
            // 
            this.gridControl_Pedido.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl_Pedido.Location = new System.Drawing.Point(0, 0);
            this.gridControl_Pedido.MainView = this.gridView_Pedidos;
            this.gridControl_Pedido.MenuManager = this.barManager_ContasMP;
            this.gridControl_Pedido.Name = "gridControl_Pedido";
            this.gridControl_Pedido.Size = new System.Drawing.Size(1246, 404);
            this.gridControl_Pedido.TabIndex = 0;
            this.gridControl_Pedido.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView_Pedidos});
            // 
            // gridView_Pedidos
            // 
            this.gridView_Pedidos.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colid,
            this.colcliente_id,
            this.colrepresentada_nome_fantasia,
            this.coltransportadora_id,
            this.colcriador_id,
            this.colstatus,
            this.colnumero,
            this.coltotal,
            this.colcondicao_pagamento_id,
            this.colforma_pagamento_id,
            this.coldata_emissao,
            this.colobservacoes,
            this.colstatus_faturamento,
            this.colultima_alteracao,
            this.colmptran});
            this.gridView_Pedidos.GridControl = this.gridControl_Pedido;
            this.gridView_Pedidos.Name = "gridView_Pedidos";
            this.gridView_Pedidos.OptionsBehavior.Editable = false;
            this.gridView_Pedidos.OptionsBehavior.ReadOnly = true;
            this.gridView_Pedidos.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView_Pedidos.OptionsSelection.MultiSelect = true;
            this.gridView_Pedidos.OptionsSelection.UseIndicatorForSelection = false;
            this.gridView_Pedidos.OptionsView.ShowGroupPanel = false;
            this.gridView_Pedidos.OptionsView.ShowIndicator = false;
            this.gridView_Pedidos.DoubleClick += new System.EventHandler(this.gridView_Clientes_DoubleClick);
            // 
            // colid
            // 
            this.colid.Caption = "ID";
            this.colid.FieldName = "id";
            this.colid.Name = "colid";
            this.colid.Visible = true;
            this.colid.VisibleIndex = 0;
            // 
            // colcliente_id
            // 
            this.colcliente_id.Caption = "ID Cliente";
            this.colcliente_id.FieldName = "cliente_id";
            this.colcliente_id.Name = "colcliente_id";
            this.colcliente_id.Visible = true;
            this.colcliente_id.VisibleIndex = 1;
            // 
            // colrepresentada_nome_fantasia
            // 
            this.colrepresentada_nome_fantasia.Caption = "Representada Nome Fantasia";
            this.colrepresentada_nome_fantasia.FieldName = "representada_nome_fantasia";
            this.colrepresentada_nome_fantasia.Name = "colrepresentada_nome_fantasia";
            this.colrepresentada_nome_fantasia.Visible = true;
            this.colrepresentada_nome_fantasia.VisibleIndex = 2;
            // 
            // coltransportadora_id
            // 
            this.coltransportadora_id.Caption = "ID Transportadora";
            this.coltransportadora_id.FieldName = "transportadora_id";
            this.coltransportadora_id.Name = "coltransportadora_id";
            this.coltransportadora_id.Visible = true;
            this.coltransportadora_id.VisibleIndex = 3;
            // 
            // colcriador_id
            // 
            this.colcriador_id.Caption = "ID Criador";
            this.colcriador_id.FieldName = "criador_id";
            this.colcriador_id.Name = "colcriador_id";
            this.colcriador_id.Visible = true;
            this.colcriador_id.VisibleIndex = 4;
            // 
            // colstatus
            // 
            this.colstatus.Caption = "Status";
            this.colstatus.FieldName = "status";
            this.colstatus.Name = "colstatus";
            this.colstatus.Visible = true;
            this.colstatus.VisibleIndex = 5;
            // 
            // colnumero
            // 
            this.colnumero.Caption = "Número";
            this.colnumero.FieldName = "numero";
            this.colnumero.Name = "colnumero";
            this.colnumero.Visible = true;
            this.colnumero.VisibleIndex = 6;
            // 
            // coltotal
            // 
            this.coltotal.Caption = "Total";
            this.coltotal.FieldName = "total";
            this.coltotal.Name = "coltotal";
            this.coltotal.Visible = true;
            this.coltotal.VisibleIndex = 7;
            // 
            // colcondicao_pagamento_id
            // 
            this.colcondicao_pagamento_id.Caption = "ID Condição Pagamento";
            this.colcondicao_pagamento_id.FieldName = "condicao_pagamento_id";
            this.colcondicao_pagamento_id.Name = "colcondicao_pagamento_id";
            this.colcondicao_pagamento_id.Visible = true;
            this.colcondicao_pagamento_id.VisibleIndex = 8;
            // 
            // colforma_pagamento_id
            // 
            this.colforma_pagamento_id.Caption = "ID Forma Pagamento";
            this.colforma_pagamento_id.FieldName = "forma_pagamento_id";
            this.colforma_pagamento_id.Name = "colforma_pagamento_id";
            this.colforma_pagamento_id.Visible = true;
            this.colforma_pagamento_id.VisibleIndex = 9;
            // 
            // coldata_emissao
            // 
            this.coldata_emissao.Caption = "Data Emissão";
            this.coldata_emissao.FieldName = "data_emissao";
            this.coldata_emissao.Name = "coldata_emissao";
            this.coldata_emissao.Visible = true;
            this.coldata_emissao.VisibleIndex = 10;
            // 
            // colobservacoes
            // 
            this.colobservacoes.Caption = "Observações";
            this.colobservacoes.FieldName = "observacoes";
            this.colobservacoes.Name = "colobservacoes";
            this.colobservacoes.Visible = true;
            this.colobservacoes.VisibleIndex = 11;
            // 
            // colstatus_faturamento
            // 
            this.colstatus_faturamento.Caption = "Status Faturamento";
            this.colstatus_faturamento.FieldName = "status_faturamento";
            this.colstatus_faturamento.Name = "colstatus_faturamento";
            this.colstatus_faturamento.Visible = true;
            this.colstatus_faturamento.VisibleIndex = 12;
            // 
            // colultima_alteracao
            // 
            this.colultima_alteracao.Caption = "Última Alteração";
            this.colultima_alteracao.FieldName = "ultima_alteracao";
            this.colultima_alteracao.Name = "colultima_alteracao";
            this.colultima_alteracao.Visible = true;
            this.colultima_alteracao.VisibleIndex = 13;
            // 
            // colmptran
            // 
            this.colmptran.Caption = "MP Tran";
            this.colmptran.FieldName = "mptran";
            this.colmptran.Name = "colmptran";
            this.colmptran.Visible = true;
            this.colmptran.VisibleIndex = 14;
            // 
            // tabPane_ConfigPedidos
            // 
            this.tabPane_ConfigPedidos.Controls.Add(this.tabNavigationPage_DataGridPedido);
            this.tabPane_ConfigPedidos.Controls.Add(this.tabNavigationPage_RegisterPedido);
            this.tabPane_ConfigPedidos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPane_ConfigPedidos.Location = new System.Drawing.Point(0, 94);
            this.tabPane_ConfigPedidos.Name = "tabPane_ConfigPedidos";
            this.tabPane_ConfigPedidos.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.tabNavigationPage_DataGridPedido,
            this.tabNavigationPage_RegisterPedido});
            this.tabPane_ConfigPedidos.RegularSize = new System.Drawing.Size(1264, 468);
            this.tabPane_ConfigPedidos.SelectedPage = this.tabNavigationPage_RegisterPedido;
            this.tabPane_ConfigPedidos.Size = new System.Drawing.Size(1264, 468);
            this.tabPane_ConfigPedidos.TabIndex = 8;
            this.tabPane_ConfigPedidos.Text = "Clientes";
            // 
            // tabPane1
            // 
            this.tabPane1.Controls.Add(this.tabNavigationPage1);
            this.tabPane1.Controls.Add(this.tabNavigationPage2);
            this.tabPane1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPane1.Location = new System.Drawing.Point(0, 94);
            this.tabPane1.Name = "tabPane1";
            this.tabPane1.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.tabNavigationPage1,
            this.tabNavigationPage2});
            this.tabPane1.RegularSize = new System.Drawing.Size(1264, 445);
            this.tabPane1.SelectedPage = this.tabNavigationPage2;
            this.tabPane1.Size = new System.Drawing.Size(1264, 445);
            this.tabPane1.TabIndex = 8;
            this.tabPane1.Text = "Clientes";
            // 
            // tabNavigationPage1
            // 
            this.tabNavigationPage1.Caption = "Clientes";
            this.tabNavigationPage1.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage1.Name = "tabNavigationPage1";
            this.tabNavigationPage1.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage1.Size = new System.Drawing.Size(1246, 381);
            // 
            // tabNavigationPage2
            // 
            this.tabNavigationPage2.Caption = "Cadastro";
            this.tabNavigationPage2.Controls.Add(this.panelControl1);
            this.tabNavigationPage2.Image = ((System.Drawing.Image)(resources.GetObject("tabNavigationPage2.Image")));
            this.tabNavigationPage2.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage2.Name = "tabNavigationPage2";
            this.tabNavigationPage2.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage2.Size = new System.Drawing.Size(1246, 381);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.layoutControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1246, 381);
            this.panelControl1.TabIndex = 0;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.textEdit2);
            this.layoutControl1.Controls.Add(this.textEdit3);
            this.layoutControl1.Controls.Add(this.textEdit4);
            this.layoutControl1.Controls.Add(this.textEdit5);
            this.layoutControl1.Controls.Add(this.textEdit6);
            this.layoutControl1.Controls.Add(this.textEdit7);
            this.layoutControl1.Controls.Add(this.textEdit8);
            this.layoutControl1.Controls.Add(this.textEdit9);
            this.layoutControl1.Controls.Add(this.textEdit10);
            this.layoutControl1.Controls.Add(this.textEdit11);
            this.layoutControl1.Controls.Add(this.textEdit12);
            this.layoutControl1.Controls.Add(this.textEdit13);
            this.layoutControl1.Controls.Add(this.textEdit14);
            this.layoutControl1.Controls.Add(this.dateEdit1);
            this.layoutControl1.Controls.Add(this.toggleSwitch1);
            this.layoutControl1.Controls.Add(this.memoEdit1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup3;
            this.layoutControl1.Size = new System.Drawing.Size(1242, 377);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textEdit2
            // 
            this.textEdit2.Location = new System.Drawing.Point(136, 83);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Size = new System.Drawing.Size(1055, 20);
            this.textEdit2.StyleController = this.layoutControl1;
            this.textEdit2.TabIndex = 4;
            this.textEdit2.Visible = false;
            // 
            // textEdit3
            // 
            this.textEdit3.Location = new System.Drawing.Point(136, 107);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Size = new System.Drawing.Size(1055, 20);
            this.textEdit3.StyleController = this.layoutControl1;
            this.textEdit3.TabIndex = 5;
            this.textEdit3.Visible = false;
            // 
            // textEdit4
            // 
            this.textEdit4.Location = new System.Drawing.Point(136, 131);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Properties.MaxLength = 1;
            this.textEdit4.Size = new System.Drawing.Size(1055, 20);
            this.textEdit4.StyleController = this.layoutControl1;
            this.textEdit4.TabIndex = 6;
            this.textEdit4.Visible = false;
            // 
            // textEdit5
            // 
            this.textEdit5.Location = new System.Drawing.Point(136, 155);
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Size = new System.Drawing.Size(1055, 20);
            this.textEdit5.StyleController = this.layoutControl1;
            this.textEdit5.TabIndex = 7;
            this.textEdit5.Visible = false;
            // 
            // textEdit6
            // 
            this.textEdit6.Location = new System.Drawing.Point(136, 179);
            this.textEdit6.Name = "textEdit6";
            this.textEdit6.Size = new System.Drawing.Size(1055, 20);
            this.textEdit6.StyleController = this.layoutControl1;
            this.textEdit6.TabIndex = 8;
            this.textEdit6.Visible = false;
            // 
            // textEdit7
            // 
            this.textEdit7.Location = new System.Drawing.Point(136, 203);
            this.textEdit7.Name = "textEdit7";
            this.textEdit7.Size = new System.Drawing.Size(1055, 20);
            this.textEdit7.StyleController = this.layoutControl1;
            this.textEdit7.TabIndex = 9;
            this.textEdit7.Visible = false;
            // 
            // textEdit8
            // 
            this.textEdit8.Location = new System.Drawing.Point(136, 303);
            this.textEdit8.Name = "textEdit8";
            this.textEdit8.Size = new System.Drawing.Size(1055, 20);
            this.textEdit8.StyleController = this.layoutControl1;
            this.textEdit8.TabIndex = 10;
            // 
            // textEdit9
            // 
            this.textEdit9.Location = new System.Drawing.Point(136, 327);
            this.textEdit9.Name = "textEdit9";
            this.textEdit9.Size = new System.Drawing.Size(1055, 20);
            this.textEdit9.StyleController = this.layoutControl1;
            this.textEdit9.TabIndex = 11;
            // 
            // textEdit10
            // 
            this.textEdit10.Location = new System.Drawing.Point(136, 351);
            this.textEdit10.Name = "textEdit10";
            this.textEdit10.Size = new System.Drawing.Size(1055, 20);
            this.textEdit10.StyleController = this.layoutControl1;
            this.textEdit10.TabIndex = 12;
            // 
            // textEdit11
            // 
            this.textEdit11.Location = new System.Drawing.Point(136, 375);
            this.textEdit11.Name = "textEdit11";
            this.textEdit11.Size = new System.Drawing.Size(1055, 20);
            this.textEdit11.StyleController = this.layoutControl1;
            this.textEdit11.TabIndex = 13;
            // 
            // textEdit12
            // 
            this.textEdit12.Location = new System.Drawing.Point(136, 399);
            this.textEdit12.Name = "textEdit12";
            this.textEdit12.Size = new System.Drawing.Size(1055, 20);
            this.textEdit12.StyleController = this.layoutControl1;
            this.textEdit12.TabIndex = 14;
            // 
            // textEdit13
            // 
            this.textEdit13.Location = new System.Drawing.Point(136, 423);
            this.textEdit13.Name = "textEdit13";
            this.textEdit13.Size = new System.Drawing.Size(1055, 20);
            this.textEdit13.StyleController = this.layoutControl1;
            this.textEdit13.TabIndex = 15;
            // 
            // textEdit14
            // 
            this.textEdit14.Location = new System.Drawing.Point(136, 227);
            this.textEdit14.Name = "textEdit14";
            this.textEdit14.Size = new System.Drawing.Size(1055, 20);
            this.textEdit14.StyleController = this.layoutControl1;
            this.textEdit14.TabIndex = 16;
            this.textEdit14.Visible = false;
            // 
            // dateEdit1
            // 
            this.dateEdit1.EditValue = null;
            this.dateEdit1.Location = new System.Drawing.Point(968, 499);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.ReadOnly = true;
            this.dateEdit1.Size = new System.Drawing.Size(223, 20);
            this.dateEdit1.StyleController = this.layoutControl1;
            this.dateEdit1.TabIndex = 17;
            // 
            // toggleSwitch1
            // 
            this.toggleSwitch1.Location = new System.Drawing.Point(968, 523);
            this.toggleSwitch1.Name = "toggleSwitch1";
            this.toggleSwitch1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.toggleSwitch1.Properties.OffText = "Não";
            this.toggleSwitch1.Properties.OnText = "Sim";
            this.toggleSwitch1.Size = new System.Drawing.Size(223, 24);
            this.toggleSwitch1.StyleController = this.layoutControl1;
            this.toggleSwitch1.TabIndex = 10;
            // 
            // memoEdit1
            // 
            this.memoEdit1.Location = new System.Drawing.Point(136, 499);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(716, 48);
            this.memoEdit1.StyleController = this.layoutControl1;
            this.memoEdit1.TabIndex = 18;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem6,
            this.emptySpaceItem8,
            this.emptySpaceItem11,
            this.emptySpaceItem12,
            this.simpleLabelItem1,
            this.emptySpaceItem13,
            this.emptySpaceItem14,
            this.layoutControlGroup4,
            this.emptySpaceItem15,
            this.layoutControlGroup5,
            this.tabbedControlGroup3});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "Root";
            this.layoutControlGroup3.Size = new System.Drawing.Size(1225, 581);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(10, 0);
            this.emptySpaceItem6.Name = "emptySpaceItem1";
            this.emptySpaceItem6.Size = new System.Drawing.Size(1185, 10);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.Location = new System.Drawing.Point(10, 251);
            this.emptySpaceItem8.Name = "emptySpaceItem2";
            this.emptySpaceItem8.Size = new System.Drawing.Size(1185, 10);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem11.Name = "emptySpaceItem3";
            this.emptySpaceItem11.Size = new System.Drawing.Size(10, 561);
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.Location = new System.Drawing.Point(1195, 0);
            this.emptySpaceItem12.Name = "emptySpaceItem4";
            this.emptySpaceItem12.Size = new System.Drawing.Size(10, 561);
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleLabelItem1
            // 
            this.simpleLabelItem1.AllowHotTrack = false;
            this.simpleLabelItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem1.Location = new System.Drawing.Point(10, 10);
            this.simpleLabelItem1.Name = "simpleLabelItem_RegisterClientes";
            this.simpleLabelItem1.OptionsPrint.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem1.OptionsPrint.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem1.Size = new System.Drawing.Size(1185, 17);
            this.simpleLabelItem1.Text = "Cadastro Clientes";
            this.simpleLabelItem1.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.Location = new System.Drawing.Point(10, 27);
            this.emptySpaceItem13.Name = "emptySpaceItem5";
            this.emptySpaceItem13.Size = new System.Drawing.Size(1185, 10);
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.Location = new System.Drawing.Point(10, 447);
            this.emptySpaceItem14.Name = "emptySpaceItem7";
            this.emptySpaceItem14.Size = new System.Drawing.Size(1185, 10);
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9});
            this.layoutControlGroup4.Location = new System.Drawing.Point(10, 261);
            this.layoutControlGroup4.Name = "layoutControlGroup_Endereço";
            this.layoutControlGroup4.Size = new System.Drawing.Size(1185, 186);
            this.layoutControlGroup4.Text = "Endereço";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.textEdit8;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem_CLIRua";
            this.layoutControlItem4.Size = new System.Drawing.Size(1161, 24);
            this.layoutControlItem4.Text = "Rua";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.textEdit9;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem5.Name = "layoutControlItem_CLIComplemento";
            this.layoutControlItem5.Size = new System.Drawing.Size(1161, 24);
            this.layoutControlItem5.Text = "Complemento";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.textEdit10;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem6.Name = "layoutControlItem_CLICep";
            this.layoutControlItem6.Size = new System.Drawing.Size(1161, 24);
            this.layoutControlItem6.Text = "CEP";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.textEdit11;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem7.Name = "layoutControlItem_Bairro";
            this.layoutControlItem7.Size = new System.Drawing.Size(1161, 24);
            this.layoutControlItem7.Text = "Bairro";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.textEdit12;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem8.Name = "layoutControlItem_Cidade";
            this.layoutControlItem8.Size = new System.Drawing.Size(1161, 24);
            this.layoutControlItem8.Text = "Cidade";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.textEdit13;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem9.Name = "layoutControlItem_CLIEstado";
            this.layoutControlItem9.Size = new System.Drawing.Size(1161, 24);
            this.layoutControlItem9.Text = "Estado";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.Location = new System.Drawing.Point(10, 551);
            this.emptySpaceItem15.Name = "emptySpaceItem9";
            this.emptySpaceItem15.Size = new System.Drawing.Size(1185, 10);
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.emptySpaceItem16,
            this.layoutControlItem12});
            this.layoutControlGroup5.Location = new System.Drawing.Point(10, 457);
            this.layoutControlGroup5.Name = "layoutControlGroup1";
            this.layoutControlGroup5.Size = new System.Drawing.Size(1185, 94);
            this.layoutControlGroup5.Text = "Status";
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.dateEdit1;
            this.layoutControlItem10.Location = new System.Drawing.Point(832, 0);
            this.layoutControlItem10.Name = "layoutControlItem_CLIUltimaAlteracao";
            this.layoutControlItem10.Size = new System.Drawing.Size(329, 24);
            this.layoutControlItem10.Text = "Última Alteração";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.toggleSwitch1;
            this.layoutControlItem11.CustomizationFormText = "Status";
            this.layoutControlItem11.Location = new System.Drawing.Point(832, 24);
            this.layoutControlItem11.Name = "layoutControlItem_CLIExcluido";
            this.layoutControlItem11.Size = new System.Drawing.Size(329, 28);
            this.layoutControlItem11.Text = "Excluído";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem16
            // 
            this.emptySpaceItem16.AllowHotTrack = false;
            this.emptySpaceItem16.Location = new System.Drawing.Point(822, 0);
            this.emptySpaceItem16.Name = "emptySpaceItem10";
            this.emptySpaceItem16.Size = new System.Drawing.Size(10, 52);
            this.emptySpaceItem16.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.memoEdit1;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem12.Name = "layoutControlItem_CLIObservacao";
            this.layoutControlItem12.Size = new System.Drawing.Size(822, 52);
            this.layoutControlItem12.Text = "Observação";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(99, 13);
            // 
            // tabbedControlGroup3
            // 
            this.tabbedControlGroup3.Location = new System.Drawing.Point(10, 37);
            this.tabbedControlGroup3.Name = "tabbedControlGroup1";
            this.tabbedControlGroup3.SelectedTabPage = this.layoutControlGroup6;
            this.tabbedControlGroup3.SelectedTabPageIndex = 1;
            this.tabbedControlGroup3.Size = new System.Drawing.Size(1185, 214);
            this.tabbedControlGroup3.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup9,
            this.layoutControlGroup6});
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup4});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup_CLIContato";
            this.layoutControlGroup6.Size = new System.Drawing.Size(1161, 168);
            this.layoutControlGroup6.Text = "Contatos";
            // 
            // tabbedControlGroup4
            // 
            this.tabbedControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup4.Name = "tabbedControlGroup2";
            this.tabbedControlGroup4.SelectedTabPage = this.layoutControlGroup7;
            this.tabbedControlGroup4.SelectedTabPageIndex = 0;
            this.tabbedControlGroup4.Size = new System.Drawing.Size(1161, 168);
            this.tabbedControlGroup4.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup7,
            this.layoutControlGroup8});
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleSeparator2});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup_CLITelefone";
            this.layoutControlGroup7.Size = new System.Drawing.Size(1137, 122);
            this.layoutControlGroup7.Text = "Telefone";
            // 
            // simpleSeparator2
            // 
            this.simpleSeparator2.AllowHotTrack = false;
            this.simpleSeparator2.Location = new System.Drawing.Point(0, 0);
            this.simpleSeparator2.Name = "simpleSeparator1";
            this.simpleSeparator2.Size = new System.Drawing.Size(1137, 122);
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup8.Name = "layoutControlGroup_CLIEmail";
            this.layoutControlGroup8.Size = new System.Drawing.Size(1137, 122);
            this.layoutControlGroup8.Text = "E-mail";
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.layoutControlItem23});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup9.Name = "layoutControlGroup_CLIDados";
            this.layoutControlGroup9.Size = new System.Drawing.Size(1161, 168);
            this.layoutControlGroup9.Text = "Dados";
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.textEdit2;
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem17.Name = "layoutControlItem_CLIRazaoSocial";
            this.layoutControlItem17.Size = new System.Drawing.Size(1161, 24);
            this.layoutControlItem17.Text = "Razão Social";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.textEdit3;
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem18.Name = "layoutControlItem_CLINomeFantasia";
            this.layoutControlItem18.Size = new System.Drawing.Size(1161, 24);
            this.layoutControlItem18.Text = "Nome Fantasia";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.textEdit4;
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem19.Name = "layoutControlItem_CLITipo";
            this.layoutControlItem19.Size = new System.Drawing.Size(1161, 24);
            this.layoutControlItem19.Text = "Tipo";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.textEdit5;
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem20.Name = "layoutControlItem_CLICnpj";
            this.layoutControlItem20.Size = new System.Drawing.Size(1161, 24);
            this.layoutControlItem20.Text = "CNPJ";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.textEdit6;
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem21.Name = "layoutControlItem_CLIInscricaoEstadual";
            this.layoutControlItem21.Size = new System.Drawing.Size(1161, 24);
            this.layoutControlItem21.Text = "Inscrição Estadual";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.textEdit7;
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem22.Name = "layoutControlItem_CLISuframa";
            this.layoutControlItem22.Size = new System.Drawing.Size(1161, 24);
            this.layoutControlItem22.Text = "Suframa";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.textEdit14;
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItem23.Name = "layoutControlItem_CLINomeExcFiscal";
            this.layoutControlItem23.Size = new System.Drawing.Size(1161, 24);
            this.layoutControlItem23.Text = "Nome Exceção Fiscal";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(99, 13);
            // 
            // simpleSeparator3
            // 
            this.simpleSeparator3.AllowHotTrack = false;
            this.simpleSeparator3.CustomizationFormText = "simpleSeparator1";
            this.simpleSeparator3.Location = new System.Drawing.Point(1005, 0);
            this.simpleSeparator3.Name = "simpleSeparator3";
            this.simpleSeparator3.Size = new System.Drawing.Size(2, 26);
            this.simpleSeparator3.Text = "simpleSeparator1";
            // 
            // layoutControlGroup_CLITelefone1
            // 
            this.layoutControlGroup_CLITelefone1.CustomizationFormText = "Telefone";
            this.layoutControlGroup_CLITelefone1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleSeparator3});
            this.layoutControlGroup_CLITelefone1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup_CLITelefone1.Name = "layoutControlGroup_CLITelefone1";
            this.layoutControlGroup_CLITelefone1.Size = new System.Drawing.Size(1205, 31);
            this.layoutControlGroup_CLITelefone1.Text = "Telefone";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup_CLITelefone1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 561);
            this.layoutControlGroup2.Name = "LayoutRootGroupForRestore";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1205, 31);
            this.layoutControlGroup2.Tag = "LayoutRootGroupForRestore";
            // 
            // colid1
            // 
            this.colid1.FieldName = "id";
            this.colid1.Name = "colid1";
            // 
            // colrazao_social
            // 
            this.colrazao_social.FieldName = "razao_social";
            this.colrazao_social.Name = "colrazao_social";
            // 
            // colnome_fantasia
            // 
            this.colnome_fantasia.FieldName = "nome_fantasia";
            this.colnome_fantasia.Name = "colnome_fantasia";
            // 
            // emptySpaceItem21
            // 
            this.emptySpaceItem21.AllowHotTrack = false;
            this.emptySpaceItem21.Location = new System.Drawing.Point(0, 34);
            this.emptySpaceItem21.Name = "emptySpaceItem21";
            this.emptySpaceItem21.Size = new System.Drawing.Size(617, 10);
            this.emptySpaceItem21.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(652, 293);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(302, 22);
            this.simpleButton1.TabIndex = 25;
            this.simpleButton1.Text = "simpleButton1";
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.simpleButton1;
            this.layoutControlItem13.Location = new System.Drawing.Point(617, 0);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(306, 44);
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextVisible = false;
            // 
            // pedidoCampoExtraTableAdapter
            // 
            this.pedidoCampoExtraTableAdapter.ClearBeforeFill = true;
            // 
            // FormPedido
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1264, 562);
            this.Controls.Add(this.tabPane_ConfigPedidos);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormPedido";
            this.ShowMdiChildCaptionInParentTitle = true;
            this.Text = "Pedidos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormCliente_Load);
            this.tabNavigationPage_RegisterPedido.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl_RegisterPedido)).EndInit();
            this.panelControl_RegisterPedido.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_Pedido)).EndInit();
            this.layoutControl_Pedido.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_CampoExtra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_CampoExtra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager_ContasMP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_PedidoItens)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_PedidoItens)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_CLIUltimaAlteracao.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_CLIUltimaAlteracao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_CLIObservacao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_PedidoData.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_PedidoData.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_PedidoTransportadora.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_PedidoCondPag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_PedidoClienteId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit_CampoExtraTipo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pedidoCampoExtraBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meusPedidosDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_PedidoStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_PedidoFormaPag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_RegisterPedido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem_RegisterPedido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_Pedido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_PedidoTransportadora)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_PedidoData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_PedidoCondPag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_PedidoClienteId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_PedidoFormaPag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLIUltimaAlteracao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_PedidoStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CLIObservacao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_PedidoItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_GradeCorTamAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CamposExtra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CampoExtraTipo)).EndInit();
            this.tabNavigationPage_DataGridPedido.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_Pedido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_Pedidos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane_ConfigPedidos)).EndInit();
            this.tabPane_ConfigPedidos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).EndInit();
            this.tabPane1.ResumeLayout(false);
            this.tabNavigationPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toggleSwitch1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CLITelefone1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Cliente;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CondicaoPagamentoCliente;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CategoriasCliente;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TabelaPreco;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Usuarios;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Produto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_ImagensProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TabelaProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TabelaPrecoProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CategoriaProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Transportadora;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CondicaoPagamento;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_FormaPagamento;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Pedido;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CamposExtraPedido;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_FaturamentoPedido;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TituloVencido;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager_Clientes;
        private DevExpress.XtraBars.Navigation.TabPane tabPane_ConfigPedidos;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage_DataGridPedido;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage_RegisterPedido;
        private DevExpress.XtraBars.BarManager barManager_ContasMP;
        private DevExpress.XtraBars.Bar bar_Acoes;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Add;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Edit;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Del;
        private DevExpress.XtraBars.Bar bar_Acoes2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Gravar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Cancelar;
        private DevExpress.XtraEditors.PanelControl panelControl_RegisterPedido;
        private DevExpress.XtraLayout.LayoutControl layoutControl_Pedido;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_RegisterPedido;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem_RegisterPedido;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_Pedido;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraEditors.DateEdit dateEdit_CLIUltimaAlteracao;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_PedidoData;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_PedidoTransportadora;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_PedidoCondPag;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_CLIUltimaAlteracao;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_PedidoStatus;
        private DevExpress.XtraEditors.MemoEdit memoEdit_CLIObservacao;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_CLIObservacao;
        private DevExpress.XtraBars.Navigation.TabPane tabPane1;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage1;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage2;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraEditors.TextEdit textEdit3;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        private DevExpress.XtraEditors.TextEdit textEdit6;
        private DevExpress.XtraEditors.TextEdit textEdit7;
        private DevExpress.XtraEditors.TextEdit textEdit8;
        private DevExpress.XtraEditors.TextEdit textEdit9;
        private DevExpress.XtraEditors.TextEdit textEdit10;
        private DevExpress.XtraEditors.TextEdit textEdit11;
        private DevExpress.XtraEditors.TextEdit textEdit12;
        private DevExpress.XtraEditors.TextEdit textEdit13;
        private DevExpress.XtraEditors.TextEdit textEdit14;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
        private DevExpress.XtraEditors.ToggleSwitch toggleSwitch1;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_CLITelefone1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraGrid.Columns.GridColumn colid1;
        private DevExpress.XtraGrid.Columns.GridColumn colrazao_social;
        private DevExpress.XtraGrid.Columns.GridColumn colnome_fantasia;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraGrid.GridControl gridControl_Pedido;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView_Pedidos;
        private MeusPedidosDataSet meusPedidosDataSet;
        private DevExpress.XtraEditors.DateEdit textEdit_PedidoData;
        private DevExpress.XtraEditors.LookUpEdit textEdit_PedidoTransportadora;
        private DevExpress.XtraEditors.LookUpEdit textEdit_PedidoCondPag;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem18;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem21;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraEditors.SimpleButton simpleButton_GradeCorTamAdd;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_GradeCorTamAdd;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_PedidoClienteId;
        private DevExpress.XtraEditors.LookUpEdit textEdit_PedidoClienteId;
        private DevExpress.XtraGrid.GridControl gridControl_PedidoItens;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView_PedidoItens;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_PedidoItems;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem24;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem22;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem20;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem23;
        private DevExpress.XtraEditors.SimpleButton simpleButton_CampoExtraAdd;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_CamposExtra;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem25;
        private DevExpress.XtraGrid.GridControl gridControl_CampoExtra;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView_CampoExtra;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem26;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_CampoExtraTipo;
        private DevExpress.XtraEditors.LookUpEdit comboBoxEdit_CampoExtraTipo;
        private System.Windows.Forms.BindingSource pedidoCampoExtraBindingSource;
        private MeusPedidosDataSetTableAdapters.PedidoCampoExtraTableAdapter pedidoCampoExtraTableAdapter;
        private DevExpress.XtraEditors.ComboBoxEdit textEdit_PedidoStatus;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem28;
        private DevExpress.XtraEditors.LookUpEdit textEdit_PedidoFormaPag;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_PedidoFormaPag;
        private DevExpress.XtraGrid.Columns.GridColumn colid;
        private DevExpress.XtraGrid.Columns.GridColumn colcliente_id;
        private DevExpress.XtraGrid.Columns.GridColumn colrepresentada_nome_fantasia;
        private DevExpress.XtraGrid.Columns.GridColumn coltransportadora_id;
        private DevExpress.XtraGrid.Columns.GridColumn colcriador_id;
        private DevExpress.XtraGrid.Columns.GridColumn colstatus;
        private DevExpress.XtraGrid.Columns.GridColumn colnumero;
        private DevExpress.XtraGrid.Columns.GridColumn coltotal;
        private DevExpress.XtraGrid.Columns.GridColumn colcondicao_pagamento_id;
        private DevExpress.XtraGrid.Columns.GridColumn colforma_pagamento_id;
        private DevExpress.XtraGrid.Columns.GridColumn coldata_emissao;
        private DevExpress.XtraGrid.Columns.GridColumn colobservacoes;
        private DevExpress.XtraGrid.Columns.GridColumn colstatus_faturamento;
        private DevExpress.XtraGrid.Columns.GridColumn colultima_alteracao;
        private DevExpress.XtraGrid.Columns.GridColumn colmptran;
    }
}


﻿namespace View_MeusPedidos
{
    partial class FormUsuarioCad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement10 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement11 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement12 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement13 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement14 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement15 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement16 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement17 = new DevExpress.XtraEditors.TileItemElement();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormUsuarioCad));
            this.tileNavSubItem_Cliente = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CondicaoPagamentoCliente = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CategoriasCliente = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TabelaPreco = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Usuarios = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Produto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_ImagensProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TabelaProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TabelaPrecoProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CategoriaProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Pedido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CamposExtraPedido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Transportadora = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CondicaoPagamento = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_FormaPagamento = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_FaturamentoPedido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TituloVencido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.splashScreenManager_User = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::View_MeusPedidos.WaitForm1), true, true);
            this.simpleLabelItem_RegisterClientes = new DevExpress.XtraLayout.SimpleLabelItem();
            this.usuarioBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.meusPedidosDataSet1 = new View_MeusPedidos.MeusPedidosDataSet();
            this.vendedorBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabPane1 = new DevExpress.XtraBars.Navigation.TabPane();
            this.tabNavigationPage1 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.tabNavigationPage2 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit8 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit9 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit10 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit11 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit12 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit13 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit14 = new DevExpress.XtraEditors.TextEdit();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.toggleSwitch1 = new DevExpress.XtraEditors.ToggleSwitch();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleLabelItem1 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem16 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup3 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup4 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.simpleSeparator2 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator3 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlGroup_CLITelefone1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.colid1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colrazao_social = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colnome_fantasia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup_CatProd = new DevExpress.XtraLayout.LayoutControlGroup();
            this.usuarioTableAdapter = new View_MeusPedidos.MeusPedidosDataSetTableAdapters.UsuarioTableAdapter();
            this.tabNavigationPage_RegisterUsuario = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.panelControl_RegisterUsuario = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl_Usuario = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit_UsuarioCNPJ = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_UsuarioCPF = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_UsuarioSMS = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_UsuarioGenero = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_UsuarioSenha = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_UsuarioTipo = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_UsuarioEmail = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_UsuarioUltNome = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_UsuarioPNome = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_UsuarioComentarios = new DevExpress.XtraEditors.MemoEdit();
            this.dateEdit_UsuarioDtCriacao = new DevExpress.XtraEditors.DateEdit();
            this.textEdit_UsuarioExcluido = new DevExpress.XtraEditors.ToggleSwitch();
            this.textEdit_UsuarioDtNasc = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup_RegisterUsuario = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleLabelItem_RegisterUsuario = new DevExpress.XtraLayout.SimpleLabelItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup_Usuario = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem18 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem_UsuarioCNPJ = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_UsuarioSMS = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_UsuarioGenero = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_UsuarioTipo = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_UsuarioCPF = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_UsuarioUltNome = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_UsuarioSenha3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_UsuarioSenha = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_UsuarioComentarios = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem_TabPrecoDtUltAlteracao = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_TabPrecoExcluido = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_UsuarioEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem17 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem_UsuarioDtNasc = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.tabNavigationPage_DataGridUsuario = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.gridControl_Usuario = new DevExpress.XtraGrid.GridControl();
            this.gridView_Usuario = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coltype = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcpf = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcnpj = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colemail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colsms = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colphone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colgender = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colbirth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colpassword = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcomments = new DevExpress.XtraGrid.Columns.GridColumn();
            this.collast_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcellphone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colpermission = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colfirst_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colin_trash = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colentity_groups_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colnewsletters = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colshipment_addresses_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colallowed_discount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcreated_at = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.tabPane_ConfigUsuario = new DevExpress.XtraBars.Navigation.TabPane();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barButtonItem_Gravar = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem_Cancelar = new DevExpress.XtraBars.BarButtonItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barButtonItem_Add = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem_Edit = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem_Del = new DevExpress.XtraBars.BarButtonItem();
            this.barManager_ContasMP = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem_RegisterClientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.usuarioBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meusPedidosDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vendedorBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).BeginInit();
            this.tabPane1.SuspendLayout();
            this.tabNavigationPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toggleSwitch1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CLITelefone1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CatProd)).BeginInit();
            this.tabNavigationPage_RegisterUsuario.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl_RegisterUsuario)).BeginInit();
            this.panelControl_RegisterUsuario.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_Usuario)).BeginInit();
            this.layoutControl_Usuario.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_UsuarioCNPJ.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_UsuarioCPF.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_UsuarioSMS.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_UsuarioGenero.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_UsuarioSenha.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_UsuarioTipo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_UsuarioEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_UsuarioUltNome.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_UsuarioPNome.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_UsuarioComentarios.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_UsuarioDtCriacao.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_UsuarioDtCriacao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_UsuarioExcluido.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_UsuarioDtNasc.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_UsuarioDtNasc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_RegisterUsuario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem_RegisterUsuario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_Usuario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_UsuarioCNPJ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_UsuarioSMS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_UsuarioGenero)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_UsuarioTipo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_UsuarioCPF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_UsuarioUltNome)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_UsuarioSenha3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_UsuarioSenha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_UsuarioComentarios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_TabPrecoDtUltAlteracao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_TabPrecoExcluido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_UsuarioEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_UsuarioDtNasc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            this.tabNavigationPage_DataGridUsuario.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_Usuario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_Usuario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane_ConfigUsuario)).BeginInit();
            this.tabPane_ConfigUsuario.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.barManager_ContasMP)).BeginInit();
            this.SuspendLayout();
            // 
            // tileNavSubItem_Cliente
            // 
            this.tileNavSubItem_Cliente.Caption = "Cliente";
            this.tileNavSubItem_Cliente.Name = "tileNavSubItem_Cliente";
            // 
            // 
            // 
            this.tileNavSubItem_Cliente.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement1.Text = "Cliente";
            this.tileNavSubItem_Cliente.Tile.Elements.Add(tileItemElement1);
            this.tileNavSubItem_Cliente.Tile.Name = "tileBarItem3";
            // 
            // tileNavSubItem_CondicaoPagamentoCliente
            // 
            this.tileNavSubItem_CondicaoPagamentoCliente.Caption = "Condições de Pagamento por Cliente";
            this.tileNavSubItem_CondicaoPagamentoCliente.Name = "tileNavSubItem_CondicaoPagamentoCliente";
            // 
            // 
            // 
            this.tileNavSubItem_CondicaoPagamentoCliente.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement2.Text = "Condições de Pagamento por Cliente";
            this.tileNavSubItem_CondicaoPagamentoCliente.Tile.Elements.Add(tileItemElement2);
            this.tileNavSubItem_CondicaoPagamentoCliente.Tile.Name = "tileBarItem4";
            // 
            // tileNavSubItem_CategoriasCliente
            // 
            this.tileNavSubItem_CategoriasCliente.Caption = "Categorias por Cliente";
            this.tileNavSubItem_CategoriasCliente.Name = "tileNavSubItem_CategoriasCliente";
            // 
            // 
            // 
            this.tileNavSubItem_CategoriasCliente.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement3.Text = "Categorias por Cliente";
            this.tileNavSubItem_CategoriasCliente.Tile.Elements.Add(tileItemElement3);
            this.tileNavSubItem_CategoriasCliente.Tile.Name = "tileBarItem6";
            // 
            // tileNavSubItem_TabelaPreco
            // 
            this.tileNavSubItem_TabelaPreco.Caption = "Tabelas de Preço por Cliente";
            this.tileNavSubItem_TabelaPreco.Name = "tileNavSubItem_TabelaPreco";
            // 
            // 
            // 
            this.tileNavSubItem_TabelaPreco.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement4.Text = "Tabelas de Preço por Cliente";
            this.tileNavSubItem_TabelaPreco.Tile.Elements.Add(tileItemElement4);
            this.tileNavSubItem_TabelaPreco.Tile.Name = "tileBarItem7";
            // 
            // tileNavSubItem_Usuarios
            // 
            this.tileNavSubItem_Usuarios.Caption = "Usuários (Vendedores)";
            this.tileNavSubItem_Usuarios.Name = "tileNavSubItem_Usuarios";
            // 
            // 
            // 
            this.tileNavSubItem_Usuarios.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement5.Text = "Usuários (Vendedores)";
            this.tileNavSubItem_Usuarios.Tile.Elements.Add(tileItemElement5);
            this.tileNavSubItem_Usuarios.Tile.Name = "tileBarItem8";
            // 
            // tileNavSubItem_Produto
            // 
            this.tileNavSubItem_Produto.Caption = "Produto";
            this.tileNavSubItem_Produto.Name = "tileNavSubItem_Produto";
            // 
            // 
            // 
            this.tileNavSubItem_Produto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement6.Text = "Produto";
            this.tileNavSubItem_Produto.Tile.Elements.Add(tileItemElement6);
            this.tileNavSubItem_Produto.Tile.Name = "tileBarItem9";
            // 
            // tileNavSubItem_ImagensProduto
            // 
            this.tileNavSubItem_ImagensProduto.Caption = "Imagens do Produto";
            this.tileNavSubItem_ImagensProduto.Name = "tileNavSubItem_ImagensProduto";
            // 
            // 
            // 
            this.tileNavSubItem_ImagensProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement7.Text = "Imagens do Produto";
            this.tileNavSubItem_ImagensProduto.Tile.Elements.Add(tileItemElement7);
            this.tileNavSubItem_ImagensProduto.Tile.Name = "tileBarItem10";
            // 
            // tileNavSubItem_TabelaProduto
            // 
            this.tileNavSubItem_TabelaProduto.Caption = "Tabela de Preço";
            this.tileNavSubItem_TabelaProduto.Name = "tileNavSubItem_TabelaProduto";
            // 
            // 
            // 
            this.tileNavSubItem_TabelaProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement8.Text = "Tabelas de Preço";
            this.tileNavSubItem_TabelaProduto.Tile.Elements.Add(tileItemElement8);
            this.tileNavSubItem_TabelaProduto.Tile.Name = "tileBarItem11";
            // 
            // tileNavSubItem_TabelaPrecoProduto
            // 
            this.tileNavSubItem_TabelaPrecoProduto.Caption = "Tabelas de Preço por Produto";
            this.tileNavSubItem_TabelaPrecoProduto.Name = "tileNavSubItem_TabelaPrecoProduto";
            // 
            // 
            // 
            this.tileNavSubItem_TabelaPrecoProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement9.Text = "Tabelas de Preço por Produto";
            this.tileNavSubItem_TabelaPrecoProduto.Tile.Elements.Add(tileItemElement9);
            this.tileNavSubItem_TabelaPrecoProduto.Tile.Name = "tileBarItem12";
            // 
            // tileNavSubItem_CategoriaProduto
            // 
            this.tileNavSubItem_CategoriaProduto.Caption = "Categorias de Produtos";
            this.tileNavSubItem_CategoriaProduto.Name = "tileNavSubItem_CategoriaProduto";
            // 
            // 
            // 
            this.tileNavSubItem_CategoriaProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement10.Text = "Categorias de Produtos";
            this.tileNavSubItem_CategoriaProduto.Tile.Elements.Add(tileItemElement10);
            this.tileNavSubItem_CategoriaProduto.Tile.Name = "tileBarItem13";
            // 
            // tileNavSubItem_Pedido
            // 
            this.tileNavSubItem_Pedido.Caption = "Pedido";
            this.tileNavSubItem_Pedido.Name = "tileNavSubItem_Pedido";
            // 
            // 
            // 
            this.tileNavSubItem_Pedido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement11.Text = "Pedido";
            this.tileNavSubItem_Pedido.Tile.Elements.Add(tileItemElement11);
            this.tileNavSubItem_Pedido.Tile.Name = "tileBarItem17";
            // 
            // tileNavSubItem_CamposExtraPedido
            // 
            this.tileNavSubItem_CamposExtraPedido.Caption = "Campos Extras do Pedido";
            this.tileNavSubItem_CamposExtraPedido.Name = "tileNavSubItem_CamposExtraPedido";
            // 
            // 
            // 
            this.tileNavSubItem_CamposExtraPedido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement12.Text = "Campos Extras do Pedido";
            this.tileNavSubItem_CamposExtraPedido.Tile.Elements.Add(tileItemElement12);
            this.tileNavSubItem_CamposExtraPedido.Tile.Name = "tileBarItem18";
            // 
            // tileNavSubItem_Transportadora
            // 
            this.tileNavSubItem_Transportadora.Caption = "Transportadoras";
            this.tileNavSubItem_Transportadora.Name = "tileNavSubItem_Transportadora";
            // 
            // 
            // 
            this.tileNavSubItem_Transportadora.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement13.Text = "Transportadoras";
            this.tileNavSubItem_Transportadora.Tile.Elements.Add(tileItemElement13);
            this.tileNavSubItem_Transportadora.Tile.Name = "tileBarItem14";
            // 
            // tileNavSubItem_CondicaoPagamento
            // 
            this.tileNavSubItem_CondicaoPagamento.Caption = "Condições de Pagamento";
            this.tileNavSubItem_CondicaoPagamento.Name = "tileNavSubItem_CondicaoPagamento";
            // 
            // 
            // 
            this.tileNavSubItem_CondicaoPagamento.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement14.Text = "Condições de Pagamento";
            this.tileNavSubItem_CondicaoPagamento.Tile.Elements.Add(tileItemElement14);
            this.tileNavSubItem_CondicaoPagamento.Tile.Name = "tileBarItem15";
            // 
            // tileNavSubItem_FormaPagamento
            // 
            this.tileNavSubItem_FormaPagamento.Caption = "Formas de Pagamento";
            this.tileNavSubItem_FormaPagamento.Name = "tileNavSubItem_FormaPagamento";
            // 
            // 
            // 
            this.tileNavSubItem_FormaPagamento.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement15.Text = "Formas de Pagamento";
            this.tileNavSubItem_FormaPagamento.Tile.Elements.Add(tileItemElement15);
            this.tileNavSubItem_FormaPagamento.Tile.Name = "tileBarItem16";
            // 
            // tileNavSubItem_FaturamentoPedido
            // 
            this.tileNavSubItem_FaturamentoPedido.Caption = "Faturamento de Pedido";
            this.tileNavSubItem_FaturamentoPedido.Name = "tileNavSubItem_FaturamentoPedido";
            // 
            // 
            // 
            this.tileNavSubItem_FaturamentoPedido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement16.Text = "Faturamento de Pedido";
            this.tileNavSubItem_FaturamentoPedido.Tile.Elements.Add(tileItemElement16);
            this.tileNavSubItem_FaturamentoPedido.Tile.Name = "tileBarItem19";
            // 
            // tileNavSubItem_TituloVencido
            // 
            this.tileNavSubItem_TituloVencido.Caption = "Títulos Vencidos";
            this.tileNavSubItem_TituloVencido.Name = "tileNavSubItem_TituloVencido";
            // 
            // 
            // 
            this.tileNavSubItem_TituloVencido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement17.Text = "Títulos Vencidos";
            this.tileNavSubItem_TituloVencido.Tile.Elements.Add(tileItemElement17);
            this.tileNavSubItem_TituloVencido.Tile.Name = "tileBarItem20";
            // 
            // splashScreenManager_User
            // 
            this.splashScreenManager_User.ClosingDelay = 500;
            // 
            // simpleLabelItem_RegisterClientes
            // 
            this.simpleLabelItem_RegisterClientes.AllowHotTrack = false;
            this.simpleLabelItem_RegisterClientes.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem_RegisterClientes.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem_RegisterClientes.Location = new System.Drawing.Point(10, 10);
            this.simpleLabelItem_RegisterClientes.Name = "simpleLabelItem_RegisterClientes";
            this.simpleLabelItem_RegisterClientes.OptionsPrint.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem_RegisterClientes.OptionsPrint.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem_RegisterClientes.Size = new System.Drawing.Size(1185, 17);
            this.simpleLabelItem_RegisterClientes.Text = "Cadastro Clientes";
            this.simpleLabelItem_RegisterClientes.TextSize = new System.Drawing.Size(155, 13);
            // 
            // usuarioBindingSource
            // 
            this.usuarioBindingSource.DataMember = "Usuario";
            this.usuarioBindingSource.DataSource = this.meusPedidosDataSet1;
            // 
            // meusPedidosDataSet1
            // 
            this.meusPedidosDataSet1.DataSetName = "MeusPedidosDataSet";
            this.meusPedidosDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tabPane1
            // 
            this.tabPane1.Controls.Add(this.tabNavigationPage1);
            this.tabPane1.Controls.Add(this.tabNavigationPage2);
            this.tabPane1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPane1.Location = new System.Drawing.Point(0, 94);
            this.tabPane1.Name = "tabPane1";
            this.tabPane1.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.tabNavigationPage1,
            this.tabNavigationPage2});
            this.tabPane1.RegularSize = new System.Drawing.Size(1264, 445);
            this.tabPane1.SelectedPage = this.tabNavigationPage2;
            this.tabPane1.Size = new System.Drawing.Size(1264, 445);
            this.tabPane1.TabIndex = 8;
            this.tabPane1.Text = "Clientes";
            // 
            // tabNavigationPage1
            // 
            this.tabNavigationPage1.Caption = "Clientes";
            this.tabNavigationPage1.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage1.Name = "tabNavigationPage1";
            this.tabNavigationPage1.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage1.Size = new System.Drawing.Size(1246, 381);
            // 
            // tabNavigationPage2
            // 
            this.tabNavigationPage2.Caption = "Cadastro";
            this.tabNavigationPage2.Controls.Add(this.panelControl1);
            this.tabNavigationPage2.Image = ((System.Drawing.Image)(resources.GetObject("tabNavigationPage2.Image")));
            this.tabNavigationPage2.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage2.Name = "tabNavigationPage2";
            this.tabNavigationPage2.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage2.Size = new System.Drawing.Size(1246, 381);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.layoutControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1246, 381);
            this.panelControl1.TabIndex = 0;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.textEdit2);
            this.layoutControl1.Controls.Add(this.textEdit3);
            this.layoutControl1.Controls.Add(this.textEdit4);
            this.layoutControl1.Controls.Add(this.textEdit5);
            this.layoutControl1.Controls.Add(this.textEdit6);
            this.layoutControl1.Controls.Add(this.textEdit7);
            this.layoutControl1.Controls.Add(this.textEdit8);
            this.layoutControl1.Controls.Add(this.textEdit9);
            this.layoutControl1.Controls.Add(this.textEdit10);
            this.layoutControl1.Controls.Add(this.textEdit11);
            this.layoutControl1.Controls.Add(this.textEdit12);
            this.layoutControl1.Controls.Add(this.textEdit13);
            this.layoutControl1.Controls.Add(this.textEdit14);
            this.layoutControl1.Controls.Add(this.dateEdit1);
            this.layoutControl1.Controls.Add(this.toggleSwitch1);
            this.layoutControl1.Controls.Add(this.memoEdit1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup3;
            this.layoutControl1.Size = new System.Drawing.Size(1242, 377);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textEdit2
            // 
            this.textEdit2.Location = new System.Drawing.Point(136, 83);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Size = new System.Drawing.Size(1072, 20);
            this.textEdit2.StyleController = this.layoutControl1;
            this.textEdit2.TabIndex = 4;
            this.textEdit2.Visible = false;
            // 
            // textEdit3
            // 
            this.textEdit3.Location = new System.Drawing.Point(136, 107);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Size = new System.Drawing.Size(1072, 20);
            this.textEdit3.StyleController = this.layoutControl1;
            this.textEdit3.TabIndex = 5;
            this.textEdit3.Visible = false;
            // 
            // textEdit4
            // 
            this.textEdit4.Location = new System.Drawing.Point(136, 131);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Properties.MaxLength = 1;
            this.textEdit4.Size = new System.Drawing.Size(1072, 20);
            this.textEdit4.StyleController = this.layoutControl1;
            this.textEdit4.TabIndex = 6;
            this.textEdit4.Visible = false;
            // 
            // textEdit5
            // 
            this.textEdit5.Location = new System.Drawing.Point(136, 155);
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Size = new System.Drawing.Size(1072, 20);
            this.textEdit5.StyleController = this.layoutControl1;
            this.textEdit5.TabIndex = 7;
            this.textEdit5.Visible = false;
            // 
            // textEdit6
            // 
            this.textEdit6.Location = new System.Drawing.Point(136, 179);
            this.textEdit6.Name = "textEdit6";
            this.textEdit6.Size = new System.Drawing.Size(1072, 20);
            this.textEdit6.StyleController = this.layoutControl1;
            this.textEdit6.TabIndex = 8;
            this.textEdit6.Visible = false;
            // 
            // textEdit7
            // 
            this.textEdit7.Location = new System.Drawing.Point(136, 203);
            this.textEdit7.Name = "textEdit7";
            this.textEdit7.Size = new System.Drawing.Size(1072, 20);
            this.textEdit7.StyleController = this.layoutControl1;
            this.textEdit7.TabIndex = 9;
            this.textEdit7.Visible = false;
            // 
            // textEdit8
            // 
            this.textEdit8.Location = new System.Drawing.Point(136, 303);
            this.textEdit8.Name = "textEdit8";
            this.textEdit8.Size = new System.Drawing.Size(1072, 20);
            this.textEdit8.StyleController = this.layoutControl1;
            this.textEdit8.TabIndex = 10;
            // 
            // textEdit9
            // 
            this.textEdit9.Location = new System.Drawing.Point(136, 327);
            this.textEdit9.Name = "textEdit9";
            this.textEdit9.Size = new System.Drawing.Size(1072, 20);
            this.textEdit9.StyleController = this.layoutControl1;
            this.textEdit9.TabIndex = 11;
            // 
            // textEdit10
            // 
            this.textEdit10.Location = new System.Drawing.Point(136, 351);
            this.textEdit10.Name = "textEdit10";
            this.textEdit10.Size = new System.Drawing.Size(1072, 20);
            this.textEdit10.StyleController = this.layoutControl1;
            this.textEdit10.TabIndex = 12;
            // 
            // textEdit11
            // 
            this.textEdit11.Location = new System.Drawing.Point(136, 375);
            this.textEdit11.Name = "textEdit11";
            this.textEdit11.Size = new System.Drawing.Size(1072, 20);
            this.textEdit11.StyleController = this.layoutControl1;
            this.textEdit11.TabIndex = 13;
            // 
            // textEdit12
            // 
            this.textEdit12.Location = new System.Drawing.Point(136, 399);
            this.textEdit12.Name = "textEdit12";
            this.textEdit12.Size = new System.Drawing.Size(1072, 20);
            this.textEdit12.StyleController = this.layoutControl1;
            this.textEdit12.TabIndex = 14;
            // 
            // textEdit13
            // 
            this.textEdit13.Location = new System.Drawing.Point(136, 423);
            this.textEdit13.Name = "textEdit13";
            this.textEdit13.Size = new System.Drawing.Size(1072, 20);
            this.textEdit13.StyleController = this.layoutControl1;
            this.textEdit13.TabIndex = 15;
            // 
            // textEdit14
            // 
            this.textEdit14.Location = new System.Drawing.Point(136, 227);
            this.textEdit14.Name = "textEdit14";
            this.textEdit14.Size = new System.Drawing.Size(1072, 20);
            this.textEdit14.StyleController = this.layoutControl1;
            this.textEdit14.TabIndex = 16;
            this.textEdit14.Visible = false;
            // 
            // dateEdit1
            // 
            this.dateEdit1.EditValue = null;
            this.dateEdit1.Location = new System.Drawing.Point(980, 499);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.ReadOnly = true;
            this.dateEdit1.Size = new System.Drawing.Size(228, 20);
            this.dateEdit1.StyleController = this.layoutControl1;
            this.dateEdit1.TabIndex = 17;
            // 
            // toggleSwitch1
            // 
            this.toggleSwitch1.Location = new System.Drawing.Point(980, 523);
            this.toggleSwitch1.Name = "toggleSwitch1";
            this.toggleSwitch1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.toggleSwitch1.Properties.OffText = "Não";
            this.toggleSwitch1.Properties.OnText = "Sim";
            this.toggleSwitch1.Size = new System.Drawing.Size(228, 24);
            this.toggleSwitch1.StyleController = this.layoutControl1;
            this.toggleSwitch1.TabIndex = 10;
            // 
            // memoEdit1
            // 
            this.memoEdit1.Location = new System.Drawing.Point(136, 499);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(728, 48);
            this.memoEdit1.StyleController = this.layoutControl1;
            this.memoEdit1.TabIndex = 18;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem6,
            this.emptySpaceItem8,
            this.emptySpaceItem11,
            this.emptySpaceItem12,
            this.simpleLabelItem1,
            this.emptySpaceItem13,
            this.emptySpaceItem14,
            this.layoutControlGroup4,
            this.emptySpaceItem15,
            this.layoutControlGroup5,
            this.tabbedControlGroup3});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "Root";
            this.layoutControlGroup3.Size = new System.Drawing.Size(1242, 581);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(10, 0);
            this.emptySpaceItem6.Name = "emptySpaceItem1";
            this.emptySpaceItem6.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.Location = new System.Drawing.Point(10, 251);
            this.emptySpaceItem8.Name = "emptySpaceItem2";
            this.emptySpaceItem8.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem11.Name = "emptySpaceItem3";
            this.emptySpaceItem11.Size = new System.Drawing.Size(10, 561);
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.Location = new System.Drawing.Point(1212, 0);
            this.emptySpaceItem12.Name = "emptySpaceItem4";
            this.emptySpaceItem12.Size = new System.Drawing.Size(10, 561);
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleLabelItem1
            // 
            this.simpleLabelItem1.AllowHotTrack = false;
            this.simpleLabelItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem1.Location = new System.Drawing.Point(10, 10);
            this.simpleLabelItem1.Name = "simpleLabelItem_RegisterClientes";
            this.simpleLabelItem1.OptionsPrint.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem1.OptionsPrint.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem1.Size = new System.Drawing.Size(1202, 17);
            this.simpleLabelItem1.Text = "Cadastro Clientes";
            this.simpleLabelItem1.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.Location = new System.Drawing.Point(10, 27);
            this.emptySpaceItem13.Name = "emptySpaceItem5";
            this.emptySpaceItem13.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.Location = new System.Drawing.Point(10, 447);
            this.emptySpaceItem14.Name = "emptySpaceItem7";
            this.emptySpaceItem14.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9});
            this.layoutControlGroup4.Location = new System.Drawing.Point(10, 261);
            this.layoutControlGroup4.Name = "layoutControlGroup_Endereço";
            this.layoutControlGroup4.Size = new System.Drawing.Size(1202, 186);
            this.layoutControlGroup4.Text = "Endereço";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.textEdit8;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem_CLIRua";
            this.layoutControlItem4.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem4.Text = "Rua";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.textEdit9;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem5.Name = "layoutControlItem_CLIComplemento";
            this.layoutControlItem5.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem5.Text = "Complemento";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.textEdit10;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem6.Name = "layoutControlItem_CLICep";
            this.layoutControlItem6.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem6.Text = "CEP";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.textEdit11;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem7.Name = "layoutControlItem_Bairro";
            this.layoutControlItem7.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem7.Text = "Bairro";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.textEdit12;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem8.Name = "layoutControlItem_Cidade";
            this.layoutControlItem8.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem8.Text = "Cidade";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.textEdit13;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem9.Name = "layoutControlItem_CLIEstado";
            this.layoutControlItem9.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem9.Text = "Estado";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.Location = new System.Drawing.Point(10, 551);
            this.emptySpaceItem15.Name = "emptySpaceItem9";
            this.emptySpaceItem15.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.emptySpaceItem16,
            this.layoutControlItem12});
            this.layoutControlGroup5.Location = new System.Drawing.Point(10, 457);
            this.layoutControlGroup5.Name = "layoutControlGroup1";
            this.layoutControlGroup5.Size = new System.Drawing.Size(1202, 94);
            this.layoutControlGroup5.Text = "Status";
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.dateEdit1;
            this.layoutControlItem10.Location = new System.Drawing.Point(844, 0);
            this.layoutControlItem10.Name = "layoutControlItem_CLIUltimaAlteracao";
            this.layoutControlItem10.Size = new System.Drawing.Size(334, 24);
            this.layoutControlItem10.Text = "Última Alteração";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.toggleSwitch1;
            this.layoutControlItem11.CustomizationFormText = "Status";
            this.layoutControlItem11.Location = new System.Drawing.Point(844, 24);
            this.layoutControlItem11.Name = "layoutControlItem_CLIExcluido";
            this.layoutControlItem11.Size = new System.Drawing.Size(334, 28);
            this.layoutControlItem11.Text = "Excluído";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem16
            // 
            this.emptySpaceItem16.AllowHotTrack = false;
            this.emptySpaceItem16.Location = new System.Drawing.Point(834, 0);
            this.emptySpaceItem16.Name = "emptySpaceItem10";
            this.emptySpaceItem16.Size = new System.Drawing.Size(10, 52);
            this.emptySpaceItem16.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.memoEdit1;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem12.Name = "layoutControlItem_CLIObservacao";
            this.layoutControlItem12.Size = new System.Drawing.Size(834, 52);
            this.layoutControlItem12.Text = "Observação";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(99, 13);
            // 
            // tabbedControlGroup3
            // 
            this.tabbedControlGroup3.Location = new System.Drawing.Point(10, 37);
            this.tabbedControlGroup3.Name = "tabbedControlGroup1";
            this.tabbedControlGroup3.SelectedTabPage = this.layoutControlGroup6;
            this.tabbedControlGroup3.SelectedTabPageIndex = 1;
            this.tabbedControlGroup3.Size = new System.Drawing.Size(1202, 214);
            this.tabbedControlGroup3.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup9,
            this.layoutControlGroup6});
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup4});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup_CLIContato";
            this.layoutControlGroup6.Size = new System.Drawing.Size(1178, 168);
            this.layoutControlGroup6.Text = "Contatos";
            // 
            // tabbedControlGroup4
            // 
            this.tabbedControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup4.Name = "tabbedControlGroup2";
            this.tabbedControlGroup4.SelectedTabPage = this.layoutControlGroup7;
            this.tabbedControlGroup4.SelectedTabPageIndex = 0;
            this.tabbedControlGroup4.Size = new System.Drawing.Size(1178, 168);
            this.tabbedControlGroup4.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup7,
            this.layoutControlGroup8});
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleSeparator2});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup_CLITelefone";
            this.layoutControlGroup7.Size = new System.Drawing.Size(1154, 122);
            this.layoutControlGroup7.Text = "Telefone";
            // 
            // simpleSeparator2
            // 
            this.simpleSeparator2.AllowHotTrack = false;
            this.simpleSeparator2.Location = new System.Drawing.Point(0, 0);
            this.simpleSeparator2.Name = "simpleSeparator1";
            this.simpleSeparator2.Size = new System.Drawing.Size(1154, 122);
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup8.Name = "layoutControlGroup_CLIEmail";
            this.layoutControlGroup8.Size = new System.Drawing.Size(1154, 122);
            this.layoutControlGroup8.Text = "E-mail";
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.layoutControlItem23});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup9.Name = "layoutControlGroup_CLIDados";
            this.layoutControlGroup9.Size = new System.Drawing.Size(1178, 168);
            this.layoutControlGroup9.Text = "Dados";
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.textEdit2;
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem17.Name = "layoutControlItem_CLIRazaoSocial";
            this.layoutControlItem17.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem17.Text = "Razão Social";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.textEdit3;
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem18.Name = "layoutControlItem_CLINomeFantasia";
            this.layoutControlItem18.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem18.Text = "Nome Fantasia";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.textEdit4;
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem19.Name = "layoutControlItem_CLITipo";
            this.layoutControlItem19.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem19.Text = "Tipo";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.textEdit5;
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem20.Name = "layoutControlItem_CLICnpj";
            this.layoutControlItem20.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem20.Text = "CNPJ";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.textEdit6;
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem21.Name = "layoutControlItem_CLIInscricaoEstadual";
            this.layoutControlItem21.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem21.Text = "Inscrição Estadual";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.textEdit7;
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem22.Name = "layoutControlItem_CLISuframa";
            this.layoutControlItem22.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem22.Text = "Suframa";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.textEdit14;
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItem23.Name = "layoutControlItem_CLINomeExcFiscal";
            this.layoutControlItem23.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem23.Text = "Nome Exceção Fiscal";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(99, 13);
            // 
            // simpleSeparator3
            // 
            this.simpleSeparator3.AllowHotTrack = false;
            this.simpleSeparator3.CustomizationFormText = "simpleSeparator1";
            this.simpleSeparator3.Location = new System.Drawing.Point(1005, 0);
            this.simpleSeparator3.Name = "simpleSeparator3";
            this.simpleSeparator3.Size = new System.Drawing.Size(2, 26);
            this.simpleSeparator3.Text = "simpleSeparator1";
            // 
            // layoutControlGroup_CLITelefone1
            // 
            this.layoutControlGroup_CLITelefone1.CustomizationFormText = "Telefone";
            this.layoutControlGroup_CLITelefone1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleSeparator3});
            this.layoutControlGroup_CLITelefone1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup_CLITelefone1.Name = "layoutControlGroup_CLITelefone1";
            this.layoutControlGroup_CLITelefone1.Size = new System.Drawing.Size(1205, 31);
            this.layoutControlGroup_CLITelefone1.Text = "Telefone";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup_CLITelefone1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 561);
            this.layoutControlGroup2.Name = "LayoutRootGroupForRestore";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1205, 31);
            this.layoutControlGroup2.Tag = "LayoutRootGroupForRestore";
            // 
            // colid1
            // 
            this.colid1.FieldName = "id";
            this.colid1.Name = "colid1";
            // 
            // colrazao_social
            // 
            this.colrazao_social.FieldName = "razao_social";
            this.colrazao_social.Name = "colrazao_social";
            // 
            // colnome_fantasia
            // 
            this.colnome_fantasia.FieldName = "nome_fantasia";
            this.colnome_fantasia.Name = "colnome_fantasia";
            // 
            // layoutControlGroup_CatProd
            // 
            this.layoutControlGroup_CatProd.Location = new System.Drawing.Point(10, 37);
            this.layoutControlGroup_CatProd.Name = "layoutControlGroup_CatProd";
            this.layoutControlGroup_CatProd.Size = new System.Drawing.Size(1202, 218);
            // 
            // usuarioTableAdapter
            // 
            this.usuarioTableAdapter.ClearBeforeFill = true;
            // 
            // tabNavigationPage_RegisterUsuario
            // 
            this.tabNavigationPage_RegisterUsuario.Caption = "Cadastro";
            this.tabNavigationPage_RegisterUsuario.Controls.Add(this.panelControl_RegisterUsuario);
            this.tabNavigationPage_RegisterUsuario.Image = ((System.Drawing.Image)(resources.GetObject("tabNavigationPage_RegisterUsuario.Image")));
            this.tabNavigationPage_RegisterUsuario.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage_RegisterUsuario.Name = "tabNavigationPage_RegisterUsuario";
            this.tabNavigationPage_RegisterUsuario.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage_RegisterUsuario.Size = new System.Drawing.Size(1246, 404);
            // 
            // panelControl_RegisterUsuario
            // 
            this.panelControl_RegisterUsuario.Controls.Add(this.layoutControl_Usuario);
            this.panelControl_RegisterUsuario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl_RegisterUsuario.Location = new System.Drawing.Point(0, 0);
            this.panelControl_RegisterUsuario.Name = "panelControl_RegisterUsuario";
            this.panelControl_RegisterUsuario.Size = new System.Drawing.Size(1246, 404);
            this.panelControl_RegisterUsuario.TabIndex = 0;
            // 
            // layoutControl_Usuario
            // 
            this.layoutControl_Usuario.Controls.Add(this.textEdit_UsuarioCNPJ);
            this.layoutControl_Usuario.Controls.Add(this.textEdit_UsuarioCPF);
            this.layoutControl_Usuario.Controls.Add(this.textEdit_UsuarioSMS);
            this.layoutControl_Usuario.Controls.Add(this.textEdit_UsuarioGenero);
            this.layoutControl_Usuario.Controls.Add(this.textEdit_UsuarioSenha);
            this.layoutControl_Usuario.Controls.Add(this.textEdit_UsuarioTipo);
            this.layoutControl_Usuario.Controls.Add(this.textEdit_UsuarioEmail);
            this.layoutControl_Usuario.Controls.Add(this.textEdit_UsuarioUltNome);
            this.layoutControl_Usuario.Controls.Add(this.textEdit_UsuarioPNome);
            this.layoutControl_Usuario.Controls.Add(this.textEdit_UsuarioComentarios);
            this.layoutControl_Usuario.Controls.Add(this.dateEdit_UsuarioDtCriacao);
            this.layoutControl_Usuario.Controls.Add(this.textEdit_UsuarioExcluido);
            this.layoutControl_Usuario.Controls.Add(this.textEdit_UsuarioDtNasc);
            this.layoutControl_Usuario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl_Usuario.Location = new System.Drawing.Point(2, 2);
            this.layoutControl_Usuario.Name = "layoutControl_Usuario";
            this.layoutControl_Usuario.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(124, 125, 450, 400);
            this.layoutControl_Usuario.Root = this.layoutControlGroup_RegisterUsuario;
            this.layoutControl_Usuario.Size = new System.Drawing.Size(1242, 400);
            this.layoutControl_Usuario.TabIndex = 0;
            this.layoutControl_Usuario.Text = "layoutControl_ProdCat";
            // 
            // textEdit_UsuarioCNPJ
            // 
            this.textEdit_UsuarioCNPJ.Location = new System.Drawing.Point(140, 171);
            this.textEdit_UsuarioCNPJ.Name = "textEdit_UsuarioCNPJ";
            this.textEdit_UsuarioCNPJ.Size = new System.Drawing.Size(465, 20);
            this.textEdit_UsuarioCNPJ.StyleController = this.layoutControl_Usuario;
            this.textEdit_UsuarioCNPJ.TabIndex = 7;
            // 
            // textEdit_UsuarioCPF
            // 
            this.textEdit_UsuarioCPF.Location = new System.Drawing.Point(715, 171);
            this.textEdit_UsuarioCPF.Name = "textEdit_UsuarioCPF";
            this.textEdit_UsuarioCPF.Size = new System.Drawing.Size(476, 20);
            this.textEdit_UsuarioCPF.StyleController = this.layoutControl_Usuario;
            this.textEdit_UsuarioCPF.TabIndex = 7;
            // 
            // textEdit_UsuarioSMS
            // 
            this.textEdit_UsuarioSMS.Location = new System.Drawing.Point(140, 219);
            this.textEdit_UsuarioSMS.Name = "textEdit_UsuarioSMS";
            this.textEdit_UsuarioSMS.Size = new System.Drawing.Size(1051, 20);
            this.textEdit_UsuarioSMS.StyleController = this.layoutControl_Usuario;
            this.textEdit_UsuarioSMS.TabIndex = 7;
            // 
            // textEdit_UsuarioGenero
            // 
            this.textEdit_UsuarioGenero.Location = new System.Drawing.Point(140, 243);
            this.textEdit_UsuarioGenero.Name = "textEdit_UsuarioGenero";
            this.textEdit_UsuarioGenero.Size = new System.Drawing.Size(1051, 20);
            this.textEdit_UsuarioGenero.StyleController = this.layoutControl_Usuario;
            this.textEdit_UsuarioGenero.TabIndex = 7;
            // 
            // textEdit_UsuarioSenha
            // 
            this.textEdit_UsuarioSenha.Location = new System.Drawing.Point(409, 113);
            this.textEdit_UsuarioSenha.Name = "textEdit_UsuarioSenha";
            this.textEdit_UsuarioSenha.Properties.UseSystemPasswordChar = true;
            this.textEdit_UsuarioSenha.Size = new System.Drawing.Size(408, 20);
            this.textEdit_UsuarioSenha.StyleController = this.layoutControl_Usuario;
            this.textEdit_UsuarioSenha.TabIndex = 7;
            // 
            // textEdit_UsuarioTipo
            // 
            this.textEdit_UsuarioTipo.Location = new System.Drawing.Point(140, 195);
            this.textEdit_UsuarioTipo.Name = "textEdit_UsuarioTipo";
            this.textEdit_UsuarioTipo.Size = new System.Drawing.Size(1051, 20);
            this.textEdit_UsuarioTipo.StyleController = this.layoutControl_Usuario;
            this.textEdit_UsuarioTipo.TabIndex = 7;
            // 
            // textEdit_UsuarioEmail
            // 
            this.textEdit_UsuarioEmail.Location = new System.Drawing.Point(409, 89);
            this.textEdit_UsuarioEmail.Name = "textEdit_UsuarioEmail";
            this.textEdit_UsuarioEmail.Size = new System.Drawing.Size(408, 20);
            this.textEdit_UsuarioEmail.StyleController = this.layoutControl_Usuario;
            this.textEdit_UsuarioEmail.TabIndex = 7;
            // 
            // textEdit_UsuarioUltNome
            // 
            this.textEdit_UsuarioUltNome.Location = new System.Drawing.Point(140, 267);
            this.textEdit_UsuarioUltNome.Name = "textEdit_UsuarioUltNome";
            this.textEdit_UsuarioUltNome.Size = new System.Drawing.Size(465, 20);
            this.textEdit_UsuarioUltNome.StyleController = this.layoutControl_Usuario;
            this.textEdit_UsuarioUltNome.TabIndex = 7;
            // 
            // textEdit_UsuarioPNome
            // 
            this.textEdit_UsuarioPNome.Location = new System.Drawing.Point(715, 267);
            this.textEdit_UsuarioPNome.Name = "textEdit_UsuarioPNome";
            this.textEdit_UsuarioPNome.Size = new System.Drawing.Size(476, 20);
            this.textEdit_UsuarioPNome.StyleController = this.layoutControl_Usuario;
            this.textEdit_UsuarioPNome.TabIndex = 7;
            // 
            // textEdit_UsuarioComentarios
            // 
            this.textEdit_UsuarioComentarios.Location = new System.Drawing.Point(140, 291);
            this.textEdit_UsuarioComentarios.MinimumSize = new System.Drawing.Size(0, 40);
            this.textEdit_UsuarioComentarios.Name = "textEdit_UsuarioComentarios";
            this.textEdit_UsuarioComentarios.Size = new System.Drawing.Size(1051, 40);
            this.textEdit_UsuarioComentarios.StyleController = this.layoutControl_Usuario;
            this.textEdit_UsuarioComentarios.TabIndex = 7;
            // 
            // dateEdit_UsuarioDtCriacao
            // 
            this.dateEdit_UsuarioDtCriacao.EditValue = null;
            this.dateEdit_UsuarioDtCriacao.Enabled = false;
            this.dateEdit_UsuarioDtCriacao.Location = new System.Drawing.Point(140, 335);
            this.dateEdit_UsuarioDtCriacao.Name = "dateEdit_UsuarioDtCriacao";
            this.dateEdit_UsuarioDtCriacao.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit_UsuarioDtCriacao.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit_UsuarioDtCriacao.Size = new System.Drawing.Size(465, 20);
            this.dateEdit_UsuarioDtCriacao.StyleController = this.layoutControl_Usuario;
            this.dateEdit_UsuarioDtCriacao.TabIndex = 7;
            // 
            // textEdit_UsuarioExcluido
            // 
            this.textEdit_UsuarioExcluido.Location = new System.Drawing.Point(715, 335);
            this.textEdit_UsuarioExcluido.Name = "textEdit_UsuarioExcluido";
            this.textEdit_UsuarioExcluido.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.textEdit_UsuarioExcluido.Properties.OffText = "Não";
            this.textEdit_UsuarioExcluido.Properties.OnText = "Sim";
            this.textEdit_UsuarioExcluido.Size = new System.Drawing.Size(476, 24);
            this.textEdit_UsuarioExcluido.StyleController = this.layoutControl_Usuario;
            this.textEdit_UsuarioExcluido.TabIndex = 10;
            // 
            // textEdit_UsuarioDtNasc
            // 
            this.textEdit_UsuarioDtNasc.EditValue = null;
            this.textEdit_UsuarioDtNasc.Location = new System.Drawing.Point(140, 147);
            this.textEdit_UsuarioDtNasc.Name = "textEdit_UsuarioDtNasc";
            this.textEdit_UsuarioDtNasc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.textEdit_UsuarioDtNasc.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.textEdit_UsuarioDtNasc.Properties.EditFormat.FormatString = "";
            this.textEdit_UsuarioDtNasc.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.textEdit_UsuarioDtNasc.Properties.Mask.EditMask = "";
            this.textEdit_UsuarioDtNasc.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.textEdit_UsuarioDtNasc.Size = new System.Drawing.Size(1051, 20);
            this.textEdit_UsuarioDtNasc.StyleController = this.layoutControl_Usuario;
            this.textEdit_UsuarioDtNasc.TabIndex = 7;
            // 
            // layoutControlGroup_RegisterUsuario
            // 
            this.layoutControlGroup_RegisterUsuario.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup_RegisterUsuario.GroupBordersVisible = false;
            this.layoutControlGroup_RegisterUsuario.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.emptySpaceItem4,
            this.simpleLabelItem_RegisterUsuario,
            this.emptySpaceItem5,
            this.emptySpaceItem9,
            this.layoutControlGroup_Usuario,
            this.emptySpaceItem3});
            this.layoutControlGroup_RegisterUsuario.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup_RegisterUsuario.Name = "Root";
            this.layoutControlGroup_RegisterUsuario.Size = new System.Drawing.Size(1225, 403);
            this.layoutControlGroup_RegisterUsuario.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(10, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(1185, 10);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(1195, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(10, 383);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleLabelItem_RegisterUsuario
            // 
            this.simpleLabelItem_RegisterUsuario.AllowHotTrack = false;
            this.simpleLabelItem_RegisterUsuario.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem_RegisterUsuario.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem_RegisterUsuario.Location = new System.Drawing.Point(10, 10);
            this.simpleLabelItem_RegisterUsuario.Name = "simpleLabelItem_RegisterUsuario";
            this.simpleLabelItem_RegisterUsuario.OptionsPrint.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem_RegisterUsuario.OptionsPrint.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem_RegisterUsuario.Size = new System.Drawing.Size(1185, 17);
            this.simpleLabelItem_RegisterUsuario.Text = "Cadastro de Empresa";
            this.simpleLabelItem_RegisterUsuario.TextSize = new System.Drawing.Size(103, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(10, 27);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(1185, 10);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.Location = new System.Drawing.Point(10, 373);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(1185, 10);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup_Usuario
            // 
            this.layoutControlGroup_Usuario.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem10,
            this.emptySpaceItem18,
            this.layoutControlItem_UsuarioCNPJ,
            this.layoutControlItem_UsuarioSMS,
            this.layoutControlItem_UsuarioGenero,
            this.layoutControlItem_UsuarioTipo,
            this.layoutControlItem_UsuarioCPF,
            this.layoutControlItem_UsuarioUltNome,
            this.layoutControlItem_UsuarioSenha3,
            this.layoutControlItem_UsuarioSenha,
            this.layoutControlItem_UsuarioComentarios,
            this.emptySpaceItem2,
            this.layoutControlItem_TabPrecoDtUltAlteracao,
            this.layoutControlItem_TabPrecoExcluido,
            this.layoutControlItem_UsuarioEmail,
            this.emptySpaceItem7,
            this.emptySpaceItem17,
            this.layoutControlItem_UsuarioDtNasc});
            this.layoutControlGroup_Usuario.Location = new System.Drawing.Point(10, 37);
            this.layoutControlGroup_Usuario.Name = "layoutControlGroup_Usuario";
            this.layoutControlGroup_Usuario.Size = new System.Drawing.Size(1185, 336);
            this.layoutControlGroup_Usuario.Text = "Empresa";
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(1161, 10);
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem18
            // 
            this.emptySpaceItem18.AllowHotTrack = false;
            this.emptySpaceItem18.Location = new System.Drawing.Point(0, 58);
            this.emptySpaceItem18.Name = "emptySpaceItem18";
            this.emptySpaceItem18.Size = new System.Drawing.Size(1161, 10);
            this.emptySpaceItem18.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem_UsuarioCNPJ
            // 
            this.layoutControlItem_UsuarioCNPJ.Control = this.textEdit_UsuarioCNPJ;
            this.layoutControlItem_UsuarioCNPJ.Location = new System.Drawing.Point(0, 92);
            this.layoutControlItem_UsuarioCNPJ.Name = "layoutControlItem_UsuarioCNPJ";
            this.layoutControlItem_UsuarioCNPJ.Size = new System.Drawing.Size(575, 24);
            this.layoutControlItem_UsuarioCNPJ.Text = "CNPJ";
            this.layoutControlItem_UsuarioCNPJ.TextSize = new System.Drawing.Size(103, 13);
            // 
            // layoutControlItem_UsuarioSMS
            // 
            this.layoutControlItem_UsuarioSMS.Control = this.textEdit_UsuarioSMS;
            this.layoutControlItem_UsuarioSMS.CustomizationFormText = "CPF";
            this.layoutControlItem_UsuarioSMS.Location = new System.Drawing.Point(0, 140);
            this.layoutControlItem_UsuarioSMS.Name = "layoutControlItem_UsuarioSMS";
            this.layoutControlItem_UsuarioSMS.Size = new System.Drawing.Size(1161, 24);
            this.layoutControlItem_UsuarioSMS.Text = "SMS / WhatsApp";
            this.layoutControlItem_UsuarioSMS.TextSize = new System.Drawing.Size(103, 13);
            // 
            // layoutControlItem_UsuarioGenero
            // 
            this.layoutControlItem_UsuarioGenero.Control = this.textEdit_UsuarioGenero;
            this.layoutControlItem_UsuarioGenero.CustomizationFormText = "CPF";
            this.layoutControlItem_UsuarioGenero.Location = new System.Drawing.Point(0, 164);
            this.layoutControlItem_UsuarioGenero.Name = "layoutControlItem_UsuarioGenero";
            this.layoutControlItem_UsuarioGenero.Size = new System.Drawing.Size(1161, 24);
            this.layoutControlItem_UsuarioGenero.Text = "Genêro";
            this.layoutControlItem_UsuarioGenero.TextSize = new System.Drawing.Size(103, 13);
            // 
            // layoutControlItem_UsuarioTipo
            // 
            this.layoutControlItem_UsuarioTipo.Control = this.textEdit_UsuarioTipo;
            this.layoutControlItem_UsuarioTipo.CustomizationFormText = "CPF";
            this.layoutControlItem_UsuarioTipo.Location = new System.Drawing.Point(0, 116);
            this.layoutControlItem_UsuarioTipo.Name = "layoutControlItem_UsuarioTipo";
            this.layoutControlItem_UsuarioTipo.Size = new System.Drawing.Size(1161, 24);
            this.layoutControlItem_UsuarioTipo.Text = "Tipo";
            this.layoutControlItem_UsuarioTipo.TextSize = new System.Drawing.Size(103, 13);
            // 
            // layoutControlItem_UsuarioCPF
            // 
            this.layoutControlItem_UsuarioCPF.Control = this.textEdit_UsuarioCPF;
            this.layoutControlItem_UsuarioCPF.CustomizationFormText = "CPF";
            this.layoutControlItem_UsuarioCPF.Location = new System.Drawing.Point(575, 92);
            this.layoutControlItem_UsuarioCPF.Name = "layoutControlItem_UsuarioCPF";
            this.layoutControlItem_UsuarioCPF.Size = new System.Drawing.Size(586, 24);
            this.layoutControlItem_UsuarioCPF.Text = "CPF";
            this.layoutControlItem_UsuarioCPF.TextSize = new System.Drawing.Size(103, 13);
            // 
            // layoutControlItem_UsuarioUltNome
            // 
            this.layoutControlItem_UsuarioUltNome.Control = this.textEdit_UsuarioUltNome;
            this.layoutControlItem_UsuarioUltNome.CustomizationFormText = "CPF";
            this.layoutControlItem_UsuarioUltNome.Location = new System.Drawing.Point(0, 188);
            this.layoutControlItem_UsuarioUltNome.Name = "layoutControlItem_UsuarioUltNome";
            this.layoutControlItem_UsuarioUltNome.Size = new System.Drawing.Size(575, 24);
            this.layoutControlItem_UsuarioUltNome.Text = "Último Nome";
            this.layoutControlItem_UsuarioUltNome.TextSize = new System.Drawing.Size(103, 13);
            // 
            // layoutControlItem_UsuarioSenha3
            // 
            this.layoutControlItem_UsuarioSenha3.Control = this.textEdit_UsuarioPNome;
            this.layoutControlItem_UsuarioSenha3.CustomizationFormText = "CPF";
            this.layoutControlItem_UsuarioSenha3.Location = new System.Drawing.Point(575, 188);
            this.layoutControlItem_UsuarioSenha3.Name = "layoutControlItem_UsuarioSenha3";
            this.layoutControlItem_UsuarioSenha3.Size = new System.Drawing.Size(586, 24);
            this.layoutControlItem_UsuarioSenha3.Text = "Primeiro Nome";
            this.layoutControlItem_UsuarioSenha3.TextSize = new System.Drawing.Size(103, 13);
            // 
            // layoutControlItem_UsuarioSenha
            // 
            this.layoutControlItem_UsuarioSenha.Control = this.textEdit_UsuarioSenha;
            this.layoutControlItem_UsuarioSenha.CustomizationFormText = "CPF";
            this.layoutControlItem_UsuarioSenha.Location = new System.Drawing.Point(269, 34);
            this.layoutControlItem_UsuarioSenha.Name = "layoutControlItem_UsuarioSenha";
            this.layoutControlItem_UsuarioSenha.Size = new System.Drawing.Size(518, 24);
            this.layoutControlItem_UsuarioSenha.Text = "Senha";
            this.layoutControlItem_UsuarioSenha.TextSize = new System.Drawing.Size(103, 13);
            // 
            // layoutControlItem_UsuarioComentarios
            // 
            this.layoutControlItem_UsuarioComentarios.Control = this.textEdit_UsuarioComentarios;
            this.layoutControlItem_UsuarioComentarios.CustomizationFormText = "CPF";
            this.layoutControlItem_UsuarioComentarios.Location = new System.Drawing.Point(0, 212);
            this.layoutControlItem_UsuarioComentarios.Name = "layoutControlItem_UsuarioComentarios";
            this.layoutControlItem_UsuarioComentarios.Size = new System.Drawing.Size(1161, 44);
            this.layoutControlItem_UsuarioComentarios.Text = "Comentários";
            this.layoutControlItem_UsuarioComentarios.TextSize = new System.Drawing.Size(103, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 284);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(1161, 10);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem_TabPrecoDtUltAlteracao
            // 
            this.layoutControlItem_TabPrecoDtUltAlteracao.Control = this.dateEdit_UsuarioDtCriacao;
            this.layoutControlItem_TabPrecoDtUltAlteracao.CustomizationFormText = "Última Alteração";
            this.layoutControlItem_TabPrecoDtUltAlteracao.Location = new System.Drawing.Point(0, 256);
            this.layoutControlItem_TabPrecoDtUltAlteracao.Name = "layoutControlItem_TabPrecoDtUltAlteracao";
            this.layoutControlItem_TabPrecoDtUltAlteracao.Size = new System.Drawing.Size(575, 28);
            this.layoutControlItem_TabPrecoDtUltAlteracao.Text = "Data de Criação";
            this.layoutControlItem_TabPrecoDtUltAlteracao.TextSize = new System.Drawing.Size(103, 13);
            // 
            // layoutControlItem_TabPrecoExcluido
            // 
            this.layoutControlItem_TabPrecoExcluido.Control = this.textEdit_UsuarioExcluido;
            this.layoutControlItem_TabPrecoExcluido.CustomizationFormText = "Status";
            this.layoutControlItem_TabPrecoExcluido.Location = new System.Drawing.Point(575, 256);
            this.layoutControlItem_TabPrecoExcluido.Name = "layoutControlItem_TabPrecoExcluido";
            this.layoutControlItem_TabPrecoExcluido.Size = new System.Drawing.Size(586, 28);
            this.layoutControlItem_TabPrecoExcluido.Text = "Excluído";
            this.layoutControlItem_TabPrecoExcluido.TextSize = new System.Drawing.Size(103, 13);
            // 
            // layoutControlItem_UsuarioEmail
            // 
            this.layoutControlItem_UsuarioEmail.Control = this.textEdit_UsuarioEmail;
            this.layoutControlItem_UsuarioEmail.CustomizationFormText = "CPF";
            this.layoutControlItem_UsuarioEmail.Location = new System.Drawing.Point(269, 10);
            this.layoutControlItem_UsuarioEmail.Name = "layoutControlItem_UsuarioEmail";
            this.layoutControlItem_UsuarioEmail.Size = new System.Drawing.Size(518, 24);
            this.layoutControlItem_UsuarioEmail.Text = "E-mail";
            this.layoutControlItem_UsuarioEmail.TextSize = new System.Drawing.Size(103, 13);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 10);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(269, 48);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem17
            // 
            this.emptySpaceItem17.AllowHotTrack = false;
            this.emptySpaceItem17.Location = new System.Drawing.Point(787, 10);
            this.emptySpaceItem17.Name = "emptySpaceItem17";
            this.emptySpaceItem17.Size = new System.Drawing.Size(374, 48);
            this.emptySpaceItem17.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem_UsuarioDtNasc
            // 
            this.layoutControlItem_UsuarioDtNasc.Control = this.textEdit_UsuarioDtNasc;
            this.layoutControlItem_UsuarioDtNasc.CustomizationFormText = "CPF";
            this.layoutControlItem_UsuarioDtNasc.Location = new System.Drawing.Point(0, 68);
            this.layoutControlItem_UsuarioDtNasc.Name = "layoutControlItem_UsuarioDtNasc";
            this.layoutControlItem_UsuarioDtNasc.Size = new System.Drawing.Size(1161, 24);
            this.layoutControlItem_UsuarioDtNasc.Text = "Data de Nascimento";
            this.layoutControlItem_UsuarioDtNasc.TextSize = new System.Drawing.Size(103, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(10, 383);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // tabNavigationPage_DataGridUsuario
            // 
            this.tabNavigationPage_DataGridUsuario.Caption = "Usuário";
            this.tabNavigationPage_DataGridUsuario.Controls.Add(this.gridControl_Usuario);
            this.tabNavigationPage_DataGridUsuario.Image = ((System.Drawing.Image)(resources.GetObject("tabNavigationPage_DataGridUsuario.Image")));
            this.tabNavigationPage_DataGridUsuario.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage_DataGridUsuario.Name = "tabNavigationPage_DataGridUsuario";
            this.tabNavigationPage_DataGridUsuario.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage_DataGridUsuario.Size = new System.Drawing.Size(1246, 404);
            // 
            // gridControl_Usuario
            // 
            this.gridControl_Usuario.DataSource = this.usuarioBindingSource;
            this.gridControl_Usuario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl_Usuario.Location = new System.Drawing.Point(0, 0);
            this.gridControl_Usuario.MainView = this.gridView_Usuario;
            this.gridControl_Usuario.Name = "gridControl_Usuario";
            this.gridControl_Usuario.Size = new System.Drawing.Size(1246, 404);
            this.gridControl_Usuario.TabIndex = 0;
            this.gridControl_Usuario.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView_Usuario,
            this.gridView1});
            // 
            // gridView_Usuario
            // 
            this.gridView_Usuario.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colid,
            this.coltype,
            this.colcpf,
            this.colcnpj,
            this.colemail,
            this.colsms,
            this.colphone,
            this.colgender,
            this.colbirth,
            this.colpassword,
            this.colcomments,
            this.collast_name,
            this.colcellphone,
            this.colpermission,
            this.colfirst_name,
            this.colin_trash,
            this.colentity_groups_id,
            this.colnewsletters,
            this.colshipment_addresses_id,
            this.colallowed_discount,
            this.colcreated_at});
            this.gridView_Usuario.GridControl = this.gridControl_Usuario;
            this.gridView_Usuario.Name = "gridView_Usuario";
            this.gridView_Usuario.OptionsBehavior.Editable = false;
            this.gridView_Usuario.OptionsBehavior.ReadOnly = true;
            this.gridView_Usuario.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView_Usuario.OptionsSelection.MultiSelect = true;
            this.gridView_Usuario.OptionsSelection.UseIndicatorForSelection = false;
            this.gridView_Usuario.OptionsView.ShowGroupPanel = false;
            this.gridView_Usuario.OptionsView.ShowIndicator = false;
            this.gridView_Usuario.DoubleClick += new System.EventHandler(this.gridView_Clientes_DoubleClick);
            // 
            // colid
            // 
            this.colid.FieldName = "id";
            this.colid.Name = "colid";
            this.colid.Visible = true;
            this.colid.VisibleIndex = 0;
            // 
            // coltype
            // 
            this.coltype.FieldName = "type";
            this.coltype.Name = "coltype";
            this.coltype.Visible = true;
            this.coltype.VisibleIndex = 1;
            // 
            // colcpf
            // 
            this.colcpf.FieldName = "cpf";
            this.colcpf.Name = "colcpf";
            this.colcpf.Visible = true;
            this.colcpf.VisibleIndex = 2;
            // 
            // colcnpj
            // 
            this.colcnpj.FieldName = "cnpj";
            this.colcnpj.Name = "colcnpj";
            this.colcnpj.Visible = true;
            this.colcnpj.VisibleIndex = 3;
            // 
            // colemail
            // 
            this.colemail.FieldName = "email";
            this.colemail.Name = "colemail";
            this.colemail.Visible = true;
            this.colemail.VisibleIndex = 4;
            // 
            // colsms
            // 
            this.colsms.FieldName = "sms";
            this.colsms.Name = "colsms";
            this.colsms.Visible = true;
            this.colsms.VisibleIndex = 5;
            // 
            // colphone
            // 
            this.colphone.FieldName = "phone";
            this.colphone.Name = "colphone";
            this.colphone.Visible = true;
            this.colphone.VisibleIndex = 6;
            // 
            // colgender
            // 
            this.colgender.FieldName = "gender";
            this.colgender.Name = "colgender";
            this.colgender.Visible = true;
            this.colgender.VisibleIndex = 7;
            // 
            // colbirth
            // 
            this.colbirth.FieldName = "birth";
            this.colbirth.Name = "colbirth";
            this.colbirth.Visible = true;
            this.colbirth.VisibleIndex = 8;
            // 
            // colpassword
            // 
            this.colpassword.FieldName = "password";
            this.colpassword.Name = "colpassword";
            this.colpassword.Visible = true;
            this.colpassword.VisibleIndex = 9;
            // 
            // colcomments
            // 
            this.colcomments.FieldName = "comments";
            this.colcomments.Name = "colcomments";
            this.colcomments.Visible = true;
            this.colcomments.VisibleIndex = 10;
            // 
            // collast_name
            // 
            this.collast_name.FieldName = "last_name";
            this.collast_name.Name = "collast_name";
            this.collast_name.Visible = true;
            this.collast_name.VisibleIndex = 11;
            // 
            // colcellphone
            // 
            this.colcellphone.FieldName = "cellphone";
            this.colcellphone.Name = "colcellphone";
            this.colcellphone.Visible = true;
            this.colcellphone.VisibleIndex = 12;
            // 
            // colpermission
            // 
            this.colpermission.FieldName = "permission";
            this.colpermission.Name = "colpermission";
            this.colpermission.Visible = true;
            this.colpermission.VisibleIndex = 13;
            // 
            // colfirst_name
            // 
            this.colfirst_name.FieldName = "first_name";
            this.colfirst_name.Name = "colfirst_name";
            this.colfirst_name.Visible = true;
            this.colfirst_name.VisibleIndex = 14;
            // 
            // colin_trash
            // 
            this.colin_trash.FieldName = "in_trash";
            this.colin_trash.Name = "colin_trash";
            this.colin_trash.Visible = true;
            this.colin_trash.VisibleIndex = 15;
            // 
            // colentity_groups_id
            // 
            this.colentity_groups_id.FieldName = "entity_groups_id";
            this.colentity_groups_id.Name = "colentity_groups_id";
            this.colentity_groups_id.Visible = true;
            this.colentity_groups_id.VisibleIndex = 16;
            // 
            // colnewsletters
            // 
            this.colnewsletters.FieldName = "newsletters";
            this.colnewsletters.Name = "colnewsletters";
            this.colnewsletters.Visible = true;
            this.colnewsletters.VisibleIndex = 17;
            // 
            // colshipment_addresses_id
            // 
            this.colshipment_addresses_id.FieldName = "shipment_addresses_id";
            this.colshipment_addresses_id.Name = "colshipment_addresses_id";
            this.colshipment_addresses_id.Visible = true;
            this.colshipment_addresses_id.VisibleIndex = 18;
            // 
            // colallowed_discount
            // 
            this.colallowed_discount.FieldName = "allowed_discount";
            this.colallowed_discount.Name = "colallowed_discount";
            this.colallowed_discount.Visible = true;
            this.colallowed_discount.VisibleIndex = 19;
            // 
            // colcreated_at
            // 
            this.colcreated_at.FieldName = "created_at";
            this.colcreated_at.Name = "colcreated_at";
            this.colcreated_at.Visible = true;
            this.colcreated_at.VisibleIndex = 20;
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl_Usuario;
            this.gridView1.Name = "gridView1";
            // 
            // tabPane_ConfigUsuario
            // 
            this.tabPane_ConfigUsuario.Controls.Add(this.tabNavigationPage_DataGridUsuario);
            this.tabPane_ConfigUsuario.Controls.Add(this.tabNavigationPage_RegisterUsuario);
            this.tabPane_ConfigUsuario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPane_ConfigUsuario.Location = new System.Drawing.Point(0, 94);
            this.tabPane_ConfigUsuario.Name = "tabPane_ConfigUsuario";
            this.tabPane_ConfigUsuario.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.tabNavigationPage_DataGridUsuario,
            this.tabNavigationPage_RegisterUsuario});
            this.tabPane_ConfigUsuario.RegularSize = new System.Drawing.Size(1264, 468);
            this.tabPane_ConfigUsuario.SelectedPage = this.tabNavigationPage_RegisterUsuario;
            this.tabPane_ConfigUsuario.Size = new System.Drawing.Size(1264, 468);
            this.tabPane_ConfigUsuario.TabIndex = 8;
            this.tabPane_ConfigUsuario.Text = "Vendedores";
            // 
            // bar2
            // 
            this.bar2.BarName = "Tools_GravarCancelar";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 1;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Gravar),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Cancelar)});
            this.bar2.OptionsBar.AllowRename = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Tools_GravarCancelar";
            this.bar2.Visible = false;
            // 
            // barButtonItem_Gravar
            // 
            this.barButtonItem_Gravar.Caption = "Gravar";
            this.barButtonItem_Gravar.Id = 3;
            this.barButtonItem_Gravar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Gravar.ImageOptions.Image")));
            this.barButtonItem_Gravar.Name = "barButtonItem_Gravar";
            this.barButtonItem_Gravar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Gravar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Gravar_ItemClick_1);
            // 
            // barButtonItem_Cancelar
            // 
            this.barButtonItem_Cancelar.Caption = "Cancelar";
            this.barButtonItem_Cancelar.Id = 4;
            this.barButtonItem_Cancelar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Cancelar.ImageOptions.Image")));
            this.barButtonItem_Cancelar.Name = "barButtonItem_Cancelar";
            this.barButtonItem_Cancelar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Cancelar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Cancelar_ItemClick_1);
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Add),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Edit),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Del)});
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Tools";
            // 
            // barButtonItem_Add
            // 
            this.barButtonItem_Add.Caption = "Adicionar";
            this.barButtonItem_Add.Id = 0;
            this.barButtonItem_Add.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Add.ImageOptions.Image")));
            this.barButtonItem_Add.Name = "barButtonItem_Add";
            this.barButtonItem_Add.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Add.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Add_ItemClick_1);
            // 
            // barButtonItem_Edit
            // 
            this.barButtonItem_Edit.Caption = "Editar";
            this.barButtonItem_Edit.Id = 1;
            this.barButtonItem_Edit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Edit.ImageOptions.Image")));
            this.barButtonItem_Edit.Name = "barButtonItem_Edit";
            this.barButtonItem_Edit.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Edit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Edit_ItemClick_1);
            // 
            // barButtonItem_Del
            // 
            this.barButtonItem_Del.Caption = "Excluir";
            this.barButtonItem_Del.Id = 2;
            this.barButtonItem_Del.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Del.ImageOptions.Image")));
            this.barButtonItem_Del.Name = "barButtonItem_Del";
            this.barButtonItem_Del.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Del.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Del_ItemClick_1);
            // 
            // barManager_ContasMP
            // 
            this.barManager_ContasMP.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar2});
            this.barManager_ContasMP.DockControls.Add(this.barDockControlTop);
            this.barManager_ContasMP.DockControls.Add(this.barDockControlBottom);
            this.barManager_ContasMP.DockControls.Add(this.barDockControlLeft);
            this.barManager_ContasMP.DockControls.Add(this.barDockControlRight);
            this.barManager_ContasMP.Form = this;
            this.barManager_ContasMP.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem_Add,
            this.barButtonItem_Edit,
            this.barButtonItem_Del,
            this.barButtonItem_Gravar,
            this.barButtonItem_Cancelar});
            this.barManager_ContasMP.MaxItemId = 5;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager_ContasMP;
            this.barDockControlTop.Size = new System.Drawing.Size(1264, 94);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 562);
            this.barDockControlBottom.Manager = this.barManager_ContasMP;
            this.barDockControlBottom.Size = new System.Drawing.Size(1264, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 94);
            this.barDockControlLeft.Manager = this.barManager_ContasMP;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 468);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1264, 94);
            this.barDockControlRight.Manager = this.barManager_ContasMP;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 468);
            // 
            // FormUsuarioCad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1264, 562);
            this.Controls.Add(this.tabPane_ConfigUsuario);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormUsuarioCad";
            this.ShowMdiChildCaptionInParentTitle = true;
            this.Text = "Usuário";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormTabelaPreco_Load);
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem_RegisterClientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.usuarioBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meusPedidosDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vendedorBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).EndInit();
            this.tabPane1.ResumeLayout(false);
            this.tabNavigationPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toggleSwitch1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CLITelefone1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CatProd)).EndInit();
            this.tabNavigationPage_RegisterUsuario.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl_RegisterUsuario)).EndInit();
            this.panelControl_RegisterUsuario.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_Usuario)).EndInit();
            this.layoutControl_Usuario.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_UsuarioCNPJ.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_UsuarioCPF.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_UsuarioSMS.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_UsuarioGenero.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_UsuarioSenha.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_UsuarioTipo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_UsuarioEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_UsuarioUltNome.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_UsuarioPNome.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_UsuarioComentarios.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_UsuarioDtCriacao.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_UsuarioDtCriacao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_UsuarioExcluido.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_UsuarioDtNasc.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_UsuarioDtNasc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_RegisterUsuario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem_RegisterUsuario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_Usuario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_UsuarioCNPJ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_UsuarioSMS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_UsuarioGenero)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_UsuarioTipo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_UsuarioCPF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_UsuarioUltNome)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_UsuarioSenha3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_UsuarioSenha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_UsuarioComentarios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_TabPrecoDtUltAlteracao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_TabPrecoExcluido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_UsuarioEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_UsuarioDtNasc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            this.tabNavigationPage_DataGridUsuario.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_Usuario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_Usuario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane_ConfigUsuario)).EndInit();
            this.tabPane_ConfigUsuario.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.barManager_ContasMP)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Cliente;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CondicaoPagamentoCliente;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CategoriasCliente;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TabelaPreco;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Usuarios;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Produto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_ImagensProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TabelaProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TabelaPrecoProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CategoriaProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Transportadora;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CondicaoPagamento;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_FormaPagamento;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Pedido;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CamposExtraPedido;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_FaturamentoPedido;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TituloVencido;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager_User;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem_RegisterClientes;
        private DevExpress.XtraBars.Navigation.TabPane tabPane1;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage1;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage2;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraEditors.TextEdit textEdit3;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        private DevExpress.XtraEditors.TextEdit textEdit6;
        private DevExpress.XtraEditors.TextEdit textEdit7;
        private DevExpress.XtraEditors.TextEdit textEdit8;
        private DevExpress.XtraEditors.TextEdit textEdit9;
        private DevExpress.XtraEditors.TextEdit textEdit10;
        private DevExpress.XtraEditors.TextEdit textEdit11;
        private DevExpress.XtraEditors.TextEdit textEdit12;
        private DevExpress.XtraEditors.TextEdit textEdit13;
        private DevExpress.XtraEditors.TextEdit textEdit14;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
        private DevExpress.XtraEditors.ToggleSwitch toggleSwitch1;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_CLITelefone1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraGrid.Columns.GridColumn colid1;
        private DevExpress.XtraGrid.Columns.GridColumn colrazao_social;
        private DevExpress.XtraGrid.Columns.GridColumn colnome_fantasia;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_CatProd;
        private System.Windows.Forms.BindingSource vendedorBindingSource;
        private MeusPedidosDataSet meusPedidosDataSet1;
        private System.Windows.Forms.BindingSource usuarioBindingSource;
        private MeusPedidosDataSetTableAdapters.UsuarioTableAdapter usuarioTableAdapter;
        private DevExpress.XtraBars.Navigation.TabPane tabPane_ConfigUsuario;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage_DataGridUsuario;
        private DevExpress.XtraGrid.GridControl gridControl_Usuario;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView_Usuario;
        private DevExpress.XtraGrid.Columns.GridColumn colid;
        private DevExpress.XtraGrid.Columns.GridColumn coltype;
        private DevExpress.XtraGrid.Columns.GridColumn colcpf;
        private DevExpress.XtraGrid.Columns.GridColumn colcnpj;
        private DevExpress.XtraGrid.Columns.GridColumn colemail;
        private DevExpress.XtraGrid.Columns.GridColumn colsms;
        private DevExpress.XtraGrid.Columns.GridColumn colphone;
        private DevExpress.XtraGrid.Columns.GridColumn colgender;
        private DevExpress.XtraGrid.Columns.GridColumn colbirth;
        private DevExpress.XtraGrid.Columns.GridColumn colpassword;
        private DevExpress.XtraGrid.Columns.GridColumn colcomments;
        private DevExpress.XtraGrid.Columns.GridColumn collast_name;
        private DevExpress.XtraGrid.Columns.GridColumn colcellphone;
        private DevExpress.XtraGrid.Columns.GridColumn colpermission;
        private DevExpress.XtraGrid.Columns.GridColumn colfirst_name;
        private DevExpress.XtraGrid.Columns.GridColumn colin_trash;
        private DevExpress.XtraGrid.Columns.GridColumn colentity_groups_id;
        private DevExpress.XtraGrid.Columns.GridColumn colnewsletters;
        private DevExpress.XtraGrid.Columns.GridColumn colshipment_addresses_id;
        private DevExpress.XtraGrid.Columns.GridColumn colallowed_discount;
        private DevExpress.XtraGrid.Columns.GridColumn colcreated_at;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage_RegisterUsuario;
        private DevExpress.XtraEditors.PanelControl panelControl_RegisterUsuario;
        private DevExpress.XtraLayout.LayoutControl layoutControl_Usuario;
        private DevExpress.XtraEditors.TextEdit textEdit_UsuarioCNPJ;
        private DevExpress.XtraEditors.TextEdit textEdit_UsuarioCPF;
        private DevExpress.XtraEditors.TextEdit textEdit_UsuarioSMS;
        private DevExpress.XtraEditors.TextEdit textEdit_UsuarioGenero;
        private DevExpress.XtraEditors.TextEdit textEdit_UsuarioSenha;
        private DevExpress.XtraEditors.TextEdit textEdit_UsuarioTipo;
        private DevExpress.XtraEditors.TextEdit textEdit_UsuarioEmail;
        private DevExpress.XtraEditors.TextEdit textEdit_UsuarioUltNome;
        private DevExpress.XtraEditors.TextEdit textEdit_UsuarioPNome;
        private DevExpress.XtraEditors.MemoEdit textEdit_UsuarioComentarios;
        private DevExpress.XtraEditors.DateEdit dateEdit_UsuarioDtCriacao;
        private DevExpress.XtraEditors.ToggleSwitch textEdit_UsuarioExcluido;
        private DevExpress.XtraEditors.DateEdit textEdit_UsuarioDtNasc;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_RegisterUsuario;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem_RegisterUsuario;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_Usuario;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_UsuarioCNPJ;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_UsuarioSMS;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_UsuarioGenero;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_UsuarioTipo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_UsuarioCPF;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_UsuarioUltNome;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_UsuarioSenha3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_UsuarioSenha;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_UsuarioDtNasc;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_UsuarioComentarios;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_TabPrecoDtUltAlteracao;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_TabPrecoExcluido;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_UsuarioEmail;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem17;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarManager barManager_ContasMP;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Add;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Edit;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Del;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Gravar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Cancelar;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
    }
}


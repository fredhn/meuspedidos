﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Control;
using Core;
using DevExpress.XtraEditors;
using System.Data.Entity;
using Control.Validations;
using DevExpress.XtraGrid.Views.Grid;
using System.Globalization;
using DevExpress.XtraEditors.Controls;

namespace View_MeusPedidos
{
    public partial class FormVendedorRegraLiberacao : DevExpress.XtraEditors.XtraForm
    {
        RegraLiberacao_Service s_rl;
        RegraLiberacao rl;
        Cliente_Service s_cli;
        Vendedor_Service s_vend;

        int _id;
        bool add_status = false;

        public FormVendedorRegraLiberacao()
        {
            InitializeComponent();
            s_rl = new RegraLiberacao_Service();
            s_cli = new Cliente_Service();
            s_vend = new Vendedor_Service();

            bar_Acoes2.Visible = false;

            tabNavigationPage_RegisterRL.PageVisible = false;
            tabPane_ConfigRL.SelectedPage = tabNavigationPage_DataGridRL;

            gridControl_RL.DataSource = s_rl.GetAll_RegraLiberacao();

            BindingSource srcCli = new BindingSource();
            srcCli.DataSource = s_cli.GetAll_Clientes();
            this.comboBoxEdit_RLCli.Properties.DataSource = srcCli.List;
            this.comboBoxEdit_RLCli.Properties.Columns.Add(new LookUpColumnInfo("id", "Id"));
            this.comboBoxEdit_RLCli.Properties.Columns.Add(new LookUpColumnInfo("razao_social", "Nome"));
            this.comboBoxEdit_RLCli.Properties.DisplayMember = "razao_social";
            this.comboBoxEdit_RLCli.Properties.ValueMember = "id";

            BindingSource srcVend = new BindingSource();
            srcVend.DataSource = s_vend.GetAll_Vendedores();
            this.comboBoxEdit_RLUser.Properties.DataSource = srcVend.List;
            this.comboBoxEdit_RLUser.Properties.Columns.Add(new LookUpColumnInfo("id", "Id"));
            this.comboBoxEdit_RLUser.Properties.Columns.Add(new LookUpColumnInfo("nome", "Nome"));
            this.comboBoxEdit_RLUser.Properties.DisplayMember = "nome";
            this.comboBoxEdit_RLUser.Properties.ValueMember = "id";
        }

        #region ToolBar1 Events - Adicionar/Editar/Deletar

        private void barButtonItem_Add_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            add_status = true;

            //UI Behavior

            FormContols_FW.fieldsClean(layoutControl_RL);

            bar_Acoes.Visible = false;
            bar_Acoes2.Visible = true;

            tabNavigationPage_RegisterRL.PageVisible = true;
            tabNavigationPage_DataGridRL.PageVisible = false;
            tabPane_ConfigRL.SelectedPage = tabNavigationPage_RegisterRL;

            //Define TextEdit Fields as ReadOnly = false
            FormContols_FW.fieldsReadOnly(layoutControl_RL, false);
        }

        private void barButtonItem_Edit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            add_status = false;

            var row = gridView_RL.GetRow(gridView_RL.GetFocusedDataSourceRowIndex());
            rl = (RegraLiberacao)row;

            if(rl != null)
            {
                if (XtraMessageBox.Show("Tem certeza que deseja alterar o registro (ID: " + rl.id + ")?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    //Retrieve entity ID
                    _id = rl.id;

                    //Fill TextEdit Fields
                    comboBoxEdit_RLUser.EditValue = rl.usuario_id.ToString();
                    comboBoxEdit_RLCli.EditValue = rl.cliente_id.ToString();
                    textEdit_RLLiberado.EditValue = rl.liberado;

                    //UI Behavior
                    bar_Acoes.Visible = false;
                    bar_Acoes2.Visible = true;

                    tabNavigationPage_RegisterRL.PageVisible = true;
                    tabNavigationPage_DataGridRL.PageVisible = false;
                    tabPane_ConfigRL.SelectedPage = tabNavigationPage_RegisterRL;

                    //Define TextEdit Fields as ReadOnly = false
                    FormContols_FW.fieldsReadOnly(layoutControl_RL, false);
                }
            }
            
        }

        private void barButtonItem_Del_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var row = gridView_RL.GetRow(gridView_RL.GetFocusedDataSourceRowIndex());
            rl = (RegraLiberacao)row;
            
            if(rl != null)
            {
                if (XtraMessageBox.Show("Tem certeza que deseja excluir o registro (ID: " + rl.id + ")?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    //rl.excluido = true;
                    s_rl.Update_RegraLiberacao(rl);

                    gridControl_RL.DataSource = s_rl.GetAll_RegraLiberacao();
                }
            }
            
        }
        #endregion

        #region ToolBar2 Events - Cancelar/Gravar
        private void barButtonItem_Gravar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (add_status == true &&
                textEdit_RLLiberado.Text != "" &&
                comboBoxEdit_RLUser.Text != "" &&
                comboBoxEdit_RLCli.Text != "")
            {
                rl = new RegraLiberacao();
                rl.cliente_id = int.Parse(comboBoxEdit_RLCli.EditValue.ToString());
                rl.usuario_id = int.Parse(comboBoxEdit_RLUser.EditValue.ToString());
                rl.liberado = textEdit_RLLiberado.IsOn;
                rl.mptran = "I";

                //Validação Nome
                var aux = s_rl.GetByUserCli_RegraLiberacao(rl.usuario_id, rl.cliente_id);

                if (aux == null)
                {
                    splashScreenManager_Transp.ShowWaitForm();

                    //Add new ProdCat
                    s_rl.AddNew_RegraLiberacao(rl);

                    splashScreenManager_Transp.CloseWaitForm();

                    XtraMessageBox.Show("Registro salvo!", "Informação");

                    FormContols_FW.fieldsClean(layoutControl_RL);

                    gridControl_RL.DataSource = s_rl.GetAll_RegraLiberacao();

                    bar_Acoes.Visible = true;
                    bar_Acoes2.Visible = false;
                    tabNavigationPage_RegisterRL.PageVisible = false;
                    tabNavigationPage_DataGridRL.PageVisible = true;
                    tabPane_ConfigRL.SelectedPage = tabNavigationPage_DataGridRL;
                }
                else
                {
                    if(splashScreenManager_Transp.IsSplashFormVisible)
                    {
                        splashScreenManager_Transp.CloseWaitForm();
                    }

                    XtraMessageBox.Show("Já existe um relacionamento entre esta tabela de preço e produto!", "Atenção");
                }

            }
            else if (add_status == false &&
                textEdit_RLLiberado.Text != "" &&
                comboBoxEdit_RLUser.Text != "" &&
                comboBoxEdit_RLCli.Text != "")
            {
                splashScreenManager_Transp.ShowWaitForm();

                rl = new RegraLiberacao();
                rl.id = _id;

                rl = s_rl.GetBySpecification_RegraLiberacao(rl.id);

                rl.cliente_id = int.Parse(comboBoxEdit_RLCli.EditValue.ToString());
                rl.usuario_id = int.Parse(comboBoxEdit_RLUser.EditValue.ToString());
                rl.liberado = textEdit_RLLiberado.IsOn;
                rl.mptran = "A";

                s_rl.Update_RegraLiberacao(rl);

                splashScreenManager_Transp.CloseWaitForm();

                XtraMessageBox.Show("Alteração realizada com sucesso!", "Informação");

                FormContols_FW.fieldsClean(layoutControl_RL);

                gridControl_RL.DataSource = s_rl.GetAll_RegraLiberacao();

                bar_Acoes.Visible = true;
                bar_Acoes2.Visible = false;
                tabNavigationPage_RegisterRL.PageVisible = false;
                tabNavigationPage_DataGridRL.PageVisible = true;
                tabPane_ConfigRL.SelectedPage = tabNavigationPage_DataGridRL;
            }
            else
            {
                XtraMessageBox.Show("Algum campo obrigatório precisa ser preenchido!", "Informação");
            }
        }

        private void barButtonItem_Cancelar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (XtraMessageBox.Show("Tem certeza que deseja sair da tela?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                barButtonItem_Gravar.Enabled = true;

                bar_Acoes.Visible = true;
                bar_Acoes2.Visible = false;
                tabNavigationPage_RegisterRL.PageVisible = false;
                tabNavigationPage_DataGridRL.PageVisible = true;
                tabPane_ConfigRL.SelectedPage = tabNavigationPage_DataGridRL;

                FormContols_FW.fieldsClean(layoutControl_RL);
            }
        }
        #endregion

        #region Grid ProdutoTabelaPreço Events
        private void gridView_Clientes_DoubleClick(object sender, EventArgs e)
        {
            barButtonItem_Gravar.Enabled = false;

            //Get (object) row values
            var row = gridView_RL.GetRow(gridView_RL.GetFocusedDataSourceRowIndex());
            rl = (RegraLiberacao)row;
            
            //Fill TextEdit Fields
            if(rl != null)
            {
                //Fill TextEdit Fields
                comboBoxEdit_RLUser.EditValue = rl.usuario_id.ToString();
                comboBoxEdit_RLCli.EditValue = rl.cliente_id.ToString();
                textEdit_RLLiberado.EditValue = rl.liberado;
            }
            

            //Define TextEdit Fields as ReadOnly = true
            FormContols_FW.fieldsReadOnly(layoutControl_RL, true);

            //UI Behavior
            bar_Acoes.Visible = false;
            bar_Acoes2.Visible = true;

            tabNavigationPage_RegisterRL.PageVisible = true;
            tabNavigationPage_DataGridRL.PageVisible = false;
            tabPane_ConfigRL.SelectedPage = tabNavigationPage_RegisterRL;
        }
        #endregion

        private void FormTabelaPreco_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'meusPedidosDataSet.RegraLiberacao' table. You can move, or remove it, as needed.
            this.regraLiberacaoTableAdapter.Fill(this.meusPedidosDataSet.RegraLiberacao);

        }

        private void comboBoxEdit_RLUser_EditValueChanged(object sender, EventArgs e)
        {

        }
    }
}

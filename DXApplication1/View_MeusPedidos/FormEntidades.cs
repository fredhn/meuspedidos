﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Control;
using Core;
using DevExpress.XtraEditors;
using System.Data.Entity;
using Control.Validations;
using DevExpress.XtraGrid.Views.Grid;
using System.Globalization;
using DevExpress.XtraBars;

namespace View_MeusPedidos
{
    public partial class FormEntidades : DevExpress.XtraEditors.XtraForm
    {
        EntidadeSaida_Service s_es;
        EntidadeEntrada_Service s_ee;

        List<Entidade_Saida> list_es;

        PopupMenu menu;

        public FormEntidades()
        {
            InitializeComponent();
            s_es = new EntidadeSaida_Service();
            s_ee = new EntidadeEntrada_Service();
            
            tabPane_ConfigEntidades.SelectedPage = tabNavigationPage_DataGridEntSaida;

            gridControl_EntSaida.DataSource = s_es.GetAll_EntidadeSaida();
            gridControl_EntEntrada.DataSource = s_ee.GetAll_EntidadeEntrada();
        }

        private void FormEntidades_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'meusPedidosDataSet1.Entidade_Entrada' table. You can move, or remove it, as needed.
            this.entidade_EntradaTableAdapter.Fill(this.meusPedidosDataSet1.Entidade_Entrada);
            // TODO: This line of code loads data into the 'meusPedidosDataSet1.Entidade_Saida' table. You can move, or remove it, as needed.
            this.entidade_SaidaTableAdapter.Fill(this.meusPedidosDataSet1.Entidade_Saida);

        }

        private void barLargeButtonItem_Ressincronizar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {   
            if (tabPane_ConfigEntidades.SelectedPage.Name.Equals("tabNavigationPage_DataGridEntSaida"))
            {
                if (XtraMessageBox.Show("Tem certeza que deseja ressincronizar todas as Entidades de Saída já integradas?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    splashScreenManager_Entidades.ShowWaitForm();

                    list_es = new List<Entidade_Saida>();
                    list_es = s_es.GetAllByStatusRessync_EntidadeSaida("I");

                    foreach (Entidade_Saida es in list_es)
                    {
                        es.json = RessyncController.Execute(es);
                        es.mptran = "A";
                        es.status = "A";
                        es.codret = "";
                        s_es.AddNew_EntidadeSaida(es);
                    }

                    if (splashScreenManager_Entidades.IsSplashFormVisible)
                    {
                        splashScreenManager_Entidades.CloseWaitForm();
                    }
                }
            }
            else
            {
                XtraMessageBox.Show("Selecione a página de navegação das Entidades de Saída para ressincronizar!", "Atenção");
            }
        }

        private void gridView_EntSaida_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                if (barManager_Entidades == null)
                {
                    barManager_Entidades = new BarManager();
                    barManager_Entidades.Form = this;
                }
                if (menu == null)
                    menu = new PopupMenu(barManager_Entidades);
                // Add item and show
                menu.ItemLinks.Clear();
                BarButtonItem item_atualizar = new BarButtonItem(barManager_Entidades, "Atualizar");
                item_atualizar.ItemClick += new ItemClickEventHandler(gridViewEntSaida_RightClick);
                menu.AddItems(new BarItem[] { item_atualizar });
                menu.ShowPopup(Cursor.Position);
            }
        }

        private void gridView_EntEntrada_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                if (barManager_Entidades == null)
                {
                    barManager_Entidades = new BarManager();
                    barManager_Entidades.Form = this;
                }
                if (menu == null)
                    menu = new PopupMenu(barManager_Entidades);
                // Add item and show
                menu.ItemLinks.Clear();
                BarButtonItem item_atualizar = new BarButtonItem(barManager_Entidades, "Atualizar");
                item_atualizar.ItemClick += new ItemClickEventHandler(gridViewEntEntrada_RightClick);
                menu.AddItems(new BarItem[] { item_atualizar });
                menu.ShowPopup(Cursor.Position);
            }
        }

        void gridViewEntSaida_RightClick(object sender, EventArgs e)
        {
            //MessageBox.Show("Click");
            splashScreenManager_Entidades.ShowWaitForm();
            gridControl_EntSaida.DataSource = s_es.GetAll_EntidadeSaida();

            if (splashScreenManager_Entidades.IsSplashFormVisible)
            {
                splashScreenManager_Entidades.CloseWaitForm();
            }
        }
        void gridViewEntEntrada_RightClick(object sender, EventArgs e)
        {
            //MessageBox.Show("Click");
            splashScreenManager_Entidades.ShowWaitForm();
            gridControl_EntEntrada.DataSource = s_ee.GetAll_EntidadeEntrada();

            if (splashScreenManager_Entidades.IsSplashFormVisible)
            {
                splashScreenManager_Entidades.CloseWaitForm();
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Control;
using Core;
using DevExpress.XtraEditors;
using System.Data.Entity;
using Control.Validations;
using DevExpress.XtraGrid.Views.Grid;
using System.Globalization;

namespace View_MeusPedidos
{
    public partial class FormTabelaPreco : DevExpress.XtraEditors.XtraForm
    {
        TabelaPreco_Service s_tp;
        TabelaPreco tp;

        int _id;
        bool add_status = false;

        public FormTabelaPreco()
        {
            InitializeComponent();
            s_tp = new TabelaPreco_Service();

            //Parametros Aba Contatos (Carregando ComboBox)
            comboBoxEdit_TabPrecoTipo.Properties.DataSource = Enum.GetValues(typeof(Core.TabelaPreco_Type));
            //comboBoxEdit_TabPrecoTipo.Properties.Items.AddRange(typeof(Core.TabelaPreco_Type).GetEnumValues());

            bar_Acoes2.Visible = false;

            tabNavigationPage_RegisterTabPreco.PageVisible = false;
            tabPane_ConfigTabPreco.SelectedPage = tabNavigationPage_DataGridTabPreco;

            gridControl_TabPreco.DataSource = s_tp.GetAll_TabelaPreco();
        }

        #region ToolBar1 Events - Adicionar/Editar/Deletar

        private void barButtonItem_Add_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            add_status = true;

            //UI Behavior

            FormContols_FW.fieldsClean(layoutControl_TabPreco);

            bar_Acoes.Visible = false;
            bar_Acoes2.Visible = true;

            tabNavigationPage_RegisterTabPreco.PageVisible = true;
            tabNavigationPage_DataGridTabPreco.PageVisible = false;
            tabPane_ConfigTabPreco.SelectedPage = tabNavigationPage_RegisterTabPreco;

            //Define TextEdit Fields as ReadOnly = false
            FormContols_FW.fieldsReadOnly(layoutControl_TabPreco, false);
        }

        private void barButtonItem_Edit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            add_status = false;

            var row = gridView_TabPreco.GetRow(gridView_TabPreco.GetFocusedDataSourceRowIndex());
            tp = (TabelaPreco)row;

            if(tp != null)
            {
                if (XtraMessageBox.Show("Tem certeza que deseja alterar o registro (ID: " + tp.id + ")?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    //Retrieve entity ID
                    _id = tp.id;

                    //Fill TextEdit Fields
                    textEdit_TabPrecoNome.Text = tp.nome;
                    textEdit_TabPrecoAcrescimo.Text = tp.acrescimo.ToString();
                    textEdit_TabPrecoDesconto.Text = tp.desconto.ToString();
                    comboBoxEdit_TabPrecoTipo.Text = tp.tipo;
                    textEdit_TabPrecoExcluido.EditValue = tp.excluido;
                    dateEdit_TabPrecoUltAlteracao.DateTime = tp.ultima_alteracao;

                    //UI Behavior
                    bar_Acoes.Visible = false;
                    bar_Acoes2.Visible = true;

                    tabNavigationPage_RegisterTabPreco.PageVisible = true;
                    tabNavigationPage_DataGridTabPreco.PageVisible = false;
                    tabPane_ConfigTabPreco.SelectedPage = tabNavigationPage_RegisterTabPreco;

                    //Define TextEdit Fields as ReadOnly = false
                    FormContols_FW.fieldsReadOnly(layoutControl_TabPreco, false);
                }
            }
            
        }

        private void barButtonItem_Del_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var row = gridView_TabPreco.GetRow(gridView_TabPreco.GetFocusedDataSourceRowIndex());
            tp = (TabelaPreco)row;
            
            if(tp != null)
            {
                if (XtraMessageBox.Show("Tem certeza que deseja excluir o registro (ID: " + tp.id + ")?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    tp.excluido = true;
                    s_tp.Update_TabelaPreco(tp);

                    gridControl_TabPreco.DataSource = s_tp.GetAll_TabelaPreco();
                }
            }
            
        }
        #endregion

        #region ToolBar2 Events - Cancelar/Gravar
        private void barButtonItem_Gravar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (add_status == true &&
                textEdit_TabPrecoNome.Text != "")
            {
                tp = new TabelaPreco();
                tp.nome = textEdit_TabPrecoNome.Text;
                tp.acrescimo = float.Parse(textEdit_TabPrecoAcrescimo.EditValue.ToString(), CultureInfo.InvariantCulture.NumberFormat);
                tp.desconto = float.Parse(textEdit_TabPrecoDesconto.EditValue.ToString(), CultureInfo.InvariantCulture.NumberFormat);
                tp.tipo = comboBoxEdit_TabPrecoTipo.Text;
                tp.excluido = textEdit_TabPrecoExcluido.IsOn;
                tp.ultima_alteracao = DateTime.Now;
                tp.mptran = "I";

                //Validação Nome
                var aux = s_tp.GetByNome_TabelaPreco(tp.nome);

                if (aux == null)
                {
                    splashScreenManager_Transp.ShowWaitForm();

                    //Add new ProdCat
                    s_tp.AddNew_TabelaPreco(tp);

                    splashScreenManager_Transp.CloseWaitForm();

                    XtraMessageBox.Show("Registro salvo!", "Informação");

                    FormContols_FW.fieldsClean(layoutControl_TabPreco);

                    gridControl_TabPreco.DataSource = s_tp.GetAll_TabelaPreco();

                    bar_Acoes.Visible = true;
                    bar_Acoes2.Visible = false;
                    tabNavigationPage_RegisterTabPreco.PageVisible = false;
                    tabNavigationPage_DataGridTabPreco.PageVisible = true;
                    tabPane_ConfigTabPreco.SelectedPage = tabNavigationPage_DataGridTabPreco;
                }
                else
                {
                    splashScreenManager_Transp.CloseWaitForm();

                    XtraMessageBox.Show("Já existe uma transportadora com este nome cadastrado!", "Atenção");
                }

            }
            else if (add_status == false &&
                textEdit_TabPrecoNome.Text != "")
            {
                splashScreenManager_Transp.ShowWaitForm();

                tp = new TabelaPreco();
                tp.id = _id;

                tp = s_tp.GetBySpecification_TabelaPreco(tp.id);

                tp.nome = textEdit_TabPrecoNome.Text;
                tp.acrescimo = float.Parse(textEdit_TabPrecoAcrescimo.EditValue.ToString(), CultureInfo.InvariantCulture.NumberFormat);
                tp.desconto = float.Parse(textEdit_TabPrecoDesconto.EditValue.ToString(), CultureInfo.InvariantCulture.NumberFormat);
                tp.tipo = comboBoxEdit_TabPrecoTipo.Text;
                tp.excluido = textEdit_TabPrecoExcluido.IsOn;
                tp.ultima_alteracao = DateTime.Now;
                tp.mptran = "A";

                s_tp.Update_TabelaPreco(tp);

                splashScreenManager_Transp.CloseWaitForm();

                XtraMessageBox.Show("Alteração realizada com sucesso!", "Informação");

                FormContols_FW.fieldsClean(layoutControl_TabPreco);

                gridControl_TabPreco.DataSource = s_tp.GetAll_TabelaPreco();

                bar_Acoes.Visible = true;
                bar_Acoes2.Visible = false;
                tabNavigationPage_RegisterTabPreco.PageVisible = false;
                tabNavigationPage_DataGridTabPreco.PageVisible = true;
                tabPane_ConfigTabPreco.SelectedPage = tabNavigationPage_DataGridTabPreco;
            }
            else
            {
                XtraMessageBox.Show("Algum campo obrigatório precisa ser preenchido!", "Informação");
            }
        }

        private void barButtonItem_Cancelar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (XtraMessageBox.Show("Tem certeza que deseja sair da tela?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                barButtonItem_Gravar.Enabled = true;

                bar_Acoes.Visible = true;
                bar_Acoes2.Visible = false;
                tabNavigationPage_RegisterTabPreco.PageVisible = false;
                tabNavigationPage_DataGridTabPreco.PageVisible = true;
                tabPane_ConfigTabPreco.SelectedPage = tabNavigationPage_DataGridTabPreco;

                FormContols_FW.fieldsClean(layoutControl_TabPreco);
            }
        }
        #endregion

        #region Grid Transportadora Events
        private void gridView_Clientes_DoubleClick(object sender, EventArgs e)
        {
            barButtonItem_Gravar.Enabled = false;

            //Get (object) row values
            var row = gridView_TabPreco.GetRow(gridView_TabPreco.GetFocusedDataSourceRowIndex());
            tp = (TabelaPreco)row;

            //Fill TextEdit Fields
            textEdit_TabPrecoNome.Text = tp.nome;
            textEdit_TabPrecoAcrescimo.Text = tp.acrescimo.ToString();
            textEdit_TabPrecoDesconto.Text = tp.desconto.ToString();
            comboBoxEdit_TabPrecoTipo.Text = tp.tipo;
            textEdit_TabPrecoExcluido.EditValue = tp.excluido;
            dateEdit_TabPrecoUltAlteracao.DateTime = tp.ultima_alteracao;

            //Define TextEdit Fields as ReadOnly = true
            FormContols_FW.fieldsReadOnly(layoutControl_TabPreco, true);

            //UI Behavior
            bar_Acoes.Visible = false;
            bar_Acoes2.Visible = true;

            tabNavigationPage_RegisterTabPreco.PageVisible = true;
            tabNavigationPage_DataGridTabPreco.PageVisible = false;
            tabPane_ConfigTabPreco.SelectedPage = tabNavigationPage_RegisterTabPreco;
        }
        #endregion

        private void FormTabelaPreco_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'meusPedidosDataSet.TabelaPreco' table. You can move, or remove it, as needed.
            this.tabelaPrecoTableAdapter.Fill(this.meusPedidosDataSet.TabelaPreco);

        }
    }
}

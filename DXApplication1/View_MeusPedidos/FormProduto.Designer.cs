﻿namespace View_MeusPedidos
{
    partial class FormProduto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormProduto));
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement10 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement11 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement12 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement13 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement14 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement15 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement16 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement17 = new DevExpress.XtraEditors.TileItemElement();
            this.textEdit_ProdGradeCores = new DevExpress.XtraEditors.TextEdit();
            this.barManager_ContasMP = new DevExpress.XtraBars.BarManager(this.components);
            this.bar_Acoes = new DevExpress.XtraBars.Bar();
            this.barButtonItem_Add = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem_Edit = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem_Del = new DevExpress.XtraBars.BarButtonItem();
            this.bar_Acoes2 = new DevExpress.XtraBars.Bar();
            this.barButtonItem_Gravar = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem_Cancelar = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.layoutControl_Prod = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit_ProdNome = new DevExpress.XtraEditors.TextEdit();
            this.dateEdit_ProdUltAlteracao = new DevExpress.XtraEditors.DateEdit();
            this.textEdit_ProdExcluido = new DevExpress.XtraEditors.ToggleSwitch();
            this.textEdit_ProdCod = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_ProdPrecoTab = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_ProdPrecoMin = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_ProdMult = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_ProdPesoBruto = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_ProdIpi = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_ProdCodNCM = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_ProdComissao = new DevExpress.XtraEditors.TextEdit();
            this.memoEdit_ProdObservacao = new DevExpress.XtraEditors.MemoEdit();
            this.simpleButton_ProdDefCorTam = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit_ProdGradeTamanhos = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton_ProdGradeCor_Gravar = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton_ProdGradeTamanho_Gravar = new DevExpress.XtraEditors.SimpleButton();
            this.comboBoxEdit_ProdTipoIpi = new DevExpress.XtraEditors.LookUpEdit();
            this.textEdit_ProdUnidade = new DevExpress.XtraEditors.LookUpEdit();
            this.comboBoxEdit_ProdMoeda = new DevExpress.XtraEditors.LookUpEdit();
            this.comboBoxEdit_ProdCat = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup_RegisterProd = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleLabelItem_RegisterProdCat = new DevExpress.XtraLayout.SimpleLabelItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup_Prod = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem_ProdNome = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_ProdCod = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_ProdCat = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_ProdPrecoTab = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_ProdPrecoMin = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_ProdUnidade = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_ProdMult = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem19 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem_ProdIpi = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_ProdTipoIpi = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem21 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem22 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem_ProdComissao = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleLabelItem_ProdComissaoPercent = new DevExpress.XtraLayout.SimpleLabelItem();
            this.layoutControlItem_ProdMoeda = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem24 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem25 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem_ProdPesoBruto = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleLabelItem_ProdPesoB_KG = new DevExpress.XtraLayout.SimpleLabelItem();
            this.layoutControlItem_ProdCodNCM = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem28 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem29 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem26 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem17 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem18 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem20 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup_ProdGrades = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem30 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem31 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem32 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem33 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup_GradeCores = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem35 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem_ProdGradeCores = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup_GradeTamanhos = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem34 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem_ProdGradeTamanhos = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem27 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup_ProdStatus = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem_ProdExcluido = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_DtUltAlteracao = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem_ProdObservacao = new DevExpress.XtraLayout.LayoutControlItem();
            this.produtoCategoriaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.meusPedidosDataSet = new View_MeusPedidos.MeusPedidosDataSet();
            this.tileNavSubItem_Cliente = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CondicaoPagamentoCliente = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CategoriasCliente = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TabelaPreco = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Usuarios = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Produto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_ImagensProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TabelaProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TabelaPrecoProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CategoriaProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Pedido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CamposExtraPedido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Transportadora = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CondicaoPagamento = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_FormaPagamento = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_FaturamentoPedido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TituloVencido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.splashScreenManager_ProdCat = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::View_MeusPedidos.WaitForm1), true, true);
            this.tabNavigationPage_RegisterProd = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.panelControl_RegisterClientes = new DevExpress.XtraEditors.PanelControl();
            this.simpleLabelItem_RegisterClientes = new DevExpress.XtraLayout.SimpleLabelItem();
            this.tabNavigationPage_DataGridProd = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.gridControl_Prod = new DevExpress.XtraGrid.GridControl();
            this.gridView_Prod = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colnome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcomissao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colpreco_tabela = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colpreco_minimo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colipi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coltipo_ipi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colst = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colmoeda = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colunidade = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colsaldo_estoque = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colobservacoes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colultima_alteracao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colexcluido = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colativo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcategoria_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcodigo_ncm = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colmultiplo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colpeso_bruto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colmptran = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tabPane_ConfigProd = new DevExpress.XtraBars.Navigation.TabPane();
            this.tabPane1 = new DevExpress.XtraBars.Navigation.TabPane();
            this.tabNavigationPage1 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.tabNavigationPage2 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit8 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit9 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit10 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit11 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit12 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit13 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit14 = new DevExpress.XtraEditors.TextEdit();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.toggleSwitch1 = new DevExpress.XtraEditors.ToggleSwitch();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleLabelItem1 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem16 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup3 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup4 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.simpleSeparator2 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator3 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlGroup_CLITelefone1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.colid1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colrazao_social = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colnome_fantasia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup_CatProd = new DevExpress.XtraLayout.LayoutControlGroup();
            this.produtoCategoriaTableAdapter = new View_MeusPedidos.MeusPedidosDataSetTableAdapters.ProdutoCategoriaTableAdapter();
            this.behaviorManager1 = new DevExpress.Utils.Behaviors.BehaviorManager(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ProdGradeCores.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager_ContasMP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_Prod)).BeginInit();
            this.layoutControl_Prod.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ProdNome.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_ProdUltAlteracao.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_ProdUltAlteracao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ProdExcluido.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ProdCod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ProdPrecoTab.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ProdPrecoMin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ProdMult.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ProdPesoBruto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ProdIpi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ProdCodNCM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ProdComissao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_ProdObservacao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ProdGradeTamanhos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit_ProdTipoIpi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ProdUnidade.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit_ProdMoeda.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit_ProdCat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_RegisterProd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem_RegisterProdCat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_Prod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdNome)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdCod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdCat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdPrecoTab)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdPrecoMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdUnidade)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdMult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdIpi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdTipoIpi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdComissao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem_ProdComissaoPercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdMoeda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdPesoBruto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem_ProdPesoB_KG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdCodNCM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_ProdGrades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_GradeCores)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdGradeCores)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_GradeTamanhos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdGradeTamanhos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_ProdStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdExcluido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_DtUltAlteracao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdObservacao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.produtoCategoriaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meusPedidosDataSet)).BeginInit();
            this.tabNavigationPage_RegisterProd.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl_RegisterClientes)).BeginInit();
            this.panelControl_RegisterClientes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem_RegisterClientes)).BeginInit();
            this.tabNavigationPage_DataGridProd.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_Prod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_Prod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane_ConfigProd)).BeginInit();
            this.tabPane_ConfigProd.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).BeginInit();
            this.tabPane1.SuspendLayout();
            this.tabNavigationPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toggleSwitch1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CLITelefone1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CatProd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // textEdit_ProdGradeCores
            // 
            this.textEdit_ProdGradeCores.Location = new System.Drawing.Point(66, 333);
            this.textEdit_ProdGradeCores.MenuManager = this.barManager_ContasMP;
            this.textEdit_ProdGradeCores.Name = "textEdit_ProdGradeCores";
            this.textEdit_ProdGradeCores.Size = new System.Drawing.Size(422, 20);
            this.textEdit_ProdGradeCores.StyleController = this.layoutControl_Prod;
            this.textEdit_ProdGradeCores.TabIndex = 24;
            // 
            // barManager_ContasMP
            // 
            this.barManager_ContasMP.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar_Acoes,
            this.bar_Acoes2});
            this.barManager_ContasMP.DockControls.Add(this.barDockControlTop);
            this.barManager_ContasMP.DockControls.Add(this.barDockControlBottom);
            this.barManager_ContasMP.DockControls.Add(this.barDockControlLeft);
            this.barManager_ContasMP.DockControls.Add(this.barDockControlRight);
            this.barManager_ContasMP.Form = this;
            this.barManager_ContasMP.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem_Add,
            this.barButtonItem_Edit,
            this.barButtonItem_Del,
            this.barButtonItem_Gravar,
            this.barButtonItem_Cancelar});
            this.barManager_ContasMP.MaxItemId = 5;
            // 
            // bar_Acoes
            // 
            this.bar_Acoes.BarName = "Tools";
            this.bar_Acoes.DockCol = 0;
            this.bar_Acoes.DockRow = 0;
            this.bar_Acoes.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar_Acoes.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Add),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Edit),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Del)});
            this.bar_Acoes.OptionsBar.UseWholeRow = true;
            this.bar_Acoes.Text = "Tools";
            // 
            // barButtonItem_Add
            // 
            this.barButtonItem_Add.Caption = "Adicionar";
            this.barButtonItem_Add.Id = 0;
            this.barButtonItem_Add.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Add.ImageOptions.Image")));
            this.barButtonItem_Add.Name = "barButtonItem_Add";
            this.barButtonItem_Add.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Add.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Add_ItemClick);
            // 
            // barButtonItem_Edit
            // 
            this.barButtonItem_Edit.Caption = "Editar";
            this.barButtonItem_Edit.Id = 1;
            this.barButtonItem_Edit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Edit.ImageOptions.Image")));
            this.barButtonItem_Edit.Name = "barButtonItem_Edit";
            this.barButtonItem_Edit.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Edit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Edit_ItemClick);
            // 
            // barButtonItem_Del
            // 
            this.barButtonItem_Del.Caption = "Excluir";
            this.barButtonItem_Del.Id = 2;
            this.barButtonItem_Del.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Del.ImageOptions.Image")));
            this.barButtonItem_Del.Name = "barButtonItem_Del";
            this.barButtonItem_Del.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Del.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Del_ItemClick);
            // 
            // bar_Acoes2
            // 
            this.bar_Acoes2.BarName = "Tools_GravarCancelar";
            this.bar_Acoes2.DockCol = 0;
            this.bar_Acoes2.DockRow = 1;
            this.bar_Acoes2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar_Acoes2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Gravar),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Cancelar)});
            this.bar_Acoes2.OptionsBar.AllowRename = true;
            this.bar_Acoes2.OptionsBar.UseWholeRow = true;
            this.bar_Acoes2.Text = "Tools_GravarCancelar";
            // 
            // barButtonItem_Gravar
            // 
            this.barButtonItem_Gravar.Caption = "Gravar";
            this.barButtonItem_Gravar.Id = 3;
            this.barButtonItem_Gravar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Gravar.ImageOptions.Image")));
            this.barButtonItem_Gravar.Name = "barButtonItem_Gravar";
            this.barButtonItem_Gravar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Gravar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Gravar_ItemClick);
            // 
            // barButtonItem_Cancelar
            // 
            this.barButtonItem_Cancelar.Caption = "Cancelar";
            this.barButtonItem_Cancelar.Id = 4;
            this.barButtonItem_Cancelar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Cancelar.ImageOptions.Image")));
            this.barButtonItem_Cancelar.Name = "barButtonItem_Cancelar";
            this.barButtonItem_Cancelar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Cancelar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Cancelar_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager_ContasMP;
            this.barDockControlTop.Size = new System.Drawing.Size(1264, 94);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 562);
            this.barDockControlBottom.Manager = this.barManager_ContasMP;
            this.barDockControlBottom.Size = new System.Drawing.Size(1264, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 94);
            this.barDockControlLeft.Manager = this.barManager_ContasMP;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 468);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1264, 94);
            this.barDockControlRight.Manager = this.barManager_ContasMP;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 468);
            // 
            // layoutControl_Prod
            // 
            this.layoutControl_Prod.Controls.Add(this.textEdit_ProdNome);
            this.layoutControl_Prod.Controls.Add(this.dateEdit_ProdUltAlteracao);
            this.layoutControl_Prod.Controls.Add(this.textEdit_ProdExcluido);
            this.layoutControl_Prod.Controls.Add(this.textEdit_ProdCod);
            this.layoutControl_Prod.Controls.Add(this.textEdit_ProdPrecoTab);
            this.layoutControl_Prod.Controls.Add(this.textEdit_ProdPrecoMin);
            this.layoutControl_Prod.Controls.Add(this.textEdit_ProdMult);
            this.layoutControl_Prod.Controls.Add(this.textEdit_ProdPesoBruto);
            this.layoutControl_Prod.Controls.Add(this.textEdit_ProdIpi);
            this.layoutControl_Prod.Controls.Add(this.textEdit_ProdCodNCM);
            this.layoutControl_Prod.Controls.Add(this.textEdit_ProdComissao);
            this.layoutControl_Prod.Controls.Add(this.memoEdit_ProdObservacao);
            this.layoutControl_Prod.Controls.Add(this.simpleButton_ProdDefCorTam);
            this.layoutControl_Prod.Controls.Add(this.textEdit_ProdGradeCores);
            this.layoutControl_Prod.Controls.Add(this.textEdit_ProdGradeTamanhos);
            this.layoutControl_Prod.Controls.Add(this.simpleButton_ProdGradeCor_Gravar);
            this.layoutControl_Prod.Controls.Add(this.simpleButton_ProdGradeTamanho_Gravar);
            this.layoutControl_Prod.Controls.Add(this.comboBoxEdit_ProdTipoIpi);
            this.layoutControl_Prod.Controls.Add(this.textEdit_ProdUnidade);
            this.layoutControl_Prod.Controls.Add(this.comboBoxEdit_ProdMoeda);
            this.layoutControl_Prod.Controls.Add(this.comboBoxEdit_ProdCat);
            this.layoutControl_Prod.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl_Prod.Location = new System.Drawing.Point(2, 2);
            this.layoutControl_Prod.Name = "layoutControl_Prod";
            this.layoutControl_Prod.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(622, 166, 450, 400);
            this.layoutControl_Prod.Root = this.layoutControlGroup_RegisterProd;
            this.layoutControl_Prod.Size = new System.Drawing.Size(1242, 400);
            this.layoutControl_Prod.TabIndex = 0;
            this.layoutControl_Prod.Text = "layoutControl_ProdCat";
            // 
            // textEdit_ProdNome
            // 
            this.textEdit_ProdNome.Location = new System.Drawing.Point(157, 113);
            this.textEdit_ProdNome.MenuManager = this.barManager_ContasMP;
            this.textEdit_ProdNome.Name = "textEdit_ProdNome";
            this.textEdit_ProdNome.Size = new System.Drawing.Size(1024, 20);
            this.textEdit_ProdNome.StyleController = this.layoutControl_Prod;
            this.textEdit_ProdNome.TabIndex = 6;
            // 
            // dateEdit_ProdUltAlteracao
            // 
            this.dateEdit_ProdUltAlteracao.EditValue = null;
            this.dateEdit_ProdUltAlteracao.Enabled = false;
            this.dateEdit_ProdUltAlteracao.Location = new System.Drawing.Point(758, 382);
            this.dateEdit_ProdUltAlteracao.MenuManager = this.barManager_ContasMP;
            this.dateEdit_ProdUltAlteracao.Name = "dateEdit_ProdUltAlteracao";
            this.dateEdit_ProdUltAlteracao.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit_ProdUltAlteracao.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit_ProdUltAlteracao.Size = new System.Drawing.Size(433, 20);
            this.dateEdit_ProdUltAlteracao.StyleController = this.layoutControl_Prod;
            this.dateEdit_ProdUltAlteracao.TabIndex = 7;
            // 
            // textEdit_ProdExcluido
            // 
            this.textEdit_ProdExcluido.Location = new System.Drawing.Point(758, 406);
            this.textEdit_ProdExcluido.Name = "textEdit_ProdExcluido";
            this.textEdit_ProdExcluido.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.textEdit_ProdExcluido.Properties.OffText = "Não";
            this.textEdit_ProdExcluido.Properties.OnText = "Sim";
            this.textEdit_ProdExcluido.Size = new System.Drawing.Size(433, 24);
            this.textEdit_ProdExcluido.StyleController = this.layoutControl_Prod;
            this.textEdit_ProdExcluido.TabIndex = 10;
            // 
            // textEdit_ProdCod
            // 
            this.textEdit_ProdCod.Location = new System.Drawing.Point(157, 89);
            this.textEdit_ProdCod.MenuManager = this.barManager_ContasMP;
            this.textEdit_ProdCod.Name = "textEdit_ProdCod";
            this.textEdit_ProdCod.Size = new System.Drawing.Size(396, 20);
            this.textEdit_ProdCod.StyleController = this.layoutControl_Prod;
            this.textEdit_ProdCod.TabIndex = 11;
            // 
            // textEdit_ProdPrecoTab
            // 
            this.textEdit_ProdPrecoTab.Location = new System.Drawing.Point(157, 137);
            this.textEdit_ProdPrecoTab.MenuManager = this.barManager_ContasMP;
            this.textEdit_ProdPrecoTab.Name = "textEdit_ProdPrecoTab";
            this.textEdit_ProdPrecoTab.Properties.Mask.EditMask = "n2";
            this.textEdit_ProdPrecoTab.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEdit_ProdPrecoTab.Size = new System.Drawing.Size(396, 20);
            this.textEdit_ProdPrecoTab.StyleController = this.layoutControl_Prod;
            this.textEdit_ProdPrecoTab.TabIndex = 13;
            // 
            // textEdit_ProdPrecoMin
            // 
            this.textEdit_ProdPrecoMin.Location = new System.Drawing.Point(157, 161);
            this.textEdit_ProdPrecoMin.MenuManager = this.barManager_ContasMP;
            this.textEdit_ProdPrecoMin.Name = "textEdit_ProdPrecoMin";
            this.textEdit_ProdPrecoMin.Properties.Mask.EditMask = "n2";
            this.textEdit_ProdPrecoMin.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEdit_ProdPrecoMin.Size = new System.Drawing.Size(396, 20);
            this.textEdit_ProdPrecoMin.StyleController = this.layoutControl_Prod;
            this.textEdit_ProdPrecoMin.TabIndex = 14;
            // 
            // textEdit_ProdMult
            // 
            this.textEdit_ProdMult.Location = new System.Drawing.Point(447, 185);
            this.textEdit_ProdMult.MenuManager = this.barManager_ContasMP;
            this.textEdit_ProdMult.Name = "textEdit_ProdMult";
            this.textEdit_ProdMult.Properties.Mask.EditMask = "n2";
            this.textEdit_ProdMult.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEdit_ProdMult.Size = new System.Drawing.Size(107, 20);
            this.textEdit_ProdMult.StyleController = this.layoutControl_Prod;
            this.textEdit_ProdMult.TabIndex = 16;
            // 
            // textEdit_ProdPesoBruto
            // 
            this.textEdit_ProdPesoBruto.Location = new System.Drawing.Point(774, 185);
            this.textEdit_ProdPesoBruto.MenuManager = this.barManager_ContasMP;
            this.textEdit_ProdPesoBruto.Name = "textEdit_ProdPesoBruto";
            this.textEdit_ProdPesoBruto.Properties.Mask.EditMask = "n0";
            this.textEdit_ProdPesoBruto.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEdit_ProdPesoBruto.Size = new System.Drawing.Size(165, 20);
            this.textEdit_ProdPesoBruto.StyleController = this.layoutControl_Prod;
            this.textEdit_ProdPesoBruto.TabIndex = 17;
            // 
            // textEdit_ProdIpi
            // 
            this.textEdit_ProdIpi.Location = new System.Drawing.Point(157, 209);
            this.textEdit_ProdIpi.MenuManager = this.barManager_ContasMP;
            this.textEdit_ProdIpi.Name = "textEdit_ProdIpi";
            this.textEdit_ProdIpi.Size = new System.Drawing.Size(51, 20);
            this.textEdit_ProdIpi.StyleController = this.layoutControl_Prod;
            this.textEdit_ProdIpi.TabIndex = 18;
            // 
            // textEdit_ProdCodNCM
            // 
            this.textEdit_ProdCodNCM.Location = new System.Drawing.Point(774, 209);
            this.textEdit_ProdCodNCM.MenuManager = this.barManager_ContasMP;
            this.textEdit_ProdCodNCM.Name = "textEdit_ProdCodNCM";
            this.textEdit_ProdCodNCM.Size = new System.Drawing.Size(165, 20);
            this.textEdit_ProdCodNCM.StyleController = this.layoutControl_Prod;
            this.textEdit_ProdCodNCM.TabIndex = 20;
            // 
            // textEdit_ProdComissao
            // 
            this.textEdit_ProdComissao.Location = new System.Drawing.Point(157, 233);
            this.textEdit_ProdComissao.MenuManager = this.barManager_ContasMP;
            this.textEdit_ProdComissao.Name = "textEdit_ProdComissao";
            this.textEdit_ProdComissao.Properties.Mask.EditMask = "n0";
            this.textEdit_ProdComissao.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEdit_ProdComissao.Size = new System.Drawing.Size(51, 20);
            this.textEdit_ProdComissao.StyleController = this.layoutControl_Prod;
            this.textEdit_ProdComissao.TabIndex = 21;
            // 
            // memoEdit_ProdObservacao
            // 
            this.memoEdit_ProdObservacao.Location = new System.Drawing.Point(147, 382);
            this.memoEdit_ProdObservacao.Name = "memoEdit_ProdObservacao";
            this.memoEdit_ProdObservacao.Size = new System.Drawing.Size(417, 48);
            this.memoEdit_ProdObservacao.StyleController = this.layoutControl_Prod;
            this.memoEdit_ProdObservacao.TabIndex = 18;
            // 
            // simpleButton_ProdDefCorTam
            // 
            this.simpleButton_ProdDefCorTam.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.simpleButton_ProdDefCorTam.Appearance.Options.UseForeColor = true;
            this.simpleButton_ProdDefCorTam.Location = new System.Drawing.Point(44, 267);
            this.simpleButton_ProdDefCorTam.Name = "simpleButton_ProdDefCorTam";
            this.simpleButton_ProdDefCorTam.Size = new System.Drawing.Size(1137, 22);
            this.simpleButton_ProdDefCorTam.StyleController = this.layoutControl_Prod;
            this.simpleButton_ProdDefCorTam.TabIndex = 23;
            this.simpleButton_ProdDefCorTam.Text = "Definir cores e tamanhos";
            this.simpleButton_ProdDefCorTam.Click += new System.EventHandler(this.simpleButton_ProdDefCorTam_Click);
            // 
            // textEdit_ProdGradeTamanhos
            // 
            this.textEdit_ProdGradeTamanhos.Location = new System.Drawing.Point(606, 333);
            this.textEdit_ProdGradeTamanhos.MenuManager = this.barManager_ContasMP;
            this.textEdit_ProdGradeTamanhos.Name = "textEdit_ProdGradeTamanhos";
            this.textEdit_ProdGradeTamanhos.Size = new System.Drawing.Size(449, 20);
            this.textEdit_ProdGradeTamanhos.StyleController = this.layoutControl_Prod;
            this.textEdit_ProdGradeTamanhos.TabIndex = 25;
            // 
            // simpleButton_ProdGradeCor_Gravar
            // 
            this.simpleButton_ProdGradeCor_Gravar.Location = new System.Drawing.Point(492, 333);
            this.simpleButton_ProdGradeCor_Gravar.Name = "simpleButton_ProdGradeCor_Gravar";
            this.simpleButton_ProdGradeCor_Gravar.Size = new System.Drawing.Size(86, 22);
            this.simpleButton_ProdGradeCor_Gravar.StyleController = this.layoutControl_Prod;
            this.simpleButton_ProdGradeCor_Gravar.TabIndex = 26;
            this.simpleButton_ProdGradeCor_Gravar.Text = "Gravar";
            this.simpleButton_ProdGradeCor_Gravar.Click += new System.EventHandler(this.simpleButton_ProdGradeCor_Gravar_Click);
            // 
            // simpleButton_ProdGradeTamanho_Gravar
            // 
            this.simpleButton_ProdGradeTamanho_Gravar.Location = new System.Drawing.Point(1059, 333);
            this.simpleButton_ProdGradeTamanho_Gravar.Name = "simpleButton_ProdGradeTamanho_Gravar";
            this.simpleButton_ProdGradeTamanho_Gravar.Size = new System.Drawing.Size(76, 22);
            this.simpleButton_ProdGradeTamanho_Gravar.StyleController = this.layoutControl_Prod;
            this.simpleButton_ProdGradeTamanho_Gravar.TabIndex = 27;
            this.simpleButton_ProdGradeTamanho_Gravar.Text = "Gravar";
            this.simpleButton_ProdGradeTamanho_Gravar.Click += new System.EventHandler(this.simpleButton_ProdGradeTamanho_Gravar_Click);
            // 
            // comboBoxEdit_ProdTipoIpi
            // 
            this.comboBoxEdit_ProdTipoIpi.Location = new System.Drawing.Point(447, 209);
            this.comboBoxEdit_ProdTipoIpi.MenuManager = this.barManager_ContasMP;
            this.comboBoxEdit_ProdTipoIpi.Name = "comboBoxEdit_ProdTipoIpi";
            this.comboBoxEdit_ProdTipoIpi.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit_ProdTipoIpi.Properties.NullText = "";
            this.comboBoxEdit_ProdTipoIpi.Properties.PopupSizeable = false;
            this.comboBoxEdit_ProdTipoIpi.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.comboBoxEdit_ProdTipoIpi.Size = new System.Drawing.Size(107, 20);
            this.comboBoxEdit_ProdTipoIpi.StyleController = this.layoutControl_Prod;
            this.comboBoxEdit_ProdTipoIpi.TabIndex = 19;
            // 
            // textEdit_ProdUnidade
            // 
            this.textEdit_ProdUnidade.Location = new System.Drawing.Point(157, 185);
            this.textEdit_ProdUnidade.MenuManager = this.barManager_ContasMP;
            this.textEdit_ProdUnidade.Name = "textEdit_ProdUnidade";
            this.textEdit_ProdUnidade.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.textEdit_ProdUnidade.Properties.NullText = "";
            this.textEdit_ProdUnidade.Properties.PopupSizeable = false;
            this.textEdit_ProdUnidade.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.textEdit_ProdUnidade.Size = new System.Drawing.Size(51, 20);
            this.textEdit_ProdUnidade.StyleController = this.layoutControl_Prod;
            this.textEdit_ProdUnidade.TabIndex = 15;
            // 
            // comboBoxEdit_ProdMoeda
            // 
            this.comboBoxEdit_ProdMoeda.Location = new System.Drawing.Point(447, 233);
            this.comboBoxEdit_ProdMoeda.MenuManager = this.barManager_ContasMP;
            this.comboBoxEdit_ProdMoeda.Name = "comboBoxEdit_ProdMoeda";
            this.comboBoxEdit_ProdMoeda.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit_ProdMoeda.Properties.NullText = "";
            this.comboBoxEdit_ProdMoeda.Properties.PopupSizeable = false;
            this.comboBoxEdit_ProdMoeda.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.comboBoxEdit_ProdMoeda.Size = new System.Drawing.Size(734, 20);
            this.comboBoxEdit_ProdMoeda.StyleController = this.layoutControl_Prod;
            this.comboBoxEdit_ProdMoeda.TabIndex = 22;
            // 
            // comboBoxEdit_ProdCat
            // 
            this.comboBoxEdit_ProdCat.Location = new System.Drawing.Point(773, 89);
            this.comboBoxEdit_ProdCat.MenuManager = this.barManager_ContasMP;
            this.comboBoxEdit_ProdCat.Name = "comboBoxEdit_ProdCat";
            this.comboBoxEdit_ProdCat.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit_ProdCat.Properties.NullText = "";
            this.comboBoxEdit_ProdCat.Properties.PopupSizeable = false;
            this.comboBoxEdit_ProdCat.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.comboBoxEdit_ProdCat.Size = new System.Drawing.Size(166, 20);
            this.comboBoxEdit_ProdCat.StyleController = this.layoutControl_Prod;
            this.comboBoxEdit_ProdCat.TabIndex = 12;
            // 
            // layoutControlGroup_RegisterProd
            // 
            this.layoutControlGroup_RegisterProd.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup_RegisterProd.GroupBordersVisible = false;
            this.layoutControlGroup_RegisterProd.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.emptySpaceItem4,
            this.simpleLabelItem_RegisterProdCat,
            this.emptySpaceItem5,
            this.emptySpaceItem9,
            this.layoutControlGroup_Prod,
            this.emptySpaceItem3,
            this.emptySpaceItem27,
            this.layoutControlGroup_ProdStatus});
            this.layoutControlGroup_RegisterProd.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup_RegisterProd.Name = "Root";
            this.layoutControlGroup_RegisterProd.Size = new System.Drawing.Size(1225, 464);
            this.layoutControlGroup_RegisterProd.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(10, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(1185, 10);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(1195, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(10, 444);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleLabelItem_RegisterProdCat
            // 
            this.simpleLabelItem_RegisterProdCat.AllowHotTrack = false;
            this.simpleLabelItem_RegisterProdCat.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem_RegisterProdCat.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem_RegisterProdCat.Location = new System.Drawing.Point(10, 10);
            this.simpleLabelItem_RegisterProdCat.Name = "simpleLabelItem_RegisterClientes";
            this.simpleLabelItem_RegisterProdCat.OptionsPrint.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem_RegisterProdCat.OptionsPrint.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem_RegisterProdCat.Size = new System.Drawing.Size(1185, 17);
            this.simpleLabelItem_RegisterProdCat.Text = "Cadastro de Produto";
            this.simpleLabelItem_RegisterProdCat.TextSize = new System.Drawing.Size(110, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(10, 27);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(1185, 10);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.Location = new System.Drawing.Point(10, 330);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(1185, 10);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup_Prod
            // 
            this.layoutControlGroup_Prod.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem10,
            this.layoutControlItem_ProdNome,
            this.layoutControlItem_ProdCod,
            this.layoutControlItem_ProdCat,
            this.layoutControlItem_ProdPrecoTab,
            this.layoutControlItem_ProdPrecoMin,
            this.layoutControlItem_ProdUnidade,
            this.layoutControlItem_ProdMult,
            this.emptySpaceItem19,
            this.layoutControlItem_ProdIpi,
            this.layoutControlItem_ProdTipoIpi,
            this.emptySpaceItem21,
            this.emptySpaceItem22,
            this.layoutControlItem_ProdComissao,
            this.simpleLabelItem_ProdComissaoPercent,
            this.layoutControlItem_ProdMoeda,
            this.emptySpaceItem24,
            this.emptySpaceItem25,
            this.layoutControlItem_ProdPesoBruto,
            this.simpleLabelItem_ProdPesoB_KG,
            this.layoutControlItem_ProdCodNCM,
            this.emptySpaceItem28,
            this.emptySpaceItem29,
            this.emptySpaceItem26,
            this.emptySpaceItem2,
            this.emptySpaceItem17,
            this.emptySpaceItem18,
            this.layoutControlItem1,
            this.emptySpaceItem20,
            this.layoutControlGroup_ProdGrades});
            this.layoutControlGroup_Prod.Location = new System.Drawing.Point(10, 37);
            this.layoutControlGroup_Prod.Name = "layoutControlGroup_Prod";
            this.layoutControlGroup_Prod.Size = new System.Drawing.Size(1185, 293);
            this.layoutControlGroup_Prod.Text = "Produto";
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.Location = new System.Drawing.Point(10, 0);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(1141, 10);
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem_ProdNome
            // 
            this.layoutControlItem_ProdNome.Control = this.textEdit_ProdNome;
            this.layoutControlItem_ProdNome.Location = new System.Drawing.Point(10, 34);
            this.layoutControlItem_ProdNome.Name = "layoutControlItem_ProdNome";
            this.layoutControlItem_ProdNome.Size = new System.Drawing.Size(1141, 24);
            this.layoutControlItem_ProdNome.Text = "Nome";
            this.layoutControlItem_ProdNome.TextSize = new System.Drawing.Size(110, 13);
            // 
            // layoutControlItem_ProdCod
            // 
            this.layoutControlItem_ProdCod.Control = this.textEdit_ProdCod;
            this.layoutControlItem_ProdCod.Location = new System.Drawing.Point(10, 10);
            this.layoutControlItem_ProdCod.Name = "layoutControlItem_ProdCod";
            this.layoutControlItem_ProdCod.Size = new System.Drawing.Size(513, 24);
            this.layoutControlItem_ProdCod.Text = "Código";
            this.layoutControlItem_ProdCod.TextSize = new System.Drawing.Size(110, 13);
            // 
            // layoutControlItem_ProdCat
            // 
            this.layoutControlItem_ProdCat.Control = this.comboBoxEdit_ProdCat;
            this.layoutControlItem_ProdCat.Location = new System.Drawing.Point(626, 10);
            this.layoutControlItem_ProdCat.Name = "layoutControlItem_ProdCat";
            this.layoutControlItem_ProdCat.Size = new System.Drawing.Size(283, 24);
            this.layoutControlItem_ProdCat.Text = "Categoria";
            this.layoutControlItem_ProdCat.TextSize = new System.Drawing.Size(110, 13);
            // 
            // layoutControlItem_ProdPrecoTab
            // 
            this.layoutControlItem_ProdPrecoTab.Control = this.textEdit_ProdPrecoTab;
            this.layoutControlItem_ProdPrecoTab.Location = new System.Drawing.Point(10, 58);
            this.layoutControlItem_ProdPrecoTab.Name = "layoutControlItem_ProdPrecoTab";
            this.layoutControlItem_ProdPrecoTab.Size = new System.Drawing.Size(513, 24);
            this.layoutControlItem_ProdPrecoTab.Text = "Preço de Tabela";
            this.layoutControlItem_ProdPrecoTab.TextSize = new System.Drawing.Size(110, 13);
            // 
            // layoutControlItem_ProdPrecoMin
            // 
            this.layoutControlItem_ProdPrecoMin.Control = this.textEdit_ProdPrecoMin;
            this.layoutControlItem_ProdPrecoMin.Location = new System.Drawing.Point(10, 82);
            this.layoutControlItem_ProdPrecoMin.Name = "layoutControlItem_ProdPrecoMin";
            this.layoutControlItem_ProdPrecoMin.Size = new System.Drawing.Size(513, 24);
            this.layoutControlItem_ProdPrecoMin.Text = "Preço Mínimo";
            this.layoutControlItem_ProdPrecoMin.TextSize = new System.Drawing.Size(110, 13);
            // 
            // layoutControlItem_ProdUnidade
            // 
            this.layoutControlItem_ProdUnidade.Control = this.textEdit_ProdUnidade;
            this.layoutControlItem_ProdUnidade.Location = new System.Drawing.Point(10, 106);
            this.layoutControlItem_ProdUnidade.Name = "layoutControlItem_ProdUnidade";
            this.layoutControlItem_ProdUnidade.Size = new System.Drawing.Size(168, 24);
            this.layoutControlItem_ProdUnidade.Text = "Unidade";
            this.layoutControlItem_ProdUnidade.TextSize = new System.Drawing.Size(110, 13);
            // 
            // layoutControlItem_ProdMult
            // 
            this.layoutControlItem_ProdMult.Control = this.textEdit_ProdMult;
            this.layoutControlItem_ProdMult.Location = new System.Drawing.Point(300, 106);
            this.layoutControlItem_ProdMult.Name = "layoutControlItem_ProdMult";
            this.layoutControlItem_ProdMult.Size = new System.Drawing.Size(224, 24);
            this.layoutControlItem_ProdMult.Text = "Vender em múltiplos de";
            this.layoutControlItem_ProdMult.TextSize = new System.Drawing.Size(110, 13);
            // 
            // emptySpaceItem19
            // 
            this.emptySpaceItem19.AllowHotTrack = false;
            this.emptySpaceItem19.Location = new System.Drawing.Point(524, 106);
            this.emptySpaceItem19.Name = "emptySpaceItem19";
            this.emptySpaceItem19.Size = new System.Drawing.Size(103, 24);
            this.emptySpaceItem19.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem_ProdIpi
            // 
            this.layoutControlItem_ProdIpi.Control = this.textEdit_ProdIpi;
            this.layoutControlItem_ProdIpi.Location = new System.Drawing.Point(10, 130);
            this.layoutControlItem_ProdIpi.Name = "layoutControlItem_ProdIpi";
            this.layoutControlItem_ProdIpi.Size = new System.Drawing.Size(168, 24);
            this.layoutControlItem_ProdIpi.Text = "IPI";
            this.layoutControlItem_ProdIpi.TextSize = new System.Drawing.Size(110, 13);
            // 
            // layoutControlItem_ProdTipoIpi
            // 
            this.layoutControlItem_ProdTipoIpi.Control = this.comboBoxEdit_ProdTipoIpi;
            this.layoutControlItem_ProdTipoIpi.Location = new System.Drawing.Point(300, 130);
            this.layoutControlItem_ProdTipoIpi.Name = "layoutControlItem_ProdTipoIpi";
            this.layoutControlItem_ProdTipoIpi.Size = new System.Drawing.Size(224, 24);
            this.layoutControlItem_ProdTipoIpi.Text = "Tipo IPI";
            this.layoutControlItem_ProdTipoIpi.TextSize = new System.Drawing.Size(110, 13);
            // 
            // emptySpaceItem21
            // 
            this.emptySpaceItem21.AllowHotTrack = false;
            this.emptySpaceItem21.Location = new System.Drawing.Point(524, 130);
            this.emptySpaceItem21.Name = "emptySpaceItem21";
            this.emptySpaceItem21.Size = new System.Drawing.Size(103, 24);
            this.emptySpaceItem21.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem22
            // 
            this.emptySpaceItem22.AllowHotTrack = false;
            this.emptySpaceItem22.Location = new System.Drawing.Point(10, 178);
            this.emptySpaceItem22.Name = "emptySpaceItem22";
            this.emptySpaceItem22.Size = new System.Drawing.Size(1141, 10);
            this.emptySpaceItem22.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem_ProdComissao
            // 
            this.layoutControlItem_ProdComissao.Control = this.textEdit_ProdComissao;
            this.layoutControlItem_ProdComissao.Location = new System.Drawing.Point(10, 154);
            this.layoutControlItem_ProdComissao.Name = "layoutControlItem_ProdComissao";
            this.layoutControlItem_ProdComissao.Size = new System.Drawing.Size(168, 24);
            this.layoutControlItem_ProdComissao.Text = "Comissão";
            this.layoutControlItem_ProdComissao.TextSize = new System.Drawing.Size(110, 13);
            // 
            // simpleLabelItem_ProdComissaoPercent
            // 
            this.simpleLabelItem_ProdComissaoPercent.AllowHotTrack = false;
            this.simpleLabelItem_ProdComissaoPercent.Location = new System.Drawing.Point(178, 154);
            this.simpleLabelItem_ProdComissaoPercent.Name = "simpleLabelItem_ProdComissaoPercent";
            this.simpleLabelItem_ProdComissaoPercent.Size = new System.Drawing.Size(122, 24);
            this.simpleLabelItem_ProdComissaoPercent.Text = "%";
            this.simpleLabelItem_ProdComissaoPercent.TextSize = new System.Drawing.Size(110, 13);
            // 
            // layoutControlItem_ProdMoeda
            // 
            this.layoutControlItem_ProdMoeda.Control = this.comboBoxEdit_ProdMoeda;
            this.layoutControlItem_ProdMoeda.Location = new System.Drawing.Point(300, 154);
            this.layoutControlItem_ProdMoeda.Name = "layoutControlItem_ProdMoeda";
            this.layoutControlItem_ProdMoeda.Size = new System.Drawing.Size(851, 24);
            this.layoutControlItem_ProdMoeda.Text = "Moeda";
            this.layoutControlItem_ProdMoeda.TextSize = new System.Drawing.Size(110, 13);
            // 
            // emptySpaceItem24
            // 
            this.emptySpaceItem24.AllowHotTrack = false;
            this.emptySpaceItem24.Location = new System.Drawing.Point(178, 130);
            this.emptySpaceItem24.Name = "emptySpaceItem24";
            this.emptySpaceItem24.Size = new System.Drawing.Size(122, 24);
            this.emptySpaceItem24.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem25
            // 
            this.emptySpaceItem25.AllowHotTrack = false;
            this.emptySpaceItem25.Location = new System.Drawing.Point(178, 106);
            this.emptySpaceItem25.Name = "emptySpaceItem25";
            this.emptySpaceItem25.Size = new System.Drawing.Size(122, 24);
            this.emptySpaceItem25.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem_ProdPesoBruto
            // 
            this.layoutControlItem_ProdPesoBruto.Control = this.textEdit_ProdPesoBruto;
            this.layoutControlItem_ProdPesoBruto.Location = new System.Drawing.Point(627, 106);
            this.layoutControlItem_ProdPesoBruto.Name = "layoutControlItem_ProdPesoBruto";
            this.layoutControlItem_ProdPesoBruto.Size = new System.Drawing.Size(282, 24);
            this.layoutControlItem_ProdPesoBruto.Text = "Peso Bruto";
            this.layoutControlItem_ProdPesoBruto.TextSize = new System.Drawing.Size(110, 13);
            // 
            // simpleLabelItem_ProdPesoB_KG
            // 
            this.simpleLabelItem_ProdPesoB_KG.AllowHotTrack = false;
            this.simpleLabelItem_ProdPesoB_KG.Location = new System.Drawing.Point(909, 106);
            this.simpleLabelItem_ProdPesoB_KG.Name = "simpleLabelItem_ProdPesoB_KG";
            this.simpleLabelItem_ProdPesoB_KG.Size = new System.Drawing.Size(242, 24);
            this.simpleLabelItem_ProdPesoB_KG.Text = "Kg";
            this.simpleLabelItem_ProdPesoB_KG.TextSize = new System.Drawing.Size(110, 13);
            // 
            // layoutControlItem_ProdCodNCM
            // 
            this.layoutControlItem_ProdCodNCM.Control = this.textEdit_ProdCodNCM;
            this.layoutControlItem_ProdCodNCM.Location = new System.Drawing.Point(627, 130);
            this.layoutControlItem_ProdCodNCM.Name = "layoutControlItem_ProdCodNCM";
            this.layoutControlItem_ProdCodNCM.Size = new System.Drawing.Size(282, 24);
            this.layoutControlItem_ProdCodNCM.Text = "Código NCM";
            this.layoutControlItem_ProdCodNCM.TextSize = new System.Drawing.Size(110, 13);
            // 
            // emptySpaceItem28
            // 
            this.emptySpaceItem28.AllowHotTrack = false;
            this.emptySpaceItem28.Location = new System.Drawing.Point(909, 130);
            this.emptySpaceItem28.Name = "emptySpaceItem28";
            this.emptySpaceItem28.Size = new System.Drawing.Size(242, 24);
            this.emptySpaceItem28.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem29
            // 
            this.emptySpaceItem29.AllowHotTrack = false;
            this.emptySpaceItem29.Location = new System.Drawing.Point(523, 10);
            this.emptySpaceItem29.Name = "emptySpaceItem29";
            this.emptySpaceItem29.Size = new System.Drawing.Size(103, 24);
            this.emptySpaceItem29.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem26
            // 
            this.emptySpaceItem26.AllowHotTrack = false;
            this.emptySpaceItem26.Location = new System.Drawing.Point(1151, 0);
            this.emptySpaceItem26.Name = "emptySpaceItem26";
            this.emptySpaceItem26.Size = new System.Drawing.Size(10, 241);
            this.emptySpaceItem26.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(10, 241);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem17
            // 
            this.emptySpaceItem17.AllowHotTrack = false;
            this.emptySpaceItem17.Location = new System.Drawing.Point(523, 58);
            this.emptySpaceItem17.Name = "emptySpaceItem17";
            this.emptySpaceItem17.Size = new System.Drawing.Size(628, 48);
            this.emptySpaceItem17.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem18
            // 
            this.emptySpaceItem18.AllowHotTrack = false;
            this.emptySpaceItem18.Location = new System.Drawing.Point(909, 10);
            this.emptySpaceItem18.Name = "emptySpaceItem18";
            this.emptySpaceItem18.Size = new System.Drawing.Size(242, 24);
            this.emptySpaceItem18.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.simpleButton_ProdDefCorTam;
            this.layoutControlItem1.Location = new System.Drawing.Point(10, 188);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1141, 26);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem20
            // 
            this.emptySpaceItem20.AllowHotTrack = false;
            this.emptySpaceItem20.Location = new System.Drawing.Point(0, 241);
            this.emptySpaceItem20.Name = "emptySpaceItem20";
            this.emptySpaceItem20.Size = new System.Drawing.Size(1161, 10);
            this.emptySpaceItem20.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup_ProdGrades
            // 
            this.layoutControlGroup_ProdGrades.Expanded = false;
            this.layoutControlGroup_ProdGrades.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem30,
            this.emptySpaceItem31,
            this.emptySpaceItem32,
            this.emptySpaceItem33,
            this.layoutControlGroup_GradeCores,
            this.layoutControlGroup_GradeTamanhos});
            this.layoutControlGroup_ProdGrades.Location = new System.Drawing.Point(10, 214);
            this.layoutControlGroup_ProdGrades.Name = "layoutControlGroup_ProdGrades";
            this.layoutControlGroup_ProdGrades.Size = new System.Drawing.Size(1141, 27);
            this.layoutControlGroup_ProdGrades.Text = "Grades";
            // 
            // emptySpaceItem30
            // 
            this.emptySpaceItem30.AllowHotTrack = false;
            this.emptySpaceItem30.Location = new System.Drawing.Point(10, 0);
            this.emptySpaceItem30.Name = "emptySpaceItem30";
            this.emptySpaceItem30.Size = new System.Drawing.Size(1097, 10);
            this.emptySpaceItem30.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem31
            // 
            this.emptySpaceItem31.AllowHotTrack = false;
            this.emptySpaceItem31.Location = new System.Drawing.Point(10, 88);
            this.emptySpaceItem31.Name = "emptySpaceItem31";
            this.emptySpaceItem31.Size = new System.Drawing.Size(1097, 10);
            this.emptySpaceItem31.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem32
            // 
            this.emptySpaceItem32.AllowHotTrack = false;
            this.emptySpaceItem32.Location = new System.Drawing.Point(1107, 0);
            this.emptySpaceItem32.Name = "emptySpaceItem32";
            this.emptySpaceItem32.Size = new System.Drawing.Size(10, 98);
            this.emptySpaceItem32.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem33
            // 
            this.emptySpaceItem33.AllowHotTrack = false;
            this.emptySpaceItem33.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem33.Name = "emptySpaceItem33";
            this.emptySpaceItem33.Size = new System.Drawing.Size(10, 98);
            this.emptySpaceItem33.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup_GradeCores
            // 
            this.layoutControlGroup_GradeCores.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem35,
            this.layoutControlItem_ProdGradeCores,
            this.layoutControlItem2});
            this.layoutControlGroup_GradeCores.Location = new System.Drawing.Point(10, 10);
            this.layoutControlGroup_GradeCores.Name = "layoutControlGroup_GradeCores";
            this.layoutControlGroup_GradeCores.Size = new System.Drawing.Size(540, 78);
            this.layoutControlGroup_GradeCores.Text = "Cores";
            // 
            // emptySpaceItem35
            // 
            this.emptySpaceItem35.AllowHotTrack = false;
            this.emptySpaceItem35.Location = new System.Drawing.Point(0, 26);
            this.emptySpaceItem35.Name = "emptySpaceItem35";
            this.emptySpaceItem35.Size = new System.Drawing.Size(516, 10);
            this.emptySpaceItem35.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem_ProdGradeCores
            // 
            this.layoutControlItem_ProdGradeCores.Control = this.textEdit_ProdGradeCores;
            this.layoutControlItem_ProdGradeCores.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem_ProdGradeCores.Name = "layoutControlItem_ProdGradeCores";
            this.layoutControlItem_ProdGradeCores.Size = new System.Drawing.Size(426, 26);
            this.layoutControlItem_ProdGradeCores.Text = "Cores";
            this.layoutControlItem_ProdGradeCores.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem_ProdGradeCores.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.simpleButton_ProdGradeCor_Gravar;
            this.layoutControlItem2.Location = new System.Drawing.Point(426, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(90, 26);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlGroup_GradeTamanhos
            // 
            this.layoutControlGroup_GradeTamanhos.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem34,
            this.layoutControlItem_ProdGradeTamanhos,
            this.layoutControlItem3});
            this.layoutControlGroup_GradeTamanhos.Location = new System.Drawing.Point(550, 10);
            this.layoutControlGroup_GradeTamanhos.Name = "layoutControlGroup_GradeTamanhos";
            this.layoutControlGroup_GradeTamanhos.Size = new System.Drawing.Size(557, 78);
            this.layoutControlGroup_GradeTamanhos.Text = "Tamanhos";
            // 
            // emptySpaceItem34
            // 
            this.emptySpaceItem34.AllowHotTrack = false;
            this.emptySpaceItem34.Location = new System.Drawing.Point(0, 26);
            this.emptySpaceItem34.Name = "emptySpaceItem34";
            this.emptySpaceItem34.Size = new System.Drawing.Size(533, 10);
            this.emptySpaceItem34.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem_ProdGradeTamanhos
            // 
            this.layoutControlItem_ProdGradeTamanhos.Control = this.textEdit_ProdGradeTamanhos;
            this.layoutControlItem_ProdGradeTamanhos.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem_ProdGradeTamanhos.Name = "layoutControlItem_ProdGradeTamanhos";
            this.layoutControlItem_ProdGradeTamanhos.Size = new System.Drawing.Size(453, 26);
            this.layoutControlItem_ProdGradeTamanhos.Text = "Tamanho";
            this.layoutControlItem_ProdGradeTamanhos.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem_ProdGradeTamanhos.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.simpleButton_ProdGradeTamanho_Gravar;
            this.layoutControlItem3.Location = new System.Drawing.Point(453, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(80, 26);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(10, 444);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem27
            // 
            this.emptySpaceItem27.AllowHotTrack = false;
            this.emptySpaceItem27.Location = new System.Drawing.Point(10, 434);
            this.emptySpaceItem27.Name = "emptySpaceItem27";
            this.emptySpaceItem27.Size = new System.Drawing.Size(1185, 10);
            this.emptySpaceItem27.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup_ProdStatus
            // 
            this.layoutControlGroup_ProdStatus.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem_ProdExcluido,
            this.layoutControlItem_DtUltAlteracao,
            this.emptySpaceItem7,
            this.layoutControlItem_ProdObservacao});
            this.layoutControlGroup_ProdStatus.Location = new System.Drawing.Point(10, 340);
            this.layoutControlGroup_ProdStatus.Name = "layoutControlGroup_ProdStatus";
            this.layoutControlGroup_ProdStatus.Size = new System.Drawing.Size(1185, 94);
            this.layoutControlGroup_ProdStatus.Text = "Status";
            // 
            // layoutControlItem_ProdExcluido
            // 
            this.layoutControlItem_ProdExcluido.Control = this.textEdit_ProdExcluido;
            this.layoutControlItem_ProdExcluido.CustomizationFormText = "Status";
            this.layoutControlItem_ProdExcluido.Location = new System.Drawing.Point(611, 24);
            this.layoutControlItem_ProdExcluido.Name = "layoutControlItem_ProdExcluido";
            this.layoutControlItem_ProdExcluido.Size = new System.Drawing.Size(550, 28);
            this.layoutControlItem_ProdExcluido.Text = "Excluído";
            this.layoutControlItem_ProdExcluido.TextSize = new System.Drawing.Size(110, 13);
            // 
            // layoutControlItem_DtUltAlteracao
            // 
            this.layoutControlItem_DtUltAlteracao.Control = this.dateEdit_ProdUltAlteracao;
            this.layoutControlItem_DtUltAlteracao.Location = new System.Drawing.Point(611, 0);
            this.layoutControlItem_DtUltAlteracao.Name = "layoutControlItem_DtUltAlteracao";
            this.layoutControlItem_DtUltAlteracao.Size = new System.Drawing.Size(550, 24);
            this.layoutControlItem_DtUltAlteracao.Text = "Última Alteração";
            this.layoutControlItem_DtUltAlteracao.TextSize = new System.Drawing.Size(110, 13);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(534, 0);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(77, 52);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem_ProdObservacao
            // 
            this.layoutControlItem_ProdObservacao.Control = this.memoEdit_ProdObservacao;
            this.layoutControlItem_ProdObservacao.CustomizationFormText = "Observação";
            this.layoutControlItem_ProdObservacao.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem_ProdObservacao.Name = "layoutControlItem_ProdObservacao";
            this.layoutControlItem_ProdObservacao.Size = new System.Drawing.Size(534, 52);
            this.layoutControlItem_ProdObservacao.Text = "Observação";
            this.layoutControlItem_ProdObservacao.TextSize = new System.Drawing.Size(110, 13);
            // 
            // produtoCategoriaBindingSource
            // 
            this.produtoCategoriaBindingSource.DataMember = "ProdutoCategoria";
            this.produtoCategoriaBindingSource.DataSource = this.meusPedidosDataSet;
            // 
            // meusPedidosDataSet
            // 
            this.meusPedidosDataSet.DataSetName = "MeusPedidosDataSet";
            this.meusPedidosDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tileNavSubItem_Cliente
            // 
            this.tileNavSubItem_Cliente.Caption = "Cliente";
            this.tileNavSubItem_Cliente.Name = "tileNavSubItem_Cliente";
            // 
            // 
            // 
            this.tileNavSubItem_Cliente.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement1.Text = "Cliente";
            this.tileNavSubItem_Cliente.Tile.Elements.Add(tileItemElement1);
            this.tileNavSubItem_Cliente.Tile.Name = "tileBarItem3";
            // 
            // tileNavSubItem_CondicaoPagamentoCliente
            // 
            this.tileNavSubItem_CondicaoPagamentoCliente.Caption = "Condições de Pagamento por Cliente";
            this.tileNavSubItem_CondicaoPagamentoCliente.Name = "tileNavSubItem_CondicaoPagamentoCliente";
            // 
            // 
            // 
            this.tileNavSubItem_CondicaoPagamentoCliente.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement2.Text = "Condições de Pagamento por Cliente";
            this.tileNavSubItem_CondicaoPagamentoCliente.Tile.Elements.Add(tileItemElement2);
            this.tileNavSubItem_CondicaoPagamentoCliente.Tile.Name = "tileBarItem4";
            // 
            // tileNavSubItem_CategoriasCliente
            // 
            this.tileNavSubItem_CategoriasCliente.Caption = "Categorias por Cliente";
            this.tileNavSubItem_CategoriasCliente.Name = "tileNavSubItem_CategoriasCliente";
            // 
            // 
            // 
            this.tileNavSubItem_CategoriasCliente.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement3.Text = "Categorias por Cliente";
            this.tileNavSubItem_CategoriasCliente.Tile.Elements.Add(tileItemElement3);
            this.tileNavSubItem_CategoriasCliente.Tile.Name = "tileBarItem6";
            // 
            // tileNavSubItem_TabelaPreco
            // 
            this.tileNavSubItem_TabelaPreco.Caption = "Tabelas de Preço por Cliente";
            this.tileNavSubItem_TabelaPreco.Name = "tileNavSubItem_TabelaPreco";
            // 
            // 
            // 
            this.tileNavSubItem_TabelaPreco.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement4.Text = "Tabelas de Preço por Cliente";
            this.tileNavSubItem_TabelaPreco.Tile.Elements.Add(tileItemElement4);
            this.tileNavSubItem_TabelaPreco.Tile.Name = "tileBarItem7";
            // 
            // tileNavSubItem_Usuarios
            // 
            this.tileNavSubItem_Usuarios.Caption = "Usuários (Vendedores)";
            this.tileNavSubItem_Usuarios.Name = "tileNavSubItem_Usuarios";
            // 
            // 
            // 
            this.tileNavSubItem_Usuarios.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement5.Text = "Usuários (Vendedores)";
            this.tileNavSubItem_Usuarios.Tile.Elements.Add(tileItemElement5);
            this.tileNavSubItem_Usuarios.Tile.Name = "tileBarItem8";
            // 
            // tileNavSubItem_Produto
            // 
            this.tileNavSubItem_Produto.Caption = "Produto";
            this.tileNavSubItem_Produto.Name = "tileNavSubItem_Produto";
            // 
            // 
            // 
            this.tileNavSubItem_Produto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement6.Text = "Produto";
            this.tileNavSubItem_Produto.Tile.Elements.Add(tileItemElement6);
            this.tileNavSubItem_Produto.Tile.Name = "tileBarItem9";
            // 
            // tileNavSubItem_ImagensProduto
            // 
            this.tileNavSubItem_ImagensProduto.Caption = "Imagens do Produto";
            this.tileNavSubItem_ImagensProduto.Name = "tileNavSubItem_ImagensProduto";
            // 
            // 
            // 
            this.tileNavSubItem_ImagensProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement7.Text = "Imagens do Produto";
            this.tileNavSubItem_ImagensProduto.Tile.Elements.Add(tileItemElement7);
            this.tileNavSubItem_ImagensProduto.Tile.Name = "tileBarItem10";
            // 
            // tileNavSubItem_TabelaProduto
            // 
            this.tileNavSubItem_TabelaProduto.Caption = "Tabela de Preço";
            this.tileNavSubItem_TabelaProduto.Name = "tileNavSubItem_TabelaProduto";
            // 
            // 
            // 
            this.tileNavSubItem_TabelaProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement8.Text = "Tabelas de Preço";
            this.tileNavSubItem_TabelaProduto.Tile.Elements.Add(tileItemElement8);
            this.tileNavSubItem_TabelaProduto.Tile.Name = "tileBarItem11";
            // 
            // tileNavSubItem_TabelaPrecoProduto
            // 
            this.tileNavSubItem_TabelaPrecoProduto.Caption = "Tabelas de Preço por Produto";
            this.tileNavSubItem_TabelaPrecoProduto.Name = "tileNavSubItem_TabelaPrecoProduto";
            // 
            // 
            // 
            this.tileNavSubItem_TabelaPrecoProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement9.Text = "Tabelas de Preço por Produto";
            this.tileNavSubItem_TabelaPrecoProduto.Tile.Elements.Add(tileItemElement9);
            this.tileNavSubItem_TabelaPrecoProduto.Tile.Name = "tileBarItem12";
            // 
            // tileNavSubItem_CategoriaProduto
            // 
            this.tileNavSubItem_CategoriaProduto.Caption = "Categorias de Produtos";
            this.tileNavSubItem_CategoriaProduto.Name = "tileNavSubItem_CategoriaProduto";
            // 
            // 
            // 
            this.tileNavSubItem_CategoriaProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement10.Text = "Categorias de Produtos";
            this.tileNavSubItem_CategoriaProduto.Tile.Elements.Add(tileItemElement10);
            this.tileNavSubItem_CategoriaProduto.Tile.Name = "tileBarItem13";
            // 
            // tileNavSubItem_Pedido
            // 
            this.tileNavSubItem_Pedido.Caption = "Pedido";
            this.tileNavSubItem_Pedido.Name = "tileNavSubItem_Pedido";
            // 
            // 
            // 
            this.tileNavSubItem_Pedido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement11.Text = "Pedido";
            this.tileNavSubItem_Pedido.Tile.Elements.Add(tileItemElement11);
            this.tileNavSubItem_Pedido.Tile.Name = "tileBarItem17";
            // 
            // tileNavSubItem_CamposExtraPedido
            // 
            this.tileNavSubItem_CamposExtraPedido.Caption = "Campos Extras do Pedido";
            this.tileNavSubItem_CamposExtraPedido.Name = "tileNavSubItem_CamposExtraPedido";
            // 
            // 
            // 
            this.tileNavSubItem_CamposExtraPedido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement12.Text = "Campos Extras do Pedido";
            this.tileNavSubItem_CamposExtraPedido.Tile.Elements.Add(tileItemElement12);
            this.tileNavSubItem_CamposExtraPedido.Tile.Name = "tileBarItem18";
            // 
            // tileNavSubItem_Transportadora
            // 
            this.tileNavSubItem_Transportadora.Caption = "Transportadoras";
            this.tileNavSubItem_Transportadora.Name = "tileNavSubItem_Transportadora";
            // 
            // 
            // 
            this.tileNavSubItem_Transportadora.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement13.Text = "Transportadoras";
            this.tileNavSubItem_Transportadora.Tile.Elements.Add(tileItemElement13);
            this.tileNavSubItem_Transportadora.Tile.Name = "tileBarItem14";
            // 
            // tileNavSubItem_CondicaoPagamento
            // 
            this.tileNavSubItem_CondicaoPagamento.Caption = "Condições de Pagamento";
            this.tileNavSubItem_CondicaoPagamento.Name = "tileNavSubItem_CondicaoPagamento";
            // 
            // 
            // 
            this.tileNavSubItem_CondicaoPagamento.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement14.Text = "Condições de Pagamento";
            this.tileNavSubItem_CondicaoPagamento.Tile.Elements.Add(tileItemElement14);
            this.tileNavSubItem_CondicaoPagamento.Tile.Name = "tileBarItem15";
            // 
            // tileNavSubItem_FormaPagamento
            // 
            this.tileNavSubItem_FormaPagamento.Caption = "Formas de Pagamento";
            this.tileNavSubItem_FormaPagamento.Name = "tileNavSubItem_FormaPagamento";
            // 
            // 
            // 
            this.tileNavSubItem_FormaPagamento.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement15.Text = "Formas de Pagamento";
            this.tileNavSubItem_FormaPagamento.Tile.Elements.Add(tileItemElement15);
            this.tileNavSubItem_FormaPagamento.Tile.Name = "tileBarItem16";
            // 
            // tileNavSubItem_FaturamentoPedido
            // 
            this.tileNavSubItem_FaturamentoPedido.Caption = "Faturamento de Pedido";
            this.tileNavSubItem_FaturamentoPedido.Name = "tileNavSubItem_FaturamentoPedido";
            // 
            // 
            // 
            this.tileNavSubItem_FaturamentoPedido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement16.Text = "Faturamento de Pedido";
            this.tileNavSubItem_FaturamentoPedido.Tile.Elements.Add(tileItemElement16);
            this.tileNavSubItem_FaturamentoPedido.Tile.Name = "tileBarItem19";
            // 
            // tileNavSubItem_TituloVencido
            // 
            this.tileNavSubItem_TituloVencido.Caption = "Títulos Vencidos";
            this.tileNavSubItem_TituloVencido.Name = "tileNavSubItem_TituloVencido";
            // 
            // 
            // 
            this.tileNavSubItem_TituloVencido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement17.Text = "Títulos Vencidos";
            this.tileNavSubItem_TituloVencido.Tile.Elements.Add(tileItemElement17);
            this.tileNavSubItem_TituloVencido.Tile.Name = "tileBarItem20";
            // 
            // splashScreenManager_ProdCat
            // 
            this.splashScreenManager_ProdCat.ClosingDelay = 500;
            // 
            // tabNavigationPage_RegisterProd
            // 
            this.tabNavigationPage_RegisterProd.Caption = "Cadastro";
            this.tabNavigationPage_RegisterProd.Controls.Add(this.panelControl_RegisterClientes);
            this.tabNavigationPage_RegisterProd.Image = ((System.Drawing.Image)(resources.GetObject("tabNavigationPage_RegisterProd.Image")));
            this.tabNavigationPage_RegisterProd.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage_RegisterProd.Name = "tabNavigationPage_RegisterProd";
            this.tabNavigationPage_RegisterProd.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage_RegisterProd.Size = new System.Drawing.Size(1246, 404);
            // 
            // panelControl_RegisterClientes
            // 
            this.panelControl_RegisterClientes.Controls.Add(this.layoutControl_Prod);
            this.panelControl_RegisterClientes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl_RegisterClientes.Location = new System.Drawing.Point(0, 0);
            this.panelControl_RegisterClientes.Name = "panelControl_RegisterClientes";
            this.panelControl_RegisterClientes.Size = new System.Drawing.Size(1246, 404);
            this.panelControl_RegisterClientes.TabIndex = 0;
            // 
            // simpleLabelItem_RegisterClientes
            // 
            this.simpleLabelItem_RegisterClientes.AllowHotTrack = false;
            this.simpleLabelItem_RegisterClientes.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem_RegisterClientes.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem_RegisterClientes.Location = new System.Drawing.Point(10, 10);
            this.simpleLabelItem_RegisterClientes.Name = "simpleLabelItem_RegisterClientes";
            this.simpleLabelItem_RegisterClientes.OptionsPrint.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem_RegisterClientes.OptionsPrint.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem_RegisterClientes.Size = new System.Drawing.Size(1185, 17);
            this.simpleLabelItem_RegisterClientes.Text = "Cadastro Clientes";
            this.simpleLabelItem_RegisterClientes.TextSize = new System.Drawing.Size(155, 13);
            // 
            // tabNavigationPage_DataGridProd
            // 
            this.tabNavigationPage_DataGridProd.Caption = "Produtos";
            this.tabNavigationPage_DataGridProd.Controls.Add(this.gridControl_Prod);
            this.tabNavigationPage_DataGridProd.Image = ((System.Drawing.Image)(resources.GetObject("tabNavigationPage_DataGridProd.Image")));
            this.tabNavigationPage_DataGridProd.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage_DataGridProd.Name = "tabNavigationPage_DataGridProd";
            this.tabNavigationPage_DataGridProd.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage_DataGridProd.Size = new System.Drawing.Size(1246, 404);
            // 
            // gridControl_Prod
            // 
            this.gridControl_Prod.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl_Prod.Location = new System.Drawing.Point(0, 0);
            this.gridControl_Prod.MainView = this.gridView_Prod;
            this.gridControl_Prod.MenuManager = this.barManager_ContasMP;
            this.gridControl_Prod.Name = "gridControl_Prod";
            this.gridControl_Prod.Size = new System.Drawing.Size(1246, 404);
            this.gridControl_Prod.TabIndex = 0;
            this.gridControl_Prod.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView_Prod});
            // 
            // gridView_Prod
            // 
            this.gridView_Prod.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colid,
            this.colcodigo,
            this.colnome,
            this.colcomissao,
            this.colpreco_tabela,
            this.colpreco_minimo,
            this.coltipo_ipi,
            this.colipi,
            this.colst,
            this.colmoeda,
            this.colunidade,
            this.colsaldo_estoque,
            this.colobservacoes,
            this.colultima_alteracao,
            this.colexcluido,
            this.colativo,
            this.colcategoria_id,
            this.colcodigo_ncm,
            this.colmultiplo,
            this.colpeso_bruto,
            this.colmptran});
            this.gridView_Prod.GridControl = this.gridControl_Prod;
            this.gridView_Prod.Name = "gridView_Prod";
            this.gridView_Prod.OptionsBehavior.Editable = false;
            this.gridView_Prod.OptionsBehavior.ReadOnly = true;
            this.gridView_Prod.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView_Prod.OptionsSelection.MultiSelect = true;
            this.gridView_Prod.OptionsSelection.UseIndicatorForSelection = false;
            this.gridView_Prod.OptionsView.ShowGroupPanel = false;
            this.gridView_Prod.OptionsView.ShowIndicator = false;
            this.gridView_Prod.DoubleClick += new System.EventHandler(this.gridView_Clientes_DoubleClick);
            // 
            // colid
            // 
            this.colid.Caption = "ID";
            this.colid.FieldName = "id";
            this.colid.Name = "colid";
            this.colid.Visible = true;
            this.colid.VisibleIndex = 0;
            // 
            // colcodigo
            // 
            this.colcodigo.Caption = "Código";
            this.colcodigo.FieldName = "codigo";
            this.colcodigo.Name = "colcodigo";
            this.colcodigo.Visible = true;
            this.colcodigo.VisibleIndex = 1;
            // 
            // colnome
            // 
            this.colnome.Caption = "Nome";
            this.colnome.FieldName = "nome";
            this.colnome.Name = "colnome";
            this.colnome.Visible = true;
            this.colnome.VisibleIndex = 2;
            // 
            // colcomissao
            // 
            this.colcomissao.Caption = "Comissão";
            this.colcomissao.FieldName = "comissao";
            this.colcomissao.Name = "colcomissao";
            this.colcomissao.Visible = true;
            this.colcomissao.VisibleIndex = 3;
            // 
            // colpreco_tabela
            // 
            this.colpreco_tabela.Caption = "Preço Tabela";
            this.colpreco_tabela.FieldName = "preco_tabela";
            this.colpreco_tabela.Name = "colpreco_tabela";
            this.colpreco_tabela.Visible = true;
            this.colpreco_tabela.VisibleIndex = 4;
            // 
            // colpreco_minimo
            // 
            this.colpreco_minimo.Caption = "Preço Mínimo";
            this.colpreco_minimo.FieldName = "preco_minimo";
            this.colpreco_minimo.Name = "colpreco_minimo";
            this.colpreco_minimo.Visible = true;
            this.colpreco_minimo.VisibleIndex = 5;
            // 
            // colipi
            // 
            this.colipi.Caption = "IPI";
            this.colipi.FieldName = "ipi";
            this.colipi.Name = "colipi";
            this.colipi.Visible = true;
            this.colipi.VisibleIndex = 6;
            // 
            // coltipo_ipi
            // 
            this.coltipo_ipi.Caption = "Tipo IPI";
            this.coltipo_ipi.FieldName = "tipo_ipi";
            this.coltipo_ipi.Name = "coltipo_ipi";
            this.coltipo_ipi.Visible = true;
            this.coltipo_ipi.VisibleIndex = 7;
            // 
            // colst
            // 
            this.colst.Caption = "ST";
            this.colst.FieldName = "st";
            this.colst.Name = "colst";
            this.colst.Visible = true;
            this.colst.VisibleIndex = 8;
            // 
            // colmoeda
            // 
            this.colmoeda.Caption = "Moeda";
            this.colmoeda.FieldName = "moeda";
            this.colmoeda.Name = "colmoeda";
            this.colmoeda.Visible = true;
            this.colmoeda.VisibleIndex = 9;
            // 
            // colunidade
            // 
            this.colunidade.Caption = "Unidade";
            this.colunidade.FieldName = "unidade";
            this.colunidade.Name = "colunidade";
            this.colunidade.Visible = true;
            this.colunidade.VisibleIndex = 10;
            // 
            // colsaldo_estoque
            // 
            this.colsaldo_estoque.Caption = "Saldo Estoque";
            this.colsaldo_estoque.FieldName = "saldo_estoque";
            this.colsaldo_estoque.Name = "colsaldo_estoque";
            this.colsaldo_estoque.Visible = true;
            this.colsaldo_estoque.VisibleIndex = 11;
            // 
            // colobservacoes
            // 
            this.colobservacoes.Caption = "Observações";
            this.colobservacoes.FieldName = "observacoes";
            this.colobservacoes.Name = "colobservacoes";
            this.colobservacoes.Visible = true;
            this.colobservacoes.VisibleIndex = 12;
            // 
            // colultima_alteracao
            // 
            this.colultima_alteracao.Caption = "Última Alteração";
            this.colultima_alteracao.FieldName = "ultima_alteracao";
            this.colultima_alteracao.Name = "colultima_alteracao";
            this.colultima_alteracao.Visible = true;
            this.colultima_alteracao.VisibleIndex = 13;
            // 
            // colexcluido
            // 
            this.colexcluido.Caption = "Excluído";
            this.colexcluido.FieldName = "excluido";
            this.colexcluido.Name = "colexcluido";
            this.colexcluido.Visible = true;
            this.colexcluido.VisibleIndex = 14;
            // 
            // colativo
            // 
            this.colativo.Caption = "Ativo";
            this.colativo.FieldName = "ativo";
            this.colativo.Name = "colativo";
            this.colativo.Visible = true;
            this.colativo.VisibleIndex = 15;
            // 
            // colcategoria_id
            // 
            this.colcategoria_id.Caption = "ID Categoria";
            this.colcategoria_id.FieldName = "categoria_id";
            this.colcategoria_id.Name = "colcategoria_id";
            this.colcategoria_id.Visible = true;
            this.colcategoria_id.VisibleIndex = 16;
            // 
            // colcodigo_ncm
            // 
            this.colcodigo_ncm.Caption = "Código NCM";
            this.colcodigo_ncm.FieldName = "codigo_ncm";
            this.colcodigo_ncm.Name = "colcodigo_ncm";
            this.colcodigo_ncm.Visible = true;
            this.colcodigo_ncm.VisibleIndex = 17;
            // 
            // colmultiplo
            // 
            this.colmultiplo.Caption = "Múltiplo";
            this.colmultiplo.FieldName = "multiplo";
            this.colmultiplo.Name = "colmultiplo";
            this.colmultiplo.Visible = true;
            this.colmultiplo.VisibleIndex = 18;
            // 
            // colpeso_bruto
            // 
            this.colpeso_bruto.Caption = "Peso Bruto";
            this.colpeso_bruto.FieldName = "peso_bruto";
            this.colpeso_bruto.Name = "colpeso_bruto";
            this.colpeso_bruto.Visible = true;
            this.colpeso_bruto.VisibleIndex = 19;
            // 
            // colmptran
            // 
            this.colmptran.Caption = "MP Tran";
            this.colmptran.FieldName = "mptran";
            this.colmptran.Name = "colmptran";
            this.colmptran.Visible = true;
            this.colmptran.VisibleIndex = 20;
            // 
            // tabPane_ConfigProd
            // 
            this.tabPane_ConfigProd.Controls.Add(this.tabNavigationPage_DataGridProd);
            this.tabPane_ConfigProd.Controls.Add(this.tabNavigationPage_RegisterProd);
            this.tabPane_ConfigProd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPane_ConfigProd.Location = new System.Drawing.Point(0, 94);
            this.tabPane_ConfigProd.Name = "tabPane_ConfigProd";
            this.tabPane_ConfigProd.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.tabNavigationPage_DataGridProd,
            this.tabNavigationPage_RegisterProd});
            this.tabPane_ConfigProd.RegularSize = new System.Drawing.Size(1264, 468);
            this.tabPane_ConfigProd.SelectedPage = this.tabNavigationPage_RegisterProd;
            this.tabPane_ConfigProd.Size = new System.Drawing.Size(1264, 468);
            this.tabPane_ConfigProd.TabIndex = 8;
            this.tabPane_ConfigProd.Text = "Produtos";
            // 
            // tabPane1
            // 
            this.tabPane1.Controls.Add(this.tabNavigationPage1);
            this.tabPane1.Controls.Add(this.tabNavigationPage2);
            this.tabPane1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPane1.Location = new System.Drawing.Point(0, 94);
            this.tabPane1.Name = "tabPane1";
            this.tabPane1.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.tabNavigationPage1,
            this.tabNavigationPage2});
            this.tabPane1.RegularSize = new System.Drawing.Size(1264, 445);
            this.tabPane1.SelectedPage = this.tabNavigationPage2;
            this.tabPane1.Size = new System.Drawing.Size(1264, 445);
            this.tabPane1.TabIndex = 8;
            this.tabPane1.Text = "Clientes";
            // 
            // tabNavigationPage1
            // 
            this.tabNavigationPage1.Caption = "Clientes";
            this.tabNavigationPage1.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage1.Name = "tabNavigationPage1";
            this.tabNavigationPage1.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage1.Size = new System.Drawing.Size(1246, 381);
            // 
            // tabNavigationPage2
            // 
            this.tabNavigationPage2.Caption = "Cadastro";
            this.tabNavigationPage2.Controls.Add(this.panelControl1);
            this.tabNavigationPage2.Image = ((System.Drawing.Image)(resources.GetObject("tabNavigationPage2.Image")));
            this.tabNavigationPage2.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage2.Name = "tabNavigationPage2";
            this.tabNavigationPage2.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage2.Size = new System.Drawing.Size(1246, 381);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.layoutControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1246, 381);
            this.panelControl1.TabIndex = 0;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.textEdit2);
            this.layoutControl1.Controls.Add(this.textEdit3);
            this.layoutControl1.Controls.Add(this.textEdit4);
            this.layoutControl1.Controls.Add(this.textEdit5);
            this.layoutControl1.Controls.Add(this.textEdit6);
            this.layoutControl1.Controls.Add(this.textEdit7);
            this.layoutControl1.Controls.Add(this.textEdit8);
            this.layoutControl1.Controls.Add(this.textEdit9);
            this.layoutControl1.Controls.Add(this.textEdit10);
            this.layoutControl1.Controls.Add(this.textEdit11);
            this.layoutControl1.Controls.Add(this.textEdit12);
            this.layoutControl1.Controls.Add(this.textEdit13);
            this.layoutControl1.Controls.Add(this.textEdit14);
            this.layoutControl1.Controls.Add(this.dateEdit1);
            this.layoutControl1.Controls.Add(this.toggleSwitch1);
            this.layoutControl1.Controls.Add(this.memoEdit1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup3;
            this.layoutControl1.Size = new System.Drawing.Size(1242, 377);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textEdit2
            // 
            this.textEdit2.Location = new System.Drawing.Point(136, 83);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Size = new System.Drawing.Size(1072, 20);
            this.textEdit2.StyleController = this.layoutControl1;
            this.textEdit2.TabIndex = 4;
            this.textEdit2.Visible = false;
            // 
            // textEdit3
            // 
            this.textEdit3.Location = new System.Drawing.Point(136, 107);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Size = new System.Drawing.Size(1072, 20);
            this.textEdit3.StyleController = this.layoutControl1;
            this.textEdit3.TabIndex = 5;
            this.textEdit3.Visible = false;
            // 
            // textEdit4
            // 
            this.textEdit4.Location = new System.Drawing.Point(136, 131);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Properties.MaxLength = 1;
            this.textEdit4.Size = new System.Drawing.Size(1072, 20);
            this.textEdit4.StyleController = this.layoutControl1;
            this.textEdit4.TabIndex = 6;
            this.textEdit4.Visible = false;
            // 
            // textEdit5
            // 
            this.textEdit5.Location = new System.Drawing.Point(136, 155);
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Size = new System.Drawing.Size(1072, 20);
            this.textEdit5.StyleController = this.layoutControl1;
            this.textEdit5.TabIndex = 7;
            this.textEdit5.Visible = false;
            // 
            // textEdit6
            // 
            this.textEdit6.Location = new System.Drawing.Point(136, 179);
            this.textEdit6.Name = "textEdit6";
            this.textEdit6.Size = new System.Drawing.Size(1072, 20);
            this.textEdit6.StyleController = this.layoutControl1;
            this.textEdit6.TabIndex = 8;
            this.textEdit6.Visible = false;
            // 
            // textEdit7
            // 
            this.textEdit7.Location = new System.Drawing.Point(136, 203);
            this.textEdit7.Name = "textEdit7";
            this.textEdit7.Size = new System.Drawing.Size(1072, 20);
            this.textEdit7.StyleController = this.layoutControl1;
            this.textEdit7.TabIndex = 9;
            this.textEdit7.Visible = false;
            // 
            // textEdit8
            // 
            this.textEdit8.Location = new System.Drawing.Point(136, 303);
            this.textEdit8.Name = "textEdit8";
            this.textEdit8.Size = new System.Drawing.Size(1072, 20);
            this.textEdit8.StyleController = this.layoutControl1;
            this.textEdit8.TabIndex = 10;
            // 
            // textEdit9
            // 
            this.textEdit9.Location = new System.Drawing.Point(136, 327);
            this.textEdit9.Name = "textEdit9";
            this.textEdit9.Size = new System.Drawing.Size(1072, 20);
            this.textEdit9.StyleController = this.layoutControl1;
            this.textEdit9.TabIndex = 11;
            // 
            // textEdit10
            // 
            this.textEdit10.Location = new System.Drawing.Point(136, 351);
            this.textEdit10.Name = "textEdit10";
            this.textEdit10.Size = new System.Drawing.Size(1072, 20);
            this.textEdit10.StyleController = this.layoutControl1;
            this.textEdit10.TabIndex = 12;
            // 
            // textEdit11
            // 
            this.textEdit11.Location = new System.Drawing.Point(136, 375);
            this.textEdit11.Name = "textEdit11";
            this.textEdit11.Size = new System.Drawing.Size(1072, 20);
            this.textEdit11.StyleController = this.layoutControl1;
            this.textEdit11.TabIndex = 13;
            // 
            // textEdit12
            // 
            this.textEdit12.Location = new System.Drawing.Point(136, 399);
            this.textEdit12.Name = "textEdit12";
            this.textEdit12.Size = new System.Drawing.Size(1072, 20);
            this.textEdit12.StyleController = this.layoutControl1;
            this.textEdit12.TabIndex = 14;
            // 
            // textEdit13
            // 
            this.textEdit13.Location = new System.Drawing.Point(136, 423);
            this.textEdit13.Name = "textEdit13";
            this.textEdit13.Size = new System.Drawing.Size(1072, 20);
            this.textEdit13.StyleController = this.layoutControl1;
            this.textEdit13.TabIndex = 15;
            // 
            // textEdit14
            // 
            this.textEdit14.Location = new System.Drawing.Point(136, 227);
            this.textEdit14.Name = "textEdit14";
            this.textEdit14.Size = new System.Drawing.Size(1072, 20);
            this.textEdit14.StyleController = this.layoutControl1;
            this.textEdit14.TabIndex = 16;
            this.textEdit14.Visible = false;
            // 
            // dateEdit1
            // 
            this.dateEdit1.EditValue = null;
            this.dateEdit1.Location = new System.Drawing.Point(981, 499);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.ReadOnly = true;
            this.dateEdit1.Size = new System.Drawing.Size(227, 20);
            this.dateEdit1.StyleController = this.layoutControl1;
            this.dateEdit1.TabIndex = 17;
            // 
            // toggleSwitch1
            // 
            this.toggleSwitch1.Location = new System.Drawing.Point(981, 523);
            this.toggleSwitch1.Name = "toggleSwitch1";
            this.toggleSwitch1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.toggleSwitch1.Properties.OffText = "Não";
            this.toggleSwitch1.Properties.OnText = "Sim";
            this.toggleSwitch1.Size = new System.Drawing.Size(227, 24);
            this.toggleSwitch1.StyleController = this.layoutControl1;
            this.toggleSwitch1.TabIndex = 10;
            // 
            // memoEdit1
            // 
            this.memoEdit1.Location = new System.Drawing.Point(136, 499);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(729, 48);
            this.memoEdit1.StyleController = this.layoutControl1;
            this.memoEdit1.TabIndex = 18;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem6,
            this.emptySpaceItem8,
            this.emptySpaceItem11,
            this.emptySpaceItem12,
            this.simpleLabelItem1,
            this.emptySpaceItem13,
            this.emptySpaceItem14,
            this.layoutControlGroup4,
            this.emptySpaceItem15,
            this.layoutControlGroup5,
            this.tabbedControlGroup3});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "Root";
            this.layoutControlGroup3.Size = new System.Drawing.Size(1242, 581);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(10, 0);
            this.emptySpaceItem6.Name = "emptySpaceItem1";
            this.emptySpaceItem6.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.Location = new System.Drawing.Point(10, 251);
            this.emptySpaceItem8.Name = "emptySpaceItem2";
            this.emptySpaceItem8.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem11.Name = "emptySpaceItem3";
            this.emptySpaceItem11.Size = new System.Drawing.Size(10, 561);
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.Location = new System.Drawing.Point(1212, 0);
            this.emptySpaceItem12.Name = "emptySpaceItem4";
            this.emptySpaceItem12.Size = new System.Drawing.Size(10, 561);
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleLabelItem1
            // 
            this.simpleLabelItem1.AllowHotTrack = false;
            this.simpleLabelItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem1.Location = new System.Drawing.Point(10, 10);
            this.simpleLabelItem1.Name = "simpleLabelItem_RegisterClientes";
            this.simpleLabelItem1.OptionsPrint.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem1.OptionsPrint.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem1.Size = new System.Drawing.Size(1202, 17);
            this.simpleLabelItem1.Text = "Cadastro Clientes";
            this.simpleLabelItem1.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.Location = new System.Drawing.Point(10, 27);
            this.emptySpaceItem13.Name = "emptySpaceItem5";
            this.emptySpaceItem13.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.Location = new System.Drawing.Point(10, 447);
            this.emptySpaceItem14.Name = "emptySpaceItem7";
            this.emptySpaceItem14.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9});
            this.layoutControlGroup4.Location = new System.Drawing.Point(10, 261);
            this.layoutControlGroup4.Name = "layoutControlGroup_Endereço";
            this.layoutControlGroup4.Size = new System.Drawing.Size(1202, 186);
            this.layoutControlGroup4.Text = "Endereço";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.textEdit8;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem_CLIRua";
            this.layoutControlItem4.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem4.Text = "Rua";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.textEdit9;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem5.Name = "layoutControlItem_CLIComplemento";
            this.layoutControlItem5.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem5.Text = "Complemento";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.textEdit10;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem6.Name = "layoutControlItem_CLICep";
            this.layoutControlItem6.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem6.Text = "CEP";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.textEdit11;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem7.Name = "layoutControlItem_Bairro";
            this.layoutControlItem7.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem7.Text = "Bairro";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.textEdit12;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem8.Name = "layoutControlItem_Cidade";
            this.layoutControlItem8.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem8.Text = "Cidade";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.textEdit13;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem9.Name = "layoutControlItem_CLIEstado";
            this.layoutControlItem9.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem9.Text = "Estado";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.Location = new System.Drawing.Point(10, 551);
            this.emptySpaceItem15.Name = "emptySpaceItem9";
            this.emptySpaceItem15.Size = new System.Drawing.Size(1202, 10);
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.emptySpaceItem16,
            this.layoutControlItem12});
            this.layoutControlGroup5.Location = new System.Drawing.Point(10, 457);
            this.layoutControlGroup5.Name = "layoutControlGroup1";
            this.layoutControlGroup5.Size = new System.Drawing.Size(1202, 94);
            this.layoutControlGroup5.Text = "Status";
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.dateEdit1;
            this.layoutControlItem10.Location = new System.Drawing.Point(845, 0);
            this.layoutControlItem10.Name = "layoutControlItem_CLIUltimaAlteracao";
            this.layoutControlItem10.Size = new System.Drawing.Size(333, 24);
            this.layoutControlItem10.Text = "Última Alteração";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.toggleSwitch1;
            this.layoutControlItem11.CustomizationFormText = "Status";
            this.layoutControlItem11.Location = new System.Drawing.Point(845, 24);
            this.layoutControlItem11.Name = "layoutControlItem_CLIExcluido";
            this.layoutControlItem11.Size = new System.Drawing.Size(333, 28);
            this.layoutControlItem11.Text = "Excluído";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem16
            // 
            this.emptySpaceItem16.AllowHotTrack = false;
            this.emptySpaceItem16.Location = new System.Drawing.Point(835, 0);
            this.emptySpaceItem16.Name = "emptySpaceItem10";
            this.emptySpaceItem16.Size = new System.Drawing.Size(10, 52);
            this.emptySpaceItem16.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.memoEdit1;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem12.Name = "layoutControlItem_CLIObservacao";
            this.layoutControlItem12.Size = new System.Drawing.Size(835, 52);
            this.layoutControlItem12.Text = "Observação";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(99, 13);
            // 
            // tabbedControlGroup3
            // 
            this.tabbedControlGroup3.Location = new System.Drawing.Point(10, 37);
            this.tabbedControlGroup3.Name = "tabbedControlGroup1";
            this.tabbedControlGroup3.SelectedTabPage = this.layoutControlGroup6;
            this.tabbedControlGroup3.SelectedTabPageIndex = 1;
            this.tabbedControlGroup3.Size = new System.Drawing.Size(1202, 214);
            this.tabbedControlGroup3.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup9,
            this.layoutControlGroup6});
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup4});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup_CLIContato";
            this.layoutControlGroup6.Size = new System.Drawing.Size(1178, 168);
            this.layoutControlGroup6.Text = "Contatos";
            // 
            // tabbedControlGroup4
            // 
            this.tabbedControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup4.Name = "tabbedControlGroup2";
            this.tabbedControlGroup4.SelectedTabPage = this.layoutControlGroup7;
            this.tabbedControlGroup4.SelectedTabPageIndex = 0;
            this.tabbedControlGroup4.Size = new System.Drawing.Size(1178, 168);
            this.tabbedControlGroup4.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup7,
            this.layoutControlGroup8});
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleSeparator2});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup_CLITelefone";
            this.layoutControlGroup7.Size = new System.Drawing.Size(1154, 122);
            this.layoutControlGroup7.Text = "Telefone";
            // 
            // simpleSeparator2
            // 
            this.simpleSeparator2.AllowHotTrack = false;
            this.simpleSeparator2.Location = new System.Drawing.Point(0, 0);
            this.simpleSeparator2.Name = "simpleSeparator1";
            this.simpleSeparator2.Size = new System.Drawing.Size(1154, 122);
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup8.Name = "layoutControlGroup_CLIEmail";
            this.layoutControlGroup8.Size = new System.Drawing.Size(1154, 122);
            this.layoutControlGroup8.Text = "E-mail";
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.layoutControlItem23});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup9.Name = "layoutControlGroup_CLIDados";
            this.layoutControlGroup9.Size = new System.Drawing.Size(1178, 168);
            this.layoutControlGroup9.Text = "Dados";
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.textEdit2;
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem17.Name = "layoutControlItem_CLIRazaoSocial";
            this.layoutControlItem17.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem17.Text = "Razão Social";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.textEdit3;
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem18.Name = "layoutControlItem_CLINomeFantasia";
            this.layoutControlItem18.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem18.Text = "Nome Fantasia";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.textEdit4;
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem19.Name = "layoutControlItem_CLITipo";
            this.layoutControlItem19.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem19.Text = "Tipo";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.textEdit5;
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem20.Name = "layoutControlItem_CLICnpj";
            this.layoutControlItem20.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem20.Text = "CNPJ";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.textEdit6;
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem21.Name = "layoutControlItem_CLIInscricaoEstadual";
            this.layoutControlItem21.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem21.Text = "Inscrição Estadual";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.textEdit7;
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem22.Name = "layoutControlItem_CLISuframa";
            this.layoutControlItem22.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem22.Text = "Suframa";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.textEdit14;
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItem23.Name = "layoutControlItem_CLINomeExcFiscal";
            this.layoutControlItem23.Size = new System.Drawing.Size(1178, 24);
            this.layoutControlItem23.Text = "Nome Exceção Fiscal";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(99, 13);
            // 
            // simpleSeparator3
            // 
            this.simpleSeparator3.AllowHotTrack = false;
            this.simpleSeparator3.CustomizationFormText = "simpleSeparator1";
            this.simpleSeparator3.Location = new System.Drawing.Point(1005, 0);
            this.simpleSeparator3.Name = "simpleSeparator3";
            this.simpleSeparator3.Size = new System.Drawing.Size(2, 26);
            this.simpleSeparator3.Text = "simpleSeparator1";
            // 
            // layoutControlGroup_CLITelefone1
            // 
            this.layoutControlGroup_CLITelefone1.CustomizationFormText = "Telefone";
            this.layoutControlGroup_CLITelefone1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleSeparator3});
            this.layoutControlGroup_CLITelefone1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup_CLITelefone1.Name = "layoutControlGroup_CLITelefone1";
            this.layoutControlGroup_CLITelefone1.Size = new System.Drawing.Size(1205, 31);
            this.layoutControlGroup_CLITelefone1.Text = "Telefone";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup_CLITelefone1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 561);
            this.layoutControlGroup2.Name = "LayoutRootGroupForRestore";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1205, 31);
            this.layoutControlGroup2.Tag = "LayoutRootGroupForRestore";
            // 
            // colid1
            // 
            this.colid1.FieldName = "id";
            this.colid1.Name = "colid1";
            // 
            // colrazao_social
            // 
            this.colrazao_social.FieldName = "razao_social";
            this.colrazao_social.Name = "colrazao_social";
            // 
            // colnome_fantasia
            // 
            this.colnome_fantasia.FieldName = "nome_fantasia";
            this.colnome_fantasia.Name = "colnome_fantasia";
            // 
            // layoutControlGroup_CatProd
            // 
            this.layoutControlGroup_CatProd.Location = new System.Drawing.Point(10, 37);
            this.layoutControlGroup_CatProd.Name = "layoutControlGroup_CatProd";
            this.layoutControlGroup_CatProd.Size = new System.Drawing.Size(1202, 218);
            // 
            // produtoCategoriaTableAdapter
            // 
            this.produtoCategoriaTableAdapter.ClearBeforeFill = true;
            // 
            // FormProduto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1264, 562);
            this.Controls.Add(this.tabPane_ConfigProd);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormProduto";
            this.ShowMdiChildCaptionInParentTitle = true;
            this.Text = "Produtos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormProduto_Load);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ProdGradeCores.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager_ContasMP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_Prod)).EndInit();
            this.layoutControl_Prod.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ProdNome.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_ProdUltAlteracao.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_ProdUltAlteracao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ProdExcluido.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ProdCod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ProdPrecoTab.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ProdPrecoMin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ProdMult.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ProdPesoBruto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ProdIpi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ProdCodNCM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ProdComissao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_ProdObservacao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ProdGradeTamanhos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit_ProdTipoIpi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ProdUnidade.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit_ProdMoeda.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit_ProdCat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_RegisterProd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem_RegisterProdCat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_Prod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdNome)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdCod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdCat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdPrecoTab)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdPrecoMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdUnidade)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdMult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdIpi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdTipoIpi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdComissao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem_ProdComissaoPercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdMoeda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdPesoBruto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem_ProdPesoB_KG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdCodNCM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_ProdGrades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_GradeCores)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdGradeCores)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_GradeTamanhos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdGradeTamanhos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_ProdStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdExcluido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_DtUltAlteracao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ProdObservacao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.produtoCategoriaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meusPedidosDataSet)).EndInit();
            this.tabNavigationPage_RegisterProd.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl_RegisterClientes)).EndInit();
            this.panelControl_RegisterClientes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem_RegisterClientes)).EndInit();
            this.tabNavigationPage_DataGridProd.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_Prod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_Prod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane_ConfigProd)).EndInit();
            this.tabPane_ConfigProd.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).EndInit();
            this.tabPane1.ResumeLayout(false);
            this.tabNavigationPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toggleSwitch1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CLITelefone1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_CatProd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Cliente;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CondicaoPagamentoCliente;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CategoriasCliente;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TabelaPreco;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Usuarios;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Produto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_ImagensProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TabelaProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TabelaPrecoProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CategoriaProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Transportadora;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CondicaoPagamento;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_FormaPagamento;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Pedido;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CamposExtraPedido;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_FaturamentoPedido;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TituloVencido;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager_ProdCat;
        private DevExpress.XtraBars.Navigation.TabPane tabPane_ConfigProd;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage_DataGridProd;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage_RegisterProd;
        private DevExpress.XtraBars.BarManager barManager_ContasMP;
        private DevExpress.XtraBars.Bar bar_Acoes;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Add;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Edit;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Del;
        private DevExpress.XtraBars.Bar bar_Acoes2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Gravar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Cancelar;
        private DevExpress.XtraEditors.PanelControl panelControl_RegisterClientes;
        private DevExpress.XtraLayout.LayoutControl layoutControl_Prod;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_RegisterProd;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem_RegisterClientes;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraBars.Navigation.TabPane tabPane1;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage1;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage2;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraEditors.TextEdit textEdit3;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        private DevExpress.XtraEditors.TextEdit textEdit6;
        private DevExpress.XtraEditors.TextEdit textEdit7;
        private DevExpress.XtraEditors.TextEdit textEdit8;
        private DevExpress.XtraEditors.TextEdit textEdit9;
        private DevExpress.XtraEditors.TextEdit textEdit10;
        private DevExpress.XtraEditors.TextEdit textEdit11;
        private DevExpress.XtraEditors.TextEdit textEdit12;
        private DevExpress.XtraEditors.TextEdit textEdit13;
        private DevExpress.XtraEditors.TextEdit textEdit14;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
        private DevExpress.XtraEditors.ToggleSwitch toggleSwitch1;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_CLITelefone1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraGrid.Columns.GridColumn colid1;
        private DevExpress.XtraGrid.Columns.GridColumn colrazao_social;
        private DevExpress.XtraGrid.Columns.GridColumn colnome_fantasia;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraGrid.GridControl gridControl_Prod;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView_Prod;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_Prod;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_CatProd;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem_RegisterProdCat;
        private DevExpress.XtraEditors.TextEdit textEdit_ProdNome;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_ProdNome;
        private DevExpress.XtraEditors.DateEdit dateEdit_ProdUltAlteracao;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_DtUltAlteracao;
        private DevExpress.XtraEditors.ToggleSwitch textEdit_ProdExcluido;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_ProdExcluido;
        private DevExpress.XtraEditors.TextEdit textEdit_ProdCod;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_ProdCod;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_ProdCat;
        private DevExpress.XtraEditors.TextEdit textEdit_ProdPrecoTab;
        private DevExpress.XtraEditors.TextEdit textEdit_ProdPrecoMin;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_ProdPrecoTab;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_ProdPrecoMin;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_ProdUnidade;
        private DevExpress.XtraEditors.TextEdit textEdit_ProdMult;
        private DevExpress.XtraEditors.TextEdit textEdit_ProdPesoBruto;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_ProdMult;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_ProdPesoBruto;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem19;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem_ProdPesoB_KG;
        private DevExpress.XtraEditors.TextEdit textEdit_ProdIpi;
        private DevExpress.XtraEditors.TextEdit textEdit_ProdCodNCM;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_ProdIpi;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_ProdTipoIpi;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_ProdCodNCM;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem21;
        private DevExpress.XtraEditors.TextEdit textEdit_ProdComissao;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_ProdComissao;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem_ProdComissaoPercent;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_ProdMoeda;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem24;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem25;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem26;
        private DevExpress.XtraEditors.MemoEdit memoEdit_ProdObservacao;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_ProdObservacao;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem27;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_ProdStatus;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem28;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem29;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem17;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem18;
        private DevExpress.XtraEditors.SimpleButton simpleButton_ProdDefCorTam;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem20;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_ProdGrades;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem30;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem31;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem32;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem33;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_GradeCores;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem35;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_GradeTamanhos;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem34;
        private DevExpress.XtraEditors.TextEdit textEdit_ProdGradeTamanhos;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_ProdGradeCores;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_ProdGradeTamanhos;
        private DevExpress.XtraEditors.SimpleButton simpleButton_ProdGradeCor_Gravar;
        private DevExpress.XtraEditors.SimpleButton simpleButton_ProdGradeTamanho_Gravar;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.TextEdit textEdit_ProdGradeCores;
        private DevExpress.XtraGrid.Columns.GridColumn colid;
        private DevExpress.XtraGrid.Columns.GridColumn colcodigo;
        private DevExpress.XtraGrid.Columns.GridColumn colnome;
        private DevExpress.XtraGrid.Columns.GridColumn colcomissao;
        private DevExpress.XtraGrid.Columns.GridColumn colpreco_tabela;
        private DevExpress.XtraGrid.Columns.GridColumn colpreco_minimo;
        private DevExpress.XtraGrid.Columns.GridColumn colipi;
        private DevExpress.XtraGrid.Columns.GridColumn coltipo_ipi;
        private DevExpress.XtraGrid.Columns.GridColumn colst;
        private DevExpress.XtraGrid.Columns.GridColumn colmoeda;
        private DevExpress.XtraGrid.Columns.GridColumn colunidade;
        private DevExpress.XtraGrid.Columns.GridColumn colsaldo_estoque;
        private DevExpress.XtraGrid.Columns.GridColumn colobservacoes;
        private DevExpress.XtraGrid.Columns.GridColumn colultima_alteracao;
        private DevExpress.XtraGrid.Columns.GridColumn colexcluido;
        private DevExpress.XtraGrid.Columns.GridColumn colativo;
        private DevExpress.XtraGrid.Columns.GridColumn colcategoria_id;
        private DevExpress.XtraGrid.Columns.GridColumn colcodigo_ncm;
        private DevExpress.XtraGrid.Columns.GridColumn colmultiplo;
        private DevExpress.XtraGrid.Columns.GridColumn colpeso_bruto;
        private DevExpress.XtraGrid.Columns.GridColumn colmptran;
        private MeusPedidosDataSet meusPedidosDataSet;
        private System.Windows.Forms.BindingSource produtoCategoriaBindingSource;
        private MeusPedidosDataSetTableAdapters.ProdutoCategoriaTableAdapter produtoCategoriaTableAdapter;
        private DevExpress.Utils.Behaviors.BehaviorManager behaviorManager1;
        private DevExpress.XtraEditors.LookUpEdit comboBoxEdit_ProdTipoIpi;
        private DevExpress.XtraEditors.LookUpEdit textEdit_ProdUnidade;
        private DevExpress.XtraEditors.LookUpEdit comboBoxEdit_ProdMoeda;
        private DevExpress.XtraEditors.LookUpEdit comboBoxEdit_ProdCat;
    }
}


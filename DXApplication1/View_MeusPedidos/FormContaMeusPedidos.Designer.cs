﻿namespace View_MeusPedidos
{
    partial class FormContaMeusPedidos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement10 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement11 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement12 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement13 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement14 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement15 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement16 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement17 = new DevExpress.XtraEditors.TileItemElement();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormContaMeusPedidos));
            this.tileNavSubItem_Cliente = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CondicaoPagamentoCliente = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CategoriasCliente = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TabelaPreco = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Usuarios = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Produto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_ImagensProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TabelaProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TabelaPrecoProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CategoriaProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Pedido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CamposExtraPedido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Transportadora = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CondicaoPagamento = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_FormaPagamento = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_FaturamentoPedido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TituloVencido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.contasMeusPedidosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.splashScreenManager_ContasMP = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::View_MeusPedidos.WaitForm1), true, true);
            this.tabNavigationPage_RegisterContas = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.panelControl_RegisterContas = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl_ContasMP = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit_CMPDesc = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_CMPURL = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_CMPAppToken = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_CMPCompanyToken = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_CMPFilial = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_CMPCodigo = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_CMPStatus = new DevExpress.XtraEditors.ToggleSwitch();
            this.toggleSwitch_enviaPedidos = new DevExpress.XtraEditors.ToggleSwitch();
            this.layoutControlGroup_RegisterContas = new DevExpress.XtraLayout.LayoutControlGroup();
            this.Descricao = new DevExpress.XtraLayout.LayoutControlItem();
            this.URL = new DevExpress.XtraLayout.LayoutControlItem();
            this.AppToken = new DevExpress.XtraLayout.LayoutControlItem();
            this.CompanyToken = new DevExpress.XtraLayout.LayoutControlItem();
            this.Status = new DevExpress.XtraLayout.LayoutControlItem();
            this.Código = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.Filial = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleLabelItem1 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.EnviaPedidos = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabNavigationPage_DataGridContas = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.gridControl_ContasMP = new DevExpress.XtraGrid.GridControl();
            this.gridView_ContasMP = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colfilial = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coldescricao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colurl = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colapptoken = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colcmptoken = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tabPane_ConfigContasMeusPedidos = new DevExpress.XtraBars.Navigation.TabPane();
            this.barManager_ContasMP = new DevExpress.XtraBars.BarManager(this.components);
            this.bar_Acoes = new DevExpress.XtraBars.Bar();
            this.barButtonItem_Add = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem_Edit = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem_Del = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.bar_Acoes2 = new DevExpress.XtraBars.Bar();
            this.barButtonItem_Gravar = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem_Cancelar = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this.contasMeusPedidosBindingSource)).BeginInit();
            this.tabNavigationPage_RegisterContas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl_RegisterContas)).BeginInit();
            this.panelControl_RegisterContas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_ContasMP)).BeginInit();
            this.layoutControl_ContasMP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CMPDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CMPURL.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CMPAppToken.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CMPCompanyToken.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CMPFilial.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CMPCodigo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CMPStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toggleSwitch_enviaPedidos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_RegisterContas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descricao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.URL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AppToken)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CompanyToken)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Status)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Código)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Filial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EnviaPedidos)).BeginInit();
            this.tabNavigationPage_DataGridContas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_ContasMP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_ContasMP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane_ConfigContasMeusPedidos)).BeginInit();
            this.tabPane_ConfigContasMeusPedidos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.barManager_ContasMP)).BeginInit();
            this.SuspendLayout();
            // 
            // tileNavSubItem_Cliente
            // 
            this.tileNavSubItem_Cliente.Caption = "Cliente";
            this.tileNavSubItem_Cliente.Name = "tileNavSubItem_Cliente";
            // 
            // 
            // 
            this.tileNavSubItem_Cliente.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement1.Text = "Cliente";
            this.tileNavSubItem_Cliente.Tile.Elements.Add(tileItemElement1);
            this.tileNavSubItem_Cliente.Tile.Name = "tileBarItem3";
            // 
            // tileNavSubItem_CondicaoPagamentoCliente
            // 
            this.tileNavSubItem_CondicaoPagamentoCliente.Caption = "Condições de Pagamento por Cliente";
            this.tileNavSubItem_CondicaoPagamentoCliente.Name = "tileNavSubItem_CondicaoPagamentoCliente";
            // 
            // 
            // 
            this.tileNavSubItem_CondicaoPagamentoCliente.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement2.Text = "Condições de Pagamento por Cliente";
            this.tileNavSubItem_CondicaoPagamentoCliente.Tile.Elements.Add(tileItemElement2);
            this.tileNavSubItem_CondicaoPagamentoCliente.Tile.Name = "tileBarItem4";
            // 
            // tileNavSubItem_CategoriasCliente
            // 
            this.tileNavSubItem_CategoriasCliente.Caption = "Categorias por Cliente";
            this.tileNavSubItem_CategoriasCliente.Name = "tileNavSubItem_CategoriasCliente";
            // 
            // 
            // 
            this.tileNavSubItem_CategoriasCliente.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement3.Text = "Categorias por Cliente";
            this.tileNavSubItem_CategoriasCliente.Tile.Elements.Add(tileItemElement3);
            this.tileNavSubItem_CategoriasCliente.Tile.Name = "tileBarItem6";
            // 
            // tileNavSubItem_TabelaPreco
            // 
            this.tileNavSubItem_TabelaPreco.Caption = "Tabelas de Preço por Cliente";
            this.tileNavSubItem_TabelaPreco.Name = "tileNavSubItem_TabelaPreco";
            // 
            // 
            // 
            this.tileNavSubItem_TabelaPreco.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement4.Text = "Tabelas de Preço por Cliente";
            this.tileNavSubItem_TabelaPreco.Tile.Elements.Add(tileItemElement4);
            this.tileNavSubItem_TabelaPreco.Tile.Name = "tileBarItem7";
            // 
            // tileNavSubItem_Usuarios
            // 
            this.tileNavSubItem_Usuarios.Caption = "Usuários (Vendedores)";
            this.tileNavSubItem_Usuarios.Name = "tileNavSubItem_Usuarios";
            // 
            // 
            // 
            this.tileNavSubItem_Usuarios.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement5.Text = "Usuários (Vendedores)";
            this.tileNavSubItem_Usuarios.Tile.Elements.Add(tileItemElement5);
            this.tileNavSubItem_Usuarios.Tile.Name = "tileBarItem8";
            // 
            // tileNavSubItem_Produto
            // 
            this.tileNavSubItem_Produto.Caption = "Produto";
            this.tileNavSubItem_Produto.Name = "tileNavSubItem_Produto";
            // 
            // 
            // 
            this.tileNavSubItem_Produto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement6.Text = "Produto";
            this.tileNavSubItem_Produto.Tile.Elements.Add(tileItemElement6);
            this.tileNavSubItem_Produto.Tile.Name = "tileBarItem9";
            // 
            // tileNavSubItem_ImagensProduto
            // 
            this.tileNavSubItem_ImagensProduto.Caption = "Imagens do Produto";
            this.tileNavSubItem_ImagensProduto.Name = "tileNavSubItem_ImagensProduto";
            // 
            // 
            // 
            this.tileNavSubItem_ImagensProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement7.Text = "Imagens do Produto";
            this.tileNavSubItem_ImagensProduto.Tile.Elements.Add(tileItemElement7);
            this.tileNavSubItem_ImagensProduto.Tile.Name = "tileBarItem10";
            // 
            // tileNavSubItem_TabelaProduto
            // 
            this.tileNavSubItem_TabelaProduto.Caption = "Tabela de Preço";
            this.tileNavSubItem_TabelaProduto.Name = "tileNavSubItem_TabelaProduto";
            // 
            // 
            // 
            this.tileNavSubItem_TabelaProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement8.Text = "Tabelas de Preço";
            this.tileNavSubItem_TabelaProduto.Tile.Elements.Add(tileItemElement8);
            this.tileNavSubItem_TabelaProduto.Tile.Name = "tileBarItem11";
            // 
            // tileNavSubItem_TabelaPrecoProduto
            // 
            this.tileNavSubItem_TabelaPrecoProduto.Caption = "Tabelas de Preço por Produto";
            this.tileNavSubItem_TabelaPrecoProduto.Name = "tileNavSubItem_TabelaPrecoProduto";
            // 
            // 
            // 
            this.tileNavSubItem_TabelaPrecoProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement9.Text = "Tabelas de Preço por Produto";
            this.tileNavSubItem_TabelaPrecoProduto.Tile.Elements.Add(tileItemElement9);
            this.tileNavSubItem_TabelaPrecoProduto.Tile.Name = "tileBarItem12";
            // 
            // tileNavSubItem_CategoriaProduto
            // 
            this.tileNavSubItem_CategoriaProduto.Caption = "Categorias de Produtos";
            this.tileNavSubItem_CategoriaProduto.Name = "tileNavSubItem_CategoriaProduto";
            // 
            // 
            // 
            this.tileNavSubItem_CategoriaProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement10.Text = "Categorias de Produtos";
            this.tileNavSubItem_CategoriaProduto.Tile.Elements.Add(tileItemElement10);
            this.tileNavSubItem_CategoriaProduto.Tile.Name = "tileBarItem13";
            // 
            // tileNavSubItem_Pedido
            // 
            this.tileNavSubItem_Pedido.Caption = "Pedido";
            this.tileNavSubItem_Pedido.Name = "tileNavSubItem_Pedido";
            // 
            // 
            // 
            this.tileNavSubItem_Pedido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement11.Text = "Pedido";
            this.tileNavSubItem_Pedido.Tile.Elements.Add(tileItemElement11);
            this.tileNavSubItem_Pedido.Tile.Name = "tileBarItem17";
            // 
            // tileNavSubItem_CamposExtraPedido
            // 
            this.tileNavSubItem_CamposExtraPedido.Caption = "Campos Extras do Pedido";
            this.tileNavSubItem_CamposExtraPedido.Name = "tileNavSubItem_CamposExtraPedido";
            // 
            // 
            // 
            this.tileNavSubItem_CamposExtraPedido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement12.Text = "Campos Extras do Pedido";
            this.tileNavSubItem_CamposExtraPedido.Tile.Elements.Add(tileItemElement12);
            this.tileNavSubItem_CamposExtraPedido.Tile.Name = "tileBarItem18";
            // 
            // tileNavSubItem_Transportadora
            // 
            this.tileNavSubItem_Transportadora.Caption = "Transportadoras";
            this.tileNavSubItem_Transportadora.Name = "tileNavSubItem_Transportadora";
            // 
            // 
            // 
            this.tileNavSubItem_Transportadora.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement13.Text = "Transportadoras";
            this.tileNavSubItem_Transportadora.Tile.Elements.Add(tileItemElement13);
            this.tileNavSubItem_Transportadora.Tile.Name = "tileBarItem14";
            // 
            // tileNavSubItem_CondicaoPagamento
            // 
            this.tileNavSubItem_CondicaoPagamento.Caption = "Condições de Pagamento";
            this.tileNavSubItem_CondicaoPagamento.Name = "tileNavSubItem_CondicaoPagamento";
            // 
            // 
            // 
            this.tileNavSubItem_CondicaoPagamento.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement14.Text = "Condições de Pagamento";
            this.tileNavSubItem_CondicaoPagamento.Tile.Elements.Add(tileItemElement14);
            this.tileNavSubItem_CondicaoPagamento.Tile.Name = "tileBarItem15";
            // 
            // tileNavSubItem_FormaPagamento
            // 
            this.tileNavSubItem_FormaPagamento.Caption = "Formas de Pagamento";
            this.tileNavSubItem_FormaPagamento.Name = "tileNavSubItem_FormaPagamento";
            // 
            // 
            // 
            this.tileNavSubItem_FormaPagamento.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement15.Text = "Formas de Pagamento";
            this.tileNavSubItem_FormaPagamento.Tile.Elements.Add(tileItemElement15);
            this.tileNavSubItem_FormaPagamento.Tile.Name = "tileBarItem16";
            // 
            // tileNavSubItem_FaturamentoPedido
            // 
            this.tileNavSubItem_FaturamentoPedido.Caption = "Faturamento de Pedido";
            this.tileNavSubItem_FaturamentoPedido.Name = "tileNavSubItem_FaturamentoPedido";
            // 
            // 
            // 
            this.tileNavSubItem_FaturamentoPedido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement16.Text = "Faturamento de Pedido";
            this.tileNavSubItem_FaturamentoPedido.Tile.Elements.Add(tileItemElement16);
            this.tileNavSubItem_FaturamentoPedido.Tile.Name = "tileBarItem19";
            // 
            // tileNavSubItem_TituloVencido
            // 
            this.tileNavSubItem_TituloVencido.Caption = "Títulos Vencidos";
            this.tileNavSubItem_TituloVencido.Name = "tileNavSubItem_TituloVencido";
            // 
            // 
            // 
            this.tileNavSubItem_TituloVencido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement17.Text = "Títulos Vencidos";
            this.tileNavSubItem_TituloVencido.Tile.Elements.Add(tileItemElement17);
            this.tileNavSubItem_TituloVencido.Tile.Name = "tileBarItem20";
            // 
            // contasMeusPedidosBindingSource
            // 
            this.contasMeusPedidosBindingSource.DataSource = typeof(Core.ContaMeusPedidos);
            // 
            // splashScreenManager_ContasMP
            // 
            this.splashScreenManager_ContasMP.ClosingDelay = 500;
            // 
            // tabNavigationPage_RegisterContas
            // 
            this.tabNavigationPage_RegisterContas.Caption = "Cadastro";
            this.tabNavigationPage_RegisterContas.Controls.Add(this.panelControl_RegisterContas);
            this.tabNavigationPage_RegisterContas.Image = ((System.Drawing.Image)(resources.GetObject("tabNavigationPage_RegisterContas.Image")));
            this.tabNavigationPage_RegisterContas.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage_RegisterContas.Name = "tabNavigationPage_RegisterContas";
            this.tabNavigationPage_RegisterContas.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage_RegisterContas.Size = new System.Drawing.Size(1246, 381);
            // 
            // panelControl_RegisterContas
            // 
            this.panelControl_RegisterContas.Controls.Add(this.layoutControl_ContasMP);
            this.panelControl_RegisterContas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl_RegisterContas.Location = new System.Drawing.Point(0, 0);
            this.panelControl_RegisterContas.Name = "panelControl_RegisterContas";
            this.panelControl_RegisterContas.Size = new System.Drawing.Size(1246, 381);
            this.panelControl_RegisterContas.TabIndex = 0;
            // 
            // layoutControl_ContasMP
            // 
            this.layoutControl_ContasMP.Controls.Add(this.textEdit_CMPDesc);
            this.layoutControl_ContasMP.Controls.Add(this.textEdit_CMPURL);
            this.layoutControl_ContasMP.Controls.Add(this.textEdit_CMPAppToken);
            this.layoutControl_ContasMP.Controls.Add(this.textEdit_CMPCompanyToken);
            this.layoutControl_ContasMP.Controls.Add(this.textEdit_CMPFilial);
            this.layoutControl_ContasMP.Controls.Add(this.textEdit_CMPCodigo);
            this.layoutControl_ContasMP.Controls.Add(this.textEdit_CMPStatus);
            this.layoutControl_ContasMP.Controls.Add(this.toggleSwitch_enviaPedidos);
            this.layoutControl_ContasMP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl_ContasMP.Location = new System.Drawing.Point(2, 2);
            this.layoutControl_ContasMP.Name = "layoutControl_ContasMP";
            this.layoutControl_ContasMP.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(550, 200, 450, 400);
            this.layoutControl_ContasMP.Root = this.layoutControlGroup_RegisterContas;
            this.layoutControl_ContasMP.Size = new System.Drawing.Size(1242, 377);
            this.layoutControl_ContasMP.TabIndex = 0;
            this.layoutControl_ContasMP.Text = "layoutControl_ContasMP";
            // 
            // textEdit_CMPDesc
            // 
            this.textEdit_CMPDesc.Location = new System.Drawing.Point(339, 131);
            this.textEdit_CMPDesc.Name = "textEdit_CMPDesc";
            this.textEdit_CMPDesc.Properties.MaxLength = 60;
            this.textEdit_CMPDesc.Size = new System.Drawing.Size(605, 20);
            this.textEdit_CMPDesc.StyleController = this.layoutControl_ContasMP;
            this.textEdit_CMPDesc.TabIndex = 6;
            // 
            // textEdit_CMPURL
            // 
            this.textEdit_CMPURL.Location = new System.Drawing.Point(339, 155);
            this.textEdit_CMPURL.Name = "textEdit_CMPURL";
            this.textEdit_CMPURL.Properties.MaxLength = 200;
            this.textEdit_CMPURL.Size = new System.Drawing.Size(605, 20);
            this.textEdit_CMPURL.StyleController = this.layoutControl_ContasMP;
            this.textEdit_CMPURL.TabIndex = 7;
            // 
            // textEdit_CMPAppToken
            // 
            this.textEdit_CMPAppToken.Location = new System.Drawing.Point(339, 179);
            this.textEdit_CMPAppToken.Name = "textEdit_CMPAppToken";
            this.textEdit_CMPAppToken.Properties.MaxLength = 150;
            this.textEdit_CMPAppToken.Size = new System.Drawing.Size(605, 20);
            this.textEdit_CMPAppToken.StyleController = this.layoutControl_ContasMP;
            this.textEdit_CMPAppToken.TabIndex = 8;
            // 
            // textEdit_CMPCompanyToken
            // 
            this.textEdit_CMPCompanyToken.Location = new System.Drawing.Point(339, 203);
            this.textEdit_CMPCompanyToken.Name = "textEdit_CMPCompanyToken";
            this.textEdit_CMPCompanyToken.Properties.MaxLength = 150;
            this.textEdit_CMPCompanyToken.Size = new System.Drawing.Size(605, 20);
            this.textEdit_CMPCompanyToken.StyleController = this.layoutControl_ContasMP;
            this.textEdit_CMPCompanyToken.TabIndex = 9;
            // 
            // textEdit_CMPFilial
            // 
            this.textEdit_CMPFilial.Location = new System.Drawing.Point(339, 83);
            this.textEdit_CMPFilial.Name = "textEdit_CMPFilial";
            this.textEdit_CMPFilial.Properties.MaxLength = 2;
            this.textEdit_CMPFilial.Size = new System.Drawing.Size(605, 20);
            this.textEdit_CMPFilial.StyleController = this.layoutControl_ContasMP;
            this.textEdit_CMPFilial.TabIndex = 11;
            // 
            // textEdit_CMPCodigo
            // 
            this.textEdit_CMPCodigo.Location = new System.Drawing.Point(339, 107);
            this.textEdit_CMPCodigo.Name = "textEdit_CMPCodigo";
            this.textEdit_CMPCodigo.Properties.MaxLength = 2;
            this.textEdit_CMPCodigo.Size = new System.Drawing.Size(605, 20);
            this.textEdit_CMPCodigo.StyleController = this.layoutControl_ContasMP;
            this.textEdit_CMPCodigo.TabIndex = 12;
            // 
            // textEdit_CMPStatus
            // 
            this.textEdit_CMPStatus.Location = new System.Drawing.Point(339, 255);
            this.textEdit_CMPStatus.Name = "textEdit_CMPStatus";
            this.textEdit_CMPStatus.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.textEdit_CMPStatus.Properties.OffText = "Inativo";
            this.textEdit_CMPStatus.Properties.OnText = "Ativo";
            this.textEdit_CMPStatus.Properties.ValueOff = "0";
            this.textEdit_CMPStatus.Properties.ValueOn = "1";
            this.textEdit_CMPStatus.Size = new System.Drawing.Size(605, 24);
            this.textEdit_CMPStatus.StyleController = this.layoutControl_ContasMP;
            this.textEdit_CMPStatus.TabIndex = 10;
            // 
            // toggleSwitch_enviaPedidos
            // 
            this.toggleSwitch_enviaPedidos.Location = new System.Drawing.Point(339, 227);
            this.toggleSwitch_enviaPedidos.Name = "toggleSwitch_enviaPedidos";
            this.toggleSwitch_enviaPedidos.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.toggleSwitch_enviaPedidos.Properties.OffText = "Inativo";
            this.toggleSwitch_enviaPedidos.Properties.OnText = "Ativo";
            this.toggleSwitch_enviaPedidos.Properties.ValueOff = "0";
            this.toggleSwitch_enviaPedidos.Properties.ValueOn = "1";
            this.toggleSwitch_enviaPedidos.Size = new System.Drawing.Size(605, 24);
            this.toggleSwitch_enviaPedidos.StyleController = this.layoutControl_ContasMP;
            this.toggleSwitch_enviaPedidos.TabIndex = 10;
            // 
            // layoutControlGroup_RegisterContas
            // 
            this.layoutControlGroup_RegisterContas.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup_RegisterContas.GroupBordersVisible = false;
            this.layoutControlGroup_RegisterContas.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.Descricao,
            this.URL,
            this.AppToken,
            this.CompanyToken,
            this.Status,
            this.Código,
            this.emptySpaceItem2,
            this.Filial,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.simpleLabelItem1,
            this.emptySpaceItem1,
            this.emptySpaceItem5,
            this.EnviaPedidos});
            this.layoutControlGroup_RegisterContas.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup_RegisterContas.Name = "Root";
            this.layoutControlGroup_RegisterContas.Size = new System.Drawing.Size(1242, 377);
            this.layoutControlGroup_RegisterContas.TextVisible = false;
            // 
            // Descricao
            // 
            this.Descricao.Control = this.textEdit_CMPDesc;
            this.Descricao.Location = new System.Drawing.Point(250, 119);
            this.Descricao.Name = "Descricao";
            this.Descricao.Size = new System.Drawing.Size(686, 24);
            this.Descricao.TextSize = new System.Drawing.Size(74, 13);
            // 
            // URL
            // 
            this.URL.Control = this.textEdit_CMPURL;
            this.URL.Location = new System.Drawing.Point(250, 143);
            this.URL.Name = "URL";
            this.URL.Size = new System.Drawing.Size(686, 24);
            this.URL.TextSize = new System.Drawing.Size(74, 13);
            // 
            // AppToken
            // 
            this.AppToken.Control = this.textEdit_CMPAppToken;
            this.AppToken.Location = new System.Drawing.Point(250, 167);
            this.AppToken.Name = "AppToken";
            this.AppToken.Size = new System.Drawing.Size(686, 24);
            this.AppToken.TextSize = new System.Drawing.Size(74, 13);
            // 
            // CompanyToken
            // 
            this.CompanyToken.Control = this.textEdit_CMPCompanyToken;
            this.CompanyToken.Location = new System.Drawing.Point(250, 191);
            this.CompanyToken.Name = "CompanyToken";
            this.CompanyToken.Size = new System.Drawing.Size(686, 24);
            this.CompanyToken.TextSize = new System.Drawing.Size(74, 13);
            // 
            // Status
            // 
            this.Status.Control = this.textEdit_CMPStatus;
            this.Status.Location = new System.Drawing.Point(250, 243);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(686, 28);
            this.Status.TextSize = new System.Drawing.Size(74, 13);
            // 
            // Código
            // 
            this.Código.Control = this.textEdit_CMPCodigo;
            this.Código.Location = new System.Drawing.Point(250, 95);
            this.Código.Name = "Código";
            this.Código.Size = new System.Drawing.Size(686, 24);
            this.Código.TextSize = new System.Drawing.Size(74, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(250, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(686, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // Filial
            // 
            this.Filial.Control = this.textEdit_CMPFilial;
            this.Filial.Location = new System.Drawing.Point(250, 71);
            this.Filial.Name = "Filial";
            this.Filial.Size = new System.Drawing.Size(686, 24);
            this.Filial.TextSize = new System.Drawing.Size(74, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(250, 357);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(936, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(286, 357);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleLabelItem1
            // 
            this.simpleLabelItem1.AllowHotTrack = false;
            this.simpleLabelItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem1.CustomizationFormText = "Cadastro Conta Meus Pedidos";
            this.simpleLabelItem1.Location = new System.Drawing.Point(250, 23);
            this.simpleLabelItem1.Name = "simpleLabelItem1";
            this.simpleLabelItem1.Size = new System.Drawing.Size(686, 24);
            this.simpleLabelItem1.Text = "Cadastro Conta Meus Pedidos";
            this.simpleLabelItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.simpleLabelItem1.TextSize = new System.Drawing.Size(50, 20);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(250, 271);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(686, 86);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(250, 47);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(686, 24);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // EnviaPedidos
            // 
            this.EnviaPedidos.Control = this.toggleSwitch_enviaPedidos;
            this.EnviaPedidos.CustomizationFormText = "Status";
            this.EnviaPedidos.Location = new System.Drawing.Point(250, 215);
            this.EnviaPedidos.Name = "EnviaPedidos";
            this.EnviaPedidos.Size = new System.Drawing.Size(686, 28);
            this.EnviaPedidos.Text = "Envia Pedidos";
            this.EnviaPedidos.TextSize = new System.Drawing.Size(74, 13);
            // 
            // tabNavigationPage_DataGridContas
            // 
            this.tabNavigationPage_DataGridContas.Caption = "Contas MP";
            this.tabNavigationPage_DataGridContas.Controls.Add(this.gridControl_ContasMP);
            this.tabNavigationPage_DataGridContas.Image = ((System.Drawing.Image)(resources.GetObject("tabNavigationPage_DataGridContas.Image")));
            this.tabNavigationPage_DataGridContas.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage_DataGridContas.Name = "tabNavigationPage_DataGridContas";
            this.tabNavigationPage_DataGridContas.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage_DataGridContas.Size = new System.Drawing.Size(1246, 381);
            // 
            // gridControl_ContasMP
            // 
            this.gridControl_ContasMP.DataSource = this.contasMeusPedidosBindingSource;
            this.gridControl_ContasMP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl_ContasMP.Location = new System.Drawing.Point(0, 0);
            this.gridControl_ContasMP.MainView = this.gridView_ContasMP;
            this.gridControl_ContasMP.Name = "gridControl_ContasMP";
            this.gridControl_ContasMP.Size = new System.Drawing.Size(1246, 381);
            this.gridControl_ContasMP.TabIndex = 0;
            this.gridControl_ContasMP.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView_ContasMP});
            // 
            // gridView_ContasMP
            // 
            this.gridView_ContasMP.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colid,
            this.colfilial,
            this.colcodigo,
            this.coldescricao,
            this.colurl,
            this.colapptoken,
            this.colcmptoken,
            this.colstatus});
            this.gridView_ContasMP.GridControl = this.gridControl_ContasMP;
            this.gridView_ContasMP.Name = "gridView_ContasMP";
            this.gridView_ContasMP.OptionsBehavior.Editable = false;
            this.gridView_ContasMP.OptionsView.ShowGroupPanel = false;
            this.gridView_ContasMP.OptionsView.ShowIndicator = false;
            this.gridView_ContasMP.DoubleClick += new System.EventHandler(this.gridView_ContasMP_DoubleClick);
            // 
            // colid
            // 
            this.colid.Caption = "ID";
            this.colid.FieldName = "id";
            this.colid.Name = "colid";
            this.colid.Visible = true;
            this.colid.VisibleIndex = 0;
            // 
            // colfilial
            // 
            this.colfilial.Caption = "Filial";
            this.colfilial.FieldName = "filial";
            this.colfilial.Name = "colfilial";
            this.colfilial.Visible = true;
            this.colfilial.VisibleIndex = 1;
            // 
            // colcodigo
            // 
            this.colcodigo.Caption = "Código";
            this.colcodigo.FieldName = "codigo";
            this.colcodigo.Name = "colcodigo";
            this.colcodigo.Visible = true;
            this.colcodigo.VisibleIndex = 2;
            // 
            // coldescricao
            // 
            this.coldescricao.Caption = "Descrição";
            this.coldescricao.FieldName = "descricao";
            this.coldescricao.Name = "coldescricao";
            this.coldescricao.Visible = true;
            this.coldescricao.VisibleIndex = 3;
            // 
            // colurl
            // 
            this.colurl.Caption = "URL";
            this.colurl.FieldName = "url";
            this.colurl.Name = "colurl";
            this.colurl.Visible = true;
            this.colurl.VisibleIndex = 4;
            // 
            // colapptoken
            // 
            this.colapptoken.Caption = "Application Token";
            this.colapptoken.FieldName = "apptoken";
            this.colapptoken.Name = "colapptoken";
            this.colapptoken.Visible = true;
            this.colapptoken.VisibleIndex = 5;
            // 
            // colcmptoken
            // 
            this.colcmptoken.Caption = "Company Token";
            this.colcmptoken.FieldName = "cmptoken";
            this.colcmptoken.Name = "colcmptoken";
            this.colcmptoken.Visible = true;
            this.colcmptoken.VisibleIndex = 6;
            // 
            // colstatus
            // 
            this.colstatus.Caption = "Status";
            this.colstatus.FieldName = "status";
            this.colstatus.Name = "colstatus";
            this.colstatus.Visible = true;
            this.colstatus.VisibleIndex = 7;
            // 
            // tabPane_ConfigContasMeusPedidos
            // 
            this.tabPane_ConfigContasMeusPedidos.Controls.Add(this.tabNavigationPage_DataGridContas);
            this.tabPane_ConfigContasMeusPedidos.Controls.Add(this.tabNavigationPage_RegisterContas);
            this.tabPane_ConfigContasMeusPedidos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPane_ConfigContasMeusPedidos.Location = new System.Drawing.Point(0, 94);
            this.tabPane_ConfigContasMeusPedidos.Name = "tabPane_ConfigContasMeusPedidos";
            this.tabPane_ConfigContasMeusPedidos.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.tabNavigationPage_DataGridContas,
            this.tabNavigationPage_RegisterContas});
            this.tabPane_ConfigContasMeusPedidos.RegularSize = new System.Drawing.Size(1264, 445);
            this.tabPane_ConfigContasMeusPedidos.SelectedPage = this.tabNavigationPage_RegisterContas;
            this.tabPane_ConfigContasMeusPedidos.Size = new System.Drawing.Size(1264, 445);
            this.tabPane_ConfigContasMeusPedidos.TabIndex = 8;
            this.tabPane_ConfigContasMeusPedidos.Text = "Conta Meus Pedidos";
            // 
            // barManager_ContasMP
            // 
            this.barManager_ContasMP.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar_Acoes,
            this.bar3,
            this.bar_Acoes2});
            this.barManager_ContasMP.DockControls.Add(this.barDockControlTop);
            this.barManager_ContasMP.DockControls.Add(this.barDockControlBottom);
            this.barManager_ContasMP.DockControls.Add(this.barDockControlLeft);
            this.barManager_ContasMP.DockControls.Add(this.barDockControlRight);
            this.barManager_ContasMP.Form = this;
            this.barManager_ContasMP.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem_Add,
            this.barButtonItem_Edit,
            this.barButtonItem_Del,
            this.barButtonItem_Gravar,
            this.barButtonItem_Cancelar});
            this.barManager_ContasMP.MaxItemId = 5;
            this.barManager_ContasMP.StatusBar = this.bar3;
            // 
            // bar_Acoes
            // 
            this.bar_Acoes.BarName = "Tools";
            this.bar_Acoes.DockCol = 0;
            this.bar_Acoes.DockRow = 0;
            this.bar_Acoes.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar_Acoes.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Add),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Edit),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Del)});
            this.bar_Acoes.OptionsBar.UseWholeRow = true;
            this.bar_Acoes.Text = "Tools";
            // 
            // barButtonItem_Add
            // 
            this.barButtonItem_Add.Caption = "Adicionar";
            this.barButtonItem_Add.Id = 0;
            this.barButtonItem_Add.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Add.ImageOptions.Image")));
            this.barButtonItem_Add.Name = "barButtonItem_Add";
            this.barButtonItem_Add.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Add.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Add_ItemClick);
            // 
            // barButtonItem_Edit
            // 
            this.barButtonItem_Edit.Caption = "Editar";
            this.barButtonItem_Edit.Id = 1;
            this.barButtonItem_Edit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Edit.ImageOptions.Image")));
            this.barButtonItem_Edit.Name = "barButtonItem_Edit";
            this.barButtonItem_Edit.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Edit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Edit_ItemClick);
            // 
            // barButtonItem_Del
            // 
            this.barButtonItem_Del.Caption = "Excluir";
            this.barButtonItem_Del.Id = 2;
            this.barButtonItem_Del.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Del.ImageOptions.Image")));
            this.barButtonItem_Del.Name = "barButtonItem_Del";
            this.barButtonItem_Del.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Del.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Del_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // bar_Acoes2
            // 
            this.bar_Acoes2.BarName = "Tools_GravarCancelar";
            this.bar_Acoes2.DockCol = 0;
            this.bar_Acoes2.DockRow = 1;
            this.bar_Acoes2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar_Acoes2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Gravar),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Cancelar)});
            this.bar_Acoes2.OptionsBar.AllowRename = true;
            this.bar_Acoes2.OptionsBar.UseWholeRow = true;
            this.bar_Acoes2.Text = "Tools_GravarCancelar";
            // 
            // barButtonItem_Gravar
            // 
            this.barButtonItem_Gravar.Caption = "Gravar";
            this.barButtonItem_Gravar.Id = 3;
            this.barButtonItem_Gravar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Gravar.ImageOptions.Image")));
            this.barButtonItem_Gravar.Name = "barButtonItem_Gravar";
            this.barButtonItem_Gravar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Gravar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Gravar_ItemClick);
            // 
            // barButtonItem_Cancelar
            // 
            this.barButtonItem_Cancelar.Caption = "Cancelar";
            this.barButtonItem_Cancelar.Id = 4;
            this.barButtonItem_Cancelar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem_Cancelar.ImageOptions.Image")));
            this.barButtonItem_Cancelar.Name = "barButtonItem_Cancelar";
            this.barButtonItem_Cancelar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItem_Cancelar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem_Cancelar_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager_ContasMP;
            this.barDockControlTop.Size = new System.Drawing.Size(1264, 94);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 539);
            this.barDockControlBottom.Manager = this.barManager_ContasMP;
            this.barDockControlBottom.Size = new System.Drawing.Size(1264, 23);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 94);
            this.barDockControlLeft.Manager = this.barManager_ContasMP;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 445);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1264, 94);
            this.barDockControlRight.Manager = this.barManager_ContasMP;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 445);
            // 
            // FormContaMeusPedidos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1264, 562);
            this.Controls.Add(this.tabPane_ConfigContasMeusPedidos);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormContaMeusPedidos";
            this.ShowMdiChildCaptionInParentTitle = true;
            this.Text = "Config. Contas MP";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormContaMeusPedidos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.contasMeusPedidosBindingSource)).EndInit();
            this.tabNavigationPage_RegisterContas.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl_RegisterContas)).EndInit();
            this.panelControl_RegisterContas.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_ContasMP)).EndInit();
            this.layoutControl_ContasMP.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CMPDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CMPURL.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CMPAppToken.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CMPCompanyToken.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CMPFilial.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CMPCodigo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CMPStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toggleSwitch_enviaPedidos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_RegisterContas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descricao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.URL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AppToken)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CompanyToken)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Status)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Código)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Filial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EnviaPedidos)).EndInit();
            this.tabNavigationPage_DataGridContas.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_ContasMP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_ContasMP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane_ConfigContasMeusPedidos)).EndInit();
            this.tabPane_ConfigContasMeusPedidos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.barManager_ContasMP)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Cliente;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CondicaoPagamentoCliente;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CategoriasCliente;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TabelaPreco;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Usuarios;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Produto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_ImagensProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TabelaProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TabelaPrecoProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CategoriaProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Transportadora;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CondicaoPagamento;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_FormaPagamento;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Pedido;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CamposExtraPedido;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_FaturamentoPedido;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TituloVencido;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private System.Windows.Forms.BindingSource contasMeusPedidosBindingSource;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager_ContasMP;
        private DevExpress.XtraBars.Navigation.TabPane tabPane_ConfigContasMeusPedidos;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage_DataGridContas;
        private DevExpress.XtraGrid.GridControl gridControl_ContasMP;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView_ContasMP;
        private DevExpress.XtraGrid.Columns.GridColumn colid;
        private DevExpress.XtraGrid.Columns.GridColumn colfilial;
        private DevExpress.XtraGrid.Columns.GridColumn colcodigo;
        private DevExpress.XtraGrid.Columns.GridColumn coldescricao;
        private DevExpress.XtraGrid.Columns.GridColumn colurl;
        private DevExpress.XtraGrid.Columns.GridColumn colapptoken;
        private DevExpress.XtraGrid.Columns.GridColumn colcmptoken;
        private DevExpress.XtraGrid.Columns.GridColumn colstatus;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage_RegisterContas;
        private DevExpress.XtraEditors.PanelControl panelControl_RegisterContas;
        private DevExpress.XtraLayout.LayoutControl layoutControl_ContasMP;
        private DevExpress.XtraEditors.TextEdit textEdit_CMPDesc;
        private DevExpress.XtraEditors.TextEdit textEdit_CMPURL;
        private DevExpress.XtraEditors.TextEdit textEdit_CMPAppToken;
        private DevExpress.XtraEditors.TextEdit textEdit_CMPCompanyToken;
        private DevExpress.XtraEditors.TextEdit textEdit_CMPFilial;
        private DevExpress.XtraEditors.TextEdit textEdit_CMPCodigo;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_RegisterContas;
        private DevExpress.XtraLayout.LayoutControlItem Descricao;
        private DevExpress.XtraLayout.LayoutControlItem URL;
        private DevExpress.XtraLayout.LayoutControlItem AppToken;
        private DevExpress.XtraLayout.LayoutControlItem CompanyToken;
        private DevExpress.XtraLayout.LayoutControlItem Status;
        private DevExpress.XtraLayout.LayoutControlItem Código;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem Filial;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraBars.BarManager barManager_ContasMP;
        private DevExpress.XtraBars.Bar bar_Acoes;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Add;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Edit;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Del;
        private DevExpress.XtraBars.Bar bar_Acoes2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Gravar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_Cancelar;
        private DevExpress.XtraEditors.ToggleSwitch textEdit_CMPStatus;
        private DevExpress.XtraEditors.ToggleSwitch toggleSwitch_enviaPedidos;
        private DevExpress.XtraLayout.LayoutControlItem EnviaPedidos;
    }
}


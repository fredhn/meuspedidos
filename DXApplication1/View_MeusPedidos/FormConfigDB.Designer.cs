﻿namespace View_MeusPedidos
{
    partial class FormConfigDB
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement10 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement11 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement12 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement13 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement14 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement15 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement16 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement17 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.tileNavSubItem_Cliente = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CondicaoPagamentoCliente = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CategoriasCliente = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TabelaPreco = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Usuarios = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Produto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_ImagensProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TabelaProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TabelaPrecoProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CategoriaProduto = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Pedido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CamposExtraPedido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_Transportadora = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_CondicaoPagamento = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_FormaPagamento = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_FaturamentoPedido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.tileNavSubItem_TituloVencido = new DevExpress.XtraBars.Navigation.TileNavSubItem();
            this.panel_ConfigDB = new System.Windows.Forms.Panel();
            this.layoutControl_LocalServer = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit_NomeServidor = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_NomeBancoDados = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_UsuarioBancoDados = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_SenhaBancoDados = new DevExpress.XtraEditors.TextEdit();
            this.checkEdit_AutenticacaoSegura = new DevExpress.XtraEditors.CheckEdit();
            this.simpleButton_Salvar = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup_LocalServer = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem_NomeServidor = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_UsuarioDB = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_NomeDatabase = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_SenhaDB = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.toastNotificationsManager_ConfigDB = new DevExpress.XtraBars.ToastNotifications.ToastNotificationsManager(this.components);
            this.panel_ServidorExterno = new System.Windows.Forms.Panel();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit_NomeServidorExt = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_NomeBancoDadosExt = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_UsuarioBancoDadosExt = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_SenhaBancoDadosExt = new DevExpress.XtraEditors.TextEdit();
            this.checkEdit_AutenticacaoSeguraExt = new DevExpress.XtraEditors.CheckEdit();
            this.simpleButton_ServidorExternoConectar = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit_NomeCon = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.gridControl_BancoDadosRegistros = new DevExpress.XtraGrid.GridControl();
            this.bancosDadosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView_BancosDadosExt = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colnome_conexao = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colnome_servidor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colnome_bancodados = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colusuario_bancodados = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colsenha_bancodados = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colautenticacao_segura = new DevExpress.XtraGrid.Columns.GridColumn();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.panel_ConfigServidorLocal = new System.Windows.Forms.Panel();
            this.labelControl_panelConfigServidorLocal = new DevExpress.XtraEditors.LabelControl();
            this.panel_ConfigServidorExterno = new System.Windows.Forms.Panel();
            this.pictureBox_ServidorExt_ConnectionStatus = new System.Windows.Forms.PictureBox();
            this.hyperlinkLabelControl_panelConfigServidorExt_HistoricoConexoes = new DevExpress.XtraEditors.HyperlinkLabelControl();
            this.labelControl_panelConfigServidorExterno = new DevExpress.XtraEditors.LabelControl();
            this.labelControl_panelConfigDB = new DevExpress.XtraEditors.LabelControl();
            this.splashScreenManager_FormConfigDB = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::View_MeusPedidos.WaitForm1), true, true);
            this.panel_ConfigDB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_LocalServer)).BeginInit();
            this.layoutControl_LocalServer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_NomeServidor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_NomeBancoDados.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_UsuarioBancoDados.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_SenhaBancoDados.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_AutenticacaoSegura.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_LocalServer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_NomeServidor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_UsuarioDB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_NomeDatabase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_SenhaDB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toastNotificationsManager_ConfigDB)).BeginInit();
            this.panel_ServidorExterno.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_NomeServidorExt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_NomeBancoDadosExt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_UsuarioBancoDadosExt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_SenhaBancoDadosExt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_AutenticacaoSeguraExt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_NomeCon.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_BancoDadosRegistros)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bancosDadosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_BancosDadosExt)).BeginInit();
            this.panel_ConfigServidorLocal.SuspendLayout();
            this.panel_ConfigServidorExterno.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ServidorExt_ConnectionStatus)).BeginInit();
            this.SuspendLayout();
            // 
            // tileNavSubItem_Cliente
            // 
            this.tileNavSubItem_Cliente.Caption = "Cliente";
            this.tileNavSubItem_Cliente.Name = "tileNavSubItem_Cliente";
            // 
            // 
            // 
            this.tileNavSubItem_Cliente.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement1.Text = "Cliente";
            this.tileNavSubItem_Cliente.Tile.Elements.Add(tileItemElement1);
            this.tileNavSubItem_Cliente.Tile.Name = "tileBarItem3";
            // 
            // tileNavSubItem_CondicaoPagamentoCliente
            // 
            this.tileNavSubItem_CondicaoPagamentoCliente.Caption = "Condições de Pagamento por Cliente";
            this.tileNavSubItem_CondicaoPagamentoCliente.Name = "tileNavSubItem_CondicaoPagamentoCliente";
            // 
            // 
            // 
            this.tileNavSubItem_CondicaoPagamentoCliente.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement2.Text = "Condições de Pagamento por Cliente";
            this.tileNavSubItem_CondicaoPagamentoCliente.Tile.Elements.Add(tileItemElement2);
            this.tileNavSubItem_CondicaoPagamentoCliente.Tile.Name = "tileBarItem4";
            // 
            // tileNavSubItem_CategoriasCliente
            // 
            this.tileNavSubItem_CategoriasCliente.Caption = "Categorias por Cliente";
            this.tileNavSubItem_CategoriasCliente.Name = "tileNavSubItem_CategoriasCliente";
            // 
            // 
            // 
            this.tileNavSubItem_CategoriasCliente.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement3.Text = "Categorias por Cliente";
            this.tileNavSubItem_CategoriasCliente.Tile.Elements.Add(tileItemElement3);
            this.tileNavSubItem_CategoriasCliente.Tile.Name = "tileBarItem6";
            // 
            // tileNavSubItem_TabelaPreco
            // 
            this.tileNavSubItem_TabelaPreco.Caption = "Tabelas de Preço por Cliente";
            this.tileNavSubItem_TabelaPreco.Name = "tileNavSubItem_TabelaPreco";
            // 
            // 
            // 
            this.tileNavSubItem_TabelaPreco.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement4.Text = "Tabelas de Preço por Cliente";
            this.tileNavSubItem_TabelaPreco.Tile.Elements.Add(tileItemElement4);
            this.tileNavSubItem_TabelaPreco.Tile.Name = "tileBarItem7";
            // 
            // tileNavSubItem_Usuarios
            // 
            this.tileNavSubItem_Usuarios.Caption = "Usuários (Vendedores)";
            this.tileNavSubItem_Usuarios.Name = "tileNavSubItem_Usuarios";
            // 
            // 
            // 
            this.tileNavSubItem_Usuarios.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement5.Text = "Usuários (Vendedores)";
            this.tileNavSubItem_Usuarios.Tile.Elements.Add(tileItemElement5);
            this.tileNavSubItem_Usuarios.Tile.Name = "tileBarItem8";
            // 
            // tileNavSubItem_Produto
            // 
            this.tileNavSubItem_Produto.Caption = "Produto";
            this.tileNavSubItem_Produto.Name = "tileNavSubItem_Produto";
            // 
            // 
            // 
            this.tileNavSubItem_Produto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement6.Text = "Produto";
            this.tileNavSubItem_Produto.Tile.Elements.Add(tileItemElement6);
            this.tileNavSubItem_Produto.Tile.Name = "tileBarItem9";
            // 
            // tileNavSubItem_ImagensProduto
            // 
            this.tileNavSubItem_ImagensProduto.Caption = "Imagens do Produto";
            this.tileNavSubItem_ImagensProduto.Name = "tileNavSubItem_ImagensProduto";
            // 
            // 
            // 
            this.tileNavSubItem_ImagensProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement7.Text = "Imagens do Produto";
            this.tileNavSubItem_ImagensProduto.Tile.Elements.Add(tileItemElement7);
            this.tileNavSubItem_ImagensProduto.Tile.Name = "tileBarItem10";
            // 
            // tileNavSubItem_TabelaProduto
            // 
            this.tileNavSubItem_TabelaProduto.Caption = "Tabela de Preço";
            this.tileNavSubItem_TabelaProduto.Name = "tileNavSubItem_TabelaProduto";
            // 
            // 
            // 
            this.tileNavSubItem_TabelaProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement8.Text = "Tabelas de Preço";
            this.tileNavSubItem_TabelaProduto.Tile.Elements.Add(tileItemElement8);
            this.tileNavSubItem_TabelaProduto.Tile.Name = "tileBarItem11";
            // 
            // tileNavSubItem_TabelaPrecoProduto
            // 
            this.tileNavSubItem_TabelaPrecoProduto.Caption = "Tabelas de Preço por Produto";
            this.tileNavSubItem_TabelaPrecoProduto.Name = "tileNavSubItem_TabelaPrecoProduto";
            // 
            // 
            // 
            this.tileNavSubItem_TabelaPrecoProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement9.Text = "Tabelas de Preço por Produto";
            this.tileNavSubItem_TabelaPrecoProduto.Tile.Elements.Add(tileItemElement9);
            this.tileNavSubItem_TabelaPrecoProduto.Tile.Name = "tileBarItem12";
            // 
            // tileNavSubItem_CategoriaProduto
            // 
            this.tileNavSubItem_CategoriaProduto.Caption = "Categorias de Produtos";
            this.tileNavSubItem_CategoriaProduto.Name = "tileNavSubItem_CategoriaProduto";
            // 
            // 
            // 
            this.tileNavSubItem_CategoriaProduto.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement10.Text = "Categorias de Produtos";
            this.tileNavSubItem_CategoriaProduto.Tile.Elements.Add(tileItemElement10);
            this.tileNavSubItem_CategoriaProduto.Tile.Name = "tileBarItem13";
            // 
            // tileNavSubItem_Pedido
            // 
            this.tileNavSubItem_Pedido.Caption = "Pedido";
            this.tileNavSubItem_Pedido.Name = "tileNavSubItem_Pedido";
            // 
            // 
            // 
            this.tileNavSubItem_Pedido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement11.Text = "Pedido";
            this.tileNavSubItem_Pedido.Tile.Elements.Add(tileItemElement11);
            this.tileNavSubItem_Pedido.Tile.Name = "tileBarItem17";
            // 
            // tileNavSubItem_CamposExtraPedido
            // 
            this.tileNavSubItem_CamposExtraPedido.Caption = "Campos Extras do Pedido";
            this.tileNavSubItem_CamposExtraPedido.Name = "tileNavSubItem_CamposExtraPedido";
            // 
            // 
            // 
            this.tileNavSubItem_CamposExtraPedido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement12.Text = "Campos Extras do Pedido";
            this.tileNavSubItem_CamposExtraPedido.Tile.Elements.Add(tileItemElement12);
            this.tileNavSubItem_CamposExtraPedido.Tile.Name = "tileBarItem18";
            // 
            // tileNavSubItem_Transportadora
            // 
            this.tileNavSubItem_Transportadora.Caption = "Transportadoras";
            this.tileNavSubItem_Transportadora.Name = "tileNavSubItem_Transportadora";
            // 
            // 
            // 
            this.tileNavSubItem_Transportadora.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement13.Text = "Transportadoras";
            this.tileNavSubItem_Transportadora.Tile.Elements.Add(tileItemElement13);
            this.tileNavSubItem_Transportadora.Tile.Name = "tileBarItem14";
            // 
            // tileNavSubItem_CondicaoPagamento
            // 
            this.tileNavSubItem_CondicaoPagamento.Caption = "Condições de Pagamento";
            this.tileNavSubItem_CondicaoPagamento.Name = "tileNavSubItem_CondicaoPagamento";
            // 
            // 
            // 
            this.tileNavSubItem_CondicaoPagamento.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement14.Text = "Condições de Pagamento";
            this.tileNavSubItem_CondicaoPagamento.Tile.Elements.Add(tileItemElement14);
            this.tileNavSubItem_CondicaoPagamento.Tile.Name = "tileBarItem15";
            // 
            // tileNavSubItem_FormaPagamento
            // 
            this.tileNavSubItem_FormaPagamento.Caption = "Formas de Pagamento";
            this.tileNavSubItem_FormaPagamento.Name = "tileNavSubItem_FormaPagamento";
            // 
            // 
            // 
            this.tileNavSubItem_FormaPagamento.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement15.Text = "Formas de Pagamento";
            this.tileNavSubItem_FormaPagamento.Tile.Elements.Add(tileItemElement15);
            this.tileNavSubItem_FormaPagamento.Tile.Name = "tileBarItem16";
            // 
            // tileNavSubItem_FaturamentoPedido
            // 
            this.tileNavSubItem_FaturamentoPedido.Caption = "Faturamento de Pedido";
            this.tileNavSubItem_FaturamentoPedido.Name = "tileNavSubItem_FaturamentoPedido";
            // 
            // 
            // 
            this.tileNavSubItem_FaturamentoPedido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement16.Text = "Faturamento de Pedido";
            this.tileNavSubItem_FaturamentoPedido.Tile.Elements.Add(tileItemElement16);
            this.tileNavSubItem_FaturamentoPedido.Tile.Name = "tileBarItem19";
            // 
            // tileNavSubItem_TituloVencido
            // 
            this.tileNavSubItem_TituloVencido.Caption = "Títulos Vencidos";
            this.tileNavSubItem_TituloVencido.Name = "tileNavSubItem_TituloVencido";
            // 
            // 
            // 
            this.tileNavSubItem_TituloVencido.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement17.Text = "Títulos Vencidos";
            this.tileNavSubItem_TituloVencido.Tile.Elements.Add(tileItemElement17);
            this.tileNavSubItem_TituloVencido.Tile.Name = "tileBarItem20";
            // 
            // panel_ConfigDB
            // 
            this.panel_ConfigDB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_ConfigDB.Controls.Add(this.layoutControl_LocalServer);
            this.panel_ConfigDB.Location = new System.Drawing.Point(36, 68);
            this.panel_ConfigDB.Name = "panel_ConfigDB";
            this.panel_ConfigDB.Size = new System.Drawing.Size(382, 191);
            this.panel_ConfigDB.TabIndex = 0;
            // 
            // layoutControl_LocalServer
            // 
            this.layoutControl_LocalServer.Controls.Add(this.textEdit_NomeServidor);
            this.layoutControl_LocalServer.Controls.Add(this.textEdit_NomeBancoDados);
            this.layoutControl_LocalServer.Controls.Add(this.textEdit_UsuarioBancoDados);
            this.layoutControl_LocalServer.Controls.Add(this.textEdit_SenhaBancoDados);
            this.layoutControl_LocalServer.Controls.Add(this.checkEdit_AutenticacaoSegura);
            this.layoutControl_LocalServer.Controls.Add(this.simpleButton_Salvar);
            this.layoutControl_LocalServer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl_LocalServer.Location = new System.Drawing.Point(0, 0);
            this.layoutControl_LocalServer.Name = "layoutControl_LocalServer";
            this.layoutControl_LocalServer.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(676, 243, 450, 400);
            this.layoutControl_LocalServer.Root = this.layoutControlGroup_LocalServer;
            this.layoutControl_LocalServer.Size = new System.Drawing.Size(380, 189);
            this.layoutControl_LocalServer.TabIndex = 0;
            this.layoutControl_LocalServer.Text = "layoutControl1";
            // 
            // textEdit_NomeServidor
            // 
            this.textEdit_NomeServidor.Location = new System.Drawing.Point(137, 36);
            this.textEdit_NomeServidor.Name = "textEdit_NomeServidor";
            this.textEdit_NomeServidor.Size = new System.Drawing.Size(231, 20);
            this.textEdit_NomeServidor.StyleController = this.layoutControl_LocalServer;
            this.textEdit_NomeServidor.TabIndex = 0;
            // 
            // textEdit_NomeBancoDados
            // 
            this.textEdit_NomeBancoDados.Location = new System.Drawing.Point(137, 60);
            this.textEdit_NomeBancoDados.Name = "textEdit_NomeBancoDados";
            this.textEdit_NomeBancoDados.Size = new System.Drawing.Size(231, 20);
            this.textEdit_NomeBancoDados.StyleController = this.layoutControl_LocalServer;
            this.textEdit_NomeBancoDados.TabIndex = 2;
            // 
            // textEdit_UsuarioBancoDados
            // 
            this.textEdit_UsuarioBancoDados.Location = new System.Drawing.Point(137, 84);
            this.textEdit_UsuarioBancoDados.Name = "textEdit_UsuarioBancoDados";
            this.textEdit_UsuarioBancoDados.Size = new System.Drawing.Size(231, 20);
            this.textEdit_UsuarioBancoDados.StyleController = this.layoutControl_LocalServer;
            this.textEdit_UsuarioBancoDados.TabIndex = 3;
            // 
            // textEdit_SenhaBancoDados
            // 
            this.textEdit_SenhaBancoDados.Location = new System.Drawing.Point(137, 108);
            this.textEdit_SenhaBancoDados.Name = "textEdit_SenhaBancoDados";
            this.textEdit_SenhaBancoDados.Properties.UseSystemPasswordChar = true;
            this.textEdit_SenhaBancoDados.Size = new System.Drawing.Size(231, 20);
            this.textEdit_SenhaBancoDados.StyleController = this.layoutControl_LocalServer;
            this.textEdit_SenhaBancoDados.TabIndex = 4;
            // 
            // checkEdit_AutenticacaoSegura
            // 
            this.checkEdit_AutenticacaoSegura.Location = new System.Drawing.Point(137, 132);
            this.checkEdit_AutenticacaoSegura.Name = "checkEdit_AutenticacaoSegura";
            this.checkEdit_AutenticacaoSegura.Properties.Caption = "";
            this.checkEdit_AutenticacaoSegura.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.checkEdit_AutenticacaoSegura.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.checkEdit_AutenticacaoSegura.Size = new System.Drawing.Size(231, 19);
            this.checkEdit_AutenticacaoSegura.StyleController = this.layoutControl_LocalServer;
            this.checkEdit_AutenticacaoSegura.TabIndex = 5;
            // 
            // simpleButton_Salvar
            // 
            this.simpleButton_Salvar.Location = new System.Drawing.Point(12, 155);
            this.simpleButton_Salvar.Name = "simpleButton_Salvar";
            this.simpleButton_Salvar.Size = new System.Drawing.Size(356, 22);
            this.simpleButton_Salvar.StyleController = this.layoutControl_LocalServer;
            this.simpleButton_Salvar.TabIndex = 6;
            this.simpleButton_Salvar.Text = "Salvar";
            this.simpleButton_Salvar.Click += new System.EventHandler(this.simpleButton_Salvar_Click);
            // 
            // layoutControlGroup_LocalServer
            // 
            this.layoutControlGroup_LocalServer.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup_LocalServer.GroupBordersVisible = false;
            this.layoutControlGroup_LocalServer.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem_NomeServidor,
            this.layoutControlItem_UsuarioDB,
            this.layoutControlItem_NomeDatabase,
            this.layoutControlItem_SenhaDB,
            this.layoutControlItem1,
            this.layoutControlItem7,
            this.emptySpaceItem1});
            this.layoutControlGroup_LocalServer.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup_LocalServer.Name = "layoutControlGroup_LocalServer";
            this.layoutControlGroup_LocalServer.Size = new System.Drawing.Size(380, 189);
            this.layoutControlGroup_LocalServer.TextVisible = false;
            // 
            // layoutControlItem_NomeServidor
            // 
            this.layoutControlItem_NomeServidor.Control = this.textEdit_NomeServidor;
            this.layoutControlItem_NomeServidor.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem_NomeServidor.Name = "layoutControlItem_NomeServidor";
            this.layoutControlItem_NomeServidor.Size = new System.Drawing.Size(360, 24);
            this.layoutControlItem_NomeServidor.Text = "Nome do Servidor";
            this.layoutControlItem_NomeServidor.TextSize = new System.Drawing.Size(122, 13);
            // 
            // layoutControlItem_UsuarioDB
            // 
            this.layoutControlItem_UsuarioDB.Control = this.textEdit_UsuarioBancoDados;
            this.layoutControlItem_UsuarioDB.CustomizationFormText = "Usuário";
            this.layoutControlItem_UsuarioDB.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem_UsuarioDB.Name = "layoutControlItem_UsuarioDB";
            this.layoutControlItem_UsuarioDB.Size = new System.Drawing.Size(360, 24);
            this.layoutControlItem_UsuarioDB.Text = "Usuário";
            this.layoutControlItem_UsuarioDB.TextSize = new System.Drawing.Size(122, 13);
            // 
            // layoutControlItem_NomeDatabase
            // 
            this.layoutControlItem_NomeDatabase.Control = this.textEdit_NomeBancoDados;
            this.layoutControlItem_NomeDatabase.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem_NomeDatabase.Name = "layoutControlItem_NomeDatabase";
            this.layoutControlItem_NomeDatabase.Size = new System.Drawing.Size(360, 24);
            this.layoutControlItem_NomeDatabase.Text = "Nome do Banco de Dados";
            this.layoutControlItem_NomeDatabase.TextSize = new System.Drawing.Size(122, 13);
            // 
            // layoutControlItem_SenhaDB
            // 
            this.layoutControlItem_SenhaDB.Control = this.textEdit_SenhaBancoDados;
            this.layoutControlItem_SenhaDB.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem_SenhaDB.Name = "layoutControlItem_SenhaDB";
            this.layoutControlItem_SenhaDB.Size = new System.Drawing.Size(360, 24);
            this.layoutControlItem_SenhaDB.Text = "Senha";
            this.layoutControlItem_SenhaDB.TextSize = new System.Drawing.Size(122, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.checkEdit_AutenticacaoSegura;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(360, 23);
            this.layoutControlItem1.Text = "Autenticação Segura";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(122, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.simpleButton_Salvar;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 143);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(360, 26);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(360, 24);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // toastNotificationsManager_ConfigDB
            // 
            this.toastNotificationsManager_ConfigDB.ApplicationId = "c867116e-c831-43a6-a13e-f02fca3105f8";
            this.toastNotificationsManager_ConfigDB.ApplicationName = "View_MeusPedidos";
            this.toastNotificationsManager_ConfigDB.Notifications.AddRange(new DevExpress.XtraBars.ToastNotifications.IToastNotificationProperties[] {
            new DevExpress.XtraBars.ToastNotifications.ToastNotification("6fdf5aff-e908-4eb1-8648-05ec7812d3d8", null, "Config. Banco de Dados", "Config. Banco de Dados", "Configuração Salva!", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.Text02)});
            // 
            // panel_ServidorExterno
            // 
            this.panel_ServidorExterno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_ServidorExterno.Controls.Add(this.layoutControl2);
            this.panel_ServidorExterno.Location = new System.Drawing.Point(45, 69);
            this.panel_ServidorExterno.Name = "panel_ServidorExterno";
            this.panel_ServidorExterno.Size = new System.Drawing.Size(364, 192);
            this.panel_ServidorExterno.TabIndex = 1;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.textEdit_NomeServidorExt);
            this.layoutControl2.Controls.Add(this.textEdit_NomeBancoDadosExt);
            this.layoutControl2.Controls.Add(this.textEdit_UsuarioBancoDadosExt);
            this.layoutControl2.Controls.Add(this.textEdit_SenhaBancoDadosExt);
            this.layoutControl2.Controls.Add(this.checkEdit_AutenticacaoSeguraExt);
            this.layoutControl2.Controls.Add(this.simpleButton_ServidorExternoConectar);
            this.layoutControl2.Controls.Add(this.textEdit_NomeCon);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(676, 243, 450, 400);
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(362, 190);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // textEdit_NomeServidorExt
            // 
            this.textEdit_NomeServidorExt.Location = new System.Drawing.Point(137, 36);
            this.textEdit_NomeServidorExt.Name = "textEdit_NomeServidorExt";
            this.textEdit_NomeServidorExt.Size = new System.Drawing.Size(213, 20);
            this.textEdit_NomeServidorExt.StyleController = this.layoutControl2;
            this.textEdit_NomeServidorExt.TabIndex = 4;
            // 
            // textEdit_NomeBancoDadosExt
            // 
            this.textEdit_NomeBancoDadosExt.Location = new System.Drawing.Point(137, 60);
            this.textEdit_NomeBancoDadosExt.Name = "textEdit_NomeBancoDadosExt";
            this.textEdit_NomeBancoDadosExt.Size = new System.Drawing.Size(213, 20);
            this.textEdit_NomeBancoDadosExt.StyleController = this.layoutControl2;
            this.textEdit_NomeBancoDadosExt.TabIndex = 5;
            // 
            // textEdit_UsuarioBancoDadosExt
            // 
            this.textEdit_UsuarioBancoDadosExt.Location = new System.Drawing.Point(137, 84);
            this.textEdit_UsuarioBancoDadosExt.Name = "textEdit_UsuarioBancoDadosExt";
            this.textEdit_UsuarioBancoDadosExt.Size = new System.Drawing.Size(213, 20);
            this.textEdit_UsuarioBancoDadosExt.StyleController = this.layoutControl2;
            this.textEdit_UsuarioBancoDadosExt.TabIndex = 6;
            // 
            // textEdit_SenhaBancoDadosExt
            // 
            this.textEdit_SenhaBancoDadosExt.Location = new System.Drawing.Point(137, 108);
            this.textEdit_SenhaBancoDadosExt.Name = "textEdit_SenhaBancoDadosExt";
            this.textEdit_SenhaBancoDadosExt.Properties.UseSystemPasswordChar = true;
            this.textEdit_SenhaBancoDadosExt.Size = new System.Drawing.Size(213, 20);
            this.textEdit_SenhaBancoDadosExt.StyleController = this.layoutControl2;
            this.textEdit_SenhaBancoDadosExt.TabIndex = 7;
            // 
            // checkEdit_AutenticacaoSeguraExt
            // 
            this.checkEdit_AutenticacaoSeguraExt.Location = new System.Drawing.Point(137, 132);
            this.checkEdit_AutenticacaoSeguraExt.Name = "checkEdit_AutenticacaoSeguraExt";
            this.checkEdit_AutenticacaoSeguraExt.Properties.Caption = "";
            this.checkEdit_AutenticacaoSeguraExt.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.checkEdit_AutenticacaoSeguraExt.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.checkEdit_AutenticacaoSeguraExt.Size = new System.Drawing.Size(213, 19);
            this.checkEdit_AutenticacaoSeguraExt.StyleController = this.layoutControl2;
            this.checkEdit_AutenticacaoSeguraExt.TabIndex = 8;
            // 
            // simpleButton_ServidorExternoConectar
            // 
            this.simpleButton_ServidorExternoConectar.Location = new System.Drawing.Point(12, 155);
            this.simpleButton_ServidorExternoConectar.Name = "simpleButton_ServidorExternoConectar";
            this.simpleButton_ServidorExternoConectar.Size = new System.Drawing.Size(338, 22);
            this.simpleButton_ServidorExternoConectar.StyleController = this.layoutControl2;
            this.simpleButton_ServidorExternoConectar.TabIndex = 9;
            this.simpleButton_ServidorExternoConectar.Text = "Conectar";
            this.simpleButton_ServidorExternoConectar.Click += new System.EventHandler(this.simpleButton_ServidorExternoConectar_Click);
            // 
            // textEdit_NomeCon
            // 
            this.textEdit_NomeCon.Location = new System.Drawing.Point(137, 12);
            this.textEdit_NomeCon.Name = "textEdit_NomeCon";
            this.textEdit_NomeCon.Size = new System.Drawing.Size(213, 20);
            this.textEdit_NomeCon.StyleController = this.layoutControl2;
            this.textEdit_NomeCon.TabIndex = 10;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem8,
            this.layoutControlItem9});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Size = new System.Drawing.Size(362, 190);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.textEdit_NomeServidorExt;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem2.Name = "layoutControlItem_NomeServidor";
            this.layoutControlItem2.Size = new System.Drawing.Size(342, 24);
            this.layoutControlItem2.Text = "Nome do Servidor";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(122, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.textEdit_UsuarioBancoDadosExt;
            this.layoutControlItem3.CustomizationFormText = "Usuário";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem3.Name = "layoutControlItem_UsuarioDB";
            this.layoutControlItem3.Size = new System.Drawing.Size(342, 24);
            this.layoutControlItem3.Text = "Usuário";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(122, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.textEdit_NomeBancoDadosExt;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem4.Name = "layoutControlItem_NomeDatabase";
            this.layoutControlItem4.Size = new System.Drawing.Size(342, 24);
            this.layoutControlItem4.Text = "Nome do Banco de Dados";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(122, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.textEdit_SenhaBancoDadosExt;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem5.Name = "layoutControlItem_SenhaDB";
            this.layoutControlItem5.Size = new System.Drawing.Size(342, 24);
            this.layoutControlItem5.Text = "Senha";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(122, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.checkEdit_AutenticacaoSeguraExt;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem6.Name = "layoutControlItem1";
            this.layoutControlItem6.Size = new System.Drawing.Size(342, 23);
            this.layoutControlItem6.Text = "Autenticação Segura";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(122, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.simpleButton_ServidorExternoConectar;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 143);
            this.layoutControlItem8.Name = "layoutControlItem7";
            this.layoutControlItem8.Size = new System.Drawing.Size(342, 27);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.textEdit_NomeCon;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(342, 24);
            this.layoutControlItem9.Text = "Nome da Conexão";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(122, 13);
            // 
            // gridControl_BancoDadosRegistros
            // 
            this.gridControl_BancoDadosRegistros.DataSource = this.bancosDadosBindingSource;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl_BancoDadosRegistros.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl_BancoDadosRegistros.Location = new System.Drawing.Point(45, 285);
            this.gridControl_BancoDadosRegistros.MainView = this.gridView_BancosDadosExt;
            this.gridControl_BancoDadosRegistros.Name = "gridControl_BancoDadosRegistros";
            this.gridControl_BancoDadosRegistros.Size = new System.Drawing.Size(364, 187);
            this.gridControl_BancoDadosRegistros.TabIndex = 2;
            this.gridControl_BancoDadosRegistros.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView_BancosDadosExt});
            this.gridControl_BancoDadosRegistros.DoubleClick += new System.EventHandler(this.gridControl_BancoDadosRegistros_DoubleClick);
            // 
            // bancosDadosBindingSource
            // 
            this.bancosDadosBindingSource.DataSource = typeof(Core.BancoDados);
            // 
            // gridView_BancosDadosExt
            // 
            this.gridView_BancosDadosExt.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colid,
            this.colnome_conexao,
            this.colnome_servidor,
            this.colnome_bancodados,
            this.colusuario_bancodados,
            this.colsenha_bancodados,
            this.colautenticacao_segura});
            this.gridView_BancosDadosExt.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.gridView_BancosDadosExt.GridControl = this.gridControl_BancoDadosRegistros;
            this.gridView_BancosDadosExt.Name = "gridView_BancosDadosExt";
            this.gridView_BancosDadosExt.OptionsPrint.PrintGroupFooter = false;
            this.gridView_BancosDadosExt.OptionsPrint.PrintHeader = false;
            this.gridView_BancosDadosExt.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView_BancosDadosExt.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gridView_BancosDadosExt.OptionsSelection.UseIndicatorForSelection = false;
            this.gridView_BancosDadosExt.OptionsView.BestFitMode = DevExpress.XtraGrid.Views.Grid.GridBestFitMode.Fast;
            this.gridView_BancosDadosExt.OptionsView.ShowColumnHeaders = false;
            this.gridView_BancosDadosExt.OptionsView.ShowGroupPanel = false;
            this.gridView_BancosDadosExt.OptionsView.ShowIndicator = false;
            // 
            // colid
            // 
            this.colid.AppearanceCell.Options.UseTextOptions = true;
            this.colid.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colid.FieldName = "id";
            this.colid.Name = "colid";
            this.colid.OptionsColumn.AllowEdit = false;
            this.colid.Visible = true;
            this.colid.VisibleIndex = 0;
            this.colid.Width = 25;
            // 
            // colnome_conexao
            // 
            this.colnome_conexao.AppearanceCell.Options.UseTextOptions = true;
            this.colnome_conexao.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colnome_conexao.FieldName = "nome_conexao";
            this.colnome_conexao.Name = "colnome_conexao";
            this.colnome_conexao.OptionsColumn.AllowEdit = false;
            this.colnome_conexao.Visible = true;
            this.colnome_conexao.VisibleIndex = 1;
            this.colnome_conexao.Width = 55;
            // 
            // colnome_servidor
            // 
            this.colnome_servidor.AppearanceCell.Options.UseTextOptions = true;
            this.colnome_servidor.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colnome_servidor.FieldName = "nome_servidor";
            this.colnome_servidor.Name = "colnome_servidor";
            this.colnome_servidor.OptionsColumn.AllowEdit = false;
            this.colnome_servidor.Visible = true;
            this.colnome_servidor.VisibleIndex = 2;
            this.colnome_servidor.Width = 55;
            // 
            // colnome_bancodados
            // 
            this.colnome_bancodados.AppearanceCell.Options.UseTextOptions = true;
            this.colnome_bancodados.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colnome_bancodados.FieldName = "nome_bancodados";
            this.colnome_bancodados.Name = "colnome_bancodados";
            this.colnome_bancodados.OptionsColumn.AllowEdit = false;
            this.colnome_bancodados.Visible = true;
            this.colnome_bancodados.VisibleIndex = 3;
            this.colnome_bancodados.Width = 55;
            // 
            // colusuario_bancodados
            // 
            this.colusuario_bancodados.AppearanceCell.Options.UseTextOptions = true;
            this.colusuario_bancodados.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colusuario_bancodados.FieldName = "usuario_bancodados";
            this.colusuario_bancodados.Name = "colusuario_bancodados";
            this.colusuario_bancodados.OptionsColumn.AllowEdit = false;
            this.colusuario_bancodados.Visible = true;
            this.colusuario_bancodados.VisibleIndex = 4;
            this.colusuario_bancodados.Width = 55;
            // 
            // colsenha_bancodados
            // 
            this.colsenha_bancodados.AppearanceCell.Options.UseTextOptions = true;
            this.colsenha_bancodados.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colsenha_bancodados.FieldName = "senha_bancodados";
            this.colsenha_bancodados.Name = "colsenha_bancodados";
            this.colsenha_bancodados.OptionsColumn.AllowEdit = false;
            this.colsenha_bancodados.Visible = true;
            this.colsenha_bancodados.VisibleIndex = 5;
            this.colsenha_bancodados.Width = 55;
            // 
            // colautenticacao_segura
            // 
            this.colautenticacao_segura.AppearanceCell.Options.UseTextOptions = true;
            this.colautenticacao_segura.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colautenticacao_segura.FieldName = "autenticacao_segura";
            this.colautenticacao_segura.Name = "colautenticacao_segura";
            this.colautenticacao_segura.OptionsColumn.AllowEdit = false;
            this.colautenticacao_segura.Visible = true;
            this.colautenticacao_segura.VisibleIndex = 6;
            this.colautenticacao_segura.Width = 62;
            // 
            // panel_ConfigServidorLocal
            // 
            this.panel_ConfigServidorLocal.Controls.Add(this.labelControl_panelConfigServidorLocal);
            this.panel_ConfigServidorLocal.Controls.Add(this.panel_ConfigDB);
            this.panel_ConfigServidorLocal.Location = new System.Drawing.Point(97, 46);
            this.panel_ConfigServidorLocal.Name = "panel_ConfigServidorLocal";
            this.panel_ConfigServidorLocal.Size = new System.Drawing.Size(455, 479);
            this.panel_ConfigServidorLocal.TabIndex = 3;
            // 
            // labelControl_panelConfigServidorLocal
            // 
            this.labelControl_panelConfigServidorLocal.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl_panelConfigServidorLocal.Appearance.Options.UseFont = true;
            this.labelControl_panelConfigServidorLocal.Location = new System.Drawing.Point(153, 28);
            this.labelControl_panelConfigServidorLocal.Name = "labelControl_panelConfigServidorLocal";
            this.labelControl_panelConfigServidorLocal.Size = new System.Drawing.Size(148, 25);
            this.labelControl_panelConfigServidorLocal.TabIndex = 1;
            this.labelControl_panelConfigServidorLocal.Text = "Servidor Local";
            // 
            // panel_ConfigServidorExterno
            // 
            this.panel_ConfigServidorExterno.Controls.Add(this.pictureBox_ServidorExt_ConnectionStatus);
            this.panel_ConfigServidorExterno.Controls.Add(this.hyperlinkLabelControl_panelConfigServidorExt_HistoricoConexoes);
            this.panel_ConfigServidorExterno.Controls.Add(this.labelControl_panelConfigServidorExterno);
            this.panel_ConfigServidorExterno.Controls.Add(this.panel_ServidorExterno);
            this.panel_ConfigServidorExterno.Controls.Add(this.gridControl_BancoDadosRegistros);
            this.panel_ConfigServidorExterno.Location = new System.Drawing.Point(712, 46);
            this.panel_ConfigServidorExterno.Name = "panel_ConfigServidorExterno";
            this.panel_ConfigServidorExterno.Size = new System.Drawing.Size(455, 479);
            this.panel_ConfigServidorExterno.TabIndex = 4;
            this.panel_ConfigServidorExterno.Paint += new System.Windows.Forms.PaintEventHandler(this.panel_ConfigServidorExterno_Paint);
            // 
            // pictureBox_ServidorExt_ConnectionStatus
            // 
            this.pictureBox_ServidorExt_ConnectionStatus.BackColor = System.Drawing.Color.Red;
            this.pictureBox_ServidorExt_ConnectionStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox_ServidorExt_ConnectionStatus.Location = new System.Drawing.Point(413, 229);
            this.pictureBox_ServidorExt_ConnectionStatus.Name = "pictureBox_ServidorExt_ConnectionStatus";
            this.pictureBox_ServidorExt_ConnectionStatus.Size = new System.Drawing.Size(15, 15);
            this.pictureBox_ServidorExt_ConnectionStatus.TabIndex = 6;
            this.pictureBox_ServidorExt_ConnectionStatus.TabStop = false;
            // 
            // hyperlinkLabelControl_panelConfigServidorExt_HistoricoConexoes
            // 
            this.hyperlinkLabelControl_panelConfigServidorExt_HistoricoConexoes.Cursor = System.Windows.Forms.Cursors.Hand;
            this.hyperlinkLabelControl_panelConfigServidorExt_HistoricoConexoes.Location = new System.Drawing.Point(203, 266);
            this.hyperlinkLabelControl_panelConfigServidorExt_HistoricoConexoes.Name = "hyperlinkLabelControl_panelConfigServidorExt_HistoricoConexoes";
            this.hyperlinkLabelControl_panelConfigServidorExt_HistoricoConexoes.Size = new System.Drawing.Size(48, 13);
            this.hyperlinkLabelControl_panelConfigServidorExt_HistoricoConexoes.TabIndex = 3;
            this.hyperlinkLabelControl_panelConfigServidorExt_HistoricoConexoes.Text = "Conexões";
            // 
            // labelControl_panelConfigServidorExterno
            // 
            this.labelControl_panelConfigServidorExterno.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl_panelConfigServidorExterno.Appearance.Options.UseFont = true;
            this.labelControl_panelConfigServidorExterno.Location = new System.Drawing.Point(140, 28);
            this.labelControl_panelConfigServidorExterno.Name = "labelControl_panelConfigServidorExterno";
            this.labelControl_panelConfigServidorExterno.Size = new System.Drawing.Size(175, 25);
            this.labelControl_panelConfigServidorExterno.TabIndex = 2;
            this.labelControl_panelConfigServidorExterno.Text = "Servidor Externo";
            // 
            // labelControl_panelConfigDB
            // 
            this.labelControl_panelConfigDB.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl_panelConfigDB.Appearance.Options.UseFont = true;
            this.labelControl_panelConfigDB.Location = new System.Drawing.Point(456, 12);
            this.labelControl_panelConfigDB.Name = "labelControl_panelConfigDB";
            this.labelControl_panelConfigDB.Size = new System.Drawing.Size(353, 25);
            this.labelControl_panelConfigDB.TabIndex = 5;
            this.labelControl_panelConfigDB.Text = "Configurações de Banco de Dados";
            // 
            // splashScreenManager_FormConfigDB
            // 
            this.splashScreenManager_FormConfigDB.ClosingDelay = 500;
            // 
            // FormConfigDB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1264, 562);
            this.Controls.Add(this.labelControl_panelConfigDB);
            this.Controls.Add(this.panel_ConfigServidorExterno);
            this.Controls.Add(this.panel_ConfigServidorLocal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormConfigDB";
            this.ShowMdiChildCaptionInParentTitle = true;
            this.Text = "Config. Banco de Dados";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Shown += new System.EventHandler(this.FormConfigDB_Shown);
            this.panel_ConfigDB.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_LocalServer)).EndInit();
            this.layoutControl_LocalServer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_NomeServidor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_NomeBancoDados.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_UsuarioBancoDados.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_SenhaBancoDados.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_AutenticacaoSegura.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup_LocalServer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_NomeServidor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_UsuarioDB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_NomeDatabase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_SenhaDB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toastNotificationsManager_ConfigDB)).EndInit();
            this.panel_ServidorExterno.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_NomeServidorExt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_NomeBancoDadosExt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_UsuarioBancoDadosExt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_SenhaBancoDadosExt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_AutenticacaoSeguraExt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_NomeCon.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_BancoDadosRegistros)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bancosDadosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_BancosDadosExt)).EndInit();
            this.panel_ConfigServidorLocal.ResumeLayout(false);
            this.panel_ConfigServidorLocal.PerformLayout();
            this.panel_ConfigServidorExterno.ResumeLayout(false);
            this.panel_ConfigServidorExterno.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ServidorExt_ConnectionStatus)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Cliente;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CondicaoPagamentoCliente;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CategoriasCliente;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TabelaPreco;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Usuarios;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Produto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_ImagensProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TabelaProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TabelaPrecoProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CategoriaProduto;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Transportadora;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CondicaoPagamento;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_FormaPagamento;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_Pedido;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_CamposExtraPedido;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_FaturamentoPedido;
        private DevExpress.XtraBars.Navigation.TileNavSubItem tileNavSubItem_TituloVencido;
        private System.Windows.Forms.Panel panel_ConfigDB;
        private DevExpress.XtraLayout.LayoutControl layoutControl_LocalServer;
        private DevExpress.XtraEditors.TextEdit textEdit_NomeServidor;
        private DevExpress.XtraEditors.TextEdit textEdit_NomeBancoDados;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup_LocalServer;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_NomeServidor;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_NomeDatabase;
        private DevExpress.XtraEditors.TextEdit textEdit_UsuarioBancoDados;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_UsuarioDB;
        private DevExpress.XtraEditors.TextEdit textEdit_SenhaBancoDados;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_SenhaDB;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.CheckEdit checkEdit_AutenticacaoSegura;
        private DevExpress.XtraEditors.SimpleButton simpleButton_Salvar;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraBars.ToastNotifications.ToastNotificationsManager toastNotificationsManager_ConfigDB;
        private System.Windows.Forms.Panel panel_ServidorExterno;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.TextEdit textEdit_NomeServidorExt;
        private DevExpress.XtraEditors.TextEdit textEdit_NomeBancoDadosExt;
        private DevExpress.XtraEditors.TextEdit textEdit_UsuarioBancoDadosExt;
        private DevExpress.XtraEditors.TextEdit textEdit_SenhaBancoDadosExt;
        private DevExpress.XtraEditors.CheckEdit checkEdit_AutenticacaoSeguraExt;
        private DevExpress.XtraEditors.SimpleButton simpleButton_ServidorExternoConectar;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.TextEdit textEdit_NomeCon;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraGrid.GridControl gridControl_BancoDadosRegistros;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView_BancosDadosExt;
        private System.Windows.Forms.BindingSource bancosDadosBindingSource;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private System.Windows.Forms.Panel panel_ConfigServidorLocal;
        private System.Windows.Forms.Panel panel_ConfigServidorExterno;
        private DevExpress.XtraEditors.LabelControl labelControl_panelConfigServidorLocal;
        private DevExpress.XtraEditors.LabelControl labelControl_panelConfigServidorExterno;
        private DevExpress.XtraEditors.LabelControl labelControl_panelConfigDB;
        private DevExpress.XtraEditors.HyperlinkLabelControl hyperlinkLabelControl_panelConfigServidorExt_HistoricoConexoes;
        private DevExpress.XtraGrid.Columns.GridColumn colid;
        private DevExpress.XtraGrid.Columns.GridColumn colnome_conexao;
        private DevExpress.XtraGrid.Columns.GridColumn colnome_servidor;
        private DevExpress.XtraGrid.Columns.GridColumn colnome_bancodados;
        private DevExpress.XtraGrid.Columns.GridColumn colusuario_bancodados;
        private DevExpress.XtraGrid.Columns.GridColumn colsenha_bancodados;
        private DevExpress.XtraGrid.Columns.GridColumn colautenticacao_segura;
        private System.Windows.Forms.PictureBox pictureBox_ServidorExt_ConnectionStatus;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager_FormConfigDB;
    }
}


﻿namespace View_MeusPedidos
{
    partial class FormPedidoItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.listBoxControl_GradeCorTam = new DevExpress.XtraEditors.ListBoxControl();
            this.textEdit_Prod = new DevExpress.XtraEditors.LookUpEdit();
            this.mainRibbonControl = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.bbiSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveAndClose = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveAndNew = new DevExpress.XtraBars.BarButtonItem();
            this.bbiReset = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDelete = new DevExpress.XtraBars.BarButtonItem();
            this.bbiClose = new DevExpress.XtraBars.BarButtonItem();
            this.mainRibbonPage = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.mainRibbonPageGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.meusPedidosDataSet = new View_MeusPedidos.MeusPedidosDataSet();
            this.textEdit_TabelaPreco = new DevExpress.XtraEditors.LookUpEdit();
            this.textEdit_Quantidade = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_PrecoBruto = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_Descontos = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_Moeda = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit_CotacaoMoeda = new DevExpress.XtraEditors.TextEdit();
            this.memoEdit_Obs = new DevExpress.XtraEditors.MemoEdit();
            this.textEdit_IPI = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_TipoIPI = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit_ST = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_PrecoMin = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton_GradeQtd = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit_GradeCorTamDesc = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_TipoTamCor = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit_GradeCorTamQuantidade = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_PrecoLiquido = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEdit_DescontosList = new DevExpress.XtraEditors.ComboBoxEdit();
            this.simpleButton_DescontoAdd = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem_Prod = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_TabelaPreco = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_Quantidade = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_PrecoBruto = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_Descontos = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_Moeda = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_CotacaoMoeda = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_Obs = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_TipoIPI = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_IPI = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_ST = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem_GradeQtd = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_TipoTamCor = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_GradeCorTamDesc = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem_GradeCorTamQuantidade = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_PrecoMin = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_PrecoLiquido = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_DescontosList = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem_descontoAdd = new DevExpress.XtraLayout.LayoutControlItem();
            this.behaviorManager1 = new DevExpress.Utils.Behaviors.BehaviorManager(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControl_GradeCorTam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_Prod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainRibbonControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meusPedidosDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_TabelaPreco.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_Quantidade.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_PrecoBruto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_Descontos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_Moeda.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CotacaoMoeda.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_Obs.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_IPI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_TipoIPI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ST.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_PrecoMin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_GradeCorTamDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_TipoTamCor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_GradeCorTamQuantidade.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_PrecoLiquido.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit_DescontosList.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_Prod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_TabelaPreco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_Quantidade)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_PrecoBruto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_Descontos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_Moeda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CotacaoMoeda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_Obs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_TipoIPI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_IPI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ST)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_GradeQtd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_TipoTamCor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_GradeCorTamDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_GradeCorTamQuantidade)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_PrecoMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_PrecoLiquido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_DescontosList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_descontoAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.AllowCustomization = false;
            this.dataLayoutControl1.Controls.Add(this.listBoxControl_GradeCorTam);
            this.dataLayoutControl1.Controls.Add(this.textEdit_Prod);
            this.dataLayoutControl1.Controls.Add(this.textEdit_TabelaPreco);
            this.dataLayoutControl1.Controls.Add(this.textEdit_Quantidade);
            this.dataLayoutControl1.Controls.Add(this.textEdit_PrecoBruto);
            this.dataLayoutControl1.Controls.Add(this.textEdit_Descontos);
            this.dataLayoutControl1.Controls.Add(this.textEdit_Moeda);
            this.dataLayoutControl1.Controls.Add(this.textEdit_CotacaoMoeda);
            this.dataLayoutControl1.Controls.Add(this.memoEdit_Obs);
            this.dataLayoutControl1.Controls.Add(this.textEdit_IPI);
            this.dataLayoutControl1.Controls.Add(this.textEdit_TipoIPI);
            this.dataLayoutControl1.Controls.Add(this.textEdit_ST);
            this.dataLayoutControl1.Controls.Add(this.textEdit_PrecoMin);
            this.dataLayoutControl1.Controls.Add(this.simpleButton_GradeQtd);
            this.dataLayoutControl1.Controls.Add(this.textEdit_GradeCorTamDesc);
            this.dataLayoutControl1.Controls.Add(this.textEdit_TipoTamCor);
            this.dataLayoutControl1.Controls.Add(this.textEdit_GradeCorTamQuantidade);
            this.dataLayoutControl1.Controls.Add(this.textEdit_PrecoLiquido);
            this.dataLayoutControl1.Controls.Add(this.comboBoxEdit_DescontosList);
            this.dataLayoutControl1.Controls.Add(this.simpleButton_DescontoAdd);
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 143);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(499, 171, 450, 400);
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(867, 385);
            this.dataLayoutControl1.TabIndex = 0;
            // 
            // listBoxControl_GradeCorTam
            // 
            this.listBoxControl_GradeCorTam.Cursor = System.Windows.Forms.Cursors.Default;
            this.listBoxControl_GradeCorTam.Location = new System.Drawing.Point(22, 344);
            this.listBoxControl_GradeCorTam.Name = "listBoxControl_GradeCorTam";
            this.listBoxControl_GradeCorTam.Size = new System.Drawing.Size(823, 19);
            this.listBoxControl_GradeCorTam.StyleController = this.dataLayoutControl1;
            this.listBoxControl_GradeCorTam.TabIndex = 16;
            // 
            // textEdit_Prod
            // 
            this.textEdit_Prod.Location = new System.Drawing.Point(115, 22);
            this.textEdit_Prod.MenuManager = this.mainRibbonControl;
            this.textEdit_Prod.Name = "textEdit_Prod";
            this.textEdit_Prod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.textEdit_Prod.Properties.DisplayMember = "nome";
            this.textEdit_Prod.Properties.NullText = "";
            this.textEdit_Prod.Properties.ValueMember = "id";
            this.textEdit_Prod.Size = new System.Drawing.Size(730, 20);
            this.textEdit_Prod.StyleController = this.dataLayoutControl1;
            this.textEdit_Prod.TabIndex = 4;
            // 
            // mainRibbonControl
            // 
            this.mainRibbonControl.ExpandCollapseItem.Id = 0;
            this.mainRibbonControl.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.mainRibbonControl.ExpandCollapseItem,
            this.bbiSave,
            this.bbiSaveAndClose,
            this.bbiSaveAndNew,
            this.bbiReset,
            this.bbiDelete,
            this.bbiClose});
            this.mainRibbonControl.Location = new System.Drawing.Point(0, 0);
            this.mainRibbonControl.MaxItemId = 10;
            this.mainRibbonControl.Name = "mainRibbonControl";
            this.mainRibbonControl.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.mainRibbonPage});
            this.mainRibbonControl.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2013;
            this.mainRibbonControl.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.mainRibbonControl.Size = new System.Drawing.Size(867, 143);
            this.mainRibbonControl.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // bbiSave
            // 
            this.bbiSave.Caption = "Adicionar";
            this.bbiSave.Id = 2;
            this.bbiSave.ImageOptions.ImageUri.Uri = "Apply";
            this.bbiSave.Name = "bbiSave";
            this.bbiSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSave_ItemClick);
            // 
            // bbiSaveAndClose
            // 
            this.bbiSaveAndClose.Caption = "Save And Close";
            this.bbiSaveAndClose.Id = 3;
            this.bbiSaveAndClose.ImageOptions.ImageUri.Uri = "SaveAndClose";
            this.bbiSaveAndClose.Name = "bbiSaveAndClose";
            // 
            // bbiSaveAndNew
            // 
            this.bbiSaveAndNew.Caption = "Save And New";
            this.bbiSaveAndNew.Id = 4;
            this.bbiSaveAndNew.ImageOptions.ImageUri.Uri = "SaveAndNew";
            this.bbiSaveAndNew.Name = "bbiSaveAndNew";
            // 
            // bbiReset
            // 
            this.bbiReset.Caption = "Reset Changes";
            this.bbiReset.Id = 5;
            this.bbiReset.ImageOptions.ImageUri.Uri = "Reset";
            this.bbiReset.Name = "bbiReset";
            // 
            // bbiDelete
            // 
            this.bbiDelete.Caption = "Delete";
            this.bbiDelete.Id = 6;
            this.bbiDelete.ImageOptions.ImageUri.Uri = "Delete";
            this.bbiDelete.Name = "bbiDelete";
            // 
            // bbiClose
            // 
            this.bbiClose.Caption = "Close";
            this.bbiClose.Id = 7;
            this.bbiClose.ImageOptions.ImageUri.Uri = "Close";
            this.bbiClose.Name = "bbiClose";
            // 
            // mainRibbonPage
            // 
            this.mainRibbonPage.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.mainRibbonPageGroup});
            this.mainRibbonPage.MergeOrder = 0;
            this.mainRibbonPage.Name = "mainRibbonPage";
            this.mainRibbonPage.Text = "Home";
            // 
            // mainRibbonPageGroup
            // 
            this.mainRibbonPageGroup.AllowTextClipping = false;
            this.mainRibbonPageGroup.ItemLinks.Add(this.bbiSave);
            this.mainRibbonPageGroup.Name = "mainRibbonPageGroup";
            this.mainRibbonPageGroup.ShowCaptionButton = false;
            this.mainRibbonPageGroup.Text = "Tasks";
            // 
            // meusPedidosDataSet
            // 
            this.meusPedidosDataSet.DataSetName = "MeusPedidosDataSet";
            this.meusPedidosDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // textEdit_TabelaPreco
            // 
            this.textEdit_TabelaPreco.Location = new System.Drawing.Point(115, 46);
            this.textEdit_TabelaPreco.MenuManager = this.mainRibbonControl;
            this.textEdit_TabelaPreco.Name = "textEdit_TabelaPreco";
            this.textEdit_TabelaPreco.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.textEdit_TabelaPreco.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("id", "id", 31, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("nome", "nome", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("tipo", "tipo", 28, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("acrescimo", "acrescimo", 57, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("desconto", "desconto", 54, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("excluido", "excluido", 49, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.textEdit_TabelaPreco.Properties.DisplayMember = "nome";
            this.textEdit_TabelaPreco.Properties.NullText = "";
            this.textEdit_TabelaPreco.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.textEdit_TabelaPreco.Properties.ValueMember = "id";
            this.textEdit_TabelaPreco.Size = new System.Drawing.Size(730, 20);
            this.textEdit_TabelaPreco.StyleController = this.dataLayoutControl1;
            this.textEdit_TabelaPreco.TabIndex = 5;
            // 
            // textEdit_Quantidade
            // 
            this.textEdit_Quantidade.Location = new System.Drawing.Point(115, 70);
            this.textEdit_Quantidade.MenuManager = this.mainRibbonControl;
            this.textEdit_Quantidade.Name = "textEdit_Quantidade";
            this.textEdit_Quantidade.Properties.Mask.EditMask = "n0";
            this.textEdit_Quantidade.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEdit_Quantidade.Size = new System.Drawing.Size(730, 20);
            this.textEdit_Quantidade.StyleController = this.dataLayoutControl1;
            this.textEdit_Quantidade.TabIndex = 6;
            // 
            // textEdit_PrecoBruto
            // 
            this.textEdit_PrecoBruto.Location = new System.Drawing.Point(115, 94);
            this.textEdit_PrecoBruto.MenuManager = this.mainRibbonControl;
            this.textEdit_PrecoBruto.Name = "textEdit_PrecoBruto";
            this.textEdit_PrecoBruto.Properties.Mask.EditMask = "c";
            this.textEdit_PrecoBruto.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEdit_PrecoBruto.Size = new System.Drawing.Size(730, 20);
            this.textEdit_PrecoBruto.StyleController = this.dataLayoutControl1;
            this.textEdit_PrecoBruto.TabIndex = 7;
            // 
            // textEdit_Descontos
            // 
            this.textEdit_Descontos.Location = new System.Drawing.Point(115, 166);
            this.textEdit_Descontos.MenuManager = this.mainRibbonControl;
            this.textEdit_Descontos.Name = "textEdit_Descontos";
            this.textEdit_Descontos.Properties.Mask.EditMask = "n2";
            this.textEdit_Descontos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEdit_Descontos.Size = new System.Drawing.Size(278, 20);
            this.textEdit_Descontos.StyleController = this.dataLayoutControl1;
            this.textEdit_Descontos.TabIndex = 8;
            // 
            // textEdit_Moeda
            // 
            this.textEdit_Moeda.Location = new System.Drawing.Point(115, 192);
            this.textEdit_Moeda.MenuManager = this.mainRibbonControl;
            this.textEdit_Moeda.Name = "textEdit_Moeda";
            this.textEdit_Moeda.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.textEdit_Moeda.Size = new System.Drawing.Size(174, 20);
            this.textEdit_Moeda.StyleController = this.dataLayoutControl1;
            this.textEdit_Moeda.TabIndex = 9;
            // 
            // textEdit_CotacaoMoeda
            // 
            this.textEdit_CotacaoMoeda.Location = new System.Drawing.Point(386, 192);
            this.textEdit_CotacaoMoeda.MenuManager = this.mainRibbonControl;
            this.textEdit_CotacaoMoeda.Name = "textEdit_CotacaoMoeda";
            this.textEdit_CotacaoMoeda.Properties.Mask.EditMask = "c";
            this.textEdit_CotacaoMoeda.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEdit_CotacaoMoeda.Size = new System.Drawing.Size(459, 20);
            this.textEdit_CotacaoMoeda.StyleController = this.dataLayoutControl1;
            this.textEdit_CotacaoMoeda.TabIndex = 10;
            // 
            // memoEdit_Obs
            // 
            this.memoEdit_Obs.Location = new System.Drawing.Point(115, 216);
            this.memoEdit_Obs.MenuManager = this.mainRibbonControl;
            this.memoEdit_Obs.Name = "memoEdit_Obs";
            this.memoEdit_Obs.Size = new System.Drawing.Size(730, 21);
            this.memoEdit_Obs.StyleController = this.dataLayoutControl1;
            this.memoEdit_Obs.TabIndex = 11;
            // 
            // textEdit_IPI
            // 
            this.textEdit_IPI.Location = new System.Drawing.Point(386, 241);
            this.textEdit_IPI.MenuManager = this.mainRibbonControl;
            this.textEdit_IPI.Name = "textEdit_IPI";
            this.textEdit_IPI.Properties.Mask.EditMask = "n";
            this.textEdit_IPI.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEdit_IPI.Size = new System.Drawing.Size(459, 20);
            this.textEdit_IPI.StyleController = this.dataLayoutControl1;
            this.textEdit_IPI.TabIndex = 12;
            // 
            // textEdit_TipoIPI
            // 
            this.textEdit_TipoIPI.Location = new System.Drawing.Point(115, 241);
            this.textEdit_TipoIPI.MenuManager = this.mainRibbonControl;
            this.textEdit_TipoIPI.Name = "textEdit_TipoIPI";
            this.textEdit_TipoIPI.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.textEdit_TipoIPI.Properties.MaxLength = 1;
            this.textEdit_TipoIPI.Size = new System.Drawing.Size(174, 20);
            this.textEdit_TipoIPI.StyleController = this.dataLayoutControl1;
            this.textEdit_TipoIPI.TabIndex = 13;
            // 
            // textEdit_ST
            // 
            this.textEdit_ST.Location = new System.Drawing.Point(115, 265);
            this.textEdit_ST.MenuManager = this.mainRibbonControl;
            this.textEdit_ST.Name = "textEdit_ST";
            this.textEdit_ST.Properties.Mask.EditMask = "n";
            this.textEdit_ST.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEdit_ST.Size = new System.Drawing.Size(730, 20);
            this.textEdit_ST.StyleController = this.dataLayoutControl1;
            this.textEdit_ST.TabIndex = 14;
            // 
            // textEdit_PrecoMin
            // 
            this.textEdit_PrecoMin.EditValue = "0.01";
            this.textEdit_PrecoMin.Location = new System.Drawing.Point(115, 142);
            this.textEdit_PrecoMin.MenuManager = this.mainRibbonControl;
            this.textEdit_PrecoMin.Name = "textEdit_PrecoMin";
            this.textEdit_PrecoMin.Properties.Mask.EditMask = "c";
            this.textEdit_PrecoMin.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEdit_PrecoMin.Size = new System.Drawing.Size(730, 20);
            this.textEdit_PrecoMin.StyleController = this.dataLayoutControl1;
            this.textEdit_PrecoMin.TabIndex = 15;
            // 
            // simpleButton_GradeQtd
            // 
            this.simpleButton_GradeQtd.Location = new System.Drawing.Point(679, 318);
            this.simpleButton_GradeQtd.Name = "simpleButton_GradeQtd";
            this.simpleButton_GradeQtd.Size = new System.Drawing.Size(53, 22);
            this.simpleButton_GradeQtd.StyleController = this.dataLayoutControl1;
            this.simpleButton_GradeQtd.TabIndex = 17;
            this.simpleButton_GradeQtd.Text = "Adicionar";
            this.simpleButton_GradeQtd.Click += new System.EventHandler(this.simpleButton_GradeQtd_Click);
            // 
            // textEdit_GradeCorTamDesc
            // 
            this.textEdit_GradeCorTamDesc.Location = new System.Drawing.Point(300, 318);
            this.textEdit_GradeCorTamDesc.Name = "textEdit_GradeCorTamDesc";
            this.textEdit_GradeCorTamDesc.Size = new System.Drawing.Size(247, 20);
            this.textEdit_GradeCorTamDesc.StyleController = this.dataLayoutControl1;
            this.textEdit_GradeCorTamDesc.TabIndex = 19;
            // 
            // textEdit_TipoTamCor
            // 
            this.textEdit_TipoTamCor.Location = new System.Drawing.Point(145, 318);
            this.textEdit_TipoTamCor.Name = "textEdit_TipoTamCor";
            this.textEdit_TipoTamCor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.textEdit_TipoTamCor.Properties.Items.AddRange(new object[] {
            "Cor",
            "Tamanho"});
            this.textEdit_TipoTamCor.Size = new System.Drawing.Size(100, 20);
            this.textEdit_TipoTamCor.StyleController = this.dataLayoutControl1;
            this.textEdit_TipoTamCor.TabIndex = 20;
            // 
            // textEdit_GradeCorTamQuantidade
            // 
            this.textEdit_GradeCorTamQuantidade.Location = new System.Drawing.Point(612, 318);
            this.textEdit_GradeCorTamQuantidade.MenuManager = this.mainRibbonControl;
            this.textEdit_GradeCorTamQuantidade.Name = "textEdit_GradeCorTamQuantidade";
            this.textEdit_GradeCorTamQuantidade.Properties.Mask.EditMask = "n";
            this.textEdit_GradeCorTamQuantidade.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEdit_GradeCorTamQuantidade.Size = new System.Drawing.Size(63, 20);
            this.textEdit_GradeCorTamQuantidade.StyleController = this.dataLayoutControl1;
            this.textEdit_GradeCorTamQuantidade.TabIndex = 21;
            // 
            // textEdit_PrecoLiquido
            // 
            this.textEdit_PrecoLiquido.Location = new System.Drawing.Point(115, 118);
            this.textEdit_PrecoLiquido.Name = "textEdit_PrecoLiquido";
            this.textEdit_PrecoLiquido.Properties.Mask.EditMask = "c";
            this.textEdit_PrecoLiquido.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEdit_PrecoLiquido.Size = new System.Drawing.Size(730, 20);
            this.textEdit_PrecoLiquido.StyleController = this.dataLayoutControl1;
            this.textEdit_PrecoLiquido.TabIndex = 7;
            // 
            // comboBoxEdit_DescontosList
            // 
            this.comboBoxEdit_DescontosList.Location = new System.Drawing.Point(524, 166);
            this.comboBoxEdit_DescontosList.MenuManager = this.mainRibbonControl;
            this.comboBoxEdit_DescontosList.Name = "comboBoxEdit_DescontosList";
            this.comboBoxEdit_DescontosList.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit_DescontosList.Properties.ButtonPressed += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.comboBoxEdit_DescontosList_Properties_ButtonPressed);
            this.comboBoxEdit_DescontosList.Size = new System.Drawing.Size(321, 20);
            this.comboBoxEdit_DescontosList.StyleController = this.dataLayoutControl1;
            this.comboBoxEdit_DescontosList.TabIndex = 22;
            // 
            // simpleButton_DescontoAdd
            // 
            this.simpleButton_DescontoAdd.Location = new System.Drawing.Point(397, 166);
            this.simpleButton_DescontoAdd.Name = "simpleButton_DescontoAdd";
            this.simpleButton_DescontoAdd.Size = new System.Drawing.Size(123, 22);
            this.simpleButton_DescontoAdd.StyleController = this.dataLayoutControl1;
            this.simpleButton_DescontoAdd.TabIndex = 23;
            this.simpleButton_DescontoAdd.Text = "Add ->";
            this.simpleButton_DescontoAdd.Click += new System.EventHandler(this.simpleButton_DescontoAdd_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.emptySpaceItem3,
            this.layoutControlItem_Prod,
            this.layoutControlItem_TabelaPreco,
            this.layoutControlItem_Quantidade,
            this.layoutControlItem_PrecoBruto,
            this.layoutControlItem_Descontos,
            this.layoutControlItem_Moeda,
            this.layoutControlItem_CotacaoMoeda,
            this.layoutControlItem_Obs,
            this.layoutControlItem_TipoIPI,
            this.layoutControlItem_IPI,
            this.layoutControlItem_ST,
            this.layoutControlItem1,
            this.emptySpaceItem6,
            this.layoutControlItem_GradeQtd,
            this.layoutControlItem_TipoTamCor,
            this.layoutControlItem_GradeCorTamDesc,
            this.emptySpaceItem7,
            this.emptySpaceItem5,
            this.layoutControlItem_GradeCorTamQuantidade,
            this.layoutControlItem_PrecoMin,
            this.layoutControlItem_PrecoLiquido,
            this.layoutControlItem_DescontosList,
            this.emptySpaceItem4,
            this.layoutControlItem_descontoAdd});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(867, 385);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(10, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(827, 10);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(10, 355);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(827, 10);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(837, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(10, 365);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem_Prod
            // 
            this.layoutControlItem_Prod.Control = this.textEdit_Prod;
            this.layoutControlItem_Prod.Location = new System.Drawing.Point(10, 10);
            this.layoutControlItem_Prod.Name = "layoutControlItem_Prod";
            this.layoutControlItem_Prod.Size = new System.Drawing.Size(827, 24);
            this.layoutControlItem_Prod.Text = "Produto";
            this.layoutControlItem_Prod.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem_TabelaPreco
            // 
            this.layoutControlItem_TabelaPreco.Control = this.textEdit_TabelaPreco;
            this.layoutControlItem_TabelaPreco.Location = new System.Drawing.Point(10, 34);
            this.layoutControlItem_TabelaPreco.Name = "layoutControlItem_TabelaPreco";
            this.layoutControlItem_TabelaPreco.Size = new System.Drawing.Size(827, 24);
            this.layoutControlItem_TabelaPreco.Text = "Tabela de Preço";
            this.layoutControlItem_TabelaPreco.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem_Quantidade
            // 
            this.layoutControlItem_Quantidade.Control = this.textEdit_Quantidade;
            this.layoutControlItem_Quantidade.Location = new System.Drawing.Point(10, 58);
            this.layoutControlItem_Quantidade.Name = "layoutControlItem_Quantidade";
            this.layoutControlItem_Quantidade.Size = new System.Drawing.Size(827, 24);
            this.layoutControlItem_Quantidade.Text = "Quantidade";
            this.layoutControlItem_Quantidade.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem_PrecoBruto
            // 
            this.layoutControlItem_PrecoBruto.Control = this.textEdit_PrecoBruto;
            this.layoutControlItem_PrecoBruto.Location = new System.Drawing.Point(10, 82);
            this.layoutControlItem_PrecoBruto.Name = "layoutControlItem_PrecoBruto";
            this.layoutControlItem_PrecoBruto.Size = new System.Drawing.Size(827, 24);
            this.layoutControlItem_PrecoBruto.Text = "Preço Bruto";
            this.layoutControlItem_PrecoBruto.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem_Descontos
            // 
            this.layoutControlItem_Descontos.Control = this.textEdit_Descontos;
            this.layoutControlItem_Descontos.Location = new System.Drawing.Point(10, 154);
            this.layoutControlItem_Descontos.Name = "layoutControlItem_Descontos";
            this.layoutControlItem_Descontos.Size = new System.Drawing.Size(375, 26);
            this.layoutControlItem_Descontos.Text = "Descontos";
            this.layoutControlItem_Descontos.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem_Moeda
            // 
            this.layoutControlItem_Moeda.Control = this.textEdit_Moeda;
            this.layoutControlItem_Moeda.Location = new System.Drawing.Point(10, 180);
            this.layoutControlItem_Moeda.Name = "layoutControlItem_Moeda";
            this.layoutControlItem_Moeda.Size = new System.Drawing.Size(271, 24);
            this.layoutControlItem_Moeda.Text = "Moeda";
            this.layoutControlItem_Moeda.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem_CotacaoMoeda
            // 
            this.layoutControlItem_CotacaoMoeda.Control = this.textEdit_CotacaoMoeda;
            this.layoutControlItem_CotacaoMoeda.Location = new System.Drawing.Point(281, 180);
            this.layoutControlItem_CotacaoMoeda.Name = "layoutControlItem_CotacaoMoeda";
            this.layoutControlItem_CotacaoMoeda.Size = new System.Drawing.Size(556, 24);
            this.layoutControlItem_CotacaoMoeda.Text = "Cotação da Moeda";
            this.layoutControlItem_CotacaoMoeda.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem_Obs
            // 
            this.layoutControlItem_Obs.Control = this.memoEdit_Obs;
            this.layoutControlItem_Obs.Location = new System.Drawing.Point(10, 204);
            this.layoutControlItem_Obs.Name = "layoutControlItem_Obs";
            this.layoutControlItem_Obs.Size = new System.Drawing.Size(827, 25);
            this.layoutControlItem_Obs.Text = "Observação";
            this.layoutControlItem_Obs.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem_TipoIPI
            // 
            this.layoutControlItem_TipoIPI.Control = this.textEdit_TipoIPI;
            this.layoutControlItem_TipoIPI.Location = new System.Drawing.Point(10, 229);
            this.layoutControlItem_TipoIPI.Name = "layoutControlItem_TipoIPI";
            this.layoutControlItem_TipoIPI.Size = new System.Drawing.Size(271, 24);
            this.layoutControlItem_TipoIPI.Text = "Tipo IPI";
            this.layoutControlItem_TipoIPI.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem_IPI
            // 
            this.layoutControlItem_IPI.Control = this.textEdit_IPI;
            this.layoutControlItem_IPI.Location = new System.Drawing.Point(281, 229);
            this.layoutControlItem_IPI.Name = "layoutControlItem_IPI";
            this.layoutControlItem_IPI.Size = new System.Drawing.Size(556, 24);
            this.layoutControlItem_IPI.Text = "IPI";
            this.layoutControlItem_IPI.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem_ST
            // 
            this.layoutControlItem_ST.Control = this.textEdit_ST;
            this.layoutControlItem_ST.Location = new System.Drawing.Point(10, 253);
            this.layoutControlItem_ST.Name = "layoutControlItem_ST";
            this.layoutControlItem_ST.Size = new System.Drawing.Size(827, 24);
            this.layoutControlItem_ST.Text = "ST";
            this.layoutControlItem_ST.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.listBoxControl_GradeCorTam;
            this.layoutControlItem1.Location = new System.Drawing.Point(10, 332);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(827, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(10, 277);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(827, 29);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem_GradeQtd
            // 
            this.layoutControlItem_GradeQtd.Control = this.simpleButton_GradeQtd;
            this.layoutControlItem_GradeQtd.Location = new System.Drawing.Point(667, 306);
            this.layoutControlItem_GradeQtd.Name = "layoutControlItem_GradeQtd";
            this.layoutControlItem_GradeQtd.Size = new System.Drawing.Size(57, 26);
            this.layoutControlItem_GradeQtd.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem_GradeQtd.TextVisible = false;
            // 
            // layoutControlItem_TipoTamCor
            // 
            this.layoutControlItem_TipoTamCor.Control = this.textEdit_TipoTamCor;
            this.layoutControlItem_TipoTamCor.CustomizationFormText = "Valor";
            this.layoutControlItem_TipoTamCor.Location = new System.Drawing.Point(108, 306);
            this.layoutControlItem_TipoTamCor.Name = "layoutControlItem_TipoTamCor";
            this.layoutControlItem_TipoTamCor.Size = new System.Drawing.Size(129, 26);
            this.layoutControlItem_TipoTamCor.Text = "Tipo";
            this.layoutControlItem_TipoTamCor.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem_TipoTamCor.TextSize = new System.Drawing.Size(20, 13);
            this.layoutControlItem_TipoTamCor.TextToControlDistance = 5;
            // 
            // layoutControlItem_GradeCorTamDesc
            // 
            this.layoutControlItem_GradeCorTamDesc.Control = this.textEdit_GradeCorTamDesc;
            this.layoutControlItem_GradeCorTamDesc.CustomizationFormText = "Nome";
            this.layoutControlItem_GradeCorTamDesc.Location = new System.Drawing.Point(237, 306);
            this.layoutControlItem_GradeCorTamDesc.Name = "layoutControlItem_GradeCorTamDesc";
            this.layoutControlItem_GradeCorTamDesc.Size = new System.Drawing.Size(302, 26);
            this.layoutControlItem_GradeCorTamDesc.Text = "Descrição";
            this.layoutControlItem_GradeCorTamDesc.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem_GradeCorTamDesc.TextSize = new System.Drawing.Size(46, 13);
            this.layoutControlItem_GradeCorTamDesc.TextToControlDistance = 5;
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(10, 306);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(98, 26);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(724, 306);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(113, 26);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem_GradeCorTamQuantidade
            // 
            this.layoutControlItem_GradeCorTamQuantidade.Control = this.textEdit_GradeCorTamQuantidade;
            this.layoutControlItem_GradeCorTamQuantidade.Location = new System.Drawing.Point(539, 306);
            this.layoutControlItem_GradeCorTamQuantidade.Name = "layoutControlItem_GradeCorTamQuantidade";
            this.layoutControlItem_GradeCorTamQuantidade.Size = new System.Drawing.Size(128, 26);
            this.layoutControlItem_GradeCorTamQuantidade.Text = "Quantidade";
            this.layoutControlItem_GradeCorTamQuantidade.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem_GradeCorTamQuantidade.TextSize = new System.Drawing.Size(56, 13);
            this.layoutControlItem_GradeCorTamQuantidade.TextToControlDistance = 5;
            // 
            // layoutControlItem_PrecoMin
            // 
            this.layoutControlItem_PrecoMin.Control = this.textEdit_PrecoMin;
            this.layoutControlItem_PrecoMin.Location = new System.Drawing.Point(10, 130);
            this.layoutControlItem_PrecoMin.Name = "layoutControlItem_PrecoMin";
            this.layoutControlItem_PrecoMin.Size = new System.Drawing.Size(827, 24);
            this.layoutControlItem_PrecoMin.Text = "Preço Mínimo";
            this.layoutControlItem_PrecoMin.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem_PrecoLiquido
            // 
            this.layoutControlItem_PrecoLiquido.Control = this.textEdit_PrecoLiquido;
            this.layoutControlItem_PrecoLiquido.CustomizationFormText = "Preço Líquido";
            this.layoutControlItem_PrecoLiquido.Location = new System.Drawing.Point(10, 106);
            this.layoutControlItem_PrecoLiquido.Name = "layoutControlItem_PrecoLiquido";
            this.layoutControlItem_PrecoLiquido.Size = new System.Drawing.Size(827, 24);
            this.layoutControlItem_PrecoLiquido.Text = "Preço Líquido";
            this.layoutControlItem_PrecoLiquido.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem_DescontosList
            // 
            this.layoutControlItem_DescontosList.Control = this.comboBoxEdit_DescontosList;
            this.layoutControlItem_DescontosList.Location = new System.Drawing.Point(512, 154);
            this.layoutControlItem_DescontosList.Name = "layoutControlItem_DescontosList";
            this.layoutControlItem_DescontosList.Size = new System.Drawing.Size(325, 26);
            this.layoutControlItem_DescontosList.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem_DescontosList.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(10, 365);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem_descontoAdd
            // 
            this.layoutControlItem_descontoAdd.Control = this.simpleButton_DescontoAdd;
            this.layoutControlItem_descontoAdd.Location = new System.Drawing.Point(385, 154);
            this.layoutControlItem_descontoAdd.Name = "layoutControlItem_descontoAdd";
            this.layoutControlItem_descontoAdd.Size = new System.Drawing.Size(127, 26);
            this.layoutControlItem_descontoAdd.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem_descontoAdd.TextVisible = false;
            // 
            // FormPedidoItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(867, 528);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.mainRibbonControl);
            this.Name = "FormPedidoItem";
            this.Ribbon = this.mainRibbonControl;
            this.Load += new System.EventHandler(this.FormPedidoItem_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControl_GradeCorTam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_Prod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainRibbonControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meusPedidosDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_TabelaPreco.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_Quantidade.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_PrecoBruto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_Descontos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_Moeda.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CotacaoMoeda.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_Obs.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_IPI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_TipoIPI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ST.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_PrecoMin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_GradeCorTamDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_TipoTamCor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_GradeCorTamQuantidade.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_PrecoLiquido.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit_DescontosList.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_Prod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_TabelaPreco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_Quantidade)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_PrecoBruto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_Descontos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_Moeda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_CotacaoMoeda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_Obs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_TipoIPI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_IPI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_ST)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_GradeQtd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_TipoTamCor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_GradeCorTamDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_GradeCorTamQuantidade)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_PrecoMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_PrecoLiquido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_DescontosList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_descontoAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonControl mainRibbonControl;
        private DevExpress.XtraBars.Ribbon.RibbonPage mainRibbonPage;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup mainRibbonPageGroup;
        private DevExpress.XtraBars.BarButtonItem bbiSave;
        private DevExpress.XtraBars.BarButtonItem bbiSaveAndClose;
        private DevExpress.XtraBars.BarButtonItem bbiSaveAndNew;
        private DevExpress.XtraBars.BarButtonItem bbiReset;
        private DevExpress.XtraBars.BarButtonItem bbiDelete;
        private DevExpress.XtraBars.BarButtonItem bbiClose;
        private DevExpress.XtraEditors.LookUpEdit textEdit_Prod;
        private DevExpress.XtraEditors.LookUpEdit textEdit_TabelaPreco;
        private DevExpress.XtraEditors.TextEdit textEdit_Quantidade;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_Prod;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_TabelaPreco;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_Quantidade;
        private DevExpress.XtraEditors.TextEdit textEdit_PrecoBruto;
        private DevExpress.XtraEditors.TextEdit textEdit_Descontos;
        private DevExpress.XtraEditors.ComboBoxEdit textEdit_Moeda;
        private DevExpress.XtraEditors.TextEdit textEdit_CotacaoMoeda;
        private DevExpress.XtraEditors.MemoEdit memoEdit_Obs;
        private DevExpress.XtraEditors.TextEdit textEdit_IPI;
        private DevExpress.XtraEditors.ComboBoxEdit textEdit_TipoIPI;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_PrecoBruto;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_Descontos;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_Moeda;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_CotacaoMoeda;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_Obs;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_TipoIPI;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_IPI;
        private DevExpress.XtraEditors.TextEdit textEdit_ST;
        private DevExpress.XtraEditors.TextEdit textEdit_PrecoMin;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_ST;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_PrecoMin;
        private MeusPedidosDataSet meusPedidosDataSet;
        private DevExpress.XtraEditors.ListBoxControl listBoxControl_GradeCorTam;
        private DevExpress.XtraEditors.SimpleButton simpleButton_GradeQtd;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_GradeQtd;
        private DevExpress.XtraEditors.TextEdit textEdit_GradeCorTamDesc;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_TipoTamCor;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_GradeCorTamDesc;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraEditors.ComboBoxEdit textEdit_TipoTamCor;
        private DevExpress.XtraEditors.TextEdit textEdit_GradeCorTamQuantidade;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_GradeCorTamQuantidade;
        private DevExpress.XtraEditors.TextEdit textEdit_PrecoLiquido;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_PrecoLiquido;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit_DescontosList;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_DescontosList;
        private DevExpress.Utils.Behaviors.BehaviorManager behaviorManager1;
        private DevExpress.XtraEditors.SimpleButton simpleButton_DescontoAdd;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_descontoAdd;
    }
}
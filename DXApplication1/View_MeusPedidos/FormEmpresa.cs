﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Control;
using Core;
using DevExpress.XtraEditors;
using System.Data.Entity;
using Control.Validations;
using DevExpress.XtraGrid.Views.Grid;
using System.Globalization;
using DevExpress.XtraBars;
using System.Configuration;

namespace View_MeusPedidos
{
    public partial class FormEmpresa : DevExpress.XtraEditors.XtraForm
    {
        public Empresa_Service s_empresa;
        public Empresa empresa;
        PopupMenu menu;
        Empresa emp_validacao;

        public FormEmpresa()
        {
            InitializeComponent();
            s_empresa = new Empresa_Service();

            bar1.Visible = false;
            bar2.Visible = false;

            var empresas_active = s_empresa.GetAll_Empresas();
            foreach(var emp in empresas_active)
            {
                textEdit_EmpresaRazaoSocial.Text = emp.razao_social;
                textEdit_EmpresaSenha.Text = emp.password;
                textEdit_EmpresaCNPJ.Text = emp.cnpj;
                textEdit_EmpresaQtdLicencas.Text = emp.license_quantity.ToString();
                dateEdit_EmpresaUltimaValidacao.EditValue = emp.ultima_validacao;
                dateEdit_EmpresaUltimaValidacao.Text = emp.ultima_validacao.ToString();
                dateEdit_UsuarioDtCriacao.EditValue = emp.created_at;
                dateEdit_UsuarioDtCriacao.Text = emp.created_at.ToString();
                textEdit_EmpresaSysVersion.Text = emp.system_version;
                textEdit_EmpresaComentarios.Text = emp.comments;
                textEdit_EmpresaExcluido.IsOn = Convert.ToBoolean(emp.in_trash);
                toggleSwitch_EmpresaAtivo.IsOn = Convert.ToBoolean(emp.active);
                textEdit_EmpresaTolerancia.Text = emp.expire_tolerance.ToString();
            }

            tabNavigationPage_DataGridEmpresa.PageVisible = false;
            tabNavigationPage_RegisterEmpresa.PageVisible = true;
            tabPane_ConfigEmpresa.SelectedPage = tabNavigationPage_RegisterEmpresa;

            gridControl_Empresa.DataSource = s_empresa.GetAll_Empresas();
        }


        #region ToolBar1 Events - Adicionar/Editar/Deletar
        /*
         private void barButtonItem_Add_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            add_status = true;

            //UI Behavior

            FormContols_FW.fieldsClean(layoutControl_Usuario);

            bar1.Visible = false;
            bar2.Visible = false;

            tabNavigationPage_RegisterUsuario.PageVisible = true;
            tabNavigationPage_DataGridUsuario.PageVisible = false;
            tabPane_ConfigUsuario.SelectedPage = tabNavigationPage_RegisterUsuario;

            //Define TextEdit Fields as ReadOnly = false
            FormContols_FW.fieldsReadOnly(layoutControl_Usuario, false);
        }

        private void barButtonItem_Edit_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            add_status = false;

            var row = gridView_Usuario.GetRow(gridView_Usuario.GetFocusedDataSourceRowIndex());
            empresa = (Usuario)row;

            if (empresa != null)
            {
                if (XtraMessageBox.Show("Tem certeza que deseja alterar o registro (ID: " + empresa.id + ")?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    //Retrieve entity ID
                    _id = empresa.id;

                    textEdit_UsuarioTipo.Text = empresa.type;
                    textEdit_UsuarioEmail.Text = empresa.email;
                    textEdit_UsuarioSenha.Text = empresa.password;
                    textEdit_UsuarioCPF.Text = empresa.cpf;
                    textEdit_UsuarioCNPJ.Text = empresa.cnpj;
                    textEdit_UsuarioComentarios.Text = empresa.comments;
                    textEdit_UsuarioDtNasc.EditValue = empresa.birth;
                    textEdit_UsuarioExcluido.EditValue = empresa.in_trash;
                    textEdit_UsuarioGenero.EditValue = empresa.gender;
                    textEdit_UsuarioSMS.Text = empresa.phone;
                    textEdit_UsuarioUltNome.Text = empresa.last_name;
                    textEdit_UsuarioPNome.Text = empresa.first_name;
                    textEdit_UsuarioGenero.Text = empresa.gender;
                    dateEdit_UsuarioDtCriacao.EditValue = empresa.created_at;

                    //UI Behavior
                    bar1.Visible = false;
                    bar2.Visible = false;

                    tabNavigationPage_RegisterUsuario.PageVisible = true;
                    tabNavigationPage_DataGridUsuario.PageVisible = false;
                    tabPane_ConfigUsuario.SelectedPage = tabNavigationPage_RegisterUsuario;

                    //Define TextEdit Fields as ReadOnly = false
                    FormContols_FW.fieldsReadOnly(layoutControl_Usuario, false);
                }
            }
        }

        private void barButtonItem_Del_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var row = gridView_Usuario.GetRow(gridView_Usuario.GetFocusedDataSourceRowIndex());
            empresa = (Usuario)row;

            if (empresa != null)
            {
                if (XtraMessageBox.Show("Tem certeza que deseja excluir o registro (ID: " + empresa.id + ")?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    empresa.in_trash = Convert.ToByte(true);
                    s_empresa.Update_Usuario(empresa);

                    gridControl_Usuario.DataSource = s_empresa.GetAll_Usuarios();
                }
            }
        }
        */
        #endregion

        #region ToolBar2 Events - Cancelar/Gravar
        /*
        private void barButtonItem_Gravar_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (add_status == true)
            {
                empresa = new Usuario();

                empresa.birth = textEdit_UsuarioDtNasc.DateTime;
                empresa.cnpj = textEdit_UsuarioCNPJ.Text;
                empresa.cpf = textEdit_UsuarioCPF.Text;
                empresa.comments = textEdit_UsuarioComentarios.Text;
                empresa.created_at = dateEdit_UsuarioDtCriacao.DateTime;
                empresa.email = textEdit_UsuarioEmail.Text;
                empresa.first_name = textEdit_UsuarioPNome.Text;
                empresa.last_name = textEdit_UsuarioUltNome.Text;
                empresa.phone = textEdit_UsuarioSMS.Text;
                empresa.password = ConvertMD5_V.CalculateMD5Hash(textEdit_UsuarioSenha.Text);
                empresa.gender = textEdit_UsuarioGenero.Text;
                empresa.in_trash = Convert.ToByte(textEdit_UsuarioExcluido.IsOn);
                empresa.created_at = DateTime.Now.Date;

                //Validação Nome
                var aux = s_empresa.GetBySpecification_Usuario(empresa.id);

                if (aux == null)
                {
                    if (!splashScreenManager_empresa.IsSplashFormVisible)
                    {
                        splashScreenManager_empresa.ShowWaitForm();
                    }

                    //Add new ProdCat
                    s_empresa.AddNew_Usuario(empresa);

                    splashScreenManager_empresa.CloseWaitForm();

                    XtraMessageBox.Show("Registro salvo!", "Informação");

                    FormContols_FW.fieldsClean(layoutControl_Usuario);

                    gridControl_Usuario.DataSource = s_empresa.GetAll_Usuarios();

                    bar1.Visible = false;
                    bar2.Visible = false;
                    tabNavigationPage_RegisterUsuario.PageVisible = false;
                    tabNavigationPage_DataGridUsuario.PageVisible = true;
                    tabPane_ConfigUsuario.SelectedPage = tabNavigationPage_DataGridUsuario;
                }
                else
                {
                    splashScreenManager_empresa.CloseWaitForm();

                    XtraMessageBox.Show("Já existe uma transportadora com este nome cadastrado!", "Atenção");
                }

            }
            else if (add_status == false &&
                    textEdit_UsuarioEmail.Text != null &&
                    textEdit_UsuarioSenha.Text != null &&
                    textEdit_UsuarioDtNasc.Text != "")
            {
                splashScreenManager_empresa.ShowWaitForm();

                empresa = new Usuario();
                empresa.id = _id;

                empresa = s_empresa.GetBySpecification_Usuario(empresa.id);

                empresa.birth = textEdit_UsuarioDtNasc.DateTime;
                empresa.cnpj = textEdit_UsuarioCNPJ.Text;
                empresa.cpf = textEdit_UsuarioCPF.Text;
                empresa.comments = textEdit_UsuarioComentarios.Text;
                empresa.created_at = dateEdit_UsuarioDtCriacao.DateTime;
                empresa.email = textEdit_UsuarioEmail.Text;
                empresa.first_name = textEdit_UsuarioPNome.Text;
                empresa.last_name = textEdit_UsuarioUltNome.Text;
                empresa.phone = textEdit_UsuarioSMS.Text;
                empresa.password = ConvertMD5_V.CalculateMD5Hash(textEdit_UsuarioSenha.Text);
                empresa.gender = textEdit_UsuarioGenero.Text;
                empresa.in_trash = Convert.ToByte(textEdit_UsuarioExcluido.IsOn);

                s_empresa.Update_Usuario(empresa);

                splashScreenManager_empresa.CloseWaitForm();

                XtraMessageBox.Show("Alteração realizada com sucesso!", "Informação");

                FormContols_FW.fieldsClean(layoutControl_Usuario);

                gridControl_Usuario.DataSource = s_empresa.GetAll_Usuarios();

                bar1.Visible = false;
                bar2.Visible = false;
                tabNavigationPage_RegisterUsuario.PageVisible = false;
                tabNavigationPage_DataGridUsuario.PageVisible = true;
                tabPane_ConfigUsuario.SelectedPage = tabNavigationPage_DataGridUsuario;
            }
            else
            {
                XtraMessageBox.Show("Algum campo obrigatório precisa ser preenchido!", "Informação");
            }
        }
        */

        private void barButtonItem_Cancelar_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (XtraMessageBox.Show("Tem certeza que deseja sair da tela?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                barButtonItem_Gravar.Enabled = true;

                bar1.Visible = false;
                bar2.Visible = false;
                tabNavigationPage_RegisterEmpresa.PageVisible = false;
                tabNavigationPage_DataGridEmpresa.PageVisible = true;
                tabPane_ConfigEmpresa.SelectedPage = tabNavigationPage_DataGridEmpresa;

                FormContols_FW.fieldsClean(layoutControl_Empresa);
            }
        }
        #endregion

        #region Grid Empresa Events
        private void gridView_Clientes_DoubleClick(object sender, EventArgs e)
        {
            barButtonItem_Gravar.Enabled = false;

            //Get (object) row values
            var row = gridView_Empresa.GetRow(gridView_Empresa.GetFocusedDataSourceRowIndex());
            empresa = (Empresa)row;

            if(empresa != null)
            {
                textEdit_EmpresaRazaoSocial.Text = empresa.razao_social;
                textEdit_EmpresaSenha.Text = empresa.password;
                textEdit_EmpresaCNPJ.Text = empresa.cnpj;
                textEdit_EmpresaQtdLicencas.Text = empresa.license_quantity.ToString();
                dateEdit_EmpresaUltimaValidacao.EditValue = empresa.ultima_validacao;
                dateEdit_EmpresaUltimaValidacao.Text = empresa.ultima_validacao.ToString();
                dateEdit_UsuarioDtCriacao.EditValue = empresa.created_at;
                dateEdit_UsuarioDtCriacao.Text = empresa.created_at.ToString();
                textEdit_EmpresaSysVersion.Text = empresa.system_version;
                textEdit_EmpresaComentarios.Text = empresa.comments;
                textEdit_EmpresaExcluido.IsOn = Convert.ToBoolean(empresa.in_trash);
                toggleSwitch_EmpresaAtivo.IsOn = Convert.ToBoolean(empresa.active);
                textEdit_EmpresaTolerancia.Text = empresa.expire_tolerance.ToString();
            }
            
            //Define TextEdit Fields as ReadOnly = true
            FormContols_FW.fieldsReadOnly(layoutControl_Empresa, true);

            //UI Behavior
            bar1.Visible = false;
            bar2.Visible = true;

            tabNavigationPage_RegisterEmpresa.PageVisible = true;
            tabNavigationPage_DataGridEmpresa.PageVisible = false;
            tabPane_ConfigEmpresa.SelectedPage = tabNavigationPage_RegisterEmpresa;
        }
        #endregion

        private void gridView_Empresa_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                if (barManager_Empresa == null)
                {
                    barManager_Empresa = new BarManager();
                    barManager_Empresa.Form = this;
                }
                if (menu == null)
                    menu = new PopupMenu(barManager_Empresa);
                // Add item and show
                menu.ItemLinks.Clear();
                BarButtonItem item_atualizar = new BarButtonItem(barManager_Empresa, "Atualizar");
                item_atualizar.ItemClick += new ItemClickEventHandler(gridView_RightClick);
                menu.AddItems(new BarItem[] { item_atualizar });
                menu.ShowPopup(Cursor.Position);
            }
        }
        void gridView_RightClick(object sender, EventArgs e)
        {
            //MessageBox.Show("Click");
            splashScreenManager_User.ShowWaitForm();
            gridControl_Empresa.DataSource = s_empresa.GetAll_Empresas();

            if(splashScreenManager_User.IsSplashFormVisible)
            {
                splashScreenManager_User.CloseWaitForm();
            }
        }

        private void FormEmpresa_Load(object sender, EventArgs e)
        {
            gridControl_Empresa.DataSource = s_empresa.GetAll_Empresas();
        }

        private void simpleButton_validarCNPJ_Click(object sender, EventArgs e)
        {
            var retorno_validacao = SyncKMS_.ExecuteByUser(textEdit_EmpresaCNPJ.Text);
            //emp_validacao = SyncKMS_.ExecuteByUser(textEdit_EmpresaCNPJ.Text);

            var type = retorno_validacao.GetType();

            switch (type.Name)
            {
                case "String":
                    XtraMessageBox.Show(retorno_validacao.ToString());
                    break;

                case "Empresa":
                    if (emp_validacao != null)
                    {
                        emp_validacao.ultima_validacao = Encryption_V.Encrypt(DateTime.Now.ToString());

                        var emp_ret = s_empresa.GetByCnpj_Empresa(emp_validacao.cnpj);

                        textEdit_EmpresaRazaoSocial.Text = emp_validacao.razao_social;
                        textEdit_EmpresaSenha.Text = emp_validacao.password;
                        textEdit_EmpresaCNPJ.Text = emp_validacao.cnpj;
                        textEdit_EmpresaQtdLicencas.Text = emp_validacao.license_quantity.ToString();
                        dateEdit_EmpresaUltimaValidacao.EditValue = emp_validacao.ultima_validacao;
                        dateEdit_EmpresaUltimaValidacao.Text = emp_validacao.ultima_validacao.ToString();
                        dateEdit_UsuarioDtCriacao.EditValue = emp_ret.created_at;
                        dateEdit_UsuarioDtCriacao.Text = emp_ret.created_at.ToString();
                        textEdit_EmpresaSysVersion.Text = emp_validacao.system_version;
                        textEdit_EmpresaComentarios.Text = emp_validacao.comments;
                        textEdit_EmpresaExcluido.IsOn = Convert.ToBoolean(emp_validacao.in_trash);
                        toggleSwitch_EmpresaAtivo.IsOn = Convert.ToBoolean(emp_validacao.active);
                        textEdit_EmpresaTolerancia.Text = emp_validacao.expire_tolerance.ToString();

                        layoutControlItem_btnGravar.ContentVisible = true;
                        simpleButton_gravarEmpresa.Visible = true;
                        simpleButton_gravarEmpresa.BackColor = Color.Green;

                        XtraMessageBox.Show("CNPJ válido!");
                    }
                    else
                    {
                        XtraMessageBox.Show("CNPJ inválido!");
                    }
                    break;

                default:

                    break;
            }
            

            
        }

        private void simpleButton_gravarEmpresa_Click(object sender, EventArgs e)
        {
            var emp_ret = s_empresa.GetByCnpj_Empresa(emp_validacao.cnpj);
            if (XtraMessageBox.Show("Tem certeza que deseja salvar alteração no cadastro da empresa?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                //validar existencia da empresa no banco, caso exista fazer update, caso nao existe fazer insert
                if (emp_ret == null)
                {
                    empresa.ultima_validacao = Encryption_V.Encrypt(DateTime.Now.ToString());
                    s_empresa.AddNew_Empresa(empresa);

                    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    config.AppSettings.Settings.Remove("CNPJ");
                    config.AppSettings.Settings.Add("CNPJ", Encryption_V.Encrypt(empresa.cnpj));
                    config.Save(ConfigurationSaveMode.Modified);
                    ConfigurationManager.RefreshSection("appSettings");
                }
                else
                {
                    emp_ret.login = emp_validacao.login;
                    emp_ret.password = emp_validacao.password;
                    emp_ret.ultima_validacao = Encryption_V.Encrypt(DateTime.Now.ToString());
                    s_empresa.Update_Empresa(emp_ret);

                    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    config.AppSettings.Settings.Remove("CNPJ");
                    config.AppSettings.Settings.Add("CNPJ", Encryption_V.Encrypt(emp_ret.cnpj));
                    config.Save(ConfigurationSaveMode.Modified);
                    ConfigurationManager.RefreshSection("appSettings");
                }

                layoutControlItem_btnGravar.ContentVisible = false;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Control;
using Core;
using DevExpress.XtraEditors;
using System.Data.Entity;
using Control.Validations;
using DevExpress.XtraGrid.Views.Grid;
using System.Globalization;
using System.Text.RegularExpressions;
using DevExpress.XtraEditors.Controls;

namespace View_MeusPedidos
{
    public partial class FormClienteCategoria : DevExpress.XtraEditors.XtraForm
    {
        ClienteCategoria_Service s_ccp;
        ClienteCategoria ccp;
        Cliente_Service s_cli;
        ProdutoCategoria_Service s_prodcat;

        int _id;
        bool add_status = false;
        string clientestp_ids;

        public FormClienteCategoria()
        {
            InitializeComponent();
            s_ccp = new ClienteCategoria_Service();
            s_cli = new Cliente_Service();
            s_prodcat = new ProdutoCategoria_Service();

            bar_Acoes2.Visible = false;

            tabNavigationPage_RegisterCliCat.PageVisible = false;
            tabPane_ConfigCliCat.SelectedPage = tabNavigationPage_DataGridCliCat;

            gridControl_CliCat.DataSource = s_ccp.GetAll_ClientesCategorias();

            BindingSource srcCli = new BindingSource();
            srcCli.DataSource = s_cli.GetAll_Clientes();
            this.comboBoxEdit_CliCat_Cli.Properties.DataSource = srcCli.List;
            this.comboBoxEdit_CliCat_Cli.Properties.Columns.Add(new LookUpColumnInfo("id", "Id"));
            this.comboBoxEdit_CliCat_Cli.Properties.Columns.Add(new LookUpColumnInfo("razao_social", "Nome"));
            this.comboBoxEdit_CliCat_Cli.Properties.DisplayMember = "razao_social";
            this.comboBoxEdit_CliCat_Cli.Properties.ValueMember = "id";

            BindingSource srcTabPreco = new BindingSource();
            srcTabPreco.DataSource = s_prodcat.GetAll_ProdutoCategorias();
            this.comboBoxEdit_CliCat_Cat.Properties.DataSource = srcTabPreco.List;
            this.comboBoxEdit_CliCat_Cat.Properties.Columns.Add(new LookUpColumnInfo("id", "Id"));
            this.comboBoxEdit_CliCat_Cat.Properties.Columns.Add(new LookUpColumnInfo("nome", "Nome"));
            this.comboBoxEdit_CliCat_Cat.Properties.DisplayMember = "nome";
            this.comboBoxEdit_CliCat_Cat.Properties.ValueMember = "id";
        }

        #region ToolBar1 Events - Adicionar/Editar/Deletar

        private void barButtonItem_Add_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            add_status = true;

            //UI Behavior

            FormContols_FW.fieldsClean(layoutControl_CliCat);

            bar_Acoes.Visible = false;
            bar_Acoes2.Visible = true;

            tabNavigationPage_RegisterCliCat.PageVisible = true;
            tabNavigationPage_DataGridCliCat.PageVisible = false;
            tabPane_ConfigCliCat.SelectedPage = tabNavigationPage_RegisterCliCat;

            //Define TextEdit Fields as ReadOnly = false
            FormContols_FW.fieldsReadOnly(layoutControl_CliCat, false);
        }

        private void barButtonItem_Edit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            add_status = false;

            var row = gridView_CliCat.GetRow(gridView_CliCat.GetFocusedDataSourceRowIndex());
            ccp = (ClienteCategoria)row;

            if(ccp != null)
            {
                if (XtraMessageBox.Show("Tem certeza que deseja alterar o registro (ID: " + ccp.id + ")?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    //Retrieve entity ID
                    _id = ccp.id;

                    //Fill TextEdit Fields
                    comboBoxEdit_CliCat_Cli.EditValue = ccp.cliente_id.ToString();
                    textEdit_CliCPSelectTabPreco.Text = ccp.categorias_liberadas.ToString();

                    //UI Behavior
                    bar_Acoes.Visible = false;
                    bar_Acoes2.Visible = true;

                    tabNavigationPage_RegisterCliCat.PageVisible = true;
                    tabNavigationPage_DataGridCliCat.PageVisible = false;
                    tabPane_ConfigCliCat.SelectedPage = tabNavigationPage_RegisterCliCat;

                    //Define TextEdit Fields as ReadOnly = false
                    FormContols_FW.fieldsReadOnly(layoutControl_CliCat, false);
                }
            }
            
        }

        private void barButtonItem_Del_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var row = gridView_CliCat.GetRow(gridView_CliCat.GetFocusedDataSourceRowIndex());
            ccp = (ClienteCategoria)row;
            
            if(ccp != null)
            {
                if (XtraMessageBox.Show("Tem certeza que deseja excluir o registro (ID: " + ccp.id + ")?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    //ccp.excluido = true;
                    s_ccp.Update_ClienteCategoria(ccp);

                    gridControl_CliCat.DataSource = s_ccp.GetAll_ClientesCategorias();
                }
            }
            
        }
        #endregion

        #region ToolBar2 Events - Cancelar/Gravar
        private void barButtonItem_Gravar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (add_status == true &&
                comboBoxEdit_CliCat_Cli.Text != "" &&
                comboBoxEdit_CliCat_Cat.Text != "")
            {
                ccp = new ClienteCategoria();
                ccp.categorias_liberadas = clientestp_ids;
                ccp.categorias_liberadas_desc = textEdit_CliCPSelectTabPreco.Text;
                ccp.cliente_id = int.Parse(comboBoxEdit_CliCat_Cli.EditValue.ToString());
                ccp.mptran = "I";

                //Validação Nome
                var aux = s_ccp.GetByClienteID_ClienteCategoria(ccp.cliente_id);

                if (aux == null)
                {
                    splashScreenManager_Transp.ShowWaitForm();

                    //Add new ProdCat
                    s_ccp.AddNew_ClienteCategoria(ccp);

                    splashScreenManager_Transp.CloseWaitForm();

                    XtraMessageBox.Show("Registro salvo!", "Informação");
                }
                else
                {
                    if(splashScreenManager_Transp.IsSplashFormVisible)
                    {
                        splashScreenManager_Transp.CloseWaitForm();
                    }

                    if (XtraMessageBox.Show("Já existe um registro de Categorias para o cliente " + ccp.cliente_id +". Deseja deletar o registro existente e incluir novo registro?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var a = s_ccp.GetByClienteID_ClienteCategoria(ccp.cliente_id);

                        s_ccp.Delete_ClienteCategoria(a);

                        s_ccp.AddNew_ClienteCategoria(ccp);
                        XtraMessageBox.Show("Registro salvo!", "Informação");
                    }
                }

                FormContols_FW.fieldsClean(layoutControl_CliCat);
                clientestp_ids = "";

                gridControl_CliCat.DataSource = s_ccp.GetAll_ClientesCategorias();

                bar_Acoes.Visible = true;
                bar_Acoes2.Visible = false;
                tabNavigationPage_RegisterCliCat.PageVisible = false;
                tabNavigationPage_DataGridCliCat.PageVisible = true;
                tabPane_ConfigCliCat.SelectedPage = tabNavigationPage_DataGridCliCat;
            }
            /*
            else if (add_status == false &&
                textEdit_ProdTabPrecoValor.Text != "" &&
                comboBoxEdit_CliCP_Cli.Text != "" &&
                comboBoxEdit_CliCP_CpID.Text != "")
            {
                splashScreenManager_Transp.ShowWaitForm();

                ccp = new ClienteCategoria();
                ccp.id = _id;

                ccp = s_ccp.GetBySpecification_ClienteCategoria(ccp.id);

                ccp.preco = float.Parse(textEdit_ProdTabPrecoValor.EditValue.ToString(), CultureInfo.InvariantCulture.NumberFormat);
                ccp.tabela_id = int.Parse(comboBoxEdit_CliCP_CpID.EditValue.ToString());
                ccp.produto_id = int.Parse(comboBoxEdit_CliCP_Cli.EditValue.ToString());
                ccp.excluido = textEdit_CliCPExcluido.IsOn;
                ccp.ultima_alteracao = DateTime.Now;
                ccp.mptran = "A";

                s_ccp.Update_ClienteCategoria(ccp);

                splashScreenManager_Transp.CloseWaitForm();

                XtraMessageBox.Show("Alteração realizada com sucesso!", "Informação");

                FormContols_FW.fieldsClean(layoutControl_CliCP);

                gridControl_CliCP.DataSource = s_ccp.GetAll_ClienteCategoria();

                bar_Acoes.Visible = true;
                bar_Acoes2.Visible = false;
                tabNavigationPage_RegisterCliCP.PageVisible = false;
                tabNavigationPage_DataGridCliCP.PageVisible = true;
                tabPane_ConfigCliCP.SelectedPage = tabNavigationPage_DataGridCliCP;
            }
            */
            else
            {
                XtraMessageBox.Show("Algum campo obrigatório precisa ser preenchido!", "Informação");
            }
        }

        private void barButtonItem_Cancelar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (XtraMessageBox.Show("Tem certeza que deseja sair da tela?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                barButtonItem_Gravar.Enabled = true;

                bar_Acoes.Visible = true;
                bar_Acoes2.Visible = false;
                tabNavigationPage_RegisterCliCat.PageVisible = false;
                tabNavigationPage_DataGridCliCat.PageVisible = true;
                tabPane_ConfigCliCat.SelectedPage = tabNavigationPage_DataGridCliCat;

                FormContols_FW.fieldsClean(layoutControl_CliCat);
                clientestp_ids = "";
            }
        }
        #endregion

        #region Grid ProdutoTabelaPreço Events
        private void gridView_Clientes_DoubleClick(object sender, EventArgs e)
        {
            barButtonItem_Gravar.Enabled = false;

            //Get (object) row values
            var row = gridView_CliCat.GetRow(gridView_CliCat.GetFocusedDataSourceRowIndex());
            ccp = (ClienteCategoria)row;
            
            //Fill TextEdit Fields
            if(ccp != null)
            {
                comboBoxEdit_CliCat_Cli.EditValue = ccp.cliente_id.ToString();
                textEdit_CliCPSelectTabPreco.Text = ccp.categorias_liberadas.ToString();
            }
            

            //Define TextEdit Fields as ReadOnly = true
            FormContols_FW.fieldsReadOnly(layoutControl_CliCat, true);

            //UI Behavior
            bar_Acoes.Visible = false;
            bar_Acoes2.Visible = true;

            tabNavigationPage_RegisterCliCat.PageVisible = true;
            tabNavigationPage_DataGridCliCat.PageVisible = false;
            tabPane_ConfigCliCat.SelectedPage = tabNavigationPage_RegisterCliCat;
        }
        #endregion

        private void FormTabelaPreco_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'meusPedidosDataSet.ProdutoCategoria' table. You can move, or remove it, as needed.
            this.produtoCategoriaTableAdapter.Fill(this.meusPedidosDataSet.ProdutoCategoria);
            // TODO: This line of code loads data into the 'meusPedidosDataSet.ClienteCategoria' table. You can move, or remove it, as needed.
            this.clienteCategoriaTableAdapter.Fill(this.meusPedidosDataSet.ClienteCategoria);
            // TODO: This line of code loads data into the 'meusPedidosDataSet.Cliente' table. You can move, or remove it, as needed.
            this.clienteTableAdapter.Fill(this.meusPedidosDataSet.Cliente);
        }

        private void simpleButton_CliCPAdd_Click(object sender, EventArgs e)
        {
            if (!Regex.IsMatch(textEdit_CliCPSelectTabPreco.Text,comboBoxEdit_CliCat_Cat.Text))
            {
                textEdit_CliCPSelectTabPreco.Text = textEdit_CliCPSelectTabPreco.Text + " - " + comboBoxEdit_CliCat_Cat.Text;
                clientestp_ids = clientestp_ids + comboBoxEdit_CliCat_Cat.EditValue + ";";
            }
            else
            {
                XtraMessageBox.Show("Esta Tabela de Preço já foi incluída!", "Informação");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Control;
using Core;
using DevExpress.XtraEditors;
using System.Data.Entity;
using Control.Validations;
using DevExpress.XtraGrid.Views.Grid;
using Newtonsoft.Json;
using DevExpress.XtraEditors.Controls;

namespace View_MeusPedidos
{
    public partial class FormProduto : DevExpress.XtraEditors.XtraForm
    {
        Produto_Service s_prod;
        ProdutoCategoria_Service s_prodcat;
        Produto prod;

        int _id;
        bool add_status = false;

        public FormProduto()
        {
            InitializeComponent();
            s_prod = new Produto_Service();
            s_prodcat = new ProdutoCategoria_Service();

            comboBoxEdit_ProdTipoIpi.Properties.DataSource = Enum.GetValues(typeof(Core.IPI_Type));
            comboBoxEdit_ProdMoeda.Properties.DataSource = Enum.GetValues(typeof(Core.Moeda_Type));
            textEdit_ProdUnidade.Properties.DataSource = Enum.GetValues(typeof(Core.UnidadeM_Type));

            BindingSource src = new BindingSource();
            src.DataSource = s_prodcat.GetAll_ProdutoCategorias().ToList();
            this.comboBoxEdit_ProdCat.Properties.DataSource = src.List;
            this.comboBoxEdit_ProdCat.Properties.Columns.Add(new LookUpColumnInfo("id", "Id"));
            this.comboBoxEdit_ProdCat.Properties.Columns.Add(new LookUpColumnInfo("nome", "Nome"));
            this.comboBoxEdit_ProdCat.Properties.DisplayMember = "nome";
            this.comboBoxEdit_ProdCat.Properties.ValueMember = "id";

            bar_Acoes2.Visible = false;

            tabNavigationPage_RegisterProd.PageVisible = false;
            tabPane_ConfigProd.SelectedPage = tabNavigationPage_DataGridProd;

            gridControl_Prod.DataSource = s_prod.GetAll_FullProdutos();
        }

        #region ToolBar1 Events - Adicionar/Editar/Deletar

        private void barButtonItem_Add_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            add_status = true;

            //UI Behavior

            FormContols_FW.fieldsClean(layoutControl_Prod);

            bar_Acoes.Visible = false;
            bar_Acoes2.Visible = true;

            tabNavigationPage_RegisterProd.PageVisible = true;
            tabNavigationPage_DataGridProd.PageVisible = false;
            tabPane_ConfigProd.SelectedPage = tabNavigationPage_RegisterProd;

            //Define TextEdit Fields as ReadOnly = false
            FormContols_FW.fieldsReadOnly(layoutControl_Prod, false);
        }

        private void barButtonItem_Edit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            add_status = false;

            var row = gridView_Prod.GetRow(gridView_Prod.GetFocusedDataSourceRowIndex());

            if (row.GetType().Name == "DataRowView")
            {
                var rowView = (DataRowView)row;
                int prod_id = int.Parse(rowView.Row.ItemArray[0].ToString());
                prod = s_prod.GetBySpecification_Produto(prod_id);
            }
            else if (row.GetType().Name == "Produto")
            {
                var r = (Produto)row;
                prod = s_prod.GetBySpecification_Produto(r.id);
            }
            
            if(prod != null)
            {
                if (XtraMessageBox.Show("Tem certeza que deseja alterar o registro (ID: " + prod.id + ")?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    //Retrieve entity ID
                    _id = prod.id;

                    try
                    {
                        //Fill TextEdit Fields
                        textEdit_ProdNome.Text = prod.nome;
                        textEdit_ProdCod.Text = prod.codigo;
                        if(prod.comissao.ToString() != null)
                            textEdit_ProdComissao.Text = prod.comissao.ToString();
                        if (prod.codigo_ncm != null)
                            textEdit_ProdCodNCM.Text = prod.codigo_ncm.ToString();
                        if (prod.ipi.ToString() != null)
                            textEdit_ProdIpi.Text = prod.ipi.ToString();
                        if (prod.multiplo.ToString() != null)
                            textEdit_ProdMult.Text = prod.multiplo.ToString();
                        if (prod.peso_bruto.ToString() != null)
                            textEdit_ProdPesoBruto.Text = prod.peso_bruto.ToString();
                        if (prod.preco_minimo.ToString() != null)
                            textEdit_ProdPrecoMin.Text = prod.preco_minimo.ToString();
                        if(prod.preco_tabela.ToString() != null)
                            textEdit_ProdPrecoTab.Text = prod.preco_tabela.ToString();
                        if (prod.categoria_id.ToString() != null)
                            comboBoxEdit_ProdCat.EditValue = prod.categoria_id;
                        if (prod.grade_cores.Count > 0)
                        {
                            var list = s_prod.GetAll_FullProdutos();
                            List<string> strings = new List<string>();
                            foreach (var l in list)
                            {
                                if (l.id == prod.id)
                                {
                                    foreach(var ax in l.grade_cores)
                                    {
                                        GradeCor gc = ax;
                                        strings.Add(gc.cor);
                                    }
                                }
                            }
                            textEdit_ProdGradeCores.Text = String.Join(", ", strings.ToArray());
                        }
                        if (prod.grade_tamanhos.Count > 0)
                        {
                            var list = s_prod.GetAll_FullProdutos();
                            List<string> strings = new List<string>();
                            foreach (var l in list)
                            {
                                if (l.id == prod.id)
                                {
                                    foreach (var ax in l.grade_tamanhos)
                                    {
                                        GradeTamanho gt = ax;
                                        strings.Add(gt.tamanho);
                                    }
                                }
                            }
                            textEdit_ProdGradeTamanhos.Text = String.Join(", ", strings.ToArray());
                        }
                        textEdit_ProdUnidade.Text = prod.unidade;
                        memoEdit_ProdObservacao.Text = prod.observacoes;
                        comboBoxEdit_ProdMoeda.Text = prod.moeda;
                        comboBoxEdit_ProdTipoIpi.Text = prod.tipo_ipi;
                        textEdit_ProdExcluido.EditValue = prod.excluido;
                        dateEdit_ProdUltAlteracao.DateTime = prod.ultima_alteracao;
                    }
                    catch
                    {
                        throw;
                    }
                    
                    //UI Behavior
                    bar_Acoes.Visible = false;
                    bar_Acoes2.Visible = true;

                    tabNavigationPage_RegisterProd.PageVisible = true;
                    tabNavigationPage_DataGridProd.PageVisible = false;
                    tabPane_ConfigProd.SelectedPage = tabNavigationPage_RegisterProd;

                    //Define TextEdit Fields as ReadOnly = false
                    FormContols_FW.fieldsReadOnly(layoutControl_Prod, false);
                }
            }
            
        }

        private void barButtonItem_Del_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var row = gridView_Prod.GetRow(gridView_Prod.GetFocusedDataSourceRowIndex());
            prod = (Produto)row;
            
            if(prod != null)
            {
                if (XtraMessageBox.Show("Tem certeza que deseja excluir o registro (ID: " + prod.id + ")?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    prod.excluido = true;
                    s_prod.Update_Produto(prod);

                    gridControl_Prod.DataSource = s_prod.GetAll_Produtos();
                }
            }
            
        }
        #endregion

        #region ToolBar2 Events - Cancelar/Gravar
        private void barButtonItem_Gravar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (add_status == true &&
                textEdit_ProdNome.Text != "" &&
                textEdit_ProdPrecoTab.Text != "")
            {
                try
                {
                    prod = new Produto();
                    prod.nome = textEdit_ProdNome.Text;
                    prod.codigo = textEdit_ProdCod.Text;
                    if(textEdit_ProdComissao.Text != "")
                        prod.comissao = double.Parse(textEdit_ProdComissao.Text);
                    if(textEdit_ProdIpi.Text != "")
                        prod.ipi = double.Parse(textEdit_ProdIpi.Text);
                    if (textEdit_ProdMult.Text != "")
                    {
                        prod.multiplo = double.Parse(textEdit_ProdMult.Text);
                    }
                    else
                    {
                        prod.multiplo = 0.01;
                    }   
                    if (textEdit_ProdPesoBruto.Text != "")
                    {
                        prod.peso_bruto = double.Parse(textEdit_ProdPesoBruto.Text);
                    }
                    else
                    {
                        prod.peso_bruto = 0.001;
                    }
                    if (textEdit_ProdPrecoMin.Text != "")
                        prod.preco_minimo = double.Parse(textEdit_ProdPrecoMin.Text);
                    if (textEdit_ProdPrecoTab.Text != "")
                        prod.preco_tabela = double.Parse(textEdit_ProdPrecoTab.Text);
                    if (comboBoxEdit_ProdCat.Text != "")
                        prod.categoria_id = int.Parse(comboBoxEdit_ProdCat.EditValue.ToString());
                    prod.codigo_ncm = textEdit_ProdCodNCM.Text;
                    prod.unidade = textEdit_ProdUnidade.Text;
                    prod.moeda = comboBoxEdit_ProdMoeda.ItemIndex.ToString();
                    prod.tipo_ipi = comboBoxEdit_ProdTipoIpi.Text;
                    prod.observacoes = memoEdit_ProdObservacao.Text;
                    var pgc = textEdit_ProdGradeCores.Text.Split(new char[] { ',', ';', '/' }).Select(p => p.Trim()).Where(p => !string.IsNullOrWhiteSpace(p)).ToList();
                    foreach (string cq in pgc)
                    {
                        GradeCor gc = new GradeCor();
                        gc.cor = cq;
                        prod.grade_cores.Add(gc);
                    }
                    var pgt = textEdit_ProdGradeTamanhos.Text.Split(new char[] { ',', ';', '/' }).Select(p => p.Trim()).Where(p => !string.IsNullOrWhiteSpace(p)).ToList();
                    foreach (string tq in pgt)
                    {
                        GradeTamanho gt = new GradeTamanho();
                        gt.tamanho = tq;
                        prod.grade_tamanhos.Add(gt);
                    }
                    prod.excluido = textEdit_ProdExcluido.IsOn;
                    prod.ultima_alteracao = DateTime.Now;
                    prod.mptran = "I";

                }
                catch
                {
                    throw;
                }
                
                //Validação Nome
                var aux = s_prod.GetByNome_Produto(prod.nome);

                if (aux == null)
                {
                    if(!splashScreenManager_ProdCat.IsSplashFormVisible)
                    {
                        splashScreenManager_ProdCat.ShowWaitForm();
                    }
                    
                    //Add new ProdCat
                    s_prod.AddNew_Produto(prod);

                    splashScreenManager_ProdCat.CloseWaitForm();

                    XtraMessageBox.Show("Registro salvo!", "Informação");

                    FormContols_FW.fieldsClean(layoutControl_Prod);

                    gridControl_Prod.DataSource = s_prod.GetAll_Produtos();

                    bar_Acoes.Visible = true;
                    bar_Acoes2.Visible = false;
                    tabNavigationPage_RegisterProd.PageVisible = false;
                    tabNavigationPage_DataGridProd.PageVisible = true;
                    tabPane_ConfigProd.SelectedPage = tabNavigationPage_DataGridProd;
                }
                else
                {
                    splashScreenManager_ProdCat.CloseWaitForm();

                    XtraMessageBox.Show("Já existe um cliente com este CNPJ/CPF cadastrado!", "Atenção");
                }

            }
            else if (add_status == false &&
                textEdit_ProdNome.Text != "" &&
                textEdit_ProdPrecoTab.Text != "")
            {
                splashScreenManager_ProdCat.ShowWaitForm();

                try
                {
                    prod = new Produto();

                    prod = s_prod.GetBySpecification_Produto(_id);

                    prod.nome = textEdit_ProdNome.Text;
                    prod.codigo = textEdit_ProdCod.Text;
                    if (textEdit_ProdComissao.Text != "")
                        prod.comissao = double.Parse(textEdit_ProdComissao.Text);
                    if (textEdit_ProdIpi.Text != "")
                        prod.ipi = double.Parse(textEdit_ProdIpi.Text);
                    if (textEdit_ProdMult.Text != "")
                    {
                        prod.multiplo = double.Parse(textEdit_ProdMult.Text);
                    }
                    else
                    {
                        prod.multiplo = 0.01;
                    }
                    if (textEdit_ProdPesoBruto.Text != "")
                    {
                        prod.peso_bruto = double.Parse(textEdit_ProdPesoBruto.Text);
                    }
                    else
                    {
                        prod.peso_bruto = 0.001;
                    }
                    if (textEdit_ProdPrecoMin.Text != "")
                        prod.preco_minimo = double.Parse(textEdit_ProdPrecoMin.Text);
                    if (textEdit_ProdPrecoTab.Text != "")
                        prod.preco_tabela = double.Parse(textEdit_ProdPrecoTab.Text);
                    if (comboBoxEdit_ProdCat.Text != "")
                        prod.categoria_id = int.Parse(comboBoxEdit_ProdCat.EditValue.ToString());
                    prod.codigo_ncm = textEdit_ProdCodNCM.Text;
                    prod.unidade = textEdit_ProdUnidade.Text;
                    prod.moeda = comboBoxEdit_ProdMoeda.Text;
                    prod.tipo_ipi = comboBoxEdit_ProdTipoIpi.Text;
                    prod.observacoes = memoEdit_ProdObservacao.Text;
                    var pgc = textEdit_ProdGradeCores.Text.Split(new char[] { ',', ';', '/' }).Select(p => p.Trim()).Where(p => !string.IsNullOrWhiteSpace(p)).ToList();
                    List<GradeCor> gcl = new List<GradeCor>();
                    foreach (string cq in pgc)
                    {
                        GradeCor gc = new GradeCor();
                        gc.cor = cq;
                        gcl.Add(gc);
                        prod.grade_cores = gcl;
                    }
                    var pgt = textEdit_ProdGradeTamanhos.Text.Split(new char[] { ',', ';', '/' }).Select(p => p.Trim()).Where(p => !string.IsNullOrWhiteSpace(p)).ToList();
                    List<GradeTamanho> gct = new List<GradeTamanho>();
                    foreach (string tq in pgt)
                    {
                        GradeTamanho gt = new GradeTamanho();
                        gt.tamanho = tq;
                        gct.Add(gt);
                        prod.grade_tamanhos = gct;
                    }
                    prod.excluido = textEdit_ProdExcluido.IsOn;
                    prod.ultima_alteracao = DateTime.Now;
                    prod.mptran = "A";
                }
                catch
                {
                    throw;
                }

                s_prod.Update_Produto(prod);

                splashScreenManager_ProdCat.CloseWaitForm();

                XtraMessageBox.Show("Alteração realizada com sucesso!", "Informação");

                FormContols_FW.fieldsClean(layoutControl_Prod);

                gridControl_Prod.DataSource = s_prod.GetAll_Produtos();

                bar_Acoes.Visible = true;
                bar_Acoes2.Visible = false;
                tabNavigationPage_RegisterProd.PageVisible = false;
                tabNavigationPage_DataGridProd.PageVisible = true;
                tabPane_ConfigProd.SelectedPage = tabNavigationPage_DataGridProd;
            }
            else
            {
                XtraMessageBox.Show("Algum campo obrigatório precisa ser preenchido!", "Informação");
            }
        }

        private void barButtonItem_Cancelar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (XtraMessageBox.Show("Tem certeza que deseja sair da tela?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                barButtonItem_Gravar.Enabled = true;

                bar_Acoes.Visible = true;
                bar_Acoes2.Visible = false;
                tabNavigationPage_RegisterProd.PageVisible = false;
                tabNavigationPage_DataGridProd.PageVisible = true;
                tabPane_ConfigProd.SelectedPage = tabNavigationPage_DataGridProd;

                FormContols_FW.fieldsClean(layoutControl_Prod);
            }
        }
        #endregion

        #region Grid Produtos Events
        private void gridView_Clientes_DoubleClick(object sender, EventArgs e)
        {
            barButtonItem_Gravar.Enabled = false;

            //Get (object) row values
            var row = gridView_Prod.GetRow(gridView_Prod.GetFocusedDataSourceRowIndex());
            prod = (Produto)row;

            try
            {
                //Fill TextEdit Fields
                textEdit_ProdNome.Text = prod.nome;
                textEdit_ProdCod.Text = prod.codigo;
                textEdit_ProdComissao.Text = prod.comissao.ToString();
                textEdit_ProdCodNCM.Text = prod.comissao.ToString();
                textEdit_ProdIpi.Text = prod.ipi.ToString();
                textEdit_ProdMult.Text = prod.multiplo.ToString();
                textEdit_ProdPesoBruto.Text = prod.peso_bruto.ToString();
                textEdit_ProdPrecoMin.Text = prod.preco_minimo.ToString();
                textEdit_ProdPrecoTab.Text = prod.preco_tabela.ToString();
                textEdit_ProdUnidade.Text = prod.unidade;
                memoEdit_ProdObservacao.Text = prod.observacoes;
                comboBoxEdit_ProdCat.EditValue = prod.categoria_id;
                comboBoxEdit_ProdMoeda.Text = prod.moeda;
                comboBoxEdit_ProdTipoIpi.Text = prod.tipo_ipi;
                if (prod.grade_cores.Count > 0)
                {
                    var list = s_prod.GetAll_FullProdutos();
                    List<string> strings = new List<string>();
                    foreach (var l in list)
                    {
                        if (l.id == prod.id)
                        {
                            foreach (var ax in l.grade_cores)
                            {
                                GradeCor gc = ax;
                                strings.Add(gc.cor);
                            }
                        }
                    }
                    textEdit_ProdGradeCores.Text = String.Join(", ", strings.ToArray());
                }
                if (prod.grade_tamanhos.Count > 0)
                {
                    var list = s_prod.GetAll_FullProdutos();
                    List<string> strings = new List<string>();
                    foreach (var l in list)
                    {
                        if (l.id == prod.id)
                        {
                            foreach (var ax in l.grade_tamanhos)
                            {
                                GradeTamanho gt = ax;
                                strings.Add(gt.tamanho);
                            }
                        }
                    }
                    textEdit_ProdGradeTamanhos.Text = String.Join(", ", strings.ToArray());
                }
                textEdit_ProdExcluido.EditValue = prod.excluido;
                dateEdit_ProdUltAlteracao.DateTime = prod.ultima_alteracao;
            }
            catch
            {
                throw;
            }

            //Define TextEdit Fields as ReadOnly = true
            FormContols_FW.fieldsReadOnly(layoutControl_Prod, true);

            //UI Behavior
            bar_Acoes.Visible = false;
            bar_Acoes2.Visible = true;

            tabNavigationPage_RegisterProd.PageVisible = true;
            tabNavigationPage_DataGridProd.PageVisible = false;
            tabPane_ConfigProd.SelectedPage = tabNavigationPage_RegisterProd;
        }
        #endregion

        private void simpleButton_ProdDefCorTam_Click(object sender, EventArgs e)
        {
            if (!layoutControlGroup_ProdGrades.Expanded)
            {
                layoutControlGroup_ProdGrades.Expanded = true;
            }
            else
            {
                layoutControlGroup_ProdGrades.Expanded = false;
            }
            //var t = (int)comboBoxEdit_ProdMoeda.EditValue;

            //var t2 = (char)(IPI_Type)Enum.Parse(typeof(IPI_Type), comboBoxEdit_ProdTipoIpi.EditValue.ToString());

            //string a = comboBoxEdit_ProdCat.EditValue.ToString();
        }

        private void comboBoxEdit_ProdMoeda_SelectedIndexChanged(object sender, EventArgs e)
        {
            //var t = comboBoxEdit_ProdMoeda.SelectedIndex;
        }

        private void simpleButton_ProdGradeCor_Gravar_Click(object sender, EventArgs e)
        {
            List<string> gradeCor = textEdit_ProdGradeCores.Text.Split(new char[] { ',', ';', '/' }).Select(p => p.Trim()).Where(p => !string.IsNullOrWhiteSpace(p)).ToList();
        }

        private void simpleButton_ProdGradeTamanho_Gravar_Click(object sender, EventArgs e)
        {
            List<string> gradeTamanho = textEdit_ProdGradeTamanhos.Text.Split(new char[] { ',', ';', '/' }).Select(p => p.Trim()).Where(p => !string.IsNullOrWhiteSpace(p)).ToList();
        }

        private void FormProduto_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'meusPedidosDataSet.ProdutoCategoria' table. You can move, or remove it, as needed.
            this.produtoCategoriaTableAdapter.Fill(this.meusPedidosDataSet.ProdutoCategoria);

        }

        private void comboBoxEdit_ProdTipoIpi_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}

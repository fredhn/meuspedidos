﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Control;
using Core;
using DevExpress.XtraEditors;
using System.Data.Entity;
using Control.Validations;
using DevExpress.XtraGrid.Views.Grid;
using System.Globalization;
using DevExpress.XtraEditors.Controls;

namespace View_MeusPedidos
{
    public partial class FormProdutoTabelaPreco : DevExpress.XtraEditors.XtraForm
    {
        ProdutoTabelaPreco_Service s_ptp;
        ProdutoTabelaPreco ptp;
        Produto_Service s_prod;
        TabelaPreco_Service s_tp;

        int _id;
        bool add_status = false;

        public FormProdutoTabelaPreco()
        {
            InitializeComponent();
            s_ptp = new ProdutoTabelaPreco_Service();
            s_prod = new Produto_Service();
            s_tp = new TabelaPreco_Service();

            bar_Acoes2.Visible = false;

            tabNavigationPage_RegisterProdTabPreco.PageVisible = false;
            tabPane_ConfigProdTabPreco.SelectedPage = tabNavigationPage_DataGridProdTabPreco;

            gridControl_ProdTabPreco.DataSource = s_ptp.GetAll_ProdutoTabelaPreco();

            BindingSource srcProd = new BindingSource();
            srcProd.DataSource = s_prod.GetAll_Produtos();
            this.comboBoxEdit_ProdTabPreco_Prod.Properties.DataSource = srcProd.List;
            this.comboBoxEdit_ProdTabPreco_Prod.Properties.Columns.Add(new LookUpColumnInfo("id", "Id"));
            this.comboBoxEdit_ProdTabPreco_Prod.Properties.Columns.Add(new LookUpColumnInfo("nome", "Nome"));
            this.comboBoxEdit_ProdTabPreco_Prod.Properties.DisplayMember = "nome";
            this.comboBoxEdit_ProdTabPreco_Prod.Properties.ValueMember = "id";

            BindingSource srcCondPag = new BindingSource();
            srcCondPag.DataSource = s_tp.GetAll_TabelaPreco();
            this.comboBoxEdit_ProdTabPreco_TabPreco.Properties.DataSource = srcCondPag.List;
            this.comboBoxEdit_ProdTabPreco_TabPreco.Properties.Columns.Add(new LookUpColumnInfo("id", "Id"));
            this.comboBoxEdit_ProdTabPreco_TabPreco.Properties.Columns.Add(new LookUpColumnInfo("nome", "Nome"));
            this.comboBoxEdit_ProdTabPreco_TabPreco.Properties.DisplayMember = "nome";
            this.comboBoxEdit_ProdTabPreco_TabPreco.Properties.ValueMember = "id";
        }

        #region ToolBar1 Events - Adicionar/Editar/Deletar

        private void barButtonItem_Add_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            add_status = true;

            //UI Behavior

            FormContols_FW.fieldsClean(layoutControl_ProdTabPreco);

            bar_Acoes.Visible = false;
            bar_Acoes2.Visible = true;

            tabNavigationPage_RegisterProdTabPreco.PageVisible = true;
            tabNavigationPage_DataGridProdTabPreco.PageVisible = false;
            tabPane_ConfigProdTabPreco.SelectedPage = tabNavigationPage_RegisterProdTabPreco;

            //Define TextEdit Fields as ReadOnly = false
            FormContols_FW.fieldsReadOnly(layoutControl_ProdTabPreco, false);
        }

        private void barButtonItem_Edit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            add_status = false;

            var row = gridView_TabPreco.GetRow(gridView_TabPreco.GetFocusedDataSourceRowIndex());
            ptp = (ProdutoTabelaPreco)row;

            if(ptp != null)
            {
                if (XtraMessageBox.Show("Tem certeza que deseja alterar o registro (ID: " + ptp.id + ")?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    //Retrieve entity ID
                    _id = ptp.id;

                    //Fill TextEdit Fields
                    textEdit_ProdTabPrecoValor.Text = ptp.preco.ToString();
                    comboBoxEdit_ProdTabPreco_Prod.EditValue = ptp.produto_id.ToString();
                    comboBoxEdit_ProdTabPreco_TabPreco.EditValue = ptp.tabela_id.ToString();
                    textEdit_TabPrecoExcluido.EditValue = ptp.excluido;
                    dateEdit_TabPrecoUltAlteracao.DateTime = ptp.ultima_alteracao;

                    //UI Behavior
                    bar_Acoes.Visible = false;
                    bar_Acoes2.Visible = true;

                    tabNavigationPage_RegisterProdTabPreco.PageVisible = true;
                    tabNavigationPage_DataGridProdTabPreco.PageVisible = false;
                    tabPane_ConfigProdTabPreco.SelectedPage = tabNavigationPage_RegisterProdTabPreco;

                    //Define TextEdit Fields as ReadOnly = false
                    FormContols_FW.fieldsReadOnly(layoutControl_ProdTabPreco, false);
                }
            }
            
        }

        private void barButtonItem_Del_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var row = gridView_TabPreco.GetRow(gridView_TabPreco.GetFocusedDataSourceRowIndex());
            ptp = (ProdutoTabelaPreco)row;
            
            if(ptp != null)
            {
                if (XtraMessageBox.Show("Tem certeza que deseja excluir o registro (ID: " + ptp.id + ")?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    ptp.excluido = true;
                    s_ptp.Update_ProdutoTabelaPreco(ptp);

                    gridControl_ProdTabPreco.DataSource = s_ptp.GetAll_ProdutoTabelaPreco();
                }
            }
            
        }
        #endregion

        #region ToolBar2 Events - Cancelar/Gravar
        private void barButtonItem_Gravar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (add_status == true &&
                textEdit_ProdTabPrecoValor.Text != "" &&
                comboBoxEdit_ProdTabPreco_Prod.Text != "" &&
                comboBoxEdit_ProdTabPreco_TabPreco.Text != "")
            {
                ptp = new ProdutoTabelaPreco();
                ptp.preco = float.Parse(textEdit_ProdTabPrecoValor.EditValue.ToString(), CultureInfo.InvariantCulture.NumberFormat);
                ptp.tabela_id = int.Parse(comboBoxEdit_ProdTabPreco_TabPreco.EditValue.ToString());
                ptp.produto_id = int.Parse(comboBoxEdit_ProdTabPreco_Prod.EditValue.ToString());
                ptp.excluido = textEdit_TabPrecoExcluido.IsOn;
                ptp.ultima_alteracao = DateTime.Now;
                ptp.mptran = "I";

                //Validação Nome
                var aux = s_ptp.GetByIdProdIdTabP_ProdutoTabelaPreco(ptp.produto_id, ptp.tabela_id);

                if (aux == null)
                {
                    splashScreenManager_Transp.ShowWaitForm();

                    //Add new ProdCat
                    s_ptp.AddNew_ProdutoTabelaPreco(ptp);

                    splashScreenManager_Transp.CloseWaitForm();

                    XtraMessageBox.Show("Registro salvo!", "Informação");

                    FormContols_FW.fieldsClean(layoutControl_ProdTabPreco);

                    gridControl_ProdTabPreco.DataSource = s_ptp.GetAll_ProdutoTabelaPreco();

                    bar_Acoes.Visible = true;
                    bar_Acoes2.Visible = false;
                    tabNavigationPage_RegisterProdTabPreco.PageVisible = false;
                    tabNavigationPage_DataGridProdTabPreco.PageVisible = true;
                    tabPane_ConfigProdTabPreco.SelectedPage = tabNavigationPage_DataGridProdTabPreco;
                }
                else
                {
                    if(splashScreenManager_Transp.IsSplashFormVisible)
                    {
                        splashScreenManager_Transp.CloseWaitForm();
                    }

                    XtraMessageBox.Show("Já existe um relacionamento entre esta tabela de preço e produto!", "Atenção");
                }

            }
            else if (add_status == false &&
                textEdit_ProdTabPrecoValor.Text != "" &&
                comboBoxEdit_ProdTabPreco_Prod.Text != "" &&
                comboBoxEdit_ProdTabPreco_TabPreco.Text != "")
            {
                splashScreenManager_Transp.ShowWaitForm();

                ptp = new ProdutoTabelaPreco();
                ptp.id = _id;

                ptp = s_ptp.GetBySpecification_ProdutoTabelaPreco(ptp.id);

                ptp.preco = float.Parse(textEdit_ProdTabPrecoValor.EditValue.ToString(), CultureInfo.InvariantCulture.NumberFormat);
                ptp.tabela_id = int.Parse(comboBoxEdit_ProdTabPreco_TabPreco.EditValue.ToString());
                ptp.produto_id = int.Parse(comboBoxEdit_ProdTabPreco_Prod.EditValue.ToString());
                ptp.excluido = textEdit_TabPrecoExcluido.IsOn;
                ptp.ultima_alteracao = DateTime.Now;
                ptp.mptran = "A";

                s_ptp.Update_ProdutoTabelaPreco(ptp);

                splashScreenManager_Transp.CloseWaitForm();

                XtraMessageBox.Show("Alteração realizada com sucesso!", "Informação");

                FormContols_FW.fieldsClean(layoutControl_ProdTabPreco);

                gridControl_ProdTabPreco.DataSource = s_ptp.GetAll_ProdutoTabelaPreco();

                bar_Acoes.Visible = true;
                bar_Acoes2.Visible = false;
                tabNavigationPage_RegisterProdTabPreco.PageVisible = false;
                tabNavigationPage_DataGridProdTabPreco.PageVisible = true;
                tabPane_ConfigProdTabPreco.SelectedPage = tabNavigationPage_DataGridProdTabPreco;
            }
            else
            {
                XtraMessageBox.Show("Algum campo obrigatório precisa ser preenchido!", "Informação");
            }
        }

        private void barButtonItem_Cancelar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (XtraMessageBox.Show("Tem certeza que deseja sair da tela?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                barButtonItem_Gravar.Enabled = true;

                bar_Acoes.Visible = true;
                bar_Acoes2.Visible = false;
                tabNavigationPage_RegisterProdTabPreco.PageVisible = false;
                tabNavigationPage_DataGridProdTabPreco.PageVisible = true;
                tabPane_ConfigProdTabPreco.SelectedPage = tabNavigationPage_DataGridProdTabPreco;

                FormContols_FW.fieldsClean(layoutControl_ProdTabPreco);
            }
        }
        #endregion

        #region Grid ProdutoTabelaPreço Events
        private void gridView_Clientes_DoubleClick(object sender, EventArgs e)
        {
            barButtonItem_Gravar.Enabled = false;

            //Get (object) row values
            var row = gridView_TabPreco.GetRow(gridView_TabPreco.GetFocusedDataSourceRowIndex());
            ptp = (ProdutoTabelaPreco)row;
            
            //Fill TextEdit Fields
            if(ptp != null)
            {
                textEdit_ProdTabPrecoValor.Text = ptp.preco.ToString();
                comboBoxEdit_ProdTabPreco_Prod.EditValue = ptp.produto_id.ToString();
                comboBoxEdit_ProdTabPreco_TabPreco.EditValue = ptp.tabela_id.ToString();
                textEdit_TabPrecoExcluido.EditValue = ptp.excluido;
                dateEdit_TabPrecoUltAlteracao.DateTime = ptp.ultima_alteracao;
            }
            

            //Define TextEdit Fields as ReadOnly = true
            FormContols_FW.fieldsReadOnly(layoutControl_ProdTabPreco, true);

            //UI Behavior
            bar_Acoes.Visible = false;
            bar_Acoes2.Visible = true;

            tabNavigationPage_RegisterProdTabPreco.PageVisible = true;
            tabNavigationPage_DataGridProdTabPreco.PageVisible = false;
            tabPane_ConfigProdTabPreco.SelectedPage = tabNavigationPage_RegisterProdTabPreco;
        }
        #endregion

        private void FormTabelaPreco_Load(object sender, EventArgs e)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Control;
using Core;
using DevExpress.XtraEditors;
using System.Data.Entity;
using Control.Validations;
using DevExpress.XtraGrid.Views.Grid;
using System.Globalization;

namespace View_MeusPedidos
{
    public partial class FormUsuarioCad : DevExpress.XtraEditors.XtraForm
    {
        public Usuario_Service s_user;
        public Usuario user;

        int _id;
        bool add_status = false;

        public FormUsuarioCad()
        {
            InitializeComponent();
            s_user = new Usuario_Service();

            bar1.Visible = false;
            bar2.Visible = false;

            tabNavigationPage_RegisterUsuario.PageVisible = false;
            tabPane_ConfigUsuario.SelectedPage = tabNavigationPage_DataGridUsuario;

            gridControl_Usuario.DataSource = s_user.GetAll_Usuarios();
        }

        #region ToolBar1 Events - Adicionar/Editar/Deletar
         private void barButtonItem_Add_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            add_status = true;

            //UI Behavior

            FormContols_FW.fieldsClean(layoutControl_Usuario);

            bar1.Visible = false;
            bar2.Visible = false;

            tabNavigationPage_RegisterUsuario.PageVisible = true;
            tabNavigationPage_DataGridUsuario.PageVisible = false;
            tabPane_ConfigUsuario.SelectedPage = tabNavigationPage_RegisterUsuario;

            //Define TextEdit Fields as ReadOnly = false
            FormContols_FW.fieldsReadOnly(layoutControl_Usuario, false);
        }

        private void barButtonItem_Edit_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            add_status = false;

            var row = gridView_Usuario.GetRow(gridView_Usuario.GetFocusedDataSourceRowIndex());
            user = (Usuario)row;

            if (user != null)
            {
                if (XtraMessageBox.Show("Tem certeza que deseja alterar o registro (ID: " + user.id + ")?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    //Retrieve entity ID
                    _id = user.id;

                    textEdit_UsuarioTipo.Text = user.type;
                    textEdit_UsuarioEmail.Text = user.email;
                    textEdit_UsuarioSenha.Text = user.password;
                    textEdit_UsuarioCPF.Text = user.cpf;
                    textEdit_UsuarioCNPJ.Text = user.cnpj;
                    textEdit_UsuarioComentarios.Text = user.comments;
                    textEdit_UsuarioDtNasc.EditValue = user.birth;
                    textEdit_UsuarioExcluido.EditValue = user.in_trash;
                    textEdit_UsuarioGenero.EditValue = user.gender;
                    textEdit_UsuarioSMS.Text = user.phone;
                    textEdit_UsuarioUltNome.Text = user.last_name;
                    textEdit_UsuarioPNome.Text = user.first_name;
                    textEdit_UsuarioGenero.Text = user.gender;
                    dateEdit_UsuarioDtCriacao.EditValue = user.created_at;

                    //UI Behavior
                    bar1.Visible = false;
                    bar2.Visible = false;

                    tabNavigationPage_RegisterUsuario.PageVisible = true;
                    tabNavigationPage_DataGridUsuario.PageVisible = false;
                    tabPane_ConfigUsuario.SelectedPage = tabNavigationPage_RegisterUsuario;

                    //Define TextEdit Fields as ReadOnly = false
                    FormContols_FW.fieldsReadOnly(layoutControl_Usuario, false);
                }
            }
        }

        private void barButtonItem_Del_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var row = gridView_Usuario.GetRow(gridView_Usuario.GetFocusedDataSourceRowIndex());
            user = (Usuario)row;

            if (user != null)
            {
                if (XtraMessageBox.Show("Tem certeza que deseja excluir o registro (ID: " + user.id + ")?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    user.in_trash = Convert.ToByte(true);
                    s_user.Update_Usuario(user);

                    gridControl_Usuario.DataSource = s_user.GetAll_Usuarios();
                }
            }
        }
        #endregion

        #region ToolBar2 Events - Cancelar/Gravar
        private void barButtonItem_Gravar_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (add_status == true &&
                    textEdit_UsuarioEmail.Text != null &&
                    textEdit_UsuarioSenha.Text != null &&
                    textEdit_UsuarioDtNasc.Text != "")
            {

                user = new Usuario();

                user.birth = textEdit_UsuarioDtNasc.DateTime;
                user.cnpj = textEdit_UsuarioCNPJ.Text;
                user.cpf = textEdit_UsuarioCPF.Text;
                user.comments = textEdit_UsuarioComentarios.Text;
                user.created_at = dateEdit_UsuarioDtCriacao.DateTime;
                user.email = textEdit_UsuarioEmail.Text;
                user.first_name = textEdit_UsuarioPNome.Text;
                user.last_name = textEdit_UsuarioUltNome.Text;
                user.phone = textEdit_UsuarioSMS.Text;
                user.password = ConvertMD5_V.CalculateMD5Hash(textEdit_UsuarioSenha.Text);
                user.gender = textEdit_UsuarioGenero.Text;
                user.in_trash = Convert.ToByte(textEdit_UsuarioExcluido.IsOn);
                user.created_at = DateTime.Now.Date;

                //Validação Nome
                var aux = s_user.GetBySpecification_Usuario(user.id);

                if (aux == null)
                {
                    if (!splashScreenManager_User.IsSplashFormVisible)
                    {
                        splashScreenManager_User.ShowWaitForm();
                    }

                    //Add new ProdCat
                    s_user.AddNew_Usuario(user);

                    splashScreenManager_User.CloseWaitForm();

                    XtraMessageBox.Show("Registro salvo!", "Informação");

                    FormContols_FW.fieldsClean(layoutControl_Usuario);

                    gridControl_Usuario.DataSource = s_user.GetAll_Usuarios();

                    bar1.Visible = false;
                    bar2.Visible = false;
                    tabNavigationPage_RegisterUsuario.PageVisible = false;
                    tabNavigationPage_DataGridUsuario.PageVisible = true;
                    tabPane_ConfigUsuario.SelectedPage = tabNavigationPage_DataGridUsuario;
                }
                else
                {
                    splashScreenManager_User.CloseWaitForm();

                    XtraMessageBox.Show("Já existe uma transportadora com este nome cadastrado!", "Atenção");
                }

            }
            else if (add_status == false &&
                    textEdit_UsuarioEmail.Text != null &&
                    textEdit_UsuarioSenha.Text != null &&
                    textEdit_UsuarioDtNasc.Text != "")
            {
                splashScreenManager_User.ShowWaitForm();

                user = new Usuario();
                user.id = _id;

                user = s_user.GetBySpecification_Usuario(user.id);

                user.birth = textEdit_UsuarioDtNasc.DateTime;
                user.cnpj = textEdit_UsuarioCNPJ.Text;
                user.cpf = textEdit_UsuarioCPF.Text;
                user.comments = textEdit_UsuarioComentarios.Text;
                user.created_at = dateEdit_UsuarioDtCriacao.DateTime;
                user.email = textEdit_UsuarioEmail.Text;
                user.first_name = textEdit_UsuarioPNome.Text;
                user.last_name = textEdit_UsuarioUltNome.Text;
                user.phone = textEdit_UsuarioSMS.Text;
                user.password = ConvertMD5_V.CalculateMD5Hash(textEdit_UsuarioSenha.Text);
                user.gender = textEdit_UsuarioGenero.Text;
                user.in_trash = Convert.ToByte(textEdit_UsuarioExcluido.IsOn);

                s_user.Update_Usuario(user);

                splashScreenManager_User.CloseWaitForm();

                XtraMessageBox.Show("Alteração realizada com sucesso!", "Informação");

                FormContols_FW.fieldsClean(layoutControl_Usuario);

                gridControl_Usuario.DataSource = s_user.GetAll_Usuarios();

                bar1.Visible = false;
                bar2.Visible = false;
                tabNavigationPage_RegisterUsuario.PageVisible = false;
                tabNavigationPage_DataGridUsuario.PageVisible = true;
                tabPane_ConfigUsuario.SelectedPage = tabNavigationPage_DataGridUsuario;
            }
            else
            {
                XtraMessageBox.Show("Algum campo obrigatório precisa ser preenchido!", "Informação");
            }
        }

        private void barButtonItem_Cancelar_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (XtraMessageBox.Show("Tem certeza que deseja sair da tela?", "Informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                barButtonItem_Gravar.Enabled = true;

                bar1.Visible = false;
                bar2.Visible = false;
                tabNavigationPage_RegisterUsuario.PageVisible = false;
                tabNavigationPage_DataGridUsuario.PageVisible = true;
                tabPane_ConfigUsuario.SelectedPage = tabNavigationPage_DataGridUsuario;

                FormContols_FW.fieldsClean(layoutControl_Usuario);
            }
        }
        #endregion

        #region Grid Transportadora Events
        private void gridView_Clientes_DoubleClick(object sender, EventArgs e)
        {
            barButtonItem_Gravar.Enabled = false;

            //Get (object) row values
            var row = gridView_Usuario.GetRow(gridView_Usuario.GetFocusedDataSourceRowIndex());
            user = (Usuario)row;

            textEdit_UsuarioTipo.Text = user.type;
            textEdit_UsuarioEmail.Text = user.email;
            textEdit_UsuarioSenha.Text = user.password;
            textEdit_UsuarioCPF.Text = user.cpf;
            textEdit_UsuarioCNPJ.Text = user.cnpj;
            textEdit_UsuarioComentarios.Text = user.comments;
            textEdit_UsuarioDtNasc.EditValue = user.birth;
            textEdit_UsuarioExcluido.EditValue = user.in_trash;
            textEdit_UsuarioGenero.EditValue = user.gender;
            textEdit_UsuarioSMS.Text = user.phone;
            textEdit_UsuarioUltNome.Text = user.last_name;
            textEdit_UsuarioPNome.Text = user.first_name;
            textEdit_UsuarioGenero.Text = user.gender;

            //Define TextEdit Fields as ReadOnly = true
            FormContols_FW.fieldsReadOnly(layoutControl_Usuario, true);

            //UI Behavior
            bar1.Visible = false;
            bar2.Visible = false;

            tabNavigationPage_RegisterUsuario.PageVisible = true;
            tabNavigationPage_DataGridUsuario.PageVisible = false;
            tabPane_ConfigUsuario.SelectedPage = tabNavigationPage_RegisterUsuario;
        }
        #endregion

        private void FormTabelaPreco_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'meusPedidosDataSet.Usuario' table. You can move, or remove it, as needed.
            this.usuarioTableAdapter.Fill(this.meusPedidosDataSet1.Usuario);
        }

       
    }
}

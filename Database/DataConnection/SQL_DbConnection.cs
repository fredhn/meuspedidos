﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.IO;
using System.Xml;
using System.Reflection;
using System.Data.SqlClient;

namespace Database.DataConnection
{
    public class SQL_DbConnection
    {
        static object locker = new object();

        public static DbConnection Conection()
        {
            DbProviderFactory provedor = DbProviderFactories.GetFactory("System.Data.SqlClient");

            DbConnection connection = provedor.CreateConnection();
            
            FileInfo fi = new FileInfo(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "conn.xml"));
            if (fi.Exists)
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(AppDomain.CurrentDomain.BaseDirectory + "conn.xml");

                XmlNode node = doc.DocumentElement.SelectSingleNode("/local-db/connectionstring");

                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(node.InnerText);

                connection.ConnectionString = builder.ConnectionString;
            }
            
            /*
            string strConnection = ConfigurationManager.ConnectionStrings["View_MeusPedidos.Properties.Settings.MeusPedidosConnectionString"].ToString();

            connection.ConnectionString = strConnection;
            */
            return connection;
        }
    }
}

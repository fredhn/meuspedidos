﻿using RestSharp;
using Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service_API
{
    public class ClienteCategoria_ServicePOST
    {

        public IRestResponse IncluirClienteCategoria(string url, string appToken, string cmpToken, string json)
        {
            //string json = JsonConvert.SerializeObject(prod);

            var client = new RestClient(url + "/api/v1/clientes_categorias");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("ApplicationToken", appToken);
            request.AddHeader("CompanyToken", cmpToken);
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", json, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            return response;
        }
        public IRestResponse IncluirClienteTodasCategorias(string url, string appToken, string cmpToken, string json)
        {
            //string json = JsonConvert.SerializeObject(prod);

            var client = new RestClient(url + "/api/v1/clientes_categorias/liberar_todas");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("ApplicationToken", appToken);
            request.AddHeader("CompanyToken", cmpToken);
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", json, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            return response;
        }


    }
}

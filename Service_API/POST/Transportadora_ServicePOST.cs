﻿using RestSharp;
using Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service_API
{
    public class Transportadora_ServicePOST
    {

        public IRestResponse IncluirTransportadora(string url, string appToken, string cmpToken, string json)
        {
            //string json = JsonConvert.SerializeObject(prod);

            var client = new RestClient(url + "/api/v1/transportadoras/");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("ApplicationToken", appToken);
            request.AddHeader("CompanyToken", cmpToken);
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", json, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            return response;
        }

    }
}

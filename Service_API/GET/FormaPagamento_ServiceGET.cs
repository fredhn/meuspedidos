﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service_API
{
    public class FormaPagamento_ServiceGET
    {

        public void TodasFormasPagamento(string appToken, string cmpToken)
        {
            var client = new RestClient("http://sandbox.meuspedidos.com.br:8080/api/v1/formas_pagamento");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("ApplicationToken", appToken);
            request.AddHeader("CompanyToken", cmpToken);
            IRestResponse response = client.Execute(request);

        }

        public void FormaPagamento(string appToken, string cmpToken, string payF)
        {
            var client = new RestClient("http://sandbox.meuspedidos.com.br:8080/api/v1/formas_pagamento/" + payF);
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("ApplicationToken", appToken);
            request.AddHeader("CompanyToken", cmpToken);
            IRestResponse response = client.Execute(request);

        }

    }
}

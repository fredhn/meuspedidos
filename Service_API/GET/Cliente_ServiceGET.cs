﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service_API
{
    public class Cliente_ServiceGET
    {

        public void TodosClientes(string appToken, string cmpToken)
        {
            var client = new RestClient("http://sandbox.meuspedidos.com.br:8080/api/v1/clientes");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("ApplicationToken", appToken);
            request.AddHeader("CompanyToken", cmpToken);
            IRestResponse response = client.Execute(request);

        }

        public void Cliente(string appToken, string cmpToken, string costumer)
        {
            var client = new RestClient("http://sandbox.meuspedidos.com.br:8080/api/v1/cliente/" + costumer);
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("ApplicationToken", appToken);
            request.AddHeader("CompanyToken", cmpToken);
            //request.AddHeader("ApplicationToken", "896d9780-e4c3-11e7-92d9-0e8f8db168a0");
            //request.AddHeader("CompanyToken", "5cb04d1c-e73b-11e7-b146-0a63c46a22ee");
            IRestResponse response = client.Execute(request);

        }

        public void Cliente_AlteradoApos(string appToken, string cmpToken, DateTime alteradoApos)
        {
            var client = new RestClient("http://sandbox.meuspedidos.com.br:8080/api/v1/cliente/?alterado_apos=" + alteradoApos.Date + "%" + alteradoApos.TimeOfDay);
            //?alterado_apos=2014-03-01%2007:21:44
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("ApplicationToken", appToken);
            request.AddHeader("CompanyToken", cmpToken);
            //request.AddHeader("ApplicationToken", "896d9780-e4c3-11e7-92d9-0e8f8db168a0");
            //request.AddHeader("CompanyToken", "5cb04d1c-e73b-11e7-b146-0a63c46a22ee");
            IRestResponse response = client.Execute(request);

        }

    }
}

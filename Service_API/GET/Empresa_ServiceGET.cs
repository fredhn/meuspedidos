﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service_API
{
    public class Empresa_ServiceGET
    {

        public IRestResponse Empresa(string url, string CNPJ, string cod_pro, string serial)
        {
            var client = new RestClient(url + "/api/v1/srv_lic?" + "CNPJ=" + CNPJ + "&COD_PRO=" + cod_pro + "&SERIAL=" + serial);
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            IRestResponse response = client.Execute(request);

            return response;
        }

    }
}

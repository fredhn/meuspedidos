﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service_API
{
    public class RegraLiberacao_ServiceGET
    {

        /*
        **  Obter todas as regras de liberação cadastradas no sistema Meus Pedidos.
        */
        public void TodasRegras(string appToken, string cmpToken)
        {
            var client = new RestClient("http://sandbox.meuspedidos.com.br:8080/api/v1/usuarios_clientes");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("ApplicationToken", appToken);
            request.AddHeader("CompanyToken", cmpToken);
            IRestResponse response = client.Execute(request);

        }

        /*
        **  Obter todas as regras de liberação de um cliente específico.
        */
        public void RegrasCliente(string appToken, string cmpToken, string customerID)
        {
            var client = new RestClient("http://sandbox.meuspedidos.com.br:8080/api/v1/cliente/" + customerID + "/");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("ApplicationToken", appToken);
            request.AddHeader("CompanyToken", cmpToken);
            IRestResponse response = client.Execute(request);

        }

        /*
        **  Obter todas as regras de liberação de um usuário específico.
        */
        public void RegrasUsuario(string userID)
        {
            var client = new RestClient("http://sandbox.meuspedidos.com.br:8080/api/v1/usuarios_clientes/usuario/" + userID + "/");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("ApplicationToken", "896d9780-e4c3-11e7-92d9-0e8f8db168a0");
            request.AddHeader("CompanyToken", "5cb04d1c-e73b-11e7-b146-0a63c46a22ee");
            IRestResponse response = client.Execute(request);

        }

        /*
        **  Obter a regra de liberação atual de um cliente específico para um usuário específico.
        */
        public void RegraClienteParaUsuario(string customerID, string userID)
        {
            var client = new RestClient("http://sandbox.meuspedidos.com.br:8080/api/v1/usuarios_clientes/usuario/" + userID + "/cliente/" + customerID + "/");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("ApplicationToken", "896d9780-e4c3-11e7-92d9-0e8f8db168a0");
            request.AddHeader("CompanyToken", "5cb04d1c-e73b-11e7-b146-0a63c46a22ee");
            IRestResponse response = client.Execute(request);

        }

    }
}

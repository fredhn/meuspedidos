﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service_API
{
    public class CondicaoPagamento_ServiceGET
    {

        public void TodasCondicoesPagamento(string appToken, string cmpToken)
        {
            var client = new RestClient("http://sandbox.meuspedidos.com.br:8080/api/v1/condicoes_pagamento");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("ApplicationToken", appToken);
            request.AddHeader("CompanyToken", cmpToken);
            IRestResponse response = client.Execute(request);

        }

        public void CondicaoPagamento(string appToken, string cmpToken, string payCondition)
        {
            var client = new RestClient("http://sandbox.meuspedidos.com.br:8080/api/v1/condicoes_pagamento/" + payCondition);
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("ApplicationToken", appToken);
            request.AddHeader("CompanyToken", cmpToken);
            IRestResponse response = client.Execute(request);

        }

    }
}

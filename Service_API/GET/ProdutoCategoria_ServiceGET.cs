﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service_API
{
    public class ProdutoCategoria_ServiceGET
    {

        public void TodosProdutosCategorias(string appToken, string cmpToken)
        {
            var client = new RestClient("http://sandbox.meuspedidos.com.br:8080/api/v1/produtos");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("ApplicationToken", appToken);
            request.AddHeader("CompanyToken", cmpToken);
            IRestResponse response = client.Execute(request);
            
        }

        public void ProdutoCategoria(string appToken, string cmpToken, string prodCategory)
        {
            var client = new RestClient("http://sandbox.meuspedidos.com.br:8080/api/v1/produto/" + prodCategory);
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("ApplicationToken", appToken);
            request.AddHeader("CompanyToken", cmpToken);
            IRestResponse response = client.Execute(request);

        }

    }
}

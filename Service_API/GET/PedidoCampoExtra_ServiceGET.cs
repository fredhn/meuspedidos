﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service_API
{
    public class PedidoCampoExtra_ServiceGET
    {

        public IRestResponse TodosPedidosCamposExtras(string appToken, string cmpToken)
        {
            var client = new RestClient("http://sandbox.meuspedidos.com.br:8080/api/v1/campos_extras");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("ApplicationToken", appToken);
            request.AddHeader("CompanyToken", cmpToken);
            IRestResponse response = client.Execute(request);

            return response;
        }

        public IRestResponse PedidoCampoExtra(string appToken, string cmpToken, string extraFields)
        {
            var client = new RestClient("http://sandbox.meuspedidos.com.br:8080/api/v1/campos_extras/" + extraFields);
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("ApplicationToken", appToken);
            request.AddHeader("CompanyToken", cmpToken);
            IRestResponse response = client.Execute(request);

            return response;
        }

    }
}

﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service_API
{
    public class Produto_ServiceGET
    {

        public void TodosProdutos(string appToken, string cmpToken)
        {
            var client = new RestClient("https://integracao.meuspedidos.com.br/api/v1/produtos");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("ApplicationToken", appToken);
            request.AddHeader("CompanyToken", cmpToken);
            IRestResponse response = client.Execute(request);
        }

        public void Produto(string appToken, string cmpToken, string prod)
        {
            var client = new RestClient("https://integracao.meuspedidos.com.br/api/v1/produtos/" + prod);
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("ApplicationToken", appToken);
            request.AddHeader("CompanyToken", cmpToken);
            IRestResponse response = client.Execute(request);
        }

    }
}

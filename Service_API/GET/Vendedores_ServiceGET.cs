﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service_API
{
    public class Vendedores_ServiceGET
    {

        public IRestResponse TodosVendedores(string url, string appToken, string cmpToken)
        {
            var client = new RestClient(url + "/api/v1/usuarios");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("ApplicationToken", appToken);
            request.AddHeader("CompanyToken", cmpToken);
            IRestResponse response = client.Execute(request);

            return response;
        }

        public IRestResponse Vendedor(string url, string appToken, string cmpToken, string salesman)
        {
            var client = new RestClient(url + "/api/v1/usuarios/" + salesman);
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("ApplicationToken", appToken);
            request.AddHeader("CompanyToken", cmpToken);
            IRestResponse response = client.Execute(request);

            return response;
        }

    }
}

﻿using RestSharp;
using Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service_API
{
    public class CondicaoPagamento_ServicePUT
    {

        public IRestResponse AlterarCondicaoPagamento(string url, string appToken, string cmpToken, string json, string id)
        {
            //string json = JsonConvert.SerializeObject(prod); 

            var client = new RestClient(url + "/api/v1/condicoes_pagamento/" + id);
            var request = new RestRequest(Method.PUT);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("ApplicationToken", appToken);
            request.AddHeader("CompanyToken", cmpToken);
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", json, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            return response;
        }

    }
}

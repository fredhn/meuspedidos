﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Control;

namespace Partners_MeusPedidos
{
    public partial class FormProductList : Form
    {
        Produto_Service ps = new Produto_Service();
        public FormProductList()
        {
            InitializeComponent();

            var produtos = ps.GetAll_Produtos();
            dataGridView_ProductList.DataSource = produtos;
        }

        #region MOUSE HOVER EVENTS

        private void pb_Home_MouseHover(object sender, EventArgs e)
        {
            ToolTip tt = new ToolTip();
            tt.SetToolTip(this.pb_Home, "Home");
        }

        private void pb_Produtos_MouseHover(object sender, EventArgs e)
        {
            ToolTip tt = new ToolTip();
            tt.SetToolTip(this.pb_Produtos, "Produtos");
        }

        private void pb_Clientes_MouseHover(object sender, EventArgs e)
        {
            ToolTip tt = new ToolTip();
            tt.SetToolTip(this.pb_Clientes, "Clientes");
        }

        private void pb_ProdutoCategoria_MouseHover(object sender, EventArgs e)
        {
            ToolTip tt = new ToolTip();
            tt.SetToolTip(this.pb_ProdutoCategoria, "Categoria de Produtos");
        }

        private void pb_Pedidos_MouseHover(object sender, EventArgs e)
        {
            ToolTip tt = new ToolTip();
            tt.SetToolTip(this.pb_Pedidos, "Pedidos");
        }

        #endregion

        #region UI BUTTON CLICK EVENTS 
        private void pb_Minimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pb_Close_Click(object sender, EventArgs e)
        {
            var formProd = (FormProduct)Tag;
            formProd.Show();
            Close();
        }

        private void pb_Home_Click(object sender, EventArgs e)
        {
            FormMain formMain = new FormMain();
            formMain.Tag = this;
            
            formMain.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            formMain.Location = new System.Drawing.Point((FormProductList.ActiveForm.Location.X + FormProductList.ActiveForm.Width / 2) - (formMain.Width / 2), (FormProductList.ActiveForm.Location.Y + FormProductList.ActiveForm.Height / 2) - (formMain.Height / 2));
            formMain.Show(FormMain.ActiveForm);

            Hide();
        }

        private void pb_Produtos_Click(object sender, EventArgs e)
        {
            var formProd = (FormProduct)Tag;
            formProd.Show();
            Close();
        }

        private void pb_Clientes_Click(object sender, EventArgs e)
        {

        }

        private void pb_ProdutoCategoria_Click(object sender, EventArgs e)
        {

        }

        private void pb_Pedidos_Click(object sender, EventArgs e)
        {

        }

        #endregion
    }
}

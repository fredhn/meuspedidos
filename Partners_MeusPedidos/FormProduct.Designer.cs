﻿namespace Partners_MeusPedidos
{
    partial class FormProduct
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel_topBar = new System.Windows.Forms.Panel();
            this.lbl_topBar_Logo = new System.Windows.Forms.Label();
            this.panel_toolBar = new System.Windows.Forms.Panel();
            this.panel_Prod = new System.Windows.Forms.Panel();
            this.panel_Prod_All = new System.Windows.Forms.Panel();
            this.label_Prod_All = new System.Windows.Forms.Label();
            this.panel_Prod_Specific = new System.Windows.Forms.Panel();
            this.label_Prod_Specific_Search = new System.Windows.Forms.Label();
            this.textBox_Prod_Specific = new System.Windows.Forms.TextBox();
            this.label_Prod_Specific = new System.Windows.Forms.Label();
            this.panel_footer = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox_Prod_All_Search = new System.Windows.Forms.PictureBox();
            this.pb_Pedidos = new System.Windows.Forms.PictureBox();
            this.pb_ProdutoCategoria = new System.Windows.Forms.PictureBox();
            this.pb_Clientes = new System.Windows.Forms.PictureBox();
            this.pb_Home = new System.Windows.Forms.PictureBox();
            this.pb_Produtos = new System.Windows.Forms.PictureBox();
            this.pb_Minimize = new System.Windows.Forms.PictureBox();
            this.pb_Close = new System.Windows.Forms.PictureBox();
            this.panel_topBar.SuspendLayout();
            this.panel_toolBar.SuspendLayout();
            this.panel_Prod.SuspendLayout();
            this.panel_Prod_All.SuspendLayout();
            this.panel_Prod_Specific.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Prod_All_Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Pedidos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_ProdutoCategoria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Clientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Home)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Produtos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Minimize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Close)).BeginInit();
            this.SuspendLayout();
            // 
            // panel_topBar
            // 
            this.panel_topBar.Controls.Add(this.lbl_topBar_Logo);
            this.panel_topBar.Controls.Add(this.pb_Minimize);
            this.panel_topBar.Controls.Add(this.pb_Close);
            this.panel_topBar.Location = new System.Drawing.Point(3, 2);
            this.panel_topBar.Name = "panel_topBar";
            this.panel_topBar.Size = new System.Drawing.Size(777, 22);
            this.panel_topBar.TabIndex = 0;
            // 
            // lbl_topBar_Logo
            // 
            this.lbl_topBar_Logo.AutoSize = true;
            this.lbl_topBar_Logo.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_topBar_Logo.ForeColor = System.Drawing.Color.White;
            this.lbl_topBar_Logo.Location = new System.Drawing.Point(1, -1);
            this.lbl_topBar_Logo.Name = "lbl_topBar_Logo";
            this.lbl_topBar_Logo.Size = new System.Drawing.Size(94, 23);
            this.lbl_topBar_Logo.TabIndex = 2;
            this.lbl_topBar_Logo.Text = "PARTNERS";
            // 
            // panel_toolBar
            // 
            this.panel_toolBar.Controls.Add(this.pb_Pedidos);
            this.panel_toolBar.Controls.Add(this.pb_ProdutoCategoria);
            this.panel_toolBar.Controls.Add(this.pb_Clientes);
            this.panel_toolBar.Controls.Add(this.pb_Home);
            this.panel_toolBar.Controls.Add(this.pb_Produtos);
            this.panel_toolBar.Location = new System.Drawing.Point(3, 31);
            this.panel_toolBar.Name = "panel_toolBar";
            this.panel_toolBar.Size = new System.Drawing.Size(777, 81);
            this.panel_toolBar.TabIndex = 1;
            // 
            // panel_Prod
            // 
            this.panel_Prod.Controls.Add(this.button1);
            this.panel_Prod.Controls.Add(this.panel_Prod_All);
            this.panel_Prod.Controls.Add(this.panel_Prod_Specific);
            this.panel_Prod.Location = new System.Drawing.Point(4, 118);
            this.panel_Prod.Name = "panel_Prod";
            this.panel_Prod.Size = new System.Drawing.Size(777, 404);
            this.panel_Prod.TabIndex = 2;
            // 
            // panel_Prod_All
            // 
            this.panel_Prod_All.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel_Prod_All.Controls.Add(this.pictureBox_Prod_All_Search);
            this.panel_Prod_All.Controls.Add(this.label_Prod_All);
            this.panel_Prod_All.Location = new System.Drawing.Point(393, 135);
            this.panel_Prod_All.Name = "panel_Prod_All";
            this.panel_Prod_All.Size = new System.Drawing.Size(375, 134);
            this.panel_Prod_All.TabIndex = 2;
            // 
            // label_Prod_All
            // 
            this.label_Prod_All.AutoSize = true;
            this.label_Prod_All.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Prod_All.Location = new System.Drawing.Point(63, 13);
            this.label_Prod_All.Name = "label_Prod_All";
            this.label_Prod_All.Size = new System.Drawing.Size(244, 24);
            this.label_Prod_All.TabIndex = 1;
            this.label_Prod_All.Text = "Buscar todos produtos";
            // 
            // panel_Prod_Specific
            // 
            this.panel_Prod_Specific.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel_Prod_Specific.Controls.Add(this.label_Prod_Specific_Search);
            this.panel_Prod_Specific.Controls.Add(this.textBox_Prod_Specific);
            this.panel_Prod_Specific.Controls.Add(this.label_Prod_Specific);
            this.panel_Prod_Specific.Location = new System.Drawing.Point(8, 135);
            this.panel_Prod_Specific.Name = "panel_Prod_Specific";
            this.panel_Prod_Specific.Size = new System.Drawing.Size(375, 134);
            this.panel_Prod_Specific.TabIndex = 1;
            // 
            // label_Prod_Specific_Search
            // 
            this.label_Prod_Specific_Search.AutoSize = true;
            this.label_Prod_Specific_Search.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Prod_Specific_Search.Location = new System.Drawing.Point(83, 55);
            this.label_Prod_Specific_Search.Name = "label_Prod_Specific_Search";
            this.label_Prod_Specific_Search.Size = new System.Drawing.Size(205, 16);
            this.label_Prod_Specific_Search.TabIndex = 2;
            this.label_Prod_Specific_Search.Text = "Digite o código do produto e pressione ENTER";
            // 
            // textBox_Prod_Specific
            // 
            this.textBox_Prod_Specific.Location = new System.Drawing.Point(137, 74);
            this.textBox_Prod_Specific.Name = "textBox_Prod_Specific";
            this.textBox_Prod_Specific.Size = new System.Drawing.Size(96, 20);
            this.textBox_Prod_Specific.TabIndex = 1;
            this.textBox_Prod_Specific.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox_Prod_Specific.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_Prod_Specific_KeyDown);
            this.textBox_Prod_Specific.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_Prod_Specific_KeyPress);
            // 
            // label_Prod_Specific
            // 
            this.label_Prod_Specific.AutoSize = true;
            this.label_Prod_Specific.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Prod_Specific.Location = new System.Drawing.Point(47, 13);
            this.label_Prod_Specific.Name = "label_Prod_Specific";
            this.label_Prod_Specific.Size = new System.Drawing.Size(277, 24);
            this.label_Prod_Specific.TabIndex = 0;
            this.label_Prod_Specific.Text = "Buscar produto específico";
            // 
            // panel_footer
            // 
            this.panel_footer.Location = new System.Drawing.Point(3, 528);
            this.panel_footer.Name = "panel_footer";
            this.panel_footer.Size = new System.Drawing.Size(777, 22);
            this.panel_footer.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(267, 334);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox_Prod_All_Search
            // 
            this.pictureBox_Prod_All_Search.Image = global::Partners_MeusPedidos.Properties.Resources.list__2_;
            this.pictureBox_Prod_All_Search.Location = new System.Drawing.Point(135, 40);
            this.pictureBox_Prod_All_Search.Name = "pictureBox_Prod_All_Search";
            this.pictureBox_Prod_All_Search.Size = new System.Drawing.Size(100, 87);
            this.pictureBox_Prod_All_Search.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_Prod_All_Search.TabIndex = 2;
            this.pictureBox_Prod_All_Search.TabStop = false;
            this.pictureBox_Prod_All_Search.Click += new System.EventHandler(this.pictureBox_Prod_All_Search_Click);
            // 
            // pb_Pedidos
            // 
            this.pb_Pedidos.Image = global::Partners_MeusPedidos.Properties.Resources.order;
            this.pb_Pedidos.Location = new System.Drawing.Point(512, 3);
            this.pb_Pedidos.Name = "pb_Pedidos";
            this.pb_Pedidos.Size = new System.Drawing.Size(75, 75);
            this.pb_Pedidos.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pb_Pedidos.TabIndex = 3;
            this.pb_Pedidos.TabStop = false;
            this.pb_Pedidos.Click += new System.EventHandler(this.pb_Pedidos_Click);
            this.pb_Pedidos.MouseHover += new System.EventHandler(this.pb_Pedidos_MouseHover);
            // 
            // pb_ProdutoCategoria
            // 
            this.pb_ProdutoCategoria.Image = global::Partners_MeusPedidos.Properties.Resources.menu_button;
            this.pb_ProdutoCategoria.Location = new System.Drawing.Point(431, 3);
            this.pb_ProdutoCategoria.Name = "pb_ProdutoCategoria";
            this.pb_ProdutoCategoria.Size = new System.Drawing.Size(75, 75);
            this.pb_ProdutoCategoria.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pb_ProdutoCategoria.TabIndex = 2;
            this.pb_ProdutoCategoria.TabStop = false;
            this.pb_ProdutoCategoria.Click += new System.EventHandler(this.pb_ProdutoCategoria_Click);
            this.pb_ProdutoCategoria.MouseHover += new System.EventHandler(this.pb_ProdutoCategoria_MouseHover);
            // 
            // pb_Clientes
            // 
            this.pb_Clientes.Image = global::Partners_MeusPedidos.Properties.Resources.user;
            this.pb_Clientes.Location = new System.Drawing.Point(350, 3);
            this.pb_Clientes.Name = "pb_Clientes";
            this.pb_Clientes.Size = new System.Drawing.Size(75, 75);
            this.pb_Clientes.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pb_Clientes.TabIndex = 1;
            this.pb_Clientes.TabStop = false;
            this.pb_Clientes.Click += new System.EventHandler(this.pb_Clientes_Click);
            this.pb_Clientes.MouseHover += new System.EventHandler(this.pb_Clientes_MouseHover);
            // 
            // pb_Home
            // 
            this.pb_Home.Image = global::Partners_MeusPedidos.Properties.Resources.home;
            this.pb_Home.Location = new System.Drawing.Point(189, 3);
            this.pb_Home.Name = "pb_Home";
            this.pb_Home.Size = new System.Drawing.Size(75, 75);
            this.pb_Home.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pb_Home.TabIndex = 1;
            this.pb_Home.TabStop = false;
            this.pb_Home.Click += new System.EventHandler(this.pb_Home_Click);
            this.pb_Home.MouseHover += new System.EventHandler(this.pb_Home_MouseHover);
            // 
            // pb_Produtos
            // 
            this.pb_Produtos.Image = global::Partners_MeusPedidos.Properties.Resources.product_realise__1_;
            this.pb_Produtos.Location = new System.Drawing.Point(269, 3);
            this.pb_Produtos.Name = "pb_Produtos";
            this.pb_Produtos.Size = new System.Drawing.Size(75, 75);
            this.pb_Produtos.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pb_Produtos.TabIndex = 0;
            this.pb_Produtos.TabStop = false;
            this.pb_Produtos.Click += new System.EventHandler(this.pb_Produtos_Click);
            this.pb_Produtos.MouseHover += new System.EventHandler(this.pb_Produtos_MouseHover);
            // 
            // pb_Minimize
            // 
            this.pb_Minimize.Image = global::Partners_MeusPedidos.Properties.Resources.minimize_tab;
            this.pb_Minimize.Location = new System.Drawing.Point(723, 0);
            this.pb_Minimize.Name = "pb_Minimize";
            this.pb_Minimize.Size = new System.Drawing.Size(20, 19);
            this.pb_Minimize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pb_Minimize.TabIndex = 1;
            this.pb_Minimize.TabStop = false;
            this.pb_Minimize.Click += new System.EventHandler(this.pb_Minimize_Click);
            // 
            // pb_Close
            // 
            this.pb_Close.Image = global::Partners_MeusPedidos.Properties.Resources.cross_symbol;
            this.pb_Close.Location = new System.Drawing.Point(749, 0);
            this.pb_Close.Name = "pb_Close";
            this.pb_Close.Size = new System.Drawing.Size(20, 19);
            this.pb_Close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pb_Close.TabIndex = 0;
            this.pb_Close.TabStop = false;
            this.pb_Close.Click += new System.EventHandler(this.pb_Close_Click);
            // 
            // FormProduct
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DodgerBlue;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.panel_footer);
            this.Controls.Add(this.panel_Prod);
            this.Controls.Add(this.panel_toolBar);
            this.Controls.Add(this.panel_topBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormProduct";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PARTNERS";
            this.panel_topBar.ResumeLayout(false);
            this.panel_topBar.PerformLayout();
            this.panel_toolBar.ResumeLayout(false);
            this.panel_Prod.ResumeLayout(false);
            this.panel_Prod_All.ResumeLayout(false);
            this.panel_Prod_All.PerformLayout();
            this.panel_Prod_Specific.ResumeLayout(false);
            this.panel_Prod_Specific.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Prod_All_Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Pedidos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_ProdutoCategoria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Clientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Home)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Produtos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Minimize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Close)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_topBar;
        private System.Windows.Forms.Panel panel_toolBar;
        private System.Windows.Forms.Panel panel_Prod;
        private System.Windows.Forms.Panel panel_footer;
        private System.Windows.Forms.PictureBox pb_Home;
        private System.Windows.Forms.PictureBox pb_Produtos;
        private System.Windows.Forms.PictureBox pb_Clientes;
        private System.Windows.Forms.PictureBox pb_ProdutoCategoria;
        private System.Windows.Forms.PictureBox pb_Pedidos;
        private System.Windows.Forms.PictureBox pb_Close;
        private System.Windows.Forms.PictureBox pb_Minimize;
        private System.Windows.Forms.Label lbl_topBar_Logo;
        private System.Windows.Forms.Panel panel_Prod_All;
        private System.Windows.Forms.Panel panel_Prod_Specific;
        private System.Windows.Forms.Label label_Prod_All;
        private System.Windows.Forms.TextBox textBox_Prod_Specific;
        private System.Windows.Forms.Label label_Prod_Specific;
        private System.Windows.Forms.PictureBox pictureBox_Prod_All_Search;
        private System.Windows.Forms.Label label_Prod_Specific_Search;
        private System.Windows.Forms.Button button1;
    }
}


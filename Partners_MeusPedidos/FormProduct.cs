﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;
using Control;
using Core;
using Partners_MeusPedidos.Properties;
using Service_API;

namespace Partners_MeusPedidos
{
    public partial class FormProduct : Form
    {
        Produto_Service ps = new Produto_Service();
        public FormProduct()
        {
            InitializeComponent();
        }

        #region MOUSE HOVER EVENTS

        private void pb_Home_MouseHover(object sender, EventArgs e)
        {
            ToolTip tt = new ToolTip();
            tt.SetToolTip(this.pb_Home, "Home");
        }

        private void pb_Produtos_MouseHover(object sender, EventArgs e)
        {
            ToolTip tt = new ToolTip();
            tt.SetToolTip(this.pb_Produtos, "Produtos");
        }

        private void pb_Clientes_MouseHover(object sender, EventArgs e)
        {
            ToolTip tt = new ToolTip();
            tt.SetToolTip(this.pb_Clientes, "Clientes");
        }

        private void pb_ProdutoCategoria_MouseHover(object sender, EventArgs e)
        {
            ToolTip tt = new ToolTip();
            tt.SetToolTip(this.pb_ProdutoCategoria, "Categoria de Produtos");
        }

        private void pb_Pedidos_MouseHover(object sender, EventArgs e)
        {
            ToolTip tt = new ToolTip();
            tt.SetToolTip(this.pb_Pedidos, "Pedidos");
        }

        #endregion

        #region UI BUTTON CLICK EVENTS 
        private void pb_Minimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pb_Close_Click(object sender, EventArgs e)
        {
            var formMain = (FormMain)Tag;
            formMain.Show();
            Close();
        }

        private void pb_Home_Click(object sender, EventArgs e)
        {
            var formMain = (FormMain)Tag;
            formMain.Show();
            Close();
        }

        private void pb_Produtos_Click(object sender, EventArgs e)
        {

        }

        private void pb_Clientes_Click(object sender, EventArgs e)
        {

        }

        private void pb_ProdutoCategoria_Click(object sender, EventArgs e)
        {

        }

        private void pb_Pedidos_Click(object sender, EventArgs e)
        {

        }

        #endregion

        #region KEYPRESS EVENTS
        private void textBox_Prod_Specific_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            //&& (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            /*
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
            */
        }

        #endregion

        #region KEYDOWN EVENTS
        private void textBox_Prod_Specific_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && !String.IsNullOrEmpty(textBox_Prod_Specific.Text))
            {
                //buttonTest_Click(this, new EventArgs());
                var prod = ps.GetBySpecification_Produto(int.Parse(textBox_Prod_Specific.Text));

                if(prod != null)
                {
                    MessageBox.Show("Produto encontrado! \n\n Nome: " + prod.nome);
                }
                else
                {
                    MessageBox.Show("Não existe um produto com o código " + textBox_Prod_Specific.Text + " no banco de dados!");
                }
                
            }
        }

        #endregion

        #region FORM_PRODUTOS CLICK EVENTS 
        private void pictureBox_Prod_All_Search_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("GET ALL PRODUCTS TEST");

            FormProductList formProdL = new FormProductList();
            formProdL.Tag = this;

            formProdL.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            formProdL.Location = new System.Drawing.Point((FormMain.ActiveForm.Location.X + FormMain.ActiveForm.Width / 2) - (formProdL.Width / 2), (FormMain.ActiveForm.Location.Y + FormMain.ActiveForm.Height / 2) - (formProdL.Height / 2));
            formProdL.Show(FormMain.ActiveForm);

            //formProdL.Show(this);
            Hide();
        }

        

        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {

                /*
                // Criando uma venda
                Produto novoProd = new Produto();
                novoProd.codigo = "123921";
                novoProd.nome = "Teste";
                novoProd.comissao = 23.0;
                novoProd.preco_tabela = 50.00;
                novoProd.preco_minimo = 45.00;
                novoProd.ipi = 2.00;
                novoProd.tipo_ipi = "V";
                novoProd.st = 13.00;
                novoProd.grade_cores = new PersistableStringCollection { "Preto", "Branco", "Verde"};
                novoProd.grade_tamanhos = new PersistableStringCollection { "P", "M", "G" };
                novoProd.moeda = "0";
                novoProd.unidade = "m";
                novoProd.saldo_estoque = 23.13;
                novoProd.observacoes = "teste";
                novoProd.ultima_alteracao = DateTime.Now;
                novoProd.excluido = false;
                novoProd.ativo = true;
                novoProd.categoria_id = 2;
                novoProd.codigo_ncm = "32KODA";
                novoProd.multiplo = 3.0;
                novoProd.peso_bruto = 0.3;

                // Criando o contexto
                MeusPedidosContexto context = new MeusPedidosContexto();

                // adicionando os registros e salvando
                context.Produtos.Add(novoProd);
                context.SaveChanges();
                MessageBox.Show("Registro adicionado com sucesso");
                */

                //var produtos = ps.GetAll_Produtos();

                Produto novoProd = new Produto();
                novoProd.codigo = "123";
                novoProd.nome = "Partners";
                novoProd.comissao = 23.0;
                novoProd.preco_tabela = 50.00;
                novoProd.preco_minimo = 45.00;
                novoProd.ipi = 2.00;
                novoProd.tipo_ipi = "V";
                novoProd.st = 13.00;
                novoProd.grade_cores = new PersistableStringCollection { "Preto", "Branco", "Verde" };
                novoProd.grade_tamanhos = new PersistableStringCollection { "P", "M", "G" };
                novoProd.moeda = "0";
                novoProd.unidade = "m";
                novoProd.saldo_estoque = 23.13;
                novoProd.observacoes = "OK";
                novoProd.ultima_alteracao = DateTime.Now;
                novoProd.excluido = false;
                novoProd.ativo = true;
                novoProd.categoria_id = 2;
                novoProd.codigo_ncm = "TOP";
                novoProd.multiplo = 3.0;
                novoProd.peso_bruto = 0.3;

                ps.AddNew_Produto(novoProd);

                CustomerService_GET _get = new CustomerService_GET();
                _get.AllCustomers();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Um erro ocorreu durante o processo: " + ex.Message);
            }
        }
    }
}

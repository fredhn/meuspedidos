﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Partners_MeusPedidos
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        #region MOUSE HOVER EVENTS

        private void pb_Home_MouseHover(object sender, EventArgs e)
        {
            ToolTip tt = new ToolTip();
            tt.SetToolTip(this.pb_Home, "Home");
        }

        private void pb_Produtos_MouseHover(object sender, EventArgs e)
        {
            ToolTip tt = new ToolTip();
            tt.SetToolTip(this.pb_Produtos, "Produtos");
        }

        private void pb_Clientes_MouseHover(object sender, EventArgs e)
        {
            ToolTip tt = new ToolTip();
            tt.SetToolTip(this.pb_Clientes, "Clientes");
        }

        private void pb_ProdutoCategoria_MouseHover(object sender, EventArgs e)
        {
            ToolTip tt = new ToolTip();
            tt.SetToolTip(this.pb_ProdutoCategoria, "Categoria de Produtos");
        }

        private void pb_Pedidos_MouseHover(object sender, EventArgs e)
        {
            ToolTip tt = new ToolTip();
            tt.SetToolTip(this.pb_Pedidos, "Pedidos");
        }

        #endregion

        #region UI BUTTON CLICK EVENTS 
        private void pb_Minimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pb_Close_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pb_Home_Click(object sender, EventArgs e)
        {

        }

        private void pb_Produtos_Click(object sender, EventArgs e)
        {
            FormProduct formProd = new FormProduct();
            formProd.Tag = this;
            formProd.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            formProd.Location = new System.Drawing.Point((FormMain.ActiveForm.Location.X + FormMain.ActiveForm.Width / 2) - (formProd.Width / 2), (FormMain.ActiveForm.Location.Y + FormMain.ActiveForm.Height / 2) - (formProd.Height / 2));
            formProd.Show(FormMain.ActiveForm);
            Hide();
        }

        private void pb_Clientes_Click(object sender, EventArgs e)
        {

        }

        private void pb_ProdutoCategoria_Click(object sender, EventArgs e)
        {

        }

        private void pb_Pedidos_Click(object sender, EventArgs e)
        {

        }

        #endregion

        #region MOVE WINFORMS FORM
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        private void FormMain_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void panel_topBar_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }
        #endregion
    }
}
